
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import org.apache.commons.codec.binary.Hex;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author jame
 */
public class NewClass {

    public static String calculateHMAC(String key, String data) {
        try {
            Mac hmac = Mac.getInstance("HmacSHA256");
            SecretKeySpec secret_key = new SecretKeySpec(key.getBytes("UTF-8"), "HmacSHA256");
            hmac.init(secret_key);
            System.out.println(hmac.doFinal(data.getBytes("UTF-8"))[0]);
            return new String(Hex.encodeHex(hmac.doFinal(data.getBytes("UTF-8"))));
        } catch (Exception e) {

            throw new RuntimeException(e);
        }
    }

    public static void main(String[] args) {

        final String secret_key = "J9rRf0Hn9qlh2ZEVjtUJ";
        System.out.println(calculateHMAC(secret_key, "ณิชอ่อนคำ15299007864871ไทย21/09/2538278หมู่ที่ 1ตำบลแจ้ห่มอำเภอแจ้ห่มจังหวัดลำปาง50130นาย15"));
    }
}
