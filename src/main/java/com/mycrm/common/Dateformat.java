/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycrm.common;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import org.ocpsoft.prettytime.PrettyTime;



public class Dateformat {
    

    //วันที่แบบสากล
    public static String dateFormat(Date dateformat) {        
        DateFormat dateFormat = new SimpleDateFormat("HH:mm EEEE dd MMMM yyyy", Locale.ENGLISH);
        String datetime = dateFormat.format(dateformat);
        return datetime;
    }
    
    //แปลงวันที่ของสากล Campaign
    public static String dateFormatCampaign(Date dateformat) {
        DateFormat dateFormat = new SimpleDateFormat("HH:mm dd-MM-yyyy", Locale.ENGLISH);
        String datetime = dateFormat.format(dateformat);
        return datetime;
    }
  
    //แปลงวันที่เป็นแบบไทย
    public static String dateThai(Date dateformat) {
        DateFormat df = new SimpleDateFormat("เวลา HH:mm  EEEE ที่ dd เดือน MMMM พ.ศ. yyyy", new Locale("th", "TH"));
        String datetime = df.format(dateformat);
        return datetime;
    }
    //แปลงวันที่เป็นแบบไทยของ Campaign
    public static String dateThaiCampaign(Date dateformat) {
        DateFormat df = new SimpleDateFormat("HH:mm dd-MMMM-yyyy", new Locale("th", "TH"));
        String datetime = df.format(dateformat);
 
        return datetime;
    }
    
        public static String dateFormatSecurity(Date dateformat) {        
        PrettyTime p = new PrettyTime(new Locale("th", "TH"));
        String datetime = p.format(dateformat);
        return datetime;
    }
        
       public static String dateThaiFormatSecurity(Date dateformat) {
        PrettyTime p = new PrettyTime(new Locale("th", "TH"));
        String datetime = p.format(dateformat);
        return datetime;
    }
       
       public static String dateFormatHeader(Date dateformat) {        
        DateFormat dateFormat = new SimpleDateFormat("dd MMMM yyyy", Locale.ENGLISH);
        String datetime = dateFormat.format(dateformat);
        return datetime;
    }
       public static String dateThaiFormatHeader(Date dateformat){
           DateFormat df = new SimpleDateFormat("   dd MMMM yyyy", new Locale("th", "TH"));
            String datetime = df.format(dateformat);
           return datetime;
       }
        
     
}
