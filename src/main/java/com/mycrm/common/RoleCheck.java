/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycrm.common;

import com.mycrm.controller.HomeController;
import com.mycrm.dao.RoleDAO;
import com.mycrm.domain.RoleItem;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author jame
 */
public class RoleCheck {

    @Autowired
    RoleDAO roleDAO;
    
    public Boolean pageView() {
        Boolean check = true;
        roleDAO.pageView("role", 13);
        return check;
    }

    public static Boolean checkAdmin(List<RoleItem> roleItems) {
        Boolean statusAdmin = false;
        
        Boolean templePage = false;
        Boolean roomPage = false;
        Boolean staffPage = false;
        Boolean memberPage = false;
        Boolean checkinPage = false;
        Boolean checkoutPage = false;
        Boolean rolePage = false;
        Boolean monkPage = false;
        Boolean contentPage = false;
        Boolean reportPage =false;

        for (int i = 0; i < roleItems.size(); i++) {
            if (roleItems.get(i).getPage().equals("temple_page") && roleItems.get(i).getView_page() == true && roleItems.get(i).getCreate_page() == true && roleItems.get(i).getEdit_page() == true && roleItems.get(i).getDelete_page() == true) {
                templePage = true;
            } else if (roleItems.get(i).getPage().equals("room_page") && roleItems.get(i).getView_page() == true && roleItems.get(i).getCreate_page() == true && roleItems.get(i).getEdit_page() == true && roleItems.get(i).getDelete_page() == true) {
                roomPage = true;
            } else if (roleItems.get(i).getPage().equals("staff_page") && roleItems.get(i).getView_page() == true && roleItems.get(i).getCreate_page() == true && roleItems.get(i).getEdit_page() == true && roleItems.get(i).getDelete_page() == true) {
                staffPage = true;
            } else if (roleItems.get(i).getPage().equals("member_page") && roleItems.get(i).getView_page() == true && roleItems.get(i).getCreate_page() == true && roleItems.get(i).getEdit_page() == true && roleItems.get(i).getDelete_page() == true) {
                memberPage = true;
            } else if (roleItems.get(i).getPage().equals("checkin") && roleItems.get(i).getView_page() == true && roleItems.get(i).getCreate_page() == true && roleItems.get(i).getEdit_page() == true && roleItems.get(i).getDelete_page() == true) {
                checkinPage = true;
            } else if (roleItems.get(i).getPage().equals("checkout") && roleItems.get(i).getView_page() == true && roleItems.get(i).getCreate_page() == true && roleItems.get(i).getEdit_page() == true && roleItems.get(i).getDelete_page() == true) {
                checkoutPage = true;
            } else if (roleItems.get(i).getPage().equals("role") && roleItems.get(i).getView_page() == true && roleItems.get(i).getCreate_page() == true && roleItems.get(i).getEdit_page() == true && roleItems.get(i).getDelete_page() == true) {
                rolePage = true;
            } else if (roleItems.get(i).getPage().equals("monk") && roleItems.get(i).getView_page() == true && roleItems.get(i).getCreate_page() == true && roleItems.get(i).getEdit_page() == true && roleItems.get(i).getDelete_page() == true) {
                monkPage = true;
            } else if (roleItems.get(i).getPage().equals("content") && roleItems.get(i).getView_page() == true && roleItems.get(i).getCreate_page() == true && roleItems.get(i).getEdit_page() == true && roleItems.get(i).getDelete_page() == true) {
                contentPage = true;
            } else if (roleItems.get(i).getPage().equals("report") && roleItems.get(i).getView_page() == true && roleItems.get(i).getCreate_page() == true && roleItems.get(i).getEdit_page() == true && roleItems.get(i).getDelete_page() == true){
                reportPage = true;
            }    
        }

        if (templePage == true && roomPage == true && staffPage == true && memberPage == true && checkinPage == true && checkoutPage == true && rolePage == true && monkPage == true && contentPage == true && reportPage == true) {
            // System.out.println("---- ADMIN ROLE ----");
            statusAdmin = true;
        } else {
            // System.out.println("---- USER ROLE ----");
            statusAdmin = false;
        }

        return statusAdmin;
    }
    
}
