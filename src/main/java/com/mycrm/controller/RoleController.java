/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycrm.controller;

/**
 *
 * @author Lenovo
 */
import com.mycrm.common.RoleCheck;
import com.mycrm.dao.RoleDAO;
import com.mycrm.dao.StaffDAO;
import com.mycrm.domain.Role;
import com.mycrm.domain.RoleItem;
import com.mycrm.domain.Staff;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@Component
public class RoleController {

    int last_id = 0;

    int user_id = 0;

    @Autowired
    RoleDAO roleDAO;

    @Autowired
    StaffDAO staffDAO;

    @Autowired
    HomeController homeController;

    @RequestMapping(value = "/impervious", method = RequestMethod.GET)
    public String impervious(Model model) {

        List<RoleItem> roleItems = new ArrayList<>();
        roleItems = roleDAO.getRoleITem(HomeController.getRole_id());
        Boolean statusadmin = RoleCheck.checkAdmin(roleItems);
        if (statusadmin == true) {
            model.addAttribute("roleadmin", "admin");
        } else if (statusadmin == false) {
            model.addAttribute("roleadmin", "user");
        }

        return "impervious/view";
    }

    @RequestMapping(value = "/addrole", method = RequestMethod.GET)
    public String view(Model model) {

        try {
            if (roleDAO.pageCreate("role", HomeController.getRole_id()) == false) {
                return "redirect:/impervious";
            }

        } catch (Exception e) {
            return "redirect:/";
        }

        List<RoleItem> roleItems = new ArrayList<>();
        roleItems = roleDAO.getRoleITem(HomeController.getRole_id());
        Boolean statusadmin = RoleCheck.checkAdmin(roleItems);
        if (statusadmin == true) {
            model.addAttribute("roleadmin", "admin");
        } else if (statusadmin == false) {
            model.addAttribute("roleadmin", "user");
        }
        model = homeController.getRole(model);

        model.addAttribute("status", null);
        return "role/role";
    }

    @RequestMapping(value = "/role", method = RequestMethod.GET)
    public String viewall(Model model) {

        try {
            if (roleDAO.pageView("role", HomeController.getRole_id()) == false) {
                return "redirect:/impervious";
            }

        } catch (Exception e) {
            return "redirect:/";
        }

        List<Role> role = new ArrayList<Role>();
        List<RoleItem> roleitem = new ArrayList<RoleItem>();
        role = roleDAO.getRole();
        model.addAttribute("roles", role);
        model.addAttribute("roleitem", roleitem);
        model.addAttribute("roleselect", 0);
        model.addAttribute("bt_show", null);
         model = homeController.getRole(model);

        List<RoleItem> roleItems = new ArrayList<>();
        roleItems = roleDAO.getRoleITem(HomeController.getRole_id());
        Boolean statusadmin = RoleCheck.checkAdmin(roleItems);

        model = homeController.getRole(model);
        if (statusadmin == true) {
            model.addAttribute("roleadmin", "admin");
        } else if (statusadmin == false) {
            model.addAttribute("roleadmin", "user");
        }

        return "role/roleAll";
    }

    @RequestMapping(value = "/role", method = RequestMethod.POST)
    public String get_roleitem(Model model, @RequestParam(value = "roleName", required = false) int id) {
        try {
            if (roleDAO.pageView("role", HomeController.getRole_id()) == false) {
                return "redirect:/impervious";
            }

        } catch (Exception e) {
            return "redirect:/";
        }

        List<Role> role = new ArrayList<Role>();
        List<RoleItem> roleitem = new ArrayList<RoleItem>();
        role = roleDAO.getRole();
        roleitem = roleDAO.getRoleITem(id);
        model.addAttribute("roles", role);
        model.addAttribute("roleitem", roleitem);
        model.addAttribute("roleselect", id);
        model.addAttribute("bt_show", "1111");
         model = homeController.getRole(model);

        List<RoleItem> roleItems = new ArrayList<>();
        roleItems = roleDAO.getRoleITem(HomeController.getRole_id());
        Boolean statusadmin = RoleCheck.checkAdmin(roleItems);
        if (statusadmin == true) {
            model.addAttribute("roleadmin", "admin");
        } else if (statusadmin == false) {
            model.addAttribute("roleadmin", "user");
        }

        return "role/roleAll";
    }

    @RequestMapping(value = "/addrole", method = RequestMethod.POST)
    public String add(Model model, @RequestParam(value = "checkbox1", required = false) Boolean checkbox1,
            @RequestParam(value = "checkbox2", required = false) Boolean checkbox2,
            @RequestParam(value = "checkbox3", required = false) Boolean checkbox3,
            @RequestParam(value = "checkbox4", required = false) Boolean checkbox4,
            @RequestParam(value = "checkbox5", required = false) Boolean checkbox5,
            @RequestParam(value = "checkbox6", required = false) Boolean checkbox6,
            @RequestParam(value = "checkbox7", required = false) Boolean checkbox7,
            @RequestParam(value = "checkbox8", required = false) Boolean checkbox8,
            @RequestParam(value = "checkbox9", required = false) Boolean checkbox9,
            @RequestParam(value = "checkbox10", required = false) Boolean checkbox10,
            @RequestParam(value = "checkbox11", required = false) Boolean checkbox11,
            @RequestParam(value = "checkbox12", required = false) Boolean checkbox12,
            @RequestParam(value = "checkbox13", required = false) Boolean checkbox13,
            @RequestParam(value = "checkbox14", required = false) Boolean checkbox14,
            @RequestParam(value = "checkbox15", required = false) Boolean checkbox15,
            @RequestParam(value = "checkbox16", required = false) Boolean checkbox16,
            @RequestParam(value = "checkbox17", required = false) Boolean checkbox17,
            @RequestParam(value = "checkbox18", required = false) Boolean checkbox18,
            @RequestParam(value = "checkbox19", required = false) Boolean checkbox19,
            @RequestParam(value = "checkbox20", required = false) Boolean checkbox20,
            @RequestParam(value = "checkbox21", required = false) Boolean checkbox21,
            @RequestParam(value = "checkbox22", required = false) Boolean checkbox22,
            @RequestParam(value = "checkbox23", required = false) Boolean checkbox23,
            @RequestParam(value = "checkbox24", required = false) Boolean checkbox24,
            @RequestParam(value = "checkbox25", required = false) Boolean checkbox25,
            @RequestParam(value = "checkbox26", required = false) Boolean checkbox26,
            @RequestParam(value = "checkbox27", required = false) Boolean checkbox27,
            @RequestParam(value = "checkbox28", required = false) Boolean checkbox28,
            @RequestParam(value = "checkbox29", required = false) Boolean checkbox29,
            @RequestParam(value = "checkbox30", required = false) Boolean checkbox30,
            @RequestParam(value = "checkbox31", required = false) Boolean checkbox31,
            @RequestParam(value = "checkbox32", required = false) Boolean checkbox32,
            @RequestParam(value = "checkbox33", required = false) Boolean checkbox33,
            @RequestParam(value = "checkbox34", required = false) Boolean checkbox34,
            @RequestParam(value = "checkbox35", required = false) Boolean checkbox35,
            @RequestParam(value = "checkbox36", required = false) Boolean checkbox36,
            @RequestParam(value = "checkbox37", required = false) Boolean checkbox37,
            @RequestParam(value = "checkbox38", required = false) Boolean checkbox38,
            @RequestParam(value = "checkbox39", required = false) Boolean checkbox39,
            @RequestParam(value = "checkbox40", required = false) Boolean checkbox40,
            @RequestParam(value = "roleName", required = false) String role_name
    ) {
        if (checkbox1 == null) {
            checkbox1 = false;
        }
        if (checkbox2 == null) {
            checkbox2 = false;
        }
        if (checkbox3 == null) {
            checkbox3 = false;
        }
        if (checkbox4 == null) {
            checkbox4 = false;
        }
        if (checkbox5 == null) {
            checkbox5 = false;
        }
        if (checkbox6 == null) {
            checkbox6 = false;
        }
        if (checkbox7 == null) {
            checkbox7 = false;
        }
        if (checkbox8 == null) {
            checkbox8 = false;
        }
        if (checkbox9 == null) {
            checkbox9 = false;
        }
        if (checkbox10 == null) {
            checkbox10 = false;
        }
        if (checkbox11 == null) {
            checkbox11 = false;
        }
        if (checkbox12 == null) {
            checkbox12 = false;
        }
        if (checkbox13 == null) {
            checkbox13 = false;
        }
        if (checkbox14 == null) {
            checkbox14 = false;
        }
        if (checkbox15 == null) {
            checkbox15 = false;
        }
        if (checkbox16 == null) {
            checkbox16 = false;
        }
        if (checkbox17 == null) {
            checkbox17 = false;
        }
        if (checkbox18 == null) {
            checkbox18 = false;
        }
        if (checkbox19 == null) {
            checkbox19 = false;
        }
        if (checkbox20 == null) {
            checkbox20 = false;
        }
        if (checkbox21 == null) {
            checkbox21 = false;
        }
        if (checkbox22 == null) {
            checkbox22 = false;
        }
        if (checkbox23 == null) {
            checkbox23 = false;
        }
        if (checkbox24 == null) {
            checkbox24 = false;
        }
        if (checkbox25 == null) {
            checkbox25 = false;
        }
        if (checkbox26 == null) {
            checkbox26 = false;
        }
        if (checkbox27 == null) {
            checkbox27 = false;
        }
        if (checkbox28 == null) {
            checkbox28 = false;
        }
        if (checkbox29 == null) {
            checkbox29 = false;
        }

        if (checkbox30 == null) {
            checkbox30 = false;
        }

        if (checkbox31 == null) {
            checkbox31 = false;
        }

        if (checkbox32 == null) {
            checkbox32 = false;
        }
        if (checkbox33 == null) {
            checkbox33 = false;
        }

        if (checkbox34 == null) {
            checkbox34 = false;
        }

        if (checkbox35 == null) {
            checkbox35 = false;
        }

        if (checkbox36 == null) {
            checkbox36 = false;
        }
        
        if (checkbox37 == null) {
            checkbox37 = false;
        }
        
        if (checkbox38 == null) {
            checkbox38 = false;
        }
        
        if (checkbox39 == null) {
            checkbox39 = false;
        }
        
        if (checkbox40 == null) {
            checkbox40 = false;
        }
        model = homeController.getRole(model);
        List<RoleItem> roleItems = new ArrayList<>();
        roleItems = roleDAO.getRoleITem(HomeController.getRole_id());
        Boolean statusadmin = RoleCheck.checkAdmin(roleItems);
        if (statusadmin == true) {
            model.addAttribute("roleadmin", "admin");
        } else if (statusadmin == false) {
            model.addAttribute("roleadmin", "user");
        }
        
        // check role name
        int check = roleDAO.checkName(role_name);
        if (check != 0) {
            model.addAttribute("status", "Invalid name");
            return "role/role";
        }
        Role add = new Role(0, role_name, false);
        last_id = roleDAO.insert(add);
        RoleItem temple_page = new RoleItem(0, last_id, "temple_page", checkbox1, checkbox2, checkbox3, checkbox4);
        RoleItem room_page = new RoleItem(0, last_id, "room_page", checkbox5, checkbox6, checkbox7, checkbox8);
        RoleItem staff_page = new RoleItem(0, last_id, "staff_page", checkbox9, checkbox10, checkbox11, checkbox12);
        RoleItem member_page = new RoleItem(0, last_id, "member_page", checkbox13, checkbox14, checkbox15, checkbox16);
        RoleItem checkin = new RoleItem(0, last_id, "checkin", checkbox17, checkbox18, checkbox19, checkbox20);
        RoleItem checkout = new RoleItem(0, last_id, "checkout", checkbox21, checkbox22, checkbox23, checkbox24);
        RoleItem role = new RoleItem(0, last_id, "role", checkbox25, checkbox26, checkbox27, checkbox28);
        RoleItem monk = new RoleItem(0, last_id, "monk", checkbox29, checkbox30, checkbox31, checkbox32);
        RoleItem content = new RoleItem(0, last_id, "content", checkbox33, checkbox34, checkbox35, checkbox36);
        RoleItem report = new RoleItem(0, last_id, "report", checkbox37, checkbox38, checkbox39, checkbox40);
        roleDAO.insertRoleItem(temple_page); //1
        roleDAO.insertRoleItem(room_page);   //2
        roleDAO.insertRoleItem(staff_page);  //3
        roleDAO.insertRoleItem(member_page); //4
        roleDAO.insertRoleItem(checkin);     //5
        roleDAO.insertRoleItem(checkout);    //6
        roleDAO.insertRoleItem(role);        //7
        roleDAO.insertRoleItem(monk);
        roleDAO.insertRoleItem(content);
        roleDAO.insertRoleItem(report);
        return "redirect:/role";
    }

    @RequestMapping(value = "/updateroleitem", method = RequestMethod.POST)
    public String updateItem(Model model,
            @RequestParam(value = "v0", required = false) Boolean checkbox1,
            @RequestParam(value = "c0", required = false) Boolean checkbox2,
            @RequestParam(value = "e0", required = false) Boolean checkbox3,
            @RequestParam(value = "d0", required = false) Boolean checkbox4,
            @RequestParam(value = "v1", required = false) Boolean checkbox5,
            @RequestParam(value = "c1", required = false) Boolean checkbox6,
            @RequestParam(value = "e1", required = false) Boolean checkbox7,
            @RequestParam(value = "d1", required = false) Boolean checkbox8,
            @RequestParam(value = "v2", required = false) Boolean checkbox9,
            @RequestParam(value = "c2", required = false) Boolean checkbox10,
            @RequestParam(value = "e2", required = false) Boolean checkbox11,
            @RequestParam(value = "d2", required = false) Boolean checkbox12,
            @RequestParam(value = "v3", required = false) Boolean checkbox13,
            @RequestParam(value = "c3", required = false) Boolean checkbox14,
            @RequestParam(value = "e3", required = false) Boolean checkbox15,
            @RequestParam(value = "d3", required = false) Boolean checkbox16,
            @RequestParam(value = "v4", required = false) Boolean checkbox17,
            @RequestParam(value = "c4", required = false) Boolean checkbox18,
            @RequestParam(value = "e4", required = false) Boolean checkbox19,
            @RequestParam(value = "d4", required = false) Boolean checkbox20,
            @RequestParam(value = "v5", required = false) Boolean checkbox21,
            @RequestParam(value = "c5", required = false) Boolean checkbox22,
            @RequestParam(value = "e5", required = false) Boolean checkbox23,
            @RequestParam(value = "d5", required = false) Boolean checkbox24,
            @RequestParam(value = "v6", required = false) Boolean checkbox25,
            @RequestParam(value = "c6", required = false) Boolean checkbox26,
            @RequestParam(value = "e6", required = false) Boolean checkbox27,
            @RequestParam(value = "d6", required = false) Boolean checkbox28,
            @RequestParam(value = "v7", required = false) Boolean checkbox29,
            @RequestParam(value = "c7", required = false) Boolean checkbox30,
            @RequestParam(value = "e7", required = false) Boolean checkbox31,
            @RequestParam(value = "d7", required = false) Boolean checkbox32,
            @RequestParam(value = "v8", required = false) Boolean checkbox33,
            @RequestParam(value = "c8", required = false) Boolean checkbox34,
            @RequestParam(value = "e8", required = false) Boolean checkbox35,
            @RequestParam(value = "d8", required = false) Boolean checkbox36,
            @RequestParam(value = "v9", required = false) Boolean checkbox37,
            @RequestParam(value = "c9", required = false) Boolean checkbox38,
            @RequestParam(value = "e9", required = false) Boolean checkbox39,
            @RequestParam(value = "d9", required = false) Boolean checkbox40,
            @RequestParam(value = "role_id", required = false) int role_id
    ) {
        if (checkbox1 == null) {
            checkbox1 = false;
        }
        if (checkbox2 == null) {
            checkbox2 = false;
        }
        if (checkbox3 == null) {
            checkbox3 = false;
        }
        if (checkbox4 == null) {
            checkbox4 = false;
        }
        if (checkbox5 == null) {
            checkbox5 = false;
        }
        if (checkbox6 == null) {
            checkbox6 = false;
        }
        if (checkbox7 == null) {
            checkbox7 = false;
        }
        if (checkbox8 == null) {
            checkbox8 = false;
        }
        if (checkbox9 == null) {
            checkbox9 = false;
        }
        if (checkbox10 == null) {
            checkbox10 = false;
        }
        if (checkbox11 == null) {
            checkbox11 = false;
        }
        if (checkbox12 == null) {
            checkbox12 = false;
        }
        if (checkbox13 == null) {
            checkbox13 = false;
        }
        if (checkbox14 == null) {
            checkbox14 = false;
        }
        if (checkbox15 == null) {
            checkbox15 = false;
        }
        if (checkbox16 == null) {
            checkbox16 = false;
        }
        if (checkbox17 == null) {
            checkbox17 = false;
        }
        if (checkbox18 == null) {
            checkbox18 = false;
        }
        if (checkbox19 == null) {
            checkbox19 = false;
        }
        if (checkbox20 == null) {
            checkbox20 = false;
        }
        if (checkbox21 == null) {
            checkbox21 = false;
        }
        if (checkbox22 == null) {
            checkbox22 = false;
        }
        if (checkbox23 == null) {
            checkbox23 = false;
        }
        if (checkbox24 == null) {
            checkbox24 = false;
        }
        if (checkbox25 == null) {
            checkbox25 = false;
        }
        if (checkbox26 == null) {
            checkbox26 = false;
        }
        if (checkbox27 == null) {
            checkbox27 = false;
        }
        if (checkbox28 == null) {
            checkbox28 = false;
        }
        if (checkbox29 == null) {
            checkbox29 = false;
        }
        if (checkbox30 == null) {
            checkbox30 = false;
        }
        if (checkbox31 == null) {
            checkbox31 = false;
        }
        if (checkbox32 == null) {
            checkbox32 = false;
        }
        if (checkbox33 == null) {
            checkbox33 = false;
        }
        if (checkbox34 == null) {
            checkbox34 = false;
        }
        if (checkbox35 == null) {
            checkbox35 = false;
        }
        if (checkbox36 == null) {
            checkbox36 = false;
        }
        if (checkbox37 == null) {
            checkbox37 = false;
        }
        if (checkbox38 == null) {
            checkbox38 = false;
        }
        if (checkbox39 == null) {
            checkbox39 = false;
        }
        if (checkbox40 == null) {
            checkbox40 = false;
        }

        RoleItem temple_page = new RoleItem(0, role_id, "temple_page", checkbox1, checkbox2, checkbox3, checkbox4);
        RoleItem room_page = new RoleItem(0, role_id, "room_page", checkbox5, checkbox6, checkbox7, checkbox8);
        RoleItem staff_page = new RoleItem(0, role_id, "staff_page", checkbox9, checkbox10, checkbox11, checkbox12);
        RoleItem member_page = new RoleItem(0, role_id, "member_page", checkbox13, checkbox14, checkbox15, checkbox16);
        RoleItem checkin = new RoleItem(0, role_id, "checkin", checkbox17, checkbox18, checkbox19, checkbox20);
        RoleItem checkout = new RoleItem(0, role_id, "checkout", checkbox21, checkbox22, checkbox23, checkbox24);
        RoleItem role = new RoleItem(0, role_id, "role", checkbox25, checkbox26, checkbox27, checkbox28);
        RoleItem monk = new RoleItem(0, role_id, "monk", checkbox29, checkbox30, checkbox31, checkbox32);
        RoleItem content = new RoleItem(0, role_id, "content", checkbox33, checkbox34, checkbox35, checkbox36);
        RoleItem report = new RoleItem(0, role_id, "report", checkbox37, checkbox38, checkbox39, checkbox40);
        roleDAO.updateRoleItem(temple_page); //0
        roleDAO.updateRoleItem(room_page);   //1
        roleDAO.updateRoleItem(staff_page);  //2
        roleDAO.updateRoleItem(member_page); //3
        roleDAO.updateRoleItem(checkin);     //4
        roleDAO.updateRoleItem(checkout);    //5
        roleDAO.updateRoleItem(role);        //6
        roleDAO.updateRoleItem(monk);
        roleDAO.updateRoleItem(content);
        roleDAO.updateRoleItem(report);
        return "redirect:/role";
    }

    @RequestMapping(value = "/deleteroleitem", method = RequestMethod.POST)
    public String deletestaff(Model model, @RequestParam(value = "id") int id) {
        roleDAO.delete(id);
        return "redirect:/role";
    }

}
