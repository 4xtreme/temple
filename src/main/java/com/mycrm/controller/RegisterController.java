/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycrm.controller;

import com.mycrm.common.RoleCheck;
import com.mycrm.common.VerifyRecaptcha;
import java.util.List;
import com.mycrm.dao.ContactDAO;
import com.mycrm.dao.RoleDAO;
import com.mycrm.dao.StaffDAO;
import com.mycrm.dao.TempleDAO;
import com.mycrm.domain.Register;
import com.mycrm.domain.RoleItem;
import com.mycrm.domain.Staff;
import com.mycrm.domain.Temple;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;

import java.util.Arrays;
import java.util.Base64;
import org.springframework.stereotype.Component;
import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author SOMON
 */
@Controller
@Component
public class RegisterController {

    @Autowired
    ContactDAO contactDAO;

    @Autowired
    HomeController homeController;

    Register register;

    private int PER_PAGE = 15;

    private static SecretKeySpec secretKey;

    private static byte[] key;

    @Autowired
    RoleDAO roleDAO;

    @Autowired
    TempleDAO templeDAO;

    @Autowired
    StaffDAO staffDAO;

    public static void setKey(String myKey) {
        MessageDigest sha = null;
        try {
            key = myKey.getBytes("UTF-8");
            sha = MessageDigest.getInstance("SHA-1");
            key = sha.digest(key);
            key = Arrays.copyOf(key, 16);
            secretKey = new SecretKeySpec(key, "AES");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    public static String encrypt(String strToEncrypt, String secret) {
        try {
            setKey(secret);
            Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
            cipher.init(Cipher.ENCRYPT_MODE, secretKey);
            return Base64.getEncoder().encodeToString(cipher.doFinal(strToEncrypt.getBytes("UTF-8")));
        } catch (Exception e) {
            System.out.println("Error while encrypting: " + e.toString());
        }
        return null;
    }

    public static String decrypt(String strToDecrypt, String secret) {
        try {
            setKey(secret);
            Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5PADDING");
            cipher.init(Cipher.DECRYPT_MODE, secretKey);
            return new String(cipher.doFinal(Base64.getDecoder().decode(strToDecrypt)));
        } catch (Exception e) {
            System.out.println("Error while decrypting: " + e.toString());
        }
        return null;
    }

    @RequestMapping(value = "/regcreate", method = RequestMethod.GET)
    public String registerContacts(Model model) {

        List<Temple> temple = new ArrayList<Temple>();
        temple = templeDAO.getALL();
        model.addAttribute("temple", temple);
        model.addAttribute("Register", new Register());
        model.addAttribute("action", "regcreate");

        return "contact/reg";
    }

    @RequestMapping(value = "/checkmail/{email}", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public String searchEmail(@PathVariable String email) throws SQLException {
        System.out.println("From JSP : " + email);

        boolean count = contactDAO.emailExits(email);

        System.out.println("count: " + count);
        String status = "OKs";
        return "{\'status\' : " + status + "}";
    }

    @RequestMapping(value = "/checkmail", method = RequestMethod.GET)
    @ResponseBody
    public String searchEmai(@RequestParam("email") String email) {
        System.out.println("Email from JSP : " + email);

        int count = contactDAO.countEmail(email);

        if (count == 0) {
            System.out.println("Can Regis");
        } else {
            System.out.println("Exits not regis");
        }
        return email;
    }

    @RequestMapping(value = "/regcreate", method = RequestMethod.POST)
    public String registerCreate(Model model, @Valid @ModelAttribute("Register") Register register, BindingResult result,
            @RequestParam(value = "g-recaptcha-response") String gRecaptchaResponse,
            @RequestParam("email") String email,
            @RequestParam("district") String district,
            @RequestParam("personid") String personid) throws ParseException {
        List<Temple> temple = new ArrayList<Temple>();
        temple = templeDAO.getALL();
        model.addAttribute("temple", temple);
        //----------checkEMail---------------------------------
        System.out.println("Email from jsp : " + register.getEmail());
        int count = contactDAO.countEmail(email);

        if (count == 0) {
            System.out.println("Can Regis");
        } else {
            System.out.println("Exits not regis");
            model.addAttribute("invalidmail", "x");
            return "contact/reg";
        }
        //----------checkPersonID-------------------------------
        String encryptPersonIDString = RegisterController.encrypt(register.getPersonid(), HomeController.getKey_hash_profile());
        int countPersonID = contactDAO.countPersonID(encryptPersonIDString);
        if (countPersonID == 0) {
            System.out.println("This personID can Regis");
        } else {
            model.addAttribute("invalidpersonID", "x");
            return "contact/reg";
        }
        //-----------Validate-----------------------------------
        System.out.println("REsult Before: " + result);
        if (result.hasErrors()) {
            String errorField = result.getFieldError().getField();
            System.out.println("Missing some field : " + errorField);
            if (!register.getPassword().equals(register.getRepassword())) {
                System.out.println("Password : " + register.getPassword());
                System.out.println("Repassword : " + register.getRepassword());
                System.out.println("Password is not the same");
                return "contact/reg";
            }
            return "contact/reg";
        }
//        //---------------Recaptcha------------------------------
        System.out.println("gRecaptchaResponse " + gRecaptchaResponse);
        boolean verify = false;
        try {
            verify = VerifyRecaptcha.verify(gRecaptchaResponse);
        } catch (IOException ex) {
            Logger.getLogger(RegisterController.class.getName()).log(Level.SEVERE, null, ex);
        }
        if (verify != true) {
            model.addAttribute("invalidcap", "x");
            return "contact/reg";
        }
//        System.out.println("---------------" + verify);

        //-------------Encryption--------------------------------
        String encryptAddressString = RegisterController.encrypt(register.getAddress(), HomeController.getKey_hash_profile());
        String encryptDistrictString = RegisterController.encrypt(register.getDistrict(), HomeController.getKey_hash_profile());
        String de = RegisterController.decrypt(encryptAddressString, HomeController.getKey_hash_profile());
        System.out.println("de before send to db : " + de);
        //String decryptString = RegisterController.decrypt(encryptString, secretKey);
        register.setPersonid(encryptPersonIDString);
        register.setAddress(encryptAddressString);
        register.setDistrict(encryptDistrictString);

        //-------------SendtoDAO---------------------------------
        System.out.println("Register----------------- " + register.toString());
        if (register.getNametitle().equals("นาย") || register.getNametitle().equals("พระ") || register.getNametitle().equals("สามเณร")) {
            register.setGender("ชาย");
        } else {
            register.setGender("หญิง");
        }

        contactDAO.insertreg(register);
        return "contact/regsuccess";
    }

    @RequestMapping(value = "/regsall", method = RequestMethod.GET)
    public String registerAll(Model model, @RequestParam(value = "page", required = false) String page) {
        String page_Create = "OK";
        String page_Delete = "OK";
        String page_Edit = "OK";
        try {
            if (roleDAO.pageView("member_page", HomeController.getRole_id()) == false) {
                return "redirect:/impervious";
            }
        } catch (Exception e) {
            return "redirect:/";
        }

        if (roleDAO.pageCreate("member_page", HomeController.getRole_id()) == false) {
            page_Create = null;
        }

        if (roleDAO.pageEdit("member_page", HomeController.getRole_id()) == false) {
            page_Edit = null;
        }
        if (roleDAO.pageDelete("member_page", HomeController.getRole_id()) == false) {
            page_Delete = null;

        }
        if (page == null || Integer.parseInt(page) < 0) {
            page = "0";
        }
        int temple_id = HomeController.getTemple_id();
        int countRow = 0;
        int page_count = 0;
        countRow = contactDAO.countRows(temple_id);
        Double pagecountDB = Math.ceil((double) countRow / (double) PER_PAGE);
        page_count = pagecountDB.intValue();
        if (Integer.parseInt(page) > 0) {
            model.addAttribute("page_priv", true);
        }
        if (Integer.parseInt(page) < (page_count - 1)) {
            model.addAttribute("page_next", true);
        }
        if (page_count >= 10) {
            page_count = 10;
        }
        List<Register> reg = new ArrayList<Register>();
        reg = contactDAO.getRegList(temple_id, page, PER_PAGE);
        model.addAttribute("page", page);
        model.addAttribute("page_count", page_count);
        for (Register register : reg) {
            String decryptAddressString = RegisterController.decrypt(register.getAddress(), HomeController.getKey_hash_profile());
            String decryptPersonIDString = RegisterController.decrypt(register.getPersonid(), HomeController.getKey_hash_profile());
            String decryptDistrictString = RegisterController.decrypt(register.getDistrict(), HomeController.getKey_hash_profile());
            register.setAddress(decryptAddressString);
            register.setDistrict(decryptDistrictString);
            register.setPersonid(decryptPersonIDString);
        }

        model = homeController.getRole(model);
        model.addAttribute("registerlist", reg);
        model.addAttribute("page_Create", page_Create);
        model.addAttribute("page_Delete", page_Delete);
        model.addAttribute("page_Edit", page_Edit);

        List<RoleItem> roleItems = new ArrayList<>();
        roleItems = roleDAO.getRoleITem(HomeController.getRole_id());
        Boolean statusadmin = RoleCheck.checkAdmin(roleItems);
        if (statusadmin == true) {
            model.addAttribute("roleadmin", "admin");
        } else if (statusadmin == false) {
            model.addAttribute("roleadmin", "user");
        }

        return "contact/contactAll";

    }

    @RequestMapping(value = "/searchreg", method = RequestMethod.POST)
    public String registerSearch(Model model, @RequestParam(value = "typeSearch") String typeSearch,
            @RequestParam(value = "message") String message) {
        List<Register> reg = new ArrayList<Register>();

        reg = contactDAO.findContact(typeSearch, message, HomeController.getTemple_id());
        System.out.println("find : " + reg);

        for (Register register : reg) {
            String decryptAddressString = RegisterController.decrypt(register.getAddress(), HomeController.getKey_hash_profile());
            String decryptPersonIDString = RegisterController.decrypt(register.getPersonid(), HomeController.getKey_hash_profile());
            String decryptDistrictString = RegisterController.decrypt(register.getDistrict(), HomeController.getKey_hash_profile());
            register.setPersonid(decryptPersonIDString);
            register.setAddress(decryptAddressString);
            register.setDistrict(decryptDistrictString);
        }

        List<RoleItem> roleItems = new ArrayList<>();
        roleItems = roleDAO.getRoleITem(HomeController.getRole_id());
        Boolean statusadmin = RoleCheck.checkAdmin(roleItems);
        if (statusadmin == true) {
            model.addAttribute("roleadmin", "admin");
        } else if (statusadmin == false) {
            model.addAttribute("roleadmin", "user");
        }
        
        model = homeController.getRole(model);
        model.addAttribute("page_Create", "searchPage");
        model.addAttribute("page_Delete", "deletePage");
        model.addAttribute("page_Edit", "editPage");
        model.addAttribute("registerlist", reg);
        return "contact/regfind";
    }

}
