package com.mycrm.controller;

import com.mycrm.common.RoleCheck;
import com.mycrm.dao.BookingDAO;
import com.mycrm.dao.ContactDAO;
import com.mycrm.dao.RoleDAO;
import com.mycrm.dao.RoomDAO;
import com.mycrm.dao.StaffDAO;
import com.mycrm.dao.TempleDAO;
import com.mycrm.domain.CheckinHistory;
import com.mycrm.domain.OverviewData;
import com.mycrm.domain.PeopleLiving;
import com.mycrm.domain.RoleItem;
import com.mycrm.domain.Room;
import com.mycrm.domain.RoomName;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@Component
public class RoomOverViewController {

    @Autowired
    RoomDAO roomDAO;

    @Autowired
    TempleDAO templeDAO;

    @Autowired
    BookingDAO bookingDAO;

    @Autowired
    ContactDAO contactDAO;

    @Autowired
    RoleDAO roleDAO;

    @Autowired
    StaffDAO staffDAO;

    @Autowired
    HomeController homeController;

    @RequestMapping(value = "/roomfront", method = RequestMethod.GET)
    public String getRoomFront(Model model) {
        int checkReport = roleDAO.checkNameReport(HomeController.getRole_id());
        System.out.println("check report : " + checkReport);
        if (checkReport == 0) {
            roleDAO.insertReport(HomeController.getRole_id());
        } else {
            System.out.println("not null");
        }
        try {
            if (roleDAO.pageCreate("temple_page", HomeController.getRole_id()) == false) {
                return "redirect:/impervious";
            }
            model = homeController.getRole(model);
        } catch (Exception e) {
            System.out.println("Error:" + e.getMessage());
            return "redirect:/";
        }
        List<RoleItem> roleItems = new ArrayList<>();
        roleItems = roleDAO.getRoleITem(HomeController.getRole_id());
        Boolean statusadmin = RoleCheck.checkAdmin(roleItems);
        if (statusadmin == true) {
            model.addAttribute("roleadmin", "admin");
        } else if (statusadmin == false) {
            model.addAttribute("roleadmin", "user");
        }
        List<Room> room = new ArrayList<Room>();
        room = roomDAO.getRoomByTemple(HomeController.getTemple_id());
        model.addAttribute("builder", room);
        return "roomfront/roomfront";
    }

    @RequestMapping(value = "/searchpeople", method = RequestMethod.POST)
    public String registerSearch(Model model, @RequestParam(value = "typeSearch") String typeSearch,
            @RequestParam(value = "message") String message) {

        try {
//            if (roleDAO.pageCreate("temple_page", HomeController.getRole_id()) == false) {
//                return "redirect:/impervious";
//            }
            List<PeopleLiving> living = new ArrayList<>();
            if (typeSearch.equals("card")) {
                living = contactDAO.getPeopleLiving(typeSearch, RegisterController.encrypt(message, HomeController.getKey_hash_profile()), HomeController.getTemple_id());
            } else {
                living = contactDAO.getPeopleLiving(typeSearch, message, HomeController.getTemple_id());
            }

            model.addAttribute("living", living);

            System.out.println("find : " + living);

        } catch (Exception e) {
            System.out.println("Error:" + e.getMessage());
            return "redirect:/";
        }

        List<RoleItem> roleItems = new ArrayList<>();
        roleItems = roleDAO.getRoleITem(HomeController.getRole_id());
        Boolean statusadmin = RoleCheck.checkAdmin(roleItems);
        if (statusadmin == true) {
            model.addAttribute("roleadmin", "admin");
        } else if (statusadmin == false) {
            model.addAttribute("roleadmin", "user");
        }
        model = homeController.getRole(model);
        List<Room> room = new ArrayList<Room>();
        room = roomDAO.getRoomByTemple(HomeController.getTemple_id());
//        System.out.println("rooms: " + room.toString());
        model.addAttribute("builder", room);

        model.addAttribute("page_Create", "searchPage");
        model.addAttribute("page_Delete", "deletePage");
        model.addAttribute("page_Edit", "editPage");
//        model.addAttribute("registerlist", reg);
        return "roomfront/roomfront_search";
    }

    @RequestMapping(value = "/roomfront/getcheckins/{id}/{type}", method = RequestMethod.GET)
    public @ResponseBody
    ResponseEntity<Map> getCheckinHistory(@PathVariable(value = "id") int id,
            @PathVariable(value = "type") int type) throws ParseException {
        System.out.println("ID checkinhistory : " + id + " " + "TYPE : " + type);
        List<OverviewData> overviewDatas = new ArrayList<OverviewData>();
        if (type == 0) {
            overviewDatas = bookingDAO.getOverviewData(HomeController.getTemple_id(), id);
        } else {
            overviewDatas = bookingDAO.getOverviewDataRoom(HomeController.getTemple_id(), id);
        }
        System.out.println("overviewDatas : " + overviewDatas.toString() + "\n");
        Date date = new Date();
        date.setTime(type);
        List<Room> room = new ArrayList<Room>();
        room = roomDAO.getRoomByTemple(HomeController.getTemple_id());
        roomDAO.updateRoomReserve();
        for (int k = 0; k < overviewDatas.size(); k++) {
            if (overviewDatas.get(k).getType().equals("เช็คอิน")) {
                updateReserveRoom(overviewDatas.get(k).getRoom_name_id());             
            }
        }
        List<RoomName> roomNames = new ArrayList<RoomName>();
        roomNames = roomDAO.getRoomNameByRoom(id);
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        SimpleDateFormat dff = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        Date today = Calendar.getInstance().getTime();
       

        for (int i = 0; i < overviewDatas.size(); i++) {
            Date endDate = dff.parse(overviewDatas.get(i).getCheckout_day());
            if(endDate.before(today)){
                overviewDatas.get(i).setType("เช็คเอ๊า");
                CheckinHistory checkinHistory = new CheckinHistory();
                checkinHistory = bookingDAO.find(overviewDatas.get(i).getId());
                checkinHistory.setStatus("เช็คเอ๊า");
                bookingDAO.updateCheckOut(checkinHistory);
            }
            if (overviewDatas.get(i).getType().equals("จอง")) {
                overviewDatas.get(i).setType("0");
            } else if (overviewDatas.get(i).getType().equals("เช็คอิน")) {
                overviewDatas.get(i).setType("1");
            } else if (overviewDatas.get(i).getType().equals("เช็คเอ๊า")) {
                overviewDatas.get(i).setType("2");
            }
        }
        Map map = new HashMap();
        map.put("overview", overviewDatas);
        map.put("room", room);
        map.put("room_name", roomNames);

        roomDAO.updateRoomReserve();
        return new ResponseEntity<Map>(map, HttpStatus.OK);
    }

    @RequestMapping(value = "/roomfront/getroom/{id}", method = RequestMethod.GET)
    public @ResponseBody
    ResponseEntity<List<RoomName>> getRoom(@PathVariable(value = "id") int id) throws ParseException {
        System.out.println("ID roomlist: " + id);
        List<RoomName> roomNames = new ArrayList<RoomName>();
        roomNames = roomDAO.getRoomName(id);
        System.out.println("RoomList " + roomNames);

        return new ResponseEntity<List<RoomName>>(roomNames, HttpStatus.OK);
    }

    @RequestMapping(value = "/roomfront/cancelreserve/{id}", method = RequestMethod.GET)
    public @ResponseBody
    ResponseEntity<String> cancelReserve(@PathVariable(value = "id") int id) throws ParseException {
        bookingDAO.updateDelete(id);
        System.out.println("ID delete:" + id);
        return new ResponseEntity<String>("1", HttpStatus.OK);
    }

    public void updateReserveRoom(int room_id) {
        RoomName roomName = new RoomName();
        roomName = roomDAO.getRoomNameByID(room_id);
        roomName.setLive(roomName.getLive() + 1);
        if (roomName.getAmount() == roomName.getLive()) {
            roomName.setStatus(1);
        }
        roomDAO.updateRoomName(roomName);
    }

}
