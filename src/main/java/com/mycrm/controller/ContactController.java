/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycrm.controller;

import com.mycrm.common.RoleCheck;
import com.mycrm.dao.ContactDAO;
import com.mycrm.dao.RoleDAO;
import com.mycrm.dao.StaffDAO;
import com.mycrm.dao.TempleDAO;
import com.mycrm.domain.Monk;
import com.mycrm.domain.Register;
import com.mycrm.domain.RoleItem;
import com.mycrm.domain.Temple;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

import javax.validation.Valid;
import org.apache.commons.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.support.ByteArrayMultipartFileEditor;

@Controller

public class ContactController {

    @Autowired
    ContactDAO contactDAO;
    RegisterController registercontroller;

    private int monk_id = 10;
    private int PER_PAGE = 10;
    @Autowired
    RoleDAO roleDAO;

    @Autowired
    TempleDAO templeDAO;

    @Autowired
    StaffDAO staffDAO;

    @Autowired
    HomeController homeController;

    @InitBinder
    protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) throws ServletException {
        binder.registerCustomEditor(byte[].class,
                new ByteArrayMultipartFileEditor());
    }

    @RequestMapping(value = "/monkreg", method = RequestMethod.GET)
    public String monkContact(Model model) {
        try {
            if (roleDAO.pageCreate("monk", HomeController.getRole_id()) == false) {
                return "redirect:/impervious";
            }

        } catch (Exception e) {
            return "redirect:/";
        }

        List<RoleItem> roleItems = new ArrayList<>();
        roleItems = roleDAO.getRoleITem(HomeController.getRole_id());
        Boolean statusadmin = RoleCheck.checkAdmin(roleItems);
        if (statusadmin == true) {
            model.addAttribute("roleadmin", "admin");
        } else if (statusadmin == false) {
            model.addAttribute("roleadmin", "user");
        }

        List<Temple> temple = new ArrayList<Temple>();
        temple = templeDAO.getALL();
        model = homeController.getRole(model);
        model.addAttribute("Monk", new Monk());
        model.addAttribute("action", "monkadd");
        model.addAttribute("action1", "monkaddPic");
        model.addAttribute("temple", temple);
        return "contact/monkContact";
    }

    @RequestMapping(value = "/monkall", method = RequestMethod.GET)
    public String monkAll(Model model, @RequestParam(value = "page", required = false) String page) {
        String page_Create = "OK";
        String page_Delete = "OK";
        String page_Edit = "OK";
        try {
            if (roleDAO.pageView("temple_page", HomeController.getRole_id()) == false) {
                return "redirect:/impervious";
            }

        } catch (Exception e) {
            return "redirect:/";
        }

        if (roleDAO.pageCreate("monk", HomeController.getRole_id()) == false) {
            page_Create = null;
        }

        if (roleDAO.pageEdit("monk", HomeController.getRole_id()) == false) {
            page_Edit = null;
        }
        if (roleDAO.pageDelete("monk", HomeController.getRole_id()) == false) {
            page_Delete = null;
        }
        if (page == null || Integer.parseInt(page) < 0) {
            page = "0";
        }

        List<RoleItem> roleItems = new ArrayList<>();
        roleItems = roleDAO.getRoleITem(HomeController.getRole_id());
        Boolean statusadmin = RoleCheck.checkAdmin(roleItems);
        if (statusadmin == true) {
            model.addAttribute("roleadmin", "admin");
        } else if (statusadmin == false) {
            model.addAttribute("roleadmin", "user");
        }

        int countRow = 0;
        int page_count = 0;
        int temple_id = HomeController.getTemple_id();
        countRow = contactDAO.countRowsMonk(temple_id);
        Double pagecountDB = Math.ceil((double) countRow / (double) PER_PAGE);
        page_count = pagecountDB.intValue();
        List<Monk> monklist = new ArrayList<Monk>();
        monklist = contactDAO.getMonkPage(temple_id, page, PER_PAGE);
        try {
            for (int i = 0; i < monklist.size(); i++) {
                monklist.get(i).setMonkByte(monklist.get(i).getPic_monk());
                monklist.get(i).setMonkString(new String(Base64.encodeBase64(monklist.get(i).getMonkByte())));
            }

        } catch (Exception e) {
        }
        if (Integer.parseInt(page) > 0) {
            model.addAttribute("page_priv", true);
        }
        if (Integer.parseInt(page) < (page_count - 1)) {
            model.addAttribute("page_next", true);
        }
        if (page_count >= 10) {
            page_count = 10;
        }
        model = homeController.getRole(model);
        model.addAttribute("page_count", page_count);
        model.addAttribute("page", page);
        model.addAttribute("page_Create", page_Create);
        model.addAttribute("page_Delete", page_Delete);
        model.addAttribute("page_Edit", page_Edit);
        model.addAttribute("monklist", monklist);
        return "contact/monkAll";
    }

    @RequestMapping(value = "/monkadd", headers = ("content-type=multipart/*"), method = RequestMethod.POST)
    public String createMonk(Model model, @Valid @ModelAttribute("Monk") Monk monk, BindingResult result,
            @RequestParam(value = "pic_monk") MultipartFile pic_monk) {
//          if(result.hasErrors()){
//              String errorField =  result.getFieldError().getField();
//              System.out.println("Missing some field : "+errorField);
//              return "contact/monkContact";
//          }
        if (result.hasErrors()) {
            List<RoleItem> roleItems = new ArrayList<>();
            roleItems = roleDAO.getRoleITem(HomeController.getRole_id());
            Boolean statusadmin = RoleCheck.checkAdmin(roleItems);
            if (statusadmin == true) {
                model.addAttribute("roleadmin", "admin");
            } else if (statusadmin == false) {
                model.addAttribute("roleadmin", "user");
            }

            model = homeController.getRole(model);
            return "contact/monkContact";
        }

        try {
            monk.setPic_monk(pic_monk.getBytes());
        } catch (IOException ex) {
            Logger.getLogger(ContactController.class.getName()).log(Level.SEVERE, null, ex);
        }
        int temple_id = HomeController.getTemple_id();
        monk.setTemple_id(temple_id);
        //int a = Integer.parseInt("1");
        monk.setIs_deactivate(false);
        System.out.println("Monk+++++++++ " + monk.toString());
        contactDAO.insert(monk);
        return "redirect:/monkall";
    }

    @RequestMapping(value = "/selectedit", method = RequestMethod.POST)
    public String selectedite(Model model, @RequestParam("reg_id") int reg_id) {

        //System.out.println("Selectedit ID:" + reg_id);
        Register editRegister = contactDAO.findregInfo(reg_id);
        System.out.println("controler " + editRegister.toString());
        System.out.println("get Health: " + editRegister.getHealth());
        //-----------------------------------------------------

        try {

            if ("ปกติ".equals(editRegister.getHealth())) {
                editRegister.setHealth("ปกติ");
            } else if (editRegister.getHealth().isEmpty() || editRegister.getHealth() == null) {
                System.out.println("null");
            } else if (!"ปกติ".equals(editRegister.getHealth())) {
                model.addAttribute("disease", editRegister.getHealth());
                editRegister.setHealth("nostrong");
            }
            
            String s = editRegister.getMental();
            String split[] = s.split(" ");
            if ("เคยเข้าบำบัด".equals(split[0])) {
                editRegister.setMental(split[0]);
                System.out.println("edit : " + editRegister.getMental());
                model.addAttribute("mentalwhere", split[1]);
                model.addAttribute("mentaltime", split[2]);
            } else if ("ไม่ได้ทานยามานาน".equals(split[0])) {
                editRegister.setMental(split[0]);
                System.out.println("edit : " + editRegister.getMental());
                model.addAttribute("mentalyear", split[1]);
            }
        } catch (Exception e) {
        }
        //-----------------------------------------------------
        System.out.println("Person id : " + editRegister.getPersonid());
        String decryptPersonIDString = registercontroller.decrypt(editRegister.getPersonid(), HomeController.getKey_hash_profile());
        String decryptAddressString = registercontroller.decrypt(editRegister.getAddress(), HomeController.getKey_hash_profile());
        String decryptDistrictString = registercontroller.decrypt(editRegister.getDistrict(), HomeController.getKey_hash_profile());
        editRegister.setPersonid(decryptPersonIDString);
        editRegister.setAddress(decryptAddressString);
        editRegister.setDistrict(decryptDistrictString);
        //----------------------------------------------------
        model.addAttribute("Register", editRegister);
        model.addAttribute("action", "regsedit");
        model.addAttribute("regId", editRegister.getReg_id());
        model.addAttribute("firstname", editRegister.getFirstname());
        List<Temple> temple = new ArrayList<Temple>();
        temple = templeDAO.getALL();
        model = homeController.getRole(model);
        model.addAttribute("temple", temple);
        model.addAttribute("temple_id", HomeController.getTemple_id());

        List<RoleItem> roleItems = new ArrayList<>();
        roleItems = roleDAO.getRoleITem(HomeController.getRole_id());
        Boolean statusadmin = RoleCheck.checkAdmin(roleItems);
        if (statusadmin == true) {
            model.addAttribute("roleadmin", "admin");
        } else if (statusadmin == false) {
            model.addAttribute("roleadmin", "user");
        }

        return "contact/editReg";
    }

    @RequestMapping(value = "/regsedit", method = RequestMethod.POST)
    public String edite(Model model, @Valid @ModelAttribute("Register") Register register, BindingResult result,
            @RequestParam(value = "disease", required = false) String disease,
            @RequestParam(value = "mentalwhere", required = false) String mentalwhere,
            @RequestParam(value = "mentaltime", required = false) String mentaltime,
            @RequestParam(value = "mentalyear", required = false) String mentalyear) {
        System.out.println("RESULT REG: " + result);
        String old_health = register.getHealth();
        String old_mental = register.getMental();
        if (result.hasErrors()) {
            List<Temple> temple = new ArrayList<Temple>();
            temple = templeDAO.getALL();
            model = homeController.getRole(model);
            model.addAttribute("temple", temple);
            model.addAttribute("temple_id", HomeController.getTemple_id());
            register.setHealth(old_health);
            register.setMental(old_mental);
            model.addAttribute("disease", disease);
            model.addAttribute("mentalwhere", mentalwhere);
            model.addAttribute("mentaltime", mentaltime);
            model.addAttribute("mentalyear", mentalyear);

            List<RoleItem> roleItems = new ArrayList<>();
            roleItems = roleDAO.getRoleITem(HomeController.getRole_id());
            Boolean statusadmin = RoleCheck.checkAdmin(roleItems);
            if (statusadmin == true) {
                model.addAttribute("roleadmin", "admin");
            } else if (statusadmin == false) {
                model.addAttribute("roleadmin", "user");
            }
            String errorField = result.getFieldError().getField();
            System.out.println("Missing some field : " + errorField);
            return "contact/editReg";
        }

        //---------set Register Model------------------------------------
        if ("nostrong".equals(register.getHealth())) {
            register.setHealth(disease);
        }
        if ("เคยเข้าบำบัด".equals(register.getMental())) {
            register.setMental(register.getMental() + " " + mentalwhere + " " + mentaltime);
        }
        if ("ไม่ได้ทานยามานาน".equals(register.getMental())) {
            register.setMental(register.getMental() + " " + mentalyear);
        }

        if (register.getGender_text().equals("ชาย") || register.getGender_text().equals("ต่างชาติชาย") || register.getGender_text().equals("ภิกษุ") || register.getGender_text().equals("สามเณร")) {
            register.setGender("ชาย");
        } else {
            register.setGender("หญิง");
        }
        //---------------encrypt and decrypt---------------------------
        String encryptPersonIDString = RegisterController.encrypt(register.getPersonid(), HomeController.getKey_hash_profile());
        String encryptAddressString = RegisterController.encrypt(register.getAddress(), HomeController.getKey_hash_profile());
        String encryptDistrictString = RegisterController.encrypt(register.getDistrict(), HomeController.getKey_hash_profile());
        // System.out.println("New Person id : " + encryptPersonIDString);
        // System.out.println("New Address : " + encryptAddressString);
        // System.out.println("New District : " + encryptDistrictString);
        register.setVillage("");
        register.setPersonid(encryptPersonIDString);
        register.setAddress(encryptAddressString);
        register.setDistrict(encryptDistrictString);
        //---------------Send to DAO---------------------------------
        //System.out.println("controler " + register.toString());
        contactDAO.updateReg(register);
        return "redirect:/regsall";
    }

    @RequestMapping(value = "/selectview", method = RequestMethod.POST)
    public String selectviewReg(Model model, @RequestParam("reg_id") int reg_id) {

        System.out.println("Selectedit ID:" + reg_id);
        Register editRegister = contactDAO.findregInfo(reg_id);
        System.out.println("controler " + editRegister.toString());
        //-----------------------------------------------------

        //-----------------------------------------------------
        System.out.println("Person id : " + editRegister.getPersonid());
        String decryptPersonIDString = registercontroller.decrypt(editRegister.getPersonid(), HomeController.getKey_hash_profile());
        String decryptAddressString = registercontroller.decrypt(editRegister.getAddress(), HomeController.getKey_hash_profile());
        String decryptDistrictString = registercontroller.decrypt(editRegister.getDistrict(), HomeController.getKey_hash_profile());
        editRegister.setPersonid(decryptPersonIDString);
        editRegister.setAddress(decryptAddressString);
        editRegister.setDistrict(decryptDistrictString);
        //----------------------------------------------------
        model.addAttribute("Register", editRegister);
        model.addAttribute("action", "regsedit");
        model.addAttribute("regId", editRegister.getReg_id());
        model.addAttribute("firstname", editRegister.getFirstname());
        model.addAttribute("temple_id", editRegister.getTemple_id());
        model.addAttribute("gender", editRegister.getGender());
        model.addAttribute("nametitle", editRegister.getNametitle());
        List<Temple> temple = new ArrayList<Temple>();
        temple = templeDAO.getALL();
        model = homeController.getRole(model);
        model.addAttribute("temple", temple);

        List<RoleItem> roleItems = new ArrayList<>();
        roleItems = roleDAO.getRoleITem(HomeController.getRole_id());
        Boolean statusadmin = RoleCheck.checkAdmin(roleItems);
        if (statusadmin == true) {
            model.addAttribute("roleadmin", "admin");
        } else if (statusadmin == false) {
            model.addAttribute("roleadmin", "user");
        }

        return "contact/viewReg";
    }

    @RequestMapping(value = "/selectmonkedit", method = RequestMethod.POST)
    public String selectmonkedit(@RequestParam("monk_id") int monk_id, Model model) {

        try {
            if (roleDAO.pageEdit("monk", HomeController.getRole_id()) == false) {
                return "redirect:/impervious";
            }

        } catch (Exception e) {
            return "redirect:/";
        }
        System.out.println("Select edit monk ID: " + monk_id);
        Monk editMonk = contactDAO.findmonkInfo(monk_id);
        List<RoleItem> roleItems = new ArrayList<>();
        roleItems = roleDAO.getRoleITem(HomeController.getRole_id());
        Boolean statusadmin = RoleCheck.checkAdmin(roleItems);
        if (statusadmin == true) {
            model.addAttribute("roleadmin", "admin");
        } else if (statusadmin == false) {
            model.addAttribute("roleadmin", "user");
        }
        System.out.println("controller " + editMonk.toString());
        model.addAttribute("Monk", editMonk);
        model.addAttribute("action", "monkedit");
        model.addAttribute("monkID", editMonk.getMonk_id());
        model.addAttribute("firstname", editMonk.getFirstname());
        List<Temple> temple = new ArrayList<Temple>();
        temple = templeDAO.getALL();
        model = homeController.getRole(model);
        model.addAttribute("temple", temple);
        model.addAttribute("Temple_id", editMonk.getTemple_id());
        return "contact/monkEdit";
    }

    @RequestMapping(value = "/viewMonk", method = RequestMethod.POST)
    public String monkView(@RequestParam("monk_id") int monk_id, Model model) {
        System.out.println("Select edit monk ID: " + monk_id);
        Monk editMonk = contactDAO.findmonkInfo(monk_id);
        System.out.println("controller " + editMonk.toString());
        model.addAttribute("Monk", editMonk);
        model.addAttribute("action", "monkedit");
        model.addAttribute("monkID", editMonk.getMonk_id());
        model.addAttribute("firstname", editMonk.getFirstname());
        List<Temple> temple = new ArrayList<Temple>();
        temple = templeDAO.getALL();
        model = homeController.getRole(model);
        model.addAttribute("temple", temple);
        model.addAttribute("Temple_id", editMonk.getTemple_id());

        List<RoleItem> roleItems = new ArrayList<>();
        roleItems = roleDAO.getRoleITem(HomeController.getRole_id());
        Boolean statusadmin = RoleCheck.checkAdmin(roleItems);
        if (statusadmin == true) {
            model.addAttribute("roleadmin", "admin");
        } else if (statusadmin == false) {
            model.addAttribute("roleadmin", "user");
        }

        return "contact/viewMonk";
    }

    @RequestMapping(value = "/monkedit", method = RequestMethod.POST)
    public String edite(Model model, @Valid @ModelAttribute("Monk") Monk monk, BindingResult result,
            @RequestParam(value = "pic_monk") MultipartFile pic_monk) {

        if (result.hasErrors()) {
            System.out.println("Monk:" + monk.toString());
            System.out.println("Error:" + result.toString());
            model = homeController.getRole(model);
            List<RoleItem> roleItems = new ArrayList<>();
            roleItems = roleDAO.getRoleITem(HomeController.getRole_id());
            Boolean statusadmin = RoleCheck.checkAdmin(roleItems);
            if (statusadmin == true) {
                model.addAttribute("roleadmin", "admin");
            } else if (statusadmin == false) {
                model.addAttribute("roleadmin", "user");
            }

            return "contact/monkEdit";
        }
        if (pic_monk.isEmpty()) {
            Monk update = contactDAO.findmonkInfo(monk.getMonk_id());
            monk.setPic_monk(update.getPic_monk());
        } else {
            try {
                monk.setPic_monk(pic_monk.getBytes());
            } catch (IOException ex) {
                Logger.getLogger(ContactController.class.getName()).log(Level.SEVERE, null, ex);
            }

        }

        System.out.println("controler " + monk.toString());
        contactDAO.updateMonk(monk);
        return "redirect:/monkall";
    }

    @RequestMapping(value = "/deleteregs", method = RequestMethod.POST)
    public String deleteReg(@RequestParam("reg_id") Integer reg_id,
            @RequestParam("reason") String reason) {
        System.out.println("reason : " + reason);
        //System.out.println("contact id =" + reg_id);
//        contactDAO.deleteContactReg(reg_id);
        contactDAO.deleteContactRegWithReason(reg_id, reason);
        return "redirect:/regsall";
    }

    @RequestMapping(value = "/reopenregs", method = RequestMethod.POST)
    public String reOpenReg(@RequestParam("reg_id") Integer reg_id) {
        contactDAO.reOpenReg(reg_id);
        return "redirect:/regsall";
    }

    @RequestMapping(value = "/deletemonk", method = RequestMethod.POST)
    public String deleteMonk(@RequestParam("monk_id") Integer monk_id) {
        System.out.println("monk id = " + monk_id);
        contactDAO.deleteMonk(monk_id);
        return "redirect:/monkall";
    }

    @RequestMapping(value = "/contactprofile", method = RequestMethod.GET)
    public String contactprofile(Model model) {
        Register editRegister = contactDAO.findregInfo(HomeController.getUser_id());
        String decryptPersonIDString = registercontroller.decrypt(editRegister.getPersonid(), HomeController.getKey_hash_profile());
        String decryptAddressString = registercontroller.decrypt(editRegister.getAddress(), HomeController.getKey_hash_profile());
        String decryptDistrictString = registercontroller.decrypt(editRegister.getDistrict(), HomeController.getKey_hash_profile());
        editRegister.setPersonid(decryptPersonIDString);
        editRegister.setAddress(decryptAddressString);
        editRegister.setDistrict(decryptDistrictString);
        //----------------------------------------------------
        model.addAttribute("Register", editRegister);
        model.addAttribute("action", "regsedit");
        model.addAttribute("regId", editRegister.getReg_id());
        model.addAttribute("firstname", editRegister.getFirstname());
        model.addAttribute("temple_id", editRegister.getTemple_id());
        model.addAttribute("gender", editRegister.getGender());
        model.addAttribute("nametitle", editRegister.getNametitle());
        List<Temple> temple = new ArrayList<Temple>();
        temple = templeDAO.getALL();
        model.addAttribute("temple", temple);

        model.addAttribute("roleadmin", "admin");

        return "contact/profile";
    }

    @RequestMapping(value = "/editcontactprofile", method = RequestMethod.GET)
    public String editcontactprofile(Model model) {
        Register editRegister = contactDAO.findregInfo(HomeController.getUser_id());
        System.out.println("Person id : " + editRegister.getPersonid());
        String decryptPersonIDString = registercontroller.decrypt(editRegister.getPersonid(), HomeController.getKey_hash_profile());
        String decryptAddressString = registercontroller.decrypt(editRegister.getAddress(), HomeController.getKey_hash_profile());
        String decryptDistrictString = registercontroller.decrypt(editRegister.getDistrict(), HomeController.getKey_hash_profile());
        editRegister.setPersonid(decryptPersonIDString);
        editRegister.setAddress(decryptAddressString);
        editRegister.setDistrict(decryptDistrictString);
        //----------------------------------------------------
        model.addAttribute("Register", editRegister);
        model.addAttribute("action", "regsedit");
        model.addAttribute("regId", editRegister.getReg_id());
        model.addAttribute("firstname", editRegister.getFirstname());
        List<Temple> temple = new ArrayList<Temple>();
        temple = templeDAO.getALL();
        model.addAttribute("temple", temple);
        model.addAttribute("temple_id", editRegister.getTemple_id());

        model.addAttribute("roleadmin", "admin");

        return "contact/editprofile";
    }

    @RequestMapping(value = "/savecontactprofile", method = RequestMethod.POST)
    public String editcontactprofile(Model model, @Valid @ModelAttribute("Register") Register register, BindingResult result) {

        model.addAttribute("roleadmin", "admin");

        if (result.hasErrors()) {
            String errorField = result.getFieldError().getField();
            System.out.println("Missing some field : " + errorField);
            if (!register.getPassword().equals(register.getRepassword())) {
                return "contact/editprofile";
            }
        }

        String encryptPersonIDString = RegisterController.encrypt(register.getPersonid(), HomeController.getKey_hash_profile());
        String encryptAddressString = RegisterController.encrypt(register.getAddress(), HomeController.getKey_hash_profile());
        String encryptDistrictString = RegisterController.encrypt(register.getDistrict(), HomeController.getKey_hash_profile());
        register.setPersonid(encryptPersonIDString);
        register.setAddress(encryptAddressString);
        register.setDistrict(encryptDistrictString);
        contactDAO.updateContactProfile(register);
        return "redirect:/contactprofile";
    }

    @RequestMapping(value = "/import", method = RequestMethod.GET)
    public String importData(Model model) {
        System.out.println("Test Import");
        try {
            if (roleDAO.pageCreate("temple_page", HomeController.getRole_id()) == false) {
                return "redirect:/impervious";
            }

        } catch (Exception e) {
            return "redirect:/";
        }
        List<Temple> temple = new ArrayList<Temple>();
        temple = templeDAO.getALL();
        model = homeController.getRole(model);
        model.addAttribute("temple", temple);

        List<RoleItem> roleItems = new ArrayList<>();
        roleItems = roleDAO.getRoleITem(HomeController.getRole_id());
        Boolean statusadmin = RoleCheck.checkAdmin(roleItems);
        if (statusadmin == true) {
            model.addAttribute("roleadmin", "admin");
        } else if (statusadmin == false) {
            model.addAttribute("roleadmin", "user");
        }

        return "setting/import";
    }

    @RequestMapping(value = "/import", method = RequestMethod.POST)
    public String saveImportData(Model model, @RequestParam(value = "importdata") MultipartFile importdata) {
        System.out.println("Test Import:" + importdata);
        try {
            File convFile = new File(importdata.getOriginalFilename());
            convFile.createNewFile();
            FileOutputStream fos = new FileOutputStream(convFile);
            fos.write(importdata.getBytes());
            fos.close();
            System.out.println(convFile);
            BufferedReader br = new BufferedReader(new FileReader(convFile));
            try {
                StringBuilder sb = new StringBuilder();
                String line = br.readLine();

                while (line != null) {
                    Register register = new Register();
                    sb.append(line);
                    sb.append(System.lineSeparator());
                    line = br.readLine();
                    if (line != null) {
                        try {
                            String[] lineArr = line.split(",");
                            if (lineArr[1].equals("น.ส.")) {
                                register.setNametitle("นางสาว");

                            } else if (lineArr[1].equals("ด.ช.")) {
                                register.setNametitle("เด็กชาย");

                            } else if (lineArr[1].equals("ด.ญ.")) {
                                register.setNametitle("เด็กหญิง");

                            } else {
                                register.setNametitle(lineArr[1]);

                            }
                            register.setFirstname(lineArr[2]);
                            register.setLastname(lineArr[3]);
                            if (lineArr[4].equals("1")) {
                                register.setGender("ชาย");
                                register.setGender_text("ชาย");
                            } else {
                                register.setGender("หญิง");
                                register.setGender_text("หญิง");

                            }
                            register.setBd_birth(lineArr[5]);
                            register.setRoyal("");
                            register.setNationality(lineArr[6]);
                            register.setPersonid(RegisterController.encrypt(lineArr[7], HomeController.getKey_hash_profile()));
                            register.setOccupation(lineArr[8]);
                            register.setAddress(RegisterController.encrypt(lineArr[9], HomeController.getKey_hash_profile()));
                            register.setVillage("");
                            register.setCity("");
                            register.setDistrict("");
                            register.setCity("");
                            register.setPassword("");
                            register.setRepassword("");
                            register.setNickname("");
                            register.setProvince(lineArr[12]);
                            register.setPostalcode(lineArr[14]);
                            register.setTemple_id(HomeController.getTemple_id());
                            String telephone = lineArr[15].replace("-", "");
                            register.setTelephone(telephone);
                            register.setEmail(lineArr[18]);
                            System.out.println("Register:" + register.toString());

                            int countPersonID = contactDAO.countPersonID(register.getPersonid());
                            if (countPersonID == 0) {
                                contactDAO.insertContactNow(register);
                            } else {
                                System.out.println("Duplicate");
                            }

                        } catch (Exception e) {
                            System.out.println("Error:" + e.getMessage());
                        }
                    }
                }
                String everything = sb.toString();

            } finally {
                br.close();
            }
        } catch (IOException e) {
            System.out.println("Error:" + e.getMessage());
        }

        return "redirect:/regsall";
    }

    @RequestMapping(value = "/monkimport", method = RequestMethod.GET)
    public String importMonkData(Model model) {
        System.out.println("Test Import Monk");
        try {
            if (roleDAO.pageCreate("temple_page", HomeController.getRole_id()) == false) {
                return "redirect:/impervious";
            }

        } catch (Exception e) {
            return "redirect:/";
        }
        List<Temple> temple = new ArrayList<Temple>();
        temple = templeDAO.getALL();
        model = homeController.getRole(model);
        model.addAttribute("temple", temple);

        List<RoleItem> roleItems = new ArrayList<>();
        roleItems = roleDAO.getRoleITem(HomeController.getRole_id());
        Boolean statusadmin = RoleCheck.checkAdmin(roleItems);
        if (statusadmin == true) {
            model.addAttribute("roleadmin", "admin");
        } else if (statusadmin == false) {
            model.addAttribute("roleadmin", "user");
        }

        return "setting/import_monk";
    }

    @RequestMapping(value = "/monkimport", method = RequestMethod.POST)
    public String saveImportMonkData(Model model, @RequestParam(value = "importmonkdata") MultipartFile importmonkdata) {
        System.out.println("Test Import Monk:" + importmonkdata);
        try {
            File convFile = new File(importmonkdata.getOriginalFilename());
            convFile.createNewFile();
            FileOutputStream fos = new FileOutputStream(convFile);
            fos.write(importmonkdata.getBytes());
            fos.close();
            System.out.println(convFile);
            BufferedReader br = new BufferedReader(new FileReader(convFile));
            try {
                StringBuilder sb = new StringBuilder();
                String line = br.readLine();

                while (line != null) {
                    Register register = new Register();
                    sb.append(line);
                    sb.append(System.lineSeparator());
                    line = br.readLine();
                    if (line != null) {
                        try {
                            String[] lineArr = line.split(",");
                            register.setNametitle(lineArr[1]);
                            register.setFirstname(lineArr[2]);
                            register.setLastname(lineArr[3]);
                            if (lineArr[4].equals("1")) {
                                register.setGender("ชาย");
                                register.setGender_text("ชาย");
                            } else {
                                register.setGender("หญิง");
                                register.setGender_text("หญิง");

                            }
                            register.setBd_birth(lineArr[5]);
                            register.setRoyal("");
                            register.setNationality(lineArr[6]);
                            register.setPersonid(RegisterController.encrypt(lineArr[7], HomeController.getKey_hash_profile()));
                            register.setOccupation(lineArr[8]);
                            register.setAddress(RegisterController.encrypt(lineArr[9], HomeController.getKey_hash_profile()));
                            register.setVillage("");
                            register.setCity("");
                            register.setDistrict("");
                            register.setCity("");
                            register.setPassword("");
                            register.setRepassword("");
                            register.setNickname("");
                            register.setProvince(lineArr[12]);
                            register.setPostalcode(lineArr[14]);
                            register.setTemple_id(HomeController.getTemple_id());
                            register.setTelephone(lineArr[15]);
                            register.setEmail(lineArr[18]);
                            int countPersonID = contactDAO.countPersonID(register.getPersonid());
                            if (countPersonID == 0) {
                                contactDAO.insertContactNow(register);
                            } else {

                            }

                        } catch (Exception e) {
                            System.out.println("Error:" + e.getMessage());
                        }
                    }
                }
                String everything = sb.toString();

            } finally {
                br.close();
            }
        } catch (IOException e) {
            System.out.println("Error:" + e.getMessage());
        }

        return "redirect:/monkall";
    }

    @RequestMapping(value = "/deactivatemonk", method = RequestMethod.POST)
    public String deactivateMonk(@RequestParam("monk_id") Integer monk_id) {

        //System.out.println("contact id =" + reg_id);
        contactDAO.deactivateMonk(monk_id);

        return "redirect:/monkall";
    }

    @RequestMapping(value = "/activatemonk", method = RequestMethod.POST)
    public String activateMonk(@RequestParam("monk_id") Integer monk_id) {

        //System.out.println("contact id =" + reg_id);
        contactDAO.activateMonk(monk_id);

        return "redirect:/monkall";
    }

    @RequestMapping(value = "/searchmonk", method = RequestMethod.POST)
    public String searchMonk(Model model,@RequestParam("search")String search){
        List<Monk> searchmonk = new ArrayList<>();
        searchmonk = contactDAO.searchMonk(search, HomeController.getTemple_id());
        try {
            for (int i = 0; i < searchmonk.size(); i++) {
                searchmonk.get(i).setMonkByte(searchmonk.get(i).getPic_monk());
                searchmonk.get(i).setMonkString(new String(Base64.encodeBase64(searchmonk.get(i).getMonkByte())));
            }

        } catch (Exception e) {
        }
        
        List<RoleItem> roleItems = new ArrayList<>();
        roleItems = roleDAO.getRoleITem(HomeController.getRole_id());
        Boolean statusadmin = RoleCheck.checkAdmin(roleItems);
        if (statusadmin == true) {
            model.addAttribute("roleadmin", "admin");
        } else if (statusadmin == false) {
            model.addAttribute("roleadmin", "user");
        }
        model = homeController.getRole(model);
        model.addAttribute("monklist", searchmonk);
        return "contact/search_monk";
    }
    
}
