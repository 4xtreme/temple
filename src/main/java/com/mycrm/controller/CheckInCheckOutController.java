package com.mycrm.controller;

import com.mycrm.common.RoleCheck;
import static com.mycrm.controller.BookingRoomController.dateThaiFormat;
import static com.mycrm.controller.BookingRoomController.getDateCompare;
import com.mycrm.dao.BookingDAO;
import com.mycrm.dao.ContactDAO;
import com.mycrm.dao.RoleDAO;
import com.mycrm.dao.RoomDAO;
import com.mycrm.dao.StaffDAO;
import com.mycrm.dao.TempleDAO;
import com.mycrm.domain.CheckinHistory;
import com.mycrm.domain.Contact;
import com.mycrm.domain.Monk;
import com.mycrm.domain.MonkDate;
import com.mycrm.domain.Register;
import com.mycrm.domain.RoleItem;
import com.mycrm.domain.Room;
import com.mycrm.domain.RoomName;
import com.mycrm.domain.Staff;
import com.mycrm.domain.Temple;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@Component
public class CheckInCheckOutController {

    @Autowired
    RoomDAO roomDAO;

    @Autowired
    TempleDAO templeDAO;

    @Autowired
    ContactDAO contactDAO;

    @Autowired
    BookingDAO bookingDAO;

    @Autowired
    RoleDAO roleDAO;

    @Autowired
    StaffDAO staffDAO;

    @Autowired
    HomeController homeController;

    @RequestMapping(value = "/checkOut", method = RequestMethod.GET)
    public String checkOutOnline(Model model) {
        try {
//            if (roleDAO.pageCreate("checkin", HomeController.getRole_id()) == false) {
//                return "redirect:/impervious";
//            }

            if (roleDAO.pageCreate("checkout", HomeController.getRole_id()) == false) {
                return "redirect:/impervious";
            }

        } catch (Exception e) {
            return "redirect:/";
        }

        List<RoleItem> roleItems = new ArrayList<>();
        roleItems = roleDAO.getRoleITem(HomeController.getRole_id());
        Boolean statusadmin = RoleCheck.checkAdmin(roleItems);
        if (statusadmin == true) {
            model.addAttribute("roleadmin", "admin");
        } else if (statusadmin == false) {
            model.addAttribute("roleadmin", "user");
        }

        List<Monk> monklist = new ArrayList<Monk>();
        monklist = contactDAO.getMonkByTemple(HomeController.getTemple_id());

        model = homeController.getRole(model);
        model.addAttribute("monklist", monklist);
        model.addAttribute("btCheckOut", "");
        model.addAttribute("btCheckOut", null);
        model.addAttribute("statusCheck", null);
        return "checkout/view";
    }

    @RequestMapping(value = "/checkInOnline", method = RequestMethod.GET)
    public String checkInOnline(Model model) {
        try {
            if (roleDAO.pageCreate("checkin", HomeController.getRole_id()) == false) {
                return "redirect:/impervious";
            }

        } catch (Exception e) {
            return "redirect:/";
        }

        List<Monk> monklist = new ArrayList<Monk>();
        monklist = contactDAO.getMonkByTemple(HomeController.getTemple_id());
        model.addAttribute("monklist", monklist);
        ArrayList<Integer> count_array = new ArrayList<Integer>();
        for (int i = 1; i <= monklist.size(); i++) {
            count_array.add(i);
        }
        List<MonkDate> monkDate = new ArrayList<MonkDate>();
        for (Monk add : monklist) {
            monkDate.add(new MonkDate(add.getFirstname(), bookingDAO.getCountMonkDate(add.getMonk_id(), getDateCompare(0)), bookingDAO.getCountMonkDate(add.getMonk_id(), getDateCompare(1)), bookingDAO.getCountMonkDate(add.getMonk_id(), getDateCompare(2)), bookingDAO.getCountMonkDate(add.getMonk_id(), getDateCompare(3)), bookingDAO.getCountMonkDate(add.getMonk_id(), getDateCompare(4)), bookingDAO.getCountMonkDate(add.getMonk_id(), getDateCompare(5)), bookingDAO.getCountMonkDate(add.getMonk_id(), getDateCompare(6)), bookingDAO.getCountMonkDate(add.getMonk_id(), getDateCompare(7)), bookingDAO.getCountMonkDate(add.getMonk_id(), getDateCompare(8)), bookingDAO.getCountMonkDate(add.getMonk_id(), getDateCompare(9))));
        }

        ArrayList<String> date_table = new ArrayList<String>();
        for (int i = 0; i <= 9; i++) {
            Date dt = new Date();
            Calendar c = Calendar.getInstance();
            c.setTime(dt);
            c.add(Calendar.DATE, i);
            dt = c.getTime();
            date_table.add(dateThaiFormat(dt));
        }

        List<RoleItem> roleItems = new ArrayList<>();
        roleItems = roleDAO.getRoleITem(HomeController.getRole_id());
        Boolean statusadmin = RoleCheck.checkAdmin(roleItems);
        if (statusadmin == true) {
            model.addAttribute("roleadmin", "admin");
        } else if (statusadmin == false) {
            model.addAttribute("roleadmin", "user");
        }

        model = homeController.getRole(model);

        model.addAttribute("monk_date", monkDate);
        model.addAttribute("count_array", count_array);
        model.addAttribute("date_table", date_table);
        model.addAttribute("btCheckOut", "");
        model.addAttribute("btCheckOut", null);
        model.addAttribute("statusCheck", null);
        return "checkout/view_checkin";
    }

    @RequestMapping(value = "/editcheckin", method = RequestMethod.GET)
    public String editCheckIn(Model model) {
        try {
//            if (roleDAO.pageCreate("checkin", HomeController.getRole_id()) == false) {
//                return "redirect:/impervious";
//            }

            if (roleDAO.pageCreate("checkout", HomeController.getRole_id()) == false) {
                return "redirect:/impervious";
            }

        } catch (Exception e) {
            return "redirect:/";
        }

        List<RoleItem> roleItems = new ArrayList<>();
        roleItems = roleDAO.getRoleITem(HomeController.getRole_id());
        Boolean statusadmin = RoleCheck.checkAdmin(roleItems);
        if (statusadmin == true) {
            model.addAttribute("roleadmin", "admin");
        } else if (statusadmin == false) {
            model.addAttribute("roleadmin", "user");
        }

        List<Monk> monklist = new ArrayList<Monk>();
        monklist = contactDAO.getMonkByTemple(HomeController.getTemple_id());
        model = homeController.getRole(model);
        model.addAttribute("monklist", monklist);
        model.addAttribute("btCheckOut", "");
        model.addAttribute("btCheckOut", null);
        model.addAttribute("statusCheck", null);
        return "checkin/editCheckIn";
    }

    @RequestMapping(value = "/checkInOnline", method = RequestMethod.POST)
    public String checkOutOnline(Model model, @RequestParam(value = "personal_cardid", required = false) String personal_cardid,
            @RequestParam(value = "monk_id", required = false) String monk_id) {
        String encryptPersonIDString = RegisterController.encrypt(personal_cardid, HomeController.getKey_hash_profile());
        Register contact = new Register();
        CheckinHistory history = null;
        Temple temple = null;
        Room room = null;
        RoomName roomname = null;
        Monk monk = new Monk();
        List<Monk> monklist = new ArrayList<Monk>();
        monklist = contactDAO.getMonkByTemple(HomeController.getTemple_id());
        model.addAttribute("monklist", monklist);
        ArrayList<Integer> count_array = new ArrayList<Integer>();
        for (int i = 1; i <= monklist.size(); i++) {
            count_array.add(i);
        }
        List<MonkDate> monkDate = new ArrayList<MonkDate>();
        for (Monk add : monklist) {
            monkDate.add(new MonkDate(add.getFirstname(), bookingDAO.getCountMonkDate(add.getMonk_id(), getDateCompare(0)), bookingDAO.getCountMonkDate(add.getMonk_id(), getDateCompare(1)), bookingDAO.getCountMonkDate(add.getMonk_id(), getDateCompare(2)), bookingDAO.getCountMonkDate(add.getMonk_id(), getDateCompare(3)), bookingDAO.getCountMonkDate(add.getMonk_id(), getDateCompare(4)), bookingDAO.getCountMonkDate(add.getMonk_id(), getDateCompare(5)), bookingDAO.getCountMonkDate(add.getMonk_id(), getDateCompare(6)), bookingDAO.getCountMonkDate(add.getMonk_id(), getDateCompare(7)), bookingDAO.getCountMonkDate(add.getMonk_id(), getDateCompare(8)), bookingDAO.getCountMonkDate(add.getMonk_id(), getDateCompare(9))));
        }

        List<RoleItem> roleItems = new ArrayList<>();
        roleItems = roleDAO.getRoleITem(HomeController.getRole_id());
        Boolean statusadmin = RoleCheck.checkAdmin(roleItems);
        if (statusadmin == true) {
            model.addAttribute("roleadmin", "admin");
        } else if (statusadmin == false) {
            model.addAttribute("roleadmin", "user");
        }

        ArrayList<String> date_table = new ArrayList<String>();
        for (int i = 0; i <= 9; i++) {
            Date dt = new Date();
            Calendar c = Calendar.getInstance();
            c.setTime(dt);
            c.add(Calendar.DATE, i);
            dt = c.getTime();
            date_table.add(dateThaiFormat(dt));
        }
        model = homeController.getRole(model);
        model.addAttribute("monk_date", monkDate);
        model.addAttribute("count_array", count_array);
        model.addAttribute("date_table", date_table);
        try {
            contact = contactDAO.getContactByPersonID(encryptPersonIDString);
            history = bookingDAO.getContactCheckIn(contact.getReg_id());
            room = roomDAO.find(history.getRoom_id());
            temple = templeDAO.findTemple(history.getTemple_id());
            roomname = roomDAO.getRoomNameByID(history.getRoom_name_id());

            monk = contactDAO.findmonkInfo(Integer.parseInt(monk_id));
        } catch (Exception e) {
            model.addAttribute("roomname", null);
            model.addAttribute("room", null);
            model.addAttribute("templename", null);
            model.addAttribute("createdate", null);
            model.addAttribute("id", null);
            model.addAttribute("btCheckin", null);
            model.addAttribute("startDate", null);
            model.addAttribute("endDate", null);
            model.addAttribute("statusCheck", "No Data");
            model.addAttribute("statusOut", null);

            return "checkout/view_checkin";

        }
        SimpleDateFormat format_date = new SimpleDateFormat("dd/MM/yyyy", new Locale("th", "TH"));
        String date_book = format_date.format(history.getCreate_date());
        String date_checkin = format_date.format(history.getCheck_in());
        String date_checkout = format_date.format(history.getCheck_out());

        model.addAttribute("monk_name", monk.getFirstname() + "  " + monk.getLastname());
        model.addAttribute("templename", temple.getName());
        model.addAttribute("room", room.getName());
        model.addAttribute("roomname", roomname.getName());
        model.addAttribute("createdate", date_book);
        model.addAttribute("id", history.getId());
        model.addAttribute("startDate", date_checkin);
        model.addAttribute("endDate", date_checkout);
        model.addAttribute("monk_id", monk_id);
        model.addAttribute("btCheckin", "OK");
        model.addAttribute("statusCheck", null);

        model.addAttribute("monk_name_out", null);
        model.addAttribute("room_out", null);
        model.addAttribute("roomname_out", null);
        model.addAttribute("createdate_out", null);
        model.addAttribute("startDate_out", null);
        model.addAttribute("endDate_out", null);
        model.addAttribute("btCheckOut", null);
        model.addAttribute("statusOut", null);

        return "checkout/view_checkin";
    }

    @RequestMapping(value = "/checkOut", method = RequestMethod.POST)
    public String checkOutOnline(Model model, @RequestParam(value = "personal_cardid", required = false) String personal_cardid) {

        String encryptPersonIDString = RegisterController.encrypt(personal_cardid, HomeController.getKey_hash_profile());
        Register contact = new Register();
        CheckinHistory history = null;
        Temple temple = null;
        Room room = null;
        RoomName roomname = null;
        Monk monk = new Monk();
        List<Monk> monklist = new ArrayList<Monk>();
        monklist = contactDAO.getMonkByTemple(HomeController.getTemple_id());
        model.addAttribute("monklist", monklist);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

        List<RoleItem> roleItems = new ArrayList<>();
        roleItems = roleDAO.getRoleITem(HomeController.getRole_id());
        Boolean statusadmin = RoleCheck.checkAdmin(roleItems);
        if (statusadmin == true) {
            model.addAttribute("roleadmin", "admin");
        } else if (statusadmin == false) {
            model.addAttribute("roleadmin", "user");
        }
        model = homeController.getRole(model);
        try {
            contact = contactDAO.getContactByPersonID(encryptPersonIDString);
            history = bookingDAO.getContactCheckOut(contact.getReg_id());
            room = roomDAO.find(history.getRoom_id());
            temple = templeDAO.findTemple(history.getTemple_id());
            roomname = roomDAO.getRoomNameByID(history.getRoom_name_id());

        } catch (Exception e) {
            model.addAttribute("roomname", null);
            model.addAttribute("room", null);
            model.addAttribute("templename", null);
            model.addAttribute("createdate", null);
            model.addAttribute("id", null);
            model.addAttribute("btCheckin", null);
            model.addAttribute("startDate", null);
            model.addAttribute("endDate", null);
            model.addAttribute("statusCheck", "No Data");
            model.addAttribute("room_out", null);
            model.addAttribute("roomname_out", null);
            model.addAttribute("startDate_out", null);
            model.addAttribute("endDate_out", null);
            model.addAttribute("btCheckOut", null);
            model.addAttribute("statusCheck", null);
            model.addAttribute("statusOut", 123);
            return "checkout/view";

        }
        model = homeController.getRole(model);
        // check in
        model.addAttribute("roomname", null);
        model.addAttribute("room", null);
        model.addAttribute("templename", null);
        model.addAttribute("createdate", null);
        model.addAttribute("id", null);
        model.addAttribute("btCheckOut", null);
        model.addAttribute("startDate", null);
        model.addAttribute("endDate", null);
        model.addAttribute("statusCheck", null);
        model.addAttribute("btCheckin", null);
        model.addAttribute("statusCheck", null);

        // check out
        model.addAttribute("room_out", room.getName());
        model.addAttribute("roomname_out", roomname.getName());
        model.addAttribute("id", history.getId());
        model.addAttribute("startDate_out", sdf.format(history.getCheck_in()));
        model.addAttribute("endDate_out", sdf.format(history.getCheck_out()));
        model.addAttribute("btCheckOut", "OK");
        model.addAttribute("statusCheck", null);
        model.addAttribute("statusOut", null);

        return "checkout/view";
    }

    @RequestMapping(value = "/editcheckin", method = RequestMethod.POST)
    public String editCheckInTime(Model model, @RequestParam(value = "personal_cardid", required = false) String personal_cardid) {

        String encryptPersonIDString = RegisterController.encrypt(personal_cardid, HomeController.getKey_hash_profile());
        Register contact = new Register();
        CheckinHistory history = null;
        Temple temple = null;
        Room room = null;
        RoomName roomname = null;
        Monk monk = new Monk();
        List<Monk> monklist = new ArrayList<Monk>();
        monklist = contactDAO.getMonkByTemple(HomeController.getTemple_id());
        model.addAttribute("monklist", monklist);
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

        List<RoleItem> roleItems = new ArrayList<>();
        roleItems = roleDAO.getRoleITem(HomeController.getRole_id());
        Boolean statusadmin = RoleCheck.checkAdmin(roleItems);
        if (statusadmin == true) {
            model.addAttribute("roleadmin", "admin");
        } else if (statusadmin == false) {
            model.addAttribute("roleadmin", "user");
        }
        model = homeController.getRole(model);

        try {
            contact = contactDAO.getContactByPersonID(encryptPersonIDString);
            history = bookingDAO.getContactCheckOut(contact.getReg_id());
            room = roomDAO.find(history.getRoom_id());
            temple = templeDAO.findTemple(history.getTemple_id());
            roomname = roomDAO.getRoomNameByID(history.getRoom_name_id());

        } catch (Exception e) {
            model.addAttribute("roomname", null);
            model.addAttribute("room", null);
            model.addAttribute("templename", null);
            model.addAttribute("createdate", null);
            model.addAttribute("id", null);
            model.addAttribute("btCheckin", null);
            model.addAttribute("startDate", null);
            model.addAttribute("endDate", null);
            model.addAttribute("statusCheck", "No Data");
            model.addAttribute("room_out", null);
            model.addAttribute("roomname_out", null);
            model.addAttribute("startDate_out", null);
            model.addAttribute("endDate_out", null);
            model.addAttribute("btCheckOut", null);
            model.addAttribute("statusCheck", null);
            model.addAttribute("statusOut", 123);
            return "checkin/editCheckInNoData";

        }
        // check in
        model.addAttribute("roomname", null);
        model.addAttribute("room", null);
        model.addAttribute("templename", null);
        model.addAttribute("createdate", null);
        model.addAttribute("id", null);
        model.addAttribute("btCheckOut", null);
        model.addAttribute("startDate", null);
        model.addAttribute("endDate", null);
        model.addAttribute("statusCheck", null);
        model.addAttribute("btCheckin", null);
        model.addAttribute("statusCheck", null);

        // check out
        model.addAttribute("room_out", room.getName());
        model.addAttribute("roomname_out", roomname.getName());
        model.addAttribute("id", history.getId());
        model.addAttribute("startDate_out", sdf.format(history.getCheck_in()));
        model.addAttribute("endDate_out", sdf.format(history.getCheck_out()));
        model.addAttribute("btCheckOut", "OK");
        model.addAttribute("statusCheck", null);
        model.addAttribute("statusOut", null);

        return "checkin/editCheckIn";
    }

    @RequestMapping(value = "/updatecheckintime", method = RequestMethod.POST)
    public String updateCheckInTime(Model model, @RequestParam(value = "id", required = false) int id,
            @RequestParam(value = "chkin", required = false) String chkin,
            @RequestParam(value = "chkout", required = false) String chkout) throws ParseException {
        System.out.println("id: " + id);
        System.out.println("chkin: " + chkin);
        System.out.println("chkout: " + chkout);

        SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy", new Locale("th", "TH"));
//        Date startDate = df.parse(chkin);
        Date endDate = df.parse(chkout);
        System.out.println("Convert END : " + chkout);

        CheckinHistory checkinHistory = new CheckinHistory();
//        Calendar c = Calendar.getInstance();
//        Date createDate = c.getTime();

        Date date = new Date();
        date.setHours(0);
        date.setMinutes(0);
        date.setSeconds(0);

        checkinHistory = bookingDAO.find(id);
        System.out.println("EndDate:" + endDate);
        System.out.println("Date:" + date);
        System.out.println(date.compareTo(endDate) == 0);
        System.out.println(endDate.before(date));

        if (date.compareTo(endDate) == 0 || endDate.before(date)) {
            checkinHistory.setStatus("เช็คเอ๊า");
        } else {
            checkinHistory.setStatus("เช็คอิน");
        }

        checkinHistory.setCheck_out(endDate);
        bookingDAO.updateCheckOut(checkinHistory);
        Register contact = contactDAO.findregInfo(checkinHistory.getContact_id());
        contactDAO.updateCountCome(contact.getCount_come() + 1, contact.getReg_id());
        return "redirect:/editcheckin";
    }

    @RequestMapping(value = "/updatecheckin", method = RequestMethod.POST)
    public String updateCheckinOnline(Model model, @RequestParam(value = "id", required = false) int id,
            @RequestParam(value = "monk_id", required = false) int monk_id) {
        System.out.println("updatecheckin --------------------------------------");
        System.out.println(id);
        System.out.println(monk_id);
        CheckinHistory checkinHistory = new CheckinHistory();
        Calendar c = Calendar.getInstance();
        Date createDate = c.getTime();
        checkinHistory.setId(id);
        checkinHistory.setStatus("เช็คอิน");
        checkinHistory.setCheck_in(createDate);
        checkinHistory.setMonk_id(monk_id);

        // update check in
        bookingDAO.updateCheckIn(checkinHistory);

        // serach check history
        CheckinHistory checkFirst = new CheckinHistory();
        checkFirst = bookingDAO.getFirstCome(id);
        if (bookingDAO.countCheckin(checkFirst.getContact_id(), checkFirst.getTemple_id())) {
            contactDAO.updateFirstCome(checkinHistory.getContact_id());
        }

        return "redirect:/checkInOnline";
    }

    @RequestMapping(value = "/updatecheckout", method = RequestMethod.POST)
    public String updateCheckoutOnline(Model model, @RequestParam(value = "id", required = false) int id) {
        CheckinHistory checkinHistory = new CheckinHistory();
        Calendar c = Calendar.getInstance();
        Date createDate = c.getTime();
        checkinHistory = bookingDAO.find(id);
        checkinHistory.setStatus("เช็คเอ๊า");
        checkinHistory.setCheck_out(createDate);
        bookingDAO.updateCheckOut(checkinHistory);
        System.out.println("Contact:" + checkinHistory.toString());
        Register contact = contactDAO.findregInfo(checkinHistory.getContact_id());
        contactDAO.updateCountCome(contact.getCount_come() + 1, contact.getReg_id());
        return "redirect:/checkOut";
    }

    public static String dateThaiFormat(Date dateformat) {
        DateFormat df = new SimpleDateFormat("dd-MM-yyyy", new Locale("th", "TH"));
        String datetime = df.format(dateformat);
        return datetime;
    }

    public static String dateFormatSQL(Date dateformat) {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        String datetime = df.format(dateformat);
        return datetime;
    }

    public static String getDateCompare(int number) {
        Date d_sql = new Date();
        Calendar c = Calendar.getInstance();
        c.setTime(d_sql);
        c.add(Calendar.DATE, number);
        d_sql = c.getTime();
        return dateFormatSQL(d_sql);
    }

}
