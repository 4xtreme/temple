/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycrm.controller;

/**
 *
 * @author Lenovo
 */
import com.mycrm.common.Dateformat;
import com.mycrm.common.RoleCheck;
import com.mycrm.dao.PositionDAO;
import com.mycrm.dao.RoleDAO;
import com.mycrm.dao.StaffDAO;
import com.mycrm.dao.TempleDAO;
import com.mycrm.dao.TemplepicDAO;
import com.mycrm.dao.UserDAO;
import com.mycrm.domain.RoleItem;
import com.mycrm.domain.Staff;
import com.mycrm.domain.Temple;
import com.mycrm.domain.User;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class TempleController {

    int PER_PAGE = 10;

    @Autowired
    UserDAO userDAO;

    @Autowired
    TempleDAO templeDAO;

    @Autowired
    PositionDAO positionDAO;

    @Autowired
    TemplepicDAO templepicDAO;

    @Autowired
    RoleDAO roleDAO;

    @Autowired
    StaffDAO staffDAO;

    @Autowired
    HomeController homeController;

    @RequestMapping(value = "/templelist", method = RequestMethod.GET)
    public String templeList(Model model, @RequestParam(value = "page", required = false) String page) throws ParseException {
        String page_Create = "OK";
        String page_Delete = "OK";
        String page_Edit = "OK";
        try {
            if (roleDAO.pageView("temple_page", HomeController.getRole_id()) == false) {
                return "redirect:/impervious";
            }

        } catch (Exception e) {
            return "redirect:/";
        }

        if (roleDAO.pageCreate("temple_page", HomeController.getRole_id()) == false) {
            page_Create = null;
        }

        if (roleDAO.pageEdit("temple_page", HomeController.getRole_id()) == false) {
            page_Edit = null;
        }
        if (roleDAO.pageDelete("temple_page", HomeController.getRole_id()) == false) {
            page_Delete = null;
        }
        if (page == null || Integer.parseInt(page) < 0) {
            page = "0";
        }
        int countRow = 0;
        int page_count = 0;
        countRow = templeDAO.countRows();
        Double pagecountDB = Math.ceil((double) countRow / (double) PER_PAGE);
        page_count = pagecountDB.intValue();
        List<Temple> temple = new ArrayList<Temple>();
        temple = templeDAO.getTempleList(page, PER_PAGE);
        if (Integer.parseInt(page) > 0) {
            model.addAttribute("page_priv", true);

        }
        if (Integer.parseInt(page) < (page_count - 1)) {
            model.addAttribute("page_next", true);
        }
        if (page_count >= 10) {
            page_count = 10;
        }
        model = homeController.getRole(model);
        model.addAttribute("temples", temple);
        model.addAttribute("page_count", page_count);
        model.addAttribute("page", page);
        model.addAttribute("page_Create", page_Create);
        model.addAttribute("page_Delete", page_Delete);
        model.addAttribute("page_Edit", page_Edit);

        List<RoleItem> roleItems = new ArrayList<>();
        roleItems = roleDAO.getRoleITem(HomeController.getRole_id());
        Boolean statusadmin = RoleCheck.checkAdmin(roleItems);
        if (statusadmin == true) {
            model.addAttribute("roleadmin", "admin");
        } else if (statusadmin == false) {
            model.addAttribute("roleadmin", "user");
        }
        
        List<Temple> temple_province = new ArrayList<>();
        temple_province = templeDAO.getProvince();
        model.addAttribute("province",temple_province);
        
        

        return "temple/templelist";
    }

    @RequestMapping(value = "/addtemple", method = RequestMethod.GET)
    public String contactAll(Model model) {
        try {
            if (roleDAO.pageCreate("temple_page", HomeController.getRole_id()) == false) {
                return "redirect:/impervious";
            }

        } catch (Exception e) {
            return "redirect:/";
        }
        String d_date = "";
        Date date = new Date();
        if (LocaleContextHolder.getLocale().toString().equals("th")) {
            d_date = Dateformat.dateThaiFormatHeader(date);
        } else {
            d_date = Dateformat.dateFormatHeader(date);
        }

        model = homeController.getRole(model);
//        List<Position> position =  positionDAO.getPositionList();
        model.addAttribute("date", d_date);
        model.addAttribute("temple", new Temple());
//        model.addAttribute("positions",position);

        List<RoleItem> roleItems = new ArrayList<>();
        roleItems = roleDAO.getRoleITem(HomeController.getRole_id());
        Boolean statusadmin = RoleCheck.checkAdmin(roleItems);
        if (statusadmin == true) {
            model.addAttribute("roleadmin", "admin");
        } else if (statusadmin == false) {
            model.addAttribute("roleadmin", "user");
        }

        return "temple/addtemple";
    }

    @RequestMapping(value = "/addtemple", method = RequestMethod.POST)
    public String addTemple(Model model, @Valid @ModelAttribute("temple") Temple temple,
            //           @RequestParam(value = "position") String position,
            //           @RequestParam(value = "supervise") String supervise,
            //           @RequestParam(value = "sratus_temple") String sratus_temple,
            //           @RequestParam(value = "note") String note,System.out.print(model);

            BindingResult result) {

        List<RoleItem> roleItems = new ArrayList<>();
        roleItems = roleDAO.getRoleITem(HomeController.getRole_id());
        Boolean statusadmin = RoleCheck.checkAdmin(roleItems);
        if (statusadmin == true) {
            model.addAttribute("roleadmin", "admin");
        } else if (statusadmin == false) {
            model.addAttribute("roleadmin", "user");
        }

        model = homeController.getRole(model);
//        List<Position> position =  positionDAO.getPositionList();

        if (result.hasErrors()) {
            return "temple/addtemple";
        }
        //  temple.setIs_delete(false);
//        temple.setPosition(position);
//        temple.setSupervise(supervise);
//        temple.setSratus_temple(sratus_temple);
//        temple.setNote(note);
        templeDAO.insert(temple);
        return "redirect:/templelist";
    }

    @RequestMapping(value = "/edittemple", method = RequestMethod.POST)
    public String edit(Model model, @RequestParam(value = "id") int id) {
        Temple temple = templeDAO.findTemple(id);
        //    String d_date = "";
        Date date = new Date();
        if (LocaleContextHolder.getLocale().toString().equals("th")) {
            //        d_date = Dateformat.dateThaiFormatHeader(date);
        } else {
            //          d_date = Dateformat.dateFormatHeader(date);
        }

        //   model.addAttribute("user", user);
        //      model.addAttribute("date", d_date);
        model = homeController.getRole(model);
        model.addAttribute("temple", temple);

        List<RoleItem> roleItems = new ArrayList<>();
        roleItems = roleDAO.getRoleITem(HomeController.getRole_id());
        Boolean statusadmin = RoleCheck.checkAdmin(roleItems);
        if (statusadmin == true) {
            model.addAttribute("roleadmin", "admin");
        } else if (statusadmin == false) {
            model.addAttribute("roleadmin", "user");
        }

        return "temple/edittemple";
    }

    @RequestMapping(value = "/edittemples", method = RequestMethod.POST)
    public String edittemple(Model model, @Valid @ModelAttribute("temple") Temple temple, BindingResult result) {
        System.out.println("Temple:" + temple.toString());

        List<RoleItem> roleItems = new ArrayList<>();
        roleItems = roleDAO.getRoleITem(HomeController.getRole_id());
        Boolean statusadmin = RoleCheck.checkAdmin(roleItems);
        if (statusadmin == true) {
            model.addAttribute("roleadmin", "admin");
        } else if (statusadmin == false) {
            model.addAttribute("roleadmin", "user");
        }
        model = homeController.getRole(model);
        if (result.hasErrors()) {
            return "temple/edittemple";
        }
        templeDAO.update(temple);
        return "redirect:/templelist";
    }

    @RequestMapping(value = "/deletetemple", method = RequestMethod.POST)
    public String deletetemple(Model model, @RequestParam(value = "id") int id) {

        templeDAO.delete(id);
        return "redirect:/templelist";
    }

    @RequestMapping(value = "/gettemple", method = RequestMethod.GET)
    public ResponseEntity<List<Temple>> getTempleForMarker() {
        List<Temple> temples = new ArrayList<Temple>();
        temples = templeDAO.getTempleForMarker();
        return new ResponseEntity<List<Temple>>(temples, HttpStatus.OK);
    }

    @RequestMapping(value = "/searchtemple", method = RequestMethod.POST)
    public String searchTemple(Model model,@RequestParam("search")String search,
            @RequestParam(value = "provinces", required = false)String province){
        System.out.println("provincess : "+province);
        String page_Create = "OK";
        String page_Delete = "OK";
        String page_Edit = "OK";
        try {
            if (roleDAO.pageView("temple_page", HomeController.getRole_id()) == false) {
                return "redirect:/impervious";
            }

        } catch (Exception e) {
            return "redirect:/";
        }

        if (roleDAO.pageCreate("temple_page", HomeController.getRole_id()) == false) {
            page_Create = null;
        }

        if (roleDAO.pageEdit("temple_page", HomeController.getRole_id()) == false) {
            page_Edit = null;
        }
        if (roleDAO.pageDelete("temple_page", HomeController.getRole_id()) == false) {
            page_Delete = null;
        }
        
        List<RoleItem> roleItems = new ArrayList<>();
        roleItems = roleDAO.getRoleITem(HomeController.getRole_id());
        Boolean statusadmin = RoleCheck.checkAdmin(roleItems);
        if (statusadmin == true) {
            model.addAttribute("roleadmin", "admin");
        } else if (statusadmin == false) {
            model.addAttribute("roleadmin", "user");
        }
        model = homeController.getRole(model);
        model.addAttribute("page_Create", page_Create);
        model.addAttribute("page_Delete", page_Delete);
        model.addAttribute("page_Edit", page_Edit);

        
        List<Temple> temples = new ArrayList<>();
        if(!"true".equals(province)){
            temples = templeDAO.searchTemple(search);
        }else{
            temples = templeDAO.searchProvinceTemple(search);
        }
        model.addAttribute("temples",temples);
        return "temple/searchtemple";
    }
}
