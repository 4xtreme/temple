/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycrm.controller;

import com.ibm.icu.util.Calendar;
import com.mycrm.common.RoleCheck;
import com.mycrm.dao.CheckinChartsDAO;
import com.mycrm.dao.ContactDAO;
import com.mycrm.dao.DailychartsDAO;
import com.mycrm.dao.EventsDAO;
import com.mycrm.dao.RoleDAO;
import com.mycrm.dao.StaffDAO;
import com.mycrm.dao.VipassanaDAO;
import com.mycrm.domain.ContactReport;
import com.mycrm.domain.EventInfo;
import com.mycrm.domain.Events;
import com.mycrm.domain.Monk;
import com.mycrm.domain.MonkReport;
import com.mycrm.domain.ReportManager;
import com.mycrm.domain.RoleItem;
import com.mycrm.domain.Vipassana;
import com.mycrm.domain.VipassanaData;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.io.output.ByteArrayOutputStream;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.DataFormat;
import org.apache.poi.ss.usermodel.FontCharset;
import org.apache.poi.ss.usermodel.FontUnderline;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author pop
 */
@Controller
@Component
public class ReportController {

    @Autowired
    DailychartsDAO DailyChartsDAO;

    @Autowired
    RoleDAO roleDAO;

    @Autowired
    ContactDAO contactDAO;

    @Autowired
    CheckinChartsDAO checkinChartsDAO;

    @Autowired
    StaffDAO staffDAO;

    @Autowired
    HomeController homeController;

    @Autowired
    VipassanaDAO vipassanaDAO;

    @Autowired
    EventsDAO eventsDAO;

    private static SecretKeySpec secretKey;

    private static byte[] key;

    ReportManager getReport = new ReportManager();

    public static void setKey(String myKey) {
        MessageDigest sha = null;
        try {
            key = myKey.getBytes("UTF-8");
            sha = MessageDigest.getInstance("SHA-1");
            key = sha.digest(key);
            key = Arrays.copyOf(key, 16);
            secretKey = new SecretKeySpec(key, "AES");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    @RequestMapping(value = "/monkreport", method = RequestMethod.GET)
    public String monkReport(Model model) {
        try {
            if (roleDAO.pageView("report", HomeController.getRole_id()) == false) {
                return "redirect:/impervious";
            }

        } catch (Exception e) {
            return "redirect:/";
        }
        
        List<Monk> monk = new ArrayList<Monk>();
        System.out.println("Temple_id:" + HomeController.getTemple_id());
        monk = contactDAO.getAllMonk(HomeController.getTemple_id());
        List<Vipassana> vipassanas = new ArrayList<>();
        vipassanas = vipassanaDAO.findAll(HomeController.getTemple_id(), "0", 1000);
        System.out.println("Monk:" + monk.toString());
        Date date = new Date();
        date.setHours(1);
        date.setMinutes(0);
        SimpleDateFormat sf = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH);
        SimpleDateFormat sdf = new SimpleDateFormat("dd MMMM G yyyy", new Locale("th", "TH"));
        List<VipassanaData> vipassanaDatas = new ArrayList();
        model = homeController.getRole(model);
        model.addAttribute("dateval", sf.format(date));
        model.addAttribute("enddateval", sf.format(date));
        model.addAttribute("vipassanas", vipassanas);
        model.addAttribute("count", vipassanas.size());
        model.addAttribute("monkreport", null);
        model.addAttribute("templeid", HomeController.getTemple_id());
        model.addAttribute("contact_type", "ชาย");
        model.addAttribute("report_date", sdf.format(date));
        model.addAttribute("vipassanaDatas", vipassanaDatas);

        List<RoleItem> roleItems = new ArrayList<>();
        roleItems = roleDAO.getRoleITem(HomeController.getRole_id());
        Boolean statusadmin = RoleCheck.checkAdmin(roleItems);
        if (statusadmin == true) {
            model.addAttribute("roleadmin", "admin");
        } else if (statusadmin == false) {
            model.addAttribute("roleadmin", "user");
        }

        return "report/monk_report";
    }

    @RequestMapping(value = "/countreport", method = RequestMethod.GET)
    public String getCountReport(Model model) {
        
        try {
            if (roleDAO.pageView("report", HomeController.getRole_id()) == false) {
                return "redirect:/impervious";
            }

        } catch (Exception e) {
            return "redirect:/";
        }
        
        model = homeController.getRole(model);
        List<RoleItem> roleItems = new ArrayList<>();
        roleItems = roleDAO.getRoleITem(HomeController.getRole_id());
        Boolean statusadmin = RoleCheck.checkAdmin(roleItems);
        if (statusadmin == true) {
            model.addAttribute("roleadmin", "admin");
        } else if (statusadmin == false) {
            model.addAttribute("roleadmin", "user");
        }
        int temple_id = HomeController.getTemple_id();
        
//        In Temple
        int allCountInTemple = 0;
//        men
        int monkCount = contactDAO.countMonk(temple_id, "พระ");
        allCountInTemple = allCountInTemple + monkCount;
        int noviceCount = contactDAO.countMonk(temple_id, "สามเณร");
        allCountInTemple = allCountInTemple + noviceCount;
        int saththi_count = contactDAO.countMonk(temple_id, "พระสัทธิวิหาริก");
        allCountInTemple = allCountInTemple + saththi_count;
        int guest_monk_count = contactDAO.countMonk(temple_id, "พระอาคันตุกะ");
        allCountInTemple = allCountInTemple + guest_monk_count;
        int samsaththi_count = contactDAO.countMonk(temple_id, "สามเราสิทธิวิหาริก");
        allCountInTemple = allCountInTemple + samsaththi_count;
//        women
        int bddhist_nun_count = contactDAO.countMonk(temple_id, "ภิกษุณี");
        allCountInTemple = allCountInTemple + bddhist_nun_count;
        int nun_count = contactDAO.countMonk(temple_id, "แม่ชี");
        allCountInTemple = allCountInTemple + nun_count;
        int guest_nun_count = contactDAO.countMonk(temple_id, "แม่ชีอาคันตุกะ");
        allCountInTemple = allCountInTemple + guest_nun_count;
//        foreigner
        int foreign_monk_count = contactDAO.countMonk(temple_id, "ภิกษุต่างประเทศ");
        allCountInTemple = allCountInTemple + foreign_monk_count;
        int foreign_nun_count = contactDAO.countMonk(temple_id, "แม่ชีต่างประเทศ");
        allCountInTemple = allCountInTemple + foreign_nun_count;

//        temple staff
        int regular_yogi_count = contactDAO.countMonk(temple_id, "โยคีประจำ");
        allCountInTemple = allCountInTemple + regular_yogi_count;
        int volunteer_count = contactDAO.countMonk(temple_id, "จิตอาสา");
        allCountInTemple = allCountInTemple + volunteer_count;

//        Visitor
        int allCountVisitor = 0;
//        men
        int monkVisitor = contactDAO.countContact(temple_id, "ภิกษุ");
        allCountVisitor = allCountVisitor + monkVisitor;
        int noviceMonkVisitor = contactDAO.countContact(temple_id, "สามเณร");
        allCountVisitor = allCountVisitor + noviceMonkVisitor;
        int yogiMaleVisitor = contactDAO.countContact(temple_id, "ชาย");
        allCountVisitor = allCountVisitor + yogiMaleVisitor;
//        women
        int bddhistNunkVisitor = contactDAO.countContact(temple_id, "ภิกษุณี");
        allCountVisitor = allCountVisitor + bddhistNunkVisitor;
        int nunVisitor = contactDAO.countContact(temple_id, "แม่ชี");
        allCountVisitor = allCountVisitor + nunVisitor;
        int yogiFemaleVisitor = contactDAO.countContact(temple_id, "หญิง");
        allCountVisitor = allCountVisitor + yogiFemaleVisitor;
//        foreigner
        int foreign_male_yogi_count = contactDAO.countContact(temple_id, "ต่างชาติชาย");
        allCountVisitor = allCountVisitor + foreign_male_yogi_count;
        int foreign_female_yogi_count = contactDAO.countContact(temple_id, "ต่างชาติหญิง");
        allCountVisitor = allCountVisitor + foreign_female_yogi_count;
        
        model.addAttribute("allCountInTemple",allCountInTemple);
        model.addAttribute("monkCount",monkCount);
        model.addAttribute("noviceCount",noviceCount);
        model.addAttribute("saththi_count",saththi_count);
        model.addAttribute("guest_monk_count",guest_monk_count);
        model.addAttribute("samsaththi_count",samsaththi_count);
        model.addAttribute("bddhist_nun_count",bddhist_nun_count);
        model.addAttribute("nun_count",nun_count);
        model.addAttribute("guest_nun_count",guest_nun_count);
        model.addAttribute("foreign_monk_count",foreign_monk_count);
        model.addAttribute("foreign_nun_count",foreign_nun_count);
        model.addAttribute("regular_yogi_count",regular_yogi_count);
        model.addAttribute("volunteer_count",volunteer_count);
        model.addAttribute("allCountVisitor",allCountVisitor);
        model.addAttribute("monkVisitor",monkVisitor);
        model.addAttribute("noviceMonkVisitor",noviceMonkVisitor);
        model.addAttribute("yogiMaleVisitor",yogiMaleVisitor);
        model.addAttribute("bddhistNunkVisitor",bddhistNunkVisitor);
        model.addAttribute("nunVisitor",nunVisitor);
        model.addAttribute("yogiFemaleVisitor",yogiFemaleVisitor);
        model.addAttribute("foreign_male_yogi_count",foreign_male_yogi_count);
        model.addAttribute("foreign_female_yogi_count",foreign_female_yogi_count);
        
        return "report/count_report";
    }

    @RequestMapping(value = "/monkreport", method = RequestMethod.POST)
    public String getMonkReport(Model model, @RequestParam(value = "exam_date") String exam_date,
            @RequestParam(value = "contact_type") String contact_type) throws ParseException {
        List<Vipassana> vipassanas = new ArrayList<>();
        vipassanas = vipassanaDAO.findAll(HomeController.getTemple_id(), "0", 1000);
        System.out.println("Monk:" + vipassanas.toString());
        List<MonkReport> report = new ArrayList<MonkReport>();
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        Date startDate = sdf.parse(exam_date);
        startDate.setYear(startDate.getYear() - 543);
        System.out.println(sdf.format(startDate));
//        if (vipassanas_id != 0) {
//            report = checkinChartsDAO.getMonkReport(vipassanas_id, startDate);
//        } else {
//        }

        report = checkinChartsDAO.getAllMonkReport(HomeController.getTemple_id(), startDate, contact_type);
        SimpleDateFormat checkoutDate = new SimpleDateFormat("dd MMM yyyy", new Locale("th", "TH"));

        for (int i = 0; i < report.size(); i++) {
            report.get(i).setDateFormat(checkoutDate.format(report.get(i).getCheckOut()));
        }

        List<VipassanaData> vipassanaDatas = new ArrayList();
        vipassanaDatas = checkinChartsDAO.countAllVipassanaReport(HomeController.getTemple_id(), startDate, contact_type);
        int examCount = 0;
        List<VipassanaData> datas = new ArrayList<>();
        for (Vipassana vipassana : vipassanas) {
            datas.add(new VipassanaData(0, vipassana.getFirstname(), (short) 0, vipassana.getId(), 0));
        }
        for (int i = 0; i < datas.size(); i++) {
            for (VipassanaData vipassanaData : vipassanaDatas) {
                if (datas.get(i).getVipassanaId() == vipassanaData.getVipassanaId()) {
                    datas.get(i).setCount(vipassanaData.getCount());
                    examCount = examCount + datas.get(i).getCount();
                }
            }
        }
        SimpleDateFormat sd = new SimpleDateFormat("dd MMMM G yyyy", new Locale("th", "TH"));

        model = homeController.getRole(model);
        model.addAttribute("dateval", sdf.format(startDate));
        model.addAttribute("vipassanas", vipassanas);
        model.addAttribute("examCount", examCount);
        model.addAttribute("count", vipassanas.size());
        model.addAttribute("monkreport", report);
        model.addAttribute("templeid", HomeController.getTemple_id());
        model.addAttribute("contact_type", contact_type);
        model.addAttribute("report_date", sd.format(startDate));
        model.addAttribute("vipassanaDatas", datas);

        List<RoleItem> roleItems = new ArrayList<>();
        roleItems = roleDAO.getRoleITem(HomeController.getRole_id());
        Boolean statusadmin = RoleCheck.checkAdmin(roleItems);
        if (statusadmin == true) {
            model.addAttribute("roleadmin", "admin");
        } else if (statusadmin == false) {
            model.addAttribute("roleadmin", "user");
        }

        return "report/monk_report";
    }

    @RequestMapping(value = "/selectmonkreport", method = RequestMethod.POST)
    public ResponseEntity<String> selectedit(@RequestParam("temple_id") String select,
            @RequestParam(value = "exam_date") String exam_date,
            @RequestParam(value = "action") String action,
            Model model) throws ParseException, IOException {
        List<Vipassana> vipassanas = new ArrayList<>();
        vipassanas = vipassanaDAO.findAll(HomeController.getTemple_id(), "0", 1000);
        System.out.println("exam:" + exam_date);
        List<MonkReport> report = new ArrayList<MonkReport>();
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH);
        Date startDate = sdf.parse(exam_date);
        System.out.println(startDate);
//        if (vipassanas_id != 0) {
//            report = checkinChartsDAO.getMonkReport(vipassanas_id, startDate);
//        } else {
//        }

        report = checkinChartsDAO.getAllMonkReport(HomeController.getTemple_id(), startDate, action);
        SimpleDateFormat checkoutDate = new SimpleDateFormat("dd MMM yyyy", new Locale("th", "TH"));

        for (int i = 0; i < report.size(); i++) {
            report.get(i).setDateFormat(checkoutDate.format(report.get(i).getCheckOut()));
        }

        List<VipassanaData> vipassanaDatas = new ArrayList();
        vipassanaDatas = checkinChartsDAO.countAllVipassanaReport(HomeController.getTemple_id(), startDate, action);
        int examCount = 0;
        int index = 3;
        List<VipassanaData> datas = new ArrayList<>();
        for (Vipassana vipassana : vipassanas) {
            datas.add(new VipassanaData(index, vipassana.getFirstname(), (short) 0, vipassana.getId(), 0));
            index++;
        }
        for (int i = 0; i < datas.size(); i++) {
            for (VipassanaData vipassanaData : vipassanaDatas) {
                if (datas.get(i).getVipassanaId() == vipassanaData.getVipassanaId()) {
                    datas.get(i).setCount(vipassanaData.getCount());
                    examCount = examCount + datas.get(i).getCount();
                }
            }

        }
        SimpleDateFormat sd = new SimpleDateFormat("dd MMMM G yyyy", new Locale("th", "TH"));
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMMM G YYYY", new Locale("th", "TH"));
//        Date examDate = dateFormat.parse(exam_date);
        Date date = new Date();
        date = sdf.parse(exam_date);

        Workbook workbook = new XSSFWorkbook();
        Sheet sheet = workbook.createSheet();
        sheet.setColumnWidth(0, 1300);

        XSSFFont font = ((XSSFWorkbook) workbook).createFont();
        font.setCharSet(FontCharset.THAI);
        font.setBold(false);

        XSSFFont font2 = ((XSSFWorkbook) workbook).createFont();
        font2.setCharSet(FontCharset.THAI);
        font2.setBold(true);

        XSSFFont font3 = ((XSSFWorkbook) workbook).createFont();
        font3.setCharSet(FontCharset.THAI);
        font3.setBold(false);

        int rowCount = 0;
        String formatStr = "#,##0.00";
        CellStyle style = workbook.createCellStyle();
        DataFormat format = workbook.createDataFormat();
        style.setDataFormat(format.getFormat(formatStr));
        style.setFont(font3);
        style.setBorderBottom(BorderStyle.THIN);
        style.setBorderLeft(BorderStyle.THIN);
        style.setBorderRight(BorderStyle.THIN);
        style.setBorderTop(BorderStyle.THIN);

        CellStyle style2 = workbook.createCellStyle();
        style2.setFont(font);
        style2.setAlignment(HorizontalAlignment.LEFT);
        style2.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
        style2.setBorderBottom(BorderStyle.MEDIUM);
        style2.setBorderLeft(BorderStyle.MEDIUM);
        style2.setBorderRight(BorderStyle.MEDIUM);
        style2.setBorderTop(BorderStyle.THIN);

        CellStyle style3 = workbook.createCellStyle();
        style3.setFont(font3);
        style3.setAlignment(HorizontalAlignment.CENTER);
        style3.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
        style3.setBorderBottom(BorderStyle.MEDIUM);
        style3.setBorderLeft(BorderStyle.MEDIUM);
        style3.setBorderRight(BorderStyle.MEDIUM);
        style3.setBorderTop(BorderStyle.THIN);

        Row row = sheet.createRow(0);
        rowCount++;

        CellStyle topHeaderStyle = workbook.createCellStyle();
        topHeaderStyle.setFont(font2);
        topHeaderStyle.setAlignment(HorizontalAlignment.CENTER);

        CellStyle columnHeaderStyle = workbook.createCellStyle();

        columnHeaderStyle.setFont(font2);
        columnHeaderStyle.setAlignment(HorizontalAlignment.CENTER);
        columnHeaderStyle.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
        columnHeaderStyle.setBorderBottom(BorderStyle.MEDIUM);
        columnHeaderStyle.setBorderLeft(BorderStyle.MEDIUM);
        columnHeaderStyle.setBorderRight(BorderStyle.MEDIUM);
        columnHeaderStyle.setBorderTop(BorderStyle.MEDIUM);

        Cell cell = row.createCell(0);
        cell.setCellValue("ตารางการส่งอารมณ์สำหรับ (โยคี" + action + ") ที่ลงทะเบียน ประจำวันที่ " + dateFormat.format(date));
        cell.setCellStyle(topHeaderStyle);

        sheet.addMergedRegion(new CellRangeAddress(0, 0, 0, 3 + vipassanas.size()));
        sheet.addMergedRegion(new CellRangeAddress(1, 2, 0, 0));
        sheet.addMergedRegion(new CellRangeAddress(1, 2, 1, 1));
        sheet.addMergedRegion(new CellRangeAddress(1, 2, 2, 2));
        sheet.addMergedRegion(new CellRangeAddress(1, 1, 3, 2 + vipassanas.size()));
        sheet.addMergedRegion(new CellRangeAddress(1, 2, 3 + vipassanas.size(), 3 + vipassanas.size()));

        row = sheet.createRow(1);
        rowCount++;
        cell = row.createCell(0);
        cell.setCellValue("ลำดับ");
        cell.setCellStyle(columnHeaderStyle);
        sheet.autoSizeColumn(0);

        cell = row.createCell(1);
        cell.setCellValue("ชื่อ-สกุล");
        cell.setCellStyle(columnHeaderStyle);

        cell = row.createCell(2);
        cell.setCellValue("กุฏิ");
        cell.setCellStyle(columnHeaderStyle);

        cell = row.createCell(3);
        cell.setCellValue("พระวิปัสสนาจารย์");
        cell.setCellStyle(columnHeaderStyle);

        cell = row.createCell(3 + vipassanas.size());
        cell.setCellValue("วันที่ออก");
        cell.setCellStyle(columnHeaderStyle);

        row = sheet.createRow(2);
        rowCount++;

        for (int i = 0; i < datas.size(); i++) {
            CellStyle vipassanaStyle = workbook.createCellStyle();
            XSSFFont vipassanaFont = ((XSSFWorkbook) workbook).createFont();
            vipassanaFont.setCharSet(FontCharset.THAI);
            vipassanaFont.setBold(true);
            vipassanaStyle.setFont(vipassanaFont);
            vipassanaStyle.setAlignment(HorizontalAlignment.CENTER);
            vipassanaStyle.setWrapText(false);
            vipassanaStyle.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
            vipassanaStyle.setBorderBottom(BorderStyle.MEDIUM);
            vipassanaStyle.setBorderLeft(BorderStyle.MEDIUM);
            vipassanaStyle.setBorderRight(BorderStyle.MEDIUM);
            vipassanaStyle.setBorderTop(BorderStyle.MEDIUM);
            vipassanaStyle.setRotation((short) 180);

            cell = row.createCell(datas.get(i).getIndex());

            cell.setCellValue(datas.get(i).getName());
            cell.setCellStyle(vipassanaStyle);
            row.setHeightInPoints(85);
//            sheet.autoSizeColumn(datas.get(i).getIndex());
        }
        row = sheet.createRow(3);
        rowCount++;
        sheet.addMergedRegion(new CellRangeAddress(3, 3, 1, 2));
        CellStyle summartStyle = workbook.createCellStyle();
        XSSFFont summaryFont = ((XSSFWorkbook) workbook).createFont();
        summaryFont.setCharSet(FontCharset.THAI);
        summaryFont.setBold(true);
        summaryFont.setUnderline(FontUnderline.SINGLE);
        summartStyle.setFont(summaryFont);
        summartStyle.setBorderBottom(BorderStyle.MEDIUM);
        summartStyle.setBorderLeft(BorderStyle.MEDIUM);
        summartStyle.setBorderRight(BorderStyle.MEDIUM);
        summartStyle.setBorderTop(BorderStyle.MEDIUM);
        summartStyle.setAlignment(HorizontalAlignment.CENTER);
        summartStyle.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
        cell = row.createCell(0);
        cell.setCellStyle(columnHeaderStyle);
        cell = row.createCell(1);
        cell.setCellValue("จำนวนโยคีทั้งหมดที่สอบอารมณ์แต่ละฐาน");
        cell.setCellStyle(summartStyle);

        for (int i = 0; i < datas.size(); i++) {
            cell = row.createCell(datas.get(i).getIndex());
            cell.setCellValue(datas.get(i).getCount());
            cell.setCellStyle(columnHeaderStyle);
        }
        row.setHeightInPoints(20);

        cell = row.createCell(3 + vipassanas.size());
        cell.setCellValue("รวม " + examCount + " คน");
        cell.setCellStyle(columnHeaderStyle);

        for (int i = 0; i < report.size(); i++) {
//            sheet.autoSizeColumn(0);
            row = sheet.createRow(rowCount);
            cell = row.createCell(0);
            cell.setCellStyle(style3);
            cell.setCellValue(i + 1);

            for (int j = 0; j < datas.size(); j++) {
                if (datas.get(j).getVipassanaId() == report.get(i).getVipassanaId()) {
                    cell = row.createCell(1);
                    cell.setCellStyle(style2);
                    cell.setCellValue(report.get(i).getContactName());
                    sheet.autoSizeColumn(1);

                    cell = row.createCell(2);
                    cell.setCellStyle(style3);
                    cell.setCellValue(report.get(i).getRoomName());
                    sheet.autoSizeColumn(2);

                    cell = row.createCell(datas.get(j).getIndex());
                    cell.setCellValue("**");
                    cell.setCellStyle(style3);

                    cell = row.createCell(3 + vipassanas.size());
                    cell.setCellValue(report.get(i).getDateFormat());
                    cell.setCellStyle(style3);
                    sheet.autoSizeColumn(3 + vipassanas.size());

                } else {
                    cell = row.createCell(datas.get(j).getIndex());
                    cell.setCellValue("");
                    cell.setCellStyle(style3);
                }

                sheet.autoSizeColumn(datas.get(j).getIndex());
            }
            row.setHeightInPoints(20);
            rowCount++;
        }

        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        try {
            workbook.write(bos);

        } finally {
            bos.close();
        }
        byte[] bytes = bos.toByteArray();
        String base64 = org.apache.commons.codec.binary.Base64.encodeBase64String(bytes);
        return new ResponseEntity<>(base64, HttpStatus.OK);

    }

    @RequestMapping(value = "/summaryreport", method = RequestMethod.POST)
    public ResponseEntity<String> summaryReport(@RequestParam("temple_id") int temple_id) throws IOException {

        int allCount = 0;
        int monkCount = contactDAO.countMonk(temple_id, "พระ");
        allCount = allCount + monkCount;
        int noviceCount = contactDAO.countMonk(temple_id, "สามเณร");
        allCount = allCount + noviceCount;
        int saththi_count = contactDAO.countMonk(temple_id, "พระสัทธิวิหาริก");
        allCount = allCount + saththi_count;
        int guest_monk_count = contactDAO.countMonk(temple_id, "พระอาคันตุกะ");
        allCount = allCount + guest_monk_count;
        int samsaththi_count = contactDAO.countMonk(temple_id, "สามเราสิทธิวิหาริก");
        allCount = allCount + samsaththi_count;
        int male_yogi_count = contactDAO.countContact(temple_id, "ชาย");
        allCount = allCount + male_yogi_count;
        int bddhist_nun_count = contactDAO.countMonk(temple_id, "ภิกษุณี");
        allCount = allCount + bddhist_nun_count;
        int nun_count = contactDAO.countMonk(temple_id, "แม่ชี");
        allCount = allCount + nun_count;
        int guest_nun_count = contactDAO.countMonk(temple_id, "แม่ชีอาคันตุกะ");
        allCount = allCount + guest_nun_count;
        int female_yogi_count = contactDAO.countContact(temple_id, "หญิง");
        allCount = allCount + female_yogi_count;
        int foreign_monk_count = contactDAO.countMonk(temple_id, "ภิกษุต่างประเทศ");
        allCount = allCount + foreign_monk_count;
        int foreign_nun_count = contactDAO.countMonk(temple_id, "แม่ชีต่างประเทศ");
        allCount = allCount + foreign_nun_count;
        int foreign_male_yogi_count = contactDAO.countContact(temple_id, "ต่างชาติชาย");
        allCount = allCount + foreign_male_yogi_count;
        int foreign_female_yogi_count = contactDAO.countContact(temple_id, "ต่างชาติหญิง");
        allCount = allCount + foreign_female_yogi_count;
        int regular_yogi_count = contactDAO.countMonk(temple_id, "โยคีประจำ");
        allCount = allCount + regular_yogi_count;
        int volunteer_count = contactDAO.countMonk(temple_id, "จิตอาสา");
        allCount = allCount + volunteer_count;

        Workbook workbook = new XSSFWorkbook();
        Sheet sheet = workbook.createSheet();
        sheet.setColumnWidth(0, 1300);

        XSSFFont font = ((XSSFWorkbook) workbook).createFont();
        font.setCharSet(FontCharset.THAI);
        font.setBold(true);
        font.setUnderline(FontUnderline.SINGLE);

        XSSFFont font2 = ((XSSFWorkbook) workbook).createFont();
        font2.setCharSet(FontCharset.THAI);
        font2.setBold(false);

        XSSFFont font3 = ((XSSFWorkbook) workbook).createFont();
        font3.setCharSet(FontCharset.THAI);
        font3.setBold(true);
        font3.setUnderline(FontUnderline.DOUBLE);

        int rowCount = 0;

        CellStyle style = workbook.createCellStyle();
        style.setAlignment(HorizontalAlignment.LEFT);
        style.setVerticalAlignment(VerticalAlignment.CENTER);
        style.setFont(font);

        CellStyle style2 = workbook.createCellStyle();
        style2.setAlignment(HorizontalAlignment.LEFT);
        style2.setVerticalAlignment(VerticalAlignment.CENTER);
        style2.setFont(font2);

        CellStyle style3 = workbook.createCellStyle();
        style3.setAlignment(HorizontalAlignment.CENTER);
        style3.setVerticalAlignment(VerticalAlignment.CENTER);
        style3.setFont(font3);

        Row row = sheet.createRow(rowCount);
        rowCount++;
        Cell cell = row.createCell(0);
        cell.setCellStyle(style);
        cell.setCellValue("สำนักงานสงฆ์");

        row = sheet.createRow(rowCount);
        rowCount++;
        cell = row.createCell(0);
        cell.setCellStyle(style2);
        cell.setCellValue("-พระ " + monkCount + " รูป");

        cell = row.createCell(1);
        cell.setCellStyle(style2);
        cell.setCellValue("-สามเณร " + noviceCount + " รูป");

        cell = row.createCell(2);
        cell.setCellStyle(style2);
        cell.setCellValue("-พระสัทธิวิหาริก " + saththi_count + " รูป");

        row = sheet.createRow(rowCount);
        rowCount++;
        cell = row.createCell(0);
        cell.setCellStyle(style2);
        cell.setCellValue("-พระอาคันตุกะ " + guest_monk_count + " รูป");

        cell = row.createCell(1);
        cell.setCellStyle(style2);
        cell.setCellValue("-สามสิทธิวิหาริก " + samsaththi_count + " รูป");

        cell = row.createCell(2);
        cell.setCellStyle(style2);
        cell.setCellValue("-โยคีชาย " + male_yogi_count + " คน");

        row = sheet.createRow(rowCount);
        rowCount++;

        row = sheet.createRow(rowCount);
        rowCount++;

        cell = row.createCell(0);
        cell.setCellStyle(style);
        cell.setCellValue("สำนักงานแม่ชี");

        row = sheet.createRow(rowCount);
        rowCount++;

        cell = row.createCell(0);
        cell.setCellStyle(style2);
        cell.setCellValue("-ภิกษุณี " + bddhist_nun_count + " รูป");

        cell = row.createCell(1);
        cell.setCellStyle(style2);
        cell.setCellValue("-แม่ชี " + nun_count + " รูป");

        cell = row.createCell(2);
        cell.setCellStyle(style2);
        cell.setCellValue("-โยคีหญิง " + female_yogi_count + " คน");

        row = sheet.createRow(rowCount);
        rowCount++;
        cell = row.createCell(0);
        cell.setCellStyle(style2);
        cell.setCellValue("-แม่ชีอาคันตุกะ " + guest_nun_count + " รูป");

        row = sheet.createRow(rowCount);
        rowCount++;

        row = sheet.createRow(rowCount);
        rowCount++;

        cell = row.createCell(0);
        cell.setCellStyle(style);
        cell.setCellValue("สำนักงานต่างประเทศ");

        row = sheet.createRow(rowCount);
        rowCount++;

        cell = row.createCell(0);
        cell.setCellStyle(style2);
        cell.setCellValue("-ภิกษุต่างประเทศ " + foreign_monk_count + " รูป");

        cell = row.createCell(1);
        cell.setCellStyle(style2);
        cell.setCellValue("-แม่ชีต่างประเทศ " + foreign_nun_count + " รูป");

        row = sheet.createRow(rowCount);
        rowCount++;

        cell = row.createCell(0);
        cell.setCellStyle(style2);
        cell.setCellValue("-โยคีชายต่างประเทศ " + foreign_male_yogi_count + " คน");

        cell = row.createCell(1);
        cell.setCellStyle(style2);
        cell.setCellValue("-โยคีหญิงต่างประเทศ " + foreign_female_yogi_count + " คน");

        row = sheet.createRow(rowCount);
        rowCount++;

        row = sheet.createRow(rowCount);
        rowCount++;

        cell = row.createCell(0);
        cell.setCellStyle(style);
        cell.setCellValue("สำนักงานวัด");

        row = sheet.createRow(rowCount);
        rowCount++;

        cell = row.createCell(0);
        cell.setCellStyle(style2);
        cell.setCellValue("-โยคีประจำ " + regular_yogi_count + " คน");

        cell = row.createCell(1);
        cell.setCellStyle(style2);
        cell.setCellValue("-จิตอาสา " + volunteer_count + " คน");

        row = sheet.createRow(rowCount);
        rowCount++;

        sheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 0, 2));
        row = sheet.createRow(rowCount);
        rowCount++;

        cell = row.createCell(0);
        cell.setCellStyle(style3);
        cell.setCellValue("รวมทั้งสิน " + allCount + " รูป/คน");

        sheet.autoSizeColumn(0);
        sheet.autoSizeColumn(1);
        sheet.autoSizeColumn(2);

        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        try {
            workbook.write(bos);

        } finally {
            bos.close();
        }
        byte[] bytes = bos.toByteArray();
        String base64 = org.apache.commons.codec.binary.Base64.encodeBase64String(bytes);
        return new ResponseEntity<>(base64, HttpStatus.OK);
    }

    @RequestMapping(value = "/contactreport", method = RequestMethod.GET)
    public String contactReport(Model model) {
        try {
            if (roleDAO.pageView("report", HomeController.getRole_id()) == false) {
                return "redirect:/impervious";
            }

        } catch (Exception e) {
            return "redirect:/";
        }
        System.out.println("Temple_id:" + HomeController.getTemple_id());
        Date date = new Date();
        date.setHours(1);
        date.setMinutes(0);
        SimpleDateFormat sf = new SimpleDateFormat("dd-MM-yyyy hh:mm");
        model = homeController.getRole(model);
        model.addAttribute("dateval", sf.format(date));
        model.addAttribute("enddateval", sf.format(date));
        model.addAttribute("contactreport", null);
        model.addAttribute("templeid", HomeController.getTemple_id());

        List<RoleItem> roleItems = new ArrayList<>();
        roleItems = roleDAO.getRoleITem(HomeController.getRole_id());
        Boolean statusadmin = RoleCheck.checkAdmin(roleItems);
        if (statusadmin == true) {
            model.addAttribute("roleadmin", "admin");
        } else if (statusadmin == false) {
            model.addAttribute("roleadmin", "user");
        }

        return "report/contact_report";
    }

    @RequestMapping(value = "/contactreport", method = RequestMethod.POST)
    public String getContactReport(Model model, @RequestParam(value = "start") String start,
            @RequestParam(value = "end") String end) throws ParseException {
        System.out.println("Start" + start);
        List<ContactReport> report = new ArrayList<ContactReport>();
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        SimpleDateFormat edf = new SimpleDateFormat("yyyy-MM-dd");
        Date startDate = sdf.parse(start);
        Date endDate = sdf.parse(end);
        String startD = edf.format(startDate);
        String endD = edf.format(endDate);

        report = checkinChartsDAO.getAllContactReport(HomeController.getTemple_id(), startD, endD);

        for (int i = 0; i < report.size(); i++) {
            report.get(i).setAddress(ReportController.decrypt(report.get(i).getAddress(), HomeController.getKey_hash_profile()));
            report.get(i).setDistrict(ReportController.decrypt(report.get(i).getDistrict(), HomeController.getKey_hash_profile()));
        }

        System.out.println("Report:" + report.toString());
        model = homeController.getRole(model);
        model.addAttribute("dateval", start);
        model.addAttribute("enddateval", end);
        model.addAttribute("contact_report", report);
        model.addAttribute("templeid", HomeController.getTemple_id());

        List<RoleItem> roleItems = new ArrayList<>();
        roleItems = roleDAO.getRoleITem(HomeController.getRole_id());
        Boolean statusadmin = RoleCheck.checkAdmin(roleItems);
        if (statusadmin == true) {
            model.addAttribute("roleadmin", "admin");
        } else if (statusadmin == false) {
            model.addAttribute("roleadmin", "user");
        }

        return "report/contact_report";
    }

    @RequestMapping(value = "/selectcontactreport", method = RequestMethod.POST)
    public String selectcontact(@RequestParam("temple_id") String select, @RequestParam(value = "start_date") String start_date,
            @RequestParam(value = "end_date") String end_date, @RequestParam(value = "action") String action,
            Model model, HttpServletRequest request, HttpServletResponse response) throws ParseException, IOException {
        System.out.println(action);
        Calendar cal = Calendar.getInstance();
        Date date = cal.getTime();
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        SimpleDateFormat edf = new SimpleDateFormat("yyyy-MM-dd");

        Date startDate = new Date();
        if (!start_date.equals("")) {
            startDate = sdf.parse(start_date);
        }

        Date endDate = cal.getTime();
        if (!end_date.equals("")) {
            endDate = sdf.parse(end_date);
        }

        String startD = edf.format(startDate);
        String endD = edf.format(endDate);
        if (action.equals("PDF")) {
            getReport.genReportContact("Contact", select, "เช็คอิน", startD, endD, HomeController.getKey_hash_profile(), request, response);
        }

        if (action.equals("CSV")) {
            getReport.genReportContactCSV("Contact", select, "เช็คอิน", startD, endD, HomeController.getKey_hash_profile(), request, response);
        }

        return "null";
    }

    @RequestMapping(value = "/eventreport", method = RequestMethod.POST)
    public ResponseEntity<String> eventReport(@RequestParam("event_id") String event_id,
            Model model) throws ParseException, IOException {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMMM G YYYY", new Locale("th", "TH"));
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm");
//        Date examDate = dateFormat.parse(exam_date);
        Date date = new Date();
        System.out.println("event id: " + event_id);

        // GET EVENT DATA
        Events events = new Events();
        events = eventsDAO.findById(Integer.parseInt(event_id));

        List<EventInfo> eventInfos = new ArrayList<>();
        eventInfos = eventsDAO.findEventInfo(Integer.parseInt(event_id));

        Monk monk = new Monk();
        List<Monk> monks = new ArrayList<>();
        for (int i = 0; i < eventInfos.size(); i++) {
            monk = contactDAO.findmonkInfo(eventInfos.get(i).getMonk_id());
            monks.add(monk);
        }

//        date = sdf.parse(exam_date);
        Workbook workbook = new XSSFWorkbook();
        Sheet sheet = workbook.createSheet();

        // css top header on sheet
        XSSFFont font = ((XSSFWorkbook) workbook).createFont();
        font.setCharSet(FontCharset.THAI);
        font.setBold(true);
        font.setColor(IndexedColors.RED.getIndex());

        // css title on table
        XSSFFont font2 = ((XSSFWorkbook) workbook).createFont();
        font2.setCharSet(FontCharset.THAI);
        font2.setBold(true);
        font2.setColor(IndexedColors.GREEN.getIndex());

        XSSFFont font3 = ((XSSFWorkbook) workbook).createFont();
        font3.setCharSet(FontCharset.THAI);
        font3.setBold(false);

        // monk font
        XSSFFont font4 = ((XSSFWorkbook) workbook).createFont();
        font3.setCharSet(FontCharset.THAI);
        font3.setBold(false);

        int rowCount = 3;
        String formatStr = "#,##0.00";
        CellStyle style = workbook.createCellStyle();
        DataFormat format = workbook.createDataFormat();
        style.setDataFormat(format.getFormat(formatStr));
        style.setFont(font3);
        style.setBorderBottom(BorderStyle.THIN);
        style.setBorderLeft(BorderStyle.THIN);
        style.setBorderRight(BorderStyle.THIN);
        style.setBorderTop(BorderStyle.THIN);

        CellStyle style2 = workbook.createCellStyle();
        style2.setFont(font3);
        style2.setBorderBottom(BorderStyle.THIN);
        style2.setBorderLeft(BorderStyle.THIN);
        style2.setBorderRight(BorderStyle.THIN);
        style2.setBorderTop(BorderStyle.THIN);

        CellStyle style3 = workbook.createCellStyle();
        style3.setFont(font2);
        style3.setAlignment(HorizontalAlignment.RIGHT);
        style3.setBorderBottom(BorderStyle.THIN);
        style3.setBorderLeft(BorderStyle.THIN);
        style3.setBorderRight(BorderStyle.THIN);
        style3.setBorderTop(BorderStyle.THIN);

        Row row = sheet.createRow(0);
        rowCount++;

        // alignment on top header
        CellStyle topHeaderStyle = workbook.createCellStyle();
        topHeaderStyle.setFont(font);
        topHeaderStyle.setAlignment(HorizontalAlignment.CENTER);
        topHeaderStyle.setBorderBottom(BorderStyle.THIN);
        topHeaderStyle.setBorderLeft(BorderStyle.THIN);
        topHeaderStyle.setBorderRight(BorderStyle.THIN);
        topHeaderStyle.setBorderTop(BorderStyle.THIN);

        Cell cell = row.createCell(1);
        cell.setCellValue("กิจนิมนต์: " + events.getEvent_name());
        cell.setCellStyle(topHeaderStyle);

        // fix top header of table border on right
        CellStyle fixTopHeaderStyle = workbook.createCellStyle();
        fixTopHeaderStyle.setFont(font);
        fixTopHeaderStyle.setAlignment(HorizontalAlignment.CENTER);
        fixTopHeaderStyle.setBorderLeft(BorderStyle.THIN);

        cell = row.createCell(4);
        cell.setCellValue(" ");
        cell.setCellStyle(fixTopHeaderStyle);

        sheet.addMergedRegion(new CellRangeAddress(0, 0, 1, 3));
        sheet.autoSizeColumn(1, true);

        // alignment on thead on table
        CellStyle columnHeaderStyle = workbook.createCellStyle();

        columnHeaderStyle.setFont(font2);
        columnHeaderStyle.setAlignment(HorizontalAlignment.CENTER);
        columnHeaderStyle.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
        columnHeaderStyle.setBorderBottom(BorderStyle.THIN);
        columnHeaderStyle.setBorderLeft(BorderStyle.THIN);
        columnHeaderStyle.setBorderRight(BorderStyle.THIN);
        columnHeaderStyle.setBorderTop(BorderStyle.THIN);

        row = sheet.createRow(1);
        cell = row.createCell(1);
        cell.setCellValue("รายละเอียด");
        cell.setCellStyle(columnHeaderStyle);
        sheet.autoSizeColumn(1);

        cell = row.createCell(2);
        cell.setCellValue("วันที่มีกิจนิมนต์");
        cell.setCellStyle(columnHeaderStyle);
        sheet.autoSizeColumn(2);

        cell = row.createCell(3);
        cell.setCellValue("ปัจจัย (บาท)");
        cell.setCellStyle(columnHeaderStyle);
        sheet.autoSizeColumn(3);

        // alignment text on event detail
        CellStyle cellStyleDetail = workbook.createCellStyle();

        cellStyleDetail.setFont(font4);
        cellStyleDetail.setAlignment(HorizontalAlignment.CENTER);
        cellStyleDetail.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
        cellStyleDetail.setBorderBottom(BorderStyle.THIN);
        cellStyleDetail.setBorderLeft(BorderStyle.THIN);
        cellStyleDetail.setBorderRight(BorderStyle.THIN);
        cellStyleDetail.setBorderTop(BorderStyle.THIN);

        row = sheet.createRow(2);
        cell = row.createCell(1);
        cell.setCellValue(events.getDescription());
        cell.setCellStyle(cellStyleDetail);
        sheet.autoSizeColumn(1);

        cell = row.createCell(2);
        cell.setCellValue(sdf.format(events.getEvent_start()));
        cell.setCellStyle(cellStyleDetail);
        sheet.autoSizeColumn(2);

        cell = row.createCell(3);
        cell.setCellValue(events.getIncome());
        cell.setCellStyle(cellStyleDetail);
        sheet.autoSizeColumn(3);

        row = sheet.createRow(5);
        cell = row.createCell(1);
        cell.setCellValue("ลำดับ");
        cell.setCellStyle(columnHeaderStyle);
        sheet.autoSizeColumn(1);

        cell = row.createCell(2);
        cell.setCellValue("รายชื่อพระลูกวัด");
        cell.setCellStyle(columnHeaderStyle);
        sheet.autoSizeColumn(2);

        // alignment monk text
        CellStyle monkStyle = workbook.createCellStyle();
        monkStyle.setFont(font4);
        monkStyle.setAlignment(HorizontalAlignment.LEFT);
        monkStyle.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
        monkStyle.setBorderBottom(BorderStyle.THIN);
        monkStyle.setBorderLeft(BorderStyle.THIN);
        monkStyle.setBorderRight(BorderStyle.THIN);
        monkStyle.setBorderTop(BorderStyle.THIN);

        if (!monks.isEmpty()) {
            for (int i = 0; i < monks.size(); i++) {
                row = sheet.createRow(6 + i);
                cell = row.createCell(1);
                cell.setCellValue(i + 1);
                cell.setCellStyle(columnHeaderStyle);
                sheet.autoSizeColumn(1);

                cell = row.createCell(2);
                cell.setCellValue(monks.get(i).getFirstname() + "  " + monks.get(i).getLastname());
                cell.setCellStyle(monkStyle);
                sheet.autoSizeColumn(2);
            }
        }

        Row subHeader = sheet.createRow(rowCount);
        rowCount++;

        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        try {
            workbook.write(bos);

        } finally {
            bos.close();
        }
        byte[] bytes = bos.toByteArray();
        String base64 = org.apache.commons.codec.binary.Base64.encodeBase64String(bytes);
        return new ResponseEntity<>(base64, HttpStatus.OK);

    }

    public static String decrypt(String strToDecrypt, String secret) {
        try {
            setKey(secret);
            Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5PADDING");
            cipher.init(Cipher.DECRYPT_MODE, secretKey);
            return new String(cipher.doFinal(Base64.getDecoder().decode(strToDecrypt)));
        } catch (Exception e) {
            System.out.println("Error while decrypting: " + e.toString());
        }
        return null;
    }

    public static int getAge(String birth_date) throws ParseException {
        int age = 0;
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-mmm-dd");
        Date date = new Date();
        Date birthDate = formatter.parse(birth_date);
        int curentYear = date.getYear();
        int birthYear = birthDate.getYear();
        age = curentYear - birthYear;
        return age;
    }

}
