/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycrm.controller;

/**
 *
 * @author Lenovo
 */
import com.mycrm.common.RoleCheck;
import com.mycrm.dao.RoleDAO;
import com.mycrm.dao.RoomDAO;
import com.mycrm.dao.StaffDAO;
import com.mycrm.dao.TempleDAO;
import com.mycrm.domain.RoleItem;
import com.mycrm.domain.Room;
import com.mycrm.domain.RoomName;
import com.mycrm.domain.Staff;
import com.mycrm.domain.Temple;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@Component
public class RoomController {

    int PER_PAGE = 10;

    @Autowired
    RoomDAO roomDAO;

    @Autowired
    TempleDAO templeDAO;

    @Autowired
    RoleDAO roleDAO;

    @Autowired
    StaffDAO staffDAO;

    @Autowired
    HomeController homeController;

    @RequestMapping(value = "/addRoom", method = RequestMethod.GET)
    public String add(Model model) {
        try {
            if (roleDAO.pageCreate("room_page", HomeController.getRole_id()) == false) {
                return "redirect:/impervious";
            }

        } catch (Exception e) {
            return "redirect:/";
        }

        List<RoleItem> roleItems = new ArrayList<>();
        roleItems = roleDAO.getRoleITem(HomeController.getRole_id());
        Boolean statusadmin = RoleCheck.checkAdmin(roleItems);
        if (statusadmin == true) {
            model.addAttribute("roleadmin", "admin");
        } else if (statusadmin == false) {
            model.addAttribute("roleadmin", "user");
        }
        model = homeController.getRole(model);
        List<Temple> temple = new ArrayList<Temple>();
        temple = templeDAO.getALL();
        model.addAttribute("room", new Room());
        model.addAttribute("temple", temple);

        return "room/addroom";
    }

    @RequestMapping(value = "/saveRoom", method = RequestMethod.POST)
    public String save(Model model, @Valid @ModelAttribute("room") Room room, BindingResult result) {
        List<RoleItem> roleItems = new ArrayList<>();
        roleItems = roleDAO.getRoleITem(HomeController.getRole_id());
        Boolean statusadmin = RoleCheck.checkAdmin(roleItems);
        if (statusadmin == true) {
            model.addAttribute("roleadmin", "admin");
        } else if (statusadmin == false) {
            model.addAttribute("roleadmin", "user");
        }
        model = homeController.getRole(model);
        if (result.hasErrors()) {
            return "room/addroom";

        }
        room.setTemple_id(HomeController.getTemple_id());
        roomDAO.insert(room);
        model.addAttribute("room", new Room());
        return "redirect:/roomAll";
    }

    @RequestMapping(value = "/roomAll", method = RequestMethod.GET)
    public String templeList(Model model, @RequestParam(value = "page", required = false) String page) throws ParseException {

        String page_Create = "OK";
        String page_Delete = "OK";
        String page_Edit = "OK";

        try {
            if (roleDAO.pageView("room_page", HomeController.getRole_id()) == false) {
                return "redirect:/impervious";
            }

        } catch (Exception e) {
            return "redirect:/";
        }

        if (roleDAO.pageCreate("room_page", HomeController.getRole_id()) == false) {
            page_Create = null;
        }

        if (roleDAO.pageEdit("room_page", HomeController.getRole_id()) == false) {
            page_Edit = null;
        }
        if (roleDAO.pageDelete("room_page", HomeController.getRole_id()) == false) {
            page_Delete = null;
        }

        if (page == null || Integer.parseInt(page) < 0) {
            page = "0";
        }
        int countRow = 0;
        int page_count = 0;
        int temple_id = HomeController.getTemple_id();
        countRow = roomDAO.countRows(temple_id);
        Double pagecountDB = Math.ceil((double) countRow / (double) PER_PAGE);
        page_count = pagecountDB.intValue();
        List<Room> rooms = new ArrayList<Room>();
        rooms = roomDAO.getRoomList(temple_id, page, PER_PAGE);
        if (Integer.parseInt(page) > 0) {
            model.addAttribute("page_priv", true);

        }
        if (Integer.parseInt(page) < (page_count - 1)) {
            model.addAttribute("page_next", true);
        }
        if (page_count >= 10) {
            page_count = 10;
        }
        model = homeController.getRole(model);
        model.addAttribute("rooms", rooms);
        model.addAttribute("page_count", page_count);
        model.addAttribute("page", page);
        model.addAttribute("page_Create", page_Create);
        model.addAttribute("page_Delete", page_Delete);
        model.addAttribute("page_Edit", page_Edit);

        List<RoleItem> roleItems = new ArrayList<>();
        roleItems = roleDAO.getRoleITem(HomeController.getRole_id());
        Boolean statusadmin = RoleCheck.checkAdmin(roleItems);
        if (statusadmin == true) {
            model.addAttribute("roleadmin", "admin");
        } else if (statusadmin == false) {
            model.addAttribute("roleadmin", "user");
        }

        return "room/roomlist";
    }

    @RequestMapping(value = "/deleteroom", method = RequestMethod.POST)
    public String deletetemple(Model model, @RequestParam(value = "id") int id) {
        roomDAO.delete(id);
        return "redirect:/roomAll";
    }

    @RequestMapping(value = "/editroom", method = RequestMethod.POST)
    public String edit(Model model, @RequestParam(value = "id") int id) {
        List<Temple> temple = new ArrayList<Temple>();
        List<RoomName> roomlist = new ArrayList<RoomName>();
        int count = 0;
        int sum = 0;
        temple = templeDAO.getALL();
        Room room = roomDAO.find(id);
        roomlist = roomDAO.getRoomName(id);
        count = roomDAO.countRoomName(id);
        sum = roomDAO.sumRoomName(id);
        model = homeController.getRole(model);
        model.addAttribute("temple", temple);
        model.addAttribute("room", room);
        model.addAttribute("roomlist", roomlist);
        model.addAttribute("id", id);
        model.addAttribute("room_amount", count);
        model.addAttribute("room_id", room.getId());
        model.addAttribute("sum", sum);

        List<RoleItem> roleItems = new ArrayList<>();
        roleItems = roleDAO.getRoleITem(HomeController.getRole_id());
        Boolean statusadmin = RoleCheck.checkAdmin(roleItems);
        if (statusadmin == true) {
            model.addAttribute("roleadmin", "admin");
        } else if (statusadmin == false) {
            model.addAttribute("roleadmin", "user");
        }

        return "room/editroom";
    }

    @RequestMapping(value = "/saveeditroom", method = RequestMethod.POST)
    public String edit(Model model, @Valid @ModelAttribute("room") Room room, BindingResult result) {
        Room roomedit = roomDAO.find(room.getId());
        if (result.hasErrors()) {
            List<Temple> temple = new ArrayList<Temple>();
            List<RoomName> roomlist = new ArrayList<RoomName>();
            int count = 0;
            int sum = 0;
            temple = templeDAO.getALL();
            roomlist = roomDAO.getRoomName(room.getId());
            count = roomDAO.countRoomName(room.getId());
            sum = roomDAO.sumRoomName(room.getId());
            model = homeController.getRole(model);
            model.addAttribute("temple", temple);
            model.addAttribute("room", room);
            model.addAttribute("roomlist", roomlist);
            model.addAttribute("id", room.getId());
            model.addAttribute("room_amount", count);
            model.addAttribute("room_id", room.getId());
            model.addAttribute("sum", sum);

            List<RoleItem> roleItems = new ArrayList<>();
            roleItems = roleDAO.getRoleITem(HomeController.getRole_id());
            Boolean statusadmin = RoleCheck.checkAdmin(roleItems);
            if (statusadmin == true) {
                model.addAttribute("roleadmin", "admin");
            } else if (statusadmin == false) {
                model.addAttribute("roleadmin", "user");
            }
            return "room/editroom";

        }

        if (room.getType() == null) {
            room.setType(roomedit.getType());
        }
        if (room.getToilet_type() == null) {
            room.setToilet_type(roomedit.getToilet_type());
        }
        room.setTemple_id(HomeController.getTemple_id());
        roomDAO.update(room);
        return "redirect:/roomAll";
    }

    @RequestMapping(value = "/addroomname", method = RequestMethod.POST)
    public String addRoomname(Model model, @RequestParam(value = "id") int id,
            @RequestParam(value = "room_id") int room_id,
            @RequestParam(value = "name") String name,
            @RequestParam(value = "amount") int amount) {
        RoomName add = new RoomName(0, room_id, name, amount, 0, 0, 0, HomeController.getTemple_id());
        roomDAO.insertRoomName(add);
        List<Temple> temple = new ArrayList<Temple>();
        List<RoomName> roomlist = new ArrayList<RoomName>();
        temple = templeDAO.getALL();
        Room room = roomDAO.find(id);
        roomlist = roomDAO.getRoomName(id);
        int count = roomDAO.countRoomName(id);
        int sum = roomDAO.sumRoomName(id);
        model = homeController.getRole(model);
        model.addAttribute("temple", temple);
        model.addAttribute("room", room);
        model.addAttribute("roomlist", roomlist);
        model.addAttribute("id", id);
        model.addAttribute("room_id", room.getId());
        model.addAttribute("room_amount", count);
        model.addAttribute("sum", sum);

        List<RoleItem> roleItems = new ArrayList<>();
        roleItems = roleDAO.getRoleITem(HomeController.getRole_id());
        Boolean statusadmin = RoleCheck.checkAdmin(roleItems);
        if (statusadmin == true) {
            model.addAttribute("roleadmin", "admin");
        } else if (statusadmin == false) {
            model.addAttribute("roleadmin", "user");
        }
        return "room/editroom";
    }

    @RequestMapping(value = "/updatedelete", method = RequestMethod.POST)
    public String deleteroom(Model model, @RequestParam(value = "id") int id,
            @RequestParam(value = "id_delete") int id_delete) {
        System.out.println("updatedelete********************************************");
        System.out.println(id);
        System.out.println(id_delete);
        roomDAO.deleteRoom(id_delete);
        List<Temple> temple = new ArrayList<Temple>();
        List<RoomName> roomlist = new ArrayList<RoomName>();
        temple = templeDAO.getALL();
        Room room = roomDAO.find(id);
        roomlist = roomDAO.getRoomName(id);
        int count = roomDAO.countRoomName(id);
        int sum = roomDAO.sumRoomName(id);
        model = homeController.getRole(model);
        model.addAttribute("temple", temple);
        model.addAttribute("room", room);
        model.addAttribute("roomlist", roomlist);
        model.addAttribute("id", id);
        model.addAttribute("room_id", room.getId());
        model.addAttribute("room_amount", count);
        model.addAttribute("sum", sum);

        List<RoleItem> roleItems = new ArrayList<>();
        roleItems = roleDAO.getRoleITem(HomeController.getRole_id());
        Boolean statusadmin = RoleCheck.checkAdmin(roleItems);
        if (statusadmin == true) {
            model.addAttribute("roleadmin", "admin");
        } else if (statusadmin == false) {
            model.addAttribute("roleadmin", "user");
        }
        return "room/editroom";

    }

    @RequestMapping(value = "/viewroom", method = RequestMethod.POST)
    public String viewroom(Model model, @RequestParam(value = "id") int id) {
        List<Temple> temple = new ArrayList<Temple>();
        List<RoomName> roomlist = new ArrayList<RoomName>();
        int count = 0;
        int sum = 0;
        temple = templeDAO.getALL();
        Room room = roomDAO.find(id);
        roomlist = roomDAO.getRoomName(id);
        count = roomDAO.countRoomName(id);
        sum = roomDAO.sumRoomName(id);
        model = homeController.getRole(model);
        model.addAttribute("temple", temple);
        model.addAttribute("room", room);
        model.addAttribute("roomlist", roomlist);
        model.addAttribute("id", id);
        model.addAttribute("room_amount", count);
        model.addAttribute("room_id", room.getId());
        model.addAttribute("sum", sum);

        List<RoleItem> roleItems = new ArrayList<>();
        roleItems = roleDAO.getRoleITem(HomeController.getRole_id());
        Boolean statusadmin = RoleCheck.checkAdmin(roleItems);
        if (statusadmin == true) {
            model.addAttribute("roleadmin", "admin");
        } else if (statusadmin == false) {
            model.addAttribute("roleadmin", "user");
        }

        return "room/view";
    }

    @RequestMapping(value = "/updatedeletetest", method = RequestMethod.POST)
    public String updatedeletetest(Model model, @RequestParam(value = "test_id") int id,
            @RequestParam(value = "test_delete_id") int id_delete) {
        System.out.println("updatedelete********************************************");
        System.out.println(id);
        System.out.println(id_delete);
        List<Temple> temple = new ArrayList<Temple>();
        List<RoomName> roomlist = new ArrayList<RoomName>();
        temple = templeDAO.getALL();
        Room room = roomDAO.find(id);
        roomlist = roomDAO.getRoomName(id);
        int count = roomDAO.countRoomName(id);
        int sum = roomDAO.sumRoomName(id);
        roomDAO.deleteRoom(id_delete);
        model = homeController.getRole(model);
        model.addAttribute("temple", temple);
        model.addAttribute("room", room);
        model.addAttribute("roomlist", roomlist);
        model.addAttribute("id", id);
        model.addAttribute("room_id", room.getId());
        model.addAttribute("room_amount", count);
        model.addAttribute("sum", sum);

        List<RoleItem> roleItems = new ArrayList<>();
        roleItems = roleDAO.getRoleITem(HomeController.getRole_id());
        Boolean statusadmin = RoleCheck.checkAdmin(roleItems);
        if (statusadmin == true) {
            model.addAttribute("roleadmin", "admin");
        } else if (statusadmin == false) {
            model.addAttribute("roleadmin", "user");
        }

        return "room/editroom";

    }
}
