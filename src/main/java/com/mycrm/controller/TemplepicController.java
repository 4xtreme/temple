/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycrm.controller;

import com.mycrm.common.RoleCheck;
import com.mycrm.dao.RoleDAO;
import com.mycrm.dao.StaffDAO;
import java.io.IOException;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import com.mycrm.dao.TemplepicDAO;
import com.mycrm.domain.RoleItem;
import com.mycrm.domain.Staff;
import com.mycrm.domain.Templepic;
import java.util.ArrayList;
import java.util.List;
import org.springframework.web.bind.annotation.PathVariable;

/**
 *
 * @author she my sunshine
 */
@Controller
public class TemplepicController {

    int PER_PAGE = 5;

    @Autowired
    TemplepicDAO templepicDAO;

    @Autowired
    RoleDAO roleDAO;

    @Autowired
    StaffDAO staffDAO;
    
    @Autowired
    HomeController homeController;

    @SuppressWarnings("unused")
    @RequestMapping(value = "/uploadfile", method = RequestMethod.POST)
    public String addtemplepicAction(
            @RequestParam(value = "templepic") MultipartFile templepic, @RequestParam(value = "description") String description) {
        System.out.println("iiiiiiiiiiinnnnn" + templepic.getOriginalFilename());
        Templepic t = new Templepic();
        t.setFilename(templepic.getOriginalFilename());
        try {
            t.setFiledata(templepic.getBytes());
        } catch (IOException ex) {
            Logger.getLogger(TemplepicController.class.getName()).log(Level.SEVERE, null, ex);
        }
        t.setFile_type(templepic.getContentType());
        t.setTemple_id(HomeController.getTemple_id());
        t.setDescription(description);

        System.out.println("ddddddddddddddddd" + t.getDescription());
        System.out.println("setTemple_id" + t);

        templepicDAO.insert(t);

        return "redirect:/templepiclist";
    }

    @RequestMapping(value = "/addpicture", method = RequestMethod.GET)
    public String uploadtemplepic(Model model) {
        try {
            if (roleDAO.pageCreate("temple_page", HomeController.getRole_id()) == false) {
                return "redirect:/impervious";
            }

        } catch (Exception e) {
            return "redirect:/";
        }

        List<RoleItem> roleItems = new ArrayList<>();
        roleItems = roleDAO.getRoleITem(HomeController.getRole_id());
        Boolean statusadmin = RoleCheck.checkAdmin(roleItems);
        if (statusadmin == true) {
            model.addAttribute("roleadmin", "admin");
        } else if (statusadmin == false) {
            model.addAttribute("roleadmin", "user");
        }
        model = homeController.getRole(model);

        return "temple/addpicture";
    }

    @RequestMapping(value = "/showpicture/{id}", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<byte[]> getImage(@PathVariable int id) throws SQLException {

        //    GridFSDBFile gridFsFile = fileService.findUserAccountAvatarById(userId);
        Templepic t = templepicDAO.findTemplepic(id);

        System.out.println(t.toString() + "-------------------------");
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.parseMediaType(t.getFile_type()));
        headers.set("Content-Disposition", "attachment; filename=fileName.jpg");

        byte[] b = t.getFiledata();
        return new ResponseEntity<byte[]>(b, headers, HttpStatus.OK);
    }

/////////////////////////////////////////////////////////////////////
    @RequestMapping(value = "/templepiclist", method = RequestMethod.GET)
    public String templepicList(Model model, @RequestParam(value = "page", required = false) String page) {
        String page_Create = "OK";
        String page_Delete = "OK";
        String page_Edit = "OK";
        try {
            if (roleDAO.pageView("temple_page", HomeController.getRole_id()) == false) {
                return "redirect:/impervious";
            }

        } catch (Exception e) {
            return "redirect:/";
        }

        if (roleDAO.pageCreate("temple_page", HomeController.getRole_id()) == false) {
            page_Create = null;
        }

        if (roleDAO.pageEdit("temple_page", HomeController.getRole_id()) == false) {
            page_Edit = null;
        }
        if (roleDAO.pageDelete("temple_page", HomeController.getRole_id()) == false) {
            page_Delete = null;
        }
        if (page == null || Integer.parseInt(page) < 0) {
            page = "0";
        }
        int countRow = 0;
        int page_count = 0;
        int temple_id = HomeController.getTemple_id();
        countRow = templepicDAO.countRows(temple_id);
        Double pagecountDB = Math.ceil((double) countRow / (double) PER_PAGE);
        page_count = pagecountDB.intValue();
        List<Templepic> templepic = new ArrayList<Templepic>();
        templepic = templepicDAO.getTemplepicList(page, PER_PAGE, temple_id);

        if (Integer.parseInt(page) > 0) {
            model.addAttribute("page_priv", true);

        }
        if (Integer.parseInt(page) < (page_count - 1)) {
            model.addAttribute("page_next", true);
        }
        if (page_count >= 10) {
            page_count = 10;
        }
        model = homeController.getRole(model);
        model.addAttribute("templepics", templepic);
        model.addAttribute("page_count", page_count);
        model.addAttribute("page", page);
        System.out.println("--++++++++++++++++++++++++++++++++" + templepic.toString());
        model.addAttribute("page_Create", page_Create);
        model.addAttribute("page_Delete", page_Delete);
        model.addAttribute("page_Edit", page_Edit);

        List<RoleItem> roleItems = new ArrayList<>();
        roleItems = roleDAO.getRoleITem(HomeController.getRole_id());
        Boolean statusadmin = RoleCheck.checkAdmin(roleItems);
        if (statusadmin == true) {
            model.addAttribute("roleadmin", "admin");
        } else if (statusadmin == false) {
            model.addAttribute("roleadmin", "user");
        }

        return "temple/templepiclist";
    }

    @RequestMapping(value = "/deletetemplepic", method = RequestMethod.POST)
    public String deletetemplepic(Model model, @RequestParam(value = "id") int id) {

        templepicDAO.delete(id);
        return "redirect:/templepiclist";
    }
}
