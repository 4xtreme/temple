/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycrm.controller.rest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import java.text.ParseException;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

/**
 *
 * @author JAME
 */
@Controller
@RequestMapping(value = "/api")
public class RestTestConnection {
  
    @RequestMapping(value = "/testconnection/obj", method = RequestMethod.POST)
    public ResponseEntity<Vector> testConnection() throws ParseException {
        Vector v = new Vector();
        Map<String, Object> obj = new HashMap<String, Object>();           
        obj.put("status", "Connect Success");
        System.out.println("tttt");
        v.add(obj);
        return new ResponseEntity<Vector>(v, HttpStatus.OK);
        
    }
    
    @RequestMapping(value = "/test1/obj", method = RequestMethod.POST)
    public ResponseEntity<Vector> test1() throws ParseException {
        Vector v = new Vector();
        Map<String, Object> obj = new HashMap<String, Object>();           
        obj.put("status", "Connect Success");
        System.out.println("tttt");
        v.add(obj);
        return new ResponseEntity<Vector>(v, HttpStatus.OK);
        
    }


    
}
