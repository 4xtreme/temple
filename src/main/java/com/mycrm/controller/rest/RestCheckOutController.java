/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycrm.controller.rest;

import com.mycrm.controller.*;
import com.mycrm.dao.BookingDAO;
import com.mycrm.dao.ContactDAO;
import com.mycrm.dao.RoleDAO;
import com.mycrm.dao.RoomDAO;
import com.mycrm.dao.TempleDAO;
import com.mycrm.domain.CheckinHistory;
import com.mycrm.domain.Monk;
import com.mycrm.domain.Register;
import com.mycrm.domain.Room;
import com.mycrm.domain.RoomName;
import com.mycrm.domain.Temple;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;
import javax.crypto.Cipher;
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import org.apache.commons.codec.binary.Hex;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author JAME
 */
@Controller
@RequestMapping(value = "/api")
public class RestCheckOutController {
   
    @Autowired
    ContactDAO contactDAO;
    
    final String secret_key = "J9rRf0Hn9qlh2ZEVjtUJ";
    
    @Autowired
    RoomDAO roomDAO;

    @Autowired
    TempleDAO templeDAO;

    @Autowired
    BookingDAO bookingDAO;

    @Autowired
    RoleDAO roleDAO;


  
    @RequestMapping(value = "/checkout/obj", method = RequestMethod.POST)
    public ResponseEntity<Vector> registerCreateNow(@RequestParam(value = "personID", required = false) String personID,
            @RequestParam(value = "templeID", required = false) String templeID, @RequestParam(value = "hash", required = false) String hash) throws ParseException {
        Vector v = new Vector();
        Map<String, Object> obj = new HashMap<String, Object>();
        Register contact = new Register();
        CheckinHistory history = null;
        
        // status
        // 0 = Data Invalid
        // 1 = No user
        // 2 = No booking
        // 3 = Check out success
        
            boolean sumMD5 = false;
            sumMD5 = validateHash(secret_key, personID+templeID, hash);
            if (sumMD5 == false) {
                obj.put("status", 0);
                obj.put("desc", "Data Invalid");
                v.add(obj);
                return new ResponseEntity<Vector>(v, HttpStatus.OK);
            }
            
        try {
            String encryptPersonIDString = RegisterController.encrypt(personID, HomeController.getKey_hash_profile());
            contact = contactDAO.getContactByPersonID(encryptPersonIDString);

        } catch (Exception e) {
                System.out.println("Error =" +e.getMessage());
                obj.put("status", 1);
                obj.put("desc", "No user");
                v.add(obj);
                return new ResponseEntity<Vector>(v, HttpStatus.OK);
        }
           
        try {
            history = bookingDAO.getContactCheckOut(contact.getReg_id());
            Calendar c = Calendar.getInstance();
            Date createDate = c.getTime();
            history.setStatus("เช็คเอ๊า");
            history.setCheck_out(createDate);
            bookingDAO.updateCheckOut(history);
            
        } catch (Exception e) {
            System.out.println("Error =" +e.getMessage());
            obj.put("status", 2);
            obj.put("desc", "No booking");
            v.add(obj);
            return new ResponseEntity<Vector>(v, HttpStatus.OK);     
        }
        obj.put("status", 3);
        obj.put("desc", "Check out success");
        v.add(obj);    
        return new ResponseEntity<Vector>(v, HttpStatus.OK);

    }
       
    // Key
    private static SecretKeySpec secretKey;

    private static byte[] key;

    public static void setKey(String myKey) {
        MessageDigest sha = null;
        try {
            key = myKey.getBytes("UTF-8");
            sha = MessageDigest.getInstance("SHA-1");
            key = sha.digest(key);
            key = Arrays.copyOf(key, 16);
            secretKey = new SecretKeySpec(key, "AES");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    public static String encrypt(String strToEncrypt, String secret) {
        try {
            setKey(secret);
            Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
            cipher.init(Cipher.ENCRYPT_MODE, secretKey);
            return Base64.getEncoder().encodeToString(cipher.doFinal(strToEncrypt.getBytes("UTF-8")));
        } catch (Exception e) {
            System.out.println("Error while encrypting: " + e.toString());
        }
        return null;
    }

    public static String decrypt(String strToDecrypt, String secret) {
        try {
            setKey(secret);
            Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5PADDING");
            cipher.init(Cipher.DECRYPT_MODE, secretKey);
            return new String(cipher.doFinal(Base64.getDecoder().decode(strToDecrypt)));
        } catch (Exception e) {
            System.out.println("Error while decrypting: " + e.toString());
        }
        return null;
    }
    
    
    
    public static Boolean validateHash(String key, String data, String hash) {
        System.out.println("data c# =  "+data);
        System.out.println("hash c# =  "+hash);        
        String _hash = calculateHMAC(key, data);
        System.out.println("hash java =  "+_hash);
        boolean a = (hash.equals(_hash));
        return a;
    }

    public static String calculateHMAC(String key, String data) {
        try {
            Mac hmac = Mac.getInstance("HmacSHA256");
            SecretKeySpec secret_key = new SecretKeySpec(key.getBytes("UTF-8"), "HmacSHA256");
            hmac.init(secret_key);
            return new String(Hex.encodeHex(hmac.doFinal(data.getBytes("UTF-8"))));
        } catch (Exception e) {

            throw new RuntimeException(e);
        }
    }




    
}
