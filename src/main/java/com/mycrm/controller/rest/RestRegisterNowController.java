
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycrm.controller.rest;

import com.mycrm.controller.*;
import com.mycrm.dao.ContactDAO;
import com.mycrm.domain.Register;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.util.Arrays;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;
import javax.crypto.Cipher;
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import org.apache.commons.codec.binary.Hex;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author JAME
 */
@Controller
@RequestMapping(value = "/api")
public class RestRegisterNowController {

    @Autowired
    ContactDAO contactDAO;

    final String secret_key = "J9rRf0Hn9qlh2ZEVjtUJ";

    @RequestMapping(value = "/register/obj", method = RequestMethod.POST)
    public ResponseEntity<Vector> registerCreateNow(Register register, @RequestParam(value = "hash", required = false) String hash) throws ParseException {
        Vector v = new Vector();
        Map<String, Object> obj = new HashMap<String, Object>();
        try {
            System.out.println("getFirstname = " + register.getFirstname());
            System.out.println("getLastname = " + register.getLastname());
            System.out.println("getPersonid = " + register.getPersonid());
            System.out.println("getRoyal = " + register.getRoyal());
            System.out.println("getNationality = " + register.getNationality());
            System.out.println("getBd_birth = " + register.getBd_birth());
            System.out.println("getGender = " + register.getGender());
            System.out.println("getAddress = " + register.getAddress());
            System.out.println("getVillage = " + register.getVillage());
            System.out.println("getDistrict = " + register.getDistrict());
            System.out.println("getCity = " + register.getCity());
            System.out.println("getProvince = " + register.getProvince());
            System.out.println("getPostalcode = " + register.getPostalcode());
            System.out.println("getNametitle = " + register.getNametitle());
            System.out.println("getTemple_id = " + register.getTemple_id());

            // Status number
            // 0 = Data Invalid
            // 1 = User is delete
            // 2 = User duplicate
            // 3 = Resgister Success
            // 4 = Error
            
            boolean sumMD5 = false;
            sumMD5 = validateHash(secret_key, register.getFirstname() + register.getLastname() + register.getPersonid() + register.getRoyal() + register.getNationality() + register.getBd_birth() + register.getGender() + register.getAddress() + register.getVillage() + register.getDistrict() + register.getCity() + register.getProvince() + register.getPostalcode() + register.getNametitle() + register.getTemple_id(), hash);
            if (sumMD5 == false) {
                obj.put("status", 0);
                obj.put("desc", "Data Invalid");
                v.add(obj);
                return new ResponseEntity<Vector>(v, HttpStatus.OK);
            }
            final String secretKey = "E0B9970BB727ADBC9F2B61969865F3F1";
            register.setPersonid(RegisterController.encrypt(register.getPersonid(), secretKey));
            int countPersonID = contactDAO.countPersonID(register.getPersonid());
            if (countPersonID == 0) {
                System.out.println("ID Card OK");
            } else if (countPersonID == 1) {
                int is_delete = 0;
                is_delete = contactDAO.findPersonidIsdelete(register.getPersonid());
                if (is_delete == 1) {
                    v.add(obj);
                    obj.put("status", 1); // contact to programer
                    obj.put("desc", "User is delete");
                    return new ResponseEntity<Vector>(v, HttpStatus.OK);
                } else {
                    v.add(obj);
                    obj.put("status", 2);
                    obj.put("desc", "User Duplicate");
                    return new ResponseEntity<Vector>(v, HttpStatus.OK);
                }
            }
            register.setAddress(RegisterController.encrypt(register.getAddress(), secretKey));
            register.setDistrict(RegisterController.encrypt(register.getDistrict(), secretKey));
            register.setEmail("contact@email.com");
            register.setTelephone("0");
            register.setPassword("1234");
            register.setRepassword("1234");
            contactDAO.insertContactIDCard(register);
            obj.put("status", 3);
            obj.put("desc", "Resgister Success");
        } catch (Exception e) {
            System.out.println("Error =" + e.getMessage());
            obj.put("status", 4);
            obj.put("desc", "Error");
        }

        v.add(obj);
        return new ResponseEntity<Vector>(v, HttpStatus.OK);

    }

    // Key
    private static SecretKeySpec secretKey;

    private static byte[] key;

    public static void setKey(String myKey) {
        MessageDigest sha = null;
        try {
            key = myKey.getBytes("UTF-8");
            sha = MessageDigest.getInstance("SHA-1");
            key = sha.digest(key);
            key = Arrays.copyOf(key, 16);
            secretKey = new SecretKeySpec(key, "AES");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    public static String encrypt(String strToEncrypt, String secret) {
        try {
            setKey(secret);
            Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
            cipher.init(Cipher.ENCRYPT_MODE, secretKey);
            return Base64.getEncoder().encodeToString(cipher.doFinal(strToEncrypt.getBytes("UTF-8")));
        } catch (Exception e) {
            System.out.println("Error while encrypting: " + e.toString());
        }
        return null;
    }

    public static String decrypt(String strToDecrypt, String secret) {
        try {
            setKey(secret);
            Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5PADDING");
            cipher.init(Cipher.DECRYPT_MODE, secretKey);
            return new String(cipher.doFinal(Base64.getDecoder().decode(strToDecrypt)));
        } catch (Exception e) {
            System.out.println("Error while decrypting: " + e.toString());
        }
        return null;
    }

    public static Boolean validateHash(String key, String data, String hash) {
        System.out.println("data c# =  " + data);
        System.out.println("hash c# =  " + hash);
        String _hash = calculateHMAC(key, data);
        System.out.println("hash java =  " + _hash);
        return (hash.equals(_hash));
    }

    public static String calculateHMAC(String key, String data) {
        try {
            Mac hmac = Mac.getInstance("HmacSHA256");
            SecretKeySpec secret_key = new SecretKeySpec(key.getBytes("UTF-8"), "HmacSHA256");
            hmac.init(secret_key);
            return new String(Hex.encodeHex(hmac.doFinal(data.getBytes("UTF-8"))));
        } catch (Exception e) {

            throw new RuntimeException(e);
        }
    }

}
