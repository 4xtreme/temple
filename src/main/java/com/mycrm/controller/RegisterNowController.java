
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycrm.controller;

import com.mycrm.common.RoleCheck;
import com.mycrm.dao.BookingDAO;
import java.util.List;
import com.mycrm.dao.ContactDAO;
import com.mycrm.dao.RoleDAO;
import com.mycrm.dao.RoomDAO;
import com.mycrm.dao.StaffDAO;
import com.mycrm.dao.TempleDAO;
import com.mycrm.dao.VipassanaDAO;
import com.mycrm.domain.Monk;
import com.mycrm.domain.MonkDate;
import com.mycrm.domain.Register;
import com.mycrm.domain.RoleItem;
import com.mycrm.domain.Room;
import com.mycrm.domain.RoomName;
import com.mycrm.domain.Temple;
import com.mycrm.domain.Vipassana;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import org.springframework.stereotype.Component;
import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author JAME
 */
@Controller
@Component
public class RegisterNowController {

    int PER_PAGE = 5;

    @Autowired
    ContactDAO contactDAO;

    Register register;

    @Autowired
    RoleDAO roleDAO;

    @Autowired
    TempleDAO templeDAO;

    @Autowired
    RoomDAO roomDAO;

    @Autowired
    BookingDAO bookingDAO;

    @Autowired
    StaffDAO staffDAO;

    @Autowired
    HomeController homeController;

    @Autowired
    VipassanaDAO vipassanaDAO;

    @InitBinder
    public void initBinder(WebDataBinder binder) {

        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        sdf.setLenient(true);
        binder.registerCustomEditor(Date.class, new CustomDateEditor(sdf, true));

    }

    @RequestMapping(value = "/regcreatenow", method = RequestMethod.GET)
    public String registerNow(Model model) {
        String page_Create = "OK";
        String page_Delete = "OK";
        String page_Edit = "OK";
        try {
            if (roleDAO.pageView("member_page", HomeController.getRole_id()) == false) {
                return "redirect:/impervious";
            }

        } catch (Exception e) {
            return "redirect:/";
        }

        if (roleDAO.pageCreate("member_page", HomeController.getRole_id()) == false) {
            page_Create = null;
        }

        if (roleDAO.pageEdit("member_page", HomeController.getRole_id()) == false) {
            page_Edit = null;
        }
        if (roleDAO.pageDelete("member_page", HomeController.getRole_id()) == false) {
            page_Delete = null;
        }
        String page = "0";
        String personal_cardid = "";
        int page_count = 0;
        page_count = 0;
        List<RoomName> rooms = new ArrayList<RoomName>();
        List<Room> builder = new ArrayList<Room>();
        List<Monk> monklist = new ArrayList<Monk>();
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy", new Locale("th", "TH"));
        Date today = Calendar.getInstance().getTime();
        String dateval = df.format(today);
        // getMonkByTemple
        monklist = contactDAO.getMonkByTemple(HomeController.getTemple_id());

        List<RoleItem> roleItems = new ArrayList<>();
        roleItems = roleDAO.getRoleITem(HomeController.getRole_id());
        Boolean statusadmin = RoleCheck.checkAdmin(roleItems);
        if (statusadmin == true) {
            model.addAttribute("roleadmin", "admin");
        } else if (statusadmin == false) {
            model.addAttribute("roleadmin", "user");
        }
        model = homeController.getRole(model);
        model.addAttribute("monklist", monklist);
        model.addAttribute("builder", builder);
        model.addAttribute("rooms", rooms);
        model.addAttribute("page_count", page_count);
        model.addAttribute("page", page);
        model.addAttribute("startdate", "");
        model.addAttribute("enddate", "");
        model.addAttribute("dateval", dateval);
        model.addAttribute("personal_cardid", personal_cardid);
        model.addAttribute("status", null);
        model.addAttribute("disease", "");
        model.addAttribute("mentalwhere", "");
        model.addAttribute("mentaltime", "");
        model.addAttribute("mentalyear", "");
        model.addAttribute("Register", new Register());

        return "contact/regnow";
    }

    @RequestMapping(value = "/regcreatenow", method = RequestMethod.POST)
    public String registerCreateNow(Model model, @Valid @ModelAttribute("Register") Register register, BindingResult result,
            @RequestParam(value = "disease", required = false) String disease,
            @RequestParam(value = "mentalwhere", required = false) String mentalwhere,
            @RequestParam(value = "mentaltime", required = false) String mentaltime,
            @RequestParam(value = "mentalyear", required = false) String mentalyear) throws ParseException {
        System.out.println("reg : " + register.toString());
        List<Temple> temple = new ArrayList<Temple>();
        temple = templeDAO.getALL();
        model.addAttribute("temple", temple);
        String old_health = register.getHealth();
        String old_mental = register.getMental();

        //---------set Register Model------------------------------------
        if ("nostrong".equals(register.getHealth())) {
            register.setHealth(disease);
        }
        if ("เคยเข้าบำบัด".equals(register.getMental())) {
            register.setMental(register.getMental() + " " + mentalwhere + " " + mentaltime);
        }
        if ("ไม่ได้ทานยามานาน".equals(register.getMental())) {
            register.setMental(register.getMental() + " " + mentalyear);
        }
        System.out.println("after set: " + register.toString());
        String old_personid = register.getPersonid();
        register.setPersonid(RegisterController.encrypt(register.getPersonid(), HomeController.getKey_hash_profile()));
        //----------checkPersonID-------------------------------
        int countPersonID = contactDAO.countPersonID(register.getPersonid());
        List<RoleItem> roleItems = new ArrayList<>();
        roleItems = roleDAO.getRoleITem(HomeController.getRole_id());
        Boolean statusadmin = RoleCheck.checkAdmin(roleItems);
        if (statusadmin == true) {
            model.addAttribute("roleadmin", "admin");
        } else if (statusadmin == false) {
            model.addAttribute("roleadmin", "user");
        }
        model = homeController.getRole(model);
        if (countPersonID == 0) {
            System.out.println("This personID can Regis");
        } else {
            register.setPersonid(old_personid);
            register.setHealth(old_health);
            register.setMental(old_mental);
            model.addAttribute("disease", disease);
            model.addAttribute("mentalwhere", mentalwhere);
            model.addAttribute("mentaltime", mentaltime);
            model.addAttribute("mentalyear", mentalyear);
            model.addAttribute("invalidpersonID", "x");
            return "contact/regnow";
        }

        //-----------Validate-----------------------------------
        if (result.hasErrors()) {
            System.out.println("Reg:" + register.toString());
            System.out.println("hasErrors =  " + result.toString());
            register.setPersonid(old_personid);
            register.setHealth(old_health);
            register.setMental(old_mental);
            model.addAttribute("disease", disease);
            model.addAttribute("mentalwhere", mentalwhere);
            model.addAttribute("mentaltime", mentaltime);
            model.addAttribute("mentalyear", mentalyear);
            return "contact/regnow";
        }

        if (register.getGender_text().equals("ชาย") || register.getGender_text().equals("ต่างชาติชาย") || register.getGender_text().equals("ภิกษุ") || register.getGender_text().equals("สามเณร")) {
            register.setGender("ชาย");
        } else {
            register.setGender("หญิง");
        }
        register.setVillage("");
        register.setAddress(RegisterController.encrypt(register.getAddress(), HomeController.getKey_hash_profile()));
        register.setDistrict(RegisterController.encrypt(register.getDistrict(), HomeController.getKey_hash_profile()));
        register.setTemple_id(HomeController.getTemple_id());
        contactDAO.insertContactNow(register);

        // model booking room
        try {
            if (roleDAO.pageCreate("temple_page", HomeController.getRole_id()) == false) {
                return "redirect:/impervious";
            }

        } catch (Exception e) {
            return "redirect:/";
        }
        String page = "0";

        int page_count = 0;
        page_count = 0;
        List<RoomName> rooms = new ArrayList<RoomName>();
        List<Room> builder = new ArrayList<Room>();
        List<Monk> monklist = new ArrayList<Monk>();
        List<Vipassana> vipassanas = new ArrayList<>();
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm", Locale.ENGLISH);
        Date today = Calendar.getInstance().getTime();
        String dateval = df.format(today);
        // getMonkByTemple
        monklist = contactDAO.getMonkByTemple(HomeController.getTemple_id());
        List<MonkDate> monkDate = new ArrayList<MonkDate>();
        vipassanas = vipassanaDAO.findAll(HomeController.getTemple_id(), "0", 1000);
        for (Vipassana add : vipassanas) {
            monkDate.add(new MonkDate(add.getFirstname(), bookingDAO.getCountMonkDate(add.getId(), getDateCompare(0)), bookingDAO.getCountMonkDate(add.getId(), getDateCompare(1)), bookingDAO.getCountMonkDate(add.getId(), getDateCompare(2)), bookingDAO.getCountMonkDate(add.getId(), getDateCompare(3)), bookingDAO.getCountMonkDate(add.getId(), getDateCompare(4)), bookingDAO.getCountMonkDate(add.getId(), getDateCompare(5)), bookingDAO.getCountMonkDate(add.getId(), getDateCompare(6)), bookingDAO.getCountMonkDate(add.getId(), getDateCompare(7)), bookingDAO.getCountMonkDate(add.getId(), getDateCompare(8)), bookingDAO.getCountMonkDate(add.getId(), getDateCompare(9))));
        }
        ArrayList<Integer> count_array = new ArrayList<Integer>();
        for (int i = 1; i <= 30; i++) {
            count_array.add(i);
        }

        ArrayList<String> date_table = new ArrayList<String>();
        for (int i = 0; i <= 9; i++) {
            Date dt = new Date();
            Calendar c = Calendar.getInstance();
            c.setTime(dt);
            c.add(Calendar.DATE, i);
            dt = c.getTime();
            date_table.add(dateThaiFormat(dt));
        }

        model.addAttribute("vipassanas", vipassanas);
        model.addAttribute("monklist", monklist);
        model.addAttribute("builder", builder);
        model.addAttribute("rooms", rooms);
        model.addAttribute("page_count", page_count);
        model.addAttribute("page", page);
        model.addAttribute("startdate", "");
        model.addAttribute("enddate", "");
        model.addAttribute("dateval", dateval);
        model.addAttribute("personal_cardid", old_personid);
        model.addAttribute("status", null);
        model.addAttribute("monk_date", monkDate);
        model.addAttribute("count_array", count_array);
        model.addAttribute("date_table", date_table);
        return "bookingroom/bookingroom";

    }

    // Key
    private static SecretKeySpec secretKey;

    private static byte[] key;

    public static void setKey(String myKey) {
        MessageDigest sha = null;
        try {
            key = myKey.getBytes("UTF-8");
            sha = MessageDigest.getInstance("SHA-1");
            key = sha.digest(key);
            key = Arrays.copyOf(key, 16);
            secretKey = new SecretKeySpec(key, "AES");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    public static String encrypt(String strToEncrypt, String secret) {
        try {
            setKey(secret);
            Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
            cipher.init(Cipher.ENCRYPT_MODE, secretKey);
            return Base64.getEncoder().encodeToString(cipher.doFinal(strToEncrypt.getBytes("UTF-8")));
        } catch (Exception e) {
            System.out.println("Error while encrypting: " + e.toString());
        }
        return null;
    }

    public static String decrypt(String strToDecrypt, String secret) {
        try {
            setKey(secret);
            Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5PADDING");
            cipher.init(Cipher.DECRYPT_MODE, secretKey);
            return new String(cipher.doFinal(Base64.getDecoder().decode(strToDecrypt)));
        } catch (Exception e) {
            System.out.println("Error while decrypting: " + e.toString());
        }
        return null;
    }

    @RequestMapping(value = "/regcreatenowcard", method = RequestMethod.POST)
    public String regcreatenowcard(Model model) {
        String page = "0";
        String personal_cardid = "";
        int page_count = 0;
        page_count = 0;
        List<RoomName> rooms = new ArrayList<RoomName>();
        List<Room> builder = new ArrayList<Room>();
        List<Monk> monklist = new ArrayList<Monk>();
        DateFormat df = new SimpleDateFormat("dd-MM-yyyy HH:mm", Locale.ENGLISH);
        Date today = Calendar.getInstance().getTime();
        String dateval = df.format(today);
        // getMonkByTemple
        monklist = contactDAO.getMonkByTemple(HomeController.getTemple_id());
        Register reg = null;
        try {
            reg = contactDAO.getRegCard(HomeController.getTemple_id());
            String decryptAddressString = RegisterController.decrypt(reg.getAddress(), HomeController.getKey_hash_profile());
            String decryptPersonIDString = RegisterController.decrypt(reg.getPersonid(), HomeController.getKey_hash_profile());
            String decryptDistrictString = RegisterController.decrypt(reg.getDistrict(), HomeController.getKey_hash_profile());
            // System.out.println("New Person Id " + decryptPersonIDString);
            // System.out.println("New Address " + decryptAddressString);
            // System.out.println("New District " + decryptDistrictString);
            reg.setAddress(decryptAddressString);
            reg.setDistrict(decryptDistrictString);
            reg.setPersonid(decryptPersonIDString);

        } catch (Exception e) {
            System.out.println("Error =" + e.getMessage());
        }

        if (reg == null) {
            return "redirect:/regcreatenow";
        }

        List<RoleItem> roleItems = new ArrayList<>();
        roleItems = roleDAO.getRoleITem(HomeController.getRole_id());
        Boolean statusadmin = RoleCheck.checkAdmin(roleItems);
        if (statusadmin == true) {
            model.addAttribute("roleadmin", "admin");
        } else if (statusadmin == false) {
            model.addAttribute("roleadmin", "user");
        }
        model = homeController.getRole(model);
        model.addAttribute("monklist", monklist);
        model.addAttribute("builder", builder);
        model.addAttribute("rooms", rooms);
        model.addAttribute("page_count", page_count);
        model.addAttribute("page", page);
        model.addAttribute("startdate", "");
        model.addAttribute("enddate", "");
        model.addAttribute("dateval", dateval);
        model.addAttribute("personal_cardid", personal_cardid);
        model.addAttribute("status", null);
        model.addAttribute("Register", reg);
        return "contact/regnowcard";
    }

    @RequestMapping(value = "/updateregcard", method = RequestMethod.POST)
    public String updateregcard(Model model, @Valid @ModelAttribute("Register") Register register, BindingResult result) throws ParseException {

        List<RoleItem> roleItems = new ArrayList<>();
        roleItems = roleDAO.getRoleITem(HomeController.getRole_id());
        Boolean statusadmin = RoleCheck.checkAdmin(roleItems);
        if (statusadmin == true) {
            model.addAttribute("roleadmin", "admin");
        } else if (statusadmin == false) {
            model.addAttribute("roleadmin", "user");
        }
        model = homeController.getRole(model);
        if (result.hasErrors()) {

            return "contact/regnowcard";
        }

        if (register.getGender_text().equals("ชาย") || register.getGender_text().equals("ต่างชาติชาย") || register.getGender_text().equals("ภิกษุ") || register.getGender_text().equals("สามเณร")) {
            register.setGender("ชาย");
        } else {
            register.setGender("หญิง");
        }
        String old_personid = register.getPersonid();
        register.setVillage("");
        register.setPersonid(RegisterController.encrypt(register.getPersonid(), HomeController.getKey_hash_profile()));
        register.setAddress(RegisterController.encrypt(register.getAddress(), HomeController.getKey_hash_profile()));
        register.setDistrict(RegisterController.encrypt(register.getDistrict(), HomeController.getKey_hash_profile()));
        register.setTemple_id(HomeController.getTemple_id());
        contactDAO.updateReg(register);

        // model booking room
        try {
            if (roleDAO.pageCreate("temple_page", HomeController.getRole_id()) == false) {
                return "redirect:/impervious";
            }

        } catch (Exception e) {
            return "redirect:/";
        }
        String page = "0";

        int page_count = 0;
        page_count = 0;
        List<RoomName> rooms = new ArrayList<RoomName>();
        List<Room> builder = new ArrayList<Room>();
        List<Monk> monklist = new ArrayList<Monk>();
        List<Vipassana> vipassanas = new ArrayList<>();
        DateFormat df = new SimpleDateFormat("dd-MM-yyyy HH:mm", Locale.ENGLISH);
        Date today = Calendar.getInstance().getTime();
        String dateval = df.format(today);
        // getMonkByTemple
        monklist = contactDAO.getMonkByTemple(HomeController.getTemple_id());
        List<MonkDate> monkDate = new ArrayList<MonkDate>();
        vipassanas = vipassanaDAO.findAll(HomeController.getTemple_id(), "0", 1000);
        for (Vipassana add : vipassanas) {
            monkDate.add(new MonkDate(add.getFirstname(), bookingDAO.getCountMonkDate(add.getId(), getDateCompare(0)), bookingDAO.getCountMonkDate(add.getId(), getDateCompare(1)), bookingDAO.getCountMonkDate(add.getId(), getDateCompare(2)), bookingDAO.getCountMonkDate(add.getId(), getDateCompare(3)), bookingDAO.getCountMonkDate(add.getId(), getDateCompare(4)), bookingDAO.getCountMonkDate(add.getId(), getDateCompare(5)), bookingDAO.getCountMonkDate(add.getId(), getDateCompare(6)), bookingDAO.getCountMonkDate(add.getId(), getDateCompare(7)), bookingDAO.getCountMonkDate(add.getId(), getDateCompare(8)), bookingDAO.getCountMonkDate(add.getId(), getDateCompare(9))));
        }
        ArrayList<Integer> count_array = new ArrayList<Integer>();
        for (int i = 1; i <= 30; i++) {
            count_array.add(i);
        }

        ArrayList<String> date_table = new ArrayList<String>();
        for (int i = 0; i <= 9; i++) {
            Date dt = new Date();
            Calendar c = Calendar.getInstance();
            c.setTime(dt);
            c.add(Calendar.DATE, i);
            dt = c.getTime();
            date_table.add(dateThaiFormat(dt));
        }

        model.addAttribute("vipassanas", vipassanas);
        model.addAttribute("monklist", monklist);
        model.addAttribute("builder", builder);
        model.addAttribute("rooms", rooms);
        model.addAttribute("page_count", page_count);
        model.addAttribute("page", page);
        model.addAttribute("startdate", "");
        model.addAttribute("enddate", "");
        model.addAttribute("dateval", dateval);
        model.addAttribute("personal_cardid", old_personid);
        model.addAttribute("status", null);
        model.addAttribute("monk_date", monkDate);
        model.addAttribute("count_array", count_array);
        model.addAttribute("date_table", date_table);

        return "bookingroom/bookingroom";

    }

    public static String dateThaiFormat(Date dateformat) {
        DateFormat df = new SimpleDateFormat("dd-MM-yyyy", new Locale("th", "TH"));
        String datetime = df.format(dateformat);
        return datetime;
    }

    public static String dateFormatSQL(Date dateformat) {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        String datetime = df.format(dateformat);
        return datetime;
    }

    public static String getDateCompare(int number) {
        Date d_sql = new Date();
        Calendar c = Calendar.getInstance();
        c.setTime(d_sql);
        c.add(Calendar.DATE, number);
        d_sql = c.getTime();
        return dateFormatSQL(d_sql);
    }

}
