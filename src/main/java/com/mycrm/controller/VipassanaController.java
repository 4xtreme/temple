/*
 * Copy Right Pay Enterprise Co.,Ltd.
 */
package com.mycrm.controller;

import com.mycrm.common.RoleCheck;
import com.mycrm.dao.RoleDAO;
import com.mycrm.dao.VipassanaDAO;
import com.mycrm.domain.RoleItem;
import com.mycrm.domain.Vipassana;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import javax.validation.Valid;
import org.apache.commons.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author pop
 */
@Controller
public class VipassanaController {

    @Autowired
    private VipassanaDAO vipassanaDAO;

    @Autowired
    HomeController homeController;

    @Autowired
    RoleDAO roleDAO;

    int PER_PAGE = 5;

    @RequestMapping(value = "/vipassana", method = RequestMethod.GET)
    public String vipassana(Model model, @RequestParam(value = "page", required = false) String page) {
        List<Vipassana> vipassanas = new ArrayList<>();
        if (page == null || Integer.parseInt(page) < 0) {
            page = "0";
        }
        int countRow = 0;
        int page_count = 0;
        int temple_id = HomeController.getTemple_id();
        countRow = vipassanaDAO.countRows(temple_id);
        Double pagecountDB = Math.ceil((double) countRow / (double) PER_PAGE);
        page_count = pagecountDB.intValue();
        vipassanas = vipassanaDAO.findAll(HomeController.getTemple_id(), page, PER_PAGE);
        if (Integer.parseInt(page) > 0) {
            model.addAttribute("page_priv", true);

        }
        if (Integer.parseInt(page) < (page_count - 1)) {
            model.addAttribute("page_next", true);
        }
        if (page_count >= 10) {
            page_count = 10;
        }
        List<String> images = new ArrayList<>();
        for (Vipassana vipassana : vipassanas) {
            try {
                images.add(new String(Base64.encodeBase64(vipassana.getImage()), "UTF-8"));
            } catch (UnsupportedEncodingException ex) {
                System.out.println("Error:" + ex.getMessage());
            }
        }

        List<RoleItem> roleItems = new ArrayList<>();
        roleItems = roleDAO.getRoleITem(HomeController.getRole_id());
        Boolean statusadmin = RoleCheck.checkAdmin(roleItems);
        if (statusadmin == true) {
            model.addAttribute("roleadmin", "admin");
        } else if (statusadmin == false) {
            model.addAttribute("roleadmin", "user");
        }
        model = homeController.getRole(model);
        model.addAttribute("images", images);
        model.addAttribute("vipassanas", vipassanas);
        model.addAttribute("page_count", page_count);
        model.addAttribute("page", page);
        return "vipassana/vipassana";
    }

    @RequestMapping(value = "/addvipassana", method = RequestMethod.GET)
    public String addVipassana(Model model) {

        List<RoleItem> roleItems = new ArrayList<>();
        roleItems = roleDAO.getRoleITem(HomeController.getRole_id());
        Boolean statusadmin = RoleCheck.checkAdmin(roleItems);
        if (statusadmin == true) {
            model.addAttribute("roleadmin", "admin");
        } else if (statusadmin == false) {
            model.addAttribute("roleadmin", "user");
        }
        model = homeController.getRole(model);
        model.addAttribute("vipassana", new Vipassana());
        return "vipassana/add_vipassana";
    }

    @RequestMapping(value = "/addvipassana", method = RequestMethod.POST)
    public String addVipassana(Model model, @Valid @ModelAttribute("vipassana") Vipassana vipassana, BindingResult result,
            @RequestParam(value = "picture") MultipartFile image) {
        try {
            vipassana.setImage(image.getBytes());

            if (result.hasErrors()) {
                System.out.println("Error:" + result.toString());
                List<RoleItem> roleItems = new ArrayList<>();
                roleItems = roleDAO.getRoleITem(HomeController.getRole_id());
                Boolean statusadmin = RoleCheck.checkAdmin(roleItems);
                if (statusadmin == true) {
                    model.addAttribute("roleadmin", "admin");
                } else if (statusadmin == false) {
                    model.addAttribute("roleadmin", "user");
                }
                model = homeController.getRole(model);
                model.addAttribute("vipassana", vipassana);
                return "vipassana/add_vipassana";
            }

        } catch (Exception e) {
            System.out.println("Error:" + e.getMessage());
        }
        vipassana.setTemple_id(HomeController.getTemple_id());
        int id = vipassanaDAO.insert(vipassana);
        return "redirect:/vipassana";
    }

    @RequestMapping(value = "/editvipassana", method = RequestMethod.GET)
    public String editVipassana(Model model, @RequestParam("id") int id) {
        Vipassana vipassana = new Vipassana();
        vipassana = vipassanaDAO.findById(id);

        List<RoleItem> roleItems = new ArrayList<>();
        roleItems = roleDAO.getRoleITem(HomeController.getRole_id());
        Boolean statusadmin = RoleCheck.checkAdmin(roleItems);
        if (statusadmin == true) {
            model.addAttribute("roleadmin", "admin");
        } else if (statusadmin == false) {
            model.addAttribute("roleadmin", "user");
        }
        model = homeController.getRole(model);
        model.addAttribute("vipassana", vipassana);
        return "vipassana/edit_vipassana";
    }

    @RequestMapping(value = "/editvipassana", method = RequestMethod.POST)
    public String editVipassana(Model model, @Valid @ModelAttribute("vipassana") Vipassana vipassana, BindingResult result,
            @RequestParam(value = "picture") MultipartFile image) {

        List<RoleItem> roleItems = new ArrayList<>();
        roleItems = roleDAO.getRoleITem(HomeController.getRole_id());
        Boolean statusadmin = RoleCheck.checkAdmin(roleItems);
        if (statusadmin == true) {
            model.addAttribute("roleadmin", "admin");
        } else if (statusadmin == false) {
            model.addAttribute("roleadmin", "user");
        }
        model = homeController.getRole(model);
        if (result.hasErrors()) {
            return "vipassana/edit_vipassana";
        }
        if (image.isEmpty()) {
            Vipassana oldVipassana = new Vipassana();
            oldVipassana = vipassanaDAO.findById(vipassana.getId());
            vipassana.setImage(oldVipassana.getImage());
        } else {
            try {
                vipassana.setImage(image.getBytes());
            } catch (Exception e) {
                System.out.println("Error:" + e.getMessage());
            }
        }
        vipassanaDAO.update(vipassana);
        return "redirect:/vipassana";
    }

    @RequestMapping(value = "/deletevipassana", method = RequestMethod.POST)
    public String deleteVipassana(@RequestParam("id") int id) {
        vipassanaDAO.delete(id);
        return "redirect:/vipassana";
    }
    
    @RequestMapping(value = "/searchvipassana", method = RequestMethod.POST)
    public String searchVipassana(Model model, @RequestParam(value = "page", required = false) String page,@RequestParam("search") String search){
        List<Vipassana> searchList = new ArrayList<Vipassana>();
        if (page == null || Integer.parseInt(page) < 0) {
            page = "0";
        }
        int countRow = 0;
        int page_count = 0;
        int temple_id = HomeController.getTemple_id();
        countRow = vipassanaDAO.countSearchRows(temple_id,search);
        Double pagecountDB = Math.ceil((double) countRow / (double) PER_PAGE);
        page_count = pagecountDB.intValue();
        searchList = vipassanaDAO.search(HomeController.getTemple_id(), page, PER_PAGE, search);
        if (Integer.parseInt(page) > 0) {
            model.addAttribute("page_priv", true);

        }
        if (Integer.parseInt(page) < (page_count - 1)) {
            model.addAttribute("page_next", true);
        }
        if (page_count >= 10) {
            page_count = 10;
        }
        
        List<RoleItem> roleItems = new ArrayList<>();
        roleItems = roleDAO.getRoleITem(HomeController.getRole_id());
        Boolean statusadmin = RoleCheck.checkAdmin(roleItems);
        if (statusadmin == true) {
            model.addAttribute("roleadmin", "admin");
        } else if (statusadmin == false) {
            model.addAttribute("roleadmin", "user");
        }
        
        List<String> images = new ArrayList<>();
        for (Vipassana vipassana : searchList) {
            try {
                images.add(new String(Base64.encodeBase64(vipassana.getImage()), "UTF-8"));
            } catch (UnsupportedEncodingException ex) {
                System.out.println("Error:" + ex.getMessage());
            }
        }
        model = homeController.getRole(model);
        System.out.println("searchList : "+searchList);
        model.addAttribute("images", images);
        model.addAttribute("vipassanas", searchList);
        model.addAttribute("page_count", page_count);
        model.addAttribute("page", page);
        return "vipassana/search_vipassana";
    }
}
