/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycrm.controller;

import com.mycrm.dao.ContactDAO;

import com.mycrm.domain.Register;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;

import org.springframework.stereotype.Controller;

import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class ForgotPasswordController {
    @Autowired
    ContactDAO contactDAO;
    @Autowired
    private MailSender mailSender;
    public void crunchifyReadyToSendEmail(String toAddress, String fromAddress, String subject, String msgBody) {

    SimpleMailMessage crunchifyMsg = new SimpleMailMessage();
    crunchifyMsg.setFrom(fromAddress);
    crunchifyMsg.setTo(toAddress);
    crunchifyMsg.setSubject(subject);
    crunchifyMsg.setText(msgBody);
    mailSender.send(crunchifyMsg);
   
} 
    
    
    @RequestMapping(value = "/forgotpassword", method = RequestMethod.GET)
    public String show(Model model){
    
        //crunchifyReadyToSendEmail("puwit.suriwongyai@gmail.com", "crmemailsender@gmail.com", "test", "test");
     
    return "forgotpassword/forgotpassword";
}
    
    @RequestMapping(value = "/sendmails", method = RequestMethod.POST)
    public String sendAndEmail(Model model,@RequestParam("email")String email){
        
        System.out.println("email before"+email);
        try{
        Register register = contactDAO.findemailInfo(email);
        System.out.println("Show Password : "+register.getPassword());        
        crunchifyReadyToSendEmail(email, "crmemailsender@gmail.com", "Your Password", "Your Username : "+email+"\nYour Password : "+register.getPassword());
        model.addAttribute("status", null);
        return "redirect:/"; 
        }catch(Exception e){
            model.addAttribute("status", "null");
           return "forgotpassword/forgotpassword"; 
        }     
    } 
}
