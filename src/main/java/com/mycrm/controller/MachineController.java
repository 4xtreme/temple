///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
//package com.mycrm.controller;
//
//import com.amazonaws.auth.BasicAWSCredentials;
//import com.amazonaws.regions.Region;
//import com.amazonaws.regions.Regions;
//import com.amazonaws.services.s3.AmazonS3;
//import com.amazonaws.services.s3.AmazonS3Client;
//import com.amazonaws.services.s3.model.CannedAccessControlList;
//import com.amazonaws.services.s3.model.GetObjectRequest;
//import com.amazonaws.services.s3.model.ObjectMetadata;
//import com.amazonaws.services.s3.model.PutObjectRequest;
//import com.amazonaws.services.s3.model.S3Object;
//import com.mycrm.common.Dateformat;
//import com.mycrm.dao.MachineDAO;
//
//import com.mycrm.dao.StaffDAO;
//
//import com.mycrm.dao.UserDAO;
//
//import com.mycrm.domain.Machine;
//import com.mycrm.domain.Staff;
//
//import com.mycrm.domain.User;
//
//import java.io.IOException;
//import java.text.ParseException;
//import java.text.SimpleDateFormat;
//import java.util.ArrayList;
//import java.util.Calendar;
//
//
//import java.util.Date;
//
//import java.util.List;
//import javax.validation.Valid;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.context.i18n.LocaleContextHolder;
//import org.springframework.stereotype.Controller;
//import org.springframework.ui.Model;
//import org.springframework.validation.BindingResult;
//import org.springframework.web.bind.annotation.ModelAttribute;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestMethod;
//import org.springframework.web.bind.annotation.RequestParam;
//import org.springframework.web.multipart.MultipartFile;
//
///**
// *
// * @author pop
// */
//@Controller
//public class MachineController {
//
// int PER_PAGE = 5;
// 
//
//    @Autowired
//    MachineDAO machineDAO;
//    
//
//      @Autowired
//    StaffDAO staffDAO;
//
//
//    @Autowired
//    UserDAO userDAO;
//
//
//    @RequestMapping(value = "/addmachine", method = RequestMethod.GET)
//    public String addMachine(Model model, @RequestParam(value = "page", required = false) String page) throws ParseException {
//
//        if (page == null || Integer.parseInt(page) < 0) {
//            page = "0";
//        }
//         List<Staff> staff = new ArrayList<Staff>();
//        staff = staffDAO.getStaffList(page, PER_PAGE);
//        model.addAttribute("machine", new Machine());
//        model.addAttribute("staff", staff);
//
//        return "machine/addmachine";
//    }
//
//    @RequestMapping(value = "/addmachine", method = RequestMethod.POST)
//    public String addMachineAction(Model model, @Valid Machine machine, BindingResult result,
//            @RequestParam(value = "machinepicture") MultipartFile machine_pic,
//            @RequestParam(value = "machinefile") MultipartFile machine_file) {
//
//        if (result.hasErrors()) {
//            String errorField = result.getFieldError().getField();
//            System.out.println("Don't have some Field: " + errorField);
//            if (machine_pic.getOriginalFilename().equals("") || machine_file.getOriginalFilename().equals("")) {
//                System.out.println("Don't have some file to upload.");
//
//                return "machine/addmachine";
//            } else if (!errorField.equals("machinepicture") && !errorField.equals("machinefile")) {
//                System.out.println("Some field empty");
//
//                return "machine/addmachine";
//            } else {
//                System.out.println("file upload OK.");
//
//            }
//        }
//        machine.setIs_delete(false);
//        machine.setMachine_pic(uploadFile(machine_pic));
//        machine.setMachine_file(uploadFile(machine_file));
//        machineDAO.insert(machine);
//        return "redirect:/machinelist";
//    }
//
//    @RequestMapping(value = "/addtest", method = RequestMethod.POST)
//    public String addTest(@RequestParam(value = "jame") String jame) {
//        System.out.println("info ========> " + jame);
//        return "machine/addmachine";
//    }
//
//    public String uploadFile(MultipartFile file) {
//
//        //Amazon set Credentials 
//        AmazonS3 s3client = new AmazonS3Client(new BasicAWSCredentials("AKIAIVWJITLXBJK5M2UQ", "vIpbKGkoY3vJa7KP/Z8V5VjGm7SFXMxeBn/0iJcf"));
//        s3client.setRegion(Region.getRegion(Regions.fromName("ap-southeast-1")));
//        String bucketname = "fidesla";
//
//        //Rename Name: file + datetime + filetype
//        String filename = file.getOriginalFilename();
//        Calendar cal = Calendar.getInstance();
//        SimpleDateFormat dateFormat = new SimpleDateFormat("YYYYMMddHHmmssSSS");
//        String nowTime = dateFormat.format(cal.getTime());
//        int spaceIndex = filename.indexOf(".");
//        int setendName = spaceIndex;
//        if (setendName > 15) {
//            setendName = 15;
//        }
//        String newfilename = filename.substring(0, setendName) + nowTime + filename.substring(spaceIndex);
//        System.out.println("datetime: " + nowTime);
//        System.out.println("Original name: " + filename);
//        System.out.println("New file name: " + newfilename);
//        String filelink = "";
//
//        //Start upload process
//        try {
//            s3client.putObject(new PutObjectRequest(bucketname, newfilename, file.getInputStream(), new ObjectMetadata())
//                    .withCannedAcl(CannedAccessControlList.Private));
//            System.out.println("Upload complete!");
//            S3Object s3Object = s3client.getObject(new GetObjectRequest(bucketname, newfilename));
//
//            filelink = s3Object.getObjectContent().getHttpRequest().getURI().toString();
//
//            System.out.println("Link: " + filelink);
//
//        } catch (IOException ex) {
//            ;
//        }
//
//        return filelink;
//    }
//    
//      @RequestMapping(value = "/machinelist", method = RequestMethod.GET)
//    public String withdrawList(Model model, @RequestParam(value = "page", required = false) String page) throws ParseException {
//        if (page == null || Integer.parseInt(page) < 0) {
//            page = "0";
//        }
//        int countRow = 0;
//        int page_count = 0;
//        countRow = machineDAO.countRows();
//        Double pagecountDB = Math.ceil((double) countRow / (double) PER_PAGE);
//        page_count = pagecountDB.intValue();
//        List<Machine> machine = new ArrayList<Machine>();
//        machine = machineDAO.getMachineList(page,PER_PAGE);
//         if (Integer.parseInt(page) > 0) {
//            model.addAttribute("page_priv", true);
//        }
//        if (Integer.parseInt(page) < (page_count - 1)) {
//            model.addAttribute("page_next", true);
//        }
//        if (page_count >= 10) {
//            page_count = 10;
//        }
//        model.addAttribute("machines", machine);
//        model.addAttribute("page_count", page_count);
//        model.addAttribute("page", page);
//        return "machine/machinelist";
//    }
//    
//        @RequestMapping(value = "/editmachine", method = RequestMethod.POST)
//    public String edit(Model model,@RequestParam(value = "id") int id) {
//        Machine machine = machineDAO.findMachineId(id);
//        model.addAttribute("machine", machine);
//        return "machine/editmachine";
//    }
//    
//         @RequestMapping(value = "/editmachines", method = RequestMethod.POST)
//    public String editstaff(Model model,@Valid @ModelAttribute("machine") Machine machine,@RequestParam(value = "id") int id,
//            @RequestParam(value = "machinepicture") MultipartFile machine_pic,
//            @RequestParam(value = "machinefile") MultipartFile machine_file,
//            BindingResult result) {
//        if (result.hasErrors()) {         
//                return "machine/editmachine";
//            } 
//            // System.out.println("Machine" +machine_pic);
//             if ((machine_pic.getOriginalFilename().equals("")) && (machine_file.getOriginalFilename().equals(""))) {
//                    Machine mc = machineDAO.findMachineId(id);  
//                    machine.setMachine_pic(mc.getMachine_pic());
//                    machine.setMachine_file(mc.getMachine_file());
//             }else{
//            machine.setMachine_pic(uploadFile(machine_pic));
//            machine.setMachine_file(uploadFile(machine_file));
//            }
//             
//
//        machine.setId(id);
//        machineDAO.update(machine);
//        return "redirect:/machinelist";
//    }
//    
//         @RequestMapping(value = "/deletemachine", method = RequestMethod.POST)
//    public String deletestaff(Model model,@RequestParam(value = "id") int id) {
//        machineDAO.delete(id);
//        return "redirect:/machinelist";
//    }
//    
//          @RequestMapping(value = "/viewmachine", method = RequestMethod.POST)
//    public String viewMachine(Model model,@RequestParam(value = "idview") int idview) {
//              System.out.println("id = " + idview);
//        Machine machine = machineDAO.findMachineId(idview);
//        model.addAttribute("machine", machine);
//        return "machine/viewmachine";
//    }
//
//}
