/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycrm.controller;

import com.mycrm.dao.OldOrderWithdrawDAO;
import com.mycrm.dao.OldWithdrawDAO;
import com.mycrm.dao.StaffDAO;
import com.mycrm.dao.UserDAO;
import com.mycrm.domain.Machine;
import com.mycrm.domain.OldWithdraw;
import com.mycrm.domain.OldorderWithdrawlist;
import com.mycrm.domain.Staff;
import com.mycrm.domain.User;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author pop
 */
@Controller
public class OldWithdrawController {

    int PER_PAGE = 5;

    @Autowired
    OldWithdrawDAO oldWithdrawDAO;

    @Autowired
    OldOrderWithdrawDAO oldOrderWithdrawDAO;

    @Autowired
    StaffDAO staffDAO;

    @Autowired
    UserDAO userDAO;

    @RequestMapping(value = "/oldwithdrawlist", method = RequestMethod.POST)
    public String OldwithdrawList(Model model, @RequestParam(value = "page", required = false) String page, @RequestParam(value = "withdraw_id") int withdraw_id) throws ParseException {
        System.out.println("ID: " + withdraw_id);
        if (page == null || Integer.parseInt(page) < 0) {
            page = "0";
        }
        int countRow = 0;
        int page_count = 0;

        Double pagecountDB = Math.ceil((double) countRow / (double) PER_PAGE);
        page_count = pagecountDB.intValue();
        List<OldWithdraw> oldwithdrawlist = new ArrayList<OldWithdraw>();
        oldwithdrawlist = oldWithdrawDAO.getOldWithdrawList(withdraw_id, page, PER_PAGE);
        System.out.println("info: " + oldwithdrawlist.toString());

        if (Integer.parseInt(page) > 0) {
            model.addAttribute("page_priv", true);
        }
        if (Integer.parseInt(page) < (page_count - 1)) {
            model.addAttribute("page_next", true);
        }
        if (page_count >= 10) {
            page_count = 10;
        }
        model.addAttribute("oldwithdraws", oldwithdrawlist);
        model.addAttribute("page_count", page_count);
        model.addAttribute("page", page);

        return "withdraw_history/oldwithdrawlist";
    }

    @RequestMapping(value = "/oldwithdrawview", method = RequestMethod.POST)
    public String OldwithdrawView(Model model, @RequestParam(value = "oldwithdrawid") int oldwithdrawid) throws ParseException {
        
        System.out.println("Old Withdraw ID: "+oldwithdrawid);
        User login_id = new User();
        int user_id = HomeController.getUser_id();
        login_id = userDAO.findUserId(user_id);

        OldWithdraw oldWithdraw = new OldWithdraw();
        List<OldorderWithdrawlist> oldOrderWithdrawlist = new ArrayList<OldorderWithdrawlist>();
       // Machine machine = new Machine("-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-");

        oldWithdraw = oldWithdrawDAO.findOldWithdrawId(oldwithdrawid);
        System.out.println("old Withdraw ======> "+oldWithdraw.toString());
//        oldOrderWithdrawlist = oldOrderWithdrawDAO.getOrderWithdrawList(oldwithdrawid);

        Staff withdrawal = new Staff();
        Staff driver = new Staff();
        withdrawal = staffDAO.findStaff(oldWithdraw.getWithdrawal_id());
        driver = staffDAO.findStaff(oldWithdraw.getDriver_id());

        model.addAttribute("oldwithdraw", oldWithdraw);
        model.addAttribute("withdrawal", withdrawal);
        model.addAttribute("driver", driver);
        model.addAttribute("login_user", login_id);

        return "withdraw_history/oldwithdrawview";
    }

}
