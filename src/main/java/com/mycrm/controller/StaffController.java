/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycrm.controller;

/**
 *
 * @author Lenovo
 */
import com.mycrm.common.RoleCheck;
import com.mycrm.dao.PositionDAO;
import com.mycrm.dao.RoleDAO;
import com.mycrm.dao.StaffDAO;
import com.mycrm.dao.TempleDAO;
import com.mycrm.dao.UserDAO;
import com.mycrm.domain.Role;
import com.mycrm.domain.RoleItem;
import com.mycrm.domain.Staff;
import com.mycrm.domain.Temple;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class StaffController {

    int PER_PAGE = 5;

    @Autowired
    UserDAO userDAO;

    @Autowired
    StaffDAO staffDAO;

    @Autowired
    PositionDAO positionDAO;

    @Autowired
    RoleDAO roleDAO;

    @Autowired
    TempleDAO templeDAO;

    @Autowired
    HomeController homeController;

    @RequestMapping(value = "/stafflist", method = RequestMethod.GET)
    public String staffList(Model model, @RequestParam(value = "page", required = false) String page) throws ParseException {
        String page_Create = "OK";
        String page_Delete = "OK";
        String page_Edit = "OK";
        try {
            if (roleDAO.pageView("staff_page", HomeController.getRole_id()) == false) {
                return "redirect:/impervious";
            }

        } catch (Exception e) {
            return "redirect:/";
        }

        if (roleDAO.pageCreate("staff_page", HomeController.getRole_id()) == false) {
            page_Create = null;
        }

        if (roleDAO.pageEdit("staff_page", HomeController.getRole_id()) == false) {
            page_Edit = null;
        }
        if (roleDAO.pageDelete("staff_page", HomeController.getRole_id()) == false) {
            page_Delete = null;
        }

        if (page == null || Integer.parseInt(page) < 0) {
            page = "0";
        }

        List<RoleItem> roleItems = new ArrayList<>();
        roleItems = roleDAO.getRoleITem(HomeController.getRole_id());
        Boolean statusadmin = RoleCheck.checkAdmin(roleItems);
        if (statusadmin == true) {
            model.addAttribute("roleadmin", "admin");
        } else if (statusadmin == false) {
            model.addAttribute("roleadmin", "user");
        }

        int countRow = 0;
        int page_count = 0;
        int temple_id = HomeController.getTemple_id();
        countRow = staffDAO.countRows(temple_id);
        Double pagecountDB = Math.ceil((double) countRow / (double) PER_PAGE);
        page_count = pagecountDB.intValue();
        List<Staff> staffs = new ArrayList<Staff>();
        staffs = staffDAO.getStaffList(page, PER_PAGE, temple_id);
        if (Integer.parseInt(page) > 0) {
            model.addAttribute("page_priv", true);

        }
        if (Integer.parseInt(page) < (page_count - 1)) {
            model.addAttribute("page_next", true);
        }
        if (page_count >= 10) {
            page_count = 10;
        }
        model = homeController.getRole(model);
        model.addAttribute("staffs", staffs);
        model.addAttribute("page_count", page_count);
        model.addAttribute("page", page);
        model.addAttribute("page_Create", page_Create);
        model.addAttribute("page_Delete", page_Delete);
        model.addAttribute("page_Edit", page_Edit);
        return "staff/stafflist";
    }

    @RequestMapping(value = "/addstaff", method = RequestMethod.GET)
    public String contactAll(Model model) {

        try {
            if (roleDAO.pageCreate("staff_page", HomeController.getRole_id()) == false) {
                return "redirect:/impervious";
            }

        } catch (Exception e) {
            return "redirect:/";
        }

        List<RoleItem> roleItems = new ArrayList<>();
        roleItems = roleDAO.getRoleITem(HomeController.getRole_id());
        Boolean statusadmin = RoleCheck.checkAdmin(roleItems);
        if (statusadmin == true) {
            model.addAttribute("roleadmin", "admin");
        } else if (statusadmin == false) {
            model.addAttribute("roleadmin", "user");
        }

        List<Role> roles = new ArrayList<Role>();
        roles = roleDAO.getRole();
        List<Temple> temple = new ArrayList<Temple>();
        temple = templeDAO.getALL();
        System.out.println("templedao-->" + temple);
        model = homeController.getRole(model);
        model.addAttribute("staff", new Staff());
        model.addAttribute("roles", roles);
        model.addAttribute("temple", temple);
        return "staff/addstaff";
    }

    @RequestMapping(value = "/addstaff", method = RequestMethod.POST)
    public String addStaff(Model model, @Valid @ModelAttribute("staff") Staff staff,
            BindingResult result) {

        List<RoleItem> roleItems = new ArrayList<>();
        roleItems = roleDAO.getRoleITem(HomeController.getRole_id());
        Boolean statusadmin = RoleCheck.checkAdmin(roleItems);
        if (statusadmin == true) {
            model.addAttribute("roleadmin", "admin");
        } else if (statusadmin == false) {
            model.addAttribute("roleadmin", "user");
        }
        model = homeController.getRole(model);

        if (result.hasErrors()) {
            List<Role> roles = new ArrayList<Role>();
            roles = roleDAO.getRole();
            List<Temple> temple = new ArrayList<Temple>();
            temple = templeDAO.getALL();
            model.addAttribute("roles", roles);
            model.addAttribute("temple", temple);
            return "staff/addstaff";
        }
        staffDAO.insert(staff);
        return "redirect:/stafflist";
    }

    @RequestMapping(value = "/editstaff", method = RequestMethod.POST)
    public String edit(Model model, @RequestParam(value = "id") int id) {

        try {
            if (roleDAO.pageEdit("staff_page", HomeController.getRole_id()) == false) {
                return "redirect:/impervious";
            }
        } catch (Exception e) {
            return "redirect:/";
        }
        Staff staff = staffDAO.findStaff(id);
        model.addAttribute("staff", staff);
        List<Role> roles = new ArrayList<Role>();
        roles = roleDAO.getRole();
        model.addAttribute("roles", roles);
        model.addAttribute("roleSelect", staff.getRole_id());
        List<Temple> temple = new ArrayList<Temple>();
        temple = templeDAO.getALL();
        model = homeController.getRole(model);
        model.addAttribute("temple", temple);
        model.addAttribute("Temple_id", staff.getTemple_id());

        List<RoleItem> roleItems = new ArrayList<>();
        roleItems = roleDAO.getRoleITem(HomeController.getRole_id());
        Boolean statusadmin = RoleCheck.checkAdmin(roleItems);
        if (statusadmin == true) {
            model.addAttribute("roleadmin", "admin");
        } else if (statusadmin == false) {
            model.addAttribute("roleadmin", "user");
        }

        return "staff/editstaff";
    }

    @RequestMapping(value = "/viewstaff", method = RequestMethod.POST)
    public String viewstaff(Model model, @RequestParam(value = "id") int id) {
        Staff staff = staffDAO.findStaff(id);
        model.addAttribute("staff", staff);
        List<Role> roles = new ArrayList<Role>();
        roles = roleDAO.getRole();
        model.addAttribute("roles", roles);
        model.addAttribute("roleSelect", staff.getRole_id());
        List<Temple> temple = new ArrayList<Temple>();
        temple = templeDAO.getALL();
        model = homeController.getRole(model);
        model.addAttribute("temple", temple);
        model.addAttribute("Temple_id", staff.getTemple_id());

        List<RoleItem> roleItems = new ArrayList<>();
        roleItems = roleDAO.getRoleITem(HomeController.getRole_id());
        Boolean statusadmin = RoleCheck.checkAdmin(roleItems);
        if (statusadmin == true) {
            model.addAttribute("roleadmin", "admin");
        } else if (statusadmin == false) {
            model.addAttribute("roleadmin", "user");
        }

        return "staff/view";
    }

    @RequestMapping(value = "/editstaffs", method = RequestMethod.POST)
    public String editstaff(Model model, @Valid @ModelAttribute("staff") Staff staff, BindingResult result) {
        System.out.println("Staff:" + staff.toString());
        List<RoleItem> roleItems = new ArrayList<>();
        roleItems = roleDAO.getRoleITem(HomeController.getRole_id());
        Boolean statusadmin = RoleCheck.checkAdmin(roleItems);
        if (statusadmin == true) {
            model.addAttribute("roleadmin", "admin");
        } else if (statusadmin == false) {
            model.addAttribute("roleadmin", "user");
        }
        model = homeController.getRole(model);
        if (result.hasErrors()) {
            List<Role> roles = new ArrayList<Role>();
            roles = roleDAO.getRole();
            model.addAttribute("roles", roles);
            model.addAttribute("roleSelect", staff.getRole_id());
            List<Temple> temple = new ArrayList<Temple>();
            temple = templeDAO.getALL();
            model = homeController.getRole(model);
            model.addAttribute("temple", temple);
            model.addAttribute("Temple_id", staff.getTemple_id());
            return "staff/editstaff";
        }
        staffDAO.update(staff);
        return "redirect:/stafflist";
    }

    @RequestMapping(value = "/deletestaff", method = RequestMethod.POST)
    public String deletestaff(Model model, @RequestParam(value = "id") int id) {
        System.out.println("Get ID : " + id);
        staffDAO.delete(id);
        return "redirect:/stafflist";
    }

    @RequestMapping(value = "/profile", method = RequestMethod.GET)
    public String profile(Model model) {
        Staff staff = null;
        String page_Create = "OK";
        String page_Delete = "OK";
        String page_Edit = "OK";
        try {
            if (roleDAO.pageView("staff_page", HomeController.getRole_id()) == false) {
                return "redirect:/impervious";
            }

        } catch (Exception e) {
            return "redirect:/";
        }

        if (roleDAO.pageCreate("staff_page", HomeController.getRole_id()) == false) {
            page_Create = null;
        }

        if (roleDAO.pageEdit("staff_page", HomeController.getRole_id()) == false) {
            page_Edit = null;
        }
        if (roleDAO.pageDelete("staff_page", HomeController.getRole_id()) == false) {
            page_Delete = null;
        }
        try {
            staff = staffDAO.findStaff(HomeController.getUser_id());
            model = homeController.getRole(model);

        } catch (Exception e) {
            System.out.println("Error:" + e.getMessage());
            return "redirect:/";
        }
        List<RoleItem> roleItems = new ArrayList<>();
        roleItems = roleDAO.getRoleITem(HomeController.getRole_id());
        Boolean statusadmin = RoleCheck.checkAdmin(roleItems);
        if (statusadmin == true) {
            model.addAttribute("roleadmin", "admin");
        } else if (statusadmin == false) {
            model.addAttribute("roleadmin", "user");
        }

        List<Temple> temple = new ArrayList<Temple>();
        temple = templeDAO.getALL();
        model.addAttribute("temple", temple);

        model.addAttribute("staff", staff);
        List<Role> roles = new ArrayList<Role>();
        roles = roleDAO.getRole();
        model.addAttribute("roles", roles);
        model.addAttribute("roleSelect", staff.getRole_id());
        return "staff/profile";
    }

    @RequestMapping(value = "/editprofile", method = RequestMethod.GET)
    public String viewstaff(Model model) {
        Staff staff = null;
        String page_Create = "OK";
        String page_Delete = "OK";
        String page_Edit = "OK";
        try {
            if (roleDAO.pageView("staff_page", HomeController.getRole_id()) == false) {
                return "redirect:/impervious";
            }

        } catch (Exception e) {
            return "redirect:/";
        }

        if (roleDAO.pageCreate("staff_page", HomeController.getRole_id()) == false) {
            page_Create = null;
        }

        if (roleDAO.pageEdit("staff_page", HomeController.getRole_id()) == false) {
            page_Edit = null;
        }
        if (roleDAO.pageDelete("staff_page", HomeController.getRole_id()) == false) {
            page_Delete = null;
        }
        try {
            staff = staffDAO.findStaff(HomeController.getUser_id());

        } catch (Exception e) {
            return "redirect:/";
        }
        model = homeController.getRole(model);

        List<RoleItem> roleItems = new ArrayList<>();
        roleItems = roleDAO.getRoleITem(HomeController.getRole_id());
        Boolean statusadmin = RoleCheck.checkAdmin(roleItems);
        if (statusadmin == true) {
            model.addAttribute("roleadmin", "admin");
        } else if (statusadmin == false) {
            model.addAttribute("roleadmin", "user");
        }

        model.addAttribute("staff", staff);
        List<Role> roles = new ArrayList<Role>();
        roles = roleDAO.getRole();
        model.addAttribute("roles", roles);
        model.addAttribute("roleSelect", staff.getRole_id());
        return "staff/editprofile";
    }

    @RequestMapping(value = "/saveprofile", method = RequestMethod.POST)
    public String saveprofile(Model model, @Valid @ModelAttribute("staff") Staff staff, BindingResult result) {

        List<RoleItem> roleItems = new ArrayList<>();
        roleItems = roleDAO.getRoleITem(HomeController.getRole_id());
        Boolean statusadmin = RoleCheck.checkAdmin(roleItems);
        if (statusadmin == true) {
            model.addAttribute("roleadmin", "admin");
        } else if (statusadmin == false) {
            model.addAttribute("roleadmin", "user");
        }
        model = homeController.getRole(model);

        if (result.hasErrors()) {
            return "staff/editprofile";
        }
        staffDAO.update(staff);
        return "redirect:/profile";
    }

}
