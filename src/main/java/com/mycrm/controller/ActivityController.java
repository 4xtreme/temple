/*
 * Copy Right Pay Enterprise Co.,Ltd.
 */
package com.mycrm.controller;

import com.mycrm.common.RoleCheck;
import com.mycrm.dao.ActivityDAO;
import com.mycrm.dao.RoleDAO;
import com.mycrm.dao.VipassanaDAO;
import com.mycrm.domain.Activity;
import com.mycrm.domain.RoleItem;
import com.mycrm.domain.Vipassana;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author pop
 */
@Controller
public class ActivityController {

    @Autowired
    HomeController homeController;

    @Autowired
    ActivityDAO activityDAO;

    @Autowired
    RoleDAO roleDAO;

    @Autowired
    VipassanaDAO vipassanaDAO;

    int PER_PAGE = 5;

    @InitBinder
    public void initBinder(WebDataBinder binder) {

        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        sdf.setLenient(true);
        binder.registerCustomEditor(Date.class, new CustomDateEditor(sdf, true));

    }

    @RequestMapping(value = "/activity", method = RequestMethod.GET)
    public String activity(Model model, @RequestParam(value = "page", required = false) String page) {
        String page_Create = "OK";
        String page_Delete = "OK";
        String page_Edit = "OK";
        try {
            if (roleDAO.pageView("monk", HomeController.getRole_id()) == false) {
                return "redirect:/impervious";
            }

        } catch (Exception e) {
            return "redirect:/";
        }

        if (roleDAO.pageCreate("monk", HomeController.getRole_id()) == false) {
            page_Create = null;
        }

        if (roleDAO.pageEdit("monk", HomeController.getRole_id()) == false) {
            page_Edit = null;
        }
        if (roleDAO.pageDelete("monk", HomeController.getRole_id()) == false) {
            page_Delete = null;
        }
        List<Activity> activitys = new ArrayList<>();
        if (page == null || Integer.parseInt(page) < 0) {
            page = "0";
        }
        int countRow = 0;
        int page_count = 0;
        int temple_id = HomeController.getTemple_id();
        countRow = activityDAO.countRows(temple_id);
        Double pagecountDB = Math.ceil((double) countRow / (double) PER_PAGE);
        page_count = pagecountDB.intValue();
        activitys = activityDAO.findAll(HomeController.getTemple_id(), page, PER_PAGE);

        if (Integer.parseInt(page) > 0) {
            model.addAttribute("page_priv", true);

        }
        if (Integer.parseInt(page) < (page_count - 1)) {
            model.addAttribute("page_next", true);
        }
        if (page_count >= 10) {
            page_count = 10;
        }

        List<RoleItem> roleItems = new ArrayList<>();
        roleItems = roleDAO.getRoleITem(HomeController.getRole_id());
        Boolean statusadmin = RoleCheck.checkAdmin(roleItems);
        if (statusadmin == true) {
            model.addAttribute("roleadmin", "admin");
        } else if (statusadmin == false) {
            model.addAttribute("roleadmin", "user");
        }
        model = homeController.getRole(model);
        model.addAttribute("activities", activitys);
        model.addAttribute("page_count", page_count);
        model.addAttribute("page", page);
        return "activity/activity";
    }

    @RequestMapping(value = "/addactivity", method = RequestMethod.GET)
    public String addActivity(Model model) {
        String page_Create = "OK";
        String page_Delete = "OK";
        String page_Edit = "OK";
        try {
            if (roleDAO.pageView("room_page", HomeController.getRole_id()) == false) {
                return "redirect:/impervious";
            }

        } catch (Exception e) {
            return "redirect:/";
        }

        if (roleDAO.pageCreate("room_page", HomeController.getRole_id()) == false) {
            page_Create = null;
        }

        if (roleDAO.pageEdit("room_page", HomeController.getRole_id()) == false) {
            page_Edit = null;
        }
        if (roleDAO.pageDelete("room_page", HomeController.getRole_id()) == false) {
            page_Delete = null;
        }
        List<RoleItem> roleItems = new ArrayList<>();
        roleItems = roleDAO.getRoleITem(HomeController.getRole_id());
        Boolean statusadmin = RoleCheck.checkAdmin(roleItems);
        if (statusadmin == true) {
            model.addAttribute("roleadmin", "admin");
        } else if (statusadmin == false) {
            model.addAttribute("roleadmin", "user");
        }
        List<Vipassana> vipassanas = new ArrayList<>();
        vipassanas = vipassanaDAO.findAll(HomeController.getTemple_id(), "0", 1000);
        model.addAttribute("vipassanas", vipassanas);
        model = homeController.getRole(model);
        model.addAttribute("activity", new Activity());
        return "activity/add_activity";
    }

    @RequestMapping(value = "/addactivity", method = RequestMethod.POST)
    public String addActivity(Model model, @Valid @ModelAttribute("activity") Activity activity, BindingResult result,
            @RequestParam("activity_start") String activity_start,
            @RequestParam("activity_end") String activity_end) {

        if (result.hasErrors()) {
            System.out.println("Error:" + result.toString());
            List<RoleItem> roleItems = new ArrayList<>();
            roleItems = roleDAO.getRoleITem(HomeController.getRole_id());
            Boolean statusadmin = RoleCheck.checkAdmin(roleItems);
            if (statusadmin == true) {
                model.addAttribute("roleadmin", "admin");
            } else if (statusadmin == false) {
                model.addAttribute("roleadmin", "user");
            }
            List<Vipassana> vipassanas = new ArrayList<>();
            vipassanas = vipassanaDAO.findAll(HomeController.getTemple_id(), "0", 1000);
            model.addAttribute("vipassanas", vipassanas);
            model = homeController.getRole(model);
            model.addAttribute("activity", activity);
            homeController.getRole(model);
            return "activity/add_activity";
        }

        activity.setTemple_id(HomeController.getTemple_id());
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy", new Locale("th","TH"));
        try {
            Date start_date = sdf.parse(activity_start);
            Date end_date = sdf.parse(activity_end);
            activity.setActivity_start(start_date);
            activity.setActivity_end(end_date);

        } catch (ParseException ex) {
            Logger.getLogger(ActivityController.class.getName()).log(Level.SEVERE, null, ex);
        }

        activityDAO.insert(activity);
        return "redirect:/activity";
    }

    @RequestMapping(value = "/editactivity", method = RequestMethod.GET)
    public String editActivity(Model model, @RequestParam("id") int id) {
        List<RoleItem> roleItems = new ArrayList<>();
        roleItems = roleDAO.getRoleITem(HomeController.getRole_id());
        Boolean statusadmin = RoleCheck.checkAdmin(roleItems);
        if (statusadmin == true) {
            model.addAttribute("roleadmin", "admin");
        } else if (statusadmin == false) {
            model.addAttribute("roleadmin", "user");
        }
        Activity activity = new Activity();
        activity = activityDAO.findById(id);
        List<Vipassana> vipassanas = new ArrayList<>();
        vipassanas = vipassanaDAO.findAll(HomeController.getTemple_id(), "0", 1000);
        model.addAttribute("vipassanas", vipassanas);
        model = homeController.getRole(model);
        model.addAttribute("activity", activity);
        return "activity/edit_activity";
    }

    @RequestMapping(value = "/editactivity", method = RequestMethod.POST)
    public String editActivity(Model model, @Valid @ModelAttribute("activity") Activity activity, BindingResult result,
            @RequestParam("activity_start") String activity_start,
            @RequestParam("activity_end") String activity_end) {

        List<RoleItem> roleItems = new ArrayList<>();
        roleItems = roleDAO.getRoleITem(HomeController.getRole_id());
        Boolean statusadmin = RoleCheck.checkAdmin(roleItems);
        if (statusadmin == true) {
            model.addAttribute("roleadmin", "admin");
        } else if (statusadmin == false) {
            model.addAttribute("roleadmin", "user");
        }
        model = homeController.getRole(model);
        if (result.hasErrors()) {
            List<Vipassana> vipassanas = new ArrayList<>();
            vipassanas = vipassanaDAO.findAll(HomeController.getTemple_id(), "0", 1000);
            model.addAttribute("vipassanas", vipassanas);
            return "activity/edit_activity";
        }
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy", new Locale("th","TH"));
        try {
            Date start_date = sdf.parse(activity_start);
            Date end_date = sdf.parse(activity_end);
            activity.setActivity_start(start_date);
            activity.setActivity_end(end_date);

        } catch (ParseException ex) {
            Logger.getLogger(ActivityController.class.getName()).log(Level.SEVERE, null, ex);
        }
        activityDAO.update(activity);
        return "redirect:/activity";
    }

    @RequestMapping(value = "/deleteactivity", method = RequestMethod.POST)
    public String deleteVipassana(@RequestParam("id") int id) {
        activityDAO.delete(id);
        return "redirect:/activity";
    }
}
