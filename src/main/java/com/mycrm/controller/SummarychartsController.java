package com.mycrm.controller;

import com.ibm.icu.util.Calendar;
import com.mycrm.common.RoleCheck;
import com.mycrm.dao.RoleDAO;
import com.mycrm.dao.StaffDAO;
import com.mycrm.dao.SummarychartsDAO;
import com.mycrm.domain.RoleItem;
import com.mycrm.domain.Staff;
import com.mycrm.domain.Summarycharts;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class SummarychartsController {

    @Autowired
    SummarychartsDAO SummarychartsDAO;

    @Autowired
    RoleDAO roleDAO;

    @Autowired
    StaffDAO staffDAO;
    
    @Autowired
    HomeController homeController;

    @RequestMapping(value = "/summarycharts", method = RequestMethod.GET)
    public String summarycharts(Model model) {
        try {
            if (roleDAO.pageView("report", HomeController.getRole_id()) == false) {
                return "redirect:/impervious";
            }

        } catch (Exception e) {
            return "redirect:/";
        }

        List<RoleItem> roleItems = new ArrayList<>();
        roleItems = roleDAO.getRoleITem(HomeController.getRole_id());
        Boolean statusadmin = RoleCheck.checkAdmin(roleItems);
        if (statusadmin == true) {
            model.addAttribute("roleadmin", "admin");
        } else if (statusadmin == false) {
            model.addAttribute("roleadmin", "user");
        }
        model = homeController.getRole(model);

        return "charts/summarycharts";
    }

    @RequestMapping(value = "/get_adata/{val}", method = RequestMethod.GET)
    public ResponseEntity<List<List<String>>> get_data(@PathVariable String val) {

        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd", Locale.ENGLISH);
        SimpleDateFormat ssdf = new SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH);

        int start = Integer.parseInt(val);
        int x = 0;
        int dayy = 0;
        ArrayList<String> arrList = new ArrayList<String>();
        ArrayList<String> arrList2 = new ArrayList<String>();
        ArrayList<List<String>> arrList3 = new ArrayList<List<String>>();
        ArrayList<String> new1 = new ArrayList<String>();
        ArrayList<String> new2 = new ArrayList<String>();

        ArrayList<String> id1 = new ArrayList<String>();
        ArrayList<String> id2 = new ArrayList<String>();
        ArrayList<String> id3 = new ArrayList<String>();
        ArrayList<String> id4 = new ArrayList<String>();
        ArrayList<String> id5 = new ArrayList<String>();
        ArrayList<String> id6 = new ArrayList<String>();
        ArrayList<String> id7 = new ArrayList<String>();
        ArrayList<String> id8 = new ArrayList<String>();

        ArrayList<String> summ1 = new ArrayList<String>();
        ArrayList<String> summ2 = new ArrayList<String>();
        ArrayList<String> summ3 = new ArrayList<String>();
        ArrayList<String> summ4 = new ArrayList<String>();
        ArrayList<String> summ5 = new ArrayList<String>();
        ArrayList<String> summ6 = new ArrayList<String>();
        ArrayList<String> summ7 = new ArrayList<String>();
        ArrayList<String> summ8 = new ArrayList<String>();

        ArrayList<String> value = new ArrayList<String>();
        ArrayList<List<String>> sumvalue = new ArrayList<List<String>>();

        String id = null;
        List<Summarycharts> g = SummarychartsDAO.genderlist(id);
        int l = 0;
        int i = 0;
        int n = 0;
        int m = 0;
        int a = 0;
        int b = 0;
        int s = 0;
        String gen = null;
        for (i = 1; i <= start; i++) {

            Calendar c = Calendar.getInstance();
            c.setTime(new Date());
            c.add(Calendar.DATE, -i);
            Date d = c.getTime();

            dayy = Integer.parseInt(sdf.format(d));
            arrList.add("" + sdf.format(d));
            arrList2.add("" + ssdf.format(d));
            summ1.add("" + 0);
            summ2.add("" + 0);
            summ3.add("" + 0);
            summ4.add("" + 0);
            summ5.add("" + 0);
            summ6.add("" + 0);
            summ7.add("" + 0);
            summ8.add("" + 0);

        }
        System.out.println("test sum" + summ1);

        int j = 0;
        for (j = arrList.size() - 1; j >= 0; j--) {
            new1.add(arrList.get(j));

        }
        int k = 0;
        for (k = arrList2.size() - 1; k >= 0; k--) {
            new2.add(arrList2.get(k));

        }

        System.out.println("arrList3----->" + arrList3);
//------------------------------------------------------------------------------------------
        for (l = 0; l < g.size(); l++) {

            if (g.get(l).getGender().equals("ชาย")) {
                id1.add(g.get(l).getId());

            } else if (g.get(l).getGender().equals("หญิง")) {
                id2.add(g.get(l).getId());
            }
            if (g.get(l).getGender().equals("ภิกษุ")) {
                id3.add(g.get(l).getId());
            }
            if (g.get(l).getGender().equals("ภิกษุณี")) {
                id4.add(g.get(l).getId());
            }
            if (g.get(l).getGender().equals("สามเณร")) {
                id5.add(g.get(l).getId());
            }
            if (g.get(l).getGender().equals("แม่ชี")) {
                id6.add(g.get(l).getId());
            }
            if (g.get(l).getGender().equals("ต่างชาติชาย")) {
                id7.add(g.get(l).getId());
            }
            if (g.get(l).getGender().equals("ต่างชาติหญิง")) {
                id8.add(g.get(l).getId());
            }

        }
//--------------------------------------ชาย-----------------------------------------------//
        for (n = 0; n < id1.size(); n++) {
            value.clear();
            for (m = 0; m < arrList2.size(); m++) {
                String day = new1.get(m);
                gen = id1.get(n);
                x = SummarychartsDAO.summarychartslist(gen, "" + day);
                value.add("" + x);

            }

            System.out.println("value list 7-1-->" + value);
            int y = 0;
            for (y = 0; y < value.size(); y++) {
                a = Integer.parseInt(value.get(y));
                b = Integer.parseInt(summ1.get(y));
                s = b + a;

                summ1.set(y, "" + s);
            }

            System.out.println("summ-------in loop--> " + summ1);
        }
//--------------------------------------หญิง-----------------------------------------------//
        for (n = 0; n < id2.size(); n++) {
            value.clear();
            for (m = 0; m < arrList2.size(); m++) {
                String day = new1.get(m);
                gen = id2.get(n);
                x = SummarychartsDAO.summarychartslist(gen, "" + day);
                value.add("" + x);

            }

            System.out.println("value list 7-1-->" + value);
            int y = 0;
            for (y = 0; y < value.size(); y++) {
                a = Integer.parseInt(value.get(y));
                b = Integer.parseInt(summ2.get(y));
                s = b + a;

                summ2.set(y, "" + s);
            }

            System.out.println("summ-------in loop--> " + summ2);
        }
        //--------------------------------ภิกษุ-----------------------------------------------------//
        for (n = 0; n < id3.size(); n++) {
            value.clear();
            for (m = 0; m < arrList2.size(); m++) {
                String day = new1.get(m);
                gen = id3.get(n);
                x = SummarychartsDAO.summarychartslist(gen, "" + day);
                value.add("" + x);

            }

            System.out.println("value list 7-1-->" + value);
            int y = 0;
            for (y = 0; y < value.size(); y++) {
                a = Integer.parseInt(value.get(y));
                b = Integer.parseInt(summ3.get(y));
                s = b + a;

                summ3.set(y, "" + s);
            }

            System.out.println("summ-------in loop--> " + summ3);
        }

        //--------------------------------------ภิกษุณี-----------------------------------------------//
        for (n = 0; n < id4.size(); n++) {
            value.clear();
            for (m = 0; m < arrList2.size(); m++) {
                String day = new1.get(m);
                gen = id4.get(n);
                x = SummarychartsDAO.summarychartslist(gen, "" + day);
                value.add("" + x);

            }

            System.out.println("value list 7-1-->" + value);
            int y = 0;
            for (y = 0; y < value.size(); y++) {
                a = Integer.parseInt(value.get(y));
                b = Integer.parseInt(summ4.get(y));
                s = b + a;

                summ4.set(y, "" + s);
            }
            System.out.println("summ-------in loop--> " + summ4);
        }

        //--------------------------------------สามเณร-----------------------------------------------//
        for (n = 0; n < id5.size(); n++) {
            value.clear();
            for (m = 0; m < arrList2.size(); m++) {
                String day = new1.get(m);
                gen = id5.get(n);
                x = SummarychartsDAO.summarychartslist(gen, "" + day);
                value.add("" + x);

            }

            System.out.println("value list 7-1-->" + value);
            int y = 0;
            for (y = 0; y < value.size(); y++) {
                a = Integer.parseInt(value.get(y));
                b = Integer.parseInt(summ5.get(y));
                s = b + a;

                summ5.set(y, "" + s);
            }
            System.out.println("summ-------in loop--> " + summ5);
        }

        //--------------------------------------แม่ชี-----------------------------------------------//
        for (n = 0; n < id6.size(); n++) {
            value.clear();
            for (m = 0; m < arrList2.size(); m++) {
                String day = new1.get(m);
                gen = id6.get(n);
                x = SummarychartsDAO.summarychartslist(gen, "" + day);
                value.add("" + x);

            }

            System.out.println("value list 7-1-->" + value);
            int y = 0;
            for (y = 0; y < value.size(); y++) {
                a = Integer.parseInt(value.get(y));
                b = Integer.parseInt(summ6.get(y));
                s = b + a;

                summ6.set(y, "" + s);
            }
            System.out.println("summ-------in loop--> " + summ6);
        }

        //--------------------------------------ต่างชาติชาย-----------------------------------------------//
        for (n = 0; n < id7.size(); n++) {
            value.clear();
            for (m = 0; m < arrList2.size(); m++) {
                String day = new1.get(m);
                gen = id7.get(n);
                x = SummarychartsDAO.summarychartslist(gen, "" + day);
                value.add("" + x);

            }

            System.out.println("value list 7-1-->" + value);
            int y = 0;
            for (y = 0; y < value.size(); y++) {
                a = Integer.parseInt(value.get(y));
                b = Integer.parseInt(summ7.get(y));
                s = b + a;

                summ7.set(y, "" + s);
            }
            System.out.println("summ-------in loop--> " + summ7);
        }

        //--------------------------------------ต่างชาติหญิง-----------------------------------------------//
        for (n = 0; n < id8.size(); n++) {
            value.clear();
            for (m = 0; m < arrList2.size(); m++) {
                String day = new1.get(m);
                gen = id8.get(n);
                x = SummarychartsDAO.summarychartslist(gen, "" + day);
                value.add("" + x);

            }

            System.out.println("value list 7-1-->" + value);
            int y = 0;
            for (y = 0; y < value.size(); y++) {
                a = Integer.parseInt(value.get(y));
                b = Integer.parseInt(summ8.get(y));
                s = b + a;

                summ8.set(y, "" + s);
            }
            System.out.println("summ-------in loop--> " + summ8);
        }

        //-------------------------------------------------------------------------------------//
        arrList3.add(new2);
        arrList3.add(summ1);
        arrList3.add(summ2);
        arrList3.add(summ3);
        arrList3.add(summ4);
        arrList3.add(summ5);
        arrList3.add(summ6);
        arrList3.add(summ7);
        arrList3.add(summ8);
        return new ResponseEntity<List<List<String>>>(arrList3, HttpStatus.OK);
    }
}
