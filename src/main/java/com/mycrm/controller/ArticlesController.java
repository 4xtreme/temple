package com.mycrm.controller;

import com.mycrm.common.RoleCheck;
import com.mycrm.dao.ArticlesDAO;
import com.mycrm.dao.RoleDAO;
import com.mycrm.dao.StaffDAO;
import com.mycrm.domain.Articles;
import com.mycrm.domain.RoleItem;
import com.mycrm.domain.Staff;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.validation.Valid;
import org.apache.commons.codec.binary.Base64;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

@Controller
public class ArticlesController {

    Staff staff;
    @Autowired
    StaffDAO staffDAO;
    @Autowired
    RoleDAO roleDAO;
    int staffId = 10;
    int article_id = 10;
    private int PER_PAGE = 5;

    @Autowired
    ArticlesDAO articlesDAO;

    @Autowired
    HomeController homeController;

    @RequestMapping(value = "/aarticles", method = RequestMethod.GET)
    public String createArticle(Model model) {
        Staff staff = null;
        try {
            if (roleDAO.pageView("content", HomeController.getRole_id()) == false) {
                return "redirect:/impervious";
            }

        } catch (Exception e) {
            return "redirect:/";
        }
        try {
            staff = staffDAO.findStaff(HomeController.getUser_id());
        } catch (Exception e) {
        }

        List<RoleItem> roleItems = new ArrayList<>();
        roleItems = roleDAO.getRoleITem(HomeController.getRole_id());
        Boolean statusadmin = RoleCheck.checkAdmin(roleItems);
        if (statusadmin == true) {
            model.addAttribute("roleadmin", "admin");
        } else if (statusadmin == false) {
            model.addAttribute("roleadmin", "user");
        }

        model = homeController.getRole(model);
        model.addAttribute("articles", new Articles());
        model.addAttribute("staff", staff);
        return "articles/createArticle";
    }

    @RequestMapping(value = "/createArticle", method = RequestMethod.POST)
    public String addArticle(Model model, @Valid @ModelAttribute("articles") Articles articles,
            BindingResult result,
            @RequestParam("article_img") MultipartFile article_pic) {

        if (result.hasErrors()) {
            List<RoleItem> roleItems = new ArrayList<>();
            roleItems = roleDAO.getRoleITem(HomeController.getRole_id());
            Boolean statusadmin = RoleCheck.checkAdmin(roleItems);
            if (statusadmin == true) {
                model.addAttribute("roleadmin", "admin");
            } else if (statusadmin == false) {
                model.addAttribute("roleadmin", "user");
            }
            model = homeController.getRole(model);
            System.out.println("result:" + result);
            Staff staff = new Staff();
            staff = staffDAO.findStaff(HomeController.getUser_id());
            model.addAttribute("staff", staff);
            System.out.println("articles : " + articles.toString());
            return "articles/createArticle";
        }
        try {
            articles.setArticle_pic(article_pic.getBytes());
        } catch (IOException ex) {
            Logger.getLogger(ContactController.class.getName()).log(Level.SEVERE, null, ex);
        }

        System.out.println("DAO--------------- : " + articles.toString());
        articlesDAO.insert(articles);

        return "redirect:/articlesall";
    }

    @RequestMapping(value = "/articlesall", method = RequestMethod.GET)
    public String showArticles(Model model, @RequestParam(value = "page", required = false) String page) {
        String page_Create = "OK";
        String page_Delete = "OK";
        String page_Edit = "OK";
        try {
            if (roleDAO.pageView("content", HomeController.getRole_id()) == false) {
                return "redirect:/impervious";
            }

        } catch (Exception e) {
            return "redirect:/";
        }

        if (roleDAO.pageCreate("content", HomeController.getRole_id()) == false) {
            page_Create = null;
        }

        if (roleDAO.pageEdit("content", HomeController.getRole_id()) == false) {
            page_Edit = null;
        }
        if (roleDAO.pageDelete("content", HomeController.getRole_id()) == false) {
            page_Delete = null;
        }
        if (page == null || Integer.parseInt(page) < 0) {
            page = "0";
        }
        int countRow = 0;
        int page_count = 0;
        int temple_id = HomeController.getTemple_id();
        System.out.println("session id : " + HomeController.getTemple_id());
        countRow = articlesDAO.countRows(temple_id);
        Double pagecountDB = Math.ceil((double) countRow / (double) PER_PAGE);
        page_count = pagecountDB.intValue();
        List<Articles> articlelist = new ArrayList<Articles>();
        articlelist = articlesDAO.getArticlesList(temple_id, page, PER_PAGE);
        try {
            for (int i = 0; i < articlelist.size(); i++) {
                articlelist.get(i).setArticle_byte(articlelist.get(i).getArticle_pic());
                articlelist.get(i).setArticle_string(new String(Base64.encodeBase64(articlelist.get(i).getArticle_byte())));
            }
        } catch (Exception e) {
        }
        if (Integer.parseInt(page) > 0) {
            model.addAttribute("page_priv", true);
        }
        if (Integer.parseInt(page) < (page_count - 1)) {
            model.addAttribute("page_next", true);
        }
        if (page_count >= 10) {
            page_count = 10;
        }
        model = homeController.getRole(model);
        model.addAttribute("page_count", page_count);
        model.addAttribute("page", page);
        model.addAttribute("articlelist", articlelist);

        List<RoleItem> roleItems = new ArrayList<>();
        roleItems = roleDAO.getRoleITem(HomeController.getRole_id());
        Boolean statusadmin = RoleCheck.checkAdmin(roleItems);
        if (statusadmin == true) {
            model.addAttribute("roleadmin", "admin");
        } else if (statusadmin == false) {
            model.addAttribute("roleadmin", "user");
        }

        return "articles/articlesShow";
    }

    @RequestMapping(value = "/selectarticleEdit", method = RequestMethod.POST)
    public String selectEditArticle(Model model, @RequestParam("article_id") int article_id) {
        Articles articles = articlesDAO.findArticleInfo(article_id);
        //System.out.println("Controller : " + articles.toString());
        System.out.println("artiecle pic : " + articles.getArticle_pic());
        model = homeController.getRole(model);
        model.addAttribute("articles", articles);

        List<RoleItem> roleItems = new ArrayList<>();
        roleItems = roleDAO.getRoleITem(HomeController.getRole_id());
        Boolean statusadmin = RoleCheck.checkAdmin(roleItems);
        if (statusadmin == true) {
            model.addAttribute("roleadmin", "admin");
        } else if (statusadmin == false) {
            model.addAttribute("roleadmin", "user");
        }

        return "articles/editArticle";
    }

    @RequestMapping(value = "/selectarticleView", method = RequestMethod.POST)
    public String selectEditArticleView(Model model, @RequestParam("article_id") int article_id) {
        Articles articles = articlesDAO.findArticleInfo(article_id);
        System.out.println("Controller : " + articles.toString());
        model = homeController.getRole(model);
        model.addAttribute("articles", articles);

        List<RoleItem> roleItems = new ArrayList<>();
        roleItems = roleDAO.getRoleITem(HomeController.getRole_id());
        Boolean statusadmin = RoleCheck.checkAdmin(roleItems);
        if (statusadmin == true) {
            model.addAttribute("roleadmin", "admin");
        } else if (statusadmin == false) {
            model.addAttribute("roleadmin", "user");
        }

        return "articles/viewArticle";
    }

    @RequestMapping(value = "/updateArticle", method = RequestMethod.POST)
    public String update(Model model, @Valid @ModelAttribute("articles") Articles articles, BindingResult result,
            @RequestParam("article_img") MultipartFile article_pic)
            throws IOException {

        if (result.hasErrors()) {
            model = homeController.getRole(model);
            model.addAttribute("articles", articles);

            List<RoleItem> roleItems = new ArrayList<>();
            roleItems = roleDAO.getRoleITem(HomeController.getRole_id());
            Boolean statusadmin = RoleCheck.checkAdmin(roleItems);
            if (statusadmin == true) {
                model.addAttribute("roleadmin", "admin");
            } else if (statusadmin == false) {
                model.addAttribute("roleadmin", "user");
            }

            return "articles/editArticle";

        }
        if (article_pic.isEmpty()) {
            System.out.println("Pic is null");
            Articles update = articlesDAO.findArticleInfo(articles.getArticle_id());
            articles.setArticle_pic(update.getArticle_pic());
        } else {
            System.out.println("Pic is not null");
            try {
                System.out.println("------------------------------------------------------");
                articles.setArticle_pic(article_pic.getBytes());
            } catch (IOException ex) {
                Logger.getLogger(ContactController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        articlesDAO.update(articles);
        return "redirect:/articlesall";
    }

    @RequestMapping(value = "/deleteArticle", method = RequestMethod.POST)
    public String delete(@RequestParam("article_id") int article_id) {
        System.out.println("Get ID : " + article_id);
        articlesDAO.delete(article_id);
        return "redirect:/articlesall";
    }
    
    @RequestMapping(value = "/searcharticle", method = RequestMethod.POST)
    public String searchArticle(Model model,@RequestParam("search")String search){
        List<RoleItem> roleItems = new ArrayList<>();
        roleItems = roleDAO.getRoleITem(HomeController.getRole_id());
        Boolean statusadmin = RoleCheck.checkAdmin(roleItems);
        if (statusadmin == true) {
            model.addAttribute("roleadmin", "admin");
        } else if (statusadmin == false) {
            model.addAttribute("roleadmin", "user");
        }
        List<Articles> articlelist = new ArrayList<Articles>();
        articlelist = articlesDAO.searchArticle(search, HomeController.getTemple_id());
        try {
            for (int i = 0; i < articlelist.size(); i++) {
                articlelist.get(i).setArticle_byte(articlelist.get(i).getArticle_pic());
                articlelist.get(i).setArticle_string(new String(Base64.encodeBase64(articlelist.get(i).getArticle_byte())));
            }
        } catch (Exception e) {
        }
        model.addAttribute("articlelist", articlelist);
        return "articles/searchArticle";
    }

}
