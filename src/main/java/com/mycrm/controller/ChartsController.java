/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycrm.controller;

/**
 *
 * @author Lenovo
 */
import com.ibm.icu.util.Calendar;
import com.mycrm.common.RoleCheck;
import com.mycrm.dao.ChartsDAO;
import com.mycrm.dao.RoleDAO;
import com.mycrm.dao.StaffDAO;
import com.mycrm.domain.Charts;
import com.mycrm.domain.RoleItem;
import com.mycrm.domain.Staff;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class ChartsController {

    @Autowired
    ChartsDAO chartsDAO;
    @Autowired
    RoleDAO roleDAO;
    @Autowired
    StaffDAO staffDAO;
    @Autowired
    HomeController homeController;

    @RequestMapping(value = "/getdata/{timerange}", method = RequestMethod.GET)
    public ResponseEntity<List<Charts>> getdata(@PathVariable String timerange) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd", Locale.ENGLISH);
        Calendar c = Calendar.getInstance();
        c.setTime(new Date());
        c.add(Calendar.DATE, -0);
        Date d = c.getTime();
        int enddate = Integer.parseInt(sdf.format(d));
        int temple_id = HomeController.getTemple_id();
        System.out.println("temple_id charts-->" + temple_id);
        int startdate = 0;
        if (timerange.equals("7")) {
            c.add(Calendar.DATE, -7);
            Date s = c.getTime();
            startdate = Integer.parseInt(sdf.format(s));

        } else if (timerange.equals("30")) {
            c.add(Calendar.DATE, -30);
            Date s = c.getTime();
            startdate = Integer.parseInt(sdf.format(s));

        } else if (timerange.equals("3")) {
            c.add(Calendar.DATE, -90);
            Date s = c.getTime();
            startdate = Integer.parseInt(sdf.format(s));

        } else if (timerange.equals("6")) {
            c.add(Calendar.DATE, -180);
            Date s = c.getTime();
            startdate = Integer.parseInt(sdf.format(s));

        } else if (timerange.equals("12")) {
            c.add(Calendar.DATE, -365);
            Date s = c.getTime();
            startdate = Integer.parseInt(sdf.format(s));

        }
        System.out.println("time" + startdate + " -----------" + enddate);

        return new ResponseEntity<List<Charts>>(chartsDAO.chartsList("" + enddate, "" + startdate, "" + temple_id), HttpStatus.OK);
    }

    @RequestMapping(value = "/ccharts", method = RequestMethod.GET)
    public String charts(Model model) {
        try {
            if (roleDAO.pageView("report", HomeController.getRole_id()) == false) {
                return "redirect:/impervious";
            }

        } catch (Exception e) {
            return "redirect:/";
        }
        
        List<RoleItem> roleItems = new ArrayList<>();
        roleItems = roleDAO.getRoleITem(HomeController.getRole_id());
        Boolean statusadmin = RoleCheck.checkAdmin(roleItems);
        if (statusadmin == true) {
            model.addAttribute("roleadmin", "admin");
        } else if (statusadmin == false) {
            model.addAttribute("roleadmin", "user");
        }

        model = homeController.getRole(model);
        return "charts/charts";
    }
}
