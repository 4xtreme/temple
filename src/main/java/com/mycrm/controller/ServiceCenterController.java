/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycrm.controller;

/**
 *
 * @author Lenovo
 */
import com.mycrm.common.Dateformat;
import com.mycrm.dao.PositionDAO;
import com.mycrm.dao.ServiceCenterDAO;
import com.mycrm.dao.StaffDAO;
import com.mycrm.dao.UserDAO;
import com.mycrm.domain.Machine;
import com.mycrm.domain.Position;
import com.mycrm.domain.ServiceCenter;
import com.mycrm.domain.Staff;
import com.mycrm.domain.User;
import com.mycrm.domain.Withdraw;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;


@Controller
public class ServiceCenterController {
    int PER_PAGE = 5;
    
    @Autowired
    UserDAO userDAO;
    
    @Autowired
    ServiceCenterDAO servicecenterDAO;
    
    
    
        @Autowired
    PositionDAO positionDAO;
        @RequestMapping(value = "/servicecenterlist", method = RequestMethod.GET)
    public String withdrawList(Model model, @RequestParam(value = "page", required = false) String page) throws ParseException {
        if (page == null || Integer.parseInt(page) < 0) {
            page = "0";
        }
        int countRow = 0;
        int page_count = 0;
        countRow = servicecenterDAO.countRows();
        Double pagecountDB = Math.ceil((double) countRow / (double) PER_PAGE);
        page_count = pagecountDB.intValue();
        List<ServiceCenter> servicecenter = new ArrayList<ServiceCenter>();
        servicecenter = servicecenterDAO.getServiceCenterList(page,PER_PAGE);
         if (Integer.parseInt(page) > 0) {
            model.addAttribute("page_priv", true);
        }
        if (Integer.parseInt(page) < (page_count - 1)) {
            model.addAttribute("page_next", true);
        }
        if (page_count >= 10) {
            page_count = 10;
        }
        model.addAttribute("servicecenters", servicecenter);
        model.addAttribute("page_count", page_count);
        model.addAttribute("page", page);
        return "servicecenter/servicecenterlist";
    }
    
    @RequestMapping(value = "/addservicecenter", method = RequestMethod.GET)
    public String contactAll(Model model) {  
        String d_date = "";
        Date date = new Date();
        if (LocaleContextHolder.getLocale().toString().equals("th")) {
            d_date = Dateformat.dateThaiFormatHeader(date);
        } else {
            d_date = Dateformat.dateFormatHeader(date);
        }
        User user = new User();
        int user_id = HomeController.getUser_id();
        try {
            user = userDAO.findUserId(user_id);
        } catch (Exception e) {
            System.out.println("ERROR ===> " + e.getMessage());
        }
        model.addAttribute("user", user);
        model.addAttribute("date", d_date);
        model.addAttribute("servicecenter", new ServiceCenter());
        return "servicecenter/addservicecenter";
    }


      @RequestMapping(value = "/addservicecenter", method = RequestMethod.POST)
    public String addStaff(Model model,@Valid @ModelAttribute("servicecenter") ServiceCenter servicecenter,BindingResult result) {
          servicecenter.setIs_delete(false);
        if (result.hasErrors()) {
             return "servicecenter/addservicecenter";
            }
        servicecenterDAO.insert(servicecenter);
        return "redirect:/servicecenterlist";
    }
    @RequestMapping(value = "/editservicecenter", method = RequestMethod.POST)
    public String edit(Model model,@RequestParam(value = "id") int id) {
        ServiceCenter servicecenter = servicecenterDAO.findServiceCenter(id);
        String d_date = "";
        Date date = new Date();
        if (LocaleContextHolder.getLocale().toString().equals("th")) {
            d_date = Dateformat.dateThaiFormatHeader(date);
        } else {
            d_date = Dateformat.dateFormatHeader(date);
        }
        User user = new User();
        int user_id = HomeController.getUser_id();
        try {
            user = userDAO.findUserId(user_id);
        } catch (Exception e) {
            System.out.println("ERROR ===> " + e.getMessage());
        }
        model.addAttribute("user", user);
        model.addAttribute("date", d_date);
        model.addAttribute("servicecenter", servicecenter);
        return "servicecenter/editservicecenter";
    }
    
         @RequestMapping(value = "/editservicecenters", method = RequestMethod.POST)
    public String editstaff(Model model,@Valid @ModelAttribute("servicecenter") ServiceCenter servicecenter,@RequestParam(value = "id") int id, BindingResult result) {
        servicecenter.setId(id);
        if (result.hasErrors()) {
             return "servicecenter/editservicecenter";
            }
     servicecenterDAO.update(servicecenter);
        return "redirect:/servicecenterlist";
    }
         @RequestMapping(value = "/deleteservicecenter", method = RequestMethod.POST)
    public String deletestaff(Model model,@RequestParam(value = "id") int id) {
        servicecenterDAO.delete(id);
        return "redirect:/servicecenterlist";
    }

}
