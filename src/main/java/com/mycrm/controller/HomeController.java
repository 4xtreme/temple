/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycrm.controller;

/**
 *
 * @author Lenovo
 */
import com.mycrm.common.RoleCheck;
import com.mycrm.dao.ArticlesDAO;
import com.mycrm.dao.ContactDAO;
import com.mycrm.dao.RoleDAO;
import com.mycrm.dao.StaffDAO;
import com.mycrm.dao.TempleDAO;
import com.mycrm.dao.UserDAO;
import com.mycrm.domain.Articles;
import com.mycrm.domain.Computer;
import com.mycrm.domain.Contact;
import com.mycrm.domain.Register;
import com.mycrm.domain.RoleItem;
import com.mycrm.domain.Staff;
import com.mycrm.domain.Temple;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.codec.binary.Base64;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@Component
public class HomeController {

    private static int user_id;

    private static int role_id;

    private static int temple_id;

    public static String getTemple_name() {
        return temple_name;
    }

    private final static String key_hash_profile = "E0B9970BB727ADBC9F2B61969865F3F1";

    public static void setTemple_name(String aTemple_name) {
        temple_name = aTemple_name;
    }

    public static String getKey_hash_profile() {
        return key_hash_profile;
    }
    private int staff_id = 1;
    private static String temple_name;

    public static int getUser_id() {
        return user_id;
    }

    public static void setUser_id(int user_id) {
        HomeController.user_id = user_id;
    }

    public static int getRole_id() {
        return role_id;
    }

    public static void setRole_id(int role_id) {
        HomeController.role_id = role_id;
    }

    public static int getTemple_id() {
        return temple_id;
    }

    public static void setTemple_id(int temple_id) {
        HomeController.temple_id = temple_id;
    }

    @Autowired
    UserDAO userDAO;

    @Autowired
    StaffDAO staffDAO;

    @Autowired
    ArticlesDAO articlesDAO;

    @Autowired
    TempleDAO templeDAO;

    @Autowired
    ContactDAO contactDAO;

    @Autowired
    RoleDAO roleDAO;

    private int PER_PAGE = 5;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String loginpage(Model model, @RequestParam(value = "page", required = false) String page) {
        //-------------------pagenigation---------------------------------------
        if (page == null || Integer.parseInt(page) < 0) {
            page = "0";
        }
        int countRow = 0;
        int page_count = 0;
        countRow = articlesDAO.countRowsAll();
        Double pagecountDB = Math.ceil((double) countRow / (double) PER_PAGE);
        page_count = pagecountDB.intValue();
        List<Articles> articlelist = new ArrayList<Articles>();
        articlelist = articlesDAO.getArticlePage(page, PER_PAGE);
        try {
            for (int i = 0; i < articlelist.size(); i++) {
                articlelist.get(i).setArticle_byte(articlelist.get(i).getArticle_pic());
                articlelist.get(i).setArticle_string(new String(Base64.encodeBase64(articlelist.get(i).getArticle_byte())));
            }
        } catch (Exception e) {
        }
        if (Integer.parseInt(page) > 0) {
            model.addAttribute("page_priv", true);
        }
        if (Integer.parseInt(page) < (page_count - 1)) {
            model.addAttribute("page_next", true);
        }
        if (page_count >= 10) {
            page_count = 10;
        }
        //---------------------pagenigation--------------------------------------
        List<Temple> temple = new ArrayList<Temple>();
        temple = templeDAO.getALL();
        model.addAttribute("temple", temple);
        model.addAttribute("page_count", page_count);
        model.addAttribute("page", page);
        model.addAttribute("articlelist", articlelist);
        model.addAttribute("status", null);
        return "jsp/login";
    }

    @RequestMapping(value = "/home", method = RequestMethod.GET)
    public String printWelcome() {
        System.out.println("Get user id" + user_id);
        return "jsp/home";
    }

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public String check(Model model, @RequestParam(value = "email", required = true) String email, @RequestParam(value = "password", required = true) String password,
            @RequestParam(value = "temple_id") int temple_id) throws SQLException {
        System.out.println("---------------Login---------------");
        System.out.println("temple id : " + temple_id);
        System.out.println("Email:    " + email);
        System.out.println("password: " + password);
//             User userlogin = new User();
        try {
            Register contact = contactDAO.findContactByEmail(email, temple_id);

            int is_delete = 0;
            is_delete = contactDAO.findEmailIsdelete(email);
            // check user is delete 
            if (is_delete == 1) {
                String page = "0";
                int countRow = 0;
                int page_count = 0;
                countRow = articlesDAO.countRowsAll();
                Double pagecountDB = Math.ceil((double) countRow / (double) PER_PAGE);
                page_count = pagecountDB.intValue();
                List<Articles> articlelist = new ArrayList<Articles>();
                articlelist = articlesDAO.getArticlePage(page, PER_PAGE);
                for (int i = 0; i < articlelist.size(); i++) {
                    articlelist.get(i).setArticle_byte(articlelist.get(i).getArticle_pic());
                    articlelist.get(i).setArticle_string(new String(Base64.encodeBase64(articlelist.get(i).getArticle_byte())));
                }
                if (Integer.parseInt(page) > 0) {
                    model.addAttribute("page_priv", true);
                }
                if (Integer.parseInt(page) < (page_count - 1)) {
                    model.addAttribute("page_next", true);
                }
                if (page_count >= 10) {
                    page_count = 10;
                }
                //---------------------pagenigation--------------------------------------
                List<Temple> temple = new ArrayList<Temple>();
                temple = templeDAO.getALL();
                model.addAttribute("temple", temple);
                model.addAttribute("page_count", page_count);
                model.addAttribute("page", page);
                model.addAttribute("articlelist", articlelist);
                model.addAttribute("status_del", "delete");
                return "jsp/login";
            }

            if (password.equals(contact.getPassword())) {
                HomeController.setUser_id(contact.getReg_id());
                HomeController.setTemple_id(temple_id);
                return "redirect:/contactprofile";
            }
        } catch (Exception e) {
            String page = "0";
            int countRow = 0;
            int page_count = 0;
            countRow = articlesDAO.countRowsAll();
            Double pagecountDB = Math.ceil((double) countRow / (double) PER_PAGE);
            page_count = pagecountDB.intValue();
            List<Articles> articlelist = new ArrayList<Articles>();
            articlelist = articlesDAO.getArticlePage(page, PER_PAGE);
            for (int i = 0; i < articlelist.size(); i++) {
                articlelist.get(i).setArticle_byte(articlelist.get(i).getArticle_pic());
                articlelist.get(i).setArticle_string(new String(Base64.encodeBase64(articlelist.get(i).getArticle_byte())));
            }
            if (Integer.parseInt(page) > 0) {
                model.addAttribute("page_priv", true);
            }
            if (Integer.parseInt(page) < (page_count - 1)) {
                model.addAttribute("page_next", true);
            }
            if (page_count >= 10) {
                page_count = 10;
            }
            //---------------------pagenigation--------------------------------------
            List<Temple> temple = new ArrayList<Temple>();
            temple = templeDAO.getALL();
            model.addAttribute("temple", temple);
            model.addAttribute("page_count", page_count);
            model.addAttribute("page", page);
            model.addAttribute("articlelist", articlelist);
            model.addAttribute("status", null);
            model.addAttribute("status", "ERROR");
            return "jsp/login";
        }

        return "redirect:/";
    }

    @RequestMapping(value = "/loginstaff", method = RequestMethod.GET)
    public String login_staff(Model model) {
        model.addAttribute("status", null);
        List<Temple> temple = new ArrayList<Temple>();
        temple = templeDAO.getALL();
        model.addAttribute("temple", temple);
        return "jsp/login_staff";
    }

    @RequestMapping(value = "/loginstaff", method = RequestMethod.POST)
    public String loginstaff(Model model, @RequestParam(value = "email", required = true) String email, @RequestParam(value = "password", required = true) String password,
            @RequestParam(value = "temple_id") int temple_id) {
        try {
            Staff userprofile = staffDAO.findStaffByEmail(email, temple_id);

            List<RoleItem> roleItems = new ArrayList<>();
            roleItems = roleDAO.getRoleITem(HomeController.getRole_id());
            Boolean statusadmin = RoleCheck.checkAdmin(roleItems);
            if (statusadmin == true) {
                model.addAttribute("roleadmin", "admin");
            } else if (statusadmin == false) {
                model.addAttribute("roleadmin", "user");
            }

            if (password.equals(userprofile.getPassword())) {
                HomeController.setUser_id(userprofile.getId());
                HomeController.setRole_id(userprofile.getRole_id());
                HomeController.setTemple_id(userprofile.getTemple_id());
                Temple temple = templeDAO.findTemple(userprofile.getTemple_id());
                HomeController.setTemple_name(temple.getName());
//                return "redirect:/profile";
                return "redirect:/roomfront";
            }
        } catch (Exception e) {
            List<Temple> temple = new ArrayList<Temple>();
            temple = templeDAO.getALL();
            model.addAttribute("temple", temple);
            model.addAttribute("status", "error");
//            return "jsp/login_staff";
            return "redirect:/login";
        }
//        List<Temple> temple = new ArrayList<Temple>();
//        temple = templeDAO.getALL();
//        model.addAttribute("temple", temple);
//        return "jsp/login_staff";
        return "redirect:/login";
    }

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String login(Model model, @RequestParam(value = "page", required = false) String page) {
        //-------------------pagenigation---------------------------------------
        if (page == null || Integer.parseInt(page) < 0) {
            page = "0";
        }
        int countRow = 0;
        int page_count = 0;
        countRow = articlesDAO.countRowsAll();
        Double pagecountDB = Math.ceil((double) countRow / (double) PER_PAGE);
        page_count = pagecountDB.intValue();
        List<Articles> articlelist = new ArrayList<Articles>();
        articlelist = articlesDAO.getArticlePage(page, PER_PAGE);
        try {
            for (int i = 0; i < articlelist.size(); i++) {
                articlelist.get(i).setArticle_byte(articlelist.get(i).getArticle_pic());
                articlelist.get(i).setArticle_string(new String(Base64.encodeBase64(articlelist.get(i).getArticle_byte())));
            }
        } catch (Exception e) {
        }
        if (Integer.parseInt(page) > 0) {
            model.addAttribute("page_priv", true);
        }
        if (Integer.parseInt(page) < (page_count - 1)) {
            model.addAttribute("page_next", true);
        }
        if (page_count >= 10) {
            page_count = 10;
        }
        //---------------------pagenigation--------------------------------------
        List<Temple> temple = new ArrayList<Temple>();
        temple = templeDAO.getALL();
        model.addAttribute("temple", temple);
        model.addAttribute("page_count", page_count);
        model.addAttribute("page", page);
        model.addAttribute("articlelist", articlelist);
        model.addAttribute("status", null);
        return "jsp/login";
    }

    @RequestMapping(value = "/produceby", method = RequestMethod.GET)
    public String produce() {
        return "jsp/produceby";
    }

    @RequestMapping(value = "/loginstaff1", method = RequestMethod.GET)
    public String login_staff1(Model model) {
        model.addAttribute("status", null);
        List<Temple> temple = new ArrayList<Temple>();
        //temple = templeDAO.getALL();
        temple = templeDAO.getALLTest();
        model.addAttribute("temple", temple);
        model.addAttribute("pop", 1);
        return "jsp/login_staff_1";
    }

    public Model getRole(Model model) {
        Staff staff = staffDAO.findStaff(HomeController.getUser_id());
        System.out.println(staff.getRole_id());
        RoleItem templePage = new RoleItem();
        RoleItem roomPage = new RoleItem();
        RoleItem staffPage = new RoleItem();
        RoleItem memberPage = new RoleItem();
        RoleItem checkinPage = new RoleItem();
        RoleItem checkoutPage = new RoleItem();
        RoleItem rolePage = new RoleItem();
        RoleItem monkPage = new RoleItem();
        RoleItem contentPage = new RoleItem();
        RoleItem reportPage = new RoleItem();
        templePage = roleDAO.getRoleItemByPage("temple_page", staff.getRole_id());
        roomPage = roleDAO.getRoleItemByPage("room_page", staff.getRole_id());
        staffPage = roleDAO.getRoleItemByPage("staff_page", staff.getRole_id());
        memberPage = roleDAO.getRoleItemByPage("member_page", staff.getRole_id());
        checkinPage = roleDAO.getRoleItemByPage("checkin", staff.getRole_id());
        checkoutPage = roleDAO.getRoleItemByPage("checkout", staff.getRole_id());
        rolePage = roleDAO.getRoleItemByPage("role", staff.getRole_id());
        monkPage = roleDAO.getRoleItemByPage("monk", staff.getRole_id());
        contentPage = roleDAO.getRoleItemByPage("content", staff.getRole_id());
        reportPage = roleDAO.getRoleItemByPage("report", staff.getRole_id());
        
        model.addAttribute("temple_page", templePage);
        model.addAttribute("room_page", roomPage);
        model.addAttribute("staff_page", staffPage);
        model.addAttribute("member_page", memberPage);
        model.addAttribute("checkin_page", checkinPage);
        model.addAttribute("checkout_page", checkoutPage);
        model.addAttribute("role_page", rolePage);
        model.addAttribute("monk_page", monkPage);
        model.addAttribute("content_page", contentPage);
        model.addAttribute("report_page", reportPage);
        return model;
    }

}
