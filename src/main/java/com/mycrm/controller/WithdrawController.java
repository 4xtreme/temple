///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
//package com.mycrm.controller;
//
//import com.amazonaws.auth.BasicAWSCredentials;
//import com.amazonaws.regions.Region;
//import com.amazonaws.regions.Regions;
//import com.amazonaws.services.s3.AmazonS3;
//import com.amazonaws.services.s3.AmazonS3Client;
//import com.amazonaws.services.s3.model.CannedAccessControlList;
//import com.amazonaws.services.s3.model.GetObjectRequest;
//import com.amazonaws.services.s3.model.ObjectMetadata;
//import com.amazonaws.services.s3.model.PutObjectRequest;
//import com.amazonaws.services.s3.model.S3Object;
//import com.mycrm.common.Dateformat;
//import com.mycrm.dao.MachineDAO;
//import com.mycrm.dao.OldOrderWithdrawDAO;
//import com.mycrm.dao.OldWithdrawDAO;
//import com.mycrm.dao.OrderWithdrawDAO;
//import com.mycrm.dao.ProjectDAO;
//import com.mycrm.dao.StaffDAO;
//import com.mycrm.dao.UserDAO;
//import com.mycrm.dao.WithdrawDAO;
//import com.mycrm.domain.Machine;
//import com.mycrm.domain.OldWithdraw;
//import com.mycrm.domain.OldorderWithdrawlist;
//import com.mycrm.domain.OrderWithdrawlist;
//import com.mycrm.domain.Project;
//import com.mycrm.domain.Staff;
//import com.mycrm.domain.User;
//import com.mycrm.domain.Withdraw;
//import java.io.IOException;
//import java.text.ParseException;
//import java.text.SimpleDateFormat;
//import java.util.ArrayList;
//import java.util.Calendar;
//import java.util.Date;
//import java.util.List;
//import javax.validation.Valid;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.context.i18n.LocaleContextHolder;
//import org.springframework.http.HttpStatus;
//import org.springframework.http.ResponseEntity;
//import org.springframework.stereotype.Controller;
//import org.springframework.ui.Model;
//import org.springframework.validation.BindingResult;
//import org.springframework.web.bind.annotation.ModelAttribute;
//import org.springframework.web.bind.annotation.PathVariable;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestMethod;
//import org.springframework.web.bind.annotation.RequestParam;
//import org.springframework.web.bind.annotation.ResponseBody;
//import org.springframework.web.multipart.MultipartFile;
//
///**
// *
// * @author pop
// */
//@Controller
//public class WithdrawController {
//
//    int PER_PAGE = 5;
//
//    @Autowired
//    MachineDAO machineDAO;
//
//    @Autowired
//    UserDAO userDAO;
//
//    @Autowired
//    WithdrawDAO withdrawDAO;
//
//    @Autowired
//    OrderWithdrawDAO orderWithdrawDAO;
//
//    @Autowired
//    OldWithdrawDAO oldWithdrawDAO;
//
//    @Autowired
//    OldOrderWithdrawDAO oldOrderWithdrawDAO;
//
//    @Autowired
//    StaffDAO staffDAO;
//
//    @Autowired
//    ProjectDAO projectDAO;
//
//    @RequestMapping(value = "/withdrawlist", method = RequestMethod.GET)
//    public String withdrawList(Model model, @RequestParam(value = "page", required = false) String page) throws ParseException {
//        if (page == null || Integer.parseInt(page) < 0) {
//            page = "0";
//        }
//        int countRow = 0;
//        int page_count = 0;
//        countRow = withdrawDAO.countRows();
//        Double pagecountDB = Math.ceil((double) countRow / (double) PER_PAGE);
//        page_count = pagecountDB.intValue();
//        List<Withdraw> withdraws = new ArrayList<Withdraw>();
//        withdraws = withdrawDAO.getWithdrawList(page, PER_PAGE);
//        List<Project> projectlist = new ArrayList<Project>();
//        List<Staff> staff = new ArrayList<Staff>();
//        staff = staffDAO.getStaffList(page, PER_PAGE);
//        projectlist = projectDAO.getProjectList();
//        if (Integer.parseInt(page) > 0) {
//            model.addAttribute("page_priv", true);
//        }
//        if (Integer.parseInt(page) < (page_count - 1)) {
//            model.addAttribute("page_next", true);
//        }
//        if (page_count >= 10) {
//            page_count = 10;
//        }
//        model.addAttribute("withdraws", withdraws);
//        model.addAttribute("page_count", page_count);
//        model.addAttribute("page", page);
//        model.addAttribute("project", projectlist);
//        model.addAttribute("staff", staff);
//        return "withdraw/withdrawlist";
//    }
//
//    @RequestMapping(value = "/addwithdraw", method = RequestMethod.GET)
//    public String addWithdraw(Model model, @RequestParam(value = "page", required = false) String page) throws ParseException {
//        if (page == null || Integer.parseInt(page) < 0) {
//            page = "0";
//        }
//        String d_date = "";
//        Date date = new Date();
//        if (LocaleContextHolder.getLocale().toString().equals("th")) {
//            d_date = Dateformat.dateThaiFormatHeader(date);
//        } else {
//            d_date = Dateformat.dateFormatHeader(date);
//        }
//        List<Project> projectlist = new ArrayList<Project>();
//        User user = new User();
//        List<Staff> staff = new ArrayList<Staff>();
//        int user_id = HomeController.getUser_id();
//        try {
//            user = userDAO.findUserId(user_id);
//            staff = staffDAO.getStaffList(page, PER_PAGE);
//            projectlist = projectDAO.getProjectList();
//        } catch (Exception e) {
//            System.out.println("ERROR ===> " + e.getMessage());
//        }
//        System.out.println("User Info =========> " + user.toString());
//        System.out.println("Staff Info =========> " + staff.toString());
//        model.addAttribute("project", projectlist);
//        model.addAttribute("staff", staff);
//        model.addAttribute("withdraw", new Withdraw());
//        model.addAttribute("user", user);
//        model.addAttribute("date", d_date);
//
//        return "withdraw/addwithdraw";
//
//    }
//
//    @RequestMapping(value = "/searchmachine/{searchnum}", method = RequestMethod.GET)
//    public @ResponseBody
//    ResponseEntity<Machine> searchMachine(Model model, @PathVariable String searchnum) {
//        System.out.println("input data =====>" + searchnum);
//        Machine machine = new Machine();
//        try {
//            machine = machineDAO.searchData(searchnum);
//            System.out.println("Search = " + machine.toString());
//        } catch (Exception e) {
//            e.getMessage();
//        }
//
//        model.addAttribute("machine", machine);
//        return new ResponseEntity<Machine>(machine, HttpStatus.OK);
//    }
//
//    @RequestMapping(value = "/addwithdraw", method = RequestMethod.POST)
//    public String addwithdrawlist(Model model, @Valid @ModelAttribute("withdraw") Withdraw withdraw,
//            BindingResult result, @RequestParam(value = "itemslist") List<String> itemlist,
//            @RequestParam(value = "file_withdraw", required = false) MultipartFile file_withdraw) {
//
//        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//        Date date = new Date();
//        String dateFormat = formatter.format(date);
//
//        int user_id = HomeController.getUser_id();
//
//        if (result.hasErrors()) {
//            String errorField = result.getFieldError().getField();
//            System.out.println("Don't have some Field: " + errorField);
//            if (file_withdraw.getOriginalFilename().equals("")) {
//                System.out.println("Don't have some file to upload.");
//
//                return "withdraw/addwithdraw";
//            } else if (!errorField.equals("withdraw_file")) {
//                System.out.println("Some field empty");
//
//                return "withdraw/addwithdraw";
//            } else {
//                System.out.println("file upload OK.");
//
//            }
//        }
//        withdraw.setCreate_date(dateFormat);
//        withdraw.setUpdate_date(dateFormat);
//        try {
//            withdraw.setWithdraw_file(uploadFile(file_withdraw));
//        } catch (Exception e) {
//            System.out.println("ERROR =====> " + e.getMessage());
//            withdraw.setWithdraw_file("No File Selectedd");
//        }
//        withdraw.setUser_id(user_id);
//        System.out.println("info ========> " + withdraw.toString());
//
//        int withdraw_id = withdrawDAO.insert(withdraw);
//
//        //add to old withdraw
//        OldWithdraw oldWithdraw = new OldWithdraw(withdraw_id, withdraw.getUser_id(), withdraw.getWithdrawal_id(), withdraw.getDriver_id(), withdraw.getAmount(), withdraw.getProject_id(), withdraw.getPay_type(), withdraw.getPay_date(), withdraw.getPay_date_count(), withdraw.getStore_id(), withdraw.getMachine_id(), withdraw.getCreate_date(), withdraw.getUpdate_date(), withdraw.getWithdraw_file(), withdraw.getWithdrawal_note(), withdraw.getInspector_note(), withdraw.getApprovers_note(), withdraw.getTotal_price(), withdraw.getStatus());
//        int oldwithdraw_id = oldWithdrawDAO.insert(oldWithdraw);
//
//        System.out.println("table ========> " + itemlist.toString() + " size ====> " + itemlist.size());
//        try {
//            int count = 1;
//            for (String number : itemlist.subList(0, itemlist.size() - 1)) {
//                System.out.println("");
//
//                System.out.print("Itemlist: ");
//                String[] objSp = number.split(",");
//                System.out.print("|| Index: " + objSp[0]);
//                System.out.print("|| BrokenItem: " + objSp[1]);
//                System.out.print("|| Type: " + objSp[2]);
//                System.out.print("|| QTY: " + objSp[3]);
//                System.out.print("|| price: " + objSp[4]);
//                System.out.print("|| Sum: " + objSp[5]);
//                System.out.print("|| Note: " + objSp[6]);
//                System.out.print("|| is_delete: " + objSp[7]);
//                System.out.println("");
//                int indexNum = count;
//                int qty = Integer.parseInt(objSp[3]);
//                int price = Integer.parseInt(objSp[4]);
//                int sum = Integer.parseInt(objSp[5]);
//                count++;
//                System.out.println("");
//                OrderWithdrawlist orderWithdrawList = new OrderWithdrawlist(withdraw_id, indexNum, objSp[1], objSp[2], qty, price, sum, objSp[6], objSp[7]);
//                orderWithdrawDAO.insert(orderWithdrawList);
//                OldorderWithdrawlist oldOrderWIthdrawList = new OldorderWithdrawlist(oldwithdraw_id, withdraw_id, indexNum, objSp[1], objSp[2], qty, price, sum, objSp[6], objSp[7]);
//                oldOrderWithdrawDAO.insert(oldOrderWIthdrawList);
//                System.out.println("");
//
//            }
//        } catch (Exception e) {
//            e.getMessage();
//        }
//        return "redirect:/withdrawlist";
//    }
//
//    @RequestMapping(value = "/editwithdraw", method = RequestMethod.POST)
//    public String editWithdraw(Model model, @RequestParam(value = "editid") int editid, @RequestParam(value = "page", required = false) String page) throws ParseException {
//
//        if (page == null || Integer.parseInt(page) < 0) {
//            page = "0";
//        }
//        Withdraw withdraw = new Withdraw();
//        List<OrderWithdrawlist> orderWithdrawlist = new ArrayList<OrderWithdrawlist>();
//        Machine machine = new Machine(1,"-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-",false);
//
//        //Get Who login
//        User login_id = new User();
//        int user_id = HomeController.getUser_id();
//        login_id = userDAO.findUserId(user_id);
//        //get withdraw
//        withdraw = withdrawDAO.findWithdrawId(editid);
//        System.out.println("withdraw ===> " + withdraw.toString());
//        //get order withdraw list
//        orderWithdrawlist = orderWithdrawDAO.getOrderWithdrawList(editid);
//        System.out.println("OrderWithdraw ===> " + orderWithdrawlist.toString());
//        //get staff list
//        List<Staff> staff = new ArrayList<Staff>();
//        staff = staffDAO.getStaffList(page, PER_PAGE);
//        //get project list
//        List<Project> projectlist = new ArrayList<Project>();
//        projectlist = projectDAO.getProjectList();
//
//        if (withdraw.getMachine_id() != 0) {
//            machine = machineDAO.findMachineId(withdraw.getMachine_id());
//            System.out.println("Machine ===> " + machine.toString());
//        }
//        String d_date = "";
//        Date date = new Date();
//        if (LocaleContextHolder.getLocale().toString().equals("th")) {
//            d_date = Dateformat.dateThaiFormatHeader(date);
//        } else {
//            d_date = Dateformat.dateFormatHeader(date);
//        }
//
//        //Get Who Create Withdraw
//        User user = new User();
//        user = userDAO.findUserId(withdraw.getUser_id());
//        System.out.println("User Info =========> " + user.toString());
//
//        model.addAttribute("user", user);
//        model.addAttribute("date", d_date);
//        model.addAttribute("withdraw", withdraw);
//        model.addAttribute("orderWithdrawlist", orderWithdrawlist);
//        model.addAttribute("machine", machine);
//        model.addAttribute("login_user", login_id);
//        model.addAttribute("staff", staff);
//        model.addAttribute("project", projectlist);
//
//        return "withdraw/editwithdraw";
//    }
//
//    @RequestMapping(value = "/editedwithdraw", method = RequestMethod.POST)
//    public String editwithdrawlist(Model model, @Valid @ModelAttribute("withdraw") Withdraw withdraw,
//            BindingResult result, @RequestParam(value = "itemslist") List<String> itemlist,
//            @RequestParam(value = "file_withdraw", required = false) MultipartFile file_withdraw,
//            @RequestParam(value = "oldfile", required = false) String old_file) {
//
//        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//        Date date = new Date();
//        String dateFormat = formatter.format(date);
//
//        int user_id = HomeController.getUser_id();
//
//        if (result.hasErrors()) {
//            String errorField = result.getFieldError().getField();
//            System.out.println("Don't have some Field: " + errorField);
//            if (file_withdraw.getOriginalFilename().equals("")) {
//                System.out.println("Don't have some file to upload.");
//
//                return "withdraw/addwithdraw";
//            } else if (!errorField.equals("withdraw_file")) {
//                System.out.println("Some field empty");
//
//                return "withdraw/addwithdraw";
//            } else {
//                System.out.println("file upload OK.");
//
//            }
//        }
//
//        System.out.println("file ========> " + file_withdraw.toString());
//        withdraw.setUpdate_date(dateFormat);
//        withdraw.setUser_id(user_id);
//
//        try {
//            withdraw.setWithdraw_file(uploadFile(file_withdraw));
//        } catch (Exception e) {
//            System.out.println("ERROR = " + e.getMessage());
//            withdraw.setWithdraw_file(old_file);
//        }
//
//        System.out.println("info ========> " + withdraw.toString());
//
//        withdrawDAO.update(withdraw);
//
//        //add to old withdraw
//        OldWithdraw oldWithdraw = new OldWithdraw(withdraw.getId(), withdraw.getUser_id(), withdraw.getWithdrawal_id(), withdraw.getDriver_id(), withdraw.getAmount(), withdraw.getProject_id(), withdraw.getPay_type(), withdraw.getPay_date(), withdraw.getPay_date_count(), withdraw.getStore_id(), withdraw.getMachine_id(), withdraw.getCreate_date(), withdraw.getUpdate_date(), withdraw.getWithdraw_file(), withdraw.getWithdrawal_note(), withdraw.getInspector_note(), withdraw.getApprovers_note(), withdraw.getTotal_price(), withdraw.getStatus());
//        int oldwithdraw_id = oldWithdrawDAO.insert(oldWithdraw);
//
//        System.out.println("table ========> " + itemlist.toString() + " size ====> " + itemlist.size());
//
//        try {
//            int count = 1;
//            for (String number : itemlist.subList(0, itemlist.size() - 1)) {
//
//                System.out.print("Itemlist: ");
//                String[] objSp = number.split(",");
//                System.out.print("|| Id: " + objSp[0]);
//                System.out.print("|| Index: " + objSp[1]);
//                System.out.print("|| BrokenItem: " + objSp[2]);
//                System.out.print("|| Type: " + objSp[3]);
//                System.out.print("|| QTY: " + objSp[4]);
//                System.out.print("|| price: " + objSp[5]);
//                System.out.print("|| Sum: " + objSp[6]);
//                System.out.print("|| Note: " + objSp[7]);
//                System.out.print("|| is_delete: " + objSp[8]);
//                System.out.print("|| Status: " + objSp[9]);
//                System.out.println("");
//                int id = Integer.parseInt(objSp[0]);
//                int indexNum = count;
//                int qty = Integer.parseInt(objSp[4]);
//                int price = Integer.parseInt(objSp[5]);
//                int sum = Integer.parseInt(objSp[6]);
//                count++;
//
//                if (objSp[9].equals("old")) {
//                    OrderWithdrawlist orderWithdrawList = new OrderWithdrawlist(id, withdraw.getId(), indexNum, objSp[2], objSp[3], qty, price, sum, objSp[7], objSp[8]);
//                    System.out.println("Update Database ====> " + orderWithdrawList.toString());
//                    orderWithdrawDAO.update(orderWithdrawList);
//                } else if (objSp[9].equals("new")) {
//                    OrderWithdrawlist orderWithdrawList = new OrderWithdrawlist(withdraw.getId(), indexNum, objSp[2], objSp[3], qty, price, sum, objSp[7], objSp[8]);
//                    System.out.println("Insert Database ====> " + orderWithdrawList.toString());
//                    orderWithdrawDAO.insert(orderWithdrawList);
//                }
//
//                OldorderWithdrawlist oldOrderWIthdrawList = new OldorderWithdrawlist(oldwithdraw_id, withdraw.getId(), indexNum, objSp[2], objSp[3], qty, count, sum, objSp[7], objSp[8]);
//                oldOrderWithdrawDAO.insert(oldOrderWIthdrawList);
//
//            }
//        } catch (Exception e) {
//            System.out.println("ERROR =====> " + e.getMessage());
//        }
//
//        return "redirect:/withdrawlist";
//    }
//
//    public String uploadFile(MultipartFile file) {
//
//        //Amazon set Credentials 
//        AmazonS3 s3client = new AmazonS3Client(new BasicAWSCredentials("AKIAIVWJITLXBJK5M2UQ", "vIpbKGkoY3vJa7KP/Z8V5VjGm7SFXMxeBn/0iJcf"));
//        s3client.setRegion(Region.getRegion(Regions.fromName("ap-southeast-1")));
//        String bucketname = "fidesla";
//
//        //Rename Name: file + datetime + filetype
//        String filename = file.getOriginalFilename();
//        Calendar cal = Calendar.getInstance();
//        SimpleDateFormat dateFormat = new SimpleDateFormat("YYYYMMddHHmmssSSS");
//        String nowTime = dateFormat.format(cal.getTime());
//        int spaceIndex = filename.indexOf(".");
//        int setendName = spaceIndex;
//        if (setendName > 15) {
//            setendName = 15;
//        }
//        String newfilename = filename.substring(0, setendName) + nowTime + filename.substring(spaceIndex);
//        System.out.println("datetime: " + nowTime);
//        System.out.println("Original name: " + filename);
//        System.out.println("New file name: " + newfilename);
//        String filelink = "";
//
//        //Start upload process
//        try {
//            s3client.putObject(new PutObjectRequest(bucketname, newfilename, file.getInputStream(), new ObjectMetadata())
//                    .withCannedAcl(CannedAccessControlList.PublicRead));
//            System.out.println("Upload complete!");
//            S3Object s3Object = s3client.getObject(new GetObjectRequest(bucketname, newfilename));
//
//            filelink = s3Object.getObjectContent().getHttpRequest().getURI().toString();
//
//            System.out.println("Link: " + filelink);
//
//        } catch (IOException ex) {
//            ;
//        }
//
//        return filelink;
//    }
//
//}
