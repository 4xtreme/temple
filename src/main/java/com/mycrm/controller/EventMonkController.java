/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycrm.controller;

import com.mycrm.common.RoleCheck;
import com.mycrm.dao.ContactDAO;
import com.mycrm.dao.EventsDAO;
import com.mycrm.dao.RoleDAO;
import com.mycrm.dao.StaffDAO;
import com.mycrm.domain.EventInfo;
import com.mycrm.domain.Events;
import com.mycrm.domain.Monk;
import com.mycrm.domain.RoleItem;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author SOMON
 */
@Controller
public class EventMonkController {

    @Autowired
    ContactDAO contactDAO;

    @Autowired
    EventsDAO eventsDAO;

    @Autowired
    HomeController homeController;

    int monk_id = 10;
    int event_id = 10;
    Events events;
    private int PER_PAGE = 5;

    @Autowired
    RoleDAO roleDAO;

    @Autowired
    StaffDAO staffDAO;

    @InitBinder
    public void initBinder(WebDataBinder binder) {

        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        sdf.setLenient(true);
        binder.registerCustomEditor(Date.class, new CustomDateEditor(sdf, true));

    }

    @RequestMapping(value = "/eventsmonk", method = RequestMethod.GET)
    public String eventsMonk(Model model) {
        try {
            if (roleDAO.pageCreate("temple_page", HomeController.getRole_id()) == false) {
                return "redirect:/impervious";
            }

        } catch (Exception e) {
            return "redirect:/";
        }

        List<RoleItem> roleItems = new ArrayList<>();
        roleItems = roleDAO.getRoleITem(HomeController.getRole_id());
        Boolean statusadmin = RoleCheck.checkAdmin(roleItems);
        if (statusadmin == true) {
            model.addAttribute("roleadmin", "admin");
        } else if (statusadmin == false) {
            model.addAttribute("roleadmin", "user");
        }

        int temple_id = HomeController.getTemple_id();
        List<Monk> monklist = contactDAO.getmonkList(monk_id, temple_id);
        System.out.println("name : " + monklist.toString());
        model = homeController.getRole(model);
        model.addAttribute("events", new Events());
        model.addAttribute("monklist", monklist);
        model.addAttribute("action", "addevents");

        return "eventMonk/events";
    }

    @RequestMapping(value = "/addevents", method = RequestMethod.POST)
    public String insert(Model model,
            @Valid @ModelAttribute("events") Events events, BindingResult result,
            @RequestParam(value = "event_start") String start,
            @RequestParam(value = "event_end") String end) throws ParseException {
        System.out.println(events.toString());
//        events.setMonk_id(monk_id);
        if (result.hasErrors()) {

            List<Monk> monks = new ArrayList<>();
            for (Integer monk_id : events.getMonks_id()) {
                monks.add(contactDAO.findmonkInfo(monk_id));
            }
            events.setMonks(monks);

            List<Monk> monklist = new ArrayList<Monk>();
            monklist = contactDAO.getmonkList(events.getMonk_id(), HomeController.getTemple_id());
            for (int i = 0; i < monklist.size(); i++) {
                for (Monk monk : events.getMonks()) {
                    if (monklist.get(i).getMonk_id() == monk.getMonk_id()) {
                        monklist.get(i).setMonk_id(0);
                    }
                }
            }

            List<RoleItem> roleItems = new ArrayList<>();
            roleItems = roleDAO.getRoleITem(HomeController.getRole_id());
            Boolean statusadmin = RoleCheck.checkAdmin(roleItems);
            if (statusadmin == true) {
                model.addAttribute("roleadmin", "admin");
            } else if (statusadmin == false) {
                model.addAttribute("roleadmin", "user");
            }

            int temple_id = HomeController.getTemple_id();
            System.out.println("name : " + monklist.toString());
            model = homeController.getRole(model);
//            model.addAttribute("events", new Events());
            model.addAttribute("monklist", monklist);
            model.addAttribute("action", "addevents");
            return "eventMonk/events";
        }

        events.setTemple_id(HomeController.getTemple_id());
        System.out.println("start : " + start);
        System.out.println("end : " + end);

        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy", new Locale("th","TH"));
        Date startDate = sdf.parse(start);
        Date endDate = sdf.parse(end);
        events.setEvent_start(startDate);
        events.setEvent_end(endDate);

        int event_id = eventsDAO.insert(events);
        for (Integer monk_id : events.getMonks_id()) {
            EventInfo eventInfo = new EventInfo(0, event_id, monk_id, new Date(), new Date(), false);
            eventsDAO.insertEventInfo(eventInfo);
        }

        return "redirect:/eventsall";
    }

    @RequestMapping(value = "/eventsall", method = RequestMethod.GET)
    public String showEvents(Model model, @RequestParam(value = "page", required = false) String page) {
        String page_Create = "OK";
        String page_Delete = "OK";
        String page_Edit = "OK";
        try {
            if (roleDAO.pageView("temple_page", HomeController.getRole_id()) == false) {
                return "redirect:/impervious";
            }
        } catch (Exception e) {
            return "redirect:/";
        }

        if (roleDAO.pageCreate("temple_page", HomeController.getRole_id()) == false) {
            page_Create = null;
        }

        if (roleDAO.pageEdit("temple_page", HomeController.getRole_id()) == false) {
            page_Edit = null;
        }
        if (roleDAO.pageDelete("temple_page", HomeController.getRole_id()) == false) {
            page_Delete = null;
        }
        if (page == null || Integer.parseInt(page) < 0) {
            page = "0";
        }
        int countRow = 0;
        int page_count = 0;
        int temple_id = HomeController.getTemple_id();
        countRow = eventsDAO.countRows(temple_id);
        Double pagecountDB = Math.ceil((double) countRow / (double) PER_PAGE);
        page_count = pagecountDB.intValue();
        List<Events> events = new ArrayList<Events>();
        events = eventsDAO.geteventsPage(temple_id, page, PER_PAGE);
        for (int i = 0; i < events.size(); i++) {
            List<EventInfo> eventInfos = new ArrayList<>();
            eventInfos = eventsDAO.findEventInfo(events.get(i).getEvent_id());
            List<Monk> monks = new ArrayList<>();
            for (EventInfo eventInfo : eventInfos) {
                monks.add(contactDAO.findmonkInfo(eventInfo.getMonk_id()));
            }
            events.get(i).setMonks(monks);
        }

        List<RoleItem> roleItems = new ArrayList<>();
        roleItems = roleDAO.getRoleITem(HomeController.getRole_id());
        Boolean statusadmin = RoleCheck.checkAdmin(roleItems);
        if (statusadmin == true) {
            model.addAttribute("roleadmin", "admin");
        } else if (statusadmin == false) {
            model.addAttribute("roleadmin", "user");
        }

        if (Integer.parseInt(page) > 0) {
            model.addAttribute("page_priv", true);
        }
        if (Integer.parseInt(page) < (page_count - 1)) {
            model.addAttribute("page_next", true);
        }
        if (page_count >= 10) {
            page_count = 10;
        }
        model = homeController.getRole(model);
        model.addAttribute("page_count", page_count);
        model.addAttribute("page", page);
        System.out.println("Get data : " + events.toString());
        model.addAttribute("eventslist", events);
        model.addAttribute("page_Create", page_Create);
        model.addAttribute("page_Delete", page_Delete);
        model.addAttribute("page_Edit", page_Edit);
        return "eventMonk/eventsAll";
        //return null;
    }

    @RequestMapping(value = "/selecteditevents", method = RequestMethod.GET)
    public String selectedit(Model model, @RequestParam(value = "event_id") int event_id) {
        System.out.println("Select event_id : " + event_id);
        Events eventEdit = eventsDAO.findById(event_id);
        List<EventInfo> eventInfos = new ArrayList<>();
        eventInfos = eventsDAO.findEventInfo(eventEdit.getEvent_id());
        List<Monk> monks = new ArrayList<>();
        for (EventInfo eventInfo : eventInfos) {
            monks.add(contactDAO.findmonkInfo(eventInfo.getMonk_id()));
        }
        eventEdit.setMonks(monks);

        List<Monk> monklist = new ArrayList<Monk>();
        monklist = contactDAO.getmonkList(monk_id, HomeController.getTemple_id());
        for (int i = 0; i < monklist.size(); i++) {
            for (Monk monk : eventEdit.getMonks()) {
                if (monklist.get(i).getMonk_id() == monk.getMonk_id()) {
                    monklist.get(i).setMonk_id(0);
                }
            }
        }
        model.addAttribute("monklist", monklist);
        List<RoleItem> roleItems = new ArrayList<>();
        roleItems = roleDAO.getRoleITem(HomeController.getRole_id());
        Boolean statusadmin = RoleCheck.checkAdmin(roleItems);
        model = homeController.getRole(model);
        model.addAttribute("events", eventEdit);
        if (statusadmin == true) {
            model.addAttribute("roleadmin", "admin");
        } else if (statusadmin == false) {
            model.addAttribute("roleadmin", "user");
        }

        return "eventMonk/eventsEdit";
    }

    @RequestMapping(value = "/updateEvent", method = RequestMethod.POST)
    public String editUpdate(Model model, @Valid @ModelAttribute("events") Events events, BindingResult result,
            @RequestParam(value = "event_start") String event_start,
            @RequestParam(value = "event_end") String event_end) throws ParseException {
        System.out.println("date start : " + event_start);
        System.out.println("Date end : " + event_end);
        System.out.println("Events:" + events.toString());
        if (result.hasErrors()) {
            List<Monk> monks = new ArrayList<>();
            for (Integer monk_id : events.getMonks_id()) {
                monks.add(contactDAO.findmonkInfo(monk_id));
            }
            events.setMonks(monks);

            List<Monk> monklist = new ArrayList<Monk>();
            monklist = contactDAO.getmonkList(events.getMonk_id(), HomeController.getTemple_id());
            for (int i = 0; i < monklist.size(); i++) {
                for (Monk monk : events.getMonks()) {
                    if (monklist.get(i).getMonk_id() == monk.getMonk_id()) {
                        monklist.get(i).setMonk_id(0);
                    }
                }
            }
            model.addAttribute("monklist", monklist);
            List<RoleItem> roleItems = new ArrayList<>();
            roleItems = roleDAO.getRoleITem(HomeController.getRole_id());
            Boolean statusadmin = RoleCheck.checkAdmin(roleItems);
            model = homeController.getRole(model);
//            model.addAttribute("events", eventEdit);
            if (statusadmin == true) {
                model.addAttribute("roleadmin", "admin");
            } else if (statusadmin == false) {
                model.addAttribute("roleadmin", "user");
            }
            return "eventMonk/eventsEdit";
        }

        //------------------convert----------------------------
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy", new Locale("th","TH"));
        Date startDate = sdf.parse(event_start);
        Date endDate = sdf.parse(event_end);
        events.setEvent_start(startDate);
        events.setEvent_end(endDate);

        //-------------------sendDAO---------------------------
        System.out.println("Controller : " + events.toString());
        eventsDAO.update(events);
        for (Integer monk_id : events.getMonks_id()) {
            EventInfo eventInfo = new EventInfo(0, events.getEvent_id(), monk_id, new Date(), new Date(), false);
            eventsDAO.insertEventInfo(eventInfo);
        }

        for (Integer monk_id : events.getDelete_monk_id()) {

            eventsDAO.deleteEventInfo(events.getEvent_id(), monk_id);
        }
//        return null;
        return "redirect:/eventsall";
    }

    @RequestMapping(value = "/deleteevent", method = RequestMethod.POST)
    public String deleteEvent(@RequestParam(value = "event_id") int event_id) {
        System.out.println("Event ID : " + event_id);
        eventsDAO.delete(event_id);
        return "redirect:/eventsall";

    }

    @RequestMapping(value = "/viewevents", method = RequestMethod.POST)
    public String view(Model model, @RequestParam(value = "event_id") int event_id) {
        System.out.println("Select event_id : " + event_id);
        Events eventEdit = eventsDAO.findById(event_id);
        System.out.println("Controller : " + eventEdit.toString());
        model.addAttribute("eventmonk", eventEdit);
        int temple_id = HomeController.getTemple_id();
        List<Monk> monklist = contactDAO.getmonkList(monk_id, temple_id);
        System.out.println("name : " + monklist.toString());
        model.addAttribute("monklist", monklist);
        model.addAttribute("monkid", eventEdit.getMonk_id());

        List<RoleItem> roleItems = new ArrayList<>();
        roleItems = roleDAO.getRoleITem(HomeController.getRole_id());
        Boolean statusadmin = RoleCheck.checkAdmin(roleItems);
        if (statusadmin == true) {
            model.addAttribute("roleadmin", "admin");
        } else if (statusadmin == false) {
            model.addAttribute("roleadmin", "user");
        }
        return "eventMonk/view";
    }

}
