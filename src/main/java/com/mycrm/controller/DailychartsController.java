package com.mycrm.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.ibm.icu.util.Calendar;
import com.mycrm.common.RoleCheck;
import com.mycrm.dao.CheckinChartsDAO;
import com.mycrm.dao.ContactDAO;
import com.mycrm.dao.DailychartsDAO;
import com.mycrm.dao.RoleDAO;
import com.mycrm.dao.StaffDAO;
import com.mycrm.domain.ReportManager;
import com.mycrm.domain.RoleItem;
import java.text.ParseException;
import com.mycrm.domain.Staff;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class DailychartsController {

    @Autowired
    DailychartsDAO DailyChartsDAO;

    @Autowired
    RoleDAO roleDAO;

    @Autowired
    ContactDAO contactDAO;

    @Autowired
    CheckinChartsDAO checkinChartsDAO;

    @Autowired
    StaffDAO staffDAO;

    @Autowired
    HomeController homeController;

    ReportManager getReport = new ReportManager();

    @RequestMapping(value = "/get_data/{val}", method = RequestMethod.GET)
    public ResponseEntity<List<List<String>>> get_data(@PathVariable String val) throws JsonProcessingException {

        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd", Locale.ENGLISH);
        SimpleDateFormat ssdf = new SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH);

        int start = Integer.parseInt(val);

        ArrayList<String> arrList = new ArrayList<String>();
        ArrayList<String> arrList2 = new ArrayList<String>();
        ArrayList<List<String>> arrList3 = new ArrayList<List<String>>();
        ArrayList<String> new1 = new ArrayList<String>();
        ArrayList<String> new2 = new ArrayList<String>();
        int i = 0;

        for (i = 1; i <= start; i++) {

            Calendar c = Calendar.getInstance();
            c.setTime(new Date());
            c.add(Calendar.DATE, -i);
            Date d = c.getTime();

            int day = Integer.parseInt(sdf.format(d));
            System.out.println("d ------" + d);
            System.out.println("day ------" + day);
            System.out.println("c ------" + c);
            int x = DailyChartsDAO.dailychartslist("" + day);

            System.out.println("จำนวนคน---->" + x);

            arrList.add("" + x);
            arrList2.add("" + ssdf.format(d));
//            System.out.println("arrList----->" + arrList);
//            System.out.println("arrList2----->" + arrList2);
        }
        int j = 0;
        for (j = arrList.size() - 1; j >= 0; j--) {
            new1.add(arrList.get(j));
            System.out.println("new1 ------" + new1);
        }
        int k = 0;
        for (k = arrList2.size() - 1; k >= 0; k--) {
            new2.add(arrList2.get(k));
            System.out.println("new2 ------" + new2);
        }
        arrList3.add(new1);
        arrList3.add(new2);
        System.out.println("arrList3----->" + arrList3);
        return new ResponseEntity<List<List<String>>>(arrList3, HttpStatus.OK);
    }

    @RequestMapping(value = "/dailycharts", method = RequestMethod.GET)
    public String charts(Model model) {
        try {
            if (roleDAO.pageView("report", HomeController.getRole_id()) == false) {
                return "redirect:/impervious";
            }

        } catch (Exception e) {
            return "redirect:/";
        }

        List<RoleItem> roleItems = new ArrayList<>();
        roleItems = roleDAO.getRoleITem(HomeController.getRole_id());
        Boolean statusadmin = RoleCheck.checkAdmin(roleItems);
        if (statusadmin == true) {
            model.addAttribute("roleadmin", "admin");
        } else if (statusadmin == false) {
            model.addAttribute("roleadmin", "user");
        }

        model = homeController.getRole(model);

        return "charts/dailycharts";

    }
}
