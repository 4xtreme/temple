/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycrm.controller;

import com.ibm.icu.util.Calendar;
import com.mycrm.common.RoleCheck;
import com.mycrm.dao.CheckinChartsDAO;
import com.mycrm.dao.ContactDAO;
import com.mycrm.dao.RoleDAO;
import com.mycrm.dao.StaffDAO;
import com.mycrm.domain.CheckinCharts;
import com.mycrm.domain.Monk;
import com.mycrm.domain.RoleItem;
import com.mycrm.domain.Staff;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author SOMON
 */
@Controller
public class CheckinChartsController {

    @Autowired
    CheckinChartsDAO checkinchartsDAO;

    @Autowired
    ContactDAO contactDAO;

    @Autowired
    StaffDAO staffDAO;

    @Autowired
    RoleDAO roleDAO;
    
    @Autowired 
    HomeController homeController;

    @RequestMapping(value = "/getdatacheckin/{timerange}", method = RequestMethod.GET)
    public ResponseEntity<Map> getdata(@PathVariable String timerange) {
//        chartsDAO.chartsList(timerange);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        SimpleDateFormat ssdf = new SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH);
//        Calendar c = Calendar.getInstance();
//        c.setTime(new Date());
//        c.add(Calendar.DATE, -0);
//        Date d = c.getTime();
        int start = Integer.parseInt(timerange);
        Map map = new HashMap();

        ArrayList<String> new2 = new ArrayList<String>();
        int i = 0;

        ArrayList<String> arrList2 = new ArrayList<String>();
        ArrayList<Object> monkName = new ArrayList<Object>();
        List<Object> data = new ArrayList<Object>();
        List<Object> data2 = new ArrayList<Object>();
        List<Monk> monks = new ArrayList<Monk>();
        monks = contactDAO.getMonkByTemple(HomeController.getTemple_id());
        System.out.println("Monk:" + monks.toString());
        int count = 1;
        for (Monk single_monk : monks) {
            count++;
            String[] monk = new String[2];
            monk[0] = single_monk.getFirstname() + "   " + single_monk.getLastname();
            monk[1] = single_monk.getMonk_id() + "";
            monkName.add(monk);
        }

        for (i = start; i >= 0; i--) {
            List<CheckinCharts> x = new ArrayList<CheckinCharts>();
            int temple_id = HomeController.getTemple_id();
            Calendar c = Calendar.getInstance();
            c.setTime(new Date());
            c.add(Calendar.DATE, -i);

            Date d = c.getTime();

            String day = sdf.format(d);
            System.out.println("Day:" + day);
            System.out.println("Temple:" + temple_id);
            List<CheckinCharts> chart = new ArrayList<CheckinCharts>();
            chart = checkinchartsDAO.charts(temple_id, "" + day);

            System.out.println("X:" + chart.size());
            String monk_name1 = "";
            if (chart.isEmpty()) {

            } else {
                for (CheckinCharts checkinCharts : chart) {
                    x.add(checkinCharts);

                }
            }
            data.add(x);

            arrList2.add("" + ssdf.format(d));
            System.out.println("arrList----->" + data);
            System.out.println("arrList2----->" + arrList2);
        }
        int j = 0;
        for (j = data.size() - 1; j >= 0; j--) {
            data2.add(data.get(j));

        }
        int k = 0;
        for (k = arrList2.size() - 1; k >= 0; k--) {
            new2.add(arrList2.get(k));

        }
        map.put("data", data2);
        map.put("date", new2);
        map.put("count", count - 1);
        map.put("monk", monkName);

        return new ResponseEntity<Map>(map, HttpStatus.OK);
//        int enddate = Integer.parseInt(sdf.format(d));
//
//        int startdate = 0;
//        if (timerange.equals("7")) {
//            c.add(Calendar.DATE, + 7);
//            Date s = c.getTime();
//            startdate = Integer.parseInt(sdf.format(s));
//
//        } else if (timerange.equals("30")) {
//            c.add(Calendar.DATE, +30);
//            Date s = c.getTime();
//            startdate = Integer.parseInt(sdf.format(s));
//
//        }
//
//        System.out.println("time" + startdate + " -----------" + enddate);
//        System.out.println("Controler:  "+ timerange.toString());

//        return new ResponseEntity<List<CheckinCharts>>(checkinchartsDAO.chartsList(temple_id,"" + enddate, "" + startdate), HttpStatus.OK);
    }

    @RequestMapping(value = "/chartscheckin", method = RequestMethod.GET)
    public String charts(Model model) {
        
        try {
            if (roleDAO.pageView("report", HomeController.getRole_id()) == false) {
                return "redirect:/impervious";
            }

        } catch (Exception e) {
            return "redirect:/";
        }
        
        List<RoleItem> roleItems = new ArrayList<>();
        roleItems = roleDAO.getRoleITem(HomeController.getRole_id());
        Boolean statusadmin = RoleCheck.checkAdmin(roleItems);
        if (statusadmin == true) {
            model.addAttribute("roleadmin", "admin");
        } else if (statusadmin == false) {
            model.addAttribute("roleadmin", "user");
        }

        model = homeController.getRole(model);

        return "checkin/checkincharts";
    }

//    @RequestMapping(value = "/chartscheckin")
//    public void chartsCSV(HttpServletResponse response) throws IOException{
//
//            response.setContentType("text/csv");
//            String reportName = "สมาชิก";
//            response.setHeader("Content-disposition", "attachment;filename="+reportName);
//    }
}
