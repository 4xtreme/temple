/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycrm.controller;

/**
 *
 * @author Lenovo
 */
import com.mycrm.common.RoleCheck;
import com.mycrm.dao.BookingDAO;
import com.mycrm.dao.ContactDAO;
import com.mycrm.dao.RoleDAO;
import com.mycrm.dao.RoomDAO;
import com.mycrm.dao.StaffDAO;
import com.mycrm.dao.TempleDAO;
import com.mycrm.dao.VipassanaDAO;

import com.mycrm.domain.CheckinHistory;
import com.mycrm.domain.Monk;
import com.mycrm.domain.MonkDate;
import com.mycrm.domain.Register;
import com.mycrm.domain.RoleItem;

import com.mycrm.domain.Room;
import com.mycrm.domain.RoomName;
import com.mycrm.domain.Vipassana;
import java.text.DateFormat;
import java.text.Format;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@Component
public class BookingRoomController {

    int PER_PAGE = 5;

    @Autowired
    RoomDAO roomDAO;

    @Autowired
    TempleDAO templeDAO;

    @Autowired
    BookingDAO bookingDAO;

    @Autowired
    ContactDAO contactDAO;

    @Autowired
    RoleDAO roleDAO;

    @Autowired
    StaffDAO staffDAO;

    @Autowired
    HomeController homeController;

    @Autowired
    VipassanaDAO vipassanaDAO;

    @RequestMapping(value = "/bookingRoom", method = RequestMethod.GET)
    public String bookingRoom(Model model) throws ParseException {
        try {
            if (roleDAO.pageCreate("temple_page", HomeController.getRole_id()) == false) {
                return "redirect:/impervious";
            }

        } catch (Exception e) {
            return "redirect:/";
        }
        String page = "0";
        String personal_cardid = "";

        int page_count = 0;
        page_count = 0;
        List<RoomName> rooms = new ArrayList<RoomName>();
        List<Room> builder = new ArrayList<Room>();
        List<Monk> monklist = new ArrayList<Monk>();
        List<Vipassana> vipassanas = new ArrayList<>();
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH);
        Date today = Calendar.getInstance().getTime();
        String dateval = df.format(today);
        //
        Date dt1 = new Date();
        Calendar c1 = Calendar.getInstance();
        c1.setTime(dt1);
        c1.add(Calendar.DATE, 1);
        dt1 = c1.getTime();
        Format formatter = new SimpleDateFormat("dd/MM/yyy",Locale.ENGLISH);
        String datetmr = formatter.format(dt1);
        System.out.println("datetmr : "+datetmr);
        model.addAttribute("datetmr", datetmr);
        // getMonkByTemple
        monklist = contactDAO.getMonkByTemple(HomeController.getTemple_id());
        builder = roomDAO.getRoomByTemple(HomeController.getTemple_id());
        vipassanas = vipassanaDAO.findAll(HomeController.getTemple_id(), "0", 1000);
        model.addAttribute("vipassanas", vipassanas);
        model.addAttribute("monklist", monklist);
        model.addAttribute("builder", builder);
        model.addAttribute("rooms", rooms);
        model.addAttribute("page_count", page_count);
        model.addAttribute("page", page);
        model.addAttribute("startdate", "");
        model.addAttribute("enddate", "");
        model.addAttribute("dateval", dateval);
        model.addAttribute("personal_cardid", personal_cardid);
        model.addAttribute("status", null);

        ArrayList<Integer> count_array = new ArrayList<Integer>();
        for (int i = 1; i <= 30; i++) {
            count_array.add(i); // count day by monk.size
        }
        List<MonkDate> monkDate = new ArrayList<MonkDate>();
        for (Vipassana add : vipassanas) {
            monkDate.add(new MonkDate(add.getFirstname(), bookingDAO.getCountMonkDate(add.getId(), getDateCompare(0)), bookingDAO.getCountMonkDate(add.getId(), getDateCompare(1)), bookingDAO.getCountMonkDate(add.getId(), getDateCompare(2)), bookingDAO.getCountMonkDate(add.getId(), getDateCompare(3)), bookingDAO.getCountMonkDate(add.getId(), getDateCompare(4)), bookingDAO.getCountMonkDate(add.getId(), getDateCompare(5)), bookingDAO.getCountMonkDate(add.getId(), getDateCompare(6)), bookingDAO.getCountMonkDate(add.getId(), getDateCompare(7)), bookingDAO.getCountMonkDate(add.getId(), getDateCompare(8)), bookingDAO.getCountMonkDate(add.getId(), getDateCompare(9))));
        }

        List<RoleItem> roleItems = new ArrayList<>();
        roleItems = roleDAO.getRoleITem(HomeController.getRole_id());
        Boolean statusadmin = RoleCheck.checkAdmin(roleItems);
        if (statusadmin == true) {
            model.addAttribute("roleadmin", "admin");
        } else if (statusadmin == false) {
            model.addAttribute("roleadmin", "user");
        }

        ArrayList<String> date_table = new ArrayList<String>();
        for (int i = 0; i <= 9; i++) {
            Date dt = new Date();
            Calendar c = Calendar.getInstance();
            c.setTime(dt);
            c.add(Calendar.DATE, i);
            dt = c.getTime();
            date_table.add(dateThaiFormat(dt));
        }
        model = homeController.getRole(model);

        model.addAttribute("monk_date", monkDate);
        model.addAttribute("count_array", count_array);
        model.addAttribute("date_table", date_table);
        return "bookingroom/bookingroom";
    }

    @RequestMapping(value = "/bookingRoom", method = RequestMethod.POST)
    public String searchBookingRoom(Model model, @RequestParam(value = "start", required = false) String start,       
            @RequestParam(value = "page", required = false) String page,
            @RequestParam(value = "personal_cardid") String personal_cardid,
            @RequestParam(value = "vipassana_id") int vipassana_id,
            @RequestParam(value = "end") String end
    ) throws ParseException {
        int count = 0;
        System.out.println("start : " + start);
        System.out.println("end : " + end);
        if (page == null || Integer.parseInt(page) < 0) {
            page = "0";
        }

        model = homeController.getRole(model);

        List<Monk> monklist = new ArrayList<Monk>();
        monklist = contactDAO.getMonkByTemple(HomeController.getTemple_id());
        List<Vipassana> vipassanas = new ArrayList<>();
        vipassanas = vipassanaDAO.findAll(HomeController.getTemple_id(), "0", 1000);
        model.addAttribute("vipassanas", vipassanas);
        ArrayList<Integer> count_array = new ArrayList<Integer>();
        for (int i = 1; i <= 30; i++) {
            count_array.add(i);
        }
        List<MonkDate> monkDate = new ArrayList<MonkDate>();
        for (Vipassana add : vipassanas) {
            monkDate.add(new MonkDate(add.getFirstname(), bookingDAO.getCountMonkDate(add.getId(), getDateCompare(0)), bookingDAO.getCountMonkDate(add.getId(), getDateCompare(1)), bookingDAO.getCountMonkDate(add.getId(), getDateCompare(2)), bookingDAO.getCountMonkDate(add.getId(), getDateCompare(3)), bookingDAO.getCountMonkDate(add.getId(), getDateCompare(4)), bookingDAO.getCountMonkDate(add.getId(), getDateCompare(5)), bookingDAO.getCountMonkDate(add.getId(), getDateCompare(6)), bookingDAO.getCountMonkDate(add.getId(), getDateCompare(7)), bookingDAO.getCountMonkDate(add.getId(), getDateCompare(8)), bookingDAO.getCountMonkDate(add.getId(), getDateCompare(9))));
        }

        ArrayList<String> date_table = new ArrayList<String>();
        for (int i = 0; i <= 9; i++) {
            Date dt = new Date();
            Calendar c = Calendar.getInstance();
            c.setTime(dt);
            c.add(Calendar.DATE, i);
            dt = c.getTime();
            date_table.add(dateThaiFormat(dt));
        }

        model.addAttribute("monk_date", monkDate);
        model.addAttribute("count_array", count_array);
        model.addAttribute("date_table", date_table);

        String encryptPersonIDString = RegisterController.encrypt(personal_cardid, HomeController.getKey_hash_profile());

        Register contact = new Register();

        // Search contact
        System.out.println("encryptPersonIDString =  " + encryptPersonIDString);

        List<RoleItem> roleItems = new ArrayList<>();
        roleItems = roleDAO.getRoleITem(HomeController.getRole_id());
        Boolean statusadmin = RoleCheck.checkAdmin(roleItems);
        if (statusadmin == true) {
            model.addAttribute("roleadmin", "admin");
        } else if (statusadmin == false) {
            model.addAttribute("roleadmin", "user");
        }

        try {
            contact = contactDAO.getContactByPersonID(encryptPersonIDString);
        } catch (Exception e) {
            int page_count = 0;
            page_count = 0;
            List<RoomName> rooms = new ArrayList<RoomName>();
            List<Room> builder = new ArrayList<Room>();
            DateFormat df = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH);
            Date today = Calendar.getInstance().getTime();
            String dateval = df.format(today);
            // getMonkByTemple

            model.addAttribute("builder", builder);
            model.addAttribute("rooms", rooms);
            model.addAttribute("page_count", page_count);
            model.addAttribute("page", page);
            model.addAttribute("startdate", dateval);
            
            model.addAttribute("dateval", dateval);
            model.addAttribute("personal_cardid", "");
            model.addAttribute("status", "error");
            model.addAttribute("vipassana_id", vipassana_id);

            return "bookingroom/bookingroom";
        }
        
        if(contact.getIs_delete() == true){
            model.addAttribute("contact",contact);
            return "bookingroom/banned";
        }

        SimpleDateFormat formatt = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH);
        SimpleDateFormat formatt5 = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH);
        SimpleDateFormat format2 = new SimpleDateFormat("dd/MM/yyyy");
        Date startdate = formatt.parse(start);
        Date enddate = formatt5.parse(end);
        System.out.println("change year in thai : "+enddate);
//        Calendar c = Calendar.getInstance();
//        c.setTime(enddate);
//        c.set(Calendar.YEAR, enddate.getYear()-543);
//        c.add(Calendar.DATE, count);
        enddate.setYear(enddate.getYear()-543);
        System.out.println("Start Date  " + startdate);
        System.out.println("End Date    " + enddate);
        List<CheckinHistory> checkinHistorys = new ArrayList<CheckinHistory>();

        checkinHistorys = bookingDAO.getCheckinHistoryBytemple(HomeController.getTemple_id());
        System.out.println(checkinHistorys.toString());

        // 
        for (int i = 0; i < checkinHistorys.size(); i++) {
            System.out.println("Round " + i);
            System.out.println("Reserve Check in: " + checkinHistorys.get(i).getCheck_in());
            System.out.println("Reserve Check out: " + checkinHistorys.get(i).getCheck_out());
            if (contact.getReg_id() == checkinHistorys.get(i).getContact_id()) {
                System.out.println("You have Reserve ");

                return "bookingroom/alreadybooking";

            }

            if (startdate.after(checkinHistorys.get(i).getCheck_in()) && startdate.before(checkinHistorys.get(i).getCheck_out())) {
                System.out.println("Start " + startdate + " >  Check IN " + checkinHistorys.get(i).getCheck_in());
                System.out.println("Start " + startdate + " <  Check OUT " + checkinHistorys.get(i).getCheck_out());
                System.out.println("Have People in");
                updateReserveRoom(checkinHistorys.get(i).getRoom_name_id());
                continue;

            }
            if (enddate.after(checkinHistorys.get(i).getCheck_in()) && enddate.before(checkinHistorys.get(i).getCheck_out())) {
                System.out.println("END " + enddate + " >  Check IN " + checkinHistorys.get(i).getCheck_in());
                System.out.println("END " + enddate + " <  Check OUT " + checkinHistorys.get(i).getCheck_out());
                System.out.println("Have People in");
                updateReserveRoom(checkinHistorys.get(i).getRoom_name_id());
                continue;
            }

            if (startdate.before(checkinHistorys.get(i).getCheck_in()) && enddate.after(checkinHistorys.get(i).getCheck_out())) {
                System.out.println("Between Date Have People in");
                updateReserveRoom(checkinHistorys.get(i).getRoom_name_id());
                continue;
            }

            if (startdate.equals(checkinHistorys.get(i).getCheck_in()) || enddate.equals(checkinHistorys.get(i).getCheck_in())) {
                System.out.println("Same Date check in Have People in");
                updateReserveRoom(checkinHistorys.get(i).getRoom_name_id());
                continue;
            }

            if (startdate.equals(checkinHistorys.get(i).getCheck_out()) || enddate.equals(checkinHistorys.get(i).getCheck_out())) {
                System.out.println("Same Date check out Have People in");
                updateReserveRoom(checkinHistorys.get(i).getRoom_name_id());
                continue;
            }

        }
        int countRow = 0;
        int page_count = 0;
        if (contact.getGender().equals("ต่างชาติชาย") || contact.getGender().equals("ภิกษุ") || contact.getGender().equals("สามเณร") || contact.getGender().equals("ชาย")) {
            contact.setGender("ชาย");
        } else {
            contact.setGender("หญิง");
        }
        countRow = roomDAO.countRowSearch(HomeController.getTemple_id(), page, PER_PAGE, contact.getGender());
        System.out.println("countRow" + countRow);
        Double pagecountDB = Math.ceil((double) countRow / (double) PER_PAGE);
        page_count = pagecountDB.intValue();

        List<RoomName> rooms = new ArrayList<RoomName>();
        rooms = roomDAO.getRoomNameSearch(page, PER_PAGE);

        List<Room> builder = new ArrayList<Room>();

        System.out.println("Contact " + contact.toString());

        builder = roomDAO.getRoomSearch(HomeController.getTemple_id(), page, PER_PAGE, contact.getGender());

        System.out.println("getRoomSearch = " + builder.toString());
        if (Integer.parseInt(page) > 0) {
            model.addAttribute("page_priv", true);

        }
        if (Integer.parseInt(page) < (page_count - 1)) {
            model.addAttribute("page_next", true);
        }
        if (page_count >= 10) {
            page_count = 10;
        }
        model.addAttribute("contact_id", contact.getReg_id());
        model.addAttribute("builder", builder);
        model.addAttribute("rooms", rooms);
        model.addAttribute("page_count", page_count);
        model.addAttribute("page", page);

        String stringdate1 = format2.format(startdate);
        String stringdate2 = format2.format(enddate);
        String raw_indate = formatt.format(startdate);
        String raw_ouString = formatt.format(enddate);
        model.addAttribute("startdate", stringdate1);
        model.addAttribute("enddate", stringdate2);
        model.addAttribute("raw_indate", raw_indate);
        model.addAttribute("raw_outdate", raw_ouString);
        model.addAttribute("datetmr", stringdate2);
        model.addAttribute("dateval", start);
        model.addAttribute("personal_cardid", personal_cardid);
        model.addAttribute("vipassana_id", vipassana_id);
        // getMonkByTemple

        roomDAO.updateRoomReserve();
        return "bookingroom/bookingroom";
    }

    public void updateReserveRoom(int room_id) {
        RoomName roomName = new RoomName();
        roomName = roomDAO.getRoomNameByID(room_id);
        roomName.setLive(roomName.getLive() + 1);
        if (roomName.getAmount() == roomName.getLive()) {
            roomName.setStatus(1);
        } else {
            roomName.setStatus(0);
        }

        roomDAO.updateRoomName(roomName);
    }

    @RequestMapping(value = "/checkin", method = RequestMethod.POST)
    public String checkIn(Model model, @RequestParam("check_in") String startDate,
            @RequestParam("check_out") String endDate,
            @RequestParam("room_id") String room_id,
            @RequestParam("room_name_id") String room_name_id,
            @RequestParam("personal_id") String personal_id,
            @RequestParam("temple_id") String temple_id,
            @RequestParam("contact_id") String contact_id,
            @RequestParam("raw_indate") String raw_inDate,
            @RequestParam("raw_outdate") String raw_outDate,
            @RequestParam(value = "vipassana_id") int vipassana_id) throws ParseException {

        SimpleDateFormat formatt = new SimpleDateFormat("dd/MM/yyyy");
        Date checkIn = formatt.parse(raw_inDate);
        Date checkOut = formatt.parse(raw_outDate);
        Calendar c = Calendar.getInstance();
        Date createDate = c.getTime();
        Date updateDate = c.getTime();

        System.out.println("Start Date Format " + checkIn);
        System.out.println("End Date Format " + checkOut);

        // add CheckinHistory
        CheckinHistory checkinHistory = new CheckinHistory(0, checkIn, checkOut, Integer.parseInt(temple_id),
                Integer.parseInt(room_id), Integer.parseInt(room_name_id), "เช็คอิน",
                Integer.parseInt(contact_id), createDate, updateDate, false, vipassana_id);
        bookingDAO.insert(checkinHistory);
        boolean first_come = bookingDAO.countCheckin(Integer.parseInt(contact_id), HomeController.getTemple_id());
        if (first_come == true) {
            contactDAO.updateFirstCome(Integer.parseInt(contact_id));
        }

        return "redirect:bookingRoom";
    }

    @RequestMapping(value = "/confirmbook", method = RequestMethod.POST)
    public String confirmBook(Model model, @RequestParam("check_in") String startDate,
            @RequestParam("check_out") String endDate,
            @RequestParam("room_id") String room_id,
            @RequestParam("room_name_id") String room_name_id,
            @RequestParam("personal_id") String personal_id,
            @RequestParam("room_name") String room_name,
            @RequestParam("contact_id") String contact_id,
            @RequestParam("raw_indate") String raw_indate,
            @RequestParam("raw_outdate") String raw_outdate,
            @RequestParam(value = "vipassana_id") int vipassana_id) {

        Room room = new Room();
        room = roomDAO.find(Integer.parseInt(room_id));
        Vipassana vipassana = new Vipassana();
        vipassana = vipassanaDAO.findById(vipassana_id);
        model.addAttribute("check_in", startDate);
        model.addAttribute("check_out", endDate);
        model.addAttribute("temple_id", room.getTemple_id());
        model.addAttribute("room_id", room_id);
        model.addAttribute("room_name_id", room_name_id);
        model.addAttribute("personal_id", personal_id);
        model.addAttribute("room_name", room_name);
        model.addAttribute("builder_name", room.getName());
        model.addAttribute("contact_id", contact_id);
        model.addAttribute("raw_indate", raw_indate);
        model.addAttribute("raw_outdate", raw_outdate);
        model.addAttribute("vipassana_id", vipassana_id);
        model.addAttribute("vipassana_name", vipassana.getFirstname() + "  " + vipassana.getLastname());

        List<RoleItem> roleItems = new ArrayList<>();
        roleItems = roleDAO.getRoleITem(HomeController.getRole_id());
        Boolean statusadmin = RoleCheck.checkAdmin(roleItems);
        if (statusadmin == true) {
            model.addAttribute("roleadmin", "admin");
        } else if (statusadmin == false) {
            model.addAttribute("roleadmin", "user");
        }

        model = homeController.getRole(model);
        return "bookingroom/confirmbooking";

    }

    public static String dateThaiFormat(Date dateformat) {
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        String datetime = df.format(dateformat);
        return datetime;
    }

    public static String dateFormatSQL(Date dateformat) {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        String datetime = df.format(dateformat);
        return datetime;
    }

    public static String getDateCompare(int number) {
        Date d_sql = new Date();
        Calendar c = Calendar.getInstance();
        c.setTime(d_sql);
        c.add(Calendar.DATE, number);
        d_sql = c.getTime();
        return dateFormatSQL(d_sql);
    }
}
