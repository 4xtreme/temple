/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycrm.domain;

/**
 *
 * @author SOMON
 */
public class CheckinCharts {
    
    private int value;
    private String name;
    private String monk_name;

    public CheckinCharts() {
    }

    public CheckinCharts(int value, String name, String monk_name) {
        this.value = value;
        this.name = name;
        this.monk_name = monk_name;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMonk_name() {
        return monk_name;
    }

    public void setMonk_name(String monk_name) {
        this.monk_name = monk_name;
    }

    @Override
    public String toString() {
        return "CheckinCharts{" + "value=" + value + ", name=" + name + ", monk_name=" + monk_name + '}';
    }

    
    
    
}
