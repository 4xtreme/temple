/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycrm.domain;

/**
 *
 * @author pop
 */
public class ContactReport {
    private String contact_name;
    private String age;
    private String address;
    private String village;
    private String district;
    private String city;
    private String province;
    private String postalcode;
    private String first_come;
    private Integer count_come;

    public ContactReport() {
    }

    public ContactReport(String contact_name, String age, String address, String village, String district, String city, String province, String postalcode, String first_come, Integer count_come) {
        this.contact_name = contact_name;
        this.age = age;
        this.address = address;
        this.village = village;
        this.district = district;
        this.city = city;
        this.province = province;
        this.postalcode = postalcode;
        this.first_come = first_come;
        this.count_come = count_come;
    }

    public String getContact_name() {
        return contact_name;
    }

    public void setContact_name(String contact_name) {
        this.contact_name = contact_name;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getVillage() {
        return village;
    }

    public void setVillage(String village) {
        this.village = village;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getPostalcode() {
        return postalcode;
    }

    public void setPostalcode(String postalcode) {
        this.postalcode = postalcode;
    }

    public String getFirst_come() {
        return first_come;
    }

    public void setFirst_come(String first_come) {
        this.first_come = first_come;
    }

    public Integer getCount_come() {
        return count_come;
    }

    public void setCount_come(Integer count_come) {
        this.count_come = count_come;
    }

    @Override
    public String toString() {
        return "ContactReport{" + "contact_name=" + contact_name + ", age=" + age + ", address=" + address + ", village=" + village + ", district=" + district + ", city=" + city + ", province=" + province + ", postalcode=" + postalcode + ", first_come=" + first_come + ", count_come=" + count_come + '}';
    }
  
}
