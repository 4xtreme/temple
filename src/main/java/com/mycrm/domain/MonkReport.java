/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycrm.domain;

import java.util.Date;

/**
 *
 * @author pop
 */
public class MonkReport {

    private Integer vipassanaId;
    private String contactName;
    private String roomName;
    private Date checkOut;
    private String dateFormat;

    public MonkReport() {
    }

    public MonkReport(Integer vipassanaId, String contactName, String roomName, Date checkOut) {
        this.vipassanaId = vipassanaId;
        this.contactName = contactName;
        this.roomName = roomName;
        this.checkOut = checkOut;
    }

    public Integer getVipassanaId() {
        return vipassanaId;
    }

    public void setVipassanaId(Integer vipassanaId) {
        this.vipassanaId = vipassanaId;
    }

    public String getContactName() {
        return contactName;
    }

    public void setContactName(String contactName) {
        this.contactName = contactName;
    }

    public String getRoomName() {
        return roomName;
    }

    public void setRoomName(String roomName) {
        this.roomName = roomName;
    }

    public Date getCheckOut() {
        return checkOut;
    }

    public void setCheckOut(Date checkOut) {
        this.checkOut = checkOut;
    }

    public String getDateFormat() {
        return dateFormat;
    }

    public void setDateFormat(String dateFormat) {
        this.dateFormat = dateFormat;
    }

    @Override
    public String toString() {
        return "MonkReport{" + "vipassanaId=" + vipassanaId + ", contactName=" + contactName + ", roomName=" + roomName + ", checkOut=" + checkOut + ", dateFormat=" + dateFormat + '}';
    }

}
