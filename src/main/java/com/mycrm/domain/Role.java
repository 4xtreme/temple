/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycrm.domain;

import org.hibernate.validator.constraints.NotEmpty;


/**
 *
 * @author Lenovo
 */
public class Role {
 
    private Integer id;
    @NotEmpty
    private String role_name;
    private Boolean is_delete;

    public Role() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getRole_name() {
        return role_name;
    }

    public void setRole_name(String role_name) {
        this.role_name = role_name;
    }

    public Boolean getIs_delete() {
        return is_delete;
    }

    public void setIs_delete(Boolean is_delete) {
        this.is_delete = is_delete;
    }

    public Role(Integer id, String role_name, Boolean is_delete) {
        this.id = id;
        this.role_name = role_name;
        this.is_delete = is_delete;
    }

    @Override
    public String toString() {
        return "Role{" + "id=" + id + ", role_name=" + role_name + ", is_delete=" + is_delete + '}';
    }

    
   
    
   
    
}
