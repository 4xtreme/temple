/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycrm.domain;

import java.io.Serializable;

/**
 *
 * @author pop
 */
public class Position implements Serializable {

    private static final long serialVersionUID = 1L;

    private int id;
    private String position_type;

    public Position() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPosition_type() {
        return position_type;
    }

    public void setPosition_type(String position_type) {
        this.position_type = position_type;
    }

    public Position(int id, String position_type) {
        this.id = id;
        this.position_type = position_type;
    }

    @Override
    public String toString() {
        return "Position{" + "id=" + id + ", position_type=" + position_type + '}';
    }
    
    

   


}
