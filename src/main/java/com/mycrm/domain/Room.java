/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycrm.domain;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import org.hibernate.validator.constraints.NotBlank;

/**
 *
 * @author Lenovo
 */
public class Room {

    private Integer id;
    private int temple_id;
    @NotBlank
    private String name;
    @NotBlank
    private String type;
    @NotBlank
    private String sex;
    @NotBlank
    private String toilet_type;
    @NotNull
    private Integer toilet_amount;
    @NotBlank
    private String status;
    private Integer is_delete;

    public Room() {
    }

    public Room(Integer id, int temple_id, String name, String type, String sex, String toilet_type, Integer toilet_amount, String status, Integer is_delete) {
        this.id = id;
        this.temple_id = temple_id;
        this.name = name;
        this.type = type;
        this.sex = sex;
        this.toilet_type = toilet_type;
        this.toilet_amount = toilet_amount;
        this.status = status;
        this.is_delete = is_delete;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public int getTemple_id() {
        return temple_id;
    }

    public void setTemple_id(int temple_id) {
        this.temple_id = temple_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getToilet_type() {
        return toilet_type;
    }

    public void setToilet_type(String toilet_type) {
        this.toilet_type = toilet_type;
    }

    public Integer getToilet_amount() {
        return toilet_amount;
    }

    public void setToilet_amount(Integer toilet_amount) {
        this.toilet_amount = toilet_amount;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getIs_delete() {
        return is_delete;
    }

    public void setIs_delete(Integer is_delete) {
        this.is_delete = is_delete;
    }

    @Override
    public String toString() {
        return "Room{" + "id=" + id + ", temple_id=" + temple_id + ", name=" + name + ", type=" + type + ", sex=" + sex + ", toilet_type=" + toilet_type + ", toilet_amount=" + toilet_amount + ", status=" + status + ", is_delete=" + is_delete + '}';
    }

}
