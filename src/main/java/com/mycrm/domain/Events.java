/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycrm.domain;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import org.hibernate.validator.constraints.NotBlank;

/**
 *
 * @author SOMON
 */
public class Events {

    private int event_id;
    private int monk_id;
    @NotBlank
    private String description;
    @NotBlank
    private String event_name;
    @NotNull
    private Date event_start;
    @NotNull
    private Date event_end;
    private Boolean is_delete;
    private int temple_id;
    @NotNull
    @Min(value = 0, message = "กรุณาใส่เป็นตัวเลข")
    private Double income;
    private Integer[] monks_id;
    private Integer[] delete_monk_id;
    private List<Monk> monks;

    public Events() {
    }

    public Events(int event_id, int monk_id, String description, String event_name, Date event_start, Date event_end, Boolean is_delete, int temple_id, Double income, Integer[] monks_id, Integer[] delete_monk_id, List<Monk> monks) {
        this.event_id = event_id;
        this.monk_id = monk_id;
        this.description = description;
        this.event_name = event_name;
        this.event_start = event_start;
        this.event_end = event_end;
        this.is_delete = is_delete;
        this.temple_id = temple_id;
        this.income = income;
        this.monks_id = monks_id;
        this.delete_monk_id = delete_monk_id;
        this.monks = monks;
    }

    public int getEvent_id() {
        return event_id;
    }

    public void setEvent_id(int event_id) {
        this.event_id = event_id;
    }

    public int getMonk_id() {
        return monk_id;
    }

    public void setMonk_id(int monk_id) {
        this.monk_id = monk_id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getEvent_name() {
        return event_name;
    }

    public void setEvent_name(String event_name) {
        this.event_name = event_name;
    }

    public Date getEvent_start() {
        return event_start;
    }

    public void setEvent_start(Date event_start) {
        this.event_start = event_start;
    }

    public Date getEvent_end() {
        return event_end;
    }

    public void setEvent_end(Date event_end) {
        this.event_end = event_end;
    }

    public Boolean getIs_delete() {
        return is_delete;
    }

    public void setIs_delete(Boolean is_delete) {
        this.is_delete = is_delete;
    }

    public int getTemple_id() {
        return temple_id;
    }

    public void setTemple_id(int temple_id) {
        this.temple_id = temple_id;
    }

    public Double getIncome() {
        return income;
    }

    public void setIncome(Double income) {
        this.income = income;
    }

    public Integer[] getMonks_id() {
        return monks_id;
    }

    public void setMonks_id(Integer[] monks_id) {
        this.monks_id = monks_id;
    }

    public Integer[] getDelete_monk_id() {
        return delete_monk_id;
    }

    public void setDelete_monk_id(Integer[] delete_monk_id) {
        this.delete_monk_id = delete_monk_id;
    }

    public List<Monk> getMonks() {
        return monks;
    }

    public void setMonks(List<Monk> monks) {
        this.monks = monks;
    }

    @Override
    public String toString() {
        return "Events{" + "event_id=" + event_id + ", monk_id=" + monk_id + ", description=" + description + ", event_name=" + event_name + ", event_start=" + event_start + ", event_end=" + event_end + ", is_delete=" + is_delete + ", temple_id=" + temple_id + ", income=" + income + ", monks_id=" + Arrays.toString(monks_id) + ", delete_monk_id=" + Arrays.toString(delete_monk_id) + ", monks=" + monks + '}';
    }

}
