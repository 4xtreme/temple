/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycrm.domain;


/**
 *
 * @author Lenovo
 */
public class RoleItem {
 
    private Integer id;
    private Integer role_id;
    private String page;
    private Boolean view_page;
    private Boolean create_page;
    private Boolean edit_page;
    private Boolean delete_page;

    public RoleItem() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getRole_id() {
        return role_id;
    }

    public void setRole_id(Integer role_id) {
        this.role_id = role_id;
    }

    public String getPage() {
        return page;
    }

    public void setPage(String page) {
        this.page = page;
    }

    public Boolean getView_page() {
        return view_page;
    }

    public void setView_page(Boolean view_page) {
        this.view_page = view_page;
    }

    public Boolean getCreate_page() {
        return create_page;
    }

    public void setCreate_page(Boolean create_page) {
        this.create_page = create_page;
    }

    public Boolean getEdit_page() {
        return edit_page;
    }

    public void setEdit_page(Boolean edit_page) {
        this.edit_page = edit_page;
    }

    public Boolean getDelete_page() {
        return delete_page;
    }

    public void setDelete_page(Boolean delete_page) {
        this.delete_page = delete_page;
    }

    public RoleItem(Integer id, Integer role_id, String page, Boolean view_page, Boolean create_page, Boolean edit_page, Boolean delete_page) {
        this.id = id;
        this.role_id = role_id;
        this.page = page;
        this.view_page = view_page;
        this.create_page = create_page;
        this.edit_page = edit_page;
        this.delete_page = delete_page;
    }

    @Override
    public String toString() {
        return "RoleItem{" + "id=" + id + ", role_id=" + role_id + ", page=" + page + ", view_page=" + view_page + ", create_page=" + create_page + ", edit_page=" + edit_page + ", delete_page=" + delete_page + '}';
    }
 
    
    
}
