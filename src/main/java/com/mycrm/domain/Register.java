package com.mycrm.domain;

import java.util.Date;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import org.hibernate.validator.constraints.NotBlank;

import org.hibernate.validator.constraints.NotEmpty;

public class Register {

    private int reg_id;
    @NotNull
    private String nametitle;
    @NotBlank
    private String firstname;
    @NotBlank
    private String lastname;
    @Size(min = 13, message = "เลขบัตรประชาชน 13 หลักไม่ถูกต้อง")
    @Pattern(regexp = "[0-9]+", message = "ใส่ตัวเลขเท่านั้น")
    private String personid;
    @NotBlank
    private String royal;
    @NotBlank
    private String nationality;
    private String birthDate;
    private Date birth;
    private String gender;
    @Pattern(regexp = ".+@.+\\..+", message = "กรุณากรอกอีเมลล์ที่ถูกต้อง เช่น example@example.com")
    private String email;
    @NotBlank
    private String address; 
    private String village;
    @NotEmpty
    private String district;
    @NotEmpty
    private String city;
    @NotEmpty
    private String province;
    @Size( min = 5, message = "กรุณาใส่เลข 5 หลัก")
    @Pattern(regexp = "[0-9]+", message = "ใส่ตัวเลขเท่านั้น")
    private String postalcode;
    @Size( min = 10, message = "กรุณาใส่เบอร์โทรศัพท์มือถือ 10 หลัก")
    @Pattern(regexp = "[0-9]+", message = "ใส่ตัวเลขเท่านั้น")
    private String telephone;
    @NotEmpty
    private String password;
    @NotEmpty
    private String repassword;
    private String gRecaptchaResponse;
    private Integer temple_id;
    @NotBlank
    private String bd_birth;
    @NotBlank
    private String nickname;
    private Integer count_come;
    @NotBlank
    private String occupation;
    private Boolean is_delete;
    @NotBlank
    private String gender_text;
    private String status;
    private String reason;
    private String drug;
    private String health;
    private String mental;

    public Register() {
    }

    public Register(int reg_id, String nametitle, String firstname, String lastname, String personid, String royal, String nationality, String birthDate, Date birth, String gender, String email, String address, String village, String district, String city, String province, String postalcode, String telephone, String password, String repassword, String gRecaptchaResponse, Integer temple_id, String bd_birth, String nickname, Integer count_come, String occupation, Boolean is_delete, String gender_text, String status, String reason, String drug, String health, String mental) {
        this.reg_id = reg_id;
        this.nametitle = nametitle;
        this.firstname = firstname;
        this.lastname = lastname;
        this.personid = personid;
        this.royal = royal;
        this.nationality = nationality;
        this.birthDate = birthDate;
        this.birth = birth;
        this.gender = gender;
        this.email = email;
        this.address = address;
        this.village = village;
        this.district = district;
        this.city = city;
        this.province = province;
        this.postalcode = postalcode;
        this.telephone = telephone;
        this.password = password;
        this.repassword = repassword;
        this.gRecaptchaResponse = gRecaptchaResponse;
        this.temple_id = temple_id;
        this.bd_birth = bd_birth;
        this.nickname = nickname;
        this.count_come = count_come;
        this.occupation = occupation;
        this.is_delete = is_delete;
        this.gender_text = gender_text;
        this.status = status;
        this.reason = reason;
        this.drug = drug;
        this.health = health;
        this.mental = mental;
    }

    public int getReg_id() {
        return reg_id;
    }

    public void setReg_id(int reg_id) {
        this.reg_id = reg_id;
    }

    public String getNametitle() {
        return nametitle;
    }

    public void setNametitle(String nametitle) {
        this.nametitle = nametitle;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getPersonid() {
        return personid;
    }

    public void setPersonid(String personid) {
        this.personid = personid;
    }

    public String getRoyal() {
        return royal;
    }

    public void setRoyal(String royal) {
        this.royal = royal;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public Date getBirth() {
        return birth;
    }

    public void setBirth(Date birth) {
        this.birth = birth;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getVillage() {
        return village;
    }

    public void setVillage(String village) {
        this.village = village;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getPostalcode() {
        return postalcode;
    }

    public void setPostalcode(String postalcode) {
        this.postalcode = postalcode;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRepassword() {
        return repassword;
    }

    public void setRepassword(String repassword) {
        this.repassword = repassword;
    }

    public String getgRecaptchaResponse() {
        return gRecaptchaResponse;
    }

    public void setgRecaptchaResponse(String gRecaptchaResponse) {
        this.gRecaptchaResponse = gRecaptchaResponse;
    }

    public Integer getTemple_id() {
        return temple_id;
    }

    public void setTemple_id(Integer temple_id) {
        this.temple_id = temple_id;
    }

    public String getBd_birth() {
        return bd_birth;
    }

    public void setBd_birth(String bd_birth) {
        this.bd_birth = bd_birth;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public Integer getCount_come() {
        return count_come;
    }

    public void setCount_come(Integer count_come) {
        this.count_come = count_come;
    }

    public String getOccupation() {
        return occupation;
    }

    public void setOccupation(String occupation) {
        this.occupation = occupation;
    }

    public Boolean getIs_delete() {
        return is_delete;
    }

    public void setIs_delete(Boolean is_delete) {
        this.is_delete = is_delete;
    }

    public String getGender_text() {
        return gender_text;
    }

    public void setGender_text(String gender_text) {
        this.gender_text = gender_text;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getDrug() {
        return drug;
    }

    public void setDrug(String drug) {
        this.drug = drug;
    }

    public String getHealth() {
        return health;
    }

    public void setHealth(String health) {
        this.health = health;
    }

    public String getMental() {
        return mental;
    }

    public void setMental(String mental) {
        this.mental = mental;
    }

    @Override
    public String toString() {
        return "Register{" + "reg_id=" + reg_id + ", nametitle=" + nametitle + ", firstname=" + firstname + ", lastname=" + lastname + ", personid=" + personid + ", royal=" + royal + ", nationality=" + nationality + ", birthDate=" + birthDate + ", birth=" + birth + ", gender=" + gender + ", email=" + email + ", address=" + address + ", village=" + village + ", district=" + district + ", city=" + city + ", province=" + province + ", postalcode=" + postalcode + ", telephone=" + telephone + ", password=" + password + ", repassword=" + repassword + ", gRecaptchaResponse=" + gRecaptchaResponse + ", temple_id=" + temple_id + ", bd_birth=" + bd_birth + ", nickname=" + nickname + ", count_come=" + count_come + ", occupation=" + occupation + ", is_delete=" + is_delete + ", gender_text=" + gender_text + ", status=" + status + ", reason=" + reason + ", drug=" + drug + ", health=" + health + ", mental=" + mental + '}';
    }

}
