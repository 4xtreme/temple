/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycrm.domain;

/**
 *
 * @author jame
 */
public class PeopleLiving {

    private Integer id;
    private String firstname;
    private String lastname;
    private String builder_name;
    private String room_name;

    public PeopleLiving() {
    }

    public PeopleLiving(Integer id, String firstname, String lastname, String builder_name, String room_name) {
        this.id = id;
        this.firstname = firstname;
        this.lastname = lastname;
        this.builder_name = builder_name;
        this.room_name = room_name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getBuilder_name() {
        return builder_name;
    }

    public void setBuilder_name(String builder_name) {
        this.builder_name = builder_name;
    }

    public String getRoom_name() {
        return room_name;
    }

    public void setRoom_name(String room_name) {
        this.room_name = room_name;
    }

    @Override
    public String toString() {
        return "PeopleLiving{" + "id=" + id + ", firstname=" + firstname + ", lastname=" + lastname + ", builder_name=" + builder_name + ", room_name=" + room_name + '}';
    }

}
