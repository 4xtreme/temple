/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycrm.domain;

import java.io.Serializable;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;
import org.hibernate.validator.constraints.Range;

/**
 *
 * @author pop
 */
public class Staff implements Serializable {

    private static final long serialVersionUID = 1L;

    private int id;
    @NotBlank
    private String firstname;
    @NotBlank
    private String lastname;
//    @Range(min=13,message="อีเมล์ไม่ถูกต้อง !")
    @Pattern(regexp = ".+@.+\\..+", message = "กรุณากรอกอีเมลล์ที่ถูกต้อง เช่น example@example.com")
    private String email;
    @Size( min = 10, message = "กรุณาใส่เบอร์โทรศัพท์ 10 หลัก")
    @Pattern(regexp = "[0-9]+", message = "ใส่ตัวเลขเท่านั้น")
    private String phone1;
    @NotBlank
    private String password;
    private int role_id;
    private int is_delete;
    private int temple_id;

    public Staff() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone1() {
        return phone1;
    }

    public void setPhone1(String phone1) {
        this.phone1 = phone1;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getRole_id() {
        return role_id;
    }

    public void setRole_id(int role_id) {
        this.role_id = role_id;
    }

    public int getIs_delete() {
        return is_delete;
    }

    public void setIs_delete(int is_delete) {
        this.is_delete = is_delete;
    }

    public int getTemple_id() {
        return temple_id;
    }

    public void setTemple_id(int temple_id) {
        this.temple_id = temple_id;
    }

    @Override
    public String toString() {
        return "Staff{" + "id=" + id + ", firstname=" + firstname + ", lastname=" + lastname + ", email=" + email + ", phone1=" + phone1 + ", password=" + password + ", role_id=" + role_id + ", is_delete=" + is_delete + ", temple_id=" + temple_id + '}';
    }

    public Staff(int id, String firstname, String lastname, String email, String phone1, String password, int role_id, int is_delete, int temple_id) {
        this.id = id;
        this.firstname = firstname;
        this.lastname = lastname;
        this.email = email;
        this.phone1 = phone1;
        this.password = password;
        this.role_id = role_id;
        this.is_delete = is_delete;
        this.temple_id = temple_id;
    }

}
