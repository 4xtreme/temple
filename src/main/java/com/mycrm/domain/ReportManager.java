/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycrm.domain;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JRParameter;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.JasperRunManager;
import net.sf.jasperreports.engine.export.JRCsvExporter;
import net.sf.jasperreports.engine.export.JRCsvExporterParameter;
import net.sf.jasperreports.engine.export.JRXlsExporterParameter;
import net.sf.jasperreports.engine.util.JRLoader;

/**
 *
 * @author Lenovo
 */
public class ReportManager {

    public ReportManager() {
    }
    private String reportFunction = "";
    private String paramName = "";
    private String paramName2 = "";
    private String paramName3 = "";
    private String paramName4 = "";
    private String paramName5 = "";

    public void genReport(String reportFunction, String inputID, String status, String start_date, String end_date, HttpServletRequest request, HttpServletResponse response) {
        System.out.println("**********Start ReportManager System**********");
        System.out.println("Function call: " + reportFunction);
        this.reportFunction = reportFunction;
        String reportFileName = "";
        System.out.print("Select report: ");
        switch (reportFunction) {
            case "Monk":
                System.out.println("MonkReportPDF");
                reportFileName = "monkReport";
                paramName = "tID";
                paramName2 = "status";
                paramName3 = "start_date";
                paramName4 = "end_date";
                break;
            default:
                System.out.println("Error don't have this function ");
                break;
        }
        Connection conn = null;
        try {
            try {

                Class.forName("org.postgresql.Driver");
            } catch (ClassNotFoundException e) {
                System.out.println("Please include Classpath Where your MySQL Driver is located");
                e.printStackTrace();
            }
            conn = (Connection) DriverManager.getConnection("jdbc:postgresql://localhost:5432/temple", "postgres", "1234");
            if (conn != null) {
                System.out.println("Database Connected");
            } else {
                System.out.println(" connection Failed ");
            }
            //Parameters as Map to be passed to Jasper
            HashMap<String, Object> hmParams = new HashMap<String, Object>();
            hmParams.put(JRParameter.REPORT_LOCALE, new Locale("th", "TH"));
            hmParams.put(paramName, new Integer(inputID));
            hmParams.put(paramName2, new String(status));
            hmParams.put(paramName3, start_date);
            hmParams.put(paramName4, end_date);
            //hmParams.put(nameofparamIN ireport, value);

            JasperReport jasperReport = getCompiledFile(reportFileName, request);
            generateReportPDF(response, hmParams, jasperReport, conn); // For PDF report

        } catch (Exception sqlExp) {
            System.out.println("Exception::" + sqlExp.toString());
        } finally {
            try {
                if (conn != null) {
                    conn.close();
                    conn = null;
                }
            } catch (SQLException expSQL) {
                System.out.println("SQLExp::CLOSING::" + expSQL.toString());
            }
        }
        System.out.println("**********End ReportManager System**********");

    }

    public void genReportContact(String reportFunction, String inputID, String status, String start_date, String end_date, String secretKey, HttpServletRequest request, HttpServletResponse response) {
        System.out.println("**********Start ReportManager System**********");
        System.out.println("Function call: " + reportFunction);
        this.reportFunction = reportFunction;
        String reportFileName = "";
        System.out.print("Select report: ");
        switch (reportFunction) {
            case "Contact":
                System.out.println("ContactReportPDF");
                reportFileName = "contactReport";
                paramName = "tID";
                paramName2 = "status";
                paramName3 = "start_date";
                paramName4 = "end_date";
                paramName5 = "secretkey";
                break;
            default:
                System.out.println("Error don't have this function ");
                break;
        }
        Connection conn = null;
        try {
            try {

                Class.forName("org.postgresql.Driver");
            } catch (ClassNotFoundException e) {
                System.out.println("Please include Classpath Where your MySQL Driver is located");
                e.printStackTrace();
            }
            conn = (Connection) DriverManager.getConnection("jdbc:postgresql://localhost:5432/temple", "postgres", "1234");
            if (conn != null) {
                System.out.println("Database Connected");
            } else {
                System.out.println(" connection Failed ");
            }
            //Parameters as Map to be passed to Jasper
            HashMap<String, Object> hmParams = new HashMap<String, Object>();
            hmParams.put(JRParameter.REPORT_LOCALE, new Locale("th", "TH"));
            hmParams.put(paramName, new Integer(inputID));
            hmParams.put(paramName2, new String(status));
            hmParams.put(paramName3, start_date);
            hmParams.put(paramName4, end_date);
            hmParams.put(paramName5, secretKey);
            //hmParams.put(nameofparamIN ireport, value);

            JasperReport jasperReport = getCompiledFile(reportFileName, request);
            generateReportPDF(response, hmParams, jasperReport, conn); // For PDF report

        } catch (Exception sqlExp) {
            System.out.println("Exception::" + sqlExp.toString());
        } finally {
            try {
                if (conn != null) {
                    conn.close();
                    conn = null;
                }
            } catch (SQLException expSQL) {
                System.out.println("SQLExp::CLOSING::" + expSQL.toString());
            }
        }
        System.out.println("**********End ReportManager System**********");

    }

    public void genReportCSV(String reportFunction, String inputID, String status, String start_date, String end_date, HttpServletRequest request, HttpServletResponse response) {
        System.out.println("**********Start ReportManager System**********");
        System.out.println("Function call: " + reportFunction);
        this.reportFunction = reportFunction;
        String reportFileName = "";
        System.out.print("Select report: ");
        switch (reportFunction) {
            case "Monk":
                System.out.println("MonkReportCSV");
                reportFileName = "monkReportCSV";
                paramName = "tID";
                paramName2 = "status";
                paramName3 = "start_date";
                paramName4 = "end_date";
                break;
            default:
                System.out.println("Error don't have this function ");
                break;
        }
        Connection conn = null;
        try {
            try {

                Class.forName("org.postgresql.Driver");
            } catch (ClassNotFoundException e) {
                System.out.println("Please include Classpath Where your MySQL Driver is located");
                e.printStackTrace();
            }
            conn = (Connection) DriverManager.getConnection("jdbc:postgresql://localhost:5432/temple", "postgres", "1234");
            if (conn != null) {
                System.out.println("Database Connected");
            } else {
                System.out.println(" connection Failed ");
            }
            //Parameters as Map to be passed to Jasper
            HashMap<String, Object> hmParams = new HashMap<String, Object>();
            hmParams.put(JRParameter.REPORT_LOCALE, new Locale("th", "TH"));
            hmParams.put(paramName, new Integer(inputID));
            hmParams.put(paramName2, new String(status));
            hmParams.put(paramName3, start_date);
            hmParams.put(paramName4, end_date);
            //hmParams.put(nameofparamIN ireport, value);

            JasperReport jasperReport = getCompiledFile(reportFileName, request);
            generateReportCSV(response, hmParams, jasperReport, conn); // For PDF report

        } catch (Exception sqlExp) {
            System.out.println("Exception::" + sqlExp.toString());
        } finally {
            try {
                if (conn != null) {
                    conn.close();
                    conn = null;
                }
            } catch (SQLException expSQL) {
                System.out.println("SQLExp::CLOSING::" + expSQL.toString());
            }
        }
        System.out.println("**********End ReportManager System**********");

    }

    public void genReportContactCSV(String reportFunction, String inputID, String status, String start_date, String end_date, String secretKey, HttpServletRequest request, HttpServletResponse response) {
        System.out.println("**********Start ReportManager System**********");
        System.out.println("Function call: " + reportFunction);
        this.reportFunction = reportFunction;
        String reportFileName = "";
        System.out.print("Select report: ");
        switch (reportFunction) {
            case "Contact":
                System.out.println("contactReportCSV");
                reportFileName = "contactReport";
                paramName = "tID";
                paramName2 = "status";
                paramName3 = "start_date";
                paramName4 = "end_date";
                paramName5 = "secretkey";
                break;
            default:
                System.out.println("Error don't have this function ");
                break;
        }
        Connection conn = null;
        try {
            try {

                Class.forName("org.postgresql.Driver");
            } catch (ClassNotFoundException e) {
                System.out.println("Please include Classpath Where your MySQL Driver is located");
                e.printStackTrace();
            }
            conn = (Connection) DriverManager.getConnection("jdbc:postgresql://localhost:5432/temple", "postgres", "1234");
            if (conn != null) {
                System.out.println("Database Connected");
            } else {
                System.out.println(" connection Failed ");
            }
            //Parameters as Map to be passed to Jasper
            HashMap<String, Object> hmParams = new HashMap<String, Object>();
            hmParams.put(JRParameter.REPORT_LOCALE, new Locale("th", "TH"));
            hmParams.put(paramName, new Integer(inputID));
            hmParams.put(paramName2, new String(status));
            hmParams.put(paramName3, start_date);
            hmParams.put(paramName4, end_date);
            hmParams.put(paramName5, secretKey);
            //hmParams.put(nameofparamIN ireport, value);

            JasperReport jasperReport = getCompiledFile(reportFileName, request);
            generateReportCSV(response, hmParams, jasperReport, conn); // For PDF report

        } catch (Exception sqlExp) {
            System.out.println("Exception::" + sqlExp.toString());
        } finally {
            try {
                if (conn != null) {
                    conn.close();
                    conn = null;
                }
            } catch (SQLException expSQL) {
                System.out.println("SQLExp::CLOSING::" + expSQL.toString());
            }
        }
        System.out.println("**********End ReportManager System**********");

    }

    private JasperReport getCompiledFile(String fileName, HttpServletRequest request) throws JRException {
        System.out.println("path " + request.getSession().getServletContext().getRealPath("/jasper/" + fileName + ".jasper"));
        File reportFile = new File(request.getSession().getServletContext().getRealPath("/jasper/" + fileName + ".jasper"));
        System.out.println("reportFile.getPath()" + reportFile.getPath());
        // If compiled file is not found, then compile XML template
        if (!reportFile.exists()) {
            System.out.println("if reportFile");
            JasperCompileManager.compileReportToFile(request.getSession().getServletContext().getRealPath("/jasper/" + fileName + ".jrxml"), request.getSession().getServletContext().getRealPath("/jasper/" + fileName + ".jasper"));
        }
        JasperReport jasperReport = (JasperReport) JRLoader.loadObjectFromFile(reportFile.getPath());
        System.out.println("Get reportfile");
        return jasperReport;
    }

    private void generateReportPDF(HttpServletResponse resp, Map parameters, JasperReport jasperReport, Connection conn) throws JRException, NamingException, SQLException, IOException {
        byte[] bytes = null;
        try {

            bytes = JasperRunManager.runReportToPdf(jasperReport, parameters, conn);
        } catch (Exception e) {
            System.out.println("Error: " + e);
        }

        resp.reset();
        resp.resetBuffer();
        resp.setContentType("application/pdf");
        resp.setHeader("Content-Disposition", "attachment; filename=\"" + generateFileName() + "\"");     // Delete! this line when you Don't need to show pop-up Download file.
        resp.setContentLength(bytes.length);
        ServletOutputStream ouputStream = resp.getOutputStream();
        ouputStream.write(bytes, 0, bytes.length);
        ouputStream.flush();
        ouputStream.close();
        System.out.println("Generate PDF file");
    }

    private void generateReportCSV(HttpServletResponse resp, Map parameters, JasperReport jasperReport, Connection conn) throws JRException, NamingException, SQLException, IOException, ServletException {
//        byte[] bytes = null;

        try {

            resp.reset();
            resp.resetBuffer();
            resp.setContentType("application/csv; charset=utf-8");
            resp.setCharacterEncoding("utf-8");
            resp.setHeader("Content-Disposition", "attachment; filename=\"" + generateFileNameCSV() + "\"");
            resp.addHeader("Content-type", "application/csv; charset=" + Charset.forName("utf-8").displayName());
            ServletOutputStream servletOutputStream = resp.getOutputStream();
            JRCsvExporter exporter = new JRCsvExporter();
            JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, conn);
            exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
            exporter.setParameter(JRExporterParameter.CHARACTER_ENCODING, "utf-8");
            exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, servletOutputStream);
            exporter.setParameter(JRCsvExporterParameter.CHARACTER_ENCODING, "utf-8");
            exporter.setParameter(JRXlsExporterParameter.IS_ONE_PAGE_PER_SHEET, Boolean.FALSE);
            exporter.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND, Boolean.TRUE);
            exporter.setParameter(JRXlsExporterParameter.IS_DETECT_CELL_TYPE, Boolean.TRUE);
            exporter.setParameter(JRXlsExporterParameter.IS_IGNORE_CELL_BORDER, Boolean.FALSE);
            exporter.setParameter(JRXlsExporterParameter.IS_COLLAPSE_ROW_SPAN, Boolean.TRUE);
            exporter.setParameter(JRXlsExporterParameter.IS_IGNORE_CELL_BORDER, Boolean.FALSE);
            exporter.setParameter(JRXlsExporterParameter.IGNORE_PAGE_MARGINS, Boolean.TRUE);
            exporter.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_COLUMNS, Boolean.TRUE);
            exporter.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, Boolean.TRUE);
            exporter.setParameter(JRXlsExporterParameter.MAXIMUM_ROWS_PER_SHEET, Boolean.TRUE);
            // exporter.setParameter(JRCsvExporterParameter.FIELD_DELIMITER, ";");
            exporter.exportReport();
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }
//
//        resp.reset();
//        resp.resetBuffer();
//        resp.setContentType("application/csv");
//        resp.setCharacterEncoding("UTF-8");
//        resp.setHeader("Content-Disposition", "attachment; filename=\"" + generateFileNameCSV() + "\"");     // Delete! this line when you Don't need to show pop-up Download file.
//        resp.setContentLength(bytes.length);
//        ServletOutputStream ouputStream2 = resp.getOutputStream();
//        ouputStream2.write(bytes, 0, bytes.length);
//        ouputStream2.flush();
//        ouputStream2.close();
        System.out.println("Generate CSV file");
    }

    public byte[] genReportEmail(String reportFunction, String inputID, HttpServletRequest request) {
        System.out.println("**********Start Email ReportManager System**********");
        System.out.println("Function call: " + reportFunction);
        String reportFileName = "";
        System.out.print("Select report: ");
        switch (reportFunction) {
            case "Quotation":
                System.out.println("QuotationReport");
                reportFileName = "QuotationReport";
                paramName = "QtID";
                break;
            case "Billing":
                System.out.println("BillingReport");
                reportFileName = "BillingReport";
                paramName = "biD";
                break;
            case "Invoice":
                System.out.println("InvoiceReport");
                reportFileName = "InvoiceReport";
                paramName = "invoiceID";
                break;
            default:
                System.out.println("Error don't have this function ");
                break;
        }
        Connection conn = null;
        byte[] bytes = null;
        try {
            try {
                Class.forName("com.mysql.jdbc.Driver");
            } catch (ClassNotFoundException e) {
                System.out.println("Please include Classpath Where your MySQL Driver is located");
                e.printStackTrace();
            }
            conn = (Connection) DriverManager.getConnection("jdbc:mysql://localhost/billing", "root", "");
            if (conn != null) {
                System.out.println("Database Connected");
            } else {
                System.out.println(" connection Failed ");
            }
            //Parameters as Map to be passed to Jasper
            HashMap<String, Object> hmParams = new HashMap<String, Object>();
            hmParams.put(paramName, new Integer(inputID));
            //hmParams.put(nameofparamIN ireport, value);

            JasperReport jasperReport = getCompiledFile(reportFileName, request);
            bytes = generatePDFforEmail(hmParams, jasperReport, conn);
        } catch (Exception sqlExp) {
            System.out.println("Exception::" + sqlExp.toString());
        } finally {
            try {
                if (conn != null) {
                    conn.close();
                    conn = null;
                }
            } catch (SQLException expSQL) {
                System.out.println("SQLExp::CLOSING::" + expSQL.toString());
            }
        }
        System.out.println("**********End ReportManager System**********");
        return bytes;
    }

    private byte[] generatePDFforEmail(Map parameters, JasperReport jasperReport, Connection conn) throws JRException, NamingException, SQLException, IOException {
        byte[] bytes = null;
        try {
            bytes = JasperRunManager.runReportToPdf(jasperReport, parameters, conn);
        } catch (Exception e) {
            System.out.println("Error: " + e);
        }
        System.out.println("Generate PDF file");
        return bytes;
    }

    public String generateFileName() {
        System.out.println("Generate filename");
        Date date = new Date();
        String[] getDate = date.toString().split(" ");
        getDate[3] = getDate[3].replaceAll(":", "");
        String newFileName = "Report_" + reportFunction + "_" + getDate[5] + getDate[1] + getDate[2] + "_" + getDate[3].substring(0, 4) + ".pdf";
        System.out.println("File Name: " + newFileName);

        return newFileName;
    }

    public String generateFileNameCSV() {
        System.out.println("Generate filename");
        Date date = new Date();
        String[] getDate = date.toString().split(" ");
        getDate[3] = getDate[3].replaceAll(":", "");
        String newFileName = "Report_" + reportFunction + "_" + getDate[5] + getDate[1] + getDate[2] + "_" + getDate[3].substring(0, 4) + ".csv";
        System.out.println("File Name: " + newFileName);

        return newFileName;
    }

}
