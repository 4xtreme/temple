/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycrm.domain;

import javax.validation.constraints.NotNull;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;

/**
 *
 * @author SOMON
 */
public class Articles {

    private int article_id;
    private int staff_id;
    private String firstname;
    private String lastname;
    @NotBlank
    private String title;
    @NotBlank
    private String contents;
    private byte[] article_pic;
    private byte[] article_byte;
    private String article_string;
    private boolean is_delete;
    private int temple_id;

    public Articles() {
    }

    public Articles(int article_id, int staff_id, String firstname, String lastname, String title, String contents, byte[] article_pic, byte[] article_byte, String article_string, boolean is_delete, int temple_id) {
        this.article_id = article_id;
        this.staff_id = staff_id;
        this.firstname = firstname;
        this.lastname = lastname;
        this.title = title;
        this.contents = contents;
        this.article_pic = article_pic;
        this.article_byte = article_byte;
        this.article_string = article_string;
        this.is_delete = is_delete;
        this.temple_id = temple_id;
    }

    public int getArticle_id() {
        return article_id;
    }

    public void setArticle_id(int article_id) {
        this.article_id = article_id;
    }

    public int getStaff_id() {
        return staff_id;
    }

    public void setStaff_id(int staff_id) {
        this.staff_id = staff_id;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContents() {
        return contents;
    }

    public void setContents(String contents) {
        this.contents = contents;
    }

    public byte[] getArticle_pic() {
        return article_pic;
    }

    public void setArticle_pic(byte[] article_pic) {
        this.article_pic = article_pic;
    }

    public byte[] getArticle_byte() {
        return article_byte;
    }

    public void setArticle_byte(byte[] article_byte) {
        this.article_byte = article_byte;
    }

    public String getArticle_string() {
        return article_string;
    }

    public void setArticle_string(String article_string) {
        this.article_string = article_string;
    }

    public boolean isIs_delete() {
        return is_delete;
    }

    public void setIs_delete(boolean is_delete) {
        this.is_delete = is_delete;
    }

    public int getTemple_id() {
        return temple_id;
    }

    public void setTemple_id(int temple_id) {
        this.temple_id = temple_id;
    }

    @Override
    public String toString() {
        return "Articles{" + "article_id=" + article_id + ", staff_id=" + staff_id + ", firstname=" + firstname + ", lastname=" + lastname + ", title=" + title + ", contents=" + contents + ", article_pic=" + article_pic + ", article_byte=" + article_byte + ", article_string=" + article_string + ", is_delete=" + is_delete + ", temple_id=" + temple_id + '}';
    }

}
