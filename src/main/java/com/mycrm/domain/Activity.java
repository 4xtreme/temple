/*
 * Copy Right Pay Enterprise Co.,Ltd.
 */
package com.mycrm.domain;

import java.util.Date;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.format.annotation.DateTimeFormat;

/**
 *
 * @author pop
 */
public class Activity {

    private Integer id;
    @NotEmpty
    private String activity_name;
    private String vipassana_name;
    @NotEmpty
    private String detail;
    @NotNull
    @DateTimeFormat(pattern = "dd/mm/yyyy")
    private Date activity_start;
    @NotNull
    private Date activity_end;
    private Integer temple_id;
    @NotNull
    private Integer vipassana_id;
    private Date create_date;
    private Date update_date;
    private Boolean is_delete;

    public Activity() {
    }

    public Activity(Integer id, String activity_name, String vipassana_name, String detail, Date activity_start, Date activity_end, Integer temple_id, Integer vipassana_id, Date create_date, Date update_date, Boolean is_delete) {
        this.id = id;
        this.activity_name = activity_name;
        this.vipassana_name = vipassana_name;
        this.detail = detail;
        this.activity_start = activity_start;
        this.activity_end = activity_end;
        this.temple_id = temple_id;
        this.vipassana_id = vipassana_id;
        this.create_date = create_date;
        this.update_date = update_date;
        this.is_delete = is_delete;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getActivity_name() {
        return activity_name;
    }

    public void setActivity_name(String activity_name) {
        this.activity_name = activity_name;
    }

    public String getVipassana_name() {
        return vipassana_name;
    }

    public void setVipassana_name(String vipassana_name) {
        this.vipassana_name = vipassana_name;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public Date getActivity_start() {
        return activity_start;
    }

    public void setActivity_start(Date activity_start) {
        this.activity_start = activity_start;
    }

    public Date getActivity_end() {
        return activity_end;
    }

    public void setActivity_end(Date activity_end) {
        this.activity_end = activity_end;
    }

    public Integer getTemple_id() {
        return temple_id;
    }

    public void setTemple_id(Integer temple_id) {
        this.temple_id = temple_id;
    }

    public Integer getVipassana_id() {
        return vipassana_id;
    }

    public void setVipassana_id(Integer vipassana_id) {
        this.vipassana_id = vipassana_id;
    }

    public Date getCreate_date() {
        return create_date;
    }

    public void setCreate_date(Date create_date) {
        this.create_date = create_date;
    }

    public Date getUpdate_date() {
        return update_date;
    }

    public void setUpdate_date(Date update_date) {
        this.update_date = update_date;
    }

    public Boolean getIs_delete() {
        return is_delete;
    }

    public void setIs_delete(Boolean is_delete) {
        this.is_delete = is_delete;
    }

    @Override
    public String toString() {
        return "Activity{" + "id=" + id + ", activity_name=" + activity_name + ", vipassana_name=" + vipassana_name + ", detail=" + detail + ", activity_start=" + activity_start + ", activity_end=" + activity_end + ", temple_id=" + temple_id + ", vipassana_id=" + vipassana_id + ", create_date=" + create_date + ", update_date=" + update_date + ", is_delete=" + is_delete + '}';
    }

}
