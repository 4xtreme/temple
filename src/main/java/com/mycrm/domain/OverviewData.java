/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycrm.domain;

/**
 *
 * @author pop
 */
public class OverviewData {

    private int id;
    private String builder_name;
    private String room_name;
    private String checkin_day;
    private String checkin_month;
    private String checkin_year;
    private String checkout_day;
    private String checkout_month;
    private String checkout_year;
    private String type;
    private String contact_name;
    private int room_id;
    private int room_name_id;

    public OverviewData() {
    }

    public OverviewData(int id,String builder_name, String room_name, String checkin_day, String checkin_month, String checkin_year, String checkout_day, String checkout_month, String checkout_year, String type, String contact_name, int room_id, int room_name_id) {
        this.id = id;
        this.builder_name = builder_name;
        this.room_name = room_name;
        this.checkin_day = checkin_day;
        this.checkin_month = checkin_month;
        this.checkin_year = checkin_year;
        this.checkout_day = checkout_day;
        this.checkout_month = checkout_month;
        this.checkout_year = checkout_year;
        this.type = type;
        this.contact_name = contact_name;
        this.room_id = room_id;
        this.room_name_id = room_name_id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getBuilder_name() {
        return builder_name;
    }

    public void setBuilder_name(String builder_name) {
        this.builder_name = builder_name;
    }

    public String getRoom_name() {
        return room_name;
    }

    public void setRoom_name(String room_name) {
        this.room_name = room_name;
    }

    public String getCheckin_day() {
        return checkin_day;
    }

    public void setCheckin_day(String checkin_day) {
        this.checkin_day = checkin_day;
    }

    public String getCheckin_month() {
        return checkin_month;
    }

    public void setCheckin_month(String checkin_month) {
        this.checkin_month = checkin_month;
    }

    public String getCheckin_year() {
        return checkin_year;
    }

    public void setCheckin_year(String checkin_year) {
        this.checkin_year = checkin_year;
    }

    public String getCheckout_day() {
        return checkout_day;
    }

    public void setCheckout_day(String checkout_day) {
        this.checkout_day = checkout_day;
    }

    public String getCheckout_month() {
        return checkout_month;
    }

    public void setCheckout_month(String checkout_month) {
        this.checkout_month = checkout_month;
    }

    public String getCheckout_year() {
        return checkout_year;
    }

    public void setCheckout_year(String checkout_year) {
        this.checkout_year = checkout_year;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getContact_name() {
        return contact_name;
    }

    public void setContact_name(String contact_name) {
        this.contact_name = contact_name;
    }

    public int getRoom_id() {
        return room_id;
    }

    public void setRoom_id(int room_id) {
        this.room_id = room_id;
    }

    public int getRoom_name_id() {
        return room_name_id;
    }

    public void setRoom_name_id(int room_name_id) {
        this.room_name_id = room_name_id;
    }

    @Override
    public String toString() {
        return "OverviewData{" + "id=" + id + ", builder_name=" + builder_name + ", room_name=" + room_name + ", checkin_day=" + checkin_day + ", checkin_month=" + checkin_month + ", checkin_year=" + checkin_year + ", checkout_day=" + checkout_day + ", checkout_month=" + checkout_month + ", checkout_year=" + checkout_year + ", type=" + type + ", contact_name=" + contact_name + ", room_id=" + room_id + ", room_name_id=" + room_name_id + '}';
    }

}
