/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycrm.domain;

/**
 *
 * @author she my sunshine
 */
public class Summarycharts {
    private String id;
    private String gender;
    private String temple_id;

    public Summarycharts() {
    }

    public Summarycharts(String id, String gender, String temple_id) {
        this.id = id;
        this.gender = gender;
        this.temple_id = temple_id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getTemple_id() {
        return temple_id;
    }

    public void setTemple_id(String temple_id) {
        this.temple_id = temple_id;
    }

    @Override
    public String toString() {
        return "Summarycharts{" + "id=" + id + ", gender=" + gender + ", temple_id=" + temple_id + '}';
    }
    
    
   
    
}
