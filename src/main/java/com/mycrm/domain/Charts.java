/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycrm.domain;

/**
 *
 * @author she my sunshine
 */
public class Charts {
    private int value;
    private String name;

    public Charts() {
    }

    @Override
    public String toString() {
        return "Charts{" + "value=" + value + ", name=" + name + '}';
    }

    public Charts(int value, String name) {
        this.value = value;
        this.name = name;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

  
    
}
