/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycrm.domain;

import com.mysql.jdbc.Blob;
import java.io.Serializable;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import org.hibernate.validator.constraints.NotBlank;

/**
 *
 * @author she my sunshine
 */
public class Temple implements Serializable {

    private static final long serialVersionUID = 1L;

    private int id;
    @NotBlank
    private String name;
//    @NotEmpty
    private String about;
    @NotBlank
    private String address;
    @NotBlank
    private String district;
    @NotBlank
    private String prefecture;
    @NotBlank
    private String province;
    @Size( min = 5, message = "กรุณาใส่เลข 5 หลัก")
    @Pattern(regexp = "[0-9]+", message = "ใส่ตัวเลขเท่านั้น")
    private String postal_code;
//    @Range(min=5,message="กรุณากรอกเบอร์ให้ถูกต้อง")
    @Size( min = 10, message = "กรุณาใส่เบอร์โทรศัพท์ 10 หลัก")
    @Pattern(regexp = "[0-9]+", message = "ใส่ตัวเลขเท่านั้น")
    private String tel;

    private int is_delete;

    private String pic;
    private Blob filedata;
    private String latitude;
    private String longitude;

    public Temple() {
    }

    public Temple(int id, String name, String about, String address, String district, String prefecture, String province, String postal_code, String tel, int is_delete, String pic, Blob filedata, String latitude, String longitude) {
        this.id = id;
        this.name = name;
        this.about = about;
        this.address = address;
        this.district = district;
        this.prefecture = prefecture;
        this.province = province;
        this.postal_code = postal_code;
        this.tel = tel;
        this.is_delete = is_delete;
        this.pic = pic;
        this.filedata = filedata;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAbout() {
        return about;
    }

    public void setAbout(String about) {
        this.about = about;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getPrefecture() {
        return prefecture;
    }

    public void setPrefecture(String prefecture) {
        this.prefecture = prefecture;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getPostal_code() {
        return postal_code;
    }

    public void setPostal_code(String postal_code) {
        this.postal_code = postal_code;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public int getIs_delete() {
        return is_delete;
    }

    public void setIs_delete(int is_delete) {
        this.is_delete = is_delete;
    }

    public String getPic() {
        return pic;
    }

    public void setPic(String pic) {
        this.pic = pic;
    }

    public Blob getFiledata() {
        return filedata;
    }

    public void setFiledata(Blob filedata) {
        this.filedata = filedata;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    @Override
    public String toString() {
        return "Temple{" + "id=" + id + ", name=" + name + ", about=" + about + ", address=" + address + ", district=" + district + ", prefecture=" + prefecture + ", province=" + province + ", postal_code=" + postal_code + ", tel=" + tel + ", is_delete=" + is_delete + ", pic=" + pic + ", filedata=" + filedata + ", latitude=" + latitude + ", longitude=" + longitude + '}';
    }

}
