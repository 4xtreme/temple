/*
 * Copy Right Pay Enterprise Co.,Ltd.
 */
package com.mycrm.domain;

import java.util.Date;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;

/**
 *
 * @author pop
 */
public class Vipassana {

    private Integer id;
    @NotBlank
    private String firstname;
    @NotBlank
    private String nickname;
    @NotBlank
    private String lastname;
    @NotEmpty
    @Size(min = 13, message="กรุณากรอกบัตรประชาชนให้ถูกต้อง")
    @Pattern(regexp = "[0-9]+", message = "กรุณากรอกตัวเลขเท่านั้น" )
    private String personal_id;
    @NotBlank
    private String academic_standing;
    @NotBlank
    private String sect;
    @NotBlank
    private String address;
    @NotBlank
    private String sub_district;
    @NotBlank
    private String district;
    @NotEmpty
    private String province;
    @Size(min = 5, message = "กรุณากรอกรหัสไปรษณีย์ให้ถูกต้อง")
    @Pattern(regexp = "[0-9]+", message = "กรุณากรอกตัวเลขเท่านั้น")
    private String postal_code;
    private byte[] image;
    private int temple_id;
    private Date create_date;
    private Date update_date;
    private Boolean is_delete;

    public Vipassana() {
    }

    public Vipassana(Integer id, String firstname, String nickname, String lastname, String personal_id, String academic_standing, String sect, String address, String sub_district, String district, String province, String postal_code, byte[] image, int temple_id, Date create_date, Date update_date, Boolean is_delete) {
        this.id = id;
        this.firstname = firstname;
        this.nickname = nickname;
        this.lastname = lastname;
        this.personal_id = personal_id;
        this.academic_standing = academic_standing;
        this.sect = sect;
        this.address = address;
        this.sub_district = sub_district;
        this.district = district;
        this.province = province;
        this.postal_code = postal_code;
        this.image = image;
        this.temple_id = temple_id;
        this.create_date = create_date;
        this.update_date = update_date;
        this.is_delete = is_delete;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getPersonal_id() {
        return personal_id;
    }

    public void setPersonal_id(String personal_id) {
        this.personal_id = personal_id;
    }

    public String getAcademic_standing() {
        return academic_standing;
    }

    public void setAcademic_standing(String academic_standing) {
        this.academic_standing = academic_standing;
    }

    public String getSect() {
        return sect;
    }

    public void setSect(String sect) {
        this.sect = sect;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getSub_district() {
        return sub_district;
    }

    public void setSub_district(String sub_district) {
        this.sub_district = sub_district;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getPostal_code() {
        return postal_code;
    }

    public void setPostal_code(String postal_code) {
        this.postal_code = postal_code;
    }

    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }

    public int getTemple_id() {
        return temple_id;
    }

    public void setTemple_id(int temple_id) {
        this.temple_id = temple_id;
    }

    public Date getCreate_date() {
        return create_date;
    }

    public void setCreate_date(Date create_date) {
        this.create_date = create_date;
    }

    public Date getUpdate_date() {
        return update_date;
    }

    public void setUpdate_date(Date update_date) {
        this.update_date = update_date;
    }

    public Boolean getIs_delete() {
        return is_delete;
    }

    public void setIs_delete(Boolean is_delete) {
        this.is_delete = is_delete;
    }

    @Override
    public String toString() {
        return "Vipassana{" + "id=" + id + ", firstname=" + firstname + ", nickname=" + nickname + ", lastname=" + lastname + ", personal_id=" + personal_id + ", academic_standing=" + academic_standing + ", sect=" + sect + ", address=" + address + ", sub_district=" + sub_district + ", district=" + district + ", province=" + province + ", postal_code=" + postal_code + ", image=" + image + ", temple_id=" + temple_id + ", create_date=" + create_date + ", update_date=" + update_date + ", is_delete=" + is_delete + '}';
    }

}
