/*
 * Copy Right Pay Enterprise Co.,Ltd.
 */
package com.mycrm.domain;

/**
 *
 * @author pop
 */
public class VipassanaData {

    private Integer index;
    private String name;
    private Short color;
    private Integer vipassanaId;
    private Integer count;

    public VipassanaData() {
    }

    public VipassanaData(Integer index, String name, Short color, Integer vipassanaId, Integer count) {
        this.index = index;
        this.name = name;
        this.color = color;
        this.vipassanaId = vipassanaId;
        this.count = count;
    }

    public Integer getIndex() {
        return index;
    }

    public void setIndex(Integer index) {
        this.index = index;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Short getColor() {
        return color;
    }

    public void setColor(Short color) {
        this.color = color;
    }

    public Integer getVipassanaId() {
        return vipassanaId;
    }

    public void setVipassanaId(Integer vipassanaId) {
        this.vipassanaId = vipassanaId;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    @Override
    public String toString() {
        return "VipassanaData{" + "index=" + index + ", name=" + name + ", color=" + color + ", vipassanaId=" + vipassanaId + ", count=" + count + '}';
    }

}
