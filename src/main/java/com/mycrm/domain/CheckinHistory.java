/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycrm.domain;

import java.util.Date;

/**
 *
 * @author pop
 */
public class CheckinHistory {
    
    private Integer id;
    private Date check_in;
    private Date check_out;
    private int temple_id;
    private int room_id;
    private int room_name_id;
    private String status;
    private Integer contact_id;
    private Date create_date;
    private Date update_date;
    private Boolean is_delete;
    private Integer monk_id;

    public CheckinHistory() {
    }

    public CheckinHistory(Integer id, Date check_in, Date check_out, int temple_id, int room_id, int room_name_id, String status, Integer contact_id, Date create_date, Date update_date, Boolean is_delete, Integer monk_id) {
        this.id = id;
        this.check_in = check_in;
        this.check_out = check_out;
        this.temple_id = temple_id;
        this.room_id = room_id;
        this.room_name_id = room_name_id;
        this.status = status;
        this.contact_id = contact_id;
        this.create_date = create_date;
        this.update_date = update_date;
        this.is_delete = is_delete;
        this.monk_id = monk_id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getCheck_in() {
        return check_in;
    }

    public void setCheck_in(Date check_in) {
        this.check_in = check_in;
    }

    public Date getCheck_out() {
        return check_out;
    }

    public void setCheck_out(Date check_out) {
        this.check_out = check_out;
    }

    public int getTemple_id() {
        return temple_id;
    }

    public void setTemple_id(int temple_id) {
        this.temple_id = temple_id;
    }

    public int getRoom_id() {
        return room_id;
    }

    public void setRoom_id(int room_id) {
        this.room_id = room_id;
    }

    public int getRoom_name_id() {
        return room_name_id;
    }

    public void setRoom_name_id(int room_name_id) {
        this.room_name_id = room_name_id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getContact_id() {
        return contact_id;
    }

    public void setContact_id(Integer contact_id) {
        this.contact_id = contact_id;
    }

    public Date getCreate_date() {
        return create_date;
    }

    public void setCreate_date(Date create_date) {
        this.create_date = create_date;
    }

    public Date getUpdate_date() {
        return update_date;
    }

    public void setUpdate_date(Date update_date) {
        this.update_date = update_date;
    }

    public Boolean getIs_delete() {
        return is_delete;
    }

    public void setIs_delete(Boolean is_delete) {
        this.is_delete = is_delete;
    }

    public Integer getMonk_id() {
        return monk_id;
    }

    public void setMonk_id(Integer monk_id) {
        this.monk_id = monk_id;
    }

    @Override
    public String toString() {
        return "CheckinHistory{" + "id=" + id + ", check_in=" + check_in + ", check_out=" + check_out + ", temple_id=" + temple_id + ", room_id=" + room_id + ", room_name_id=" + room_name_id + ", status=" + status + ", contact_id=" + contact_id + ", create_date=" + create_date + ", update_date=" + update_date + ", is_delete=" + is_delete + ", monk_id=" + monk_id + '}';
    }
    
    

}