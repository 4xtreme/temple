/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycrm.domain;

import java.io.Serializable;
import org.hibernate.validator.constraints.NotEmpty;

/**
 *
 * @author pop
 */
public class Machine implements Serializable {

    private static final long serialVersionUID = 1L;

    private int id;
    @NotEmpty
    private String machine_name;
    @NotEmpty
    private String car_regis_num;
    @NotEmpty
    private String brand;
    @NotEmpty
    private String machine_model;
    @NotEmpty
    private String engine_size;
    @NotEmpty
    private String size_type;
    @NotEmpty
    private String machine_num;
    @NotEmpty
    private String body_num;
    @NotEmpty
    private String type;
    @NotEmpty
    private String category;
    private String machine_file;
    private String machine_pic;
    @NotEmpty
    private String start_date;
    @NotEmpty
    private String buy_form;
    @NotEmpty
    private String driver;
    @NotEmpty
    private String machine_status;
    private Boolean is_delete;

    public Machine() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMachine_name() {
        return machine_name;
    }

    public void setMachine_name(String machine_name) {
        this.machine_name = machine_name;
    }

    public String getCar_regis_num() {
        return car_regis_num;
    }

    public void setCar_regis_num(String car_regis_num) {
        this.car_regis_num = car_regis_num;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getMachine_model() {
        return machine_model;
    }

    public void setMachine_model(String machine_model) {
        this.machine_model = machine_model;
    }

    public String getEngine_size() {
        return engine_size;
    }

    public void setEngine_size(String engine_size) {
        this.engine_size = engine_size;
    }

    public String getSize_type() {
        return size_type;
    }

    public void setSize_type(String size_type) {
        this.size_type = size_type;
    }

    public String getMachine_num() {
        return machine_num;
    }

    public void setMachine_num(String machine_num) {
        this.machine_num = machine_num;
    }

    public String getBody_num() {
        return body_num;
    }

    public void setBody_num(String body_num) {
        this.body_num = body_num;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getMachine_file() {
        return machine_file;
    }

    public void setMachine_file(String machine_file) {
        this.machine_file = machine_file;
    }

    public String getMachine_pic() {
        return machine_pic;
    }

    public void setMachine_pic(String machine_pic) {
        this.machine_pic = machine_pic;
    }

    public String getStart_date() {
        return start_date;
    }

    public void setStart_date(String start_date) {
        this.start_date = start_date;
    }

    public String getBuy_form() {
        return buy_form;
    }

    public void setBuy_form(String buy_form) {
        this.buy_form = buy_form;
    }

    public String getDriver() {
        return driver;
    }

    public void setDriver(String driver) {
        this.driver = driver;
    }

    public String getMachine_status() {
        return machine_status;
    }

    public void setMachine_status(String machine_status) {
        this.machine_status = machine_status;
    }

    public Boolean getIs_delete() {
        return is_delete;
    }

    public void setIs_delete(Boolean is_delete) {
        this.is_delete = is_delete;
    }

    public Machine(int id, String machine_name, String car_regis_num, String brand, String machine_model, String engine_size, String size_type, String machine_num, String body_num, String type, String category, String machine_file, String machine_pic, String start_date, String buy_form, String driver, String machine_status, Boolean is_delete) {
        this.id = id;
        this.machine_name = machine_name;
        this.car_regis_num = car_regis_num;
        this.brand = brand;
        this.machine_model = machine_model;
        this.engine_size = engine_size;
        this.size_type = size_type;
        this.machine_num = machine_num;
        this.body_num = body_num;
        this.type = type;
        this.category = category;
        this.machine_file = machine_file;
        this.machine_pic = machine_pic;
        this.start_date = start_date;
        this.buy_form = buy_form;
        this.driver = driver;
        this.machine_status = machine_status;
        this.is_delete = is_delete;
    }

    @Override
    public String toString() {
        return "Machine{" + "id=" + id + ", machine_name=" + machine_name + ", car_regis_num=" + car_regis_num + ", brand=" + brand + ", machine_model=" + machine_model + ", engine_size=" + engine_size + ", size_type=" + size_type + ", machine_num=" + machine_num + ", body_num=" + body_num + ", type=" + type + ", category=" + category + ", machine_file=" + machine_file + ", machine_pic=" + machine_pic + ", start_date=" + start_date + ", buy_form=" + buy_form + ", driver=" + driver + ", machine_status=" + machine_status + ", is_delete=" + is_delete + '}';
    }




}
