/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycrm.domain;


/**
 *
 * @author Lenovo
 */
public class RoomName {
 
    private Integer id;
    private Integer room_id;
    private String name;
    private Integer amount;
    private Integer live;
    private Integer status;
    private Integer is_delete;
    private int temple_id;

    public RoomName() {
    }

    public RoomName(Integer id, Integer room_id, String name, Integer amount, Integer live, Integer status, Integer is_delete, int temple_id) {
        this.id = id;
        this.room_id = room_id;
        this.name = name;
        this.amount = amount;
        this.live = live;
        this.status = status;
        this.is_delete = is_delete;
        this.temple_id = temple_id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getRoom_id() {
        return room_id;
    }

    public void setRoom_id(Integer room_id) {
        this.room_id = room_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public Integer getLive() {
        return live;
    }

    public void setLive(Integer live) {
        this.live = live;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getIs_delete() {
        return is_delete;
    }

    public void setIs_delete(Integer is_delete) {
        this.is_delete = is_delete;
    }

    public int getTemple_id() {
        return temple_id;
    }

    public void setTemple_id(int temple_id) {
        this.temple_id = temple_id;
    }

    @Override
    public String toString() {
        return "RoomName{" + "id=" + id + ", room_id=" + room_id + ", name=" + name + ", amount=" + amount + ", live=" + live + ", status=" + status + ", is_delete=" + is_delete + ", temple_id=" + temple_id + '}';
    }

      
}
