/*
 * Copy Right Pay Enterprise Co.,Ltd.
 */
package com.mycrm.domain;

import java.util.Date;

/**
 *
 * @author pop
 */
public class EventInfo {

    private Integer id;
    private Integer event_id;
    private Integer monk_id;
    private Date create_date;
    private Date update_date;
    private Boolean is_delete;

    public EventInfo() {
    }

    public EventInfo(Integer id, Integer event_id, Integer monk_id, Date create_date, Date update_date, Boolean is_delete) {
        this.id = id;
        this.event_id = event_id;
        this.monk_id = monk_id;
        this.create_date = create_date;
        this.update_date = update_date;
        this.is_delete = is_delete;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getEvent_id() {
        return event_id;
    }

    public void setEvent_id(Integer event_id) {
        this.event_id = event_id;
    }

    public Integer getMonk_id() {
        return monk_id;
    }

    public void setMonk_id(Integer monk_id) {
        this.monk_id = monk_id;
    }

    public Date getCreate_date() {
        return create_date;
    }

    public void setCreate_date(Date create_date) {
        this.create_date = create_date;
    }

    public Date getUpdate_date() {
        return update_date;
    }

    public void setUpdate_date(Date update_date) {
        this.update_date = update_date;
    }

    public Boolean getIs_delete() {
        return is_delete;
    }

    public void setIs_delete(Boolean is_delete) {
        this.is_delete = is_delete;
    }

    @Override
    public String toString() {
        return "EventInfo{" + "id=" + id + ", event_id=" + event_id + ", monk_id=" + monk_id + ", create_date=" + create_date + ", update_date=" + update_date + ", is_delete=" + is_delete + '}';
    }

}
