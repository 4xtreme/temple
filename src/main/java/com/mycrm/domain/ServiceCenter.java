/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycrm.domain;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import org.hibernate.validator.constraints.NotEmpty;

/**
 *
 * @author pop
 */
public class ServiceCenter implements Serializable {

    private static final long serialVersionUID = 1L;

    private int id;
    @NotEmpty
    private String name;
    @NotEmpty
    private String servicecenter_type;
    @NotEmpty
    private String servicecenter_group;    
    @NotEmpty
    private String address;
    @NotEmpty
    private String street;
    @NotEmpty
    private String local;
    @NotEmpty
    private String district;
    @NotEmpty
    private String province;
    @NotEmpty
    private String zipcode;
    @NotEmpty
    private String status;
    @NotEmpty
    private String contact;
    @NotEmpty
    private String position;
    @NotEmpty
    private String email;
    @NotEmpty
    private String tel;
    private String chat;
    private String note;
    private Boolean is_delete;

    public ServiceCenter() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getServicecenter_type() {
        return servicecenter_type;
    }

    public void setServicecenter_type(String servicecenter_type) {
        this.servicecenter_type = servicecenter_type;
    }

    public String getServicecenter_group() {
        return servicecenter_group;
    }

    public void setServicecenter_group(String servicecenter_group) {
        this.servicecenter_group = servicecenter_group;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getLocal() {
        return local;
    }

    public void setLocal(String local) {
        this.local = local;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getZipcode() {
        return zipcode;
    }

    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getChat() {
        return chat;
    }

    public void setChat(String chat) {
        this.chat = chat;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Boolean getIs_delete() {
        return is_delete;
    }

    public void setIs_delete(Boolean is_delete) {
        this.is_delete = is_delete;
    }

    public ServiceCenter(int id, String name, String servicecenter_type, String servicecenter_group, String address, String street, String local, String district, String province, String zipcode, String status, String contact, String position, String email, String tel, String chat, String note, Boolean is_delete) {
        this.id = id;
        this.name = name;
        this.servicecenter_type = servicecenter_type;
        this.servicecenter_group = servicecenter_group;
        this.address = address;
        this.street = street;
        this.local = local;
        this.district = district;
        this.province = province;
        this.zipcode = zipcode;
        this.status = status;
        this.contact = contact;
        this.position = position;
        this.email = email;
        this.tel = tel;
        this.chat = chat;
        this.note = note;
        this.is_delete = is_delete;
    }

    @Override
    public String toString() {
        return "ServiceCenter{" + "id=" + id + ", name=" + name + ", servicecenter_type=" + servicecenter_type + ", servicecenter_group=" + servicecenter_group + ", address=" + address + ", street=" + street + ", local=" + local + ", district=" + district + ", province=" + province + ", zipcode=" + zipcode + ", status=" + status + ", contact=" + contact + ", position=" + position + ", email=" + email + ", tel=" + tel + ", chat=" + chat + ", note=" + note + ", is_delete=" + is_delete + '}';
    }



    
}
