/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycrm.domain;

import javax.validation.constraints.Min;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import org.hibernate.validator.constraints.NotEmpty;
import org.hibernate.validator.constraints.Range;

/**
 *
 * @author SOMON
 */
public class Monk {

    private int monk_id;
    @NotEmpty
    private String firstname;
    @NotEmpty
    private String nickname;
    @NotEmpty
    private String lastname;
    @NotEmpty
    @Size(min = 13, message="กรุณากรอกบัตรประชาชนให้ถูกต้อง")
    @Pattern(regexp = "[0-9]+", message = "กรุณากรอกตัวเลข" )
    private String personid;
    @NotEmpty
    private String position;
    @NotEmpty
    private String sect;
    private String templename;
    @NotEmpty
    private String address;
    @NotEmpty
    private String district;
    @NotEmpty
    private String amphoe;
    @NotEmpty
    private String province;
    private byte[] monkPic;
    private byte[] monkByte;
    private String monkString;
    private String event_start;
    private String event_end;
    @Size(min = 5, message="กรุณากรอกรหัสไปรษณีย์ให้ถูกต้อง")
    @Pattern(regexp = "[0-9]+", message = "กรุณากรอกตัวเลข" )
    private String postcode;
    private Boolean is_delete;
    private int temple_id;
    private byte[] pic_monk;
    private String monk_type;
    private Boolean is_deactivate;

    public Monk() {
    }

    public Monk(int monk_id, String firstname, String nickname, String lastname, String personid, String position, String sect, String templename, String address, String district, String amphoe, String province, byte[] monkPic, byte[] monkByte, String monkString, String event_start, String event_end, String postcode, Boolean is_delete, int temple_id, byte[] pic_monk, String monk_type, Boolean is_deactivate) {
        this.monk_id = monk_id;
        this.firstname = firstname;
        this.nickname = nickname;
        this.lastname = lastname;
        this.personid = personid;
        this.position = position;
        this.sect = sect;
        this.templename = templename;
        this.address = address;
        this.district = district;
        this.amphoe = amphoe;
        this.province = province;
        this.monkPic = monkPic;
        this.monkByte = monkByte;
        this.monkString = monkString;
        this.event_start = event_start;
        this.event_end = event_end;
        this.postcode = postcode;
        this.is_delete = is_delete;
        this.temple_id = temple_id;
        this.pic_monk = pic_monk;
        this.monk_type = monk_type;
        this.is_deactivate = is_deactivate;
    }

    public int getMonk_id() {
        return monk_id;
    }

    public void setMonk_id(int monk_id) {
        this.monk_id = monk_id;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getPersonid() {
        return personid;
    }

    public void setPersonid(String personid) {
        this.personid = personid;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getSect() {
        return sect;
    }

    public void setSect(String sect) {
        this.sect = sect;
    }

    public String getTemplename() {
        return templename;
    }

    public void setTemplename(String templename) {
        this.templename = templename;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getAmphoe() {
        return amphoe;
    }

    public void setAmphoe(String amphoe) {
        this.amphoe = amphoe;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public byte[] getMonkPic() {
        return monkPic;
    }

    public void setMonkPic(byte[] monkPic) {
        this.monkPic = monkPic;
    }

    public byte[] getMonkByte() {
        return monkByte;
    }

    public void setMonkByte(byte[] monkByte) {
        this.monkByte = monkByte;
    }

    public String getMonkString() {
        return monkString;
    }

    public void setMonkString(String monkString) {
        this.monkString = monkString;
    }

    public String getEvent_start() {
        return event_start;
    }

    public void setEvent_start(String event_start) {
        this.event_start = event_start;
    }

    public String getEvent_end() {
        return event_end;
    }

    public void setEvent_end(String event_end) {
        this.event_end = event_end;
    }

    public String getPostcode() {
        return postcode;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    public Boolean getIs_delete() {
        return is_delete;
    }

    public void setIs_delete(Boolean is_delete) {
        this.is_delete = is_delete;
    }

    public int getTemple_id() {
        return temple_id;
    }

    public void setTemple_id(int temple_id) {
        this.temple_id = temple_id;
    }

    public byte[] getPic_monk() {
        return pic_monk;
    }

    public void setPic_monk(byte[] pic_monk) {
        this.pic_monk = pic_monk;
    }

    public String getMonk_type() {
        return monk_type;
    }

    public void setMonk_type(String monk_type) {
        this.monk_type = monk_type;
    }

    public Boolean getIs_deactivate() {
        return is_deactivate;
    }

    public void setIs_deactivate(Boolean is_deactivate) {
        this.is_deactivate = is_deactivate;
    }

    @Override
    public String toString() {
        return "Monk{" + "monk_id=" + monk_id + ", firstname=" + firstname + ", nickname=" + nickname + ", lastname=" + lastname + ", personid=" + personid + ", position=" + position + ", sect=" + sect + ", templename=" + templename + ", address=" + address + ", district=" + district + ", amphoe=" + amphoe + ", province=" + province + ", monkPic=" + monkPic + ", monkByte=" + monkByte + ", monkString=" + monkString + ", event_start=" + event_start + ", event_end=" + event_end + ", postcode=" + postcode + ", is_delete=" + is_delete + ", temple_id=" + temple_id + ", pic_monk=" + pic_monk + ", monk_type=" + monk_type + ", is_deactivate=" + is_deactivate + '}';
    }

}
