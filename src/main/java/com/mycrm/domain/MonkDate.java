/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycrm.domain;

/**
 *
 * @author Jame
 */
public class MonkDate {
    
    private String monk_name;
    private int day1;
    private int day2;
    private int day3;
    private int day4;
    private int day5;
    private int day6;
    private int day7;
    private int day8;
    private int day9;
    private int day10;

    public String getMonk_name() {
        return monk_name;
    }

    public void setMonk_name(String monk_name) {
        this.monk_name = monk_name;
    }

    public int getDay1() {
        return day1;
    }

    public void setDay1(int day1) {
        this.day1 = day1;
    }

    public int getDay2() {
        return day2;
    }

    public void setDay2(int day2) {
        this.day2 = day2;
    }

    public int getDay3() {
        return day3;
    }

    public void setDay3(int day3) {
        this.day3 = day3;
    }

    public int getDay4() {
        return day4;
    }

    public void setDay4(int day4) {
        this.day4 = day4;
    }

    public int getDay5() {
        return day5;
    }

    public void setDay5(int day5) {
        this.day5 = day5;
    }

    public int getDay6() {
        return day6;
    }

    public void setDay6(int day6) {
        this.day6 = day6;
    }

    public int getDay7() {
        return day7;
    }

    public void setDay7(int day7) {
        this.day7 = day7;
    }

    public int getDay8() {
        return day8;
    }

    public void setDay8(int day8) {
        this.day8 = day8;
    }

    public int getDay9() {
        return day9;
    }

    public void setDay9(int day9) {
        this.day9 = day9;
    }

    public int getDay10() {
        return day10;
    }

    public void setDay10(int day10) {
        this.day10 = day10;
    }

    public MonkDate() {
    }

    public MonkDate(String monk_name, int day1, int day2, int day3, int day4, int day5, int day6, int day7, int day8, int day9, int day10) {
        this.monk_name = monk_name;
        this.day1 = day1;
        this.day2 = day2;
        this.day3 = day3;
        this.day4 = day4;
        this.day5 = day5;
        this.day6 = day6;
        this.day7 = day7;
        this.day8 = day8;
        this.day9 = day9;
        this.day10 = day10;
    }

    @Override
    public String toString() {
        return "MonkDate{" + "monk_name=" + monk_name + ", day1=" + day1 + ", day2=" + day2 + ", day3=" + day3 + ", day4=" + day4 + ", day5=" + day5 + ", day6=" + day6 + ", day7=" + day7 + ", day8=" + day8 + ", day9=" + day9 + ", day10=" + day10 + '}';
    }
    
}
