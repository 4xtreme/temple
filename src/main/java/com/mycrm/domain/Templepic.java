/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycrm.domain;

import java.sql.Blob;

/**
 *
 * @author she my sunshine
 */
public class Templepic {
    private int id;

    public Templepic() {
    }
    private String filename;
    private String file_type;
    private byte [] filedata;
    private int temple_id;
    private boolean t_delete;
    private String description;

    public Templepic(int id, String filename, String file_type, byte [] filedata, int temple_id, boolean t_delete, String description) {
        this.id = id;
        this.filename = filename;
        this.file_type = file_type;
        this.filedata = filedata;
        this.temple_id = temple_id;
        this.t_delete = t_delete;
        this.description = description;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getFile_type() {
        return file_type;
    }

    public void setFile_type(String file_type) {
        this.file_type = file_type;
    }

    public byte [] getFiledata() {
        return filedata;
    }

        public void setFiledata(byte [] filedata) {
        this.filedata = filedata;
    }

    public int getTemple_id() {
        return temple_id;
    }

    public void setTemple_id(int temple_id) {
        this.temple_id = temple_id;
    }

    public boolean isT_delete() {
        return t_delete;
    }

    public void setT_delete(boolean t_delete) {
        this.t_delete = t_delete;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "Templepic{" + "id=" + id + ", filename=" + filename + ", file_type=" + file_type + ", filedata=" + filedata + ", temple_id=" + temple_id + ", t_delete=" + t_delete + ", description=" + description + '}';
    }

      
}
