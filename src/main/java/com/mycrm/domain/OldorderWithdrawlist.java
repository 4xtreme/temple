/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycrm.domain;

/**
 *
 * @author pop
 */
public class OldorderWithdrawlist {

    private int id;
    private int old_withdraw_id;
    private int withdraw_id;
    private int index_number;
    private String broken_item;
    private String type;
    private int quantity;
    private int amount;
    private int sum_price;
    private String note;
    private String is_delete;

    public OldorderWithdrawlist() {
    }

    public OldorderWithdrawlist(int old_withdraw_id, int withdraw_id, int index_number, String broken_item, String type, int quantity, int amount, int sum_price, String note, String is_delete) {
        this.old_withdraw_id = old_withdraw_id;
        this.withdraw_id = withdraw_id;
        this.index_number = index_number;
        this.broken_item = broken_item;
        this.type = type;
        this.quantity = quantity;
        this.amount = amount;
        this.sum_price = sum_price;
        this.note = note;
        this.is_delete = is_delete;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getOld_withdraw_id() {
        return old_withdraw_id;
    }

    public void setOld_withdraw_id(int old_withdraw_id) {
        this.old_withdraw_id = old_withdraw_id;
    }

    public int getWithdraw_id() {
        return withdraw_id;
    }

    public void setWithdraw_id(int withdraw_id) {
        this.withdraw_id = withdraw_id;
    }

    public int getIndex_number() {
        return index_number;
    }

    public void setIndex_number(int index_number) {
        this.index_number = index_number;
    }

    public String getBroken_item() {
        return broken_item;
    }

    public void setBroken_item(String broken_item) {
        this.broken_item = broken_item;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public int getSum_price() {
        return sum_price;
    }

    public void setSum_price(int sum_price) {
        this.sum_price = sum_price;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getIs_delete() {
        return is_delete;
    }

    public void setIs_delete(String is_delete) {
        this.is_delete = is_delete;
    }

    @Override
    public String toString() {
        return "OldorderWithdrawlist{" + "id=" + id + ", old_withdraw_id=" + old_withdraw_id + ", withdraw_id=" + withdraw_id + ", index_number=" + index_number + ", broken_item=" + broken_item + ", type=" + type + ", quantity=" + quantity + ", amount=" + amount + ", sum_price=" + sum_price + ", note=" + note + ", is_delete=" + is_delete + '}';
    }
    
    
}
