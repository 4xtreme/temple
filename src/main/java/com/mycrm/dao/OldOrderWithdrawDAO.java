/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycrm.dao;

import com.mycrm.domain.OldorderWithdrawlist;
import java.util.List;

/**
 *
 * @author pop
 */
public interface OldOrderWithdrawDAO {
    
    public Integer insert(OldorderWithdrawlist oldOrderWithdrawlist);
    
    public List<OldorderWithdrawlist> getOrderWithdrawList(int oldWithdraw_id);
    
}
