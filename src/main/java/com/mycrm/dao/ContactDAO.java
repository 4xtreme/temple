/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycrm.dao;

import com.mycrm.domain.Contact;
import com.mycrm.domain.Monk;
import com.mycrm.domain.PeopleLiving;
import com.mycrm.domain.Product;
import com.mycrm.domain.Register;
import java.util.List;

/**
 *
 * @author Lenovo
 */
public interface ContactDAO {

    //----------------------------------------------------------------Checkmail
    public boolean isUserExists(String email);

    public Register emailfindUser(String email);

    public boolean emailExits(String email);

    public int countEmail(String email);

    public List<Register> findEmailInfo(String email);

    //------------------------------------------------------------------Check person id
    public boolean isUserExistsPID(String personid);

    public Register personidfindUser(String personid);

    public boolean personidExits(String personid);

    public List<Register> findPersonidInfo(String personid);

    public int countPersonID(String personid);

    public int findPersonidIsdelete(String personid);

    public int findEmailIsdelete(String email);

    //-------------------------------------------------------------------
    public Register findemailInfo(String email);

    public Register getContactByPersonID(String person_id);

    //public int countPersonid(String personid);
    //---------------------------------------------
    public Integer insertPic(Monk monk);

    public Contact findcontactInfo(int contactId);

    public Register findregInfo(int reg_id);

    //public Register findregInfo(int reg_id, String nametitle, String firstname, String lastname, String personid, String royal, String nationality, String birthDate, String gender, String email, String address, String village, String district, String city, String province, String postalcode, String telephone, String password, String repassword);
    public Monk findmonkInfo(int monk_id);

    public Integer insertreg(Register register);

    public List<Contact> getcontactList(int user_id);

    public List<Register> getregcontactList(int temple_id);

    public List<Monk> getmonkList(int monk_id, int temple_id);

    public void update(Contact contact);

    public void updateReg(Register register);

    public void updateMonk(Monk monk);

    public void deleteContact(int contactId);

    public void deleteContactReg(int reg_id);

    public void deleteMonk(int monk_id);

    public List<Contact> getemailList(String memtype);

    public Integer insert(Monk monk);

    public Register findcontactByID(int contactId);

    public List<Register> getRegList(int temple_id, String page, int per_page);

    public int countRows(int temple_id);

    public List<Monk> getMonkPage(int temple_id, String page, int per_page);

    public int countRowsMonk(int temple_id);

    public List<Monk> getMonkByTemple(int temple_id);

    public Register findContactByEmail(String useremail, int temple_id);

    public void updateContactProfile(Register register);

    public List<Register> findContact(String typeSearch, String message, int temple_id);

    public List<Monk> getAllMonk(int temple_id);

    public void updateCountCome(int count_come, int reg_id);

    public void updateFirstCome(int contact_id);

    public void insertContactNow(Register register);

    public void insertContactIDCard(Register register);

    public Register getRegCard(int temple_id);

    public void reOpenReg(int reg_id);

    public List<PeopleLiving> getPeopleLiving(String typeSearch, String message, int temple_id);

    public void deactivateMonk(int monk_id);

    public void activateMonk(int monk_id);
    
    public int countMonk(int temple_id, String monk_type);
    
    public int countContact(int temple_id, String gender_text);
    
    public void deleteContactRegWithReason(int reg_id, String reason);
    
    public List<Monk> searchMonk(String search, int temple_id);
    
}
