/*
 * Copy Right Pay Enterprise Co.,Ltd.
 */
package com.mycrm.dao;

import com.mycrm.domain.Activity;
import java.util.List;

/**
 *
 * @author pop
 */
public interface ActivityDAO {

    public int insert(Activity activity);

    public Activity findById(int id);

    public List<Activity> findAll(int temple_id, String page, int per_page);

    public void update(Activity activity);

    public void delete(int id);
    
    public int countRows(int temple_id);

}
