/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycrm.dao;

import com.mycrm.domain.OldWithdraw;
import java.util.List;

/**
 *
 * @author pop
 */
public interface OldWithdrawDAO {
    
        public Integer insert(OldWithdraw oldwithdraw);
        
        public List<OldWithdraw> getOldWithdrawList(int withdraw_id,String page,int per_page);
        
        public OldWithdraw findOldWithdrawId(int oldwithdrawId);
        
        public int countRows(int withdraw_id);

    
}
