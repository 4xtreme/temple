/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycrm.dao.impl.jdbc;

import com.mycrm.dao.WithdrawDAO;
import com.mycrm.domain.Withdraw;
import java.util.ArrayList;
import java.util.List;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcDaoSupport;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

/**
 *
 * @author pop
 */
public class WithdrawDAOImpl extends NamedParameterJdbcDaoSupport implements WithdrawDAO {

    private final static String SQL_INSERT
            = "INSERT INTO withdraw  (id, user_id, withdrawal_id, driver_id, amount, project_id, pay_type, pay_date, pay_date_count, store_id, machine_id, create_date, update_date, withdraw_file, withdrawal_note, inspector_note, approvers_note, total_price, status ) "
            + "VALUES (:id, :user_id, :withdrawal_id, :driver_id, :amount, :project_id, :pay_type, :pay_date, :pay_date_count, :store_id, :machine_id, :create_date, :update_date, :withdraw_file, :withdrawal_note, :inspector_note, :approvers_note, :total_price, :status) ";

    private final static String SQL_SELECT_INSERT_ID
            = "SELECT id FROM withdraw ORDER by id DESC LIMIT 1";

    private final static String SQL_SELECT_LIST
            = "select * from withdraw where status != 4 limit ? offset ?";

    private final static String SQL_SELECT
            = "select * from withdraw where id = ?";

    private final static String SQL_UPDATE
            = "UPDATE  withdraw  SET  user_id = :user_id, withdrawal_id = :withdrawal_id, driver_id = :driver_id, amount = :amount, project_id = :project_id, pay_type = :pay_type, pay_date = :pay_date, pay_date_count = :pay_date_count, store_id = :store_id, machine_id = :machine_id,"
            + " create_date = :create_date, update_date = :update_date, withdraw_file = :withdraw_file, withdrawal_note = :withdrawal_note, inspector_note = :inspector_note, approvers_note = :approvers_note, total_price = :total_price, status = :status WHERE id = :id";

     private final static String SQL_ROWS_ALL
            = "select count(*) from withdraw where  status != 4" ;
    
    @Override
    public Integer insert(Withdraw withdraw) {
        System.out.println("DAO" + withdraw.toString());
        int key = -1;
        try {
            SqlParameterSource parameterSource = new BeanPropertySqlParameterSource(withdraw);
            key = getNamedParameterJdbcTemplate().update(SQL_INSERT, parameterSource);
            key = getNamedParameterJdbcTemplate().getJdbcOperations().queryForObject(SQL_SELECT_INSERT_ID, Integer.class);
        } catch (Exception e) {
            System.out.println("Insert error: " + e.getMessage());
        }
        System.out.println("key ==> " + key);

        return key;
    }

    @Override
    public List<Withdraw> getWithdrawList(String page,int per_page) {
        BeanPropertyRowMapper<Withdraw> bookRowMapper = BeanPropertyRowMapper.newInstance(Withdraw.class);
        List<Withdraw> withdraw = new ArrayList<Withdraw>();
        withdraw = getJdbcTemplate().query(SQL_SELECT_LIST, new Object[]{ Integer.parseInt(page)*per_page,per_page}, bookRowMapper);
        System.out.println("Get Data Table is" + withdraw.toString());

        return withdraw;
    }

    @Override
    public Withdraw findWithdrawId(int withdrawId) {
        System.out.println("DAO Find WithdrawId: " + withdrawId);
        BeanPropertyRowMapper<Withdraw> bookRowMapper = BeanPropertyRowMapper.newInstance(Withdraw.class);
        Withdraw withdraw = getJdbcTemplate().queryForObject(SQL_SELECT, bookRowMapper, withdrawId);
        return withdraw;
    }

    @Override
    public void update(Withdraw withdraw) {
        System.out.println("DAO Edit:" + withdraw.toString());
        SqlParameterSource parameterSource = new BeanPropertySqlParameterSource(withdraw);
        getNamedParameterJdbcTemplate().update(SQL_UPDATE, parameterSource);
        System.out.println("After update: " + withdraw.toString());
        System.out.println("----------DAO END update---------");
    }
    
    @Override
    public int countRows() {
        int count = getJdbcTemplate().queryForObject(SQL_ROWS_ALL, new Object[] {}, Integer.class);
        return count;
    }
}
