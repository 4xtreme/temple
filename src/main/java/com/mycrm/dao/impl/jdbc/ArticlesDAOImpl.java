/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycrm.dao.impl.jdbc;

import com.mycrm.dao.ArticlesDAO;
import com.mycrm.dao.EventsDAO;
import com.mycrm.domain.Articles;
import com.mycrm.domain.Events;
import java.util.ArrayList;
import java.util.List;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcDaoSupport;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

/**
 *
 * @author SOMON
 */
public class ArticlesDAOImpl extends NamedParameterJdbcDaoSupport implements ArticlesDAO{
    
    private static final String SQL_INSERT
            = "INSERT INTO articles (staff_id, title, contents, article_pic,temple_id) "
            + "VALUES(:staff_id, :title, :contents, :article_pic,:temple_id)";
    private static final String SQL_SELECT
            = "SELECT articles.article_id, articles.staff_id,staff.firstname, staff.lastname, articles.title, articles.contents,articles.article_pic \n" +
              "FROM articles\n" +
              "INNER JOIN staff ON articles.staff_id = staff.id\n" +
              "WHERE articles.temple_id = ? AND articles.is_delete = 0 ORDER BY create_date DESC offset ? limit ?";
    private static final String SQL_SELECT_LIST
            = "SELECT * FROM articles WHERE is_delete = 0 ORDER BY create_date DESC offset ? limit ?";
    private static final String SQL_SELECT_UPDATE
            = "SELECT articles.article_id,articles.staff_id, staff.firstname, staff.lastname, articles.title, articles.contents,articles.article_pic \n" +
              "FROM articles\n" +
              "INNER JOIN staff ON articles.staff_id = staff.id\n" +
              "WHERE articles.article_id = ?";
    private static final String SQL_UPDATE
            = "UPDATE articles SET title = :title, contents = :contents, article_pic = :article_pic, update_date = NOW() WHERE article_id = :article_id";
    private static final String SQL_UPDATE_NO_PIC
            = "UPDATE articles SET title = :title, contents = :contents, update_date = NOW() WHERE article_id = :article_id";
    private static final String SQL_DELETE
            = "UPDATE articles SET is_delete = 1 WHERE article_id = :article_id";
    private final static String SQL_ROWS
            = "select count(*) from articles where temple_id = ? AND is_delete = 0" ;
    private final static String SQL_ROWS_ALL
            = "select count(*) from articles where is_delete = 0" ;
    private final static String SQL_SEARCH
            = "select articles.article_id, articles.staff_id,staff.firstname, staff.lastname, articles.title, articles.contents,articles.article_pic\n"
            + "from articles\n"
            + "INNER JOIN staff ON articles.staff_id = staff.id\n"
            + "where articles.title like ? or articles.contents like ? and articles.temple_id = ? and articles.is_delete = 0\n";

    @Override
    public Integer insert(Articles articles) {
         int key = -1;
        try {
            SqlParameterSource parameterSource = new BeanPropertySqlParameterSource(articles);
            key = getNamedParameterJdbcTemplate().update(SQL_INSERT, parameterSource);
        } catch (Exception e) {
            System.out.println("Insert Error : "+e.getMessage());
        }
        return key;
    }

    @Override
    public List<Articles> getArticlesList(int temple_id,String page,int per_page) {
        BeanPropertyRowMapper<Articles> bookRowMapper = BeanPropertyRowMapper.newInstance(Articles.class);
        List<Articles> articlelist = new ArrayList<Articles>();
        articlelist = getJdbcTemplate().query(SQL_SELECT, new Object[]{ temple_id,Integer.parseInt(page)*per_page,per_page}, bookRowMapper);
        System.out.println("Get Data Table is : "+articlelist.toString());
        return articlelist;
    }

    @Override
    public Articles findArticleInfo(int article_id) {
        BeanPropertyRowMapper<Articles> bookRowMapper = BeanPropertyRowMapper.newInstance(Articles.class);
        Articles articles = getJdbcTemplate().queryForObject(SQL_SELECT_UPDATE, bookRowMapper, article_id);
        System.out.println("DAO Get article : "+articles.toString());
        
         System.out.println(SQL_SELECT_UPDATE);
        return articles;
    }

    @Override
    public void update(Articles articles) {
        SqlParameterSource parameterSource = new BeanPropertySqlParameterSource(articles);
        getNamedParameterJdbcTemplate().update(SQL_UPDATE, parameterSource);
        System.out.println("After update : " + articles.toString());
        System.out.println("----------------DAO END UPDATE-------------------");
        System.out.println("After update  : " +SQL_UPDATE.toString());
        System.out.println(SQL_UPDATE);
    }
    
    @Override
    public void updateNopic(Articles articles) {
        SqlParameterSource parameterSource = new BeanPropertySqlParameterSource(articles);
        getNamedParameterJdbcTemplate().update(SQL_UPDATE_NO_PIC, parameterSource);
        System.out.println("After update : " + articles.toString());
        System.out.println("----------------DAO END UPDATE-------------------");
    }

    @Override
    public void delete(int article_id) {
        Articles article = new Articles();
        article.setArticle_id(article_id);
        SqlParameterSource parameterSource = new BeanPropertySqlParameterSource(article);
        getNamedParameterJdbcTemplate().update(SQL_DELETE, parameterSource);
        System.out.println("----------DAO Delete---------");
    }

    @Override
    public int countRows(int temple_id) {
        int count = getJdbcTemplate().queryForObject(SQL_ROWS, new Object[] {temple_id}, Integer.class);
        return count;
    }
    
    @Override
    public int countRowsAll() {
        int count = getJdbcTemplate().queryForObject(SQL_ROWS_ALL, new Object[] {}, Integer.class);
        return count;
    }

    @Override
    public List<Articles> getArticlePage(String page, int per_page) {
        BeanPropertyRowMapper<Articles> bookRowMapper = BeanPropertyRowMapper.newInstance(Articles.class);
        List<Articles> articlelist = new ArrayList<>();
        articlelist = getJdbcTemplate().query(SQL_SELECT_LIST, new Object[]{ Integer.parseInt(page)*per_page,per_page}, bookRowMapper);
        return articlelist;
    }

    @Override
    public List<Articles> searchArticle(String search, int temple_id) {
        BeanPropertyRowMapper<Articles> bookRowMapper = BeanPropertyRowMapper.newInstance(Articles.class);
        List<Articles> articlelist = new ArrayList<>();
        articlelist = getJdbcTemplate().query(SQL_SEARCH, new Object[]{"%"+search+"%","%"+search+"%",temple_id}, bookRowMapper);
        return articlelist;
    }
      
}
