/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycrm.dao.impl.jdbc;

import com.mycrm.dao.TempleDAO;
import com.mycrm.domain.Temple;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcDaoSupport;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

/**
 *
 * @author pop
 */
public class TempleDAOImpl extends NamedParameterJdbcDaoSupport implements TempleDAO {

    private final static String SQL_INSERT
            = "INSERT INTO temple  (name, about ,address,district,prefecture,province,postal_code,tel, latitude, longitude)"
            + "VALUES (:name, :about ,:address,:district,:prefecture,:province,:postal_code,:tel, :latitude, :longitude)";

    private final static String SQL_SELECT_INSERT_ID
            = "SELECT id FROM temple ORDER by id DESC LIMIT 1";
    
    private final static String SQL_SELECT_LIST
            = "select * from temple where is_delete = 0 offset ? limit ?";
    
    private final static String SQL_SELECT 
            = "select * from temple where id = ?";
    
    private final static String SQL_UPDATE
            = "update  temple set   name = :name, about = :about , address = :address, district = :district , prefecture = :prefecture, province = :province, postal_code = :postal_code,tel = :tel, latitude = :latitude, longitude = :longitude where id = :id";
    
    private final static String SQL_DELETE
            = "update temple set is_delete = 1 where id = :id";
    
    private final static String SQL_ROWS_ALL
            = "select count(*) from temple where  is_delete = 0" ;
    

    private final static String SQL_SELECT_ALL
            = "select * from temple where is_delete = 0";

    private final static String SQL_SELECT_LATLNG
            = "select * from temple where is_delete = 0 ";
    
    private final static String SQL_SELECT_ALL_TEST
            = "select * from temple where is_delete = 0";

    private final static String SQL_SELECT_DISTINCT
            = "SELECT DISTINCT province FROM temple WHERE is_delete = 0";
    
    private final static String SQL_SEARCH
            = "SELECT * from temple WHERE name like ? AND is_delete = 0";
    
    private final static String SQL_SEARCH_PROVINCE
            = "SELECT * from temple WHERE province like ? AND is_delete = 0";  

    
    @Override
    public Integer insert(Temple temple) {
        int key = -1;
        try {
            SqlParameterSource parameterSource = new BeanPropertySqlParameterSource(temple);
            key = getNamedParameterJdbcTemplate().update(SQL_INSERT, parameterSource);
            key = getNamedParameterJdbcTemplate().getJdbcOperations().queryForObject(SQL_SELECT_INSERT_ID, Integer.class);
        } catch (DataAccessException e) {
            System.out.println("Insert error: " + e.getMessage());
        }
        System.out.println("key ==> " + key);
        
        return key;
    }
    
    @Override
    public List<Temple> getTempleList(String page,int per_page) {
        BeanPropertyRowMapper<Temple> bookRowMapper = BeanPropertyRowMapper.newInstance(Temple.class);
        List<Temple> temple = new ArrayList<Temple>();
        temple = getJdbcTemplate().query(SQL_SELECT_LIST, new Object[]{ Integer.parseInt(page)*per_page,per_page}, bookRowMapper);
        System.out.println("--------------------------------------------"+temple.toString());
        return temple;
    }
    
    @Override
    public Temple findTemple(int templeId) {
        BeanPropertyRowMapper<Temple> bookRowMapper = BeanPropertyRowMapper.newInstance(Temple.class);
        Temple temple = getJdbcTemplate().queryForObject(SQL_SELECT, bookRowMapper, templeId);
        return temple;
    }
    
    @Override
    public void update(Temple temple) {
            System.out.println("DAO" + temple);
        SqlParameterSource parameterSource = new BeanPropertySqlParameterSource(temple);
        getNamedParameterJdbcTemplate().update(SQL_UPDATE, parameterSource);
    }
    
    @Override
    public void delete(Integer id) {
        Map<String, Object> args = new HashMap<String, Object>();
        args.put("id", id);
        getNamedParameterJdbcTemplate().update(SQL_DELETE, args);
    }
    
      @Override
    public int countRows() {
        int count = getJdbcTemplate().queryForObject(SQL_ROWS_ALL, new Object[] {}, Integer.class);
        return count;
    }
    

        @Override
    public List<Temple> getALL() {
        BeanPropertyRowMapper<Temple> bookRowMapper = BeanPropertyRowMapper.newInstance(Temple.class);
        List<Temple> temple = new ArrayList<Temple>();
        temple = getJdbcTemplate().query(SQL_SELECT_ALL, new Object[]{}, bookRowMapper);
        return temple;
    }

     @Override
    public List<Temple> getTempleForMarker(){
        BeanPropertyRowMapper<Temple> bookRowMapper = BeanPropertyRowMapper.newInstance(Temple.class);
        List<Temple> temples = new ArrayList<Temple>();
         temples = getJdbcTemplate().query(SQL_SELECT_LATLNG,  bookRowMapper);
         return temples;
    }
    
    
    @Override
    public List<Temple> getALLTest() {
        BeanPropertyRowMapper<Temple> bookRowMapper = BeanPropertyRowMapper.newInstance(Temple.class);
        List<Temple> temple = new ArrayList<Temple>();
        temple = getJdbcTemplate().query(SQL_SELECT_ALL_TEST, new Object[]{}, bookRowMapper);
        return temple;
    }

    @Override
    public List<Temple> getProvince() {
        BeanPropertyRowMapper<Temple> bookRowMapper = BeanPropertyRowMapper.newInstance(Temple.class);
        List<Temple> temple = new ArrayList<Temple>();
        temple = getJdbcTemplate().query(SQL_SELECT_DISTINCT, new Object[]{}, bookRowMapper);
        return temple;
    }

    @Override
    public List<Temple> searchTemple(String search) {
        BeanPropertyRowMapper<Temple> bookRowMapper = BeanPropertyRowMapper.newInstance(Temple.class);
        List<Temple> temple = new ArrayList<Temple>();
        temple = getJdbcTemplate().query(SQL_SEARCH, new Object[]{"%"+search+"%"}, bookRowMapper);
        return temple;
    }

    @Override
    public List<Temple> searchProvinceTemple(String search) {
        BeanPropertyRowMapper<Temple> bookRowMapper = BeanPropertyRowMapper.newInstance(Temple.class);
        List<Temple> temple = new ArrayList<Temple>();
        temple = getJdbcTemplate().query(SQL_SEARCH_PROVINCE, new Object[]{"%"+search+"%"}, bookRowMapper);
        return temple;
    }

    
}
