
package com.mycrm.dao.impl.jdbc;

import com.mycrm.dao.ChartsDAO;
import com.mycrm.domain.Charts;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcDaoSupport;

/**
 *
 * @author she my sunshine
 */
public class ChartsDAOImpl extends NamedParameterJdbcDaoSupport implements ChartsDAO {

    private final static String SQL_SELECT
            = "SELECT count(gender) as value,gender as name FROM contact WHERE create_date BETWEEN ? AND ? GROUP by gender ";

    @Override
    public List<Charts> chartsList(String startdate, String enddate,String temple_id) {
        System.out.println("time" + startdate + "++ -----------" + enddate);
        BeanPropertyRowMapper<Charts> bookRowMapper = BeanPropertyRowMapper.newInstance(Charts.class);
        List<Charts> charts = new ArrayList<Charts>();
//         charts =  getJdbcTemplate().queryForList(SQL_SELECT,new Object[]{ enddate,startdate},Charts.class);

//        charts = getJdbcTemplate().query("SELECT count(gender) as value,gender as name FROM contact WHERE create_date BETWEEN '" + enddate + "' AND '" + startdate + "' AND temple_id = '"+temple_id+"' GROUP by gender", bookRowMapper);
          charts = getJdbcTemplate().query("SELECT count(contact.gender_text) as value,contact.gender_text as name FROM contact LEFT JOIN checkin_history ON contact.reg_id = checkin_history.contact_id WHERE checkin_history.check_in BETWEEN '" + enddate + "' AND '" + startdate + "' AND checkin_history.temple_id = '"+temple_id+"' GROUP BY contact.gender_text", bookRowMapper);
//        SELECT count(contact.gender) as value,contact.gender as name FROM contact LEFT JOIN checkin_history ON contact.reg_id = checkin_history.contact_id WHERE checkin_history.check_in BETWEEN '" + enddate + "' AND '" + startdate + "' AND checkin_history.temple_id = '"+temple_id+"' GROUP BY contact.gender
//        System.out.println("SELECT count(gender) as value,gender as name FROM contact WHERE create_date BETWEEN '" + enddate + "' AND '" + startdate + "' GROUP by gender" + charts.toString());

        return charts;

    }

}
