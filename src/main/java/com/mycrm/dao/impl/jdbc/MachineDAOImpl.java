/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycrm.dao.impl.jdbc;

import com.mycrm.dao.MachineDAO;
import com.mycrm.domain.Machine;
import com.mycrm.domain.ServiceCenter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcDaoSupport;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

/**
 *
 * @author pop
 */
public class MachineDAOImpl extends NamedParameterJdbcDaoSupport implements MachineDAO {
    
    private final static String SQL_INSERT
            = "insert into machine (id, machine_name, car_regis_num, brand, machine_model, engine_size, size_type, machine_num, body_num, type, category, machine_file, machine_picture, start_date, buy_form, driver, machine_status, is_delete ) "
            + "values(:id,:machine_name, :car_regis_num, :brand, :machine_model, :engine_size, :size_type, :machine_num, :body_num, :type, :category, :machine_file, :machine_pic, :start_date, :buy_form, :driver, :machine_status, :is_delete )";
    
    private final static String SQL_SEARCH
            = "SELECT * FROM machine WHERE car_regis_num = ? OR machine_num = ?";
    
    private final static String SQL_SELECT
            = "select * from machine where id = ?";
    
    private final static String SQL_SELECT_LIST
            = "select * from machine where is_delete = false limit ? offset ?";
    
    private final static String SQL_UPDATE
            = "update  machine set  machine_name = :machine_name, car_regis_num = :car_regis_num, brand = :brand, machine_model = :machine_model, engine_size = :engine_size, size_type = :size_type, machine_num = :machine_num, body_num = :body_num, type = :type, category = :category, machine_file = :machine_file, machine_pic = :machine_pic, start_date = :start_date, buy_form = :buy_form, driver = :driver, machine_status = :machine_status where id = :id";
    
    private final static String SQL_DELETE
            = "update machine set is_delete = true  where id = :id";
    
    private final static String SQL_ROWS_ALL
            = "select count(*) from machine where  is_delete = false" ;
    
    @Override
    public Integer insert(Machine machine) {
        System.out.println("DAO" + machine.toString());
        int key = -1;
        try {
            SqlParameterSource parameterSource = new BeanPropertySqlParameterSource(machine);
            key = getNamedParameterJdbcTemplate().update(SQL_INSERT, parameterSource);
        } catch (Exception e) {
            System.out.println("Insert error: "+e.getMessage());
        }
        return key;
    }
    
    @Override
    public Machine searchData(String search) {
        BeanPropertyRowMapper<Machine> bookRowMapper = BeanPropertyRowMapper.newInstance(Machine.class);
        Machine machine = new Machine();
        machine = getJdbcTemplate().queryForObject(SQL_SEARCH, bookRowMapper, search,search);
        return machine;
    }
    
    @Override
    public Machine findMachineId(int machineId) {
        BeanPropertyRowMapper<Machine> bookRowMapper = BeanPropertyRowMapper.newInstance(Machine.class);
        Machine withdraw = getJdbcTemplate().queryForObject(SQL_SELECT, bookRowMapper, machineId);
        return withdraw;
    }
    
    @Override
    public List<Machine> getMachineList(String page,int per_page) {
        BeanPropertyRowMapper<Machine> bookRowMapper = BeanPropertyRowMapper.newInstance(Machine.class);
        List<Machine> machine = new ArrayList<Machine>();
        machine = getJdbcTemplate().query(SQL_SELECT_LIST, new Object[]{ Integer.parseInt(page)*per_page,per_page}, bookRowMapper);
        return machine;
    }
        
    @Override
    public void update(Machine machine) {
        SqlParameterSource parameterSource = new BeanPropertySqlParameterSource(machine);
        getNamedParameterJdbcTemplate().update(SQL_UPDATE, parameterSource);
    }
    
    @Override
    public void delete(Integer id) {
        System.out.println("DAO");
        Map<String, Object> args = new HashMap<String, Object>();
        args.put("id", id);
        getNamedParameterJdbcTemplate().update(SQL_DELETE, args);
    }
    
      @Override
    public int countRows() {
        int count = getJdbcTemplate().queryForObject(SQL_ROWS_ALL, new Object[] {}, Integer.class);
        return count;
    }

    
}
