/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycrm.dao.impl.jdbc;

import com.mycrm.dao.RoomDAO;
import com.mycrm.domain.Room;
import com.mycrm.domain.RoomName;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcDaoSupport;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

/**
 *
 * @author pop
 */
public class RoomDAOImpl extends NamedParameterJdbcDaoSupport implements RoomDAO {

    private final static String SQL_INSERT
            = "INSERT INTO room  (temple_id, name, type, sex, toilet_type, toilet_amount, status) VALUES (:temple_id, :name, :type, :sex, :toilet_type, :toilet_amount, :status) ";

    private final static String SQL_SELECT_LIST
            = "select * from room where temple_id = ? AND is_delete = 0 ORDER BY id offset ? limit ?";

    private final static String SQL_ROWS_ALL
            = "select count(*) from room where temple_id = ? AND is_delete = 0";

    private final static String SQL_DELETE
            = "update room set is_delete = 1  where id = :id";

    private final static String SQL_SELECT
            = "select * from room where id = ?";

    private final static String SQL_UPDATE
            = "UPDATE  room  SET temple_id = :temple_id, name = :name, type = :type, sex = :sex, toilet_type = :toilet_type, toilet_amount = :toilet_amount, status = :status WHERE id = :id";

    private final static String SQL_SEARCH
            = "select * from room where status = 'ว่าง' and temple_id = ? and sex = ? AND is_delete = 0 ";

    private final static String SQL_COUNT_SEARCH
            = "select count(*) from room where status = 'ว่าง' and temple_id = ? and sex = ? AND is_delete = 0 ";

    private final static String SQL_INSERT_NAME
            = "INSERT INTO room_name (room_id, name, amount, live, status, is_delete, temple_id ) VALUES ( :room_id, :name, :amount, :live, :status, :is_delete, :temple_id )";

    private final static String SQL_SELECT_ROOM_NAME_LIST
            = "select * from room_name where room_id = ? and is_delete = 0 ";

    private final static String SQL_COUNT_ROOM_NAME
            = "select count(*) from room_name where room_id = ? and is_delete = 0 ";

    private final static String SQL_SUM_AMOUNT
            = "select sum(amount) from room_name where room_id = ? and is_delete = 0";

    private final static String SQL_DELETE_ROOM
            = "update room_name set is_delete = 1  where id = :id";

    private final static String SQL_COUNT_ALL_ROOMNAME
            = "select count(*) from room_name where is_delete = 0  ";

    private final static String SQL_SELECT_ALL_ROOMNAME
            = "select * from room_name where is_delete = 0 ";

    private final static String SQL_COUNT_SEARCH_ROOMNAME
            = "select count(*) from room_name where status = 0 AND is_delete = 0";

    private final static String SQL_SEARCH_ROOMNAME
            = "select * from room_name where status = 0 AND is_delete = 0";

    private final static String SQL_SELECT_ROOM_NAME
            = "SELECT * FROM room_name where id = ? and is_delete = 0";

    private final static String SQL_UPDATE_ROOMNAME
            = "UPDATE room_name SET id =:id, room_id=:room_id, name=:name, amount=:amount, live=:live, status=:status, is_delete=:is_delete WHERE id = :id ";

    private final static String SQL_UPDATE_RESERVE_ROOM
            = "UPDATE room_name SET live = :live, status = 0";

    private final static String SQL_SELECT_ROOM_BY_TEMPLE
            = "SELECT * FROM room WHERE temple_id = ? AND is_delete = 0 ORDER BY id";

    private final static String SQL_SELECT_ROOMNAME_BY_TEMPLE
            = "SELECT * FROM room_name WHERE temple_id = ? AND is_delete = 0 ORDER BY id";

    private final static String SQL_SELECT_ROOMNAME_BYROOM
            = "SELECT * FROM room_name WHERE room_id = ? and is_delete = 0 ORDER BY id";

    @Override
    public Integer insert(Room room) {
        int key = -1;
        SqlParameterSource parameterSource = new BeanPropertySqlParameterSource(room);
        key = getNamedParameterJdbcTemplate().update(SQL_INSERT, parameterSource);
        return key;
    }

    @Override
    public Integer insertRoomName(RoomName roomname) {
        int key = -1;
        SqlParameterSource parameterSource = new BeanPropertySqlParameterSource(roomname);
        key = getNamedParameterJdbcTemplate().update(SQL_INSERT_NAME, parameterSource);
        return key;
    }

    @Override
    public List<Room> getRoomList(int temple_id, String page, int per_page) {
        BeanPropertyRowMapper<Room> bookRowMapper = BeanPropertyRowMapper.newInstance(Room.class);
        List<Room> rooms = new ArrayList<Room>();
        rooms = getJdbcTemplate().query(SQL_SELECT_LIST, new Object[]{temple_id, Integer.parseInt(page) * per_page, per_page}, bookRowMapper);
        return rooms;
    }

    @Override
    public int countRows(int temple_id) {
        int count = getJdbcTemplate().queryForObject(SQL_ROWS_ALL, new Object[]{temple_id}, Integer.class);
        return count;
    }

    @Override
    public void delete(Integer id) {
        Map<String, Object> args = new HashMap<String, Object>();
        args.put("id", id);
        getNamedParameterJdbcTemplate().update(SQL_DELETE, args);
    }

    @Override
    public Room find(int id) {
        BeanPropertyRowMapper<Room> bookRowMapper = BeanPropertyRowMapper.newInstance(Room.class);
        Room room = getJdbcTemplate().queryForObject(SQL_SELECT, bookRowMapper, id);
        return room;
    }

    @Override
    public void update(Room room) {
        SqlParameterSource parameterSource = new BeanPropertySqlParameterSource(room);
        getNamedParameterJdbcTemplate().update(SQL_UPDATE, parameterSource);
    }

    @Override
    public List<Room> getRoomSearch(int temple_id, String page, int per_page, String sex) {
        BeanPropertyRowMapper<Room> bookRowMapper = BeanPropertyRowMapper.newInstance(Room.class);
        List<Room> rooms = new ArrayList<Room>();
        System.out.println("Temple ID:" + temple_id);
        System.out.println("Page:" + page);
        System.out.println("Per Page:" + per_page);
        System.out.println("Gender:" + sex);
        rooms = getJdbcTemplate().query(SQL_SEARCH, new Object[]{temple_id, sex}, bookRowMapper);
        return rooms;
    }

    @Override
    public int countRowSearch(int temple_id, String page, int per_page, String sex) {

        int count = getJdbcTemplate().queryForObject(SQL_COUNT_SEARCH, new Object[]{temple_id, sex}, Integer.class);
        return count;
    }

    @Override
    public List<RoomName> getRoomName(int room_id) {
        BeanPropertyRowMapper<RoomName> bookRowMapper = BeanPropertyRowMapper.newInstance(RoomName.class);
        List<RoomName> rooms = new ArrayList<RoomName>();
        rooms = getJdbcTemplate().query(SQL_SELECT_ROOM_NAME_LIST, new Object[]{room_id}, bookRowMapper);
        return rooms;
    }

    @Override
    public List<RoomName> getAllRoomName(String page, int per_page) {
        BeanPropertyRowMapper<RoomName> bookRowMapper = BeanPropertyRowMapper.newInstance(RoomName.class);
        List<RoomName> rooms = new ArrayList<RoomName>();
        rooms = getJdbcTemplate().query(SQL_SELECT_ALL_ROOMNAME, bookRowMapper);
        return rooms;
    }

    @Override
    public int countRoomNameRows() {
        int count = getJdbcTemplate().queryForObject(SQL_COUNT_ALL_ROOMNAME, new Object[]{}, Integer.class);
        return count;
    }

    @Override
    public int countRoomName(int room_id) {
        int count = 0;
        try {
            count = getJdbcTemplate().queryForObject(SQL_COUNT_ROOM_NAME, new Object[]{room_id}, Integer.class);
        } catch (Exception e) {
            e.getMessage();
        }

        return count;
    }

    @Override
    public int sumRoomName(int room_id) {
        int count = 0;
        try {
            count = getJdbcTemplate().queryForObject(SQL_SUM_AMOUNT, new Object[]{room_id}, Integer.class);
        } catch (Exception e) {
            e.getMessage();
        }

        return count;
    }

    @Override
    public void deleteRoom(Integer id) {
        Map<String, Object> args = new HashMap<String, Object>();
        args.put("id", id);
        getNamedParameterJdbcTemplate().update(SQL_DELETE_ROOM, args);
    }

    @Override
    public List<RoomName> getRoomNameSearch(String page, int per_page) {
        BeanPropertyRowMapper<RoomName> bookRowMapper = BeanPropertyRowMapper.newInstance(RoomName.class);
        List<RoomName> rooms = new ArrayList<RoomName>();
        rooms = getJdbcTemplate().query(SQL_SEARCH_ROOMNAME, bookRowMapper);
        return rooms;
    }

    @Override
    public int countRowRoomNameSearch(String page, int per_page) {
        int count = getJdbcTemplate().queryForObject(SQL_COUNT_SEARCH_ROOMNAME, Integer.class);
        return count;
    }

    @Override
    public RoomName getRoomNameByID(int id) {
        BeanPropertyRowMapper<RoomName> bookRowMapper = BeanPropertyRowMapper.newInstance(RoomName.class);
        RoomName room_name = getJdbcTemplate().queryForObject(SQL_SELECT_ROOM_NAME, bookRowMapper, id);
        return room_name;
    }

    @Override
    public void updateRoomName(RoomName room) {
        SqlParameterSource parameterSource = new BeanPropertySqlParameterSource(room);
        getNamedParameterJdbcTemplate().update(SQL_UPDATE_ROOMNAME, parameterSource);
    }

    @Override
    public void updateRoomReserve() {
        Map<String, Object> args = new HashMap<String, Object>();
        args.put("live", 0);
        args.put("checkin", 1);
        args.put("checkout", 2);
        getNamedParameterJdbcTemplate().update(SQL_UPDATE_RESERVE_ROOM, args);
        System.out.println("args : "+args);
    }

    @Override
    public List<Room> getRoomByTemple(int temple_id) {
        BeanPropertyRowMapper<Room> bookRowMapper = BeanPropertyRowMapper.newInstance(Room.class);
        List<Room> rooms = new ArrayList<Room>();
        rooms = getJdbcTemplate().query(SQL_SELECT_ROOM_BY_TEMPLE, new Object[]{temple_id}, bookRowMapper);
        return rooms;
    }

    @Override
    public List<RoomName> getRoomNameByTemple(int temple_id) {
        BeanPropertyRowMapper<RoomName> bookRowMapper = BeanPropertyRowMapper.newInstance(RoomName.class);
        List<RoomName> roomNames = new ArrayList<RoomName>();
        roomNames = getJdbcTemplate().query(SQL_SELECT_ROOMNAME_BY_TEMPLE, new Object[]{temple_id}, bookRowMapper);
        return roomNames;
    }

    @Override
    public List<RoomName> getRoomNameByRoom(int room_id) {
        BeanPropertyRowMapper<RoomName> bookRowMapper = BeanPropertyRowMapper.newInstance(RoomName.class);
        List<RoomName> roomNames = new ArrayList<RoomName>();
        roomNames = getJdbcTemplate().query(SQL_SELECT_ROOMNAME_BYROOM, new Object[]{room_id}, bookRowMapper);
        return roomNames;
    }

}
