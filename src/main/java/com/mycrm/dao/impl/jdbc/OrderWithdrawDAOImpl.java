/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycrm.dao.impl.jdbc;

import com.mycrm.dao.OrderWithdrawDAO;
import com.mycrm.domain.OrderWithdrawlist;
import java.util.ArrayList;
import java.util.List;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcDaoSupport;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

/**
 *
 * @author pop
 */
public class OrderWithdrawDAOImpl extends NamedParameterJdbcDaoSupport implements OrderWithdrawDAO {

    private final static String SQL_INSERT
            = "INSERT INTO order_withdrawlist( withdraw_id, index_number, broken_item, type, quantity, amount, sum_price, note) "
            + "VALUES ( :withdraw_id, :index_number, :broken_item, :type, :quantity, :amount, :sum_price, :note)";

    private final static String SQL_SELECT_LIST
            = "select * from order_withdrawlist where withdraw_id = ? AND is_delete = false";

    private final static String SQL_UPDATE
            = "UPDATE  order_withdrawlist SET withdraw_id = :withdraw_id, index_number = :index_number, broken_item = :broken_item, type = :type, quantity = :quantity, amount = :amount, sum_price = :sum_price, note = :note, is_delete = :is_delete WHERE id = :id";

    @Override
    public Integer insert(OrderWithdrawlist orderWithdrawlist) {
        System.out.println("DAO" + orderWithdrawlist.toString());
        int key = -1;
        try {
            SqlParameterSource parameterSource = new BeanPropertySqlParameterSource(orderWithdrawlist);
            key = getNamedParameterJdbcTemplate().update(SQL_INSERT, parameterSource);
        } catch (Exception e) {
            System.out.println("Insert error: " + e.getMessage());
        }

        return key;
    }

    @Override
    public List<OrderWithdrawlist> getOrderWithdrawList(int withdraw_id) {
        BeanPropertyRowMapper<OrderWithdrawlist> bookRowMapper = BeanPropertyRowMapper.newInstance(OrderWithdrawlist.class);
        List<OrderWithdrawlist> orderWithdraw = new ArrayList<OrderWithdrawlist>();
        orderWithdraw = getJdbcTemplate().query(SQL_SELECT_LIST, bookRowMapper, withdraw_id);
        System.out.println("Get Data Table is" + orderWithdraw.toString());

        return orderWithdraw;
    }

    @Override
    public void update(OrderWithdrawlist orderWithdraw) {
        SqlParameterSource parameterSource = new BeanPropertySqlParameterSource(orderWithdraw);
        getNamedParameterJdbcTemplate().update(SQL_UPDATE, parameterSource);
    }
}
