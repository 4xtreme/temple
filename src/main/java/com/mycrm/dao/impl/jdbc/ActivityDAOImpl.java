/*
 * Copy Right Pay Enterprise Co.,Ltd.
 */
package com.mycrm.dao.impl.jdbc;

import com.mycrm.dao.ActivityDAO;
import com.mycrm.domain.Activity;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcDaoSupport;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;

/**
 *
 * @author pop
 */
public class ActivityDAOImpl extends NamedParameterJdbcDaoSupport implements ActivityDAO {

    private static String SQL_INSERT
            = "INSERT INTO activity (activity_name, detail, activity_start, activity_end, temple_id, vipassana_id, create_date, update_date, is_delete) VALUES (:activity_name, :detail, :activity_start, :activity_end, :temple_id, :vipassana_id, NOW(), NOW(), false)";

    private static String SQL_SELECT
            = "SELECT * FROM activity WHERE id = ? AND is_delete = false";

    private static String SQL_SELECT_ALL
            = "SELECT activity.id, activity.activity_name, activity.detail, activity.activity_start, activity.activity_end, activity.temple_id, activity.vipassana_id, activity.create_date, activity.update_date, activity.is_delete, "
            + "vipassana.firstname AS vipassana_name "
            + "FROM activity "
            + "LEFT JOIN vipassana on vipassana.id = activity.vipassana_id "
            + "WHERE activity.temple_id = ? AND activity.is_delete = false "
            + "ORDER BY id ASC "
            + "OFFSET ? LIMIT ?";

    private static String SQL_UPDATE
            = "UPDATE activity SET activity_name = :activity_name, detail = :detail, activity_start = :activity_start, activity_end = :activity_end, temple_id = :temple_id, vipassana_id = :vipassana_id, update_date = NOW() WHERE id = :id AND is_delete = false";

    private static String SQL_DELETE
            = "UPDATE activity SET is_delete = true, update_date = NOW() WHERE id = :id";

    private static String SQL_COUNT_ROW
            = "SELECT COUNT(*) FROM activity WHERE temple_id = ? AND is_delete = false";

    @Override
    public int insert(Activity activity) {
        int last_id = 0;
        GeneratedKeyHolder generatedKeyHolder = new GeneratedKeyHolder();
        SqlParameterSource parameterSource = new BeanPropertySqlParameterSource(activity);
        try {
            getNamedParameterJdbcTemplate().update(SQL_INSERT, parameterSource, generatedKeyHolder);
            Map<String, Object> newId = generatedKeyHolder.getKeys();
            last_id = (int) newId.get("id");

        } catch (Exception e) {
            System.out.println("Error:" + e.getMessage());
        }
        return last_id;
    }

    @Override
    public Activity findById(int id) {
        Activity activity = new Activity();
        try {
            BeanPropertyRowMapper<Activity> bookRowMapper = BeanPropertyRowMapper.newInstance(Activity.class);
            activity = getJdbcTemplate().queryForObject(SQL_SELECT, bookRowMapper, id);
        } catch (Exception e) {
            System.out.println("Select activity Error:" + e.getMessage());
        }
        return activity;
    }

    @Override
    public List<Activity> findAll(int temple_id, String page, int per_page) {
        List<Activity> activitys = new ArrayList<>();
        try {
            BeanPropertyRowMapper<Activity> bookRowMapper = BeanPropertyRowMapper.newInstance(Activity.class);
            activitys = getJdbcTemplate().query(SQL_SELECT_ALL, new Object[]{temple_id, Integer.parseInt(page) * per_page, per_page}, bookRowMapper);
        } catch (Exception e) {
            System.out.println("Select All vipassana Error:" + e.getMessage());
        }
        return activitys;
    }

    @Override
    public void update(Activity activity) {
        SqlParameterSource parameterSource = new BeanPropertySqlParameterSource(activity);
        getNamedParameterJdbcTemplate().update(SQL_UPDATE, parameterSource);
    }

    @Override
    public void delete(int id) {
        Activity activity = new Activity();
        activity.setId(id);
        SqlParameterSource parameterSource = new BeanPropertySqlParameterSource(activity);
        getNamedParameterJdbcTemplate().update(SQL_DELETE, parameterSource);
    }

    @Override
    public int countRows(int temple_id) {
        int count = getJdbcTemplate().queryForObject(SQL_COUNT_ROW, new Object[]{temple_id}, Integer.class);
        return count;
    }

}
