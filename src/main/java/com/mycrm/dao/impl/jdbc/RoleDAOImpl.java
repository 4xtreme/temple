/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycrm.dao.impl.jdbc;

import com.mycrm.dao.RoleDAO;
import com.mycrm.domain.Role;
import com.mycrm.domain.RoleItem;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcDaoSupport;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;

/**
 *
 * @author pop
 */
public class RoleDAOImpl extends NamedParameterJdbcDaoSupport implements RoleDAO {

    private final static String SQL_INSERT
            = "INSERT INTO role(role_name) VALUES (:role_name)";

    private final static String SQL_SELECT
            = "SELECT * FROM role WHERE is_delete = 0";

    private final static String SQL_INSERT_NAME
            = "INSERT INTO role_item (role_id, page, view_page, create_page, edit_page, delete_page ) VALUES ( :role_id, :page, :view_page, :create_page, :edit_page, :delete_page)";

    private final static String SQL_SELECT_VIEW_PAGE
            = "SELECT view_page FROM role_item WHERE page = ? and role_id = ?";

    private final static String SQL_SELECT_CREATE_PAGE
            = "SELECT create_page FROM role_item WHERE page = ? and role_id = ?";

    private final static String SQL_SELECT_EDIT_PAGE
            = "SELECT edit_page FROM role_item WHERE page = ? and role_id = ?";

    private final static String SQL_SELECT_DELETE_PAGE
            = "SELECT delete_page FROM role_item WHERE page = ? and role_id = ?";

    private final static String SQL_SELECT_ITEM
            = "SELECT * FROM role_item WHERE role_id = ? ORDER BY id";

    private final static String SQL_UPDATE_ITEM
            = "update role_item set view_page = :view_page , create_page = :create_page, edit_page = :edit_page, delete_page = :delete_page  where role_id = :role_id and page = :page";

    private final static String SQL_DELETE
            = "update role set is_delete = 1  where id = :id";

    private final static String SQL_SELECT_NAME
            = "SELECT COUNT(*) FROM role where role_name = ? and is_delete = 0";
private final static String SQL_SELECT_NAME_REPORT
            = "SELECT COUNT(*) FROM role_item where page = 'report' and role_id = ? and is_delete = 0";
    private final static String SQL_SELECT_BY_NAME
            = "SELECT * FROM role_item where page = ? and is_delete = 0 and role_id = ?";
    private final static String SQL_SELECT_FIND_REPORT
            = "SELECT count(*) as value FROM role_item where page = 'report' and is_delete = 0 and role_id = ?";
    private final static String SQL_INSERT_REPORT
            = "INSERT INTO role_item (role_id, page, view_page, create_page, edit_page, delete_page ) VALUES ( ?, 'report', true, true, true, true)";
    @Override
    public Integer insert(Role role) {
        int lastid = 0;
        GeneratedKeyHolder generatedKeyHolder = new GeneratedKeyHolder();
        SqlParameterSource parameterSource = new BeanPropertySqlParameterSource(role);
        getNamedParameterJdbcTemplate().update(SQL_INSERT, parameterSource, generatedKeyHolder);
        Map<String, Object> newId = generatedKeyHolder.getKeys();
        lastid = (int) newId.get("id");

        return lastid;
    }

    @Override
    public List<Role> getRole() {
        BeanPropertyRowMapper<Role> bookRowMapper = BeanPropertyRowMapper.newInstance(Role.class);
        List<Role> role = new ArrayList<Role>();
        role = getJdbcTemplate().query(SQL_SELECT, bookRowMapper);
        return role;
    }

    @Override
    public Integer insertRoleItem(RoleItem roleitem) {
        int key = -1;
        SqlParameterSource parameterSource = new BeanPropertySqlParameterSource(roleitem);
        key = getNamedParameterJdbcTemplate().update(SQL_INSERT_NAME, parameterSource);
        return key;
    }
    
    @Override
    public void insertReport(Integer role_id){
        getJdbcTemplate().update(SQL_INSERT_REPORT, new Object[]{role_id});
    }

    @Override
    public Boolean pageView(String page, Integer role_id) {
        Boolean check = getJdbcTemplate().queryForObject(SQL_SELECT_VIEW_PAGE, new Object[]{page, role_id}, Boolean.class);
        return check;
    }

    @Override
    public Boolean pageCreate(String page, Integer role_id) {
        Boolean check = getJdbcTemplate().queryForObject(SQL_SELECT_CREATE_PAGE, new Object[]{page, role_id}, Boolean.class);
        return check;
    }

    @Override
    public Boolean pageEdit(String page, Integer role_id) {
        Boolean check = getJdbcTemplate().queryForObject(SQL_SELECT_EDIT_PAGE, new Object[]{page, role_id}, Boolean.class);
        return check;
    }

    @Override
    public Boolean pageDelete(String page, Integer role_id) {
        Boolean check = getJdbcTemplate().queryForObject(SQL_SELECT_DELETE_PAGE, new Object[]{page, role_id}, Boolean.class);
        return check;
    }

    @Override
    public List<RoleItem> getRoleITem(int id) {
        BeanPropertyRowMapper<RoleItem> bookRowMapper = BeanPropertyRowMapper.newInstance(RoleItem.class);
        List<RoleItem> roleitem = new ArrayList<RoleItem>();
        roleitem = getJdbcTemplate().query(SQL_SELECT_ITEM, bookRowMapper, id);
        return roleitem;
    }

    @Override
    public void updateRoleItem(RoleItem roleitem) {
        SqlParameterSource parameterSource = new BeanPropertySqlParameterSource(roleitem);
        getNamedParameterJdbcTemplate().update(SQL_UPDATE_ITEM, parameterSource);
    }

    @Override
    public void delete(Integer id) {
        Map<String, Object> args = new HashMap<String, Object>();
        args.put("id", id);
        getNamedParameterJdbcTemplate().update(SQL_DELETE, args);
    }

    @Override
    public int checkName(String name) {
        int check = 1;
        check = getJdbcTemplate().queryForObject(SQL_SELECT_NAME, new Object[]{name}, Integer.class);
        return check;
    }
    @Override
    public int checkNameReport(Integer id) {
        int check = 1;
        check = getJdbcTemplate().queryForObject(SQL_SELECT_NAME_REPORT, new Object[]{id}, Integer.class);
        return check;
    }

    @Override
    public RoleItem getRoleItemByPage(String pageName, int roleId) {
        RoleItem roleItem = new RoleItem();
        try {
            BeanPropertyRowMapper<RoleItem> bookRowMapper = BeanPropertyRowMapper.newInstance(RoleItem.class);
            roleItem = getJdbcTemplate().queryForObject(SQL_SELECT_BY_NAME, bookRowMapper, new Object[]{pageName, roleId});
        } catch (Exception e) {
            System.out.println("Error:" + e.getMessage());
        }

        return roleItem;
    }
    
}
