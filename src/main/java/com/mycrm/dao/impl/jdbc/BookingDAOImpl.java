/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycrm.dao.impl.jdbc;

import com.mycrm.dao.BookingDAO;
import com.mycrm.domain.CheckinHistory;
import com.mycrm.domain.OverviewData;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcDaoSupport;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class BookingDAOImpl extends NamedParameterJdbcDaoSupport implements BookingDAO {

    private static final String SQL_SELECT
            = "select * from checkin_history WHERE id = ?";

    private static final String SQL_INSERT
            = "INSERT INTO checkin_history( check_in, check_out, temple_id, room_id, room_name_id, status, contact_id, create_date, update_date, is_delete, monk_id) VALUES (:check_in, :check_out, :temple_id, :room_id,:room_name_id, :status, :contact_id, :create_date, :update_date, 0, :monk_id)";

    private static final String SQL_UPDATE
            = "UPDATE checkin_history SET id= :id,check_in= :check_in ,check_out= :check_out,temple_id= :temple_id,room_id= :room_id,room_name_id = :room_name_id ,status= :status,personal_id= :personal_id WHERE id = :id";

    private static final String SQL_DELETE
            = "DELETE FROM checkin_history WHERE id = :id";

    private static final String SQL_SELECT_BY_TEMPLE
            = "SELECT * FROM checkin_history WHERE temple_id = ? and status != 'เช็คเอ๊า' and is_delete = 0";

    private static final String SQL_SELECT_OVERALL_BUILDING
            = "SELECT checkin_history.id,checkin_history,(checkin_history.check_in) as checkin_day,(checkin_history.check_in) as checkin_month  , (checkin_history.check_in) as checkin_year,\n"
            + "(checkin_history.check_out) as checkout_day, (checkin_history.check_out) as checkout_month  , (checkin_history.check_out) as checkout_year,\n"
            + "checkin_history.room_id, checkin_history.room_name_id, checkin_history.status as type,room.name as builder_name, room_name.name as room_name, CONCAT(contact.firstname,'     ',contact.lastname) as contact_name\n"
            + "FROM checkin_history \n"
            + "LEFT JOIN room on room.id = checkin_history.room_id\n"
            + "LEFT JOIN room_name on room_name.id = checkin_history.room_name_id\n"
            + "LEFT JOIN contact on contact.reg_id = checkin_history.contact_id "
            + "WHERE checkin_history.temple_id = ? AND checkin_history.room_id = ? AND check_in BETWEEN NOW() - interval '7 day' AND NOW() + interval '7 day' AND checkin_history.is_delete = 0 "
            + "ORDER BY checkin_history.room_name_id";

    private static final String SQL_SELECT_OVERALL_ROOM
            = "SELECT checkin_history.id, (checkin_history.check_in) as checkin_day, (checkin_history.check_in) as checkin_month  , (checkin_history.check_in) as checkin_year,\n"
            + " (checkin_history.check_out) as checkout_day, (checkin_history.check_out) as checkout_month  , (checkin_history.check_out) as checkout_year,\n"
            + " checkin_history.room_id, checkin_history.room_name_id, checkin_history.status as type,room.name as builder_name, room_name.name as room_name, CONCAT(contact.firstname,'     ', contact.lastname) as contact_name\n"
            + "FROM checkin_history \n"
            + "LEFT JOIN room on room.id = checkin_history.room_id\n"
            + "LEFT JOIN room_name on room_name.id = checkin_history.room_name_id\n"
            + "LEFT JOIN contact on contact.reg_id = checkin_history.contact_id WHERE checkin_history.temple_id = ? AND checkin_history.room_name_id = ? AND check_in BETWEEN NOW() - interval '7 day' AND NOW() + interval '7 day' AND checkin_history.is_delete = 0  "
            + "ORDER BY checkin_history.room_name_id";

    private static final String SQL_SELECT_CHECKIN
            = "SELECT * FROM checkin_history WHERE contact_id = ? and status = 'จอง' and is_delete = 0";

    private static final String SQL_UPDATE_CHECKIN
            = "UPDATE checkin_history SET monk_id = :monk_id, check_in = :check_in, status= :status WHERE id = :id";

    private static final String SQL_UPDATE_CHECKOUT
            = "UPDATE checkin_history SET  check_out = :check_out, status= :status WHERE id = :id";

    private static final String SQL_UPDATE_DELETE
            = "UPDATE checkin_history SET  is_delete = 1 WHERE id = :id";

    private static final String SQL_SELECT_CHECKOUT
            = "SELECT * FROM checkin_history WHERE contact_id = ? and status = 'เช็คอิน' and is_delete = 0";

    private static final String SQL_COUNT_CHECKIN
            = "SELECT COUNT(contact_id) FROM checkin_history WHERE is_delete = 0 AND contact_id = ? AND temple_id = ? AND status != 'จอง'";

    private static final String SQL_SELECT_FIRST_COME
            = "SELECT * FROM checkin_history WHERE id = ? ";

    private static final String SQL_SELECT_MONK
            = "SELECT count(*) FROM checkin_history WHERE monk_id = ? and  check_out >= ?::date AND check_in   <= ?::date and status = 'เช็คอิน' and is_delete = 0";

    private final static String SQL_SELECT_CHECKIN_BY_CONTACT_ID
            = "SELECT * FROM checkin_history WHERE room_id = ? and is_delete = 0 ORDER BY id";

    @Override
    public CheckinHistory find(int id) {
        BeanPropertyRowMapper<CheckinHistory> bookRowMapper = BeanPropertyRowMapper.newInstance(CheckinHistory.class);
        CheckinHistory checkinHistory = getJdbcTemplate().queryForObject(SQL_SELECT, bookRowMapper, id);
        return checkinHistory;
    }

    @Override
    public Integer insert(CheckinHistory checkinHistory) {
        int key = -1;
        SqlParameterSource parameterSource = new BeanPropertySqlParameterSource(checkinHistory);
        key = getNamedParameterJdbcTemplate().update(SQL_INSERT, parameterSource);
        return key;
    }

    @Override
    public void update(CheckinHistory checkinHistory) {
        SqlParameterSource parameterSource = new BeanPropertySqlParameterSource(checkinHistory);
        getNamedParameterJdbcTemplate().update(SQL_UPDATE, parameterSource);
    }

    @Override
    public void delete(int id) {
        Map<String, Object> args = new HashMap<String, Object>();
        args.put("id", id);
        getNamedParameterJdbcTemplate().update(SQL_DELETE, args);
    }

    @Override
    public List<CheckinHistory> getCheckinHistoryBytemple(int temple_id) {
        BeanPropertyRowMapper<CheckinHistory> bookRowMapper = BeanPropertyRowMapper.newInstance(CheckinHistory.class);
        List<CheckinHistory> checkinHistorys = new ArrayList<CheckinHistory>();
        checkinHistorys = getJdbcTemplate().query(SQL_SELECT_BY_TEMPLE, new Object[]{temple_id}, bookRowMapper);
        return checkinHistorys;
    }

    @Override
    public List<OverviewData> getOverviewData(int temple_id, int room_id) {
        BeanPropertyRowMapper<OverviewData> bookRowMapper = BeanPropertyRowMapper.newInstance(OverviewData.class);
        List<OverviewData> overviewDatas = new ArrayList<OverviewData>();
        overviewDatas = getJdbcTemplate().query(SQL_SELECT_OVERALL_BUILDING, new Object[]{temple_id, room_id}, bookRowMapper);
        return overviewDatas;
    }

    @Override
    public List<OverviewData> getOverviewDataRoom(int temple_id, int room_name_id) {
        BeanPropertyRowMapper<OverviewData> bookRowMapper = BeanPropertyRowMapper.newInstance(OverviewData.class);
        List<OverviewData> overviewDatas = new ArrayList<OverviewData>();
        overviewDatas = getJdbcTemplate().query(SQL_SELECT_OVERALL_ROOM, new Object[]{temple_id, room_name_id}, bookRowMapper);
        return overviewDatas;
    }

    @Override
    public CheckinHistory getContactCheckIn(int contact_id) {
        CheckinHistory contact = null;
        BeanPropertyRowMapper<CheckinHistory> bookRowMapper = BeanPropertyRowMapper.newInstance(CheckinHistory.class);
        contact = getJdbcTemplate().queryForObject(SQL_SELECT_CHECKIN, bookRowMapper, contact_id);
        System.out.println(SQL_SELECT_CHECKIN);
        return contact;
    }

    @Override
    public void updateCheckIn(CheckinHistory checkinHistory) {
        SqlParameterSource parameterSource = new BeanPropertySqlParameterSource(checkinHistory);
        getNamedParameterJdbcTemplate().update(SQL_UPDATE_CHECKIN, parameterSource);
    }

    @Override
    public void updateCheckOut(CheckinHistory checkinHistory) {
        SqlParameterSource parameterSource = new BeanPropertySqlParameterSource(checkinHistory);
        getNamedParameterJdbcTemplate().update(SQL_UPDATE_CHECKOUT, parameterSource);
    }

    @Override
    public void updateDelete(int id) {
        Map<String, Object> args = new HashMap<String, Object>();
        args.put("id", id);
        getNamedParameterJdbcTemplate().update(SQL_UPDATE_DELETE, args);
    }

    @Override
    public CheckinHistory getContactCheckOut(int contact_id) {
        CheckinHistory contact = null;
        BeanPropertyRowMapper<CheckinHistory> bookRowMapper = BeanPropertyRowMapper.newInstance(CheckinHistory.class);
        contact = getJdbcTemplate().queryForObject(SQL_SELECT_CHECKOUT, bookRowMapper, contact_id);
        return contact;
    }

    @Override
    public boolean countCheckin(int contact_id, int temple_id) {
        boolean first_come = false;
        int count = getJdbcTemplate().queryForObject(SQL_COUNT_CHECKIN, new Object[]{contact_id, temple_id}, Integer.class);
        if (count <= 1) {
            first_come = true;
        }
        return first_come;
    }

    @Override
    public CheckinHistory getFirstCome(int id) {
        CheckinHistory checkin = null;
        BeanPropertyRowMapper<CheckinHistory> bookRowMapper = BeanPropertyRowMapper.newInstance(CheckinHistory.class);
        checkin = getJdbcTemplate().queryForObject(SQL_SELECT_FIRST_COME, bookRowMapper, id);
        return checkin;
    }

    @Override
    public int getCountMonkDate(int monk_id, String date_com) {
        int count = 0;
        System.out.println(monk_id + "   " + date_com);
        try {
            count = getJdbcTemplate().queryForObject(SQL_SELECT_MONK, new Object[]{monk_id, date_com, date_com}, Integer.class);
        } catch (Exception e) {
            System.out.println("Error =" + e.getMessage());
        }
        return count;
    }

    @Override
    public List<CheckinHistory> getRoomCheckIn(int contact_id) {
        BeanPropertyRowMapper<CheckinHistory> bookRowMapper = BeanPropertyRowMapper.newInstance(CheckinHistory.class);
        List<CheckinHistory> checkinHistorys = new ArrayList<CheckinHistory>();
        checkinHistorys = getJdbcTemplate().query(SQL_SELECT_CHECKIN_BY_CONTACT_ID, new Object[]{contact_id}, bookRowMapper);
        return checkinHistorys;
    }

}
