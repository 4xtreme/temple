/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycrm.dao.impl.jdbc;

import com.mycrm.dao.PositionDAO;
import com.mycrm.dao.StaffDAO;
import com.mycrm.dao.WithdrawDAO;
import com.mycrm.domain.Position;
import com.mycrm.domain.Staff;
import com.mycrm.domain.Withdraw;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcDaoSupport;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

/**
 *
 * @author pop
 */
public class PositionDAOImpl extends NamedParameterJdbcDaoSupport implements PositionDAO {

    private final static String SQL_INSERT
            = "INSERT INTO position  (id, position_type) VALUES (:id, :position_type)";

    private final static String SQL_SELECT_INSERT_ID
            = "SELECT id FROM position ORDER by id DESC LIMIT 1";
    
    private final static String SQL_SELECT_LIST
            = "SELECT * from position";
    
    @Override
    public Integer insert(Position position) {
        int key = -1;
        try {
            SqlParameterSource parameterSource = new BeanPropertySqlParameterSource(position);
            key = getNamedParameterJdbcTemplate().update(SQL_INSERT, parameterSource);
            key = getNamedParameterJdbcTemplate().getJdbcOperations().queryForObject(SQL_SELECT_INSERT_ID, Integer.class);
        } catch (Exception e) {
            System.out.println("Insert error: " + e.getMessage());
        }
        System.out.println("key ==> " + key);
        return key;
    }
    
    @Override
    public List<Position> getPositionList() {
        BeanPropertyRowMapper<Position> bookRowMapper = BeanPropertyRowMapper.newInstance(Position.class);
        List<Position> position = new ArrayList<Position>();
        position = getJdbcTemplate().query(SQL_SELECT_LIST, new Object[]{}, bookRowMapper);
        return position;
    }
    

}
