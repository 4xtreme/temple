/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycrm.dao.impl.jdbc;

import com.mycrm.dao.OldOrderWithdrawDAO;
import com.mycrm.domain.OldorderWithdrawlist;
import java.util.ArrayList;
import java.util.List;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcDaoSupport;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

/**
 *
 * @author pop
 */
public class OldOrderWithdrawDAOImpl extends NamedParameterJdbcDaoSupport implements OldOrderWithdrawDAO{
    
     private final static String SQL_INSERT
            = "INSERT INTO old_order_withdrawlist( old_withdraw_id, withdraw_id, index_number, broken_item, type, quantity, amount, sum_price, note, is_delete) "
            + "VALUES (:old_withdraw_id, :withdraw_id, :index_number, :broken_item, :type, :quantity, :amount, :sum_price, :note, :is_delete)";

    private final static String SQL_SELECT_LIST
            = "select * from old_order_withdrawlist where old_withdraw_id = ? ";
    
     @Override
    public Integer insert(OldorderWithdrawlist oldOrderWithdrawlist) {
        System.out.println("DAO" + oldOrderWithdrawlist.toString());
        int key = -1;
        try {
            SqlParameterSource parameterSource = new BeanPropertySqlParameterSource(oldOrderWithdrawlist);
            key = getNamedParameterJdbcTemplate().update(SQL_INSERT, parameterSource);
        } catch (Exception e) {
            System.out.println("Insert error: " + e.getMessage());
        }

        return key;
    }

    @Override
    public List<OldorderWithdrawlist> getOrderWithdrawList(int oldWithdraw_id) {
        BeanPropertyRowMapper<OldorderWithdrawlist> bookRowMapper = BeanPropertyRowMapper.newInstance(OldorderWithdrawlist.class);
        List<OldorderWithdrawlist> oldOrderWithdrawlist = new ArrayList<OldorderWithdrawlist>();
        oldOrderWithdrawlist = getJdbcTemplate().query(SQL_SELECT_LIST, bookRowMapper, oldWithdraw_id);
        System.out.println("Get Data Table is" + oldOrderWithdrawlist.toString());

        return oldOrderWithdrawlist;
    }

    
    
}
