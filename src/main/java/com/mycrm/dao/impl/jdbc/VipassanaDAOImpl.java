/*
 * Copy Right Pay Enterprise Co.,Ltd.
 */
package com.mycrm.dao.impl.jdbc;

import com.mycrm.dao.VipassanaDAO;
import com.mycrm.domain.Vipassana;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcDaoSupport;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;

/**
 *
 * @author pop
 */
public class VipassanaDAOImpl extends NamedParameterJdbcDaoSupport implements VipassanaDAO {

    private static final String SQL_INSERT
            = "INSERT INTO vipassana (firstname, nickname, lastname, personal_id, academic_standing, sect, address, sub_district, district, province, postal_code, image, temple_id, create_date, update_date, is_delete ) VALUES (:firstname, :nickname, :lastname, :personal_id, :academic_standing, :sect, :address, :sub_district, :district, :province, :postal_code, :image, :temple_id, NOW(), NOW(), false)";

    private static final String SQL_UPDATE
            = "UPDATE vipassana SET firstname = :firstname, nickname = :nickname, lastname = :lastname, personal_id = :personal_id, academic_standing = :academic_standing, sect = :sect, address = :address, sub_district = :sub_district, district = :district, province = :province, postal_code = :postal_code, image = :image, update_date = NOW() WHERE id = :id AND is_delete = false";

    private static final String SQL_SELECT
            = "SELECT * FROM vipassana WHERE id = ? AND is_delete = false";

    private static final String SQL_SELECT_ALL
            = "SELECT * FROM vipassana WHERE temple_id = ? AND is_delete = false  ORDER BY id ASC OFFSET ? LIMIT ?";

    private static final String SQL_DELETE
            = "UPDATE vipassana SET is_delete = true, update_date = NOW() WHERE id = :id";
    
    private final static String SQL_ROWS_ALL
            = "SELECT COUNT(*) FROM vipassana WHERE is_delete = false AND temple_id = ?" ;
    
    private final static String SQL_ROWS_SEARCH
            = "SELECT COUNT(*) FROM vipassana  WHERE firstname LIKE ? OR nickname LIKE ? OR lastname LIKE ? OR sect LIKE ? OR academic_standing LIKE ? AND temple_id = ? AND is_delete = false";

    private final static String SQL_SEARCH
            = "SELECT * FROM vipassana WHERE firstname LIKE ? OR nickname LIKE ? OR lastname LIKE ? OR sect LIKE ? OR academic_standing LIKE ? AND is_delete = false AND temple_id = ? ORDER BY id DESC OFFSET ? LIMIT ?";
    
    @Override
    public Integer insert(Vipassana vipassana) {
        int last_id = 0;
        GeneratedKeyHolder generatedKeyHolder = new GeneratedKeyHolder();
        SqlParameterSource parameterSource = new BeanPropertySqlParameterSource(vipassana);
        try {
            getNamedParameterJdbcTemplate().update(SQL_INSERT, parameterSource, generatedKeyHolder);
            Map<String, Object> newId = generatedKeyHolder.getKeys();
            last_id = (int) newId.get("id");
        } catch (Exception e) {
            System.out.println("Insert vipassana Error:" + e.getMessage());
        }
        return last_id;
    }

    @Override
    public void update(Vipassana vipassana) {
        SqlParameterSource parameterSource = new BeanPropertySqlParameterSource(vipassana);
        getNamedParameterJdbcTemplate().update(SQL_UPDATE, parameterSource);
    }

    @Override
    public Vipassana findById(int id) {
        Vipassana vipassana = new Vipassana();
        try {
            BeanPropertyRowMapper<Vipassana> bookRowMapper = BeanPropertyRowMapper.newInstance(Vipassana.class);
            vipassana = getJdbcTemplate().queryForObject(SQL_SELECT, bookRowMapper, id);
        } catch (Exception e) {
            System.out.println("Select vipassana Error:" + e.getMessage());
        }
        return vipassana;
    }

    @Override
    public List<Vipassana> findAll(int temple_id, String page, int per_page) {
        List<Vipassana> vipassanas = new ArrayList<>();
        try {
            BeanPropertyRowMapper<Vipassana> bookRowMapper = BeanPropertyRowMapper.newInstance(Vipassana.class);
            vipassanas = getJdbcTemplate().query(SQL_SELECT_ALL, new Object[]{temple_id,Integer.parseInt(page)*per_page,per_page}, bookRowMapper);
        } catch (Exception e) {
            System.out.println("Select All vipassana Error:" + e.getMessage());
        }
        return vipassanas;
    }

    @Override
    public void delete(int id) {
        Vipassana vipassana = new Vipassana();
        vipassana.setId(id);
        SqlParameterSource parameterSource = new BeanPropertySqlParameterSource(vipassana);
        getNamedParameterJdbcTemplate().update(SQL_DELETE, parameterSource);
    }
    
    @Override
    public int countRows(int temple_id) {
        int count = getJdbcTemplate().queryForObject(SQL_ROWS_ALL, new Object[] {temple_id}, Integer.class);
        return count;
    }

    @Override
    public List<Vipassana> search(int temple_id, String page, int per_page, String search) {
        BeanPropertyRowMapper<Vipassana> bookRowMapper = BeanPropertyRowMapper.newInstance(Vipassana.class);
        List<Vipassana> searchVipassana = new ArrayList<Vipassana>();
        searchVipassana = getJdbcTemplate().query(SQL_SEARCH, new Object[]{"%"+search+"%", "%"+search+"%", "%"+search+"%", "%"+search+"%", "%"+search+"%",temple_id,Integer.parseInt(page)*per_page,per_page},bookRowMapper);
        return searchVipassana;
    }

    @Override
    public int countSearchRows(int temple_id, String search) {
        int count = getJdbcTemplate().queryForObject(SQL_ROWS_SEARCH, new Object[] {"%"+search+"%", "%"+search+"%", "%"+search+"%", "%"+search+"%", "%"+search+"%", temple_id}, Integer.class);
        System.out.println("count : "+count);
        return count;
    }

}
