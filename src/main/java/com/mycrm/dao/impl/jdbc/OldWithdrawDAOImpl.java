/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycrm.dao.impl.jdbc;

import com.mycrm.dao.OldWithdrawDAO;
import com.mycrm.domain.OldWithdraw;
import java.util.ArrayList;
import java.util.List;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcDaoSupport;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

/**
 *
 * @author pop
 */
public class OldWithdrawDAOImpl extends NamedParameterJdbcDaoSupport implements OldWithdrawDAO {

    private final static String SQL_INSERT
            = "INSERT INTO old_withdraw  (id, withdraw_id, user_id, withdrawal_id, driver_id, amount, project_id, pay_type, pay_date, pay_date_count, store_id, machine_id, create_date, update_date, withdraw_file, withdrawal_note, inspector_note, approvers_note, total_price, status ) "
            + "VALUES (:id, :withdraw_id,  :user_id, :withdrawal_id, :driver_id, :amount, :project_id, :pay_type, :pay_date, :pay_date_count, :store_id, :machine_id, :create_date, :update_date, :withdraw_file, :withdrawal_note, :inspector_note, :approvers_note, :total_price, :status) ";

    private final static String SQL_SELECT_INSERT_ID
            = "SELECT id FROM old_withdraw ORDER by id DESC LIMIT 1";

    private final static String SQL_SELECT_LIST
            = "SELECT * FROM old_withdraw WHERE withdraw_id = ? limit ? offset?";

    private final static String SQL_SELECT
            = "select * from old_withdraw where id = ?";
    
     private final static String SQL_ROWS_ALL
            = "select count(*) from old_withdraw where withdraw_id = ?" ;

    @Override
    public Integer insert(OldWithdraw oldwithdraw) {
        System.out.println("DAO" + oldwithdraw.toString());
        int key = -1;
        try {
            SqlParameterSource parameterSource = new BeanPropertySqlParameterSource(oldwithdraw);
            key = getNamedParameterJdbcTemplate().update(SQL_INSERT, parameterSource);
            key = getNamedParameterJdbcTemplate().getJdbcOperations().queryForObject(SQL_SELECT_INSERT_ID, Integer.class);

        } catch (Exception e) {
            System.out.println("Insert error: " + e.getMessage());
        }
        System.out.println("key ==> " + key);

        return key;
    }

    @Override
    public List<OldWithdraw> getOldWithdrawList(int withdraw_id,String page,int per_page) {
        BeanPropertyRowMapper<OldWithdraw> bookRowMapper = BeanPropertyRowMapper.newInstance(OldWithdraw.class);
        List<OldWithdraw> oldwithdraw = new ArrayList<OldWithdraw>();
        oldwithdraw = getJdbcTemplate().query(SQL_SELECT_LIST, bookRowMapper, withdraw_id, Integer.parseInt(page)*per_page,per_page);
        System.out.println("Get Data Table is" + oldwithdraw.toString());

        return oldwithdraw;
    }

    @Override
    public OldWithdraw findOldWithdrawId(int oldwithdrawId) {
        System.out.println("DAO Find OldWithdrawId: " + oldwithdrawId);
        BeanPropertyRowMapper<OldWithdraw> bookRowMapper = BeanPropertyRowMapper.newInstance(OldWithdraw.class);
        OldWithdraw withdraw = getJdbcTemplate().queryForObject(SQL_SELECT, bookRowMapper, oldwithdrawId);
        return withdraw;
    }
    
     @Override
    public int countRows(int withdraw_id) {
        int count = getJdbcTemplate().queryForInt(SQL_ROWS_ALL, new Object[] {withdraw_id} );
        return count;
    }

}
