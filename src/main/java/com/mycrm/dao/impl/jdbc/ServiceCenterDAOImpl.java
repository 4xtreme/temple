/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycrm.dao.impl.jdbc;

import com.mycrm.dao.ServiceCenterDAO;
import com.mycrm.dao.StaffDAO;
import com.mycrm.dao.WithdrawDAO;
import com.mycrm.domain.ServiceCenter;
import com.mycrm.domain.Staff;
import com.mycrm.domain.Withdraw;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcDaoSupport;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

/**
 *
 * @author pop
 */
public class ServiceCenterDAOImpl extends NamedParameterJdbcDaoSupport implements ServiceCenterDAO {

    private final static String SQL_INSERT
            = "INSERT INTO servicecenter  (id, name, servicecenter_type, servicecenter_group , address, street, local, district, province, zipcode, status, contact, position, email, tel, chat, note, is_delete)"
            + "VALUES (:id, :name, :servicecenter_type, :servicecenter_group, :address, :street, :local, :district, :province, :zipcode, :status, :contact, :position, :email, :tel, :chat, :note, :is_delete)";

    private final static String SQL_SELECT_INSERT_ID
            = "SELECT id FROM servicecenter ORDER by id DESC LIMIT 1";
    
    private final static String SQL_SELECT_LIST
            = "select * from servicecenter where is_delete = false limit ? offset ?";
    
    private final static String SQL_SELECT 
            = "select * from servicecenter where id = ?";
    
    private final static String SQL_UPDATE
            = "update  servicecenter set  name = :name, servicecenter_type = :servicecenter_type, servicecenter_group = :servicecenter_group, address =  :address, street = :street, local = :local, district = :district, province = :province, zipcode = :zipcode, status = :status, contact = :contact, position = :position, email = :email, tel = :tel, chat = :chat, note = :note where id = :id";
    
    private final static String SQL_DELETE
            = "update servicecenter set is_delete = true  where id = :id";
    
    private final static String SQL_ROWS_ALL
            = "select count(*) from servicecenter where  is_delete = false" ;
    
    @Override
    public Integer insert(ServiceCenter servicecenter) {
        int key = -1;
        try {
            SqlParameterSource parameterSource = new BeanPropertySqlParameterSource(servicecenter);
            key = getNamedParameterJdbcTemplate().update(SQL_INSERT, parameterSource);
            key = getNamedParameterJdbcTemplate().getJdbcOperations().queryForObject(SQL_SELECT_INSERT_ID, Integer.class);
        } catch (Exception e) {
            System.out.println("Insert error: " + e.getMessage());
        }
        System.out.println("key ==> " + key);
        return key;
    }
    
    @Override
    public List<ServiceCenter> getServiceCenterList(String page,int per_page) {
        BeanPropertyRowMapper<ServiceCenter> bookRowMapper = BeanPropertyRowMapper.newInstance(ServiceCenter.class);
        List<ServiceCenter> servicecenter = new ArrayList<ServiceCenter>();
        servicecenter = getJdbcTemplate().query(SQL_SELECT_LIST, new Object[]{ Integer.parseInt(page)*per_page,per_page}, bookRowMapper);
        return servicecenter;
    }
    
    @Override
    public ServiceCenter findServiceCenter(int Id){
        BeanPropertyRowMapper<ServiceCenter> bookRowMapper = BeanPropertyRowMapper.newInstance(ServiceCenter.class);
        ServiceCenter servicecenter = getJdbcTemplate().queryForObject(SQL_SELECT, bookRowMapper, Id);
        return servicecenter;
    }
    
    @Override
    public void update(ServiceCenter servicecenter) {
        SqlParameterSource parameterSource = new BeanPropertySqlParameterSource(servicecenter);
        getNamedParameterJdbcTemplate().update(SQL_UPDATE, parameterSource);
    }
    
    @Override
    public void delete(Integer id) {
        Map<String, Object> args = new HashMap<String, Object>();
        args.put("id", id);
        getNamedParameterJdbcTemplate().update(SQL_DELETE, args);
    }
    
      @Override
    public int countRows() {
        int count = getJdbcTemplate().queryForObject(SQL_ROWS_ALL, new Object[] {}, Integer.class);
        return count;
    }
}
