package com.mycrm.dao.impl.jdbc;

import com.mycrm.dao.StaffDAO;
import com.mycrm.domain.Register;
import com.mycrm.domain.Staff;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcDaoSupport;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class StaffDAOImpl extends NamedParameterJdbcDaoSupport implements StaffDAO {

    private final static String SQL_INSERT
            = "INSERT INTO staff(firstname, lastname, email, phone1, password, role_id, temple_id) VALUES (:firstname, :lastname, :email, :phone1, :password, :role_id, :temple_id);";

    private final static String SQL_SELECT_INSERT_ID
            = "SELECT id FROM staff ORDER by id DESC LIMIT 1";

    private final static String SQL_SELECT_LIST
            = "select * from staff where is_delete = 0 AND temple_id = ? ORDER BY id offset ? limit ?";

    private final static String SQL_SELECT
            = "select * from staff where id = ?";

    private final static String SQL_UPDATE
            = "update  staff set   firstname = :firstname, lastname = :lastname, email = :email, phone1 = :phone1, password = :password, role_id = :role_id where id = :id";

    private final static String SQL_DELETE
            = "update staff set is_delete = 1 where id = :id";

    private final static String SQL_ROWS_ALL
            = "select count(*) from staff where  is_delete = 0 AND temple_id = ? ";

    private final static String SQL_SELECT_ROLE_ID
            = "SELECT role_id FROM staff  where id = ?";

    private final static String SQL_SELECT_EMAIL_LOGIN
            = "SELECT * FROM staff  WHERE email = ? AND staff.temple_id = ? ";

    private final static String SQL_SELECT_EMAIL
            = "select count(email) from staff where email = ? ";

//    private final static String SELECT_EMAIL_FORGOT
//            = "select password from staff where email = ?";
//    
    @Override
    public Integer insert(Staff staff) {
        int key = 0;
        //try {
        SqlParameterSource parameterSource = new BeanPropertySqlParameterSource(staff);
        //key = 
        getNamedParameterJdbcTemplate().update(SQL_INSERT, parameterSource);
        //key = 
        // getNamedParameterJdbcTemplate().getJdbcOperations().queryForObject(SQL_SELECT_INSERT_ID, Integer.class, parameterSource);
        //} catch (Exception e) {
        //    System.out.println("Insert error: " + e.getMessage());
        //}
        //System.out.println("key ==> " + key);
        System.out.println("----------------DAOp END INSERT staff-------------------");
        return key;
    }

    @Override
    public List<Staff> getStaffList(String page, int per_page, int temple_id) {
        BeanPropertyRowMapper<Staff> bookRowMapper = BeanPropertyRowMapper.newInstance(Staff.class);
        List<Staff> staff = new ArrayList<Staff>();
        staff = getJdbcTemplate().query(SQL_SELECT_LIST, new Object[]{temple_id, Integer.parseInt(page) * per_page, per_page}, bookRowMapper);
        System.out.println("--------------------------------------------" + staff.toString());
        return staff;
    }

    @Override
    public Staff findStaff(int staffId) {
        Staff staff = new Staff();
        try {
            BeanPropertyRowMapper<Staff> bookRowMapper = BeanPropertyRowMapper.newInstance(Staff.class);
            staff = getJdbcTemplate().queryForObject(SQL_SELECT, bookRowMapper, staffId);

        } catch (Exception e) {
            System.out.println("Get Staff Error:"+e.getMessage());
        }
        return staff;
    }

    @Override
    public void update(Staff staff) {
        System.out.println("DAO" + staff);
        SqlParameterSource parameterSource = new BeanPropertySqlParameterSource(staff);
        getNamedParameterJdbcTemplate().update(SQL_UPDATE, parameterSource);
    }

    @Override
    public void delete(Integer id) {
        Map<String, Object> args = new HashMap<String, Object>();
        args.put("id", id);
        getNamedParameterJdbcTemplate().update(SQL_DELETE, args);
    }

    @Override
    public int countRows(int temple_id) {
        int count = getJdbcTemplate().queryForObject(SQL_ROWS_ALL, new Object[]{temple_id}, Integer.class);
        return count;
    }

    @Override
    public int find_role(Integer id) {
        int count = getJdbcTemplate().queryForObject(SQL_SELECT_ROLE_ID, new Object[]{id}, Integer.class);
        return count;
    }

    @Override
    public Staff findStaffByEmail(String email, int temple_id) {
        BeanPropertyRowMapper<Staff> bookRowMapper = BeanPropertyRowMapper.newInstance(Staff.class);
        Staff staff = getJdbcTemplate().queryForObject(SQL_SELECT_EMAIL_LOGIN, bookRowMapper, email, temple_id);

        return staff;
    }
//---------------------------------------CHECK MAIL----------------------------------------------------------------

    @Override
    public int countEmail(String email) {
        int count = getJdbcTemplate().queryForObject(SQL_SELECT_EMAIL, new Object[]{email}, Integer.class);
        System.out.println("Value of Email staff : " + count);
        return count;
    }

    @Override
    public boolean isUserExists(String email) {
        String sql = "SELECT count(*) FROM staff WHERE email = ?";
        boolean result = false;

        int count = getJdbcTemplate().queryForObject(
                SQL_SELECT_EMAIL, new Object[]{email}, Integer.class);

        if (count > 0) {
            result = true;
        } else {
            result = false;
        }

        return result;
    }

    @Override
    public Staff emailfindUser(String email) {
        System.out.println("DAO Find by Email staff: " + email);
        BeanPropertyRowMapper<Staff> bookRowMapper = BeanPropertyRowMapper.newInstance(Staff.class);
        Staff staff = getJdbcTemplate().queryForObject(SQL_SELECT_EMAIL, bookRowMapper, email);
        System.out.println("Value email staff : " + staff);
        return staff;
    }

    @Override
    public boolean emailExits(String email) {
        boolean result = false;
        int count = getJdbcTemplate().queryForObject(SQL_SELECT_EMAIL, new Object[]{email}, Integer.class);
        System.out.println("Value of Email : " + count);

        if (count > 0) {
            result = true;
        } else {
            result = false;
        }

        return result;
    }

    @Override
    public List<Staff> findEmailInfo(String email) {
        BeanPropertyRowMapper<Staff> bookRowMapper = BeanPropertyRowMapper.newInstance(Staff.class);
        List<Staff> staff = new ArrayList<Staff>();
        staff = getJdbcTemplate().query(SQL_SELECT_EMAIL, bookRowMapper, email);
        System.out.println("DB : " + staff.toString());
        System.out.println("Type info from DB : " + staff.getClass());

        return staff;
    }
//    @Override
//    public Staff findemailInfo(String email) {
//        BeanPropertyRowMapper<Register> bookRowMapper = BeanPropertyRowMapper.newInstance(Register.class);
//        Staff staff = getJdbcTemplate().queryForObject(SELECT_EMAIL_FORGOT, bookRowMapper, email);
//        System.out.println("DAO Get register contact password : " + staff.getPassword());
//        return staff;
//    }
}
