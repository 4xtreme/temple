/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycrm.dao.impl.jdbc;

import com.mycrm.dao.ContactDAO;
import com.mycrm.domain.Contact;
import com.mycrm.domain.Monk;
import com.mycrm.domain.PeopleLiving;
import com.mycrm.domain.Register;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcDaoSupport;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

/**
 *
 * @author Lenovo
 */
public class ContactDAOImpl extends NamedParameterJdbcDaoSupport implements ContactDAO {

    private final static String SQL_SELECT_ID
            = "select * from contact where reg_id = ?";

    private final static String SQL_SELECT_ID1
            = "select * from contact where reg_id = ?";

    private final static String SQL_SELECT_MONK_ID
            = "select * from monk where is_delete = 0 AND monk_id = ?";

    private final static String SQL_SELECT
            = "select * from customer where is_delete = 0 AND user_id = ?";

    private final static String SQL_SELECT_LIST
            = "SELECT * FROM contact WHERE temple_id = ? ORDER BY reg_id offset ? limit ?";

    private final static String SQL_SELECT1
            = "select * from contact where is_delete = 0";

    private final static String SQL_SELECT_MONK
            = "select * from monk where temple_id = ? AND is_delete = 0 AND monk_type = 'พระ'";

    private final static String SQL_SELECT_MONK_LIST
            = "select * from monk where temple_id = ? AND is_delete = 0 ORDER BY monk_id offset ? limit ?";

    private final static String SQL_INSERT
            = "insert into contact(nametitle,firstname, lastname, personid, royal, nationality,gender, email, address, village, district, city, province, postalcode, telephone, password, repassword, create_date, update_date,temple_id,bd_birth,nickname, occupation)"
            + "VALUES (:nametitle,:firstname, :lastname, :personid, :royal, :nationality,:gender, :email, :address, :village, :district, :city, :province, :postalcode, :telephone, :password, :repassword, NOW(), NOW(), :temple_id,:bd_birth,:nickname, :occupation)";

    private final static String SQL_INSERT_MONK
            = "insert into monk (firstname, nickname, lastname, personid, position, sect, address, district, amphoe, province, postcode, temple_id, pic_monk, monk_type, is_deactivate)"
            + " values(:firstname, :nickname, :lastname, :personid, :position, :sect, :address, :district, :amphoe, :province, :postcode, :temple_id, :pic_monk, :monk_type, :is_deactivate)";

    private final static String SQL_INSERT_MONK_PIC
            = "insert into monk (pic_monk) values(:pic_monk)";

    private final static String SQL_UPDATE
            = "update contact set  nametitle=:nametitle,firstname=:firstname, lastname=:lastname, personid=:personid, royal=:royal, nationality=:nationality,gender=:gender, email=:email, address=:address, village=:village, district=:district, city=:city, province=:province, postalcode=:postalcode, telephone=:telephone, password=:password, repassword=:repassword, create_date=NOW(), update_date=NOW(), temple_id=:temple_id,bd_birth=:bd_birth, nickname =:nickname , status_idcard = false, gender_text = :gender_text, occupation = :occupation, status = :status, drug= :drug, health= :health, mental = :mental where reg_id = :reg_id";

    private final static String SQL_UPDATE1
            = "update contact set firstname =:firstname , lastname =:lastname, personid=:personid, royal = :royal, nationality = :nationality,gender = :gender, email = :email, address=:address, village = :village, district = :district, city = :city, province = :province,postalcode = :postalcode, telephone = :telephone, password = :password, repassword = :repassword, update_date=NOW(), temple_id = :temple_id,bd_birth=:bd_birth, nickname =:nickname , status_idcard = false , occupation = :occupation where reg_id  = :reg_id";

    private final static String SQL_UPDATE_MONK
            = "update monk set firstname=:firstname, lastname=:lastname, nickname=:nickname, personid=:personid, position=:position,sect=:sect,address=:address,district=:district,amphoe=:amphoe,province=:province,postcode=:postcode,update_date=NOW(),temple_id = :temple_id,pic_monk=:pic_monk, monk_type = :monk_type, is_deactivate = :is_deactivate where monk_id=:monk_id";

    private final static String SQL_DELETE
            = "update customer set  is_delete = 1  where customer_id = :customer_id ";

    private final static String SQL_DELETE1
            = "update contact set  is_delete = 1 where reg_id = :reg_id ";

    private final static String SQL_DELETE_MONK
            = "update monk set is_delete = 1 where monk_id = :monk_id";

    private final static String SELETE_EMAIL
            = "SELECT email FROM customer WHERE member_type = ?";

    private final static String SELETE_EMAIL_ALL
            = "SELECT email FROM customer ";

    private final static String SELECT_EMAIL_FORGOT
            = "select password from contact where email = ?";

    private final static String SQL_SELECT_CONTACT_BY_PERSONID
            = "SELECT * FROM contact where personid = ?";

    private final static String SQL_SELECT_CONTACTID
            = "select * from contact where reg_id = ? ";

    private final static String SQL_SELECT_EMAIL
            = "select count(email) from contact where email = ? ";
    private final static String SQL_SELECT_PERSONID
            = "SELECT COUNT(personid) from contact where personid = ?";

    private final static String SQL_SELECT_PERSONID_AND_ISDELETE
            = "SELECT is_delete from contact where personid = ?";

    private final static String SQL_SELECT_EMAIL_AND_ISDELETE
            = "SELECT is_delete from contact where email = ?";

    private final static String SQL_ROWS_ALL
            = "select count(*) from contact where temple_id = ?";

    private final static String SQL_ROWS_Monk_ALL
            = "select count(*) from monk where  is_delete = 0 and  temple_id = ?";

    private final static String SQL_SELECT_MONK_BY_TEMPLE_ID
            = "select * from monk where is_delete = 0 and  temple_id = ?";

    private final static String SQL_SELECT_EMAIL_LOGIN
            = "SELECT * FROM contact WHERE email = ? AND contact.temple_id = ? ";

    private final static String SQL_UPDATE_PROFILE
            = "update contact set nametitle=:nametitle,firstname = :firstname, lastname =:lastname, personid=:personid, royal =:royal,nationality=:nationality,gender=:gender, email=:email, address=:address, village=:village,district=:district, city=:city, province=:province,postalcode=:postalcode, telephone=:telephone,password=:password, repassword=:repassword, update_date=NOW(),bd_birth=:bd_birth , occupation = :occupation WHERE reg_id = :reg_id";

    private final static String SQL_SELECT_ALL_MONK
            = "SELECT * FROM monk WHERE temple_id = ? AND is_delete = 0";

    private final static String SQL_UPDATE_COUNT_COME
            = "UPDATE contact set count_come = :count_come WHERE reg_id = :id";

    private final static String SQL_UPDATE_FIRST_COME
            = "UPDATE contact set first_come = NOW() WHERE reg_id = :id";

    private final static String SQL_INSERT_NOW
            = "insert into contact(nametitle,firstname, lastname, personid, royal, nationality,gender, email, address, village, district, city, province, postalcode, telephone, password, repassword, create_date, update_date, temple_id, bd_birth, nickname, status_offline, gender_text, occupation, status, drug, health, mental)"
            + "VALUES (:nametitle,:firstname, :lastname, :personid, :royal, :nationality,:gender, :email, :address, :village, :district, :city, :province, :postalcode, :telephone, :password, :repassword, NOW(), NOW(), :temple_id, :bd_birth, :nickname, true, :gender_text, :occupation, :status, :drug, :health, :mental)";

    private final static String SQL_INSERT_IDCARD
            = "insert into contact(nametitle,firstname, lastname, personid, royal, nationality,gender, email, address, village, district, city, province, postalcode, telephone, password, repassword, create_date, update_date, temple_id, bd_birth, nickname, status_idcard, gender_text, occupation)"
            + "VALUES (:nametitle,:firstname, :lastname, :personid, :royal, :nationality,:gender, :email, :address, :village, :district, :city, :province, :postalcode, :telephone, :password, :repassword, NOW(), NOW(), :temple_id, :bd_birth, :nickname, true, :gender_text, :occupation)";

    private final static String SQL_SELECT_CARD
            = "SELECT * FROM contact WHERE temple_id = ? AND is_delete = 0 AND status_idcard = true";

    private final static String SQL_SELECT_BY_NAME
            = "SELECT * from contact WHERE   temple_id = ? AND firstname ILIKE ?";

    private final static String SQL_SELECT_BY_SURNAME
            = "SELECT * from contact WHERE   temple_id = ? AND lastname ILIKE ?";

    private final static String SQL_SELECT_BY_EMAIL
            = "SELECT * from contact WHERE   temple_id = ? AND email ILIKE ?";
    private final static String SQL_SELECT_BY_GENDER
            = "SELECT * from contact WHERE   temple_id = ? AND gender_text ILIKE ?";
    private final static String SQL_SELECT_BY_PHONE
            = "SELECT * from contact WHERE   temple_id = ? AND telephone ILIKE ?";

    private final static String SQL_SELECT_BY_CARD
            = "SELECT * from contact WHERE   temple_id = ? AND personid ILIKE ?";

    private final static String SQL_RE_OPEN_CONTACT
            = "UPDATE contact SET  is_delete = 0 WHERE reg_id = :reg_id";

    private final static String SQL_SELECT_LIVE_PEOPLE_BY_CONTACT_FIRSTNAME
            = "SELECT contact.reg_id as id, contact.firstname, contact.lastname, checkin_history.temple_id , room.name as builder_name , room_name.name as room_name, COUNT(*)\n"
            + "FROM contact\n"
            + "LEFT JOIN checkin_history ON contact.reg_id = checkin_history.contact_id\n"
            + "LEFT JOIN room on room.id = checkin_history.room_id\n"
            + "LEFT JOIN room_name on room_name.id = checkin_history.room_name_id\n"
            + "WHERE contact.firstname ILIKE ? AND contact.temple_id = ? AND contact.is_delete = 0 AND checkin_history.status = 'เช็คอิน' AND checkin_history.is_delete = 0\n"
            + "GROUP BY contact.reg_id, checkin_history.temple_id, room.id, room_name.id\n"
            + "HAVING COUNT(*) = 1;";

    private final static String SQL_SELECT_LIVE_PEOPLE_BY_CONTACT_PHONE
            = "SELECT contact.reg_id as id, contact.firstname, contact.lastname, checkin_history.status, room.name as builder_name , room_name.name as room_name, COUNT(*)\n"
            + "FROM contact\n"
            + "LEFT JOIN checkin_history ON contact.reg_id = checkin_history.contact_id\n"
            + "LEFT JOIN room on room.id = checkin_history.room_id\n"
            + "LEFT JOIN room_name on room_name.id = checkin_history.room_name_id\n"
            + "WHERE contact.telephone = ? AND contact.temple_id = ? AND contact.is_delete = 0 AND checkin_history.status = 'เช็คอิน' AND checkin_history.is_delete = 0\n"
            + "GROUP BY contact.reg_id, contact.lastname, checkin_history.status, room.id, room_name.id\n"
            + "HAVING COUNT(*) = 1;";

    private final static String SQL_SELECT_LIVE_PEOPLE_BY_CONTACT_CARD
            = "SELECT contact.reg_id as id, contact.firstname, contact.lastname, checkin_history.temple_id , room.name as builder_name , room_name.name as room_name , COUNT(*)\n"
            + "FROM contact\n"
            + "LEFT JOIN checkin_history ON contact.reg_id = checkin_history.contact_id\n"
            + "LEFT JOIN room on room.id = checkin_history.room_id\n"
            + "LEFT JOIN room_name on room_name.id = checkin_history.room_name_id\n"
            + "WHERE contact.personid = ? AND contact.temple_id = ? AND contact.is_delete = 0 AND checkin_history.status = 'เช็คอิน' AND checkin_history.is_delete = 0\n"
            + "GROUP BY contact.reg_id, checkin_history.temple_id, room.id, room_name.id\n"
            + "HAVING COUNT(*) = 1;";

    private final static String SQL_UPDATE_IS_DEACTIVATE
            = "UPDATE monk SET is_deactivate = :is_deactivate WHERE monk_id = :monk_id";

    private final static String SQL_COUNT_MONK
            = "SELECT COUNT(monk_id) FROM monk WHERE temple_id = ? AND is_delete = 0 AND is_deactivate = false AND monk_type = ?";

    private final static String SQL_COUNT_CONTACT
            = "SELECT COUNT(reg_id) FROM contact WHERE temple_id = ? AND is_delete = 0 AND gender_text = ?";
    
    private final static String SQL_DELETE_2
            = "UPDATE contact SET is_delete = 1 , reason = :reason where reg_id = :reg_id ";
    
    private final static String SQL_SEARCH_MONK
            = "SELECT * FROM monk WHERE firstname LIKE ? OR nickname LIKE ? OR lastname LIKE ? OR sect LIKE ? OR templename LIKE ? OR monk_type LIKE ? AND temple_id = ? AND is_delete = 0";
    
    @Override
    public boolean isUserExists(String email) {
        String sql = "SELECT count(*) FROM contact WHERE email = ?";
        boolean result = false;

        int count = getJdbcTemplate().queryForObject(
                SQL_SELECT_EMAIL, new Object[]{email}, Integer.class);

        if (count > 0) {
            result = true;
        } else {
            result = false;
        }

        return result;
    }

    @Override
    public Register emailfindUser(String email) {
        System.out.println("DAO Find by Email: " + email);
        BeanPropertyRowMapper<Register> bookRowMapper = BeanPropertyRowMapper.newInstance(Register.class);
        Register register = getJdbcTemplate().queryForObject(SQL_SELECT_EMAIL, bookRowMapper, email);
        System.out.println("Value email : " + register);
        return register;
    }

    @Override
    public boolean emailExits(String email) {
        boolean result = false;
        int count = getJdbcTemplate().queryForObject(SQL_SELECT_EMAIL, new Object[]{email}, Integer.class);
        System.out.println("Value of Email : " + count);

        if (count > 0) {
            result = true;
        } else {
            result = false;
        }

        return result;
    }

    @Override
    public int countEmail(String email) {
        //BeanPropertyRowMapper<Register> bookRowMapper = BeanPropertyRowMapper.newInstance(Register.class);
        int count = getJdbcTemplate().queryForObject(SQL_SELECT_EMAIL, new Object[]{email}, Integer.class);
        System.out.println("Value of Email : " + count);
        return count;
    }

    @Override
    public List<Register> findEmailInfo(String email) {
        BeanPropertyRowMapper<Register> bookRowMapper = BeanPropertyRowMapper.newInstance(Register.class);
        List<Register> register = new ArrayList<Register>();
        register = getJdbcTemplate().query(SQL_SELECT_EMAIL, bookRowMapper, email);
        System.out.println("DB : " + register.toString());
        System.out.println("Type info from DB : " + register.getClass());

        return register;
    }
//--------------------------------------------------END CHECK MAIL

    @Override
    public Contact findcontactInfo(int contactId) {
        BeanPropertyRowMapper<Contact> bookRowMapper = BeanPropertyRowMapper.newInstance(Contact.class);
        Contact contact = getJdbcTemplate().queryForObject(SQL_SELECT_ID, bookRowMapper, contactId);

        System.out.println("DAO Get contact: " + contact.getFirstname() + " " + contact.getLastname());
        return contact;
    }

    @Override
    public Register findregInfo(int reg_id) {
        BeanPropertyRowMapper<Register> bookRowMapper = BeanPropertyRowMapper.newInstance(Register.class);
        Register register = getJdbcTemplate().queryForObject(SQL_SELECT_ID1, bookRowMapper, reg_id);
        return register;
    }

    @Override
    public Monk findmonkInfo(int monk_id) {
        BeanPropertyRowMapper<Monk> bookRowMapper = BeanPropertyRowMapper.newInstance(Monk.class);
        Monk monk = getJdbcTemplate().queryForObject(SQL_SELECT_MONK_ID, bookRowMapper, monk_id);
        System.out.println("DAO get monk contact: " + monk.getFirstname() + " " + monk.getLastname());

        return monk;
    }

    @Override
    public Integer insertreg(Register register) {
        System.out.println("DAO" + register.toString());
        int key = 0;

        SqlParameterSource parameterSource = new BeanPropertySqlParameterSource(register);
        getNamedParameterJdbcTemplate().update(SQL_INSERT, parameterSource);

        System.out.println("----------------DAOp END INSERT-------------------");
        return key;
    }

    @Override
    public Integer insert(Monk monk) {
        System.out.println("DAO" + monk.toString());
        int key = -1;
        //try {
        SqlParameterSource parameterSource = new BeanPropertySqlParameterSource(monk);
        key = getNamedParameterJdbcTemplate().update(SQL_INSERT_MONK, parameterSource);
        //} catch (Exception e) {
        //   System.out.println("Insert error: " + e.getMessage());
        //}
        return key;
    }

    @Override
    public Integer insertPic(Monk monk) {
        System.out.println("DAO" + monk.toString());
        int key = -1;
        try {
            SqlParameterSource parameterSource = new BeanPropertySqlParameterSource(monk);
            key = getNamedParameterJdbcTemplate().update(SQL_INSERT_MONK_PIC, parameterSource);
        } catch (Exception e) {
            System.out.println("Insert error : " + e.getMessage());
        }
        return key;
    }

    @Override
    public List<Contact> getcontactList(int user_id) {
        BeanPropertyRowMapper<Contact> bookRowMapper = BeanPropertyRowMapper.newInstance(Contact.class);
        List<Contact> contact = new ArrayList<Contact>();
        contact = getJdbcTemplate().query(SQL_SELECT, bookRowMapper, user_id);
        System.out.println("Get Data Table is" + contact.toString());

        return contact;
    }

    @Override
    public List<Register> getregcontactList(int temple_id) {
        BeanPropertyRowMapper<Register> bookRowMapper = BeanPropertyRowMapper.newInstance(Register.class);
        List<Register> register = new ArrayList<Register>();
        register = getJdbcTemplate().query(SQL_SELECT1, bookRowMapper);
        System.out.println("Get Data table is " + register.toString());

        return register;
    }

    @Override
    public List<Register> getRegList(int temple_id, String page, int per_page) {
        System.out.println("DAO -- " + "temple = " + temple_id + " page " + page + " per = " + per_page);
        BeanPropertyRowMapper<Register> bookRowMapper = BeanPropertyRowMapper.newInstance(Register.class);
        List<Register> register = new ArrayList<Register>();
        register = getJdbcTemplate().query(SQL_SELECT_LIST, new Object[]{temple_id, Integer.parseInt(page) * per_page, per_page}, bookRowMapper);
        //System.out.println("Show : " + register.toString());
        return register;
    }

    @Override
    public List<Monk> getmonkList(int monk_id, int temple_id) {
        BeanPropertyRowMapper<Monk> bookRowMapper = BeanPropertyRowMapper.newInstance(Monk.class);
        List<Monk> monk = new ArrayList<Monk>();
        monk = getJdbcTemplate().query(SQL_SELECT_MONK, new Object[]{temple_id}, bookRowMapper);
        System.out.println("Get Data table is " + monk.toString());
        return monk;
    }

    @Override
    public List<Monk> getMonkPage(int temple_id, String page, int per_page) {
        BeanPropertyRowMapper<Monk> bookRowMapper = BeanPropertyRowMapper.newInstance(Monk.class);
        List<Monk> monk = new ArrayList<Monk>();
        monk = getJdbcTemplate().query(SQL_SELECT_MONK_LIST, new Object[]{temple_id, Integer.parseInt(page) * per_page, per_page}, bookRowMapper);
        System.out.println("Get Data table is " + monk.toString());
        return monk;
    }

    @Override
    public void update(Contact contact) {
        SqlParameterSource parameterSource = new BeanPropertySqlParameterSource(contact);
        getNamedParameterJdbcTemplate().update(SQL_UPDATE1, parameterSource);
        System.out.println("After update: " + contact.toString());
        System.out.println("----------DAO1 END update---------");

    }

    @Override
    public void updateReg(Register register) {
        SqlParameterSource parameterSource = new BeanPropertySqlParameterSource(register);
        getNamedParameterJdbcTemplate().update(SQL_UPDATE, parameterSource);
        //System.out.println("After update : " + register.toString());
        System.out.println("----------------DAO END UPDATE REG-------------------");
        //System.out.println(SQL_UPDATE+register.toString());
    }

    @Override
    public void updateMonk(Monk monk) {
        SqlParameterSource parameterSource = new BeanPropertySqlParameterSource(monk);
        getNamedParameterJdbcTemplate().update(SQL_UPDATE_MONK, parameterSource);
        System.out.println("After update : " + monk.toString());
        System.out.println("----------------DAO END UPDATE-------------------");
    }

    @Override
    public void deleteContact(int contactId) {
        Contact c = new Contact();
        c.setCustomer_id(contactId);

        SqlParameterSource parameterSource = new BeanPropertySqlParameterSource(c);
        getNamedParameterJdbcTemplate().update(SQL_DELETE, parameterSource);
        System.out.println("----------DAO Delete---------");
    }

    @Override
    public void deleteContactReg(int reg_id) {
        Register c = new Register();
        c.setReg_id(reg_id);

        SqlParameterSource parameterSource = new BeanPropertySqlParameterSource(c);
        getNamedParameterJdbcTemplate().update(SQL_DELETE1, parameterSource);
        System.out.println("----------DAO Delete---------");
    }

    @Override
    public void deleteMonk(int monk_id) {
        Monk m = new Monk();
        m.setMonk_id(monk_id);
        SqlParameterSource parameterSource = new BeanPropertySqlParameterSource(m);
        getNamedParameterJdbcTemplate().update(SQL_DELETE_MONK, parameterSource);
        System.out.println("----------DAO Delete---------");
    }

    @Override
    public List<Contact> getemailList(String memtype) {
        BeanPropertyRowMapper<Contact> bookRowMapper = BeanPropertyRowMapper.newInstance(Contact.class);
        List<Contact> contact = new ArrayList<Contact>();
        if (memtype.equals("all")) {
            contact = getJdbcTemplate().query(SELETE_EMAIL_ALL, bookRowMapper);
        } else {
            contact = getJdbcTemplate().query(SELETE_EMAIL, bookRowMapper, memtype);
        }

        System.out.println("Get Data Table is" + contact.toString());

        return contact;
    }

    @Override
    public Register getContactByPersonID(String person_id) {
        BeanPropertyRowMapper<Register> bookRowMapper = BeanPropertyRowMapper.newInstance(Register.class);
        Register contact = getJdbcTemplate().queryForObject(SQL_SELECT_CONTACT_BY_PERSONID, bookRowMapper, person_id);
        return contact;
    }

    @Override
    public Register findemailInfo(String email) {
        BeanPropertyRowMapper<Register> bookRowMapper = BeanPropertyRowMapper.newInstance(Register.class);
        Register register = getJdbcTemplate().queryForObject(SELECT_EMAIL_FORGOT, bookRowMapper, email);
        System.out.println("DAO Get register contact password : " + register.getPassword());
        return register;
    }

    @Override
    public Register findcontactByID(int contactId) {
        BeanPropertyRowMapper<Register> bookRowMapper = BeanPropertyRowMapper.newInstance(Register.class);
        Register contact = getJdbcTemplate().queryForObject(SQL_SELECT_CONTACTID, bookRowMapper, contactId);
        System.out.println("DAO Get contact: " + contact.getFirstname() + " " + contact.getLastname());
        return contact;
    }

    public int countRows(int temple_id) {
        int count = getJdbcTemplate().queryForObject(SQL_ROWS_ALL, new Object[]{temple_id}, Integer.class);
        return count;
    }

    @Override
    public int countRowsMonk(int temple_id) {
        int count = getJdbcTemplate().queryForObject(SQL_ROWS_Monk_ALL, new Object[]{temple_id}, Integer.class);
        return count;
    }

    @Override
    public List<Monk> getMonkByTemple(int temple_id) {
        BeanPropertyRowMapper<Monk> bookRowMapper = BeanPropertyRowMapper.newInstance(Monk.class);
        List<Monk> monk = new ArrayList<Monk>();
        monk = getJdbcTemplate().query(SQL_SELECT_MONK_BY_TEMPLE_ID, new Object[]{temple_id}, bookRowMapper);
        return monk;
    }

    @Override
    public Register findContactByEmail(String email, int temple_id) {
        BeanPropertyRowMapper<Register> bookRowMapper = BeanPropertyRowMapper.newInstance(Register.class);
        Register contact = getJdbcTemplate().queryForObject(SQL_SELECT_EMAIL_LOGIN, bookRowMapper, email, temple_id);
        return contact;
    }

    @Override
    public List<Register> findContact(String typeSearch, String message, int temple_id) {
        BeanPropertyRowMapper<Register> bookRowMapper = BeanPropertyRowMapper.newInstance(Register.class);
        List<Register> register = new ArrayList<Register>();
        if (typeSearch.equals("firstname")) {
            register = getJdbcTemplate().query(SQL_SELECT_BY_NAME, new Object[]{temple_id, "%" + message + "%"}, bookRowMapper);
        } else if (typeSearch.equals("lastname")) {
            register = getJdbcTemplate().query(SQL_SELECT_BY_SURNAME, new Object[]{temple_id, "%" + message + "%"}, bookRowMapper);
        } else if (typeSearch.equals("gender_text")) {
            register = getJdbcTemplate().query(SQL_SELECT_BY_GENDER, new Object[]{temple_id, "%" + message + "%"}, bookRowMapper);
        }
        //List<Register> register = new ArrayList<>();

        System.out.println("find from DAO : " + register);

        return register;
    }

    @Override
    public void updateContactProfile(Register register) {
        SqlParameterSource parameterSource = new BeanPropertySqlParameterSource(register);
        getNamedParameterJdbcTemplate().update(SQL_UPDATE_PROFILE, parameterSource);
        System.out.println("After update : " + register.toString());
        System.out.println("----------------DAOprofile END UPDATE-------------------");
        System.out.println(SQL_UPDATE_PROFILE + register.toString());
    }
//-------------------------------START CHECK PERSONID

    @Override
    public boolean isUserExistsPID(String personid) {
        String sql = "SELECT count(*) FROM contact WHERE email = ?";
        boolean result = false;

        int count = getJdbcTemplate().queryForObject(
                SQL_SELECT_PERSONID, new Object[]{personid}, Integer.class);

        if (count > 0) {
            result = true;
        } else {
            result = false;
        }

        return result;
    }

    @Override
    public Register personidfindUser(String personid) {
        System.out.println("DAO Find by Personid : " + personid);
        BeanPropertyRowMapper<Register> bookRowMapper = BeanPropertyRowMapper.newInstance(Register.class);
        Register register = getJdbcTemplate().queryForObject(SQL_SELECT_PERSONID, bookRowMapper, personid);
        System.out.println("Value personid : " + register);
        return register;
    }

    @Override
    public boolean personidExits(String personid) {
        boolean result = false;
        int count = getJdbcTemplate().queryForObject(SQL_SELECT_PERSONID, new Object[]{personid}, Integer.class);
        System.out.println("Value of Personid : " + count);

        if (count > 0) {
            result = true;
        } else {
            result = false;
        }

        return result;
    }

    @Override
    public List<Register> findPersonidInfo(String personid) {
        BeanPropertyRowMapper<Register> bookRowMapper = BeanPropertyRowMapper.newInstance(Register.class);
        List<Register> register = new ArrayList<Register>();
        register = getJdbcTemplate().query(SQL_SELECT_PERSONID, bookRowMapper, personid);
        System.out.println("DB : " + register.toString());
        System.out.println("Type info from DB : " + register.getClass());

        return register;
    }

    @Override
    public int countPersonID(String personid) {
        int count = getJdbcTemplate().queryForObject(SQL_SELECT_PERSONID, new Object[]{personid}, Integer.class);
        System.out.println("Value of PersonID : " + count);
        return count;
    }

    @Override
    public int findPersonidIsdelete(String personid) {
        int count = getJdbcTemplate().queryForObject(SQL_SELECT_PERSONID_AND_ISDELETE, new Object[]{personid}, Integer.class);
        System.out.println("Isdelete Count : " + count);
        return count;
    }

    @Override
    public int findEmailIsdelete(String email) {
        int count = getJdbcTemplate().queryForObject(SQL_SELECT_EMAIL_AND_ISDELETE, new Object[]{email}, Integer.class);
        System.out.println("email delete Count : " + count);
        return count;
    }

    @Override
    public List<Monk> getAllMonk(int temple_id) {
        BeanPropertyRowMapper<Monk> bookRowMapper = BeanPropertyRowMapper.newInstance(Monk.class);
        List<Monk> monk = getJdbcTemplate().query(SQL_SELECT_ALL_MONK, bookRowMapper, temple_id);
        return monk;
    }

    @Override
    public void updateCountCome(int count_come, int reg_id) {
        Map<String, Object> args = new HashMap<String, Object>();
        args.put("count_come", count_come);
        args.put("id", reg_id);
        getNamedParameterJdbcTemplate().update(SQL_UPDATE_COUNT_COME, args);

    }

    @Override
    public void updateFirstCome(int reg_id) {
        Map<String, Object> args = new HashMap<String, Object>();
        args.put("id", reg_id);
        getNamedParameterJdbcTemplate().update(SQL_UPDATE_FIRST_COME, args);
    }

    @Override
    public void insertContactNow(Register register) {
        SqlParameterSource parameterSource = new BeanPropertySqlParameterSource(register);
        getNamedParameterJdbcTemplate().update(SQL_INSERT_NOW, parameterSource);
    }

    @Override
    public void insertContactIDCard(Register register) {
        SqlParameterSource parameterSource = new BeanPropertySqlParameterSource(register);
        getNamedParameterJdbcTemplate().update(SQL_INSERT_IDCARD, parameterSource);
    }

    @Override
    public Register getRegCard(int temple_id) {
        BeanPropertyRowMapper<Register> bookRowMapper = BeanPropertyRowMapper.newInstance(Register.class);
        Register register = getJdbcTemplate().queryForObject(SQL_SELECT_CARD, bookRowMapper, temple_id);
        return register;
    }

    @Override
    public void reOpenReg(int reg_id) {
        Register c = new Register();
        c.setReg_id(reg_id);

        SqlParameterSource parameterSource = new BeanPropertySqlParameterSource(c);
        getNamedParameterJdbcTemplate().update(SQL_RE_OPEN_CONTACT, parameterSource);
        System.out.println("----------DAO Delete---------");
    }

    @Override
    public List<PeopleLiving> getPeopleLiving(String typeSearch, String message, int temple_id) {
        BeanPropertyRowMapper<PeopleLiving> bookRowMapper = BeanPropertyRowMapper.newInstance(PeopleLiving.class);
        List<PeopleLiving> peopleLivings = new ArrayList<>();
        System.out.println("DAO -- type search: " + typeSearch);
        if (typeSearch.equals("firstname")) {
            peopleLivings = getJdbcTemplate().query(SQL_SELECT_LIVE_PEOPLE_BY_CONTACT_FIRSTNAME, new Object[]{"%" + message + "%", temple_id}, bookRowMapper);
        } else if (typeSearch.equals("phone")) {
            peopleLivings = getJdbcTemplate().query(SQL_SELECT_LIVE_PEOPLE_BY_CONTACT_PHONE, new Object[]{message, temple_id}, bookRowMapper);
        } else if (typeSearch.equals("card")) {
            peopleLivings = getJdbcTemplate().query(SQL_SELECT_LIVE_PEOPLE_BY_CONTACT_CARD, new Object[]{message, temple_id}, bookRowMapper);
        }

        return peopleLivings;
    }

    @Override
    public void deactivateMonk(int monk_id) {
        Monk monk = new Monk();
        monk.setIs_deactivate(true);
        monk.setMonk_id(monk_id);
        SqlParameterSource parameterSource = new BeanPropertySqlParameterSource(monk);
        getNamedParameterJdbcTemplate().update(SQL_UPDATE_IS_DEACTIVATE, parameterSource);
    }

    @Override
    public void activateMonk(int monk_id) {
        Monk monk = new Monk();
        monk.setIs_deactivate(false);
        monk.setMonk_id(monk_id);
        SqlParameterSource parameterSource = new BeanPropertySqlParameterSource(monk);
        getNamedParameterJdbcTemplate().update(SQL_UPDATE_IS_DEACTIVATE, parameterSource);
    }

    @Override
    public int countMonk(int temple_id, String monk_type) {

        int count = getJdbcTemplate().queryForObject(SQL_COUNT_MONK, new Object[]{temple_id, monk_type}, Integer.class);

        return count;
    }

    @Override
    public int countContact(int temple_id, String gender_text) {

        int count = getJdbcTemplate().queryForObject(SQL_COUNT_CONTACT, new Object[]{temple_id, gender_text}, Integer.class);

        return count;
    }
    
    @Override
    public void deleteContactRegWithReason(int reg_id, String reason) {
        Register c = new Register();
        c.setReg_id(reg_id);
        c.setReason(reason);
        SqlParameterSource parameterSource = new BeanPropertySqlParameterSource(c);
        getNamedParameterJdbcTemplate().update(SQL_DELETE_2, parameterSource);
        System.out.println("----------DAO Delete---------");
    }

    @Override
    public List<Monk> searchMonk(String search, int temple_id) {
        BeanPropertyRowMapper<Monk> bookRowMapper = BeanPropertyRowMapper.newInstance(Monk.class);
        List<Monk> searchmonk = new ArrayList<>();
        searchmonk = getJdbcTemplate().query(SQL_SEARCH_MONK, new Object[]{"%"+search+"%", "%"+search+"%", "%"+search+"%", "%"+search+"%", "%"+search+"%", "%"+search+"%", temple_id}, bookRowMapper);
        return searchmonk;
    }

}
