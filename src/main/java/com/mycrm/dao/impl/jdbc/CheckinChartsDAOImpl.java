/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycrm.dao.impl.jdbc;

import com.mycrm.dao.CheckinChartsDAO;
import com.mycrm.domain.CheckinCharts;
import com.mycrm.domain.ContactReport;
import com.mycrm.domain.MonkReport;
import com.mycrm.domain.VipassanaData;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcDaoSupport;

/**
 *
 * @author SOMON
 */
public class CheckinChartsDAOImpl extends NamedParameterJdbcDaoSupport implements CheckinChartsDAO {

    private final String SQL_SELECT = "SELECT count(contact_id) as value,monk_id as name, check_in FROM checkin_history WHERE temple_id = ? AND check_in BETWEEN '?' AND '?' GROUP by monk_id,check_in";
    private final String SQL_SELECT_2
            = "SELECT count(checkin_history.id) as count, checkin_history.monk_id,checkin_history.check_in, CONCAT(monk.firstname,\" \",monk.lastname) as monk_name FROM checkin_history \n"
            + "JOIN monk on checkin_history.monk_id = monk.monk_id \n"
            + "where checkin_history.check_in >= ? AND checkin_history.check_in <= '?' AND checkin_history.temple_id = ? \n"
            + "GROUP BY checkin_history.monk_id,checkin_history.check_in";

    private final String SQL_COUNT_MONK
            = "SELECT count(checkin_history.id) as count, checkin_history.monk_id,checkin_history.check_in, CONCAT(monk.firstname,\" \",monk.lastname) as monk_name FROM `checkin_history` LEFT JOIN monk on checkin_history.monk_id = monk.monk_id where checkin_history.check_in BETWEEN CURRENT_DATE AND CURRENT_DATE+7 AND checkin_history.temple_id = 1 GROUP BY checkin_history.monk_id,checkin_history.check_in";

    private final String SQL_SELECT_REPORT
            = "SELECT CONCAT(monk.firstname,' ',monk.lastname) as monkName, CONCAT(contact.nametitle,' ',contact.firstname,' ', contact.lastname) as contactName, checkin_history.check_in as examDate FROM checkin_history\n"
            + "JOIN monk on checkin_history.monk_id = monk.monk_id\n"
            + "JOIN contact on checkin_history.contact_id = contact.reg_id \n"
            + "WHERE checkin_history.is_delete = 0 AND checkin_history.status = 'เช็คอิน' AND checkin_history.monk_id = ? AND checkin_history.check_in::varchar >= ? AND checkin_history.check_in::varchar <= ?";

    private final String SQL_SELECT_REPORT_ALL
            = " SELECT vipassana.id as vipassanaId, CONCAT(contact.nametitle,' ',contact.firstname,' ', contact.lastname) as contactName, room_name.name as roomName, checkin_history.check_out as checkOut FROM checkin_history\n"
            + "JOIN vipassana on checkin_history.monk_id = vipassana.id\n"
            + "JOIN contact on checkin_history.contact_id = contact.reg_id\n"
            + "JOIN room_name on checkin_history.room_name_id = room_name.id\n"
            + "WHERE checkin_history.is_delete = 0 AND checkin_history.status = 'เช็คอิน' AND checkin_history.temple_id = ? AND  ?  BETWEEN checkin_history.check_in AND checkin_history.check_out AND contact.gender = ? AND contact.gender_text != 'ต่างชาติชาย' AND contact.gender_text != 'ต่างชาติหญิง'\n"
            + "ORDER BY vipassanaId";

    private final String SQL_SELECT_REPORT_FOREIGN
            = "SELECT vipassana.id as vipassanaId, CONCAT(contact.nametitle,' ',contact.firstname,' ', contact.lastname) as contactName, room_name.name as roomName, checkin_history.check_out as checkOut FROM checkin_history\n"
            + "            JOIN vipassana on checkin_history.monk_id = vipassana.id\n"
            + "            JOIN contact on checkin_history.contact_id = contact.reg_id\n"
            + "            JOIN room_name on checkin_history.room_name_id = room_name.id\n"
            + "            WHERE checkin_history.is_delete = 0 AND checkin_history.status = 'เช็คอิน' AND checkin_history.temple_id = ? AND  ?  BETWEEN checkin_history.check_in AND checkin_history.check_out AND contact.gender_text = 'ต่างชาติชาย' OR contact.gender_text = 'ต่างชาติหญิง'\n"
            + "            ORDER BY vipassanaId";

    private static final String SQL_SELECT_CONTACT_REPORT
            = "SELECT CONCAT(contact.nametitle,'  ',contact.firstname,'  ',contact.lastname) AS contact_name, contact.bd_birth AS age, contact.address, contact.village, contact.district, contact.city, contact.province, contact.postalcode,contact.first_come, contact.count_come FROM checkin_history JOIN contact on contact.reg_id = checkin_history.contact_id WHERE checkin_history.is_delete = 0 AND checkin_history.check_in::varchar >= ? AND checkin_history.check_in::varchar <= ? AND checkin_history.temple_id = ? AND checkin_history.status = 'เช็คอิน' GROUP BY checkin_history.contact_id, contact.reg_id";

    private static final String SQL_COUNT_VIPASSANA
            = "SELECT vipassana.id as vipassanaId, COUNT(vipassana.id) AS count, vipassana.firstname AS name \n"
            + "FROM checkin_history \n"
            + "JOIN vipassana on checkin_history.monk_id = vipassana.id\n"
            + "JOIN contact on checkin_history.contact_id = contact.reg_id\n"
            + "WHERE checkin_history.is_delete = 0 AND checkin_history.status = 'เช็คอิน' AND checkin_history.temple_id = ? AND ?  BETWEEN checkin_history.check_in AND checkin_history.check_out AND contact.gender = ? AND contact.gender_text != 'ต่างชาติชาย' AND contact.gender_text != 'ต่างชาติหญิง' \n"
            + "GROUP BY vipassanaId\n"
            + "ORDER BY vipassanaId";

    private static final String SQL_COUNT_VIPASSANA_FOREIGN
            = "SELECT vipassana.id as vipassanaId, COUNT(vipassana.id) AS count, vipassana.firstname AS name \n"
            + "FROM checkin_history \n"
            + "JOIN vipassana on checkin_history.monk_id = vipassana.id\n"
            + "JOIN contact on checkin_history.contact_id = contact.reg_id\n"
            + "WHERE checkin_history.is_delete = 0 AND checkin_history.status = 'เช็คอิน' AND checkin_history.temple_id = ? AND ?  BETWEEN checkin_history.check_in AND checkin_history.check_out AND contact.gender_text = 'ต่างชาติชาย' OR contact.gender_text = 'ต่างชาติหญิง' \n"
            + "GROUP BY vipassanaId\n"
            + "ORDER BY vipassanaId";

    @Override

    public List<CheckinCharts> chartsList(int temple_id, String day) {
        //System.out.println("time" + startdate + "++ -----------" + enddate);
        BeanPropertyRowMapper<CheckinCharts> bookRowMapper = BeanPropertyRowMapper.newInstance(CheckinCharts.class);
        List<CheckinCharts> checkinCharts = new ArrayList<CheckinCharts>();
        checkinCharts = getJdbcTemplate().query("SELECT count(checkin_history.id) as value, checkin_history.monk_id as name,checkin_history.check_in, CONCAT(monk.firstname,\" \",monk.lastname) as monk_name FROM checkin_history \n"
                + "JOIN monk on checkin_history.monk_id = monk.monk_id \n"
                + "where checkin_history.check_in >= '" + day + "' AND checkin_history.check_in <= '" + day + "' AND checkin_history.temple_id = '" + temple_id + "' AND checkin_history.is_delete = 0 \n"
                + "GROUP BY checkin_history.monk_id,checkin_history.check_in", bookRowMapper);
        System.out.println("Data : " + checkinCharts.toString());

        return checkinCharts;
    }

    @Override
    public List<CheckinCharts> charts(int temple_id, String day) {
        //System.out.println("time" + startdate + "++ -----------" + enddate);
        List<CheckinCharts> checkinCharts = new ArrayList<CheckinCharts>();
        try {
            BeanPropertyRowMapper<CheckinCharts> bookRowMapper = BeanPropertyRowMapper.newInstance(CheckinCharts.class);

            checkinCharts = getJdbcTemplate().query("SELECT count(checkin_history.id) as value, checkin_history.monk_id as name,checkin_history.check_in, CONCAT(monk.firstname,' ',monk.lastname) as monk_name FROM checkin_history \n"
                    + "JOIN monk on checkin_history.monk_id = monk.monk_id \n"
                    + "where checkin_history.check_in >= '" + day + "' AND checkin_history.check_in <= '" + day + "' AND checkin_history.temple_id = '" + temple_id + "' AND checkin_history.is_delete = 0 \n"
                    + "GROUP BY checkin_history.monk_id,checkin_history.check_in, monk.monk_id", bookRowMapper);
            System.out.println("Chart:" + checkinCharts.toString());
        } catch (Exception e) {
            System.out.println("Error:" + e.getMessage());
        }

        return checkinCharts;
    }

    @Override
    public List<MonkReport> getMonkReport(int monk_id, Date start) {
        List<MonkReport> report = new ArrayList<MonkReport>();
        BeanPropertyRowMapper<MonkReport> bookRowMapper = BeanPropertyRowMapper.newInstance(MonkReport.class);
        report = getJdbcTemplate().query(SQL_SELECT_REPORT, new Object[]{monk_id, start}, bookRowMapper);

        return report;
    }

    @Override
    public List<MonkReport> getAllMonkReport(int temple_id, Date start, String contact_type) {

        List<MonkReport> report = new ArrayList<>();
        BeanPropertyRowMapper<MonkReport> bookRowMapper = BeanPropertyRowMapper.newInstance(MonkReport.class);
        if (contact_type.equals("ต่างชาติ")) {
            report = getJdbcTemplate().query(SQL_SELECT_REPORT_FOREIGN, new Object[]{temple_id, start}, bookRowMapper);

        } else {
            report = getJdbcTemplate().query(SQL_SELECT_REPORT_ALL, new Object[]{temple_id, start, contact_type}, bookRowMapper);
        }

        return report;
    }

    @Override
    public List<ContactReport> getAllContactReport(int temple_id, String start, String end) {
        List<ContactReport> report = new ArrayList<ContactReport>();
        BeanPropertyRowMapper<ContactReport> bookRowMapper = BeanPropertyRowMapper.newInstance(ContactReport.class);
        report = getJdbcTemplate().query(SQL_SELECT_CONTACT_REPORT, new Object[]{start, end, temple_id}, bookRowMapper);

        return report;
    }

    @Override
    public List<VipassanaData> countAllVipassanaReport(int temple_id, Date start, String contact_type) {
        List<VipassanaData> report = new ArrayList<>();
        BeanPropertyRowMapper<VipassanaData> bookRowMapper = BeanPropertyRowMapper.newInstance(VipassanaData.class);
        if (contact_type.equals("ต่างชาติ")) {
            report = getJdbcTemplate().query(SQL_COUNT_VIPASSANA_FOREIGN, new Object[]{temple_id, start}, bookRowMapper);
        } else {
            report = getJdbcTemplate().query(SQL_COUNT_VIPASSANA, new Object[]{temple_id, start, contact_type}, bookRowMapper);
        }
         

        return report;
    }
}
