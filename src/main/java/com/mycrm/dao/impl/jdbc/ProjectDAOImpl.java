/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycrm.dao.impl.jdbc;

import com.mycrm.dao.ProjectDAO;
import com.mycrm.domain.Project;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcDaoSupport;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

/**
 *
 * @author pop
 */
public class ProjectDAOImpl extends NamedParameterJdbcDaoSupport implements ProjectDAO{
    
    private final static String SQL_SELECT
            = "SELECT * FROM project where id = ?";
    
    private final static String SQL_SELECT_ALL 
            = "SELECT * FROM project";
    
    private final static String SQL_UPDATE 
            = "UPDATE project SET name = :project_name";
    
    private final static String SQL_INSERT
            = "INSERT INTO project (project_name) VALUES (:project_name)";
    
    private final static String SQL_DELETE
            = "DELETE FROM project WHERE id = ?";
    
    
     @Override
    public Project findProject(int project_id) {
        BeanPropertyRowMapper<Project> bookRowMapper = BeanPropertyRowMapper.newInstance(Project.class);
        Project project = getJdbcTemplate().queryForObject(SQL_SELECT, bookRowMapper, project_id);
        return project;
    }
    
    @Override
    public List<Project> getProjectList() {
        BeanPropertyRowMapper<Project> bookRowMapper = BeanPropertyRowMapper.newInstance(Project.class);
        List<Project> project = new ArrayList<Project>();
        project = getJdbcTemplate().query(SQL_SELECT_ALL,  bookRowMapper);
        return project;
    }
    
    @Override
    public Integer insert(Project project) {
        int key = -1;
        try {
            SqlParameterSource parameterSource = new BeanPropertySqlParameterSource(project);
            key = getNamedParameterJdbcTemplate().update(SQL_INSERT, parameterSource);
        } catch (Exception e) {
            System.out.println("Insert error: " + e.getMessage());
        }
        System.out.println("key ==> " + key);
        return key;
    }
    
    @Override
    public void update(Project project) {
            System.out.println("DAO" + project);
        SqlParameterSource parameterSource = new BeanPropertySqlParameterSource(project);
        getNamedParameterJdbcTemplate().update(SQL_UPDATE, parameterSource);
    }
    
    @Override
    public void delete(int project_id) {
        Map<String, Object> args = new HashMap<String, Object>();
        args.put("id", project_id);
        getNamedParameterJdbcTemplate().update(SQL_DELETE, args);
    }
    
}
