package com.mycrm.dao.impl.jdbc;

import com.mycrm.dao.EventsDAO;
import com.mycrm.domain.EventInfo;
import com.mycrm.domain.Events;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcDaoSupport;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author SOMON
 */
public class EventsDAOImpl extends NamedParameterJdbcDaoSupport implements EventsDAO {

    private static final String SQL_INSERT
            = "INSERT INTO events (monk_id, event_name, description, event_start, event_end, create_date, temple_id, income) "
            + "values(:monk_id, :event_name, :description, :event_start, :event_end, NOW(), :temple_id, :income)";

    private static final String SQL_SELECT
            = "SELECT *\n"
            + "FROM events\n"
            + "INNER JOIN monk on events.monk_id = monk.monk_id\n"
            + "WHERE events.is_delete = 0";

    private static final String SQL_SELECT_EDIT
            = "SELECT * FROM events WHERE event_id = ?";

    private static final String SQL_UPDATE
            = "UPDATE events SET  event_name = :event_name, description = :description, event_start = :event_start, event_end = :event_end, income = :income, update_date = NOW() WHERE event_id = :event_id";

    private static final String SQL_DELETE
            = "UPDATE events SET is_delete = 1 WHERE event_id = :event_id";

    private final static String SQL_ROWS_ALL
            = "SELECT COUNT(*) from events where temple_id = ? AND  is_delete = 0";

    private final static String SQL_SELECT_LIST
            = "SELECT * FROM events WHERE temple_id = ? AND is_delete = 0 ORDER BY event_id OFFSET ? LIMIT ?";

    private final static String SQL_INSERT_EVENT_INFO
            = "INSERT INTO event_info (event_id, monk_id, create_date, update_date, is_delete) VALUES (:event_id, :monk_id, NOW(), NOW(), false)";

    private final static String SQL_SELECT_EVENT_INFO
            = "SELECT * FROM event_info WHERE event_id = ? AND is_delete = false";

    private final static String SQL_DELETE_EVENT_INFO
            = "UPDATE event_info SET is_delete = true, update_date = NOW() WHERE monk_id = :monk_id AND event_id = :event_id";

    private final static String SQL_COUNT_EVENT_INFO
            = "SELECT COUNT(*) FROM event_info WHERE event_id = ? AND monk_id = ? AND is_delete = false";

    @Override
    public Integer insert(Events events) {
        System.out.println("DAO : " + events.toString());
        int last_id = 0;
        GeneratedKeyHolder generatedKeyHolder = new GeneratedKeyHolder();
        try {
            SqlParameterSource parameterSource = new BeanPropertySqlParameterSource(events);
            getNamedParameterJdbcTemplate().update(SQL_INSERT, parameterSource, generatedKeyHolder);
            Map<String, Object> newId = generatedKeyHolder.getKeys();
            last_id = (int) newId.get("event_id");
        } catch (Exception e) {
            System.out.println("Insert error: " + e.getMessage());
        }
        return last_id;
    }

    @Override
    public List<Events> geteventsList(int event_id) {
        BeanPropertyRowMapper<Events> bookRowMapper = BeanPropertyRowMapper.newInstance(Events.class);
        List<Events> eventslist = new ArrayList<Events>();
        eventslist = getJdbcTemplate().query(SQL_SELECT, bookRowMapper);
        System.out.println("Get Data table is " + eventslist.toString());
        return eventslist;
    }

    @Override
    public Events findById(int event_id) {
        BeanPropertyRowMapper<Events> bookRowMapper = BeanPropertyRowMapper.newInstance(Events.class);
        Events events = getJdbcTemplate().queryForObject(SQL_SELECT_EDIT, bookRowMapper, event_id);
        System.out.println("DAO get event id : " + events.getEvent_id() + "event name : " + events.getEvent_name());
        return events;
    }

    @Override
    public void update(Events events) {
        SqlParameterSource parameterSource = new BeanPropertySqlParameterSource(events);
        getNamedParameterJdbcTemplate().update(SQL_UPDATE, parameterSource);
        System.out.println("After update : " + events.toString());
        System.out.println("----------------DAO END UPDATE-------------------");

    }

    @Override
    public void delete(int event_id) {
        Events event = new Events();
        event.setEvent_id(event_id);
        SqlParameterSource parameterSource = new BeanPropertySqlParameterSource(event);
        getNamedParameterJdbcTemplate().update(SQL_DELETE, parameterSource);
        System.out.println("----------DAO Delete---------");
    }

    @Override
    public List<Events> geteventsPage(int temple_id, String page, int per_page) {
        BeanPropertyRowMapper<Events> bookRowMapper = BeanPropertyRowMapper.newInstance(Events.class);
        List<Events> events = new ArrayList<Events>();
        events = getJdbcTemplate().query(SQL_SELECT_LIST, new Object[]{temple_id, Integer.parseInt(page) * per_page, per_page}, bookRowMapper);
        return events;
    }

    @Override
    public int countRows(int temple_id) {
        int count = getJdbcTemplate().queryForObject(SQL_ROWS_ALL, new Object[]{temple_id}, Integer.class);
        return count;
    }

    @Override
    public Integer insertEventInfo(EventInfo eventInfo) {
        int last_id = 0;
        GeneratedKeyHolder generatedKeyHolder = new GeneratedKeyHolder();
        try {
            int count = getJdbcTemplate().queryForObject(SQL_COUNT_EVENT_INFO, new Object[]{eventInfo.getEvent_id(), eventInfo.getMonk_id()}, Integer.class);
            if (count == 0) {
                SqlParameterSource parameterSource = new BeanPropertySqlParameterSource(eventInfo);
                getNamedParameterJdbcTemplate().update(SQL_INSERT_EVENT_INFO, parameterSource, generatedKeyHolder);
                Map<String, Object> newId = generatedKeyHolder.getKeys();
                last_id = (int) newId.get("id");
            }

        } catch (Exception e) {
            System.out.println("Insert error: " + e.getMessage());
        }
        return last_id;
    }

    @Override
    public List<EventInfo> findEventInfo(int event_id) {
        BeanPropertyRowMapper<EventInfo> bookRowMapper = BeanPropertyRowMapper.newInstance(EventInfo.class);
        List<EventInfo> eventInfos = new ArrayList<>();
        eventInfos = getJdbcTemplate().query(SQL_SELECT_EVENT_INFO, new Object[]{event_id}, bookRowMapper);
        return eventInfos;
    }

    @Override
    public void deleteEventInfo(int event_id, int monk_id) {
        EventInfo eventInfo = new EventInfo();
        eventInfo.setEvent_id(event_id);
        eventInfo.setMonk_id(monk_id);
        int count = getJdbcTemplate().queryForObject(SQL_COUNT_EVENT_INFO, new Object[]{event_id, monk_id}, Integer.class);
        if (count != 0) {
            SqlParameterSource parameterSource = new BeanPropertySqlParameterSource(eventInfo);
            getNamedParameterJdbcTemplate().update(SQL_DELETE_EVENT_INFO, parameterSource);
            System.out.println("----------DAO Delete---------");
        }

    }

}
