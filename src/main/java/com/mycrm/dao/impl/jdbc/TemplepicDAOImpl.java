/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycrm.dao.impl.jdbc;

import com.mycrm.domain.Templepic;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcDaoSupport;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import com.mycrm.dao.TemplepicDAO;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author she my sunshine
 */
public class TemplepicDAOImpl extends NamedParameterJdbcDaoSupport implements TemplepicDAO {

    private final static String SQL_INSERT
            = "INSERT INTO templepic  (filename, file_type ,filedata,temple_id,description)"
            + "VALUES (:filename, :file_type, :filedata, :temple_id, :description)";
    
    private final static String SQL_SELECT 
            = "select * from templepic where id = ?";

    private final static String SQL_ROWS_ALL
            = "select count(*) from templepic where  t_delete = 0 AND temple_id = ?";
      
    private final static String SQL_SELECT_LIST
            = "select * from templepic where t_delete = 0 AND temple_id = ? ORDER BY id offset ? limit ?";
      
    private final static String SQL_DELETE
            = "update templepic set t_delete = 1  where id = :id";
      
    
    
    @Override
    public Integer insert(Templepic templepic) {
        int key = -1;
        try {
            SqlParameterSource parameterSource = new BeanPropertySqlParameterSource(templepic);
            key = getNamedParameterJdbcTemplate().update(SQL_INSERT, parameterSource);

        } catch (DataAccessException e) {
            System.out.println("Insert error: " + e.getMessage());
        }
        System.out.println("key ==> " + key);
        System.out.println("key ==> " + templepic);
        System.out.println(SQL_INSERT);
        return key;
    }

    @Override
    public Templepic findTemplepic(int templepicId) {
        BeanPropertyRowMapper<Templepic> bookRowMapper = BeanPropertyRowMapper.newInstance(Templepic.class);
        Templepic templepic = getJdbcTemplate().queryForObject(SQL_SELECT, bookRowMapper, templepicId);
        System.out.println(SQL_SELECT);
        return templepic;
        
        
    }

    @Override
    public List<Templepic> getTemplepicList(String page,int per_page ,int temple_id) {
        BeanPropertyRowMapper<Templepic> bookRowMapper = BeanPropertyRowMapper.newInstance(Templepic.class);
        List<Templepic> templepic = new ArrayList<Templepic>();
        templepic = getJdbcTemplate().query(SQL_SELECT_LIST, new Object[]{temple_id,Integer.parseInt(page)*per_page,per_page}, bookRowMapper);
        System.out.println("--------------------------------------------"+templepic.toString());
        System.out.println(SQL_SELECT_LIST);
        return templepic;
    }

    @Override
    public void delete(Integer id) {
        Map<String, Object> args = new HashMap<String, Object>();
        args.put("id", id);
        getNamedParameterJdbcTemplate().update(SQL_DELETE, args);
         System.out.println(SQL_DELETE);
    }

    @Override
    public int countRows(int temple_id) {
        int count = getJdbcTemplate().queryForObject(SQL_ROWS_ALL, new Object[] {temple_id}, Integer.class);
         System.out.println(SQL_ROWS_ALL);
        return count;
       
    }
    


}
