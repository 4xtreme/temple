/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycrm.dao;

import com.mycrm.domain.ServiceCenter;
import com.mycrm.domain.Staff;
import com.mycrm.domain.Withdraw;
import java.util.List;

/**
 *
 * @author pop
 */
public interface ServiceCenterDAO {
    public Integer insert(ServiceCenter servicecenter);
    
    public List<ServiceCenter> getServiceCenterList(String page,int per_page);
    
    public ServiceCenter findServiceCenter(int Id);
    
    public void update(ServiceCenter servicecenter);
    
    public void delete(Integer id);
    
    public int countRows();
}
