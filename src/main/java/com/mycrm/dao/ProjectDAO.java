/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycrm.dao;

import com.mycrm.domain.Project;
import java.util.List;

/**
 *
 * @author pop
 */
public interface ProjectDAO {

    public Project findProject(int project_id);

    public List<Project> getProjectList();

    public Integer insert(Project project);

    public void update(Project project);

    public void delete(int project_id);

}
