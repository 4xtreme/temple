/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycrm.dao;

import com.mycrm.domain.OrderWithdrawlist;
import java.util.List;

/**
 *
 * @author pop
 */
public interface OrderWithdrawDAO {

    public Integer insert(OrderWithdrawlist orderWithdrawlist);

    public List<OrderWithdrawlist> getOrderWithdrawList(int withdraw_id);

    public void update(OrderWithdrawlist orderWithdraw);

}
