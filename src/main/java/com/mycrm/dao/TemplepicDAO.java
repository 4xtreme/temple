/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycrm.dao;


import com.mycrm.domain.Templepic;
import java.util.List;

/**
 *
 * @author pop
 */
public interface TemplepicDAO {

    
    public Integer insert(Templepic templepic );
    public Templepic findTemplepic(int templepicId);
    public List<Templepic> getTemplepicList(String page,int per_page,int temple_id);
    public int countRows(int temple_id);
    public void delete(Integer id);
}
