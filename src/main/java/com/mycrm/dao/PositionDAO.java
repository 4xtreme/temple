/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycrm.dao;

import com.mycrm.domain.Position;
import com.mycrm.domain.Staff;
import com.mycrm.domain.Withdraw;
import java.util.List;

/**
 *
 * @author pop
 */
public interface PositionDAO {
    public Integer insert(Position position);    
    public List<Position> getPositionList();
    
}
