/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycrm.dao;

import com.mycrm.domain.Temple;
import java.util.List;

/**
 *
 * @author pop
 */
public interface TempleDAO {
    
    public Integer insert(Temple temple);
    
    public List<Temple> getTempleList(String page,int per_page);
    
    public Temple findTemple(int templeId);
    
    public void update(Temple temple);
    
    public void delete(Integer id);
    
    public int countRows();
    
    public List<Temple> getALL();

    public List<Temple> getTempleForMarker();
    
    public List<Temple> getALLTest();
    
    public List<Temple> getProvince();
    
    public List<Temple> searchTemple(String search);
    
    public List<Temple> searchProvinceTemple(String search);
   
}
