/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycrm.dao;

import com.mycrm.domain.CheckinCharts;
import com.mycrm.domain.ContactReport;
import com.mycrm.domain.MonkReport;
import com.mycrm.domain.VipassanaData;
import java.util.Date;
import java.util.List;

/**
 *
 * @author SOMON
 */
public interface CheckinChartsDAO {

    public List<CheckinCharts> chartsList(int temple_id, String val);

    public List<CheckinCharts> charts(int temple_id, String day);

    public List<MonkReport> getMonkReport(int monk_id, Date start);

    public List<MonkReport> getAllMonkReport(int temple_id, Date start, String contact_type);

    public List<ContactReport> getAllContactReport(int temple_id, String start, String end);

    public List<VipassanaData> countAllVipassanaReport(int temple_id, Date start, String contact_type);
}
