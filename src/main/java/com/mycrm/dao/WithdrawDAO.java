/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycrm.dao;

import com.mycrm.domain.Room;
import com.mycrm.domain.Withdraw;
import java.util.List;

/**
 *
 * @author pop
 */
public interface WithdrawDAO {
    public Integer insert(Withdraw withdraw);
    
    public List<Withdraw> getWithdrawList(String page,int per_page);
    
    public Withdraw findWithdrawId(int withdrawId);
    
    public void update(Withdraw withdraw);
    
    public int countRows();
    
}
