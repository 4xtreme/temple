/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycrm.dao;
import com.mycrm.domain.Role;
import com.mycrm.domain.RoleItem;
import java.util.List;

/**
 *
 * @author pop
 */
public interface RoleDAO {
    public Integer insert(Role role);
    public Integer insertRoleItem(RoleItem roleitem);    
    public void insertReport(Integer role_id);
    public List<Role> getRole();
    public Boolean pageView(String page,Integer role_id);
    public Boolean pageCreate(String page,Integer role_id);
    public Boolean pageEdit(String page,Integer role_id);
    public Boolean pageDelete(String page,Integer role_id);
    public List<RoleItem> getRoleITem(int id);
    public void updateRoleItem(RoleItem roleitem);
    public void delete(Integer id);
    public int checkName(String name);
    public int checkNameReport(Integer id);
    public RoleItem getRoleItemByPage(String pageName, int roleId);
    
}
