/*
 * Copy Right Pay Enterprise Co.,Ltd.
 */
package com.mycrm.dao;

import com.mycrm.domain.Vipassana;
import java.util.List;

/**
 *
 * @author pop
 */
public interface VipassanaDAO {
    
    public Integer insert (Vipassana vipassana);
    
    public void update (Vipassana vipassana);
    
    public Vipassana findById (int id);
    
    public List<Vipassana> findAll (int temple_id, String page, int per_page);
    
    public void delete (int id);
    
    public int countRows(int temple_id);
    
    public int countSearchRows(int temple_id, String search);
    
    public List<Vipassana> search(int temple_id, String page, int per_page,String search);
    
}
