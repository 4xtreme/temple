/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycrm.dao;

import com.mycrm.domain.Charts;
import java.util.List;

/**
 *
 * @author she my sunshine
 */
public interface ChartsDAO {

    public List<Charts> chartsList(String startdate,String enddate,String temple_id);
   
    
}
