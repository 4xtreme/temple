/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycrm.dao;

import com.mycrm.domain.Machine;
import java.util.List;

/**
 *
 * @author pop
 */
public interface MachineDAO {
    public Integer insert(Machine machine);
    
    public Machine searchData(String search);
    
    public Machine findMachineId(int machineId);
    
    public List<Machine> getMachineList(String page,int per_page);
    
    public void update(Machine machine);
    
    public void delete(Integer id);
    
    public int countRows();
}
