
package com.mycrm.dao;

import com.mycrm.domain.CheckinHistory;
import com.mycrm.domain.OverviewData;
import java.util.List;


public interface BookingDAO {

    public CheckinHistory find(int id);

    public List<OverviewData> getOverviewData(int temple_id, int room_id);

    public List<OverviewData> getOverviewDataRoom(int temple_id, int room_name_id);

    public Integer insert(CheckinHistory checkinHistory);

    public void update(CheckinHistory checkinHistory);

    public void delete(int id);

    public List<CheckinHistory> getCheckinHistoryBytemple(int temple_id);

    public CheckinHistory getContactCheckIn(int contact_id);

    public void updateCheckIn(CheckinHistory checkinHistory);

    public void updateCheckOut(CheckinHistory checkinHistory);

    public void updateDelete(int id);

    public CheckinHistory getContactCheckOut(int contact_id);
    
    public boolean countCheckin(int contact_id, int temple_id);
    
    public CheckinHistory getFirstCome(int id);
    
    public int getCountMonkDate(int monk_id,String date_com);
    
    public List<CheckinHistory> getRoomCheckIn(int contact_id);
}
