/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycrm.dao;

import com.mycrm.domain.Articles;
import java.util.List;

/**
 *
 * @author SOMON
 */
public interface ArticlesDAO {
    public Integer insert(Articles articles);
    public List<Articles> getArticlesList(int temple_id,String page,int per_page);
    public Articles findArticleInfo(int article_id);
    public void update(Articles articles);
    public void delete(int article_id);
    public List<Articles> getArticlePage(String page,int per_page);
    public int countRows(int temple_id);
    public int countRowsAll();
    public void updateNopic(Articles articles);
    public List<Articles> searchArticle(String search,int temple_id);
}
