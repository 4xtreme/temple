/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycrm.dao;

import com.mycrm.domain.Register;
import com.mycrm.domain.Staff;
import java.util.List;

/**
 *
 * @author pop
 */
public interface StaffDAO {
    public Integer insert(Staff staff);
    
    public List<Staff> getStaffList(String page,int per_page,int temple_id);
    
    public Staff findStaff(int staffId);
    
    public void update(Staff staff);
    
    public void delete(Integer id);
    
    public int countRows(int temple_id);
    
    public int find_role(Integer id);
    
    public Staff findStaffByEmail(String email,int temple_id) ;
    //-----------------------------------CHECKIN
    public int countEmail(String email);
    public List<Staff> findEmailInfo(String email);
   
    public boolean emailExits(String email);
    public boolean isUserExists(String email);
    public Staff emailfindUser(String email);
    
     //public Staff findemailInfo(String email);
}
