/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycrm.dao;

import com.mycrm.domain.EventInfo;
import com.mycrm.domain.Events;
import java.util.List;

/**
 *
 * @author SOMON
 */
public interface EventsDAO {

    public Integer insert(Events events);

    public List<Events> geteventsList(int event_id);

    public Events findById(int event_id);

    public void update(Events events);

    public void delete(int event_id);

    public List<Events> geteventsPage(int temple_id, String page, int per_page);

    public int countRows(int temple_id);

    public Integer insertEventInfo(EventInfo eventInfo);

    public List<EventInfo> findEventInfo(int event_id);

    public void deleteEventInfo(int event_id, int monk_id);
}
