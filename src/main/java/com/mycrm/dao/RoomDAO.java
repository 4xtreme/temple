/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycrm.dao;

import com.mycrm.domain.Room;
import com.mycrm.domain.RoomName;
import java.util.List;

/**
 *
 * @author pop
 */
public interface RoomDAO {

    public Integer insert(Room room);

    public List<Room> getRoomList(int temple_id, String page, int per_page);

    public int countRows(int temple_id);

    public void delete(Integer id);

    public Room find(int id);

    public void update(Room room);

    public List<Room> getRoomSearch(int id_temple, String page, int per_page, String sex);

    public int countRowSearch(int id_temple, String page, int per_page, String sex);

    public Integer insertRoomName(RoomName roomname);

    public List<RoomName> getAllRoomName(String page, int per_page);

    public int countRoomNameRows();

    public List<RoomName> getRoomName(int room_id);

    public int countRoomName(int room_id);

    public int sumRoomName(int room_id);

    public void deleteRoom(Integer id);

    public List<RoomName> getRoomNameSearch(String page, int per_page);

    public int countRowRoomNameSearch(String page, int per_page);

    public RoomName getRoomNameByID(int id);

    public void updateRoomName(RoomName room);

    public void updateRoomReserve();

    public List<Room> getRoomByTemple(int temple_id);

    public List<RoomName> getRoomNameByTemple(int temple_id);

    public List<RoomName> getRoomNameByRoom(int room_id);

}
