<%-- 
    Document   : add_vipassana
    Created on : Sep 3, 2019, 2:23:13 PM
    Author     : pop
--%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html;charset=UTF-8" %>
<jsp:include page="../header.jsp" />
<c:if test="${roleadmin == 'admin'}">
    <jsp:include page="../navigation.jsp" />
</c:if>
<c:if test="${roleadmin == 'user'}">
    <jsp:include page="../navigation_top.jsp" />
</c:if>
<style>
    label {
        margin-bottom: 5px;
        margin-top: 10px;
    }

    .error-msg {
        color: red;
    }

</style>
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header" id="title_head">เพิ่มพระวิปัสสนาจารย์</h1>
            </div>
        </div>
        <div class="row">
            <form:form id="addvipassana" modelAttribute="vipassana" action="addvipassana" method="POST"  enctype="multipart/form-data" >
                <fieldset>


                    <c:if test="${roleadmin == 'admin'}">
                        <div class="col-lg-5 col-lg-offset-1" style="margin-bottom: 10px;">
                        </c:if>
                        <c:if test="${roleadmin == 'user'}">
                            <div class="col-lg-4 col-lg-offset-2" style="margin-bottom: 10px;">
                            </c:if>

                            <label for="fistname">
                                ชื่อปัจจุบัน:
                            </label>
                            <form:errors path="firstname" class="error-msg"/>
                            <form:input type="text" class="form-control" path="firstname" id="firstname"/>
                            <label for="nickname">
                                ฉายา:
                            </label>
                            <form:errors path="nickname" class="error-msg"/>
                            <form:input type="text" class="form-control" path="nickname" id="nickname"/>
                            <label for="lastname">
                                นามสกุล:
                            </label>
                            <form:errors path="lastname" class="error-msg"/>
                            <form:input type="text" class="form-control" path="lastname" id="lastname"/>
                            <label for="personal_id">
                                หมายเลขบัตรประชาชน:
                            </label>
                            <form:errors path="personal_id" class="error-msg"/>
                            <form:input type="text" class="form-control" maxlength="13" path="personal_id" id="personal_id"/>
                            <label for="academic_standing">
                                วิทยฐานะ:
                            </label>
                            <form:errors path="academic_standing" class="error-msg"/>
                            <form:input type="text" class="form-control" path="academic_standing" id="academic_standing"/>
                            <label for="sect">
                                สังกัดนิกาย:
                            </label>
                            <form:errors path="sect" class="error-msg"/>
                            <form:input type="text" class="form-control" path="sect" id="sect"/>
                        </div>


                        <c:if test="${roleadmin == 'admin'}">
                            <div class="col-lg-5">
                            </c:if>
                            <c:if test="${roleadmin == 'user'}">
                                <div class="col-lg-4">
                                </c:if>

                                <label for="address">
                                    ที่อยู่:
                                </label>
                                <form:errors path="address" class="error-msg"/>
                                <form:input type="text" class="form-control" path="address"/>
                                <label for="sub_district">
                                    ตำบล:
                                </label>
                                <form:errors path="sub_district" class="error-msg"/>
                                <form:input type="text" class="form-control" path="sub_district"/>
                                <label for="district">
                                    อำเภอ: 
                                </label>
                                <form:errors path="district" class="error-msg"/>
                                <form:input type="text" class="form-control" path="district"/>
                                <label for="province">
                                    จังหวัด:
                                </label>
                                <form:errors path="province" class="error-msg"/>
                                <form:input type="text" class="form-control" path="province"/>
                                <label for="postal_code">
                                    รหัสไปรษณีย์:
                                </label>
                                <form:errors path="postal_code" class="error-msg"/>
                                <form:input type="text" class="form-control" maxlength="5" path="postal_code"/>
                                <label for="image">
                                    รูปภาพ:
                                </label>
                                <input type="file"  name="picture" id="image" />
                            </div>
                            <div class="col-lg-12" style="text-align: center; margin-top: 15px;">
                                <button class="btn btn-primary" type="submit" >Save</button>
                            </div>
                            </fieldset>

                        </form:form>
                    </div>
                </div>
        </div>

        <script>
            function checkData() {
                var firstname = $("#firstname").val();
                var nickname = $("#nickname").val();
                var lastname = $("#lastname").val();
                var person = $("#personal_id").val();
                var academic_standing = $("#academic_standing").val();
                var sect = $("#sect").val();
                var address = $("#address").val();
                var sub_district = $("#sub_district").val();
                var district = $("#district").val();
                var province = $("#province").val();
                var postal_code = $("#postal_code").val();

                if (firstname == '') {
                    alert('กรุณาเพิ่มชื่อ');
                } else if (nickname == '') {
                    alert('กรุณาเพิ่มฉายา');
                } else if (lastname == '') {
                    alert('กรุณาเพิ่มนามสกุล');
                } else if (person == '') {
                    alert('กรุณาเพิ่มเลขบัตรประชาชน');
                } else if (academic_standing == '') {
                    alert('กรุณาเพิ่มวิทยฐานะ');
                } else if (sect == '') {
                    alert('กรุณาเพิ่มสังกัดนิกาย');
                } else if (address == '') {
                    alert('กรุณาเพิ่มที่อยู่');
                } else if (sub_district == '') {
                    alert('กรุณาเพิ่มตำบล');
                } else if (district == '') {
                    alert('กรุณาเพิ่มอำเภอ');
                } else if (province == '') {
                    alert('กรุณาเพิ่มจังหวัด');
                } else if (postal_code == '') {
                    alert('กรุณาเพิ่มรหัสไปรษณีย์');
                } else {
                    $("#addvipassana").submit();
                }
            }
        </script>
        <jsp:include page="../footer.jsp" /> 