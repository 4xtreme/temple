<%-- 
    Document   : contact_report
    Created on : Aug 9, 2018, 2:45:04 PM
    Author     : pop
--%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:include page="../header.jsp" />
<c:if test="${roleadmin == 'admin'}">
    <jsp:include page="../navigation.jsp" />
</c:if>
<c:if test="${roleadmin == 'user'}">
    <jsp:include page="../navigation_top.jsp" />
</c:if>
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header" id="title_head">รายงาน</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <form action="contactreport" method="POST" >
                    <span>ตั้งแต่วันที่</span>
                    <input type="text" id="start" name="start" autocomplete="off" required="true" value="${dateval}" class="form-control width_15 form_start"   />
                    <span>ถึงวันที่</span>
                    <input type="text" id="end" name="end" autocomplete="off" required="true" value="${enddateval}" class="form-control width_15 form_start"   />
                    <input type="submit" value="ค้นหา" />
                </form>
                <br/>
                <br/>
                <input type="hidden" id="contactreport" value="${contact_report}"/>
                <div class="table-responsive" >
                    <table class="table table-striped table-bordered table-hover">
                        <thead>
                            <tr>
                                <th class="text-center">ชื่อ-นามสกุล</th>
                                <th class="text-center">ที่อยู่</th>
                                <th class="text-center">วันที่เข้าครั้งแรก</th>
                                <th class="text-center">จำนวนครั้ง</th>
                            </tr>
                        </thead>
                        <tbody>
                            <c:if test="${contact_report != null}">  
                                <c:forEach var="contactreport" items="${contact_report}" varStatus="loop">
                                    <tr>
                                        <td class="text-center">${contactreport.contact_name}</td>
                                        <td class="text-center">${contactreport.address}  ${contactreport.village}  ${contactreport.district}  ${contactreport.city}  ${contactreport.province}  ${contactreport.postalcode}</td>
                                        <td class="text-center">${contactreport.first_come}</td>
                                        <td class="text-center">${contactreport.count_come}</td>
                                    </tr>
                                </c:forEach>
                            </c:if>
                        </tbody>
                    </table>
                </div>
                <form  action="selectcontactreport" method="post" id="print_form" >
                    <input type="hidden" value="${templeid}" name="temple_id"/>
                    <input type="hidden" id="start_date" name="start_date" value="${dateval}" />
                    <input type="hidden" id="end_date" name="end_date"  value="${enddateval}"/>
                    <input  type="hidden" id="action" name="action" />
                    <center>
                        <button type="button" style="width: auto;" onclick="submitFrom()">PDF  <i style="font-size: 20px" class="fas fa-file-pdf"></i></button>

                        <button type="button" onclick="submitFromCSV()" > CSV  <i style="font-size: 20px" class="fas fa-file-excel"></i> </button>
                    </center>
                </form>

            </div>
        </div>
    </div>

</div>
<script>
    $(document).ready(function (){
       if($("#contactreport").val() === "[]" || $("#contactreport").val() === ""){
           $("#print_form").hide();
       }else{
           $("#print_form").show();
       }
    });
    function submitFrom() {
        $('#action').val("PDF");
        $('#start_date').val($('#start').val());
        $('#end_date').val($('#end').val());
        $('#print_form').submit();
    }
    function submitFromCSV() {
        $('#action').val("CSV");
        $('#start_date').val($('#start').val());
        $('#end_date').val($('#end').val());
        $('#print_form').submit();
    }
</script>

<jsp:include page="../footer.jsp" />     
