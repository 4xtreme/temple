<%-- 
    Document   : monk_report
    Created on : Jul 31, 2018, 8:42:18 AM
    Author     : pop
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:include page="../header.jsp" />
<c:if test="${roleadmin == 'admin'}">
    <jsp:include page="../navigation.jsp" />
</c:if>
<c:if test="${roleadmin == 'user'}">
    <jsp:include page="../navigation_top.jsp" />
</c:if>
<style>
    .row {
        margin-bottom: 10px;
    }

    label {
        margin: 7px 0;
    }

    .btn-action {
        width: 100%;
        /*margin: 5px 0;*/
    }

    .rotate {

        transform: rotate(90deg);
        /* Legacy vendor prefixes that you probably don't need... */

        /* Safari */
        -webkit-transform: rotate(90deg);

        /* Firefox */
        -moz-transform: rotate(90deg);

        /* IE */
        -ms-transform: rotate(90deg);

        /* Opera */
        -o-transform: rotate(90deg);

        /* Internet Explorer */
        filter: progid:DXImageTransform.Microsoft.BasicImage(rotation=3);

    }

    .table-header {
        vertical-align: middle !important;
    }

</style>
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header" id="title_head">รายงาน</h1>
            </div>
        </div>
        <div class="row">   
            <div class="col-lg-12">
                <form class='row' action="monkreport" method="POST" >
                    <div class="col-lg-1">
                        <label for="contact_type">ประเภทโยคี:</label>
                    </div>
                    <div class='col-lg-2'>
                        <select class='form-control' name="contact_type" id="contact_type">
                            <option <c:if test="${contact_type eq 'ชาย'}">selected="selected" </c:if> value='ชาย'>
                                    ชาย    
                                </option>
                                <option <c:if test="${contact_type eq 'หญิง'}">selected="selected" </c:if> value='หญิง'>
                                    หญิง
                                </option>
                                <option <c:if test="${contact_type eq 'ต่างชาติ'}">selected="selected" </c:if> value='ต่างชาติ'>
                                    ต่างชาติ
                                </option>
                            </select>
                        </div>

                        <div class="col-lg-1">
                            <label for="exam_date">วันส่งอารมณ์:</label>
                        </div>
                        <div class="col-lg-2"  style="margin-bottom: 5px;">
                            <input type="text" id="examDate" name="exam_date" autocomplete="off" required="true"  class="form-control exam_date"  value="${dateval}" />
                    </div>
                    <div class="col-lg-1" >
                        <input type="submit" value="ค้นหา" class="btn btn-success btn-action" />
                    </div>
                </form>

                    
                <c:if test="${monkreport != null}"> 
                    <div class='row' style="margin-top: 20px;">
                        <div class='col-lg-12 text-center'>
                            <span>ตารางการส่งอารมณ์สำหรับ (โยคี${contact_type}) ที่ลงทะเบียน ประจำวันที่ ${report_date}</span>
                        </div>
                    </div>

                    <input type="hidden" id="monkreport" value="${monkreport}"/>
                    <div class="table-responsive" >
                        <table class="table table-striped table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th class="text-center table-header" scope="col" rowspan="2">ลำดับ</th>
                                    <th class="text-center table-header" scope="col" rowspan="2">ชื่อ-สกุล</th>
                                    <th class="text-center table-header" scope="col" rowspan="2"> กุฏิ </th>
                                    <th class="text-center table-header" scope="col" colspan="${count}"  >
                                        พระวิปัสสนาจารย์
                                    </th>
                                    <th class="text-center table-header" scope="col" rowspan="2"> วันที่ออก</th>                      
                                </tr>
                                <tr>
                                    <c:forEach var="vipassana" items="${vipassanas}" varStatus="loop">
                                        <th class="text-center rotate table-header">
                                            ${vipassana.firstname}
                                        </th>
                                    </c:forEach>
                                </tr>
                            </thead>
                            <tbody>
                                <c:if test="${monkreport != null}"> 
                                    <tr>
                                        <td></td>
                                        <td class="text-center"><b><u>จำนวนโยคีทั้งหมดที่สอบอารมณ์แต่ละฐาน</u></b></td>
                                        <td></td>
                                        <c:forEach var="vipassanaData" items="${vipassanaDatas}" varStatus="loop">
                                            <td class="text-center">${vipassanaData.count}</td>
                                        </c:forEach>
                                        <%--</c:forEach>--%>
                                        <td class="text-center">รวม ${examCount} คน</td>
                                    </tr>
                                    <c:forEach var="monkreport" items="${monkreport}" varStatus="loop">
                                        <tr>
                                            <td class="text-center">${loop.index + 1}</td>
                                            <td class="text-center">${monkreport.contactName}</td>
                                            <td class="text-center">${monkreport.roomName}</td>
                                            <c:forEach var="vipassana" items="${vipassanas}" varStatus="loop">
                                                <c:if test="${vipassana.id == monkreport.vipassanaId}">
                                                    <td class="text-center">**</td>
                                                </c:if>
                                                <c:if test="${vipassana.id != monkreport.vipassanaId}">
                                                    <td class="text-center"></td>
                                                </c:if>
                                            </c:forEach>
                                            <td class="text-center">${monkreport.dateFormat}</td>
                                        </tr>
                                    </c:forEach>
                                </c:if>
                            </tbody>
                        </table>
                    </div>
                    <form  action="selectmonkreport" method="post" id="print_form">
                        <input type="hidden" id="temple_id" value="${templeid}" name="temple_id"/>
                        <input type="hidden" id="start_date" name="exam_date" value="${dateval}" />

                        <center>
                            <!--<button type="button" style="width: auto;" onclick="submitFrom()">PDF  <i style="font-size: 20px" class="fas fa-file-pdf"></i></button>-->

                            <!--<button type="button" onclick="submitFromCSV()" > CSV  <i style="font-size: 20px" class="fas fa-file-excel"></i> </button>-->
                            <a id="downloadReport" href="#" type="button"  class="btn btn-default" style="background-color: black;color: white;" onclick="submitFromCSV()">พิมพ์รายงาน</a>
                            <a id="downloadSummaryReport" href="#" type="button"  class="btn btn-default" style="background-color: black;color: white;" onclick="submitFromSummary()">พิมพ์รายงานสรุป</a>

                        </center>
                    </form>

                </c:if>
            </div>
        </div>
    </div>

</div>
<script>
    $(document).ready(function () {
        if ($("#monkreport").val() === "[]" || $("#monkreport").val() === "") {
            $("#print_form").hide();
        } else {
            $("#print_form").show();
        }
        console.log($('.rotate').width());
        $('.rotate').css('height', "150px");
    });

    function submitFrom() {
        $('#action').val("PDF");
        $('#start_date').val($('#start').val());
        $('#end_date').val($('#end').val());
        $('#print_form').submit();
    }

    const b64toBlob = (b64Data, contentType = '', sliceSize = 512) => {
        console.log(contentType);
        const byteCharacters = atob(b64Data);
        const byteArrays = [];

        for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
            const slice = byteCharacters.slice(offset, offset + sliceSize);

            const byteNumbers = new Array(slice.length);
            for (let i = 0; i < slice.length; i++) {
                byteNumbers[i] = slice.charCodeAt(i);
            }

            const byteArray = new Uint8Array(byteNumbers);
            byteArrays.push(byteArray);
        }

        const blob = new Blob(byteArrays, {type: contentType});
        return blob;
    }

    function submitFromCSV() {

        let data = {
            "temple_id": $('#temple_id').val(),
            "exam_date": $('#start_date').val(),
            "action": $('#contact_type').val()
        };
        $.ajax({
            url: "${pageContext.request.contextPath}/selectmonkreport",
            type: 'POST',
            data: data,
            async: false,
            success: function (data, textStatus, jqXHR) {
                const blob = b64toBlob(data, 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
                blobUrl = URL.createObjectURL(blob);
            }, complete: function (jqXHR, textStatus) {
                $('#downloadReport').attr({
                    'download': 'ตารางการส่งอารมณ์สำหรับ(โยคี${contact_type}).xlsx',
                    'href': blobUrl
                });
            }
        });
//        $('#print_form').submit();
    }

    function submitFromSummary() {

        let data = {
            "temple_id": $('#temple_id').val()
        };
        $.ajax({
            url: "${pageContext.request.contextPath}/summaryreport",
            type: 'POST',
            data: data,
            async: false,
            success: function (data, textStatus, jqXHR) {
                const blob = b64toBlob(data, 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
                blobUrl = URL.createObjectURL(blob);
            }, complete: function (jqXHR, textStatus) {
                $('#downloadSummaryReport').attr({
                    'download': 'จำนวนผู้ใช้ทั้งหมด.xlsx',
                    'href': blobUrl
                });
            }
        });
//        $('#print_form').submit();
    }
</script>

<jsp:include page="../footer.jsp" />     
