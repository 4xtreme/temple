<%-- 
    Document   : count_report
    Created on : Sep 24, 2019, 12:54:35 PM
    Author     : ak
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:include page="../header.jsp" />
<c:if test="${roleadmin == 'admin'}">
    <jsp:include page="../navigation.jsp" />
</c:if>
<c:if test="${roleadmin == 'user'}">
    <jsp:include page="../navigation_top.jsp" />
</c:if>
<style>
    .row {
        margin-bottom: 10px;
    }

    label {
        margin: 7px 0;
    }

    .btn-action {
        width: 100%;
        /*margin: 5px 0;*/
    }

    .rotate {

        transform: rotate(90deg);
        /* Legacy vendor prefixes that you probably don't need... */

        /* Safari */
        -webkit-transform: rotate(90deg);

        /* Firefox */
        -moz-transform: rotate(90deg);

        /* IE */
        -ms-transform: rotate(90deg);

        /* Opera */
        -o-transform: rotate(90deg);

        /* Internet Explorer */
        filter: progid:DXImageTransform.Microsoft.BasicImage(rotation=3);

    }

    .table-header {
        vertical-align: middle !important;
    }

</style>
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header" id="title_head">จำนวนบุคคลภายในวัด</h1>
            </div>
        </div>
        <div class="row">   
            <div class="col-lg-12">
                <div class='row' style="margin-top: 20px;overflow: hidden">
                    <div class="table-responsive" style="width: 40%;float: left;margin-left :50px">
                    <table class="table table-striped table-bordered table-hover">
                        <thead>
                            <tr>
                                <th class="text-center table-header" scope="col" colspan="2" style="background-color: #3e8f3e">บุคลากรในวัด</th>
                                
                            </tr>
                            <tr>
                                <th class="text-center table-header">บุคคล</th>
                                <th class="text-center table-header">จำนวน</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td class="" colspan="2">สำนักสงฆ์</td>
                            </tr>
                            <tr>
                                <td class="text-center">พระ</td>
                                <td class="text-center">${monkCount}</td>
                            </tr>
                            <tr>
                                <td class="text-center">สามเณร</td>
                                <td class="text-center">${noviceCount}</td>
                            </tr>
                            <tr>
                                <td class="text-center">พระสัทธิวิหาริก</td>
                                <td class="text-center">${saththi_count}</td>
                            </tr>
                            <tr>
                                <td class="text-center">พระอาคัรตุกะ</td>
                                <td class="text-center">${guest_monk_count}</td>
                            </tr>
                            <tr>
                                <td class="text-center">สามสิทะฺวิหาริก</td>
                                <td class="text-center">${samsaththi_count}</td>
                            </tr>
                            <tr>
                                <td class="" colspan="2">สำนักงานแม่ชี</td>
                            </tr>
                            <tr>
                                <td class="text-center">ภิกษุณี</td>
                                <td class="text-center">${bddhist_nun_count}</td>
                            </tr>
                            <tr>
                                <td class="text-center">แม่ชี</td>
                                <td class="text-center">${nun_count}</td>
                            </tr>
                            <tr>
                                <td class="text-center">แม่ชีอาคันตุกะ</td>
                                <td class="text-center">${guest_nun_count}</td>
                            </tr>
                            <tr>
                                <td class="" colspan="2">สำนักงานต่างประเทศ</td>
                            </tr>
                            <tr>
                                <td class="text-center">ภิกษุต่างประเทศ</td>
                                <td class="text-center">${foreign_monk_count}</td>
                            </tr>
                            <tr>
                                <td class="text-center">แม่ชีต่างประเทศ</td>
                                <td class="text-center">${foreign_nun_count}</td>
                            </tr>
                            <tr>
                                <td class="" colspan="2">สำนักงานวัด</td>
                            </tr>
                            <tr>
                                <td class="text-center">โยคีประจำ</td>
                                <td class="text-center">${regular_yogi_count}</td>
                            </tr>
                            <tr>
                                <td class="text-center">จิตอาสา</td>
                                <td class="text-center">${volunteer_count}</td>
                            </tr>
                            <tr>
                                <th class="text-center">รวม</th>
                                <th class="text-center">${allCountInTemple}</th>
                            </tr>
                        </tbody>
                    </table>
                </div>
                    <!------------------------------------------------------------------------------------------------------------------------------------>
                    <div class="table-responsive" style="width: 40%;float: right;margin-right: 50px">
                    <table class="table table-striped table-bordered table-hover">
                        <thead>
                            <tr>
                                <th class="text-center table-header" scope="col" colspan="2" style="background-color: #eea236">ผู้มาปฏิบัติธรรม</th>
                            </tr>
                            <tr>
                                <th class="text-center table-header">บุคคล</th>
                                <th class="text-center table-header">จำนวน</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td class="" colspan="2">สำนักสงฆ์</td>
                            </tr>
                            <tr>
                                <td class="text-center">พระ</td>
                                <td class="text-center">${monkVisitor}</td>
                            </tr>
                            <tr>
                                <td class="text-center">สามเณร</td>
                                <td class="text-center">${noviceMonkVisitor}</td>
                            </tr>
                            <tr>
                                <td class="text-center">โยคีชาย</td>
                                <td class="text-center">${yogiMaleVisitor}</td>
                            </tr>
                            <tr>
                                <td class="" colspan="2">สำนักงานแม่ชี</td>
                            </tr>
                            <tr>
                                <td class="text-center">ภิกษุณี</td>
                                <td class="text-center">${bddhistNunkVisitor}</td>
                            </tr>
                            <tr>
                                <td class="text-center">แม่ชี</td>
                                <td class="text-center">${nunVisitor}</td>
                            </tr>
                            <tr>
                                <td class="text-center">โยคีหญิง</td>
                                <td class="text-center">${yogiFemaleVisitor}</td>
                            </tr>
                            <tr>
                                <td class="" colspan="2">สำนักงานต่างประเทศ</td>
                            </tr>
                            <tr>
                                <td class="text-center">โยคีต่างชาติชาย</td>
                                <td class="text-center">${foreign_male_yogi_count}</td>
                            </tr>
                            <tr>
                                <td class="text-center">โยคีต่างชาติหญิง</td>
                                <td class="text-center">${foreign_female_yogi_count}</td>
                            </tr>
                            <tr>
                                <th class="text-center">รวม</th>
                                <th class="text-center">${allCountVisitor}</th>
                            </tr>
                        </tbody>
                    </table>
                </div>
                </div>
            </div>
        </div>
    </div>

</div>
<jsp:include page="../footer.jsp" />     
