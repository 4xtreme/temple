<%-- 
    Document   : withdrawlist
    Created on : Dec 1, 2017, 8:20:20 AM
    Author     : pop
--%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:include page="../header.jsp" />
<jsp:include page="../navigation.jsp" />

<div id="page-wrapper">
    <div class="container-fluid">
        <!-- start Content -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">รายการเครื่องจักร</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                
         <button type="button" class="btn btn-default btn-success">
          <span class="glyphicon glyphicon-print"></span> Print
        </button>
               
           <button type="button" class="btn btn-default btn-sm">
          <span class="glyphicon glyphicon-download-alt"></span> Download
        </button>

                <br>
                <br>
                <br>
                
                <div class="table-responsive" >
                    <table class="table table-striped table-bordered table-hover">
                        <thead>
                            <tr>
                                <th class="text-center">สถานะ</th>
                                <th class="text-center">รหัสเครื่องจักร</th>
                                <th class="text-center">เลขทะเบียน</th>                            
                                <th class="text-center">ยี่ห้อ / รุ่น</th>
                                <th class="text-center">ประเภท</th>
                                <th class="text-center">หมวด</th>
                                <!--th class="text-center">แก้ไข</th-->
                                <!--th class="text-center">ลบ</th-->
                            </tr>
                        </thead>
                        <tbody>
                            <c:if test="${machines != null}">  
                                <c:forEach var="machines" items="${machines}" varStatus="loop">
                                    <tr>
                                        <td class="text-center">${machines.machine_status}</td>
                                        <td class="text-center">${machines.id}</td>                                       
                                        <td class="text-center"> 
                                             <form id="viewmachine" action="viewmachine" method="post" >    
                                                <input type="hidden" value="${machines.id}" name="idview"/>
                                            <a  href="javascript:;" onclick="document.getElementById('viewmachine').submit();"> ${machines.id} /${machines.car_regis_num}</a>
                                             </form>   
                                        </td>
                                        <td class="text-center">${machines.brand} / ${machines.machine_model}</td>
                                        <td class="text-center">${machines.type}</td>
                                        <td class="text-center">${machines.category}</td>
                                        <!--td class="text-center">
                                           
                                                <button type="submit"  class="no-border tran-bg"><i class="fa fa-edit edit-btn" style="font-size: 30px"></i></button>
                                            </form>  
                                        </td-->
                                        <!--td class="text-center">
                                            <form id="delete_${machines.id}"  action="deletemachine" method="post" >    
                                                <input type="hidden" value="${machines.id}" name="id"/>
                                                <a  onclick="chkConfirm(${machines.id});" ><i class="fa fa-trash fa-2x" aria-hidden="true"></i></a>                                              
                                            </form>  
                                        </td-->
                                    </tr>
                                </c:forEach>
                            </c:if>
                        </tbody>
                    </table>
                </div>
      <div class="row">
          <center>
        <div class="pagination_center">
            <nav aria-label="Page navigation col-centered">
                <ul class="pagination">
                    <c:if test="${page_priv == true}">  
                        <li class="page-item"><a class="page-link"  href="${pageContext.request.contextPath}/machinelist?page=${page-1}"><i class="fa fa-angle-left  "></i></a></li>
                            </c:if>
                            <c:if test="${page_count != 0}">
                                <c:forEach begin="0" end="${page_count-1}" varStatus="loop">
                                    <c:if test="${page == loop.index}"> 
                                <li class="page-item"><a class="page-link active"  href="${pageContext.request.contextPath}/machinelist?page=${loop.index}">${loop.index+1}</a></li>
                                </c:if> 
                                <c:if test="${page != loop.index}"> 
                                <li class="page-item"><a class="page-link"  href="${pageContext.request.contextPath}/machinelist?page=${loop.index}">${loop.index+1}</a></li>
                                </c:if> 
                            </c:forEach>
                        </c:if>
                        <c:if test="${page_next == true}"> 
                        <li class="page-item"><a class="page-link" href="${pageContext.request.contextPath}/machinelist?page=${page+1}"><i class="fa fa-angle-right "></i></a></li>
                            </c:if>
                </ul>
            </nav>
        </div>
    </div><!--end row-->
            </div>
        </div>
    </div><!-- End Content -->    
</div><!-- End Page Content -->
<script <script language="JavaScript">
    function chkConfirm(rowid) {
        bootbox.confirm({
            message: "คุณแน่ใจหรือว่าต้องการลบ ?",
            buttons: {
                confirm: {
                    label: 'ยืนยัน',
                    className: 'btn-success'
                },
                cancel: {
                    label: 'ยกเลิก',
                    className: 'btn-danger'
                }
            },
            callback: function (result) {
                if (result) {
                    $("#delete_" + rowid).submit();
                }
                console.log('This was logged in the callback: ' + result + "#delete_" + rowid);
            }
        });
    }
   </script>
</script>
<jsp:include page="../footer.jsp" />
