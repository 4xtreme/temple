<%-- 
    Document   : addmachine
    Created on : Nov 21, 2017, 1:23:44 PM
    Author     : pop
--%>

<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<jsp:include page="../header.jsp" />
<jsp:include page="../navigation.jsp" />
<!-- Page Content -->
<div id="page-wrapper">
    <div class="container-fluid">
        <!-- start Content -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">รายละเอียดเครื่องจักร</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="tab-content">
                    <div class="tab-pane fade active in">
                        <form:form action="editmachines" method="POST" modelAttribute="machine" enctype="multipart/form-data"  >
                            <fieldset>
                                <input type="hidden" value="${machine.id}" name="id"/>
                                <div class="row">
                                    <div class="col-lg-8" >
                                        <div class="dl-horizontal">
                                            <div class="form-group">
                                                <span class="label_title">ชื่อเครื่อง</span>
                                                <form:input type="text" class="no-border tran-bg" path="machine_name" /><form:errors path="machine_name"/>
                                            </div>
                                            <div class="form-group">
                                                <span class="label_title">เลขทะเบียนรุ่น</span>
                                                <form:input type="text" class="no-border tran-bg" path="car_regis_num" /><form:errors path="car_regis_num"/>
                                            </div>

                                            <div class="form-group">
                                                <span>ยี่ห้อ</span>
                                                <form:input type="text" class="no-border tran-bg" path="brand"  /><form:errors path="brand"/>
                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                <span>รุ่น</span>
                                                <form:input type="text" class="no-border tran-bg" path="machine_model" /><form:errors path="machine_model"/>
                                            </div>

                                            <div class="form-group">
                                                <span>ขนาดเครื่องยนต์</span>
                                                <form:input type="text" class="no-border tran-bg" path="engine_size" /><form:errors path="engine_size"/>
                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                <form:input type="text" class="no-border tran-bg" path="size_type" />
                                               
                                            </div>

                                            <div class="form-group">
                                                <span>หมายเลขเครื่อง</span>
                                                <form:input type="text" class="no-border tran-bg" path="machine_num" /><form:errors path="machine_num"/>
                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                <span>หมายเลขตัวถัง</span> 
                                                <form:input type="text" class="no-border tran-bg"  path="body_num" /><form:errors path="body_num"/>
                                            </div>

                                            <div class="form-group">
                                                <span>ประเภท</span>
                                                <form:input type="text" class="no-border tran-bg"  path="type" />
                                                &nbsp;&nbsp;&nbsp;&nbsp;
                                                <span>หมวด/กลุ่ม</span>
                                                 <form:input type="text" class="no-border tran-bg"  path="category" />
                                            </div>

                                            <div class="form-group">
                                                <div class="row margin_top10"> 
                                                    <div class="col-xs-3 " style="margin-left: 15px">
                                                        <span style="font-size: 20px; color: skyblue; line-height: 60px"> แนบเอกสาร</span>
                                                    </div>

                                                    <div class="col-xs-6">
                                                        <div class="file_upload_box">
                                                            <div class="col-xs-6">
                                                                <form:input path="machine_file" class="form-control-sm no-border width_full file_dir" type="text"   disabled="disabled"  /><br>
                                                            </div>
                                                            <div class="clearfix"></div>
                                                        </div>
                                                    </div>		
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="dl-horizontal">
                                            <img src="${machine.machine_pic}"  alt="Image preview" class="thumbnail" style="max-width: 350px; max-height: 350px; width: 350px; height: 350px;margin-left:-19px; margin-top:-19px">           
                                        </div>
                                    </div>
                                </div>
                                </br>
                                </br>
                                </br>
                                <div class="row">
                                    <div class="col-lg-8" style="border-top: solid; border-width: 1px; ">
                                        </br></br>
                                        <div class="dl-horizontal">
                                            <div class="form-group">
                                                <span>วันเริ่มใช้งาน </span>
                                                <i class="fa fa-calendar " style="font-size: 20px"></i>
                                                <form:input class="no-border tran-bg" type="text"   path="start_date" /> &nbsp; &nbsp; &nbsp; <form:errors path="start_date" />  &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp;             
                                                <span>อายุงาน 2 ปี 3 เดือน 20 วัน</span>
                                            </div>
                                            <div class="form-group">
                                                <span>ซื้อจาก / ตัวแทนจำหน่าย</span>
                                                 <form:input class="no-border tran-bg" type="text"   path="buy_form" />                                         
                                            </div>                 
                                            <div class="form-group">
                                                <span>คนขับ / พนักงานประจำเครื่อง </span>
                                                <form:input class="no-border tran-bg" type="text"   path="driver" />
                                            </div>
                                            <div class="form-group">
                                                <span>สถานะเครื่องจักร </span>
                                                  <form:input class="no-border tran-bg" type="text"   path="machine_status" />
                                            </div>
                                        </div>
                                    </div>
                                    <right>
                                        <div class="col-lg-2 margin_left10 ">
                                            <div class="dl-horizontal">
                                                <label>Machine Status :  </label>
                                                <span>Avaliable</span></br>
                                                <label>Machine Number : </label>
                                                <span>xxxxxxxxxxxx</span></br>
                                                <label>Last update date : </label>
                                                <span>xxxxxxxxxxxx</span>
                                            </div>
                                        </div>
                                    </right>
                                </div>
                                </br>
                                </br>
                                </br>
                            </fieldset>
                        </form:form>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- End Content -->
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js" ></script>  
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/uikit/3.0.0-beta.20/js/uikit.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/img/bootstrap-imageupload.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/img/bootstrap-imageupload.js"></script>
<script>
    var $imageupload = $('.imageupload');
    $imageupload.imageupload();

</script>
<script>
    document.getElementById("uploadBtn").onchange = function () {
        var split = this.value;
        split = split.substr(12, );
        document.getElementById("uploadFile").value = split;
    };

</script>
<script>
    $(document).ready(function () {
        $("#sub_mainmenu").show();
        $("#side-menu").hide();
        $("#back-btn").click(function () {
            $("#side-menu").show(500);
            $("#sub_mainmenu").hide(1000);
            $("#bottom-nav").hide(250);
            $("#bottom-nav").show(100);

        });
    });
</script>
<jsp:include page="../footer.jsp" />
