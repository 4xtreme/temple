<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<jsp:include page="../header.jsp" />
<c:if test="${roleadmin == 'admin'}">
    <jsp:include page="../navigation.jsp" />
</c:if>
<c:if test="${roleadmin == 'user'}">
    <jsp:include page="../navigation_top.jsp" />
</c:if>
<!-- Page Content -->
<!DOCTYPE html>
<html>
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/echarts/echarts.min.js"></script>


    <div id="page-wrapper">
        <div class="container-fluid">
            <!-- start Content -->
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header" id="title_head"> สถิติผู้เข้ามาปฏิบัติธรรมทั้งหมด</h1>
                </div>
            </div>

            <fieldset>
                <select id="select" class="form-control width_15">
                    <option value="">เลือกช่วงเวลา</option>
                    <option value="7">7วัน</option>
                    <option value="30">30วัน</option>
                    <option value="180">6เดือน</option>
                    <option value="365">1ปี</option>

                </select>  

                <div class="container">
                    <div class="col-md-12">
                        <div id="chart" style="height: 550px; width: 100%; "></div>
                        <script type="text/javascript">
                            var dom = document.getElementById("chart");
                            var Chart = echarts.init(dom);
                            var app = {};
                            option = null;
                            option = {
                                tooltip: {
                                    trigger: 'axis',
                                    axisPointer: {// 坐标轴指示器，坐标轴触发有效
                                        type: 'shadow'        // 默认为直线，可选为：'line' | 'shadow'
                                    }
                                },
                                legend: {
                                    data: ['ชาย', 'หญิง', 'ภิกษุ', 'ภิกษุณี', 'สามเณร', 'แม่ชี', 'ต่างชาติชาย', 'ต่างชาติหญิง']
                                },
                                grid: {
                                    left: '3%',
                                    right: '4%',
                                    bottom: '3%',
                                    containLabel: true
                                },
                                dataZoom: [{
                                        type: 'inside'
                                    }, {
                                        type: 'slider'
                                    }],
                                xAxis: {
                                    type: 'category',
                                    data: []
                                },
                                yAxis: {
                                    type: 'value'
                                },
                                series: [
                                    {
                                        name: 'ชาย',
                                        type: 'bar',
                                        stack: 'จำนวน',
                                        label: {
                                            normal: {
                                                show: true,
                                                position: 'inside'
                                            }
                                        },
                                        data: [0, 0, 0, 0, 0, 0, 0]
                                    },
                                    {
                                        name: 'หญิง',
                                        type: 'bar',
                                        stack: 'จำนวน',
                                        label: {
                                            normal: {
                                                show: true,
                                                position: 'inside'
                                            }
                                        },
                                        data: [0, 0, 0, 0, 0, 0, 0]
                                    },
                                    {
                                        name: 'ภิกษุ',
                                        type: 'bar',
                                        stack: 'จำนวน',
                                        label: {
                                            normal: {
                                                show: true,
                                                position: 'inside'
                                            }
                                        },
                                        data: [0, 0, 0, 0, 0, 0, 0]
                                    },
                                    {
                                        name: 'ภิกษุณี',
                                        type: 'bar',
                                        stack: 'จำนวน',
                                        label: {
                                            normal: {
                                                show: true,
                                                position: 'inside'
                                            }
                                        },
                                        data: [0, 0, 0, 0, 0, 0, 0]
                                    },
                                    {
                                        name: 'สามเณร',
                                        type: 'bar',
                                        stack: 'จำนวน',
                                        label: {
                                            normal: {
                                                show: true,
                                                position: 'inside'
                                            }
                                        },
                                        data: [0, 0, 0, 0, 0, 0, 0]
                                    },

                                    {
                                        name: 'แม่ชี',
                                        type: 'bar',
                                        stack: 'จำนวน',
                                        label: {
                                            normal: {
                                                show: true,
                                                position: 'inside'
                                            }
                                        },
                                        data: [0, 0, 0, 0, 0, 0, 0]
                                    },
                                    {
                                        name: 'ต่างชาติชาย',
                                        type: 'bar',
                                        stack: 'จำนวน',
                                        label: {
                                            normal: {
                                                show: true,
                                                position: 'inside'
                                            }
                                        },
                                        data: [0, 0, 0, 0, 0, 0, 0]
                                    }, {
                                        name: 'ต่างชาติหญิง',
                                        type: 'bar',
                                        stack: 'จำนวน',
                                        label: {
                                            normal: {
                                                show: true,
                                                position: 'inside'
                                            }
                                        },
                                        data: [0, 0, 0, 0, 0, 0, 0]
                                    }
                                ]
                            };



                            if (option && typeof option === "object") {
                                Chart.setOption(option, true);
                            }
                            $(document).ready(function () {
                                $('#select').change(function () {
                                    getdata($(this).val());

                                }
                                );
                            });

                            function getdata(v) {
                                $.ajax({
                                    type: "GET",
                                    url: "get_adata/" + v})
                                        .done(function (d)


                                        {

                                            option.xAxis.data = d[0];
                                            option.series[0].data = d[1];
                                            option.series[1].data = d[2];
                                            option.series[2].data = d[3];
                                            option.series[3].data = d[4];
                                            option.series[4].data = d[5];
                                            option.series[5].data = d[6];
                                            option.series[6].data = d[7];
                                            option.series[7].data = d[8];
                                            Chart.setOption(option, true);

                                        }
                                        );
                            }


                        </script>
                    </div> 
                </div>   


            </fieldset>    
            </br> </br> </br> </br> 
        </div>
    </div>

</body>
</html>
<!-- End Content -->
<script language="JavaScript">

</script>
<jsp:include page="../footer.jsp" />     