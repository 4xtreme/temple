<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<jsp:include page="../header.jsp" />
<c:if test="${roleadmin == 'admin'}">
    <jsp:include page="../navigation.jsp" />
</c:if>
<c:if test="${roleadmin == 'user'}">
    <jsp:include page="../navigation_top.jsp" />
</c:if>
<!-- Page Content -->
<!DOCTYPE html>
<html>
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/echarts/echarts.min.js"></script>


    <div id="page-wrapper">
        <div class="container-fluid">
            <!-- start Content -->
            <div class="row"><br>
                <div class="col-lg-12">
                    <h1 class="page-header" id="title_head">แผนภูมิย้อนหลัง</h1>
                </div>
            </div>

            <fieldset>
                <select id="select" class="form-control width_15">
                    <option value="">เลือกช่วงเวลา</option>
                    <option value="7">7วัน</option>
                    <option value="15">15วัน</option>
                    <option value="30">30วัน</option>
        
                </select>  

                <div class="container">
                    <div class="col-md-12">
                        <div id="chart1" style="height: 300px; width: 100%; "></div>
                        <script type="text/javascript">
                            var dom1 = document.getElementById("chart1");
                            var barChart = echarts.init(dom1);
                            var app1 = {};
                            baroption = null;
                            baroption = {
                                title: {
                                    text: ' สถิติผู้เข้ามาปฏิบัติธรรมในแต่ละวัน',
                                    subtext: '',
                                    x: 'center'
                                },
                                color: ['#3398DB'],
                                tooltip: {
                                    trigger: 'axis',
                                    axisPointer: {// 坐标轴指示器，坐标轴触发有效
                                        type: 'shadow'        // 默认为直线，可选为：'line' | 'shadow'
                                    }
                                },
                                grid: {
                                    left: '3%',
                                    right: '4%',
                                    bottom: '3%',
                                    containLabel: true
                                },
                                xAxis: [
                                    {
                                        type: 'category',
                                        data: [],
                                        axisTick: {
                                            alignWithLabel: true
                                        }
                                    }
                                ],
                                yAxis: [
                                    {
                                        type: 'value'
                                    }
                                ],
                                series: [
                                    {
                                        name: 'จำนวน',
                                        type: 'bar',
                                        barWidth: '60%',
                                        data: []
                                    }
                                ]
                            };



                            if (baroption && typeof baroption === "object") {
                                barChart.setOption(baroption, true);
                            }


                            $(document).ready(function () {
                                $('#select').change(function () {
                                    getdata($(this).val());

                                }
                                );
                            });

                            function getdata(v) {
                                $.ajax({
                                    type: "GET",
                                    url: "get_data/" + v})
                                        .done(function (d)
                                        {
                                           baroption.xAxis[0].data=d[1];
                                           baroption.series[0].data=d[0];
                                            barChart.setOption(baroption, true);
                                        }
                                        );
                            }
                        </script>
                    </div> 
                </div>   


            </fieldset>  
            </br> </br> </br> 
        </div>
    </div>

</body>
</html>
<!-- End Content -->
<script language="JavaScript">

</script>
<jsp:include page="../footer.jsp" />     