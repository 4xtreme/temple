<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<jsp:include page="../header.jsp" />
<c:if test="${roleadmin == 'admin'}">
    <jsp:include page="../navigation.jsp" />
</c:if>
<c:if test="${roleadmin == 'user'}">
    <jsp:include page="../navigation_top.jsp" />
</c:if>
<!-- Page Content -->
<!DOCTYPE html>
<html>
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/echarts/echarts.min.js"></script>


    <div id="page-wrapper">
        <div class="container-fluid">
            <!-- start Content -->
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header" id="title_head">แผนภูมิ</h1>
                </div>
            </div>

            <fieldset>
                <select id="select" class="form-control width_15">
                    <option value="">เลือกช่วงเวลา</option>
                    <option value="7">7วัน</option>
                    <option value="30">30วัน</option>
                    <option value="3">3เดือน</option>
                    <option value="6">6เดือน</option>
                    <option value="12">12เดือน</option>
                </select>  

                <div class="container">
                    <div class="col-md-12">
                        <div id="chart1" style="height: 300px; width: 100%; "></div>
                        <script type="text/javascript">
                            var dom1 = document.getElementById("chart1");
                            var barChart = echarts.init(dom1);
                            var app1 = {};
                            baroption = null;
                            baroption = {
                                title: {
                                    x: 'center',
                                    text: 'สถิติผู้เข้ามาปฏิบัติธรรม',
                                    subtext: '',
                                    link: ''
                                },
                                tooltip: {
                                    trigger: 'item'
                                },
                                toolbox: {
                                    show: true,
                                    feature: {
                                        dataView: {show: true, readOnly: false},
                                        restore: {show: true},
                                        saveAsImage: {show: true}
                                    }
                                },
                                calculable: true,
                                grid: {
                                    borderWidth: 0,
                                    y: 80,
                                    y2: 60
                                },
                                xAxis: [
                                    {
                                        type: 'category',
                                        show: false,
                                        data: ['ชาย', 'หญิง', 'ภิกษุ', 'ภิกษุณี', 'สามเณร', 'แม่ชี', 'ต่างชาติชาย', 'ต่างชาติหญิง']
                                    }
                                ],
                                yAxis: [
                                    {
                                        type: 'value',
                                        show: false
                                    }
                                ],

                                series: [
                                    {
                                        name: 'amount',
                                        type: 'bar',
                                        itemStyle: {
                                            normal: {
                                                color: function (params) {
                                                    // build a color map as your need.
                                                    var colorList = [
                                                        '#C23531', '#2F4454', '#64A0A8', '#D48265', '#91C7AE',
                                                        '#749F83', '#CA8622', '#BDA29A', '#F3A43B', '#60C0DD',
                                                        '#D7504B', '#C6E579', '#F4E001', '#F0805A', '#26C0C0'
                                                    ];
                                                    return colorList[params.dataIndex];
                                                },
                                                label: {
                                                    show: true,
                                                    position: 'top',
                                                    formatter: '{b}\n{c}'
                                                }
                                            }
                                        },
                                        data: [0, 0, 0, 0, 0, 0, 0, 0],

                                        markPoint: {
                                            tooltip: {
                                                trigger: 'item',
                                                backgroundColor: 'rgba(0,0,0,0)',
                                                formatter: function (params) {
                                                    return '<img src="'
                                                            + params.data.symbol.replace('image://', '')
                                                            + '"/>';
                                                }
                                            },
                                            data: [

                                            ]
                                        }
                                    }
                                ]
                            };


                            if (baroption && typeof baroption === "object") {
                                barChart.setOption(baroption, true);
                            }

                        </script>
                    </div> 
                </div>   






                <div class="container">
                    <div class="row">
                        <div class="col-md-12"> 
                            <div id="chart2" style="height: 340px; width: 90%; "></div>
                        </div>   
                        <script type="text/javascript">
                            var dom = document.getElementById("chart2");
                            var pieChart = echarts.init(dom);
                            var app = {};
                            pieoption = {
                                title: {
                                    text: 'สถิติผู้เข้ามาปฏิบัติธรรม',
                                    subtext: '',
                                    x: 'center'
                                },
                                tooltip: {
                                    trigger: 'item',
                                    formatter: "{a} <br/>{b} : {c} ({d}%)"
                                },
                                legend: {
                                    orient: 'vertical',
                                    x: 'left',
                                    data: ['ชาย', 'หญิง', 'ภิกษุ', 'ภิกษุณี', 'สามเณร', 'แม่ชี', 'ต่างชาติชาย', 'ต่างชาติหญิง']
                                },
                                toolbox: {
                                    show: true,
                                    feature: {
                                        mark: {show: true},
                                        dataView: {show: true, readOnly: false},
                                        magicType: {
                                            show: true,
                                            type: ['pie', 'funnel'],
                                            option: {
                                                funnel: {
                                                    x: '25%',
                                                    width: '50%',
                                                    funnelAlign: 'left',
                                                    max: 1548
                                                }
                                            }
                                        },
                                        restore: {show: true},
                                        saveAsImage: {show: true}
                                    }
                                },
                                calculable: true,
                                series: [
                                    {
                                        name: 'สถิติการใช้งาน',
                                        type: 'pie',
                                        radius: '55%',
                                        center: ['50%', '60%'],
                                        data: [
                                            {value: 0, name: 'ชาย'},
                                            {value: 0, name: 'หญิง'},
                                            {value: 0, name: 'ภิกษุ'},
                                            {value: 0, name: 'ภิกษุณี'},
                                            {value: 0, name: 'สามเณร'},
                                            {value: 0, name: 'แม่ชี'},
                                            {value: 0, name: 'ต่างชาติชาย'},
                                            {value: 0, name: 'ต่างชาติหญิง'}
                                        ]
                                    }
                                ]
                            };
                            if (pieoption && typeof pieoption === "object") {
                                pieChart.setOption(pieoption, true);
                            }

                            $(document).ready(function () {
                                $('#select').change(function () {
                                    getdata($(this).val());

                                }
                                );
                            });

                            function getdata(v) {
                                $.ajax({
                                    type: "GET",
                                    url: "getdata/" + v})
                                        .done(function (d) {
                                           // alert(JSON.stringify(d));
                                            baroption.series[0].data = [0, 0, 0, 0, 0, 0, 0, 0];
                                            
                                            for (j = 0; j < d.length; j++) {
                                                if (d[j].name == "ชาย") {
                                                    baroption.series[0].data[0] = d[j].value;
                                                }
                                                if (d[j].name == "หญิง") {
                                                    baroption.series[0].data[1] = d[j].value;
                                                }
                                                if (d[j].name == "ภิกษุ") {
                                                    baroption.series[0].data[2] = d[j].value;
                                                }
                                                if (d[j].name == "ภิกษุณี") {
                                                    baroption.series[0].data[3] = d[j].value;
                                                }
                                                if (d[j].name == "สามเณร") {
                                                    baroption.series[0].data[4] = d[j].value;
                                                }
                                                if (d[j].name == "แม่ชี") {
                                                    baroption.series[0].data[5] = d[j].value;
                                                }
                                                if (d[j].name == "ต่างชาติชาย") {
                                                    baroption.series[0].data[6] = d[j].value;
                                                }
                                                if (d[j].name == "ต่างชาติหญิง") {
                                                    baroption.series[0].data[7] = d[j].value;
                                                }
                                            }

                                            pieoption.series[0].data = d;
                                            pieChart.setOption(pieoption, true);
                                            barChart.setOption(baroption, true);
                                        });
                                    }
                        </script>        
                    </div>
                </div>                
            </fieldset> 
            </br> </br> </br> 
        </div>
    </div>

</body>
</html>
<!-- End Content -->
<script language="JavaScript">

</script>
<jsp:include page="../footer.jsp" />     