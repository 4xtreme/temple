
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:include page="../header.jsp" />
<c:if test="${roleadmin == 'admin'}">
    <jsp:include page="../navigation.jsp" />
</c:if>
<c:if test="${roleadmin == 'user'}">
    <jsp:include page="../navigation_top.jsp" />
</c:if>
<!-- Page Content -->
<div id="page-wrapper">
    <div class="container-fluid">
        <!-- start Content -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">นำเข้าข้อมูลผู้ปฏิบัติธรรม</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <form:form id="saveimport"  action="import" method="POST"  enctype="multipart/form-data">
                    <fieldset>
                        <div class="tab-pane fade active in">
                            <div class="col-lg-12" >
                                <div class="col-lg-2" ></div>
                                <div class="col-lg-8" >
                                    <div class="dl-horizontal">
                                        <div class="form-group">
                                            <p>กรุณาเลือกไฟล์ที่ต้องการนำเข้า</p>
                                            <p id="error_msg" style="color: red;">กรุณาเลือกไฟล์</p>
                                            <input id="importdata" name="importdata" type="file" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </fieldset>
                </form:form>
                </br>
                </br>
                </br>
                <div class="row">
                    <div class="col-lg-12">
                        <center>
                            <div class="form-group text-center"><button type="submit" class="btn btn-primary" onclick="succesRegis()"> <span class="fa fa-save "></span> บันทึก</button></div>
                        </center>
                    </div>
                </div>  

            </div>
        </div>
    </div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js" ></script>  
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/uikit/3.0.0-beta.20/js/uikit.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/img/bootstrap-imageupload.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/img/bootstrap-imageupload.js"></script>
<script>
    $(document).ready(function() {
        $("#error_msg").hide();
    });
    function succesRegis() {
        if($("#importdata").val() === ''){
            $("#error_msg").show();
        }else{
            $("#saveimport").submit();
        }
        
    }


</script>

<!-- End Page Content -->
<jsp:include page="../footer.jsp" />