<%-- 
    Document   : header
    Created on : Jan 11, 2017, 1:38:57 PM
    Author     : chatcharin
--%>


<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html;charset=UTF-8" %>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>ระบบจองห้องศูนย์ปฏิบัติธรรม</title>
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/images/icon.jpg" />
        <!-- Bootstrap Core CSS -->
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">

        <!-- MetisMenu CSS -->
        <link href="${pageContext.request.contextPath}/css/metisMenu.min.css" rel="stylesheet">

        <!-- Timeline CSS -->
        <link href="${pageContext.request.contextPath}/css/timeline.css" rel="stylesheet">

        <!-- Custom CSS -->
        <c:if test="${roleadmin == 'admin'}">
            <link href="${pageContext.request.contextPath}/css/startmin.css" rel="stylesheet">
        </c:if>
        <c:if test="${roleadmin == 'user'}">
            <link href="${pageContext.request.contextPath}/css/startmin_new.css" rel="stylesheet">
        </c:if>

        <!--Custom Font-->
        <link href="${pageContext.request.contextPath}/css/font.css" rel="stylesheet">

        <!-- Morris Charts CSS -->
        <link href="${pageContext.request.contextPath}/css/morris.css" rel="stylesheet">

        <!-- Custom Fonts -->
        <link href="${pageContext.request.contextPath}/css/font-awesome.min.css" rel="stylesheet" type="text/css">

        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">

        <!--Date Time -->
        <link href="${pageContext.request.contextPath}/css/bootstrap-datetimepicker.min.css" rel="stylesheet" media="screen">
        <!--<link href="${pageContext.request.contextPath}/css/datepicker.css" rel="stylesheet" media="screen">-->

        <!--Grap-->
        <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.css">

        <!--toggle-->
        <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
        <!--image-->
        <link href="${pageContext.request.contextPath}/css/img/bootstrap-imageupload.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath}/css/img/bootstrap-imageupload.min.css" rel="stylesheet">

        <!--theme-->
        <link href="${pageContext.request.contextPath}/css/theme.css" rel="stylesheet">

        <!--Address-->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Kanit">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/uikit/3.0.0-beta.20/css/uikit.css">
        <link href="${pageContext.request.contextPath}/css/jquery.Thailand.js/dist/jquery.Thailand.min.css " rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/jquery-ui-1.8.4.css" />
        <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/jquery.ganttView.css" />
        <!--<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/reset.css" />-->
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
        <script src='https://www.google.com/recaptcha/api.js'></script>
    </head>
    <body>

        <div id="wrapper">
