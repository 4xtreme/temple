<%-- 
    Document   : footer
    Created on : Jan 11, 2017, 1:40:02 PM
    Author     : chatcharin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>


<style>
    .navbar-center
    {
        position: absolute;
        width: 100%;
        left: 0;
        top: 0;
        text-align: center;
    }
</style>
<br/>
<br/>
<br/>
<br/>
<div class="navbar navbar-default navbar-fixed-bottom">
    <div class="container">
        <p class="navbar-text navbar-center footer-copyright text-center text-black-50 py-3">จัดทำโดย
            <a href="produceby" target="_blank" >บริษัท โฟร์เอ็กซ์ตรีม จำกัด</a>
        </p>


    </div>
</div>

<!-- jQuery -->
<script src="${pageContext.request.contextPath}/js/jquery.min.js"></script>

<!--View Grant-->
<script type="text/javascript" src="${pageContext.request.contextPath}/js/date.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery.ganttView.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>

<!-- Moment Js -->
<script src="${pageContext.request.contextPath}/js/moment.min.js"></script>
<script src="${pageContext.request.contextPath}/js/moment.js"></script>


<script src="${pageContext.request.contextPath}/js/bootbox.min.js"></script>
<!-- Metis Menu Plugin JavaScript -->
<script src="${pageContext.request.contextPath}/js/metisMenu.min.js"></script>




<!-- Custom Theme JavaScript -->
<script src="${pageContext.request.contextPath}/js/startmin.js"></script>

<!-- Custom search-filters -->
<script src="${pageContext.request.contextPath}/js/search-filters.js"></script>
<!--Date Time -->
<!--<script type="text/javascript" src="${pageContext.request.contextPath}/js/bootstrap-datetimepicker.js" charset="UTF-8"></script>-->
<!--<script type="text/javascript" src="${pageContext.request.contextPath}/js/locales/bootstrap-datetimepicker.th.js" charset="UTF-8"></script>-->

<link href="${pageContext.request.contextPath}/css/bootstrap-datepicker.css" rel="stylesheet" />
<script src="${pageContext.request.contextPath}/js/bootstrap-datepicker-custom.js"></script>
<script src="${pageContext.request.contextPath}/js/locales/bootstrap-datepicker.th.min.js" charset="UTF-8"></script>


<!-- Grap -->

<script type="text/javascript" src="${pageContext.request.contextPath}/js/raphael.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/morris.min.js"></script>
<!-- toggle --> 
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>

<!-- text area -->
<script src="${pageContext.request.contextPath}/js/tinymce/jquery.tinymce.min.js"></script>
<script src="${pageContext.request.contextPath}/js/tinymce/tinymce.min.js"></script>


<script>tinymce.init({selector: 'textareaTest'});</script>



<script type="text/javascript" >
    window.setTimeout(function () {
        $(".alert").fadeTo(500, 0).slideUp(500, function () {
            $(this).remove();
        });
    }, 4000);

    function rand(filter, length, current) {
        current = current ? current : '';
        var types = {
            number: "0123456789",
            uppercase: "ABCDEFGHIJKLMNOPQRSTUVWXTZ",
            lowercase: "abcdefghiklmnopqrstuvwxyz"
        };
        var r = (filter[0] === "all" ? Object.keys(types) : filter).reduce((s, t) => {
            s += types[t];
            return s
        }, "")
        return length ? rand(filter, --length
                , r.charAt(Math.floor(Math.random() * r.length)) + current) : current;
    }


//    $('.datepicker').datepicker();


    $('.datetime').datepicker({
        format: 'dd/mm/yyyy',
        todayBtn: true,
        language: 'th', //เปลี่ยน label ต่างของ ปฏิทิน ให้เป็น ภาษาไทย   (ต้องใช้ไฟล์ bootstrap-datepicker.th.min.js นี้ด้วย)
        thaiyear: true              //Set เป็นปี พ.ศ.
    }).datepicker("setDate", "0");
    
    console.log("checkin "+$('.checkin').val())
    $('.checkin').datepicker({
        format: 'dd/mm/yyyy',
        todayBtn: true,
        language: 'th', //เปลี่ยน label ต่างของ ปฏิทิน ให้เป็น ภาษาไทย   (ต้องใช้ไฟล์ bootstrap-datepicker.th.min.js นี้ด้วย)
        thaiyear: true              //Set เป็นปี พ.ศ.
    }).datepicker("setDate", $('.checkin').val());
    
    console.log($('.checkout').val())
    $('.checkout').datepicker({
        format: 'dd/mm/yyyy',
        todayBtn: true,
        language: 'th', //เปลี่ยน label ต่างของ ปฏิทิน ให้เป็น ภาษาไทย   (ต้องใช้ไฟล์ bootstrap-datepicker.th.min.js นี้ด้วย)
        thaiyear: true              //Set เป็นปี พ.ศ.
    }).datepicker("setDate", $('.checkout').val());

    $('.form_date').datepicker({
        format: 'dd/mm/yyyy',
        todayBtn: true,
        language: 'th', //เปลี่ยน label ต่างของ ปฏิทิน ให้เป็น ภาษาไทย   (ต้องใช้ไฟล์ bootstrap-datepicker.th.min.js นี้ด้วย)
        thaiyear: true              //Set เป็นปี พ.ศ.
    })

    $('.form_birthdate-1').datepicker({
       format: 'dd/mm/yyyy',
        todayBtn: true,
        language: 'th', //เปลี่ยน label ต่างของ ปฏิทิน ให้เป็น ภาษาไทย   (ต้องใช้ไฟล์ bootstrap-datepicker.th.min.js นี้ด้วย)
        thaiyear: true              //Set เป็นปี พ.ศ.
    })

    $('.form_birthdate').datepicker({
        format: 'dd/mm/yyyy',
        todayBtn: true,
        language: 'th', //เปลี่ยน label ต่างของ ปฏิทิน ให้เป็น ภาษาไทย   (ต้องใช้ไฟล์ bootstrap-datepicker.th.min.js นี้ด้วย)
        thaiyear: true              //Set เป็นปี พ.ศ.
    })

    $('.form_start').datepicker({
       format: 'dd/mm/yyyy',
        todayBtn: true,
        language: 'th', //เปลี่ยน label ต่างของ ปฏิทิน ให้เป็น ภาษาไทย   (ต้องใช้ไฟล์ bootstrap-datepicker.th.min.js นี้ด้วย)
        thaiyear: true              //Set เป็นปี พ.ศ.
    })

    $('.exam_date').datepicker({
        format: 'dd/mm/yyyy',
        todayBtn: true,
        language: 'th', //เปลี่ยน label ต่างของ ปฏิทิน ให้เป็น ภาษาไทย   (ต้องใช้ไฟล์ bootstrap-datepicker.th.min.js นี้ด้วย)
        thaiyear: true              //Set เป็นปี พ.ศ.
    }).datepicker("setDate",  $('.exam_date').val());


    $('.form_db').datepicker({
       format: 'dd/mm/yyyy',
        todayBtn: true,
        language: 'th', //เปลี่ยน label ต่างของ ปฏิทิน ให้เป็น ภาษาไทย   (ต้องใช้ไฟล์ bootstrap-datepicker.th.min.js นี้ด้วย)
        thaiyear: true              //Set เป็นปี พ.ศ.
    });
    
    $('.event_date').datepicker({
       format: 'dd/mm/yyyy',
        todayBtn: true,
        language: 'th', 
        thaiyear: true,   
        autoclose: true
    }).datepicker("setDate", $('.event_date').val());
    
    $('.event_end_date').datepicker({
       format: 'dd/mm/yyyy',
        todayBtn: true,
        language: 'th', 
        thaiyear: true,   
        autoclose: true
    }).datepicker("setDate", $('.event_end_date').val());

</script>
</body>
</html>

