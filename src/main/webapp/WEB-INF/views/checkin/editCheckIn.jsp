<%-- 
    Document   : withdrawlist
    Created on : Dec 1, 2017, 8:20:20 AM
    Author     : pop
--%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:include page="../header.jsp" />

<c:if test="${roleadmin == 'admin'}">
    <jsp:include page="../navigation.jsp" />
</c:if>
<c:if test="${roleadmin == 'user'}">
    <jsp:include page="../navigation_top.jsp" />
</c:if>

<div id="page-wrapper">
    <div class="container-fluid">
        <!-- start Content -->
        <div class="row">
            <center>
                <div class="col-lg-12">    
                    <h1 class="page-header">แก้ไขการเช็คอิน</h1>          
                </div></center>
        </div>
        <div class="row">
            <div class="col-lg-12" >
                <div class="tab-content">
                    <div class="tab-pane fade active in"> 
                        <div class="dl-horizontal">
                            <form   action="editcheckin" method="POST"   >    
                                <center>
                                    <div class="form-group">
                                        <span class="label_title"style="font-size: 20px" >รหัสบัตรประชาชน</span>
                                        <input type="text" name="personal_cardid" autocomplete="off" maxlength="13" required="" max="1"  class="form-control width_20"   />
                                        <button type="submit" class="btn btn-primary btn-sm"><span class="glyphicon glyphicon-search"></span> ค้นหา</button>
                                    </div>
                                </center>
                            </form>
                        </div>
                    </div>
                </div>    
            </div>
        </div>
        <br>
        <br>
        <div class="row">

            <div class="col-lg-12">
                <center>
                    <c:if test="${statusOut != null}">
                        <h4 style="color: red;">ไม่มีข้อมูล !!!</h4>
                    </c:if>

                    <c:if test="${room_out != null}">
                        <div class="tab-content">
                            <div class="tab-pane fade active in"> 
                                <div class="dl-horizontal">     
                                    <h2 >รายละเอียดการเช็คอิน</h2>
                                    <h3 >อาคาร / กุฏิ  &nbsp; : &nbsp;&nbsp;   ${room_out}</h3>                                                    
                                    <h3 >ห้อง &nbsp;  : &nbsp;&nbsp;${roomname_out}</h3>      

<!--                                    <h3 >เช็คอิน &nbsp;  : &nbsp;&nbsp;<input type="text" name="" required="true" disabled="" value="${startDate_out}"  class="form-control width_15 form_start"   /> </h3>
                                    <h3 >เช็คเอาท์  &nbsp;  : &nbsp;&nbsp;<input type="text" name="" required="true"  value="${endDate_out}"  class="form-control width_15 form_start"   /> </h3>   -->
                                    <form action="updatecheckintime" method="POST">
                                        <input type="hidden" name="id" value="${id}">
                                        <h3 >เช็คอิน &nbsp;  : &nbsp;&nbsp;<input type="text" name="chkin" required="true" disabled="" value="${startDate_out}"  class="form-control width_15  checkin"  autocomplete="off" /> </h3>
                                        <h3 >แก้ไขวันกลับ  &nbsp;  : &nbsp;&nbsp;<input type="text" name="chkout" required="true"  value="${endDate_out}"  class="form-control width_15  checkout"  autocomplete="off" /> </h3> 
                                            <c:if test="${btCheckOut != null}">
                                            <button type="submit" onclick="succesCheckOut()" class="btn btn-primary btn-sm"> <span class="glyphicon glyphicon-plus-sign"></span> แก้ไข</button>    
                                        </c:if>                    
                                    </form>  
                                </div>
                            </div>
                        </div>       
                    </c:if>


                    <!--                    <form action="updatecheckout" method="POST">
                                            <input type="hidden" name="id" value="${id}">
                                            <h3 >เช็คอิน &nbsp;  : &nbsp;&nbsp;<input type="text" name="" required="true" disabled="" value="${startDate_out}"  class="form-control width_15 form_start"   /> </h3>
                                            <h3 >เช็คเอาท์  &nbsp;  : &nbsp;&nbsp;<input type="text" name="" required="true"  value="${endDate_out}"  class="form-control width_15 form_start"   /> </h3> 
                    <c:if test="${btCheckOut != null}">
                    <button type="submit" onclick="succesCheckOut()" class="btn btn-primary btn-sm"> <span class="glyphicon glyphicon-plus-sign"></span> แก้ไข</button>    
                    </c:if>                    
                </form>   -->
                </center>
            </div>

        </div>
    </div><!-- End Content --> 
    <BR><BR><BR>
</div><!-- End Page Content -->
<script language="JavaScript">
    function succesCheckOut() {
        alert("แก้ไขเช็คอินสำเร็จ");
    }
</script>
<jsp:include page="../footer.jsp" />
