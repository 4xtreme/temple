<%-- 
    Document   : withdrawlist
    Created on : Dec 1, 2017, 8:20:20 AM
    Author     : pop
--%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:include page="../header.jsp" />
<jsp:include page="../navigation.jsp" />

<div id="page-wrapper">
    <div class="container-fluid">
        <!-- start Content -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">เช็คอินออนไลน์</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-2" ></div>
            <div class="col-lg-8">
                <div class="tab-content">
                    <div class="tab-pane fade active in"> 
                        <div class="dl-horizontal">
                            <form  action="checkInOnline" method="POST"   >                                                        
                                <div class="form-group">
                                    <span class="label_title">รหัสบัตรประชาชน</span>
                                    <input type="text" name="personal_cardid" maxlength="13" required=""  class="form-control width_30"   />
                                    <button type="submit" class="btn btn-primary btn-sm"><span class="glyphicon glyphicon-search"></span> ค้นหา</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-2" ></div>
        </div>
        <br>
        <br>
        
        <div class="row">
            <div class="col-lg-2" ></div>
            <div class="col-lg-8">
            <h3 class="">รายละเอียดการจอง</h3>
                <div class="tab-content">
                    <div class="tab-pane fade active in"> 
                        <div class="dl-horizontal">
                            <div class="form-group">
                                <span class="label_title">วัด</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                                <input type="text" readonly="" value="${templename}" class="form-control width_30"   />
                            </div>
                            <div class="form-group">
                                <span class="label_title">อาคาร / กุฏิ</span>&nbsp;
                                <input type="text" readonly="" value="${room}" class="form-control width_30"   />
                            </div>
                            <div class="form-group">
                                <span class="label_title">ห้อง</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                                <input type="text" readonly="" value="${roomname}" class="form-control width_30"   />
                            </div>
                            <div class="form-group">
                                <span class="label_title">วันที่จอง</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <input type="text" readonly="" value="${createdate}" class="form-control width_30"   />
                            </div>
                            <div class="form-group">
                                <span class="label_title">เช็คอิน</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <input type="text" readonly="" value="" class="form-control width_30"   />
                            </div>
                            <div class="form-group">
                                <span class="label_title">เช็คเอาท์</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <input type="text" readonly="" value="" class="form-control width_30"   />
                            </div>
                        </div>
                    </div>
                </div>
            <c:if test="${templename != null}">        
                <center>
                    <form action="">
                    <button type="submit" class="btn btn-primary btn-sm"> <span class="glyphicon glyphicon-plus-sign"></span> ยืนยันการเช็คอิน</button> 
                    </form>
                </center>
            </c:if>    

            </div>
            <div class="col-lg-2" ></div>
        </div>
    </div><!-- End Content --> 
</div><!-- End Page Content -->
<script <script language="JavaScript">
    </script>
    <jsp:include page="../footer.jsp" />
