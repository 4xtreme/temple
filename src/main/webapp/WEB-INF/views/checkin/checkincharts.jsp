<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<jsp:include page="../header.jsp" />
<c:if test="${roleadmin == 'admin'}">
    <jsp:include page="../navigation.jsp" />
</c:if>
<c:if test="${roleadmin == 'user'}">
    <jsp:include page="../navigation_top.jsp" />
</c:if>
<!-- Page Content -->
<!DOCTYPE html>
<html>
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/echarts/echarts.min.js"></script>


    <div id="page-wrapper">
        <div class="container-fluid">
            <!-- start Content -->
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header" id="title_head">แผนภูมิ</h1>
                </div>
            </div>

            <fieldset>
                <!--                    <select id="monk_id" name="monk_id" class="form-control width_20 " >
                                        <option value="" >--------รายชื่อพระวิปัสสนาจารย์--------</option>
                <c:forEach var = "monk" items="${monklist}">
                    <option value="${monk.monk_id}"  >${monk.firstname}&nbsp;${monk.lastname}</option>
                </c:forEach>
            </select><br><br>-->
                <select id="select" class="form-control width_15"  >
                    <option value="" disabled="" selected="">เลือกช่วงเวลา</option>
                    <option value="7">7วัน</option>
                    <option value="30">30วัน</option>
                </select>  <br><br>

                <!--<input type="submit" id="sendto" class="form-control width_20 send" value="ค้นหา">-->

                <div class="container">
                    <div class="col-md-12">
                        <div id="chart1" style="height: 300px; width: 100%; "></div>
                        <script type="text/javascript">
                            var labelOption = {
                                normal: {
                                    show: true,
                                    position: 'insideBottom',
                                    distance: 15,
                                    align: 'left',
                                    verticalAlign: 'middle',
                                    rotate: 90,
                                    formatter: '{c}  {name|{a}}',
                                    fontSize: 16,
                                    rich: {
                                        name: {
                                            textBorderColor: '#fff'
                                        }
                                    }
                                }
                            };
                            var dom1 = document.getElementById("chart1");
                            var barChart = echarts.init(dom1);
                            var app1 = {};
                            baroption = null;
                            baroption = {
                                color: ['#003366', '#006699', '#4cabce', '#e5323e'],
                                tooltip: {
                                    trigger: 'axis',
                                    axisPointer: {
                                        type: 'shadow'
                                    }
                                },
                                legend: {
                                    data: []
                                },
                                toolbox: {
                                    show: true,
                                    orient: 'vertical',
                                    left: 'right',
                                    top: 'center',
                                    feature: {
                                        mark: {show: true},
                                        dataView: {show: true},
                                        restore: {show: true},
                                        saveAsImage: {show: true},
                                        magicType: {show: true, type: ['line', 'bar']},
                                    }
                                },
                                calculable: true,
                                xAxis: [
                                    {
                                        type: 'category',
                                        axisTick: {show: false},
                                        data: []
                                    }
                                ],
                                yAxis: [
                                    {
                                        type: 'value'
                                    }
                                ],
                                series: []
                            };


                            if (baroption && typeof baroption === "object") {
                                barChart.setOption(baroption, true);
                            }

                            $(document).ready(function () {
                                $(".send").change(function () {
                                    getdata($(this).val());
                                });
                            });

                            $(document).ready(function () {
                                $('#select').change(function () {
                                    getdata($(this).val());
                                });
                            });



                            function getdata(v) {
                                var series_length = baroption.series.length;
                                for (count_serie = 0; count_serie < series_length; count_serie++) {
                                    baroption.series.pop();
                                }
                                $.ajax({
                                    type: "GET",
                                    url: "${pageContext.request.contextPath}/getdatacheckin/" + v})

                                        .done(function (d) {
                                            for (i = 0; i < d.count; i++) {

                                                var data = [];
                                                for (data_c = 0; data_c < d.data.length; data_c++) {
                                                    if (d.data[data_c].length > 0) {
                                                        d.data[data_c].forEach(function (element) {
                                                            if (d.monk[i][1] === element.name) {
                                                                data.push(element.value);
                                                            }
                                                        });
                                                    } else {
                                                        data.push(0);
                                                    }
                                                }

                                                baroption.series.push({
                                                    name: d.monk[i][0],
                                                    type: 'bar',
                                                    barGap: 0,
                                                    label: labelOption,
                                                    data: data
                                                });

                                            }
                                            baroption.xAxis[0].data = d.date;

                                            barChart.setOption(baroption, true);
                                            //barChart.setOption(baroption, true);

                                        });
                            }

//                         
                        </script>
                    </div> 
                </div>   
            </fieldset>          
        </div>
    </div>

</body>
</html>
<!-- End Content -->
<script language="JavaScript">

</script>
<jsp:include page="../footer.jsp" />     
