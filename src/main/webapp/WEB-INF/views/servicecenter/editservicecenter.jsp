<%-- 
    Document   : addmachine
    Created on : Nov 21, 2017, 1:23:44 PM
    Author     : pop
--%>

<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<jsp:include page="../header.jsp" />
<jsp:include page="../navigation.jsp" />
<!-- Page Content -->
<div id="page-wrapper">
    <div class="container-fluid">
        <!-- start Content -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">เพิ่มศูนย์บริการ</h1>
            </div>
        </div>
             <form:form id="addstaff" modelAttribute="servicecenter" action="editservicecenters" method="POST"  enctype="multipart/form-data" >
                            <fieldset>
                                <input type="hidden" value="${servicecenter.id}" name="id"/>
        <div class="row">
            <div class="col-lg-12">

                <div class="tab-content">
                    <div class="tab-pane fade active in"> 
                                <div class="row">
                                    <div class="col-lg-8" >
                                        <div class="dl-horizontal">
              
                                            <div class="form-group">                                                 
                                                <span class="label_title"> ชื่อ  </span>
                                                <form:input type="text"  path="name" />&nbsp;&nbsp;&nbsp;<form:errors path="name"/>
                                                 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                  <span class="label_title">รหัสศูนย์</span>
                                                  <form:input class="no-border tran-bg" type="text" disabled="true" path="id" /><form:errors path="id"/>
                                                </div>
                                            <div class="form-group">
                                                <span class="label_title">ประเภทศูนย์ </span>
                                               <form:select   class="form-control width_20"  path="servicecenter_type">     
                                                    <option  value="aa"  selected="">ช่วงล่าง</option>
                                                 </form:select>&nbsp;&nbsp;&nbsp;<form:errors path="servicecenter_type" />&nbsp;&nbsp;
                                                      <button class="btn btn-circle-plus">+</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                        <span class="label_title">หมวด </span>
                                               <form:select   class="form-control width_20"  path="servicecenter_group">     
                                                    <option  value="aa"  selected="">ศูนย์เจ้าของเครื่อง</option>
                                                 </form:select>&nbsp;&nbsp;&nbsp;<form:errors path="servicecenter_group" />&nbsp;&nbsp;
                                                      <button class="btn btn-circle-plus">+</button>
                                            </div>
                                            <div class="form-group">                                                 
                                                <span class="label_title">ที่อยู่</span>
                                               <form:textarea rows="3" cols="30" path="address" />&nbsp;&nbsp;&nbsp;<form:errors path="address"/>
                                            </div>
                                             <div class="form-group">                                                 
                                                <span class="label_title">ถนน</span>&nbsp;&nbsp;&nbsp;&nbsp;
                                                <form:input type="text"  path="street" />&nbsp;&nbsp;&nbsp;<form:errors path="street"/>
                                                </div>
                                                                       <div class="form-group">
                                             <span class="label_title">ตำบล</span>&nbsp;&nbsp;
                                                <form:input type="text" id="district"  path="local" />&nbsp;&nbsp;&nbsp;<form:errors path="local"/> 
                                          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                         <span class="label_title"> อำเภอ</span>
                                                <form:input type="text" id="amphoe"  path="district" />&nbsp;&nbsp;&nbsp;<form:errors path="district"/>
                                          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                           <span class="label_title"> สถานะ</span>
                                          <form:select   class="form-control width_20"  path="status">     
                                                    <option  value="aa"  selected="">ปกติ</option>
                                                 </form:select>&nbsp;&nbsp;&nbsp;<form:errors path="status" />
                                            </div>
                                                               <div class="form-group">
                                            <span class="label_title"> จังหวัด</span>
                                                <form:input type="text" id="province"  path="province" />&nbsp;&nbsp;&nbsp;<form:errors path="province"/>                       
                                       &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                   <span class="label_title">รหัสไปรษณีย์</span>
                                                   <form:input type="text" id="zipcode" path="zipcode" />&nbsp;&nbsp;&nbsp;<form:errors path="zipcode"/>
                                            </div>
                     
                                        </div>
                                    </div>
                                             <div class="col-lg-4">
                                        <div class="dl-horizontal">
                                            <div class="form-group">
                                                <span> วันที่ </span>
                                                <input type="text" path="" class="no-border tran-bg " disabled="" value="${date}"/>
                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            </div>
                                            <div class="form-group">
                                                <span>หมายเลขใบเบิก : </span>
                                                <input type="text" path="" class="no-border tran-bg "disabled=""/>
                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            </div>
                                            <div class="form-group">
                                                <span>ผู้กรอกข้อมูล : </span>
                                                <input type="text" path="" class="no-border tran-bg" disabled="" value="${user.firstname}"/>
                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            </div>                           
                                        </div>
                                    </div>    
                                </div>
                             
                                            
                                  </div>
                           
                                </div>
                  
                                </br>
                                </br>
                                </br>          
                                <div class="row">

                                    <div class="col-lg-12" style="border-top: solid; border-width: 1px; ">        
                                    </div>
                                </div>
                               <div class="row">
                                </br>
                                </br>
                                </br>  
                                            <div class="col-lg-6">
                                                    <div class="dl-horizontal">
                                                <div class="form-group">
                                                <span class="label_title"> ผู้ติดต่อ </span>
                                                <form:input type="text"  path="contact" />&nbsp;&nbsp;&nbsp;<form:errors path="contact"/> &nbsp;&nbsp;&nbsp;&nbsp;
                                                <span class="label_title"> อีเมล</span>
                                            <form:input type="text"  path="email" />&nbsp;&nbsp;&nbsp;<form:errors path="email"/> 
                                                </div>
                                                     <div class="form-group">
                                                <span class="label_title">ตำแหน่ง </span>
                                                <form:input type="text"  path="position" />&nbsp;&nbsp;&nbsp;<form:errors path="position"/>  &nbsp;&nbsp;&nbsp;
                                                <span class="label_title"> โทร</span>&nbsp;
                                            <form:input type="text"  path="tel" />&nbsp;&nbsp;&nbsp;<form:errors path="tel"/> 
                                                </div>
                                                       <div class="form-group">
                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                <span class="label_title">แชท </span>
                                                <form:input type="text"  path="chat" />&nbsp;&nbsp;&nbsp;<form:errors path="chat"/>  
                                                </div>
                                    </div>
                                                </div>
                                        <div class="col-lg-6">
                                                 <div class="dl-horizontal">
                                                     <div class="form-group">
                                                <span class="label_title">บันทึก</span>                                                                                   
                                                </div>
                                                     <form:textarea rows="5" cols="50" path="note" />&nbsp;&nbsp;&nbsp;<form:errors path="note"/>
                                            </div>
                                    </div>
                                </div>

                                </br>
                                </br>
                                </br>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <center>
                                            <button onclick="document.getElementById('addstaff').submit();" class="btn  button-size-save"style="margin-left: 20px;margin-right: 20px">Save</button>
                                            <button class="btn  button-size-sucess" type="submit" >เรียบร้อย</button>
                                        </center>
                                    </div>
                                </div>                
                    </div>
                </div>
      </fieldset>
          </form:form> 
            </div>
        </div>
   


    <!-- End Content -->


<jsp:include page="../footer.jsp" />
