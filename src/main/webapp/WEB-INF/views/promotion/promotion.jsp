<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:include page="../header.jsp" />
<jsp:include page="../navigation.jsp" />
 <!-- Page Content -->
    <div id="page-wrapper">
       <div class="container-fluid">
		<!-- start Content -->
            <div class="row">
				<div class="col-lg-6">
								<dl class="dl-horizontal">
                        <h1 class="page-header text-center">Promotion </h1>
                         <form action="sendemailcontact" method="POST">
                        <h3 class="page-header text-left">Customer </h3>
                        <select class="form-control " name="typect" >
                              <option  value="0" disabled=" " selected="">Select Customer</option>
                              <option value="all">All Customer</option>
                            <c:forEach var="contactlist" items="${contactlists}" varStatus="loop">
                                <option value="${contactlist.member_type}">${contactlist.member_type}</option>
                            </c:forEach>                                           
                        </select>
                        
                         <h3 class="page-header ">Topic </h3>
                         <input required="" class="form-control" name="topice" type="text"  />
                       
                         <h3 class="page-header ">Detail </h3>
                         <textareaTest required="" name="detail" class="form-control" ></textareaTest>
                          <div  class="form-group text-left"><button  type="submit" class="btn btn-primary"> <span class="fa fa-send "></span> Sent Email</button></div>
                          </form>
                           </dl>
                </div>
            
			</div>
                
			
	                   
              
			<!-- End Content -->
        </div>
	<!-- End Page Content -->

</div>
 <jsp:include page="../footer.jsp" />

    
    
  
