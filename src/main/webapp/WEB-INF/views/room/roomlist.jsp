<%-- 
    Document   : withdrawlist
    Created on : Dec 1, 2017, 8:20:20 AM
    Author     : pop
--%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:include page="../header.jsp" />
<c:if test="${roleadmin == 'admin'}">
    <jsp:include page="../navigation.jsp" />
</c:if>
<c:if test="${roleadmin == 'user'}">
    <jsp:include page="../navigation_top.jsp" />
</c:if>

<div id="page-wrapper">
    <div class="container-fluid">
        <!-- start Content -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header" id="title_head">อาคาร / กุฏิ ทั้งหมด</h1>
            </div>
            <div>

                <c:if test="${page_Create != null}">
                    <dd  class="form-group text-right">
                        <form method="get" action="addRoom">
                            <button type="submit" class="btn btn-primary btn-sm"> <span class="glyphicon glyphicon-plus-sign"></span>เพิ่ม อาคาร / กุฏิ</button>
                        </form> 
                    </c:if>   

            </div>
        </div>
        <div class="row">
            <c:if test="${roleadmin == 'admin'}">
                <div class="col-lg-12">
                </c:if>
                <c:if test="${roleadmin == 'user'}">
                    <div class="col-lg-10 col-lg-offset-1">
                    </c:if>
                    <div class="table-responsive" >
                        <table class="table table-striped table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th class="text-center">อาคารที่</th>
                                    <th class="text-center">ชื่ออาคาร</th>
                                    <th class="text-center">สถานะ</th>
                                    <th class="text-center">ประเภท</th>
                                    <th class="text-center">ดูรายละเอียด</th>
                                    <th class="text-center">แก้ไข</th>
                                    <th class="text-center">ลบ</th>

                                </tr>
                            </thead>
                            <tbody>
                                <c:if test="${rooms != null}">  
                                    <c:forEach var="rooms" items="${rooms}" varStatus="loop">
                                        <tr>
                                            <td class="text-center">${rooms.id}</td>
                                            <td class="text-center">${rooms.name}</td>
                                            <td class="text-center">${rooms.status}</td>
                                            <td class="text-center">${rooms.type} / ${rooms.sex}</td>
                                            <td class="text-center">
                                                <!--                                            <form  action="viewroom" method="post" >    
                                                                                                <input type="hidden" value="${rooms.id}" name="id"/>
                                                                                                <button type="submit"  class="no-border tran-bg"><i  style="color: green;" class="fa fa-search-plus" style="font-size: 30px"></i></button>
                                                                                            </form>-->
                                                <button type="button" class="no-border tran-bg" data-toggle="modal" data-target="#flipFlop${rooms.id}">
                                                    <i  style="color: green;" class="fa fa-search-plus" style="font-size: 30px"></i>
                                                </button>
                                            </td>
                                            <td class="text-center">
                                                <c:if test="${page_Edit != null}">
                                                    <form  action="editroom" method="post" >    
                                                        <input type="hidden" value="${rooms.id}" name="id"/>
                                                        <button type="submit"  class="no-border tran-bg"><i class="fa fa-edit edit-btn" style="font-size: 30px"></i></button>
                                                    </form> 
                                                </c:if>
                                            </td>
                                            <td class="text-center">
                                                <c:if test="${page_Delete != null}">
                                                    <form id="delete_${rooms.id}"  action="deleteroom" method="post" >    
                                                        <input type="hidden" value="${rooms.id}" name="id"/>
                                                        <a  onclick="chkConfirm(${rooms.id});" ><i style="color: red;" class="fa fa-trash fa-2x" aria-hidden="true"></i></a>

                                                    </form> 
                                                </c:if>
                                            </td>
                                        </tr>

                                        <!-- The modal -->
                                    <div class="modal fade" id="flipFlop${rooms.id}" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                    <h3 class="modal-title" id="modalLabel"  >${rooms.name}</h3>
                                                </div>
                                                <div class="modal-body">


                                                    <div class="row" style="font-size: 20px">

                                                        <div class="col-lg-12 ">
                                                            <dl class="dl-horizontal">

                                                                <div class="form-group">
                                                                    <span class="label_title modal_label">ประเภท</span>
                                                                    <span id="modal_span">${rooms.type}</span>

                                                                </div>
                                                                <div class="form-group">
                                                                    <span class="label_title modal_label"> ชื่อ  </span>
                                                                    <span id="modal_span">${rooms.name}</span>
                                                                    &nbsp;&nbsp;
                                                                    <!--                                                                <span class="label_title">จำนวนห้อง</span>
                                                                    ${room_amount}&nbsp;
                                                                    <span class="label_title">ห้อง</span>
                                                                    &nbsp;&nbsp; &nbsp;&nbsp;
                                                                    <span class="label_title">ทั้งหมด</span>
                                                                    ${sum}
                                                                    <span class="label_title">คน</span>-->
                                                                </div>
                                                                <div class="form-group">    
                                                                    <span class="label_title modal_label">สำหรับ</span>
                                                                    &nbsp;

                                                                    <span id="modal_span">${rooms.sex}</span>
                                                                    &nbsp;&nbsp;

                                                                </div>
                                                                <div class="form-group">
                                                                    <span class="label_title modal_label">ประเภทห้องน้ำ</span>  
                                                                    <span id="modal_span">${rooms.toilet_type}</span>
                                                                    &nbsp;&nbsp;
                                                                    <span class="label_title modal_label">จำนวน</span>
                                                                    <span id="modal_span">${rooms.toilet_amount}</span>&nbsp;<form:errors path="toilet_amount"/>
                                                                    <span class="label_title modal_label">ห้อง</span>                                         
                                                                </div>
                                                                <div class="form-group">    
                                                                    <span class="label_title modal_label">สถานะ</span>
                                                                    &nbsp;<span id="modal_span">${rooms.status}</span>                                               
                                                                </div>

                                                            </dl>
                                                        </div>

                                                    </div>


                                                </div>



                                                <div class="modal-footer">
                                                    <form  action="viewroom" method="post" >    
                                                        <input type="hidden" value="${rooms.id}" name="id"/>
                                                        <button type="submit" class="btn btn-primary">รายละเอียดเพิ่มเติม</button>
                                                    </form> 

                                                    <button type="button" class="btn btn-danger" data-dismiss="modal">ปิด</button>
                                                </div>
                                            </div>
                                        </div>

                                    </div>


                            </div>
                            <!------------------------------->

                        </c:forEach>
                    </c:if>
                    </tbody>
                    </table>
                </div>
                <div class="row">
                    <center>
                        <div class="pagination_center">
                            <nav aria-label="Page navigation col-centered">
                                <ul class="pagination">
                                    <c:if test="${page_priv == true}">  
                                        <li class="page-item"><a class="page-link"  href="${pageContext.request.contextPath}/roomAll?page=${page-1}"><i class="fa fa-angle-left  "></i></a></li>
                                            </c:if>
                                            <c:if test="${page_count != 0}">
                                                <c:forEach begin="0" end="${page_count-1}" varStatus="loop">
                                                    <c:if test="${page == loop.index}"> 
                                                <li class="page-item" ><a class="page-link active"  href="${pageContext.request.contextPath}/roomAll?page=${loop.index}">${loop.index+1}</a></li>
                                                </c:if> 
                                                <c:if test="${page != loop.index}"> 
                                                <li class="page-item"><a class="page-link"  href="${pageContext.request.contextPath}/roomAll?page=${loop.index}">${loop.index+1}</a></li>
                                                </c:if> 
                                            </c:forEach>
                                        </c:if>
                                        <c:if test="${page_next == true}"> 
                                        <li class="page-item"><a class="page-link" href="${pageContext.request.contextPath}/roomAll?page=${page+1}"><i class="fa fa-angle-right "></i></a></li>
                                            </c:if>
                                </ul>
                            </nav>
                        </div>
                </div><!--end row-->
            </div>
        </div>
    </div><!-- End Content -->    
</div><!-- End Page Content -->
<script <script language="JavaScript">
    function chkConfirm(rowid) {
        bootbox.confirm({
            message: "คุณแน่ใจหรือว่าต้องการลบ ?",
            buttons: {
                confirm: {
                    label: 'ยืนยัน',
                    className: 'btn-success'
                },
                cancel: {
                    label: 'ยกเลิก',
                    className: 'btn-danger'
                }
            },
            callback: function (result) {
                if (result) {
                    $("#delete_" + rowid).submit();
                }
                console.log('This was logged in the callback: ' + result + "#delete_" + rowid);
            }
        });
    }
    </script>
</script>
<jsp:include page="../footer.jsp" />
