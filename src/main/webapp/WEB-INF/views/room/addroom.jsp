<%-- 
    Document   : addmachine
    Created on : Nov 21, 2017, 1:23:44 PM
    Author     : pop
--%>

<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<jsp:include page="../header.jsp" />
<c:if test="${roleadmin == 'admin'}">
    <jsp:include page="../navigation.jsp" />
</c:if>
<c:if test="${roleadmin == 'user'}">
    <jsp:include page="../navigation_top.jsp" />
</c:if>
<!-- Page Content -->
<style>
    .label_title{
        float: left;
        width: 100px;
    }
</style>
<div id="page-wrapper">
    <div class="container-fluid">
        <!-- start Content -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header" id="title_head">เพิ่ม อาคาร / กุฏิ</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12"> 
                <div class="tab-content">
                    <div class="tab-pane fade active in"> 
                        <div class="row">
                            <!--<div class="col-lg-3" ></div>-->
                            <c:if test="${roleadmin == 'admin'}">
                                <div class="col-lg-6 col-lg-offset-3">
                                </c:if>
                                <c:if test="${roleadmin == 'user'}">
                                    <div class="col-lg-6 col-lg-offset-4">
                                    </c:if>
                                    <form:form id="addroom" modelAttribute="room" action="saveRoom" method="POST"  enctype="multipart/form-data" >
                                        <fieldset>
                                            <div class="dl-horizontal"> 
                                                <div class="form-group">
                                                    <span class="label_title" width="15%">ประเภท</span>
                                                    <form:select path="type" class="form-control width_30">
                                                        <option  value="อาคาร" selected="">อาคาร</option>
                                                        <option  value="กุฏิ" >กุฏิ</option>
                                                        <option  value="อาคารสำรอง" >อาคารสำรอง</option>
                                                    </form:select><form:errors path="type" />
                                                </div>
                                                <div class="form-group">
                                                    <span class="label_title" width="15%"> ชื่อ  </span>
                                                    <form:input type="text" id="name" class="form-control width_60"   path="name" style="margin-right: 5px;"/><form:errors path="name" class="text-danger"/>

                                                </div>
                                                <div class="form-group">    
                                                    <span class="label_title">สำหรับ</span>
                                                    &nbsp;
                                                    <form:radiobutton checked="checked"  value="ชาย" path="sex"/> <form:errors path="sex" />
                                                    <span class="">ชาย</span>
                                                    &nbsp;&nbsp;
                                                    <form:radiobutton  value="หญิง" path="sex"/> <form:errors path="sex" />
                                                    <span class="">หญิง</span>                                               
                                                </div>
                                                <div class="form-group">
                                                    <span class="label_title">ประเภทห้องน้ำ</span>
                                                    <form:select path="toilet_type" class="form-control width_20">
                                                        <option  value="รวม" selected="">รวม</option>
                                                        <option  value="ในตัว" >ในตัว</option>
                                                    </form:select><form:errors path="toilet_type" />
                                                    &nbsp;&nbsp;
                                                    <span class="">จำนวน</span>
                                                    <form:input type="number" value="1" id="toilet_amount" class="form-control width_10"   path="toilet_amount" />
                                                    <span class="" style="margin-right: 5px;">ห้อง</span>     <form:errors path="toilet_amount" class="text-danger"/>                                    
                                                </div>
                                                <div class="form-group">    
                                                    <span class="label_title">สถานะ</span>
                                                    &nbsp;
                                                    <form:radiobutton checked="checked"  value="ว่าง" path="status"/> <form:errors path="status" />
                                                    <span class="">ว่าง</span>
                                                    &nbsp;&nbsp;
                                                    <form:radiobutton  value="ไม่ว่าง" path="status"/> <form:errors path="status" />
                                                    <span class="">ไม่ว่าง</span>                                               
                                                </div>
                                            </div>
                                            <center>
                                                <button type="button" id="addroom_btn" onclick="submitForm();" class="btn btn-primary btn-sm"> <span class="glyphicon glyphicon-plus-sign"></span> เพิ่ม</button>
                                            </center> 
                                        </fieldset>
                                    </form:form>                                  

                                </div>
                                <!--<div class="col-lg-3" ></div>-->
                            </div>
                        </div>
                    </div>         
                </div>
            </div>

        </div>
    </div>



    <!-- End Content -->
    <script language="JavaScript">
        function submitForm() {
            document.getElementById("addroom").submit();
        }
    </script>

    <jsp:include page="../footer.jsp" />
