<%-- 
    Document   : addmachine
    Created on : Nov 21, 2017, 1:23:44 PM
    Author     : pop
--%>

<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<jsp:include page="../header.jsp" />
<c:if test="${roleadmin == 'admin'}">
    <jsp:include page="../navigation.jsp" />
</c:if>
<c:if test="${roleadmin == 'user'}">
    <jsp:include page="../navigation_top.jsp" />
</c:if>
<!-- Page Content -->
<div id="page-wrapper">
    <div class="container-fluid">
        <!-- start Content -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header" id="title_head">ดูรายละเอียดอาคาร/กุฏิ</h1>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">

                <div class="tab-content">
                    <div class="tab-pane fade active in"> 
                        <div class="row">
                            <div class="col-lg-3" ></div>
                            <div class="col-lg-6" >
                                <form:form id="addroom" modelAttribute="room" action="saveeditroom" method="POST"  enctype="multipart/form-data" >
                                    <fieldset>
                                        <form:input type="hidden" class="form-control width_40"   path="id" /><form:errors path="id"/>
                                        <div class="dl-horizontal"> 
                                            <div class="form-group">
                                                <span class="label_title">ประเภท</span>
                                                ${room.type}
                                                <form:errors path="type" />
                                            </div>
                                            <div class="form-group">
                                                <span class="label_title"> ชื่อ  </span>
                                                ${room.name}<form:errors path="name"/>
                                                &nbsp;&nbsp;
                                                <span class="label_title">จำนวนห้อง</span>
                                                ${room_amount}&nbsp;
                                                <span class="label_title">ห้อง</span>
                                                &nbsp;&nbsp; &nbsp;&nbsp;
                                                <span class="label_title">ทั้งหมด</span>
                                                ${sum}
                                                <span class="label_title">คน</span>
                                            </div>
                                            <div class="form-group">    
                                                <span class="label_title">สำหรับ</span>
                                                &nbsp;
                                                
                                                ${room.sex}
                                                &nbsp;&nbsp;
                                                
                                            </div>
                                            <div class="form-group">
                                                <span class="label_title">ประเภทห้องน้ำ</span>  
                                                   ${room.toilet_type}
                                                &nbsp;&nbsp;
                                                <span class="label_title">จำนวน</span>
                                                ${room.toilet_amount}&nbsp;<form:errors path="toilet_amount"/>
                                                <span class="label_title">ห้อง</span>                                         
                                            </div>
                                            <div class="form-group">    
                                                <span class="label_title">สถานะ</span>
                                                &nbsp;${room.status}                                               
                                            </div>
                                        </div>
                                    </fieldset>
                                </form:form>
                                <hr>
                                <h3>ห้องทั้งหมด</h3>
                                <br>
                                <div class="table-responsive" >
                                    <table class="table table-striped table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                <th class="text-center">ชื่อ</th>                                    
                                                <th class="text-center">จำนวนคน / ห้อง</th>
                                            </tr>
                                        </thead>
                                        <tbody>    
                                            <c:if test="${roomlist != null}">                                 
                                                <c:forEach var="roomlist" items="${roomlist}" varStatus="loop">
                                                    <tr> 
                                                        <td class="text-center">${roomlist.name}</td>
                                                        <td class="text-center">${roomlist.amount}</td>  
                                                    </tr> 
                                                </c:forEach>
                                            </c:if>
                                        </tbody>
                                    </table>
                                </div>                                 
                            </div>
                            <div class="col-lg-3" ></div>
                        </div>
                    </div>
                </div>         
            </div>
        </div>

    </div>
</div>



<!-- End Content -->


<jsp:include page="../footer.jsp" />
<script>
    function chkConfirm(rowid) {
        bootbox.confirm({
            message: "คุณแน่ใจหรือว่าต้องการลบ ?",
            buttons: {
                confirm: {
                    label: 'ยืนยัน',
                    className: 'btn-success'
                },
                cancel: {
                    label: 'ยกเลิก',
                    className: 'btn-danger'
                }
            },
            callback: function (result) {
                if (result) {
                    $("#delete_" + rowid).submit();
                }
                console.log('This was logged in the callback: ' + result + "#delete_" + rowid);
            }
        });
    }


</script>