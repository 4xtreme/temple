<%-- 
    Document   : addmachine
    Created on : Nov 21, 2017, 1:23:44 PM
    Author     : pop
--%>

<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<jsp:include page="../header.jsp" />
<c:if test="${roleadmin == 'admin'}">
    <jsp:include page="../navigation.jsp" />
</c:if>
<c:if test="${roleadmin == 'user'}">
    <jsp:include page="../navigation_top.jsp" />
</c:if>
<!-- Page Content -->
<div id="page-wrapper">
    <div class="container-fluid">
        <!-- start Content -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header" id="title_head">แก้ไขอาคาร/กุฏิ</h1>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">

                <div class="tab-content">
                    <div class="tab-pane fade active in"> 
                        <div class="row">
                            <div class="col-lg-3" ></div>
                            <div class="col-lg-6" >
                                <form:form id="addroom" modelAttribute="room" action="saveeditroom" method="POST"  enctype="multipart/form-data" >
                                    <fieldset>
                                        <form:input type="hidden" class="form-control width_40"   path="id" /><form:errors path="id"/>
                                        <div class="dl-horizontal"> 
                                            <div class="form-group">
                                                <span class="label_title">ประเภท</span>
                                                <form:select path="type" class="form-control width_30">
                                                    <form:option  value="${room.type}" disabled="true" selected="true" >ประเภท</form:option>
                                                    <form:option  value="อาคาร" >อาคาร</form:option>
                                                    <form:option  value="กุฏิ" >กุฏิ</form:option>
                                                    <form:option  value="อาคารสำรอง" >อาคารสำรอง</form:option>
                                                </form:select><form:errors path="type" />
                                            </div>
                                            <div class="form-group">
                                                <span class="label_title"> ชื่อ  </span>  
                                                <form:input type="text" class="form-control width_30"   path="name" style="margin-right:5px;" /><form:errors path="name" class="text-danger"/>

                                                
                                            </div>
                                            <div class="form-group">    
                                                <span class="label_title">จำนวนห้อง</span>
                                                <input type="text" class="form-control width_10" readonly="" value="${room_amount}"    />&nbsp;
                                                <span class="label_title">ห้อง</span>
                                                &nbsp;&nbsp; &nbsp;&nbsp;
                                                <span class="label_title">ทั้งหมด</span>
                                                <input type="text" class="form-control width_10" readonly="" value="${sum}"    />
                                                <span class="label_title">คน</span>
                                                <span class="label_title">สำหรับ</span>
                                                &nbsp;
                                                <form:radiobutton checked="checked"  value="ชาย" path="sex"/> <form:errors path="sex" />
                                                <span class="label_title">ชาย</span>
                                                &nbsp;&nbsp;
                                                <form:radiobutton  value="หญิง" path="sex"/> <form:errors path="sex" />
                                                <span class="label_title">หญิง</span>                                               
                                            </div>
                                            <div class="form-group">
                                                <span class="label_title">ประเภทห้องน้ำ</span>
                                                <form:select path="toilet_type" class="form-control width_20">
                                                    <form:option  value="${room.toilet_type}" disabled="true" selected="true" >ประเภทห้องน้า</form:option>
                                                    <form:option  value="รวม" >รวม</form:option>
                                                    <form:option  value="ในตัว" >ในตัว</form:option>
                                                </form:select><form:errors path="toilet_type" />
                                                &nbsp;&nbsp;
                                                <span class="label_title">จำนวน</span>
                                                <form:input type="number" class="form-control width_10"    path="toilet_amount" /><form:errors path="toilet_amount"/>
                                                <span class="label_title">ห้อง</span>                                         
                                            </div>
                                            <div class="form-group">    
                                                <span class="label_title">สถานะ</span>
                                                &nbsp;
                                                <form:radiobutton checked="checked"  value="ว่าง" path="status"/> <form:errors path="status" />
                                                <span class="label_title">ว่าง</span>
                                                &nbsp;&nbsp;
                                                <form:radiobutton  value="ไม่ว่าง" path="status"/> <form:errors path="status" />
                                                <span class="label_title">ไม่ว่าง</span>                                               
                                            </div>
                                        </div>
                                    </fieldset>
                                </form:form>
                                <hr>
                                <h3>เพิ่มห้อง</h3>
                                <div class="dl-horizontal">
                                    <div class="form-group">
                                        <form  action="addroomname" method="post" >
                                            <input type="hidden" name="id" value="${id}">
                                            <input type="hidden" name="room_id" value="${room_id}">
                                            <span class="label_title">ชื่อห้อง</span>
                                            <input type="text" class="form-control width_40" required=""   name="name" />
                                            &nbsp;&nbsp;&nbsp;&nbsp;
                                            <span class="label_title">จำนวนคน / ห้อง</span>
                                            <input type="number" class="form-control width_10" maxlength="5" required="" value="1"   name="amount" />&nbsp;&nbsp;&nbsp;&nbsp;
                                            <button class="btn btn-success" type="submit" >เพิ่ม</button>
                                        </form>
                                    </div>  
                                </div>
                                <br>
                                <div class="table-responsive" style="max-height: 500px; overflow: auto;">
                                    <table class="table table-striped table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                <th class="text-center">ชื่อ</th>                                    
                                                <th class="text-center">จำนวนคน / ห้อง</th>
                                                <th class="text-center">ลบ</th>
                                            </tr>
                                        </thead>
                                        <tbody>    
                                            <c:if test="${roomlist != null}">                                 
                                                <c:forEach var="roomlist" items="${roomlist}" varStatus="loop">
                                                    <tr> 
                                                        <td class="text-center">${roomlist.name}</td>
                                                        <td class="text-center">${roomlist.amount}</td>  
                                                        <td class="text-center">
                                                            <form id="delete_${roomlist.id}"  action="updatedelete" method="post" >    
                                                                <input type="hidden" value="${roomlist.id}" name="id_delete"/>
                                                                <input type="hidden" name="id" value="${id}">
                                                                <a  onclick="chkConfirm(${roomlist.id});" ><i style="color: red;" class="fa fa-trash fa-2x" aria-hidden="true"></i></a>
                                                            </form>                                       
                                                        </td>
                                                    </tr> 
                                                </c:forEach>
                                            </c:if>
                                        </tbody>
                                    </table>
                                </div>
                                <center>

                                    <INPUT class="btn button-size-cancel"TYPE="button" VALUE="ยกเลิก" onClick="history.back()">

                                    <button onclick="document.getElementById('addroom').submit();" class="btn  button-size-sucess"style="margin-left: 20px;margin-right: 20px">บันทึก</button>
                                </center>  
                                <br><br><br><br>
                            </div>
                            <div class="col-lg-3" ></div>

                        </div>
                    </div>
                </div>         
            </div>
        </div>

    </div>
</div>

<form id="delete_room"  action="updatedeletetest" method="post" >    
    <input type="hidden" name="test_delete_id" id="test_delete_id" />
    <input type="hidden" name="test_id" id="test_id" >
</form>  



<!-- End Content -->


<jsp:include page="../footer.jsp" />
<script>
    function chkConfirm(rowid, id) {
        bootbox.confirm({
            message: "คุณแน่ใจหรือว่าต้องการลบ ?",
            buttons: {
                confirm: {
                    label: 'ยืนยัน',
                    className: 'btn-success'
                },
                cancel: {
                    label: 'ยกเลิก',
                    className: 'btn-danger'
                }
            },
            callback: function (result) {
                if (result) {
                    $("#delete_" + rowid).submit();
                }
                console.log('This was logged in the callback: ' + result + "#delete_" + rowid);
            }
        });
    }

    function chkConfirmTest(rowid, id) {
        $('#test_delete_id').val(rowid);
        $('#test_id').val(id);

    }


</script>
