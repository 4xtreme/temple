<%-- 
    Document   : header
    Created on : Jan 11, 2017, 1:38:57 PM
    Author     : chatcharin
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<style>

    .dropdown-submenu {
        position: relative;
    }

    .dropdown-submenu>.dropdown-menu {
        top: 0;
        left: 100%;
        margin-top: -6px;
        margin-left: -1px;
        -webkit-border-radius: 0 6px 6px 6px;
        -moz-border-radius: 0 6px 6px;
        border-radius: 0 6px 6px 6px;
    }

    .dropdown-submenu:hover>.dropdown-menu {
        display: block;
    }

    .dropdown-submenu>a:after {
        display: block;
        content: " ";
        float: right;
        width: 0;
        height: 0;
        border-color: transparent;
        border-style: solid;
        border-width: 5px 0 5px 5px;
        border-left-color: #ccc;
        margin-top: 5px;
        margin-right: -10px;
    }

    .dropdown-submenu:hover>a:after {
        border-left-color: #fff;
    }

    .dropdown-submenu.pull-left {
        float: none;
    }

    .dropdown-submenu.pull-left>.dropdown-menu {
        left: -100%;
        margin-left: 10px;
        -webkit-border-radius: 6px 0 6px 6px;
        -moz-border-radius: 6px 0 6px 6px;
        border-radius: 6px 0 6px 6px;
    }
</style>

<!-- Navigation -->

<a class="navbar-brand navbar-center"href="">ระบบจองห้องศูนย์ปฏิบัติธรรม</a>
<nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <!--<a class="navbar-brand" href="#">ระบบจองห้องศูนย์ปฏิบัติธรรม</a>-->
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav" id="ul_nav_top">
                <!--<li class="active"><a href="#">Link <span class="sr-only">(current)</span></a></li>-->
                <li>
                    <a href="${pageContext.request.contextPath}/roomfront"><i class="glyphicon glyphicon-tasks" aria-hidden="true"></i> ดูรายละเอียดห้อง ทั้งหมด </a>
                </li>
                <li>
                    <a href="${pageContext.request.contextPath}/regcreatenow"><i class="fa fa-bed" aria-hidden="true"></i> ลงทะเบียน </a>
                </li>
                <li>
                    <a href="${pageContext.request.contextPath}/bookingRoom"><i class="fa fa-calendar-check-o" aria-hidden="true"></i> เช็คอินยังไม่จอง </a>
                </li>

                <li>
                    <a href="${pageContext.request.contextPath}/editcheckin"><i class="fa fa-sign-in-alt" aria-hidden="true"></i> แก้ไขการเช็คอิน </a>
                </li>
                <li class="dropdown">
                    <a id="dLabel" role="button" data-toggle="dropdown" data-target="#" href="/page.html">
                        อื่นๆ <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu multi-level" role="menu" aria-labelledby="dropdownMenu">
                        <!--                        <li><a href="#">Some action</a></li>
                                                <li><a href="#">Some other action</a></li>
                                                <li class="divider"></li>-->
                        <li class="dropdown-submenu">
                            <a tabindex="-1" href="#" ><i class="fas fa-star" aria-hidden="true"></i> พระวิปัสสนาจารย์ </a>
                            <ul class="dropdown-menu">
                                <c:if test="${monk_page.view_page == true}">
                                    <li>
                                        <a href="${pageContext.request.contextPath}/vipassana"><i class="fa fa-list" aria-hidden="true"></i> ดูรายชื่อพระวิปัสสนาจารย์</a>
                                    </li>
                                </c:if>
                                <c:if test="${monk_page.create_page == true}">
                                    <li>
                                        <a href="${pageContext.request.contextPath}/addvipassana"><i class="fa fa-plus-circle" aria-hidden="true"></i> เพิ่มรายชื่อพระวิปัสสนาจารย์</a>
                                    </li>
                                </c:if>
                                <li>
                                    <a href="${pageContext.request.contextPath}/activity"><i class="fa fa-list" aria-hidden="true"></i> ศาสนกิจ</a>
                                </li>
                                <li>
                                    <a href="${pageContext.request.contextPath}/addactivity"><i class="fa fa-plus-circle" aria-hidden="true"></i> เพิ่มศาสนกิจ</a>
                                </li>
                            </ul>
                        </li>
                        <li class="dropdown-submenu">
                            <a tabindex="-1" href="#" ><i class="fas fa-star" aria-hidden="true"></i> บุคลากรในวัด </a>
                            <ul class="dropdown-menu">
                                <li>
                                    <a href="${pageContext.request.contextPath}/monkall"><i class="fa fa-list" aria-hidden="true"></i> ดูรายชื่อบุคลากรในวัด</a>
                                </li>
                                <li>
                                    <a href="${pageContext.request.contextPath}/monkreg"><i class="fa fa-plus-circle" aria-hidden="true"></i> เพิ่มรายชื่อบุคลากรในวัด</a>
                                </li>
                                <li>
                                    <a href="${pageContext.request.contextPath}/eventsall"><i class="fa fa-list" aria-hidden="true"></i> กิจนิมนต์</a>
                                </li>
                                <li>
                                    <a href="${pageContext.request.contextPath}/eventsmonk"><i class="fa fa-plus-circle" aria-hidden="true"></i> เพิ่มกิจนิมนต์</a>
                                </li>
                                <!--                                <li class="dropdown-submenu">
                                                                    <a href="#">Even More..</a>
                                                                    <ul class="dropdown-menu">
                                                                        <li><a href="#">3rd level</a></li>
                                                                        <li><a href="#">3rd level</a></li>
                                                                    </ul>
                                                                </li>-->
                            </ul>
                        </li>
                        <li class="dropdown-submenu">
                            <a tabindex="-1" href="#" ><i class="glyphicon glyphicon-home" aria-hidden="true"></i> อาคาร / กุฏิ</a>
                            <ul class="dropdown-menu">
                                <li>
                                    <a href="${pageContext.request.contextPath}/roomAll"><i class="fa fa-list" aria-hidden="true"></i> รายการอาคาร /กุฏิ</a>
                                </li>
                                <li>
                                    <a href="${pageContext.request.contextPath}/addRoom"><i class="fa fa-plus-circle" aria-hidden="true"></i> เพิ่ม  อาคาร / กุฏิ</a>
                                </li>
                            </ul>
                        </li>
                        <li class="dropdown-submenu">
                            <a tabindex="-1" href="#" ><i class="fa fa-users" aria-hidden="true"></i> ผู้ปฏิบัติธรรม </a>
                            <ul class="dropdown-menu">
                                <li>
                                    <a href="${pageContext.request.contextPath}/regsall"><i class="fa fa-list" aria-hidden="true"></i> ดูรายชื่อผู้ปฏิบัติธรรม</a>
                                </li>
                            </ul>
                        </li>
                        <li class="dropdown-submenu">
                            <a tabindex="-1" href="#" ><i  class="glyphicon glyphicon-file"></i> รายงาน </a>
                            <ul class="dropdown-menu">
                                <li>
                                    <a href="${pageContext.request.contextPath}/monkreport"><i class="glyphicon glyphicon-book" aria-hidden="true"></i> รายงานผู้เข้าสอบอารมณ์</a>
                                </li>
<!--                                <li>
                                    <a href="${pageContext.request.contextPath}/contactreport"><i class="glyphicon glyphicon-ok-sign" aria-hidden="true"></i> รายงานผู้เข้าปฏิบัติธรรม</a>
                                </li>-->
<li>
                                <a href="${pageContext.request.contextPath}/countreport"><i class="glyphicon glyphicon-ok-sign" aria-hidden="true"></i> จำนวนบุคคลภายในวัด</a>
                            </li>
                                <li>
                                    <a href="${pageContext.request.contextPath}/chartscheckin"><i class="glyphicon glyphicon-user" aria-hidden="true"></i> คนกับพระวิปัสสนาจารย์</a>
                                </li>
                                <li>
                                    <a href="${pageContext.request.contextPath}/ccharts"><i class="glyphicon glyphicon-asterisk" aria-hidden="true"></i> สถิติการใช้งาน</a>
                                </li>
                                <li>
                                    <a href="${pageContext.request.contextPath}/dailycharts"><i class="glyphicon glyphicon-tasks" aria-hidden="true"></i> สถิติการใช้งานในแต่ละวัน</a>
                                </li>
                                <li>
                                    <a href="${pageContext.request.contextPath}/summarycharts"><i class="glyphicon glyphicon-sort" aria-hidden="true"></i> สถิติการใช้งานทั้งหมด</a>
                                </li>
                            </ul>
                        </li>
                        <li class="dropdown">
                            <!--<a href="${pageContext.request.contextPath}/loginstaff"><i class="fas fa-power-off"></i> ออกจากระบบ</a>-->
                            <a href="${pageContext.request.contextPath}/login"><i class="fas fa-power-off"></i> ออกจากระบบ</a>
                        </li>

                    </ul>
                </li>
            </ul>

        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script>
    $(document).ready(function () {
        $("#close-btn").show();
        $("#open-btn").hide();
        $("#mainmenu").click(function () {
            $("#side-menu").hide(100);
            $("#sub_mainmenu").show(100);
        });
        $("#close-btn").click(function () {
            $("#navbar").hide(100);
            $("#open-btn").show(100);
            $("#close-btn").hide(100);
            $("#bottom-nav").hide(100);
        });
        $("#open-btn").click(function () {
            $("#navbar").show(100);
            $("#open-btn").hide(100);
            $("#close-btn").show(100);
            $("#bottom-nav").show(100);
        });

        $('.dropdown-submenu a.test').on("click", function (e) {
            $(this).next('ul').toggle();
            e.stopPropagation();
            e.preventDefault();
        });
    });


</script>
