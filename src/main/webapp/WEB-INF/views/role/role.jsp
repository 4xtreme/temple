<%-- 
    Document   : addmachine
    Created on : Nov 21, 2017, 1:23:44 PM
    Author     : pop
--%>

<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<jsp:include page="../header.jsp" />
<c:if test="${roleadmin == 'admin'}">
    <jsp:include page="../navigation.jsp" />
</c:if>
<c:if test="${roleadmin == 'user'}">
    <jsp:include page="../navigation_top.jsp" />
</c:if>
<!-- Page Content -->
<div id="page-wrapper">
    <div class="container-fluid">
        <!-- start Content -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">เพิ่มการเข้าถึง</h1>
                <dd  class="form-group text-right">           
                    <button type="button" onclick="window.location.href = '${pageContext.request.contextPath}/role'" class="btn btn-primary btn-sm">
                        <span class=""></span> การเข้าถึงทั้งหมด
                    </button>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
<form action="addrole" method="POST">
                <div class="tab-content">
                    <div class="tab-pane fade active in"> 
                        <div class="row">
                            <div class="col-lg-3" ></div>
                            <div class="col-lg-6" >
                                <div class="dl-horizontal">
                                    <div class="form-group">
                           <c:if test="${status != null}">                                    
                           <p style="color: red;"> ไม่สามาถใช้ชื่อนี้ได้  เนื่องจากมีชื่อนี้อยู่ในระบบแล้ว</p>
                           </c:if>
                           <span class="label_title">ชื่อ</span>                          
                           <input type="text" required="" name="roleName" class="form-control width_30"   />
                                                &nbsp;&nbsp;
                                                <button type="submit" class="btn btn-primary btn-sm"> <span class="glyphicon glyphicon-plus-sign"></span> เพิ่ม</button>
                          
                                    </div>  
                                </div> 
                                    
                <div class="table-responsive" >
                    <table class="table table-striped table-bordered table-hover">
                        <thead>
                            <tr>
                                <th class="text-center"></th>
                                <th class="text-center">ดูรายละเอียด</th>
                                <th class="text-center">เพิ่ม</th>
                                <th class="text-center">แก้ไข</th>
                                <th class="text-center">ลบ</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr> 
                                        <td class="text-center"><B>วัด</B></td>
                                        <td class="text-center"><input type="checkbox" name="checkbox1" value="true"></td>  
                                        <td class="text-center"><input type="checkbox" name="checkbox2" value="true"></td>  
                                        <td class="text-center"><input type="checkbox" name="checkbox3" value="true"></td>  
                                        <td class="text-center"><input type="checkbox" name="checkbox4" value="true"></td>  
                                    </tr>
                                                                <tr> 
                                        <td class="text-center"><B>อาคาร / กุฏิ</B></td>
                                        <td class="text-center"><input type="checkbox" name="checkbox5" value="true"></td>  
                                        <td class="text-center"><input type="checkbox" name="checkbox6" value="true"></td>  
                                        <td class="text-center"><input type="checkbox" name="checkbox7" value="true"></td>  
                                        <td class="text-center"><input type="checkbox" name="checkbox8" value="true"></td>  
                                    </tr>
                                    <tr> 
                                        <td class="text-center"><B>เจ้าหน้าที่</B></td>
                                        <td class="text-center"><input type="checkbox" name="checkbox9" value="true"></td>  
                                        <td class="text-center"><input type="checkbox" name="checkbox10" value="true"></td>  
                                        <td class="text-center"><input type="checkbox" name="checkbox11" value="true"></td>  
                                        <td class="text-center"><input type="checkbox" name="checkbox12" value="true"></td>  
                                    </tr>
                                    <tr> 
                                        <td class="text-center"><B>สมาชิก</B></td>
                                        <td class="text-center"><input type="checkbox" name="checkbox13" value="true"></td>  
                                        <td class="text-center"><input type="checkbox" name="checkbox14" value="true"></td>  
                                        <td class="text-center"><input type="checkbox" name="checkbox15" value="true"></td>  
                                        <td class="text-center"><input type="checkbox" name="checkbox16" value="true"></td>  
                                    </tr>
                                    <tr> 
                                        <td class="text-center"><B>เช็คอิน</B></td>
                                        <td class="text-center"><input type="checkbox" name="checkbox17" value="true"></td>  
                                        <td class="text-center"><input type="checkbox" name="checkbox18" value="true"></td>  
                                        <td class="text-center"><input type="checkbox" name="checkbox19" value="true"></td>  
                                        <td class="text-center"><input type="checkbox" name="checkbox20" value="true"></td>  
                                    </tr>
                                    <tr> 
                                        <td class="text-center"><B>เช็คเอาท์</B></td>
                                        <td class="text-center"><input type="checkbox" name="checkbox21" value="true"></td>  
                                        <td class="text-center"><input type="checkbox" name="checkbox22" value="true"></td>  
                                        <td class="text-center"><input type="checkbox" name="checkbox23" value="true"></td>  
                                        <td class="text-center"><input type="checkbox" name="checkbox24" value="true"></td>  
                                    </tr>
                                    <tr> 
                                        <td class="text-center"><B>ตั่งค่าการเข้าถึง</B></td>
                                        <td class="text-center"><input type="checkbox" name="checkbox25" value="true"></td>  
                                        <td class="text-center"><input type="checkbox" name="checkbox26" value="true"></td>  
                                        <td class="text-center"><input type="checkbox" name="checkbox27" value="true"></td>  
                                        <td class="text-center"><input type="checkbox" name="checkbox28" value="true"></td>  
                                    </tr>
                                    <tr> 
                                        <td class="text-center"><B>พระวิปัสสนาจารย์</B></td>
                                        <td class="text-center"><input type="checkbox" name="checkbox29" value="true"></td>  
                                        <td class="text-center"><input type="checkbox" name="checkbox30" value="true"></td>  
                                        <td class="text-center"><input type="checkbox" name="checkbox31" value="true"></td>  
                                        <td class="text-center"><input type="checkbox" name="checkbox32" value="true"></td>  
                                    </tr>
                                    <tr> 
                                        <td class="text-center"><B>บทความ</B></td>
                                        <td class="text-center"><input type="checkbox" name="checkbox33" value="true"></td>  
                                        <td class="text-center"><input type="checkbox" name="checkbox34" value="true"></td>  
                                        <td class="text-center"><input type="checkbox" name="checkbox35" value="true"></td>  
                                        <td class="text-center"><input type="checkbox" name="checkbox36" value="true"></td>  
                                    </tr>
                                    <tr> 
                                        <td class="text-center"><B>รายงาน</B></td>
                                        <td class="text-center"><input type="checkbox" name="checkbox37" value="true"></td>  
                                        <td class="text-center"><input type="checkbox" name="checkbox38" value="true"></td>  
                                        <td class="text-center"><input type="checkbox" name="checkbox39" value="true"></td>  
                                        <td class="text-center"><input type="checkbox" name="checkbox40" value="true"></td>  
                                    </tr>
                        </tbody>
                    </table>
                </div>                                     
                            </div>
                        </div>
                    </div>  

                </form>
            </div>
        </div>

    </div>
</div>
<script>
    function check(a) {
        if (a === 'true') {
            $('#select_all').val(false);
            for (var i = 1; i < 29; i++) {
                document.getElementById("myCheck" + i).checked = true;
            }
        } else {
            $('#select_all').val(true);
            for (var i = 1; i < 29; i++) {
                document.getElementById("myCheck" + i).checked = false;
            }
        }


    }
</script>


<!-- End Content -->


<jsp:include page="../footer.jsp" />
