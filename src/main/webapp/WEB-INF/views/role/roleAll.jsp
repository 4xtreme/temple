<%-- 
    Document   : addmachine
    Created on : Nov 21, 2017, 1:23:44 PM
    Author     : pop
--%>

<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<jsp:include page="../header.jsp" />
<c:if test="${roleadmin == 'admin'}">
    <jsp:include page="../navigation.jsp" />
</c:if>
<c:if test="${roleadmin == 'user'}">
    <jsp:include page="../navigation_top.jsp" />
</c:if>
<!-- Page Content -->
<div id="page-wrapper">
    <div class="container-fluid">
        <!-- start Content -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">การเข้าถึงทั้งหมด</h1>
                                                  <dd  class="form-group text-right">           
          <button type="button" onclick="window.location.href='${pageContext.request.contextPath}/addrole'" class="btn btn-primary btn-sm">
          <span class="glyphicon glyphicon-plus-sign"></span> เพิ่มการเข้าถึง
        </button>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                
                    <div class="tab-content">
                        <div class="tab-pane fade active in"> 
                            <div class="row">
                                <div class="col-lg-3" ></div>
                                <div class="col-lg-6" >
                                    <div class="dl-horizontal">
                                        <form id="findRow" action="role" method="POST">
                                        <div class="form-group">
                                            <span class="label_title" >ชื่อ</span>                                           
                                            <select  name="roleName" id="roleName" class="form-control width_30"  >
                                                <option value="" disabled="" selected="" >กรุณาเลือก</option>   
                                                <c:forEach var="roles" items="${roles}" varStatus="loop">                                   
                                                    <option <c:if test="${roleselect eq roles.id}"> selected="selected" </c:if> value="${roles.id}">${roles.role_name}</option>
                                                </c:forEach>
                                            </select>
                                            &nbsp;&nbsp;
                                            <button type="button" class="btn btn-primary btn-sm" onclick="succesRegis()"><span class="glyphicon glyphicon-search"></span> ค้นหา</button>
                                            &nbsp;&nbsp;

                                        </div>  
                                        </form>   
                                    </div>        
                                    <form id="updateroleitem" action="updateroleitem" method="POST">
                                        <input type="hidden" name="role_id" value="${roleselect}" />
                                    <div class="table-responsive" >
                                        <table class="table table-striped table-bordered table-hover">
                                            <thead>
                                                <tr>
                                                    <th class="text-center"><input type="checkbox" name="checkbox30" id="select_all" onclick="check(this.value);" value="true"> เลือกทั้งหมด</th>
                                                    <th class="text-center">ดูรายละเอียด</th>
                                                    <th class="text-center">เพิ่ม</th>
                                                    <th class="text-center">แก้ไข</th>
                                                    <th class="text-center">ลบ</th>
                                                </tr>
                                            </thead>
                                            
                                            <tbody>
                                                <c:if test="${roleitem != null}">                                               
                                                    <c:forEach var="roleitem" items="${roleitem}" varStatus="loop">
                                                        <tr> 
                                                            <td class="text-center"><B>
                                                                    <c:if test="${roleitem.page eq 'temple_page'}">วัด</c:if>
                                                                    <c:if test="${roleitem.page eq 'room_page'}">อาคาร / กุฏิ</c:if> 
                                                                    <c:if test="${roleitem.page eq 'staff_page'}">เจ้าหน้าที่</c:if> 
                                                                    <c:if test="${roleitem.page eq 'member_page'}">สมาชิก</c:if> 
                                                                    <c:if test="${roleitem.page eq 'checkin'}">เช็คอิน</c:if> 
                                                                    <c:if test="${roleitem.page eq 'checkout'}">เช็คเอาท์</c:if>
                                                                    <c:if test="${roleitem.page eq 'role'}">ตั่งค่าการเข้าถึง</c:if>       
                                                                    <c:if test="${roleitem.page eq 'monk'}">พระวิปัสสนาจารย์</c:if>    
                                                                    <c:if test="${roleitem.page eq 'content'}">บทความ</c:if>    
                                                                    <c:if test="${roleitem.page eq 'report'}">รายงาน</c:if>   
                                                                </B>
                                                            </td>
                                                            <td class="text-center"><input name="v${loop.index}" id="v${loop.index}" <c:if test="${roleitem.view_page eq true}">checked=checked</c:if> type="checkbox" value="true" ></td>  
                                                            <td class="text-center"><input name="c${loop.index}" id="c${loop.index}" <c:if test="${roleitem.create_page eq true}">checked=checked</c:if> type="checkbox" value="true"  ></td>  
                                                            <td class="text-center"><input name="e${loop.index}" id="e${loop.index}" <c:if test="${roleitem.edit_page eq true}">checked=checked</c:if> type="checkbox" value="true"  ></td>  
                                                            <td class="text-center"><input name="d${loop.index}" id="d${loop.index}" <c:if test="${roleitem.delete_page eq true}">checked=checked</c:if> type="checkbox" value="true"  ></td>  
                                                        </tr>
                                                    </c:forEach>
                                                </c:if>  
                                            </tbody>
                                        </table>
                                    </div> 
                                    </form>
                                    <form id="deleteroleitem" action="deleteroleitem" method="POST">
                                       <input type="hidden" name="id" value="${roleselect}" /> 
                                    </form>
                                    <center>
                                    <c:if test="${bt_show != null}">
                                            <button onclick="document.getElementById('updateroleitem').submit();"  class="btn btn-primary btn-sm"><span class="glyphicon glyphicon-floppy-save"></span> บันทึก</button>
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <button onclick="chkConfirm();"  class="btn btn-danger btn-sm"><span  class="glyphicon glyphicon-trash"></span> ลบ</button>
                                    </c:if>   
                                    </center>

                                </div>
                                <div class="col-lg-3" ></div>
                            </div><BR><BR><BR><BR>
                        </div>
                    </div>  
            </div>
        </div>

    </div>
</div>



<!-- End Content -->
<script language="JavaScript">
     function succesRegis() {
         
       var roleName = $("#roleName").val();  
       if(roleName === null){
            alert("กรุณาเลือกการเข้าถึง");
            $("#roleName").focus();
        }else{
            $('#findRow').submit();
        }
     }
     
    function chkConfirm() {
        bootbox.confirm({
            message: "คุณแน่ใจหรือว่าต้องการลบ ?",
            buttons: {
                confirm: {
                    label: 'ยืนยัน',
                    className: 'btn-success'
                },
                cancel: {
                    label: 'ยกเลิก',
                    className: 'btn-danger'
                }
            },
            callback: function (result) {
                if (result) {
                    $("#deleteroleitem" ).submit();
                }
                console.log('This was logged in the callback: ' + result + "#deleteroleitem");
            }
        });
    }
    
    function check(a) {
        if (a === 'true') {
            $('#select_all').val(false);
            for (var i = 0; i < 10; i++) {
                document.getElementById("v" + i).checked = true;
                document.getElementById("c" + i).checked = true;
                document.getElementById("e" + i).checked = true;
                document.getElementById("d" + i).checked = true;
            }
        } else {
            $('#select_all').val(true);
            for (var i = 0; i < 10; i++) {
                document.getElementById("v" + i).checked = false;
                document.getElementById("c" + i).checked = false;
                document.getElementById("e" + i).checked = false;
                document.getElementById("d" + i).checked = false;
            }
        }


    }
    </script>

<jsp:include page="../footer.jsp" />
 
