<%-- 
    Document   : addmachine
    Created on : Nov 21, 2017, 1:23:44 PM
    Author     : pop
--%>

<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<jsp:include page="../header.jsp" />
<c:if test="${roleadmin == 'admin'}">
    <jsp:include page="../navigation.jsp" />
</c:if>
<c:if test="${roleadmin == 'user'}">
    <jsp:include page="../navigation_top.jsp" />
</c:if>
<style>

    /* Always set the map height explicitly to define the size of the div
     * element that contains the map. */
    #map {
        height: 500px;
    }
    /* Optional: Makes the sample page fill the window. */

    #description {
        font-family: Roboto;
        font-size: 15px;
        font-weight: 300;
    }

    #infowindow-content .title {
        font-weight: bold;
    }

    #infowindow-content {
        display: none;
    }

    #map #infowindow-content {
        display: inline;
    }

    .pac-card {
        margin: 10px 10px 0 0;
        border-radius: 2px 0 0 2px;
        box-sizing: border-box;
        -moz-box-sizing: border-box;
        outline: none;
        box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
        background-color: #fff;
        font-family: Roboto;
    }

    #pac-container {
        padding-bottom: 12px;
        margin-right: 12px;
    }

    .pac-controls {
        display: inline-block;
        padding: 5px 11px;
    }

    .pac-controls label {
        font-family: Roboto;
        font-size: 13px;
        font-weight: 300;
    }

    #pac-input {
        background-color: #fff;
        font-family: Roboto;
        font-size: 15px;
        font-weight: 300;
        margin-left: 12px;
        padding: 0 11px 0 13px;
        text-overflow: ellipsis;
        width: 400px;
    }

    #pac-input:focus {
        border-color: #4d90fe;
    }

    #title {
        color: #fff;
        background-color: #4d90fe;
        font-size: 25px;
        font-weight: 500;
        padding: 6px 12px;
    }
    #target {
        width: 345px;
    }
    .row {
        margin-bottom: 10px;
    }

    label {
        margin: 7px 0;
    }
</style>


<!-- Page Content -->
<div id="page-wrapper">
    <div class="container-fluid">
        <!-- start Content -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">แก้ไขวัด</h1>
            </div>
        </div>


        <div class="row">
            <div class="col-lg-12">

                <div class="tab-content">
                    <div class="tab-pane fade active in"> 
                        <div class="row">
                            <div class="dl-horizontal">
                                <form:form id="addtemple" modelAttribute="temple" action="edittemples" method="POST"  enctype="multipart/form-data" class="col-lg-6" >
                                    <fieldset>
                                        <form:hidden name="id" path="id"  />
                                        <div class="col-lg-12" >                                                 
                                            <label for="temple_name"> ชื่อวัด: </label>
                                            <form:errors class="text-danger" path="name"/>
                                            <form:input id="temple_name" type="text" class="form-control" path="name" /> 
                                        </div>
                                        <div class="col-lg-12">
                                            <label for="about">เกี่ยวกับวัด:</label>
                                            <form:errors class="text-danger" path="about"/>  
                                            <form:textarea id="about" type="text" class="form-control"  path="about" />
                                        </div>
                                        <div class="col-lg-6">
                                            <label for="address">ที่อยู่:</label>
                                            <form:errors class="text-danger" path="address"/>  
                                            <form:input id="address" type="text"  class="form-control"  path="address" />
                                        </div>
                                        <div class="col-lg-6">
                                            <label for="district">ตำบล:</label>
                                            <form:errors class="text-danger" path="district"/>  
                                            <form:input id="district" type="text"  class="form-control"  path="district" />
                                        </div>
                                        <div class="col-lg-6">
                                            <label for="prefecture">อำเภอ:</label>
                                            <form:errors class="text-danger" path="prefecture"/>  
                                            <form:input id="prefecture" type="text" class="form-control"  path="prefecture" />
                                        </div>
                                        <div class="col-lg-6">
                                            <label for="province">จังหวัด:</label>
                                            <form:errors class="text-danger" path="province"/>  
                                            <form:input id="province" type="text" class="form-control"  path="province" />
                                        </div>
                                        <div class="col-lg-6">
                                            <label for="postal_code">รหัสไปษณีย์:</label>
                                            <form:errors class="text-danger" path="postal_code"/> 
                                            <form:input id="postal_code" type="text" class="form-control" maxlength="5" path="postal_code" />
                                        </div>
                                        <div class="col-lg-6">
                                            <label for="tel">เบอร์โทร:</label>
                                            <form:errors class="text-danger" path="tel"/>
                                            <form:input id="tel" type="text" class="form-control" maxlength="10"  path="tel" />

                                        </div>
                                        <div class="col-lg-6">
                                            <label for="lat">latitude:</label>
                                            <form:errors class="text-danger" path="latitude"/>
                                            <form:input id="lat" type="text" class="form-control"  path="latitude"  /> 

                                        </div>
                                        <div class="col-lg-6">
                                            <label for="long">longitude:</label>
                                            <form:errors class="text-danger" path="longitude"/>
                                            <form:input id="long" type="text" class="form-control" path="longitude"  /> 
                                        </div>
                                    </fieldset>
                                </form:form> 

                            </div>
                            <div class="col-lg-6">
                                <div class="row">
                                    <input id="pac-input" class="controls" type="text" placeholder="Search Box"/>
                                    <div id="map" ></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row" style="text-align: center">
                    <div class="col-lg-12">
                        <button type="button" onclick="succesRegis()" class="btn  button-size-sucess"style="margin-left: 20px;margin-right: 20px">บันทึก</button>
                    </div>
                </div>    

            </div>
        </div>
    </div>
</div>



<!-- End Content -->

<script>
    // This example adds a search box to a map, using the Google Place Autocomplete
    // feature. People can enter geographical searches. The search box will return a
    // pick list containing a mix of places and predicted search terms.

    // This example requires the Places library. Include the libraries=places
    // parameter when you first load the API. For example:
    // <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">


    function initAutocomplete() {
        var lat = parseFloat($("#lat").val());
        var lng = parseFloat($("#long").val());
        var map = new google.maps.Map(document.getElementById('map'), {
            center: {lat: lat, lng: lng},
            zoom: 22,
            mapTypeId: 'roadmap'
        });



        // Create the search box and link it to the UI element.
        var input = document.getElementById('pac-input');
        var searchBox = new google.maps.places.SearchBox(input);

        map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

        // Bias the SearchBox results towards current map's viewport.
        map.addListener('bounds_changed', function () {
            searchBox.setBounds(map.getBounds());
        });

        var markers = [];
        markers.push(new google.maps.Marker({
            position: {lat: lat, lng: lng},
            map: map,
            title: $('#temple_name').val()
        }));
        // Listen for the event fired when the user selects a prediction and retrieve
        // more details for that place.
        searchBox.addListener('places_changed', function () {
            var places = searchBox.getPlaces();

            if (places.length == 0) {
                return;
            }

            // Clear out the old markers.
            markers.forEach(function (marker) {
                marker.setMap(null);
            });
            markers = [];

            // For each place, get the icon, name and location.
            var bounds = new google.maps.LatLngBounds();
            places.forEach(function (place) {
                if (!place.geometry) {
                    console.log("Returned place contains no geometry");
                    return;
                }
                // getAddress(place.formatted_address);
                $("#lat").val(place.geometry.location.lat());
                $("#long").val(place.geometry.location.lng());
                $("#temple_name").val(place.name);
                $("#address").val(place.formatted_address);
                // Create a marker for each place.
                markers.push(new google.maps.Marker({
                    map: map,
                    title: place.name,
                    position: place.geometry.location
                }));

                if (place.geometry.viewport) {
                    // Only geocodes have viewport.
                    bounds.union(place.geometry.viewport);
                } else {
                    bounds.extend(place.geometry.location);
                }
            });
            map.fitBounds(bounds);
        });
    }

    function getAddress(raw_address) {
        var array_address = raw_address.split(" ");
        var address = "";
        var tambon;
        var amphoe;
        var province;
        var postal_code;
        alert(array_address);
        for (i = 0; i < array_address.length; i++) {
            address += array_address[i] + " ";
        }
        alert(address);
    }



    function succesRegis() {
        $("#addtemple").submit();
    }
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC7ROIXTKM7N0d6fNNn110jVyEbLvq5-rI&libraries=places&callback=initAutocomplete&language=th&region=TH"
async defer></script>

<jsp:include page="../footer.jsp" />
