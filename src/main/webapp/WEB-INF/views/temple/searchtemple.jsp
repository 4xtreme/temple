<%-- 
    Document   : withdrawlist
    Created on : Dec 1, 2017, 8:20:20 AM
    Author     : pop
--%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:include page="../header.jsp" />
<c:if test="${roleadmin == 'admin'}">
    <jsp:include page="../navigation.jsp" />
</c:if>
<c:if test="${roleadmin == 'user'}">
    <jsp:include page="../navigation_top.jsp" />
</c:if>

<div id="page-wrapper">
    <div class="container-fluid">
        <!-- start Content -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">รายการวัด</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-2">
                <select class="form-control" id="selectBox" onchange="showDiv(this)">
                    <option disabled selected>เลือก</option>
                    <option value="selectname">ชื่อวัด</option>
                    <option value="selectprovince">จังหวัด</option>
                </select>
            </div>
            <div class="col-lg-8" id="space"></div>
            <div class="col-lg-8" style="display:none" id="textboxes">
                <form method="POST" action="searchtemple" > 
                    <input class="form-control width_20" type="text" name="search">
                    <button type="submit" class="btn btn-primary btn-signup width_10 align-center">
                        <i class="glyphicon glyphicon-search"></i> ค้นหา
                    </button>
                </form>
            </div>
            <div class="col-lg-8" style="display:none" id="optionboxes">
                <form method="POST" action="searchtemple"> 
                    <select class="form-control width_20" id="selectBox" name="search">
                        <option disabled selected>เลือกจังหวัด</option>
                        <c:forEach var="province" items="${province}" varStatus="loop">
                            <option value="${province.province}">${province.province}</option>
                        </c:forEach>
                    </select>
                    <button type="submit" class="btn btn-primary btn-signup width_10 align-center">
                        <i class="glyphicon glyphicon-search"></i> ค้นหา
                    </button>
                </form>
            </div>
            <c:if test="${page_Create != null}">
                <div class="col-lg-2">
                    <a href="addtemple" style="float: right" class="btn btn-primary" >เพิ่มวัด</a>
                </div>
            </c:if> 
        </div>
        <br>
        <div class="row">
            <div class="col-lg-12">
                <div class="table-responsive" >
                    <table class="table table-striped table-bordered table-hover">
                        <thead>
                            <tr>
                                <th class="text-center">รหัส</th>
                                <th class="text-center">ชื่อวัด </th>
                                <th class="text-center">จังหวัด</th>
                                <th class="text-center">เบอร์โทร</th>  
                                <th class="text-center">แก้ไข</th>
                                <th class="text-center">ลบ</th>
                            </tr>
                        </thead>
                        <tbody>
                            <c:if test="${temples != null}">  
                                <c:forEach var="temples" items="${temples}" varStatus="loop">
                                    <tr>
                                        <td class="text-center">${temples.id}</td>
                                        <td class="text-center">${temples.name}</td>
                                        <td class="text-center">${temples.province}</td>
                                        <td class="text-center">${temples.tel}</td>
                                        <td class="text-center">
                                            <c:if test="${page_Edit != null}">
                                                <form  action="edittemple" method="post" >    
                                                    <input type="hidden" value="${temples.id}" name="id"/>
                                                    <button type="submit"  class="no-border tran-bg"><i class="fa fa-edit edit-btn" style="font-size: 30px"></i></button>
                                                </form>
                                            </c:if>
                                        </td>
                                        <td class="text-center">
                                            <c:if test="${page_Delete != null}">
                                                <form id="delete_${temples.id}"  action="deletetemple" method="post" >    
                                                    <input type="hidden" value="${temples.id}" name="id"/>
                                                    <a  onclick="chkConfirm(${temples.id});" ><i style="color: red;" class="fa fa-trash fa-2x" aria-hidden="true"></i></a>
                                                </form> 
                                            </c:if>
                                        </td>
                                    </tr>
                                </c:forEach>
                            </c:if>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div><!-- End Content -->    
</div><!-- End Page Content -->
<script type="text/javascript">
    function chkConfirm(rowid) {
        bootbox.confirm({
            message: "คุณแน่ใจหรือว่าต้องการลบ ?",
            buttons: {
                confirm: {
                    label: 'ยืนยัน',
                    className: 'btn-success'
                },
                cancel: {
                    label: 'ยกเลิก',
                    className: 'btn-danger'
                }
            },
            callback: function (result) {
                if (result) {
                    $("#delete_" + rowid).submit();
                }
                console.log('This was logged in the callback: ' + result + "#delete_" + rowid);
            }
        });
    }

    function showDiv(select) {
        if (select.value === "selectname") {
            document.getElementById('textboxes').style.display = "block";
            document.getElementById('optionboxes').style.display = "none";
            document.getElementById('space').style.display = "none";
        } else {
            document.getElementById('textboxes').style.display = "none";
            document.getElementById('optionboxes').style.display = "block";
            document.getElementById('space').style.display = "none";
        }
    }
</script>
<jsp:include page="../footer.jsp" />
