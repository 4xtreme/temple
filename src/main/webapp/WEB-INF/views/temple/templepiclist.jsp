<%-- 
    Document   : showpicture
    Created on : Apr 18, 2018, 9:34:49 AM
    Author     : she my sunshine
--%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<jsp:include page="../header.jsp" />
<c:if test="${roleadmin == 'admin'}">
    <jsp:include page="../navigation.jsp" />
</c:if>
<c:if test="${roleadmin == 'user'}">
    <jsp:include page="../navigation_top.jsp" />
</c:if>

<html>
    <div id="page-wrapper">
    <div class="container-fluid">
        <!-- start Content -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">รายการรูปภาพวัด</h1>
            </div>
            <div>
                
                <c:if test="${page_Create != null}">
                <dd  class="form-group text-right">
                    <form method="get" action="addpicture">
                 <button class="btn btn-primary" type="submit">เพิ่มรูปภาพวัด</button>
                    </form> 
                 </c:if>   
                    
                   
                
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="table-responsive" >
                    <table class="table table-striped table-bordered table-hover">
                        <thead>
                            <tr>
                                <th class="text-center">#</th>
                                <th class="text-center">ภาพ </th>
                                <th class="text-center">รายละเอียด</th>   
                                <th class="text-center">ลบ</th>
                         
                            </tr>
                        </thead>
                        <tbody>
                            <c:if test="${templepics != null}">  
                                
                                <c:forEach var="templepics" items="${templepics}" varStatus="loop">
                                    <tr>
                                        <td class="text-center">${templepics.id}</td>
                                        <td class="text-center"><img alt="{templepics.filename}"
                                                                src="showpicture/${templepics.id}"width="150" height="150"></td>
                                        <td class="text-center">${templepics.description}</td>
                                       
                                        <td class="text-center">
                                            <c:if test="${page_Delete != null}">
                                            <form id="delete_${templepics.id}"  action="deletetemplepic" method="post" >    
                                                <input type="hidden" value="${templepics.id}" name="id"/>
                                                <a  onclick="chkConfirm(${templepics.id});" ><i style="color: red;" class="fa fa-trash fa-2x" aria-hidden="true"></i></a>

                                            </form>
                                            </c:if>
                                        </td>
                                    </tr>
                                </c:forEach>
                            </c:if>
                        </tbody>
                    </table>
                </div>
                <div class="row">
                    <center>
                        <div class="pagination_center">
                            <nav aria-label="Page navigation col-centered">
                                <ul class="pagination">
                                    <c:if test="${page_priv == true}">  
                                        <li class="page-item"><a class="page-link"  href="${pageContext.request.contextPath}/templepiclist?page=${page-1}"><i class="fa fa-angle-left  "></i></a></li>
                                            </c:if>
                                            <c:if test="${page_count != 0}">
                                                <c:forEach begin="0" end="${page_count-1}" varStatus="loop">
                                                    <c:if test="${page == loop.index}"> 
                                                <li class="page-item"><a class="page-link active"  href="${pageContext.request.contextPath}/templepiclist?page=${loop.index}">${loop.index+1}</a></li>
                                                </c:if> 
                                                <c:if test="${page != loop.index}"> 
                                                <li class="page-item"><a class="page-link"  href="${pageContext.request.contextPath}/templepiclist?page=${loop.index}">${loop.index+1}</a></li>
                                                </c:if> 
                                            </c:forEach>
                                        </c:if>
                                        <c:if test="${page_next == true}"> 
                                        <li class="page-item"><a class="page-link" href="${pageContext.request.contextPath}/templepiclist?page=${page+1}"><i class="fa fa-angle-right "></i></a></li>
                                            </c:if>
                                </ul>
                            </nav>
                        </div>
                        </center>
                    <br><br><br><br>
                </div><!--end row-->
            </div>
        </div>
    </div><!-- End Content -->    
</div><!-- End Page Content -->
<script <script language="JavaScript">
    function chkConfirm(rowid) {
        bootbox.confirm({
            message: "คุณแน่ใจหรือว่าต้องการลบ ?",
            buttons: {
                confirm: {
                    label: 'ยืนยัน',
                    className: 'btn-success'
                },
                cancel: {
                    label: 'ยกเลิก',
                    className: 'btn-danger'
                }
            },
            callback: function (result) {
                if (result) {
                    $("#delete_" + rowid).submit();
                }
                console.log('This was logged in the callback: ' + result + "#delete_" + rowid);
            }
        });
    }
    </script>
</script>
<jsp:include page="../footer.jsp" />
</html>
