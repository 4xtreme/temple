<%-- 
    Document   : upload
    Created on : Apr 17, 2018, 8:38:31 AM
    Author     : she my sunshine
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>



<jsp:include page="../header.jsp" />
<c:if test="${roleadmin == 'admin'}">
    <jsp:include page="../navigation.jsp" />
</c:if>
<c:if test="${roleadmin == 'user'}">
    <jsp:include page="../navigation_top.jsp" />
</c:if>
<!-- Page Content -->
<div id="page-wrapper">
    <div class="container-fluid">
        <!-- start Content -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">เพิ่มรูปภาพวัด</h1>
            </div>
        </div>


        <fieldset>
            <div class="row">
                <div class="col-lg-12">

                    <div class="tab-content">
                        <div class="tab-pane fade active in"> 
                            <div class="row">
                                <div class="col-lg-12" >
                                    <form id="addpicture" modelAttribute="temple" action="uploadfile" method="POST"  enctype="multipart/form-data">

                                        <div class="row">
                                            <div class="col-lg-12">
                                                <center>
                                                    <input id="templepic" name="templepic" type="file" path="templepic"></br>
                                                    <span class="label_title"><label>คำอธิบาย</label></span>
                                                    <input id="description" class="form-control"name="description" type="text" path="description" style="width: 20%">


                                                    </br></br></br></br></br></br></br></br>
                                                    <div class="form-group text-center">
                                                    <FORM>
                                    <INPUT class="btn button-size-cancel"TYPE="button" VALUE="ยกเลิก" onClick="history.back()">
                                </FORM>
                                                  <button type="button" class="btn  button-size-sucess" onclick="succesRegis()"> <span class="fa fa-save "></span> บันทึก</button></div>

                                                </center>
                                            </div>
                                        </div> 
                                    </form>    
                                </div>

                            </div>
                        </div>

                    </div>

                    </br>
                    </br>
                    </br>          



                    </br>
                    </br>
                    </br>

                </div>
            </div>
        </fieldset>







    </div>
</div><!-- End Content -->
<script>
    function succesRegis() {
        var templepic = $("#templepic").val();
        var description = $("#description").val();
        if (templepic == '') {
            alert('กรุณาเพิ่มรูปภาพ');
        } else if (description == '') {
            alert('กรุณาเพิ่มคำอธิบาย');
        } else {
            $("#addpicture").submit();
        }
    }

</script>




<jsp:include page="../footer.jsp" />




