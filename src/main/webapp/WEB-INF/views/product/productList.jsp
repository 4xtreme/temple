<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:include page="../header.jsp" />
<jsp:include page="../navigation.jsp" />
 <!-- Page Content -->
    <div id="page-wrapper">
    <div class="container-fluid">
        <!-- start Content -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Product</h1>
            </div>
        </div>
        
        <div class="row">
            <div class="col-lg-12">
                <div class="table-responsive">
                                    	<table class="table table-striped table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                <th class="text-center">Product ID</th>
                                                <th class="text-center">Product Name	</th>
                                                <th class="text-center">Detail</th>
                                                <th class="text-center">Size</th>
                                                <th class="text-center">Pirce</th>
                                                <th class="text-center">Stock</th>
                                                <th class="text-center">Edit</th>
                                            </tr>
                                        </thead>
                                        
                                        <tbody>
                                    <c:if test="${productgetlist != null}">  
                                    <c:forEach var="productgetlists" items="${productgetlist}" varStatus="loop">
                                    <tr>
                                                <td class="text-center">${productgetlists.product_id}</td>
                                                <td class="text-center">${productgetlists.product_name}</td>
                                                <td class="text-center">${productgetlists.detail}</td>
                                                <td class="text-center">${productgetlists.size}</td>
                                                <td class="text-center">${productgetlists.pirce}</td>
                                                <td class="text-center">${productgetlists.stock}</td>
                                                <td class="text-center">                                                
                                                         <form  action="selectedit" method="post" >
                                                            <input type="hidden" value="${productgetlists.product_id}" name="editid"/>
                                                            <button type="submit" class="btn btn-warning btn-circle " ><i class="fa fa-edit"></i></button>
                                                        </form>
                                                
                                                </td>
                                            </tr>  
                                     </c:forEach>
                                            
                                
                                 </c:if>
                                        </tbody>
                                                                           
                                    </table>
                </div>
            </div>
        </div>
               


    </div>


    <!-- End Content -->
</div>
 <jsp:include page="../footer.jsp" />