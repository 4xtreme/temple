<%-- 
    Document   : addmachine
    Created on : Nov 21, 2017, 1:23:44 PM
    Author     : pop
--%>

<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<jsp:include page="../header.jsp" />
<c:if test="${roleadmin == 'admin'}">
    <jsp:include page="../navigation.jsp" />
</c:if>
<c:if test="${roleadmin == 'user'}">
    <jsp:include page="../navigation_top.jsp" />
</c:if>
<!-- Page Content -->
<div id="page-wrapper">
    <div class="container-fluid">
        <!-- start Content -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">ดูรายละเอียดเจ้าหน้าที่</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12"> 
                <div class="tab-content">
                    <div class="tab-pane fade active in">
        
                               <input type="hidden" value="${staff.id}" name="id"/>
                                    <div class="col-lg-12" >
                                        <div class="col-lg-2" ></div>
                                        <div class="col-lg-4" >
                                            <dl class="dl-horizontal" style="font-size: 20px">
                                                <c:if test="${invalid != null}"> <div style="color: red;">อีเมลไม่ถูกต้องหรืออีเมลซ้ำ </div></c:if>
                                                <dt>ชื่อ</dt> 
                                                <dd> ${staff.firstname}</dd>

                                                <dt>นามสกุล</dt> 
                                                <dd> ${staff.lastname}</dd>

                                                <dt>อีเมล์</dt> 
                                               <dd> ${staff.email}</dd>

                                                    <dt>เบอร์โทรศัพท์</dt> 
                                                <dd> ${staff.phone1}</dd>
                                                

                                                <dt>ตั้งค่าการเข้าถึง</dt> 
                                                <dd>
                                                        <c:forEach var="roles" items="${roles}" varStatus="loop">
                                                        <c:if test="${roleSelect eq roles.id}"> ${roles.role_name}</c:if> 
                                                    </c:forEach>
                                             
                                                </dd>

                                                <dt>วัด</dt> 
                                                <dd>
                                                 <c:forEach var="temple" items="${temple}" varStatus="loop">
                                                        <c:if test="${Temple_id eq temple.id}">
                                                        ${temple.name}
                                                        </c:if> 
                                                    </c:forEach>

                                                </dd>

                                            </dl>
                              
                                                </div>
                                                 <div class="col-lg-4" ></div>
                                                <div class="col-lg-2" ></div>
                                    </div>
                                                 
                                            
                                  </div>
                           
                                </div>
                                </br>
                                </br>
                                </br>          
                      
                    </div>
                </div>
     
            </div>
        </div>
   


    <!-- End Content -->

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js" ></script>  
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/uikit/3.0.0-beta.20/js/uikit.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/img/bootstrap-imageupload.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/img/bootstrap-imageupload.js"></script>

<jsp:include page="../footer.jsp" />
