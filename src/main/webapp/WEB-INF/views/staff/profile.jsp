<%-- 
    Document   : addmachine
    Created on : Nov 21, 2017, 1:23:44 PM
    Author     : pop
--%>

<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<jsp:include page="../header.jsp" />
<c:if test="${roleadmin == 'admin'}">
    <jsp:include page="../navigation.jsp" />
</c:if>
<c:if test="${roleadmin == 'user'}">
    <jsp:include page="../navigation_top.jsp" />
</c:if>
<!-- Page Content -->
<div id="page-wrapper">
    <div class="container-fluid">
        <!-- start Content -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">ข้อมูลผู้ใช้</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12"> <input type="hidden" value="${staff.id}" name="id"/></div>
                <form:form id="addstaff" modelAttribute="staff" action="editstaffs" method="POST"  enctype="multipart/form-data" >
                <fieldset>
                    <div class="tab-content">
                        <div class="tab-pane fade active in">
                            <input type="hidden" value="${staff.id}" name="id"/>
                            <div class="col-lg-12" >
                                <div class="row">
                                    <div class="col-lg-3" ></div>
                                    <div class="col-lg-3" >
                                        <div class="dl-horizontal">

                                            <div class="tab-content">                                                 
                                                <span class="label_title" style="font-weight: bold;"> ชื่อ  :</span>&nbsp;&nbsp;&nbsp;&nbsp;
                                                ${staff.firstname}
                                            </div>

                                            <br><br>
                                            <div class="tab-content">                                                 
                                                <span class="label_title" style="font-weight: bold;">email :</span>
                                                ${staff.email}
                                            </div>


                                        </div>
                                    </div>
                                    <div class="col-lg-3" >
                                        <div class="dl-horizontal">

                                            <div class="tab-content">    
                                                <span class="label_title" style="font-weight: bold;"> นามสกุล :</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                ${staff.lastname}
                                            </div>

                                            <br><br>
                                            <div class="tab-content">
                                                <span class="label_title" style="font-weight: bold;"> เบอร์โทรศัพท์ :</span>
                                                ${staff.phone1}
                                            </div>


                                        </div>
                                    </div>

                                </div></div>


                        </div>

                    </div>
                    </br>
                    </br>
                    </br>          


                </fieldset>
            </form:form> 

        </div>
    </div>
</div>



<!-- End Content -->

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js" ></script>  
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/uikit/3.0.0-beta.20/js/uikit.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/img/bootstrap-imageupload.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/img/bootstrap-imageupload.js"></script>

<jsp:include page="../footer.jsp" />
