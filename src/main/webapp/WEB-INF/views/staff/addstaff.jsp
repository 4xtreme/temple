

<%-- 
    Document   : addmachine
    Created on : Nov 21, 2017, 1:23:44 PM
    Author     : pop
--%>

<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:include page="../header.jsp" />
<c:if test="${roleadmin == 'admin'}">
    <jsp:include page="../navigation.jsp" />
</c:if>
<c:if test="${roleadmin == 'user'}">
    <jsp:include page="../navigation_top.jsp" />
</c:if>
<style>
    .dl-horizontal input{
        width: 40%;
        position: relative;
        display: inline-block;
    }

    .dl-horizontal select{
        width: 40%;
        position: relative;
        display: inline-block;
    }

    .dl-horizontal .text-danger{
        font-size: 16px;
        margin-left: 10px;
    }
    .dl-horizontal dt,dd{
        margin-top: 5px;
    }
</style>
<!-- Page Content -->
<div id="page-wrapper">
    <div class="container-fluid">
        <!-- start Content -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">เพิ่มเจ้าหน้าที่</h1>
            </div>
        </div>
        <form:form id="addstaff" modelAttribute="staff" action="addstaff" method="POST"  enctype="multipart/form-data" >
            <fieldset>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="tab-content">
                            <div class="tab-pane fade active in"> 
                                <div class="row">
                                    <div class="col-lg-12" >
                                        <div class="col-lg-2" ></div>
                                        <div class="col-lg-8" >
                                            <dl class="dl-horizontal" style="font-size: 20px">
                                                <c:if test="${invalid != null}"> 
                                                    <div style="color: red;">อีเมลไม่ถูกต้องหรืออีเมลซ้ำ </div>
                                                </c:if>
                                                <dt>ชื่อ</dt> 
                                                <dd>
                                                    <form:input type="text"  class="form-control" path="firstname" /><form:errors class="text-danger" path="firstname"/>
                                                </dd>

                                                <dt>นามสกุล</dt> 
                                                <dd>
                                                    <form:input type="text"  class="form-control" path="lastname" /><form:errors class="text-danger" path="lastname"/>
                                                </dd>

                                                <dt>อีเมล์</dt> 
                                                <dd>
                                                    <form:input type="text"  class="form-control" pattern="[^@]+@[^@]+\.[a-zA-Z]{2,6}"  path="email" /><form:errors class="text-danger" path="email"/>
                                                </dd>

                                                <dt>เบอร์โทรศัพท์</dt> 
                                                <dd>
                                                    <form:input type="text"  class="form-control"  path="phone1" maxlength="10" /><form:errors class="text-danger" path="phone1"/>
                                                </dd>

                                                <dt>รหัสผ่าน</dt> 
                                                <dd>
                                                    <form:input type="password"  class="form-control" path="password" /><form:errors class="text-danger" path="password"/>
                                                </dd>

                                                <dt>ตั้งค่าการเข้าถึง</dt> 
                                                <dd>
                                                    <form:select path="role_id" class="form-control">
                                                        <c:forEach var="roles" items="${roles}" varStatus="loop">
                                                        <option value="${roles.id}">
                                                            ${roles.role_name}
                                                        </option>
                                                    </c:forEach>
                                                </form:select>
                                                </dd>

                                                <dt>วัด</dt> 
                                                <dd>
                                                    <form:select path="temple_id" class="form-control">

                                                        <c:forEach var="temple" items="${temple}" varStatus="loop">
                                                        <option value="${temple.id}">
                                                            ${temple.name}
                                                        </option>
                                                    </c:forEach>
                                                </form:select><form:errors path="temple_id" /> 

                                                </dd>
                                            </dl>
                                            <center>
                                                <button onclick="document.getElementById('addstaff').submit();" class="btn btn-primary btn-sm"> <span class="glyphicon glyphicon-floppy-save"></span>  บันทึก </button>                                            
                                            </center>
                                        </div>
                                        <!--<div class="col-lg-4" ></div>-->
                                        <div class="col-lg-2" ></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        </br>
                        </br>
                        </br>                       
                    </div>
                </div>
            </fieldset>
        </form:form> 
    </div>
</div>
<!-- End Content -->
<jsp:include page="../footer.jsp" />
