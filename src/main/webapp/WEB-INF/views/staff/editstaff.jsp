<%-- 
    Document   : addmachine
    Created on : Nov 21, 2017, 1:23:44 PM
    Author     : pop
--%>

<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<jsp:include page="../header.jsp" />
<c:if test="${roleadmin == 'admin'}">
    <jsp:include page="../navigation.jsp" />
</c:if>
<c:if test="${roleadmin == 'user'}">
    <jsp:include page="../navigation_top.jsp" />
</c:if>
<!-- Page Content -->
<div id="page-wrapper">
    <div class="container-fluid">
        <!-- start Content -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">แก้ไขเจ้าหน้าที่</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12"> <input type="hidden" value="${staff.id}" name="id"/>
                <form:form id="addstaff" modelAttribute="staff" action="editstaffs" method="POST"  enctype="multipart/form-data" >
                    <fieldset>
                        <div class="tab-content">
                            <div class="tab-pane fade active in">

                                <input type="hidden" value="${staff.id}" name="id"/>
                                <div class="col-lg-12" >
                                    <div class="col-lg-2" ></div>
                                    <div class="col-lg-4" >
                                        <dl class="dl-horizontal" style="font-size: 20px">
                                            <c:if test="${invalid != null}"> 
                                                <div style="color: red;">อีเมลไม่ถูกต้องหรืออีเมลซ้ำ </div>
                                            </c:if>
                                            <dt>ชื่อ</dt> 
                                            <dd><form:errors class="text-danger" path="firstname"/><form:input type="text"  class="form-control" path="firstname" /></dd>

                                            <dt>นามสกุล</dt> 
                                            <dd><form:errors class="text-danger" path="lastname"/><form:input type="text"  class="form-control" path="lastname" /></dd>

                                            <dt>อีเมล์</dt> 
                                            <dd>
                                                <form:errors class="text-danger" path="email"/><form:input type="text"  class="form-control" pattern="[^@]+@[^@]+\.[a-zA-Z]{2,6}"  path="email" />
                                            </dd>

                                            <dt>เบอร์โทรศัพท์</dt> 
                                            <dd><form:errors class="text-danger" path="phone1"/><form:input type="text"  class="form-control"  path="phone1" maxlength="10" /></dd>

                                            <dt>รหัสผ่าน</dt> 
                                            <dd><form:errors class="text-danger" path="password"/><form:input type="password"  class="form-control" path="password" /></dd>

                                            <dt>ตั้งค่าการเข้าถึง</dt> 
                                            <dd>
                                                <form:select path="role_id" class="form-control">
                                                    <c:forEach var="roles" items="${roles}" varStatus="loop">
                                                    <option <c:if test="${roleSelect eq roles.id}"> selected="selected" </c:if> value="${roles.id}">
                                                        ${roles.role_name}
                                                    </option>
                                                </c:forEach>
                                            </form:select>
                                            </dd>

                                            <dt>วัด</dt> 
                                            <dd>
                                                <form:select path="temple_id" class="form-control">

                                                    <c:forEach var="temple" items="${temple}" varStatus="loop">
                                                    <option <c:if test="${Temple_id eq temple.id}"> selected="selected" </c:if> value="${temple.id}">${temple.name}</option>
                                                </c:forEach>
                                            </form:select><form:errors path="temple_id" /> 
                                            </dd>
                                        </dl>
                                        <center>                                           
                                            <div class="form-group text-center"><button type="button" class="btn btn-primary" onclick="succesRegis()"> <span class="fa fa-save "></span> บันทึก</button></div>
                                        </center>
                                    </div>
                                    <div class="col-lg-4" ></div>
                                    <div class="col-lg-2" ></div>
                                </div>
                            </div>
                        </div>
                        </br>
                        </br>
                        </br>         
                    </fieldset>
                </form:form> 
            </div>
        </div>
    </div>
</div>



<!-- End Content -->

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js" ></script>  
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/uikit/3.0.0-beta.20/js/uikit.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/img/bootstrap-imageupload.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/img/bootstrap-imageupload.js"></script>
<script language="JavaScript">
                                                // for more info check README.md
                                                $.Thailand({
                                                    database: '${pageContext.request.contextPath}/css/jquery.Thailand.js/database/db.json', // ฐานข้อมูลเป็นไฟล์ zip
                                                    $district: $('#district'), // input ของตำบล
                                                    $amphoe: $('#amphoe'), // input ของอำเภอ
                                                    $province: $('#province'), // input ของจังหวัด
                                                    $zipcode: $('#zipcode'), // input ของรหัสไปรษณีย์

                                                    onDataFill: function (data) {
                                                        console.info('Data Filled', data);
                                                    }

                                                });
                                                $.Thailand({
                                                    database: '${pageContext.request.contextPath}/css/jquery.Thailand.js/database/db.json', // path หรือ url ไปยัง database
                                                    $search: $('#search'), // input ของช่องค้นหา
                                                    onDataFill: function (data) { // callback เมื่อเกิดการ auto complete ขึ้น
                                                        console.log(data);
                                                    }
                                                });



                                                function succesRegis() {

                                                    $("#addstaff").submit();

                                                }


</script>

<jsp:include page="../footer.jsp" />
