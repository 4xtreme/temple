<%-- 
    Document   : addmachine
    Created on : Nov 21, 2017, 1:23:44 PM
    Author     : pop
--%>

<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<jsp:include page="../header.jsp" />
<jsp:include page="../navigation.jsp" />
<!-- Page Content -->
<style>
    .row {
        margin-bottom: 10px;
    }

    label {
        margin: 7px 0;
    }
</style>
<div id="page-wrapper">
    <div class="container-fluid">
        <!-- start Content -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">ตั้งค่าผู้ใช้</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12"> <input type="hidden" value="${staff.id}" name="id"/>
                <form:form id="addstaff" modelAttribute="staff" action="saveprofile" method="POST"  enctype="multipart/form-data" >
                    <fieldset>
                        <div class="tab-content">
                            <div class="tab-pane fade active in">
                                <form:hidden value="${staff.id}" path="id"/>
                                <div class="col-lg-12" >
                                    <div class="col-lg-2" ></div>
                                    <div class="col-lg-8" >
                                        <div class="dl-horizontal">
                                            <div class="form-group">   
                                                <div class="col-lg-6">
                                                    <label > ชื่อ : </label>
                                                    <form:errors class="text-danger" path="firstname"/>
                                                    <form:input id="firstname" type="text" class="form-control" path="firstname" />
                                                </div>
                                                <div class="col-lg-6">
                                                    <label > นามสกุล:</label>
                                                    <form:errors path="lastname" class="text-danger"/>
                                                    <form:input id="lastname" type="text" class="form-control" path="lastname" />
                                                </div>
                                            </div>
                                            <div class="form-group">             
                                                <div class="col-lg-6">
                                                    <label >email:</label>
                                                    <form:errors path="email" class="text-danger"/>
                                                    <form:input id="email" type="email" class="form-control " title="email" pattern="[^@]+@[^@]+\.[a-zA-Z]{2,6}"  path="email" />
                                                </div>
                                                <div class="col-lg-6">
                                                    <label > เบอร์โทรศัพท์:</label>
                                                    <form:errors path="phone1" class="text-danger"/>
                                                    <form:input id="phone1" type="text" class="form-control "  path="phone1" maxlength="10"/>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-lg-6">
                                                    <label >รหัสผ่าน:</label>    
                                                    <form:errors path="password" class="text-danger"/>
                                                    <form:input id="password" type="password" class="form-control "  path="password" />
                                                </div>
                                                <form:select path="role_id" class="form-control width_20" hidden="true" >                                    
                                                    <c:forEach var="roles" items="${roles}" varStatus="loop">

                                                        <option <c:if test="${roleSelect eq roles.id}"> selected="selected" </c:if>  value="${roles.id}">${roles.role_name}</option>
                                                    </c:forEach>
                                                </form:select><form:errors path="role_id" />                                                                                             
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-2" ></div>
                                </div>
                            </div>
                        </div>
                        </br>
                        </br>
                        </br>          
                    </fieldset>
                </form:form> 
                </br>
                </br>
                </br>
                <div class="row">
                    <div class="col-lg-12">
                        <center>
                            <div class="form-group text-center"><button type="submit" class="btn btn-primary" onclick="succesRegis()"> <span class="fa fa-save "></span> บันทึก</button></div>
                        </center>
                    </div>
                </div>  
            </div>
        </div>
    </div>
</div>



<!-- End Content -->

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js" ></script>  
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/uikit/3.0.0-beta.20/js/uikit.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/img/bootstrap-imageupload.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/img/bootstrap-imageupload.js"></script>



<script>
                                function succesRegis() {
                                    $("#addstaff").submit();
                                }

</script>
<jsp:include page="../footer.jsp" />
