<%-- 
    Document   : withdrawlist
    Created on : Dec 1, 2017, 8:20:20 AM
    Author     : pop
--%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:include page="../header.jsp" />
<c:if test="${roleadmin == 'admin'}">
    <jsp:include page="../navigation.jsp" />
</c:if>
<c:if test="${roleadmin == 'user'}">
    <jsp:include page="../navigation_top.jsp" />
</c:if>

<div id="page-wrapper">
    <div class="container-fluid">
        <!-- start Content -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">รายชื่อเจ้าหน้าที่</h1>
            </div>
            <div>

                <c:if test="${page_Create != null}">
                    <dd  class="form-group text-right">
                        <a href="addstaff" class="btn btn-primary btn-sm" >เพิ่มเจ้าหน้าที่</a>

                    </c:if>





            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="table-responsive" >
                    <table class="table table-striped table-bordered table-hover">
                        <thead>
                            <tr>
                                <th class="text-center">ไอดี</th>
                                <th class="text-center">ชื่อ</th>
                                <th class="text-center">นามสกุล</th>
                                <th class="text-center">อีเมล</th>  
                                <th class="text-center">ดูรายละเอียด</th>
                                <th class="text-center">แก้ไข</th>
                                <th class="text-center">ลบ</th>

                            </tr>
                        </thead>
                        <tbody>
                            <c:if test="${staffs != null}">  

                                <c:forEach var="staffs" items="${staffs}" varStatus="loop">
                                    <tr>
                                        <td class="text-center">${staffs.id}</td>
                                        <td class="text-center">${staffs.firstname}</td>
                                        <td class="text-center">${staffs.lastname}</td>
                                        <td class="text-center">${staffs.email}</td>
                                        <td class="text-center">
                                            <!--                                            <form  action="viewstaff" method="post" >    
                                                                                            <input type="hidden" value="${staffs.id}" name="id"/>
                                                                                            <button type="submit"  class="no-border tran-bg"><i style="color: green;" class="fa fa-search-plus" style="font-size: 30px"></i></button>
                                                                                        </form>  -->
                                            <button type="button" class="no-border tran-bg" data-toggle="modal" data-target="#flipFlop${staffs.id}">
                                                <i  style="color: green;" class="fa fa-search-plus" style="font-size: 30px"></i>
                                            </button>
                                        </td>
                                        <td class="text-center">
                                            <c:if test="${page_Edit != null}">
                                                <form  action="editstaff" method="post" >    
                                                    <input type="hidden" value="${staffs.id}" name="id"/>
                                                    <button type="submit"  class="no-border tran-bg"><i class="fa fa-edit edit-btn" style="font-size: 30px"></i></button>
                                                </form> 
                                            </c:if>
                                        </td>
                                        <td class="text-center">
                                            <c:if test="${page_Delete != null}">
                                                <form id="delete_${staffs.id}"  action="deletestaff" method="post" >    
                                                    <input type="hidden" value="${staffs.id}" name="id"/>
                                                    <a  onclick="chkConfirm(${staffs.id});" ><i style="color: red;" class="fa fa-trash fa-2x" aria-hidden="true"></i></a>

                                                </form>  
                                            </c:if>
                                        </td>
                                    </tr>

                                    <!-- The modal -->
                                <div class="modal fade" id="flipFlop${staffs.id}" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                                <h4 class="modal-title" id="modalLabel">เจ้าหน้าที่</h4>
                                            </div>
                                            <div class="modal-body">

                                                <p></p>
                                                <div class="row" style="font-size: 20px">

                                                    <div class="col-lg-12 ">
                                                        <dl class="dl-horizontal">

                                                            <dt>ไอดี</dt> <dd><span id="modal_span">${staffs.id}</span></dd>
                                                            <dt>ชื่อ:</dt> <dd><span id="modal_span">${staffs.firstname}</span></dd>
                                                            <dt>นามสกุล:</dt> <dd><span id="modal_span">${staffs.lastname}</span></dd>
                                                            <dt>อีเมล</dt> <dd><span id="modal_span">${staffs.email}</span></dd>
                                                            <dt>เบอร์โทรศัพท์ </dt> <dd><span id="modal_span">${staffs.phone1}</span></dd>

                                                        </dl>
                                                    </div>

                                                </div>



                                            </div>
                                            <div class="modal-footer">
                                                <c:if test="${page_Edit != null}">
                                                    <form  action="editstaff" method="post" >    
                                                        <input type="hidden" value="${staffs.id}" name="id"/>
                                                        <button type="submit" class="btn btn-primary">แก้ไขข้อมูล</button>
                                                    </form> 
                                                </c:if>

                                                <button type="button" class="btn btn-danger" data-dismiss="modal">ปิด</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!------------------------------->

                            </c:forEach>
                        </c:if>
                        </tbody>
                    </table>
                </div>
                <div class="row">
                    <center>
                        <div class="pagination_center">
                            <nav aria-label="Page navigation col-centered">
                                <ul class="pagination">
                                    <c:if test="${page_priv == true}">  
                                        <li class="page-item"><a class="page-link"  href="${pageContext.request.contextPath}/stafflist?page=${page-1}"><i class="fa fa-angle-left  "></i></a></li>
                                            </c:if>
                                            <c:if test="${page_count != 0}">
                                                <c:forEach begin="0" end="${page_count-1}" varStatus="loop">
                                                    <c:if test="${page == loop.index}"> 
                                                <li class="page-item"><a class="page-link active"  href="${pageContext.request.contextPath}/stafflist?page=${loop.index}">${loop.index+1}</a></li>
                                                </c:if> 
                                                <c:if test="${page != loop.index}"> 
                                                <li class="page-item"><a class="page-link"  href="${pageContext.request.contextPath}/stafflist?page=${loop.index}">${loop.index+1}</a></li>
                                                </c:if> 
                                            </c:forEach>
                                        </c:if>
                                        <c:if test="${page_next == true}"> 
                                        <li class="page-item"><a class="page-link" href="${pageContext.request.contextPath}/stafflist?page=${page+1}"><i class="fa fa-angle-right "></i></a></li>
                                            </c:if>
                                </ul>
                            </nav>
                        </div>
                </div><!--end row-->
            </div>
        </div>
    </div><!-- End Content -->    
</div><!-- End Page Content -->
<script language="JavaScript">
    function chkConfirm(rowid) {
        bootbox.confirm({
            message: "คุณแน่ใจหรือว่าต้องการลบ ?",
            buttons: {
                confirm: {
                    label: 'ยืนยัน',
                    className: 'btn-success'
                },
                cancel: {
                    label: 'ยกเลิก',
                    className: 'btn-danger'
                }
            },
            callback: function (result) {
                if (result) {
                    $("#delete_" + rowid).submit();
                }
                console.log('This was logged in the callback: ' + result + "#delete_" + rowid);
            }
        });
    }
</script>
<jsp:include page="../footer.jsp" />
