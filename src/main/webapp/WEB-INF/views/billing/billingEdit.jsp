<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:include page="../header.jsp" />
<jsp:include page="../navigation.jsp" />
<!-- Page Content -->
<div id="page-wrapper">
    <div class="container-fluid">
        <!-- start Content -->
<form  action="editsave" method="post" >  
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Quatation Edit</h1>
            </div>
        </div>


        <div class="row">
            <div class="col-lg-12">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover">
                        <thead>
                            <tr>
                                <th class="text-center">ID</th>
                                <th class="text-center">Billing ID</th>
                                <th class="text-center">Product ID</th>
                                <th class="text-center">Product Name</th>
                                <th class="text-center">Product Price</th>
                                <th class="text-center">Quantity</th>
                                <th class="text-center">Sum Price</th>

                            </tr>
                        </thead>
  
                        <tbody>
                            
                            <c:forEach var="orderBillinglist" items="${orderBillinglist}" varStatus="loop">
                                <tr>
                                    <td class="text-center"><input class="text-center" hidden="true" name="orid" value="${orderBillinglist.id}">  ${orderBillinglist.id}</td>
                                    <td class="text-center"><input class="text-center" hidden="true" name="qtid" value="${orderBillinglist.billing_id}">${orderBillinglist.billing_id}</td>
                                    <td class="text-center"><input class="text-center" hidden="true" name="prid" value="${orderBillinglist.product_id}">${orderBillinglist.product_id}</td>
                                    <td class="text-center"><input class="text-center" name="productname" value="${orderBillinglist.product_name}"> </td>
                                    <td class="text-center"><input class="text-center" name="productprice" value="${orderBillinglist.product_price}"></td>
                                    <td class="text-center"><input class="text-center" name="quantity" value="${orderBillinglist.quantity}"></td>
                                    <td class="text-center"><input class="text-center" name="sumproce" value="${orderBillinglist.sum_proce}"></td>
                             </tr>  
                            </c:forEach>
                            
                        </tbody>

                    </table>
                

                </div>
                    <br>
                    <br>
              <div class="form-group text-right"><button type="submit" class="btn btn-primary"> <span class="fa fa-save "></span> Save</button></div>
                <!-- End Content -->
            </div>
            <!-- End Page Content -->

        </div>
          </form>   
    </div>
</div>
<jsp:include page="../footer.jsp" />
