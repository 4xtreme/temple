<%-- 
    Document   : header
    Created on : Jan 11, 2017, 1:38:57 PM
    Author     : chatcharin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!-- Navigation -->
<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <a class="navbar-brand navbar-center" href="">ระบบจองห้องศูนย์ปฏิบัติธรรม</a>
   

    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"> </span>
        <span class="icon-bar"> </span>
        <span class="icon-bar"> </span>
    </button>

    <!-- Top Navigation: Left Menu -->
    <ul class="nav navbar-nav navbar-left navbar-top-links" >
        <!-- <li><a href="#"><i class="fa fa-home fa-fw"></i> Home</a></li>-->

    </ul>
    
    <!-- Sidebar -->

    <div class="navbar-default sidebar "  style="height: 1200%;"  role="navigation" >
        <div id="open-btn" class="close-btn"><i class="fa fa-angle-double-right" style="font-size: 30px" aria-hidden="true"></i></div>
        <div id="close-btn" class="close-btn"><i class="fa fa-times"style="font-size: 30px" aria-hidden="true"></i></div>
        <br></br>
        <div id="navbar" style="border-bottom:solid 0px; height: 500px;font-size: 20px ">
            <div class="sidebar-nav navbar-collapse"  id="side-menu" >
                <ul class="nav" > 
                    <li>
                        <a href="#" ><i class="fa fa-bed" aria-hidden="true"></i> จองออนไลน์</a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="${pageContext.request.contextPath}/bookingOnline"><i class="fa fa-suitcase" aria-hidden="true"></i> จองออนไลน์</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="#" ><i  class="fa fa-cog"></i> ตั้งค่าผู้ใช้</a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="${pageContext.request.contextPath}/contactprofile"><i class="fa fa-info-circle" aria-hidden="true"></i> ข้อมูลผู้ใช้</a>
                            </li>
                            <li>
                                <a href="${pageContext.request.contextPath}/editcontactprofile"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> แก้ไขข้อมูลผู้ใช้</a>
                            </li>
                        </ul>
                    </li>
                    <hr style="border-bottom:solid 1px;">
                    <li>
                        <a href="login"><i class="fas fa-power-off" aria-hidden="true"></i> ออกจากระบบ</a>
                    </li>
                </ul>                  </ul> <br><br><br><br><br>           
                
            </div>
        </div>
    </div>
</nav>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

<script>
    $(document).ready(function () {
        $("#close-btn").show();
        $("#open-btn").hide();
        $("#mainmenu").click(function () {
            $("#side-menu").hide(100);
            $("#sub_mainmenu").show(100);
        });
        $("#close-btn").click(function () {
            $("#navbar").hide(100);
            $("#open-btn").show(100);
            $("#close-btn").hide(100);
            $("#bottom-nav").hide(100);
        });
        $("#open-btn").click(function () {
            $("#navbar").show(100);
            $("#open-btn").hide(100);
            $("#close-btn").show(100);
            $("#bottom-nav").show(100);

        });

    });


</script>
