<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:include page="../header.jsp" />
<jsp:include page="../navigation.jsp" />
 <!-- Page Content -->
    <div id="page-wrapper">
       <div class="container-fluid">
		<!-- start Content -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Invoice </h1>
                        
                    </div>
                     
                </div>
               <form  action="searchinvoice" method="post" >  
                       <div class="col-lg-6">        
                           <h4 class="page-header">Search Invoice</h4>
                        <dl class="dl-horizontal">
                              <dt>Invoice ID:</dt> <dd>  
                                  <input  name="invoiceid" class="form-control"  >   
                              </dd><br>
                                 <dt>Customer ID:</dt> <dd>  
                                  <input  name="customerid" class="form-control"  >   
                              </dd><br>              
                               
                              
                                <dt> </dt>  <dd class="form-group text-right"> <button class="btn btn-primary" type="submit">Search</button></dd>                                
                        </dl>                        
                    </div>     
                 </form>
                <form  action="editdate" method="post" >  
                       <div class="col-lg-6">        
                           <h4 class="page-header">Edit Create Date </h4>
                        <dl class="dl-horizontal">
                              <dt>Invoice ID:</dt> <dd>  
                                  <select class="form-control " id="selectinvoice"  name="selectinvoice" required="">
                                      <option  value="0" disabled=" " selected="">Select Invoice ID</option>
                                      <c:forEach var="listinvoice" items="${invoicelist}" varStatus="loop">
                                          <option value="${listinvoice.invoice_id}">${listinvoice.invoice_id}</option>
                                      </c:forEach>                                           
                                  </select>
                              </dd><br>
                                             
                               
                                <dt>Set Date: </dt> 
                                <dd>
                                    <div class='input-group date' >
                                        <input  class="form-control datetime" name="setdate" required=""/>
                                        <span class="input-group-addon">
                                            <span class="fa fa-calendar"></span>
                                        </span>
                                    </div>                                                            
                                </dd><br>
                                <dt> </dt>  <dd class="form-group text-right"> <button class="btn btn-primary" type="submit">Edit</button></dd>                                
                        </dl>                        
                    </div>     
                 </form>
                <c:if test="${errMessage != null}">
                     <h4 class="text-danger"> ${errMessage}</h4>                     
                 </c:if>
                   <c:if test="${completemessage != null}">
                     <h4 class="text-success"> ${completemessage}</h4>                     
                 </c:if>
          
           <div class="row">
             <div class="col-lg-12">
                 <h4 class="page-header">Invoice List  </h4>  
                 
               <div class="table-responsive">
                                    	<table class="table table-striped table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                <th class="text-center">Invoice ID</th>
                                                <th class="text-center">Customer ID</th>
                                                <th class="text-center">Create Date</th>
                                                <th class="text-center">Due Date</th>
                                                <th class="text-center">Status</th>
                                                <th class="text-center">Print</th>
                                                 <th class="text-center">Send Email</th>
                                                   <th class="text-center">Edit</th>
                                            </tr>
                                        </thead>
                                        
                                        <tbody>
                                    <c:if test="${invoicelist != null}">  
                                    <c:forEach var="invoicelists" items="${invoicelist}" varStatus="loop">
                                    <tr>
                                                <td class="text-center">${invoicelists.invoice_id}</td>
                                                <td class="text-center">${invoicelists.customer_id}</td>  
                                                 <td class="text-center">${invoicelists.create_date}</td>
                                                <td class="text-center">${invoicelists.due_date}</td>
                                                <td class="text-center">
                                                    <c:if test="${invoicelists.status != true}">
                                                        <form  action="changestatusinvoice" method="post" >
                                                            
                                                            <input type="hidden" value="${invoicelists.invoice_id}" name="selectid"/>
                                                            <button type="submit" class="btn btn-warning btn-circle "><i class="fa fa-edit"></i></button>
                                                        </form>                                                        
                                                    </c:if>
                                                    <c:if test="${invoicelists.status == true}">
                                                          <button type="submit" class="btn btn-success btn-circle "><i class="fa fa-check"></i></button>                                
                                                    </c:if>
                                                        
                                                </td>
                                                <td class="text-center">                                                
                                                         <form  action="printinvoice" method="post" >
                                                            <input type="hidden" value="${invoicelists.invoice_id}" name="selectid"/>
                                                            <button type="submit" class="btn btn-warning btn-circle "><i class="fa fa-print"></i></button>
                                                        </form>
                                                
                                                </td>
                                                   <td class="text-center"> 
                                                       <c:if test="${invoicelists.sent_email == false}">
                                                           <form  action="sendemailinvoice" method="post" >
                                                            <input type="hidden" value="${invoicelists.invoice_id}" name="selectid"/>
                                                           <button  type="submit" class="btn btn-danger btn-circle "><i class="fa fa-send"></i></button>
                                                           </form>
                                                          </c:if>
                                                           <c:if test="${invoicelists.sent_email == true}">
                                                         <form  action="sendemailinvoice" method="post" >
                                                            <input type="hidden" value="${invoicelists.invoice_id}" name="selectid"/>
                                                            
                                                            <button type="submit"   class="btn btn-success btn-circle "><i class="fa fa-send"></i></button>
                                                        </form>
                                                 </c:if>
                                                </td>
                                                 <td class="text-center">                                                
                                                           <form  action="editinvoice" method="post" >                                     
                                                               <button type="submit" value="${invoicelists.invoice_id}" name="editid" class="btn btn-warning btn-circle"><i class="fa fa-edit"></i></button>
                                                  </form>    
                                                
                                                </td>
                                            </tr>  
                                     </c:forEach>
                                            
                                
                                 </c:if>
                                        </tbody>
                                                                           
                                    </table>
                </div>
                 <!-- End Content -->
             </div>
             <!-- End Page Content -->

         </div>
	                   
              
			
        </div>
	

</div>

 
 <jsp:include page="../footer.jsp" />
 

