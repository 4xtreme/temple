<%-- 
    Document   : header
    Created on : Jan 11, 2017, 1:38:57 PM
    Author     : chatcharin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!-- Navigation -->
<nav class="navbar navbar-inverse navbar-fixed-top"role="navigation">
    <a class="navbar-brand navbar-center"href="">ระบบจองห้องศูนย์ปฏิบัติธรรม</a>
    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"> </span>
        <span class="icon-bar"> </span>
        <span class="icon-bar"> </span>
    </button>
    <!-- Top Navigation: Left Menu -->
    <ul class="nav navbar-nav navbar-left navbar-top-links" >
        <!-- <li><a href="#"><i class="fa fa-home fa-fw"></i> Home</a></li>-->
    </ul>
    <!-- Sidebar -->
    <div class="navbar-default sidebar "   role="navigation" >
        <div id="open-btn" class="close-btn"><i class="fa fa-angle-double-right" style="font-size: 30px" aria-hidden="true"></i></div>
        <div id="close-btn" class="close-btn"><i class="fa fa-times"style="font-size: 30px" aria-hidden="true"></i></div>
        <br>
        <br>

        <div id="navbar" style="font-size: 18px ">
            <div class="sidebar-nav navbar-collapse"  id="side-menu" >
                <ul class="nav" > 
                    <c:if test="${checkin_page.view_page == true || checkout_page.view_page == true}">
                        <li>
                            <a href="${pageContext.request.contextPath}/roomfront"><i class="glyphicon glyphicon-tasks" aria-hidden="true"></i> ดูรายละเอียดห้อง ทั้งหมด </a>
                        </li>
                    </c:if>

                    <c:if test="${member_page.create_page == true}">
                        <li>
                            <a href="${pageContext.request.contextPath}/regcreatenow"><i class="fa fa-bed" aria-hidden="true"></i> ลงทะเบียน </a>
                        </li>
                    </c:if>

                    <c:if test="${checkin_page.create_page == true}">
                        <li>
                            <a href="${pageContext.request.contextPath}/bookingRoom"><i class="fa fa-calendar-check-o" aria-hidden="true"></i> เช็คอินยังไม่จอง </a>
                        </li>
                    </c:if>

                    <c:if test="${checkin_page.create_page == true}">
                        <!--                        <li>
                                                    <a href="${pageContext.request.contextPath}/checkInOnline"><i class="fa fa-calendar-check-o" aria-hidden="true"></i> เช็คอินจอง</a>
                                                </li>-->
                    </c:if>

                    <c:if test="${checkout_page.create_page == true}">
                        <!--                        <li>
                                                    <a href="${pageContext.request.contextPath}/checkOut"><i class="fas fa-sign-in-alt" aria-hidden="true"></i> เช็คเอาท์ </a>
                                                </li>-->
                    </c:if>

                    <li>
                        <a href="${pageContext.request.contextPath}/editcheckin"><i class="fa fa-sign-in-alt" aria-hidden="true"></i> แก้ไขการเช็คอิน </a>
                    </li>

                    <c:if test="${temple_page.view_page == true || temple_page.create_page == true }">
                        <li>
                            <a href="#" ><i class="fa fa-sun-o" aria-hidden="true"></i>  วัด </a>
                            <ul class="nav nav-second-level">
                                <c:if test="${temple_page.view_page == true}">
                                    <li>
                                        <a href="${pageContext.request.contextPath}/templelist"><i class="fa fa-list" aria-hidden="true"></i> รายการวัด</a>
                                    </li>
                                </c:if>
                                <c:if test="${temple_page.create_page == true}">
                                    <li>
                                        <a href="${pageContext.request.contextPath}/addtemple"><i class="fa fa-plus-circle" aria-hidden="true"></i>  เพิ่มวัด</a>
                                    </li>
                                </c:if>

                                <c:if test="${temple_page.view_page == true}">
                                    <li>
                                        <a href="${pageContext.request.contextPath}/templepiclist"><i class="fa fa-list" aria-hidden="true"></i> รายการรูปภาพ</a>
                                    </li>
                                </c:if>
                                <c:if test="${temple_page.create_page == true}">
                                    <li>
                                        <a href="${pageContext.request.contextPath}/addpicture"><i class="fa fa-plus-circle" aria-hidden="true"></i> เพิ่มรูปภาพ</a>
                                    </li>
                                </c:if>
                            </ul>
                        </li>
                    </c:if>
                    <li>
                        <a href="#" ><i class="fas fa-star" aria-hidden="true"></i> พระวิปัสสนาจารย์ </a>
                        <ul class="nav nav-second-level">
                            <c:if test="${monk_page.view_page == true}">
                                <li>
                                    <a href="${pageContext.request.contextPath}/vipassana"><i class="fa fa-list" aria-hidden="true"></i> ดูรายชื่อพระวิปัสสนาจารย์</a>
                                </li>
                            </c:if>
                            <c:if test="${monk_page.create_page == true}">
                                <li>
                                    <a href="${pageContext.request.contextPath}/addvipassana"><i class="fa fa-plus-circle" aria-hidden="true"></i> เพิ่มรายชื่อพระวิปัสสนาจารย์</a>
                                </li>
                            </c:if>

                            <li>
                                <a href="${pageContext.request.contextPath}/activity"><i class="fa fa-list" aria-hidden="true"></i> ศาสนกิจ</a>
                            </li>
                            <li>
                                <a href="${pageContext.request.contextPath}/addactivity"><i class="fa fa-plus-circle" aria-hidden="true"></i> เพิ่มศาสนกิจ</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="#" ><i class="fas fa-star" aria-hidden="true"></i> บุคลากรในวัด </a>
                        <ul class="nav nav-second-level">
                            <c:if test="${monk_page.view_page == true}">
                                <li>
                                    <a href="${pageContext.request.contextPath}/monkall"><i class="fa fa-list" aria-hidden="true"></i> ดูรายชื่อบุคลากรในวัด</a>
                                </li>
                            </c:if>
                            <c:if test="${monk_page.create_page == true}">
                                <li>
                                    <a href="${pageContext.request.contextPath}/monkreg"><i class="fa fa-plus-circle" aria-hidden="true"></i> เพิ่มรายชื่อบุคลากรในวัด</a>
                                </li>
                            </c:if>

                            <li>
                                <a href="${pageContext.request.contextPath}/eventsall"><i class="fa fa-list" aria-hidden="true"></i> กิจนิมนต์</a>
                            </li>
                            <li>
                                <a href="${pageContext.request.contextPath}/eventsmonk"><i class="fa fa-plus-circle" aria-hidden="true"></i> เพิ่มกิจนิมนต์</a>
                            </li>
                        </ul>
                    </li>

                    <c:if test="${room_page.create_page == true || room_page.view_page == true }">
                        <li>
                            <a href="#" ><i class="glyphicon glyphicon-home" aria-hidden="true"></i> อาคาร / กุฏิ</a>
                            <ul class="nav nav-second-level">
                                <c:if test="${room_page.view_page == true }">
                                    <li>
                                        <a href="${pageContext.request.contextPath}/roomAll"><i class="fa fa-list" aria-hidden="true"></i> รายการอาคาร /กุฏิ</a>
                                    </li>
                                </c:if>

                                <c:if test="${room_page.create_page == true}">
                                    <li>
                                        <a href="${pageContext.request.contextPath}/addRoom"><i class="fa fa-plus-circle" aria-hidden="true"></i> เพิ่ม  อาคาร / กุฏิ</a>
                                    </li>
                                </c:if>
                            </ul>
                        </li>
                    </c:if>
                    <c:if test="${staff_page.create_page == true || staff_page.view_page == true || content_page.create_page == true || content_page.view_page == true}">
                        <li>
                            <a href="#" ><i class="glyphicon glyphicon-user"></i> เจ้าหน้าที่ </a>
                            <ul class="nav nav-second-level">
                                <c:if test="${staff_page.view_page == true}">
                                    <li>
                                        <a href="${pageContext.request.contextPath}/stafflist"><i class="fa fa-list" aria-hidden="true"></i> รายชื่อเจ้าหน้าที่</a>
                                    </li>
                                </c:if>
                                <c:if test="${staff_page.create_page == true}">
                                    <li>
                                        <a href="${pageContext.request.contextPath}/addstaff"><i class="fa fa-plus-circle" aria-hidden="true"></i> เพิ่มเจ้าหน้าที่</a>
                                    </li>
                                </c:if>
                                <c:if test="${content_page.view_page == true}">
                                    <li>
                                        <a href="${pageContext.request.contextPath}/articlesall"><i class="fa fa-list" aria-hidden="true"></i> ดูบทความ</a>
                                    </li>
                                </c:if>
                                <c:if test="${content_page.create_page == true}">
                                    <li>
                                        <a href="${pageContext.request.contextPath}/aarticles"><i class="fa fa-plus-circle" aria-hidden="true"></i> เพิ่มบทความ</a>
                                    </li>
                                </c:if>
                            </ul>
                        </li>
                    </c:if>
                    <c:if test="${member_page.create_page == true || member_page.view_page == true }">
                        <li>
                            <a href="#" ><i class="fa fa-users" aria-hidden="true"></i> ผู้ปฏิบัติธรรม </a>
                            <ul class="nav nav-second-level">
                                <c:if test="${member_page.view_page == true}">
                                    <li>
                                        <a href="${pageContext.request.contextPath}/regsall"><i class="fa fa-list" aria-hidden="true"></i> ดูรายชื่อผู้ปฏิบัติธรรม</a>
                                    </li>
                                </c:if>
                            </ul>
                        </li>
                    </c:if>
                    <li>
                        <a href="#" ><i  class="fa fa-cog"></i> ตั้งค่า </a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="${pageContext.request.contextPath}/profile"><i class="fa fa-info-circle" aria-hidden="true"></i> ข้อมูลผู้ใช้</a>
                            </li>
                            <li>
                                <a href="${pageContext.request.contextPath}/editprofile"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> แก้ไขข้อมูลผู้ใช้</a>
                            </li>
                            <c:if test="${role_page.view_page == true}">
                                <li>
                                    <a href="${pageContext.request.contextPath}/role"><i class="fa fa-list" aria-hidden="true"></i> การเข้าถึงทั้งหมด</a>
                                </li>
                            </c:if>
                            <c:if test="${role_page.create_page == true}">
                                <li>
                                    <a href="${pageContext.request.contextPath}/addrole"><i class="fa fa-plus-circle" aria-hidden="true"></i> เพิ่มการเข้าถึง</a>
                                </li>
                            </c:if>

                            <li>
                                <a href="${pageContext.request.contextPath}/import"><i class="fa fa-plus-circle" aria-hidden="true"></i> นำเข้าข้อมูลผู้ปฏิบัติธรรม</a>
                            </li>
                            <!--                            <li>
                                                            <a href="${pageContext.request.contextPath}/monkimport"><i class="fa fa-plus-circle" aria-hidden="true"></i> นำเข้าข้อมูลพระวิปัสสนาจารย์</a>
                                                        </li>-->
                        </ul>
                    </li>
                    <li>
                        <a href="#" ><i  class="glyphicon glyphicon-file"></i> รายงาน </a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="${pageContext.request.contextPath}/monkreport"><i class="glyphicon glyphicon-book" aria-hidden="true"></i> รายงานผู้เข้าสอบอารมณ์</a>
                            </li>
<!--                            <li>
                                <a href="${pageContext.request.contextPath}/contactreport"><i class="glyphicon glyphicon-ok-sign" aria-hidden="true"></i> รายงานผู้เข้าปฏิบัติธรรม</a>
                            </li>-->
                            <li>
                                <a href="${pageContext.request.contextPath}/countreport"><i class="glyphicon glyphicon-ok-sign" aria-hidden="true"></i> จำนวนบุคคลภายในวัด</a>
                            </li>
                            <li>
                                <a href="${pageContext.request.contextPath}/chartscheckin"><i class="glyphicon glyphicon-user" aria-hidden="true"></i> คนกับพระวิปัสสนาจารย์</a>
                            </li>
                            <li>
                                <a href="${pageContext.request.contextPath}/ccharts"><i class="glyphicon glyphicon-asterisk" aria-hidden="true"></i> สถิติผู้เข้ามาปฏิบัติธรรม</a>
                            </li>
                            <li>
                                <a href="${pageContext.request.contextPath}/dailycharts"><i class="glyphicon glyphicon-tasks" aria-hidden="true"></i> สถิติผู้เข้ามาปฏิบัติธรรมในแต่ละวัน</a>
                            </li>
                            <li>
                                <a href="${pageContext.request.contextPath}/summarycharts"><i class="glyphicon glyphicon-sort" aria-hidden="true"></i> สถิติผู้เข้ามาปฏิบัติธรรมทั้งหมด</a>
                            </li>

                        </ul>
                    </li>
                    <hr style="border-bottom:solid 1px;">
                    <li>
                        <!--<a href="${pageContext.request.contextPath}/loginstaff"><i class="fas fa-power-off"></i> ออกจากระบบ</a>-->
                        <a href="${pageContext.request.contextPath}/login"><i class="fas fa-power-off"></i> ออกจากระบบ</a>
                    </li>
                </ul>        
            </div>
        </div>
    </div>
</nav>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script>
    $(document).ready(function () {
        $("#close-btn").show();
        $("#open-btn").hide();
        $("#mainmenu").click(function () {
            $("#side-menu").hide(100);
            $("#sub_mainmenu").show(100);
        });
        $("#close-btn").click(function () {
            $("#navbar").hide(100);
            $("#open-btn").show(100);
            $("#close-btn").hide(100);
            $("#bottom-nav").hide(100);
        });
        $("#open-btn").click(function () {
            $("#navbar").show(100);
            $("#open-btn").hide(100);
            $("#close-btn").show(100);
            $("#bottom-nav").show(100);
        });
    });
</script>
