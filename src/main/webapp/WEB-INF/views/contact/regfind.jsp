<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html;charset=UTF-8" %>
<jsp:include page="../header.jsp" />
<c:if test="${roleadmin == 'admin'}">
    <jsp:include page="../navigation.jsp" />
</c:if>
<c:if test="${roleadmin == 'user'}">
    <jsp:include page="../navigation_top.jsp" />
</c:if>
<!-- Page Content -->
<div id="page-wrapper">
    <div class="container-fluid">
        <!-- start Content -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header" id="title_head">สมาชิก</h1>
            </div>
        </div>
        <form action="searchreg" method="POST">
            <div>
                <select class="form-control width_15" name="typeSearch">
                    <option value="firstname">ชื่อ</option>
                    <option value="lastname">นามสกุล</option>
                    <option value="gender_text">เพศ</option>
                </select>&nbsp;
                <input type="text" class="form-control width_20" name="message">&nbsp;
                <button type="submit" class="btn btn-primary btn-signup width_10 align-center">
                    <i class="glyphicon glyphicon-search"></i> ค้นหา
                </button>
            </div>
        </form><br>
        <div class="row">
            <div class="col-lg-12">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover">
                        <thead>
                            <tr>
                                <th class="text-center">ลำดับ</th>
                                <th class="text-center">ชื่อ-นามสกุล</th>
                                <!--<th class="text-center">อีเมล</th>-->
                                <th class="text-center">เชื้อชาติ,สัญชาติ</th>
                                <th class="text-center">วันเดือนปีเกิด</th>
                                <th class="text-center">เพศ</th>
                                <th class="text-center">หมายเลขบัตรประชาชน</th>
                                <!--<th class="text-center">ที่อยู่</th>-->
                                <!--<th class="text-center">หมู่บ้าน</th>-->
                                <!--<th class="text-center">ตำบล</th>-->
                                <th class="text-center">อำเภอ</th>
                                <th class="text-center">จังหวัด</th>
                                <th class="text-center">สถานะ</th>
                                <th class="text-center">ดูรายละเอียด</th>
                                <th class="text-center">แก้ไข</th>
                                <th class="text-center">เปิด/ปิดสถานะการใช้งาน</th>

                            </tr>
                        </thead>
                        <tbody>

                            <c:forEach var="registerlist" items="${registerlist}" varStatus="loop">
                                <tr>
                                    <td class="text-center">${registerlist.reg_id}</td>
                                    <td class="text-center">${registerlist.firstname} &nbsp;&nbsp;${registerlist.lastname}</td>
                                    <!--<td class="text-center">${registerlist.email}</td>-->
                                    <td class="text-center">${registerlist.royal},&nbsp;&nbsp;${registerlist.nationality}</td>
                                    <td class="text-center">${registerlist.bd_birth}</td>
                                    <td class="text-center">${registerlist.gender_text}</td>
                                    <td class="text-center">${registerlist.personid}</td>
<!--                                    <td class="text-center">${registerlist.address}</td>
                                    <td class="text-center">${registerlist.village}</td>
                                    <td class="text-center">${registerlist.district}</td>-->
                                    <td class="text-center">${registerlist.city}</td>
                                    <td class="text-center">${registerlist.province}</td>
                                    <td class="text-center">
                                        <c:if test="${registerlist.is_delete == false}">
                                            ปกติ
                                        </c:if>
                                        <c:if test="${registerlist.is_delete == true}">
                                            ปิดการใช้งาน
                                        </c:if>    

                                    </td>
                                    <td class="text-center">
                                        <button type="button" class="no-border tran-bg" data-toggle="modal" data-target="#flipFlop${registerlist.reg_id}">
                                            <i  style="color: green;" class="fa fa-search-plus" style="font-size: 30px"></i>
                                        </button>
                                    </td>
                                    <td class="text-center">
                                        <c:if test="${page_Edit != null}">
                                            <form  action="selectedit" method="post" >
                                                <input type="hidden" value="${registerlist.reg_id}" name="reg_id"/>
                                                <button type="submit" class="no-border tran-bg "><i class="fa fa-edit edit-btn" style="font-size: 30px"></i></button>
                                            </form>
                                        </c:if>
                                    </td>
                                    <td class="text-center">
                                        <c:if test="${page_Edit != null}">
                                            <c:if test="${registerlist.is_delete == false}">
                                                <form  id="close_${registerlist.reg_id}" action="deleteregs" method="post" >                                                                  
                                                    <input type="hidden" value="${registerlist.reg_id}" name="reg_id"/>
                                                    <div  onclick="chkConfirm(${registerlist.reg_id});" ><i style="color: green;" class="fa fa-lock-open fa-2x" aria-hidden="true"></i></div>
                                                </form>
                                            </c:if>
                                            <c:if test="${registerlist.is_delete == true}">
                                                <form  id="open_${registerlist.reg_id}" action="reopenregs" method="post" >                                                                  
                                                    <input type="hidden" value="${registerlist.reg_id}" name="reg_id"/>
                                                    <div  onclick="chkConfirmOpen(${registerlist.reg_id});" ><i style="color: red;" class="fa fa-lock fa-2x" aria-hidden="true"></i></div>
                                                </form>
                                            </c:if>
                                        </c:if>
                                    </td>
                                </tr>
                                <!-- The modal -->
                            <div class="modal fade" id="flipFlop${registerlist.reg_id}" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                            <h4 class="modal-title" id="modalLabel">ข้อมูลส่วนตัว</h4>
                                        </div>
                                        <div class="modal-body">
                                            <div class="row" style="font-size: 20px">
                                                <div class="col-lg-6">
                                                    <p><b>คำนำหน้า:</b> ${registerlist.nametitle} </p>
                                                    <p> <b>ฉายา:</b> ${registerlist.nickname}</p>
                                                    <p> <b>ชื่อ:</b> ${registerlist.firstname}</p>
                                                    <p> <b>นามสกุล:</b> ${registerlist.lastname}</p>
                                                    <p> <b>หมายเลขบัตรประชาชน:</b>${registerlist.personid}</p>
                                                </div>
                                                <div class="col-lg-6">
                                                    <p><b>สัญชาติ: </b>${registerlist.royal}</p>
                                                    <p><b>เชื้อชาติ:</b> ${registerlist.nationality}</p>
                                                    <p><b>วันเกิด:</b>${registerlist.bd_birth}</p>
                                                    <p><b>เพศ:</b> ${registerlist.gender}</p>
                                                    <p><b>เบอร์โทรศัพท์:</b> ${registerlist.telephone}</p>
                                                </div>

                                            </div>
                                            <hr>
                                            <h4>ข้อมูลที่อยู่</h4>
                                            <div class="row" style="font-size: 20px" >
                                                <div class="col-lg-12">
                                                    <p><b>ที่อยู่:</b>${registerlist.address}</p>
                                                    <p><b>หมู่บ้าน:</b>${registerlist.village}</p>
                                                    <p><b>ตำบล:</b>${registerlist.district}</p>
                                                    <p><b>อำเภอ:</b>${registerlist.city}</p>
                                                    <p><b>จังหวัด:</b>${registerlist.province}</p>
                                                    <p><b>รหัสไปรษณีย์:</b>${registerlist.postalcode}</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <c:if test="${page_Edit != null}">
                                                <form  action="selectedit" method="post" >
                                                    <input type="hidden" value="${registerlist.reg_id}" name="reg_id"/>
                                                    <button type="submit" class="btn btn-primary">แก้ไขข้อมูล</button>
                                                </form>
                                            </c:if>
                                            <button type="button" class="btn btn-danger" data-dismiss="modal">ปิด</button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </c:forEach>
                        </tbody>
                    </table
                </div>
                <div class="row">
                    &nbsp;&nbsp;&nbsp;
                    <div class="col-lg-12" style="text-align: center;">
                        <a href="regsall" type="submit" class="btn btn-primary btn-signup width_10 align-center">
                            <i class="glyphicon glyphicon-arrow-left"></i> ย้อนกลับ
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- End Content -->
<script language="JavaScript">
    function chkConfirm(reg_id) {
        bootbox.confirm({
            message: "คุณแน่ใจหรือว่าต้องการปิดการใช้งาน ?",
            buttons: {
                confirm: {
                    label: 'ยืนยัน',
                    className: 'btn-success'
                },
                cancel: {
                    label: 'ยกเลิก',
                    className: 'btn-danger'
                }
            },
            callback: function (result) {
                if (result) {
                    $("#close_" + reg_id).submit();
                }
                console.log('This was logged in the callback: ' + result + "#delete_" + reg_id);
            }
        });
    }

    function chkConfirmOpen(reg_id) {
        bootbox.confirm({
            message: "คุณแน่ใจหรือว่าต้องการเปิดการใช้งาน ?",
            buttons: {
                confirm: {
                    label: 'ยืนยัน',
                    className: 'btn-success'
                },
                cancel: {
                    label: 'ยกเลิก',
                    className: 'btn-danger'
                }
            },
            callback: function (result) {
                if (result) {
                    $("#open_" + reg_id).submit();
                }
                console.log('This was logged in the callback: ' + result + "#delete_" + reg_id);
            }
        });
    }
</script>
<jsp:include page="../footer.jsp" />
