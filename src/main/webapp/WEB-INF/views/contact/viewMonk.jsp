<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html;charset=UTF-8" %>
<jsp:include page="../header.jsp" />
<c:if test="${roleadmin == 'admin'}">
    <jsp:include page="../navigation.jsp" />
</c:if>
<c:if test="${roleadmin == 'user'}">
    <jsp:include page="../navigation_top.jsp" />
</c:if>
<%--<jsp:include page="../navigation.jsp" />--%>
<!-- Page Content -->
<div id="page-wrapper">
    <div class="container-fluid">
        <!-- start Content -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">ดูรายละเอียดพระ</h1>
            </div>
        </div>

        <form:form action="${action}" method="POST" modelAttribute="Monk" enctype="multipart/form-data">

            <div class="row">
                <div class="col-lg-12">

                    <div class="tab-content">
                        <div class="tab-pane fade active in" id="Profile">
                            <h4>ข้อมูลส่วนตัว</h4>
                            <p></p>
                            <div class="row" style="font-size: 20px">
                                <div class="col-lg-1"></div>
                                <div class="col-lg-4">
                                    <dl class="dl-horizontal">           
                                        <dt>ชื่อปัจจุบัน:</dt> <dd> ${firstname} </dd>
                                        <dt>ฉายา:</dt> <dd> ${Monk.nickname}</dd>
                                        <dt>นามสกุล:</dt> <dd>${Monk.lastname}</dd>
                                        <dt>หมายเลขบัตรประชาชน:</dt> <dd>${Monk.personid} </dd>
                                        <dt>วิทยฐานะ: </dt> <dd>${Monk.position}</dd>
                                        <dt>สังกัดนิกาย: </dt> <dd>${Monk.sect}</dd>
                                        
                                    </dl>
                                </div>
                                <div class="col-lg-4">
                                    <dl class="dl-horizontal">
                                        <dt>วัด:</dt>
                                        <dd>  
                                           <c:forEach var="temple" items="${temple}" varStatus="loop">
                                                        <c:if test="${Temple_id eq temple.id}">
                                                        ${temple.name}
                                                        </c:if> 
                                                    </c:forEach>     
                                        </dd>
                                        <dt>ที่อยู่:</dt> <dd>${Monk.address}</dd>
                                        <dt>ตำบล:</dt> <dd>${Monk.district}</dd>
                                        <dt>อำเภอ:</dt> <dd>${Monk.amphoe}</dd>
                                        <dt>จังหวัด:</dt> <dd>${Monk.province}</dd>
                                        <dt>รหัสไปรษณีย์:</dt><dd>${Monk.postcode} </dd>
                                    </dl>
                                </div>
                            </div>
                            <hr>

                            <form:hidden path="monk_id" value ="${monk_id}"/>






                        </div>

                    </div>

                </div>
            </div>

        </form:form>
        <!-- End Content -->
    </div>
</div>
<!-- End Page Content -->
<script>
    function succesRegis() {
        alert("ลงทะเบียนเสร็จสิ้น");
    }
</script>

<jsp:include page="../footer.jsp" /> 


