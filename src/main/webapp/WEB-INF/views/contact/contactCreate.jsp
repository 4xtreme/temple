<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html;charset=UTF-8" %>
<jsp:include page="../header.jsp" />
<jsp:include page="../navigation.jsp" />
<!-- Page Content -->
    <div id="page-wrapper">
        <div class="container-fluid">
		<!-- start Content -->
            <div class="row">
				<div class="col-lg-12">
                        <h1 class="page-header">Create Contact</h1>
                </div>
			</div>
			
                <form:form action="${action}" method="POST" modelAttribute="Contact">
                
			<div class="row">
				<div class="col-lg-12">
				
								<div class="tab-content">
                                    <div class="tab-pane fade active in" id="Profile">
                                        <h4>Create Info</h4>
                                        <p></p>
								<div class="row">
								<div class="col-lg-6">
								<dl class="dl-horizontal">
                                   
									<dt>First Name:</dt> <dd> <form:input class="form-control"   path="firstname"  /><form:errors path="firstname" /> </dd>
								    <dt>Last Name:</dt> <dd><form:input class="form-control"   path="lastname"  /><form:errors path="lastname" /></dd>
									<dt>Phone1:</dt> <dd><form:input class="form-control"   path="phone1"  /><form:errors path="phone1" /></dd>
									<dt>Phone2: </dt> <dd><form:input class="form-control"   path="phone2"  /><form:errors path="phone2" /></dd>
									
                                </dl>
								</div>
								<div class="col-lg-6">
								<dl class="dl-horizontal">
                                                                        <dt>Member Type:</dt> <dd><form:input class="form-control"   path="member_type"  /><form:errors path="member_type" /></dd>
									<dt>Email:</dt> <dd><form:input class="form-control"   path="email"  /><form:errors path="email" /></dd>
								    <dt>Facebook:</dt> <dd><form:input class="form-control"   path="facebook"  /><form:errors path="facebook" /></dd>
									<dt>Line:</dt> <dd><form:input class="form-control"   path="line"  /><form:errors path="line" /></dd>
									
                                </dl>
								</div>
								</div>
								<hr>
								<h4>Address</h4>
                                 <p></p>
										<div class="row">
								<div class="col-lg-6">
								<dl class="dl-horizontal">
                                   
									<dt>Address:</dt> <dd><form:input class="form-control"   path="address1"  /><form:errors path="address1" /></dd>
								    <dt>Address2:</dt> <dd><form:input class="form-control"   path="address2"  /><form:errors path="address2" /></dd>
									<dt>Sub-district:</dt> <dd><form:input class="form-control"   path="district"  /><form:errors path="district" /></dd>
									<dt>District: </dt> <dd><form:input class="form-control"   path="city"  /><form:errors path="city" /></dd>
									
                                </dl>
								</div>
								<div class="col-lg-6">
								<dl class="dl-horizontal">
                                    
									<dt>Province:</dt> <dd><form:input class="form-control"   path="country"  /><form:errors path="country" /></dd>
								    <dt>Postal code:</dt> <dd><form:input class="form-control"   path="postalcode"  /><form:errors path="postalcode" /></dd>
									
									
                                </dl>
								</div>
								</div>
                                                                    <form:hidden path="customer_id" value ="${customerId}"/>
                                                                     <form:hidden path="user_id" value ="${userId}"/>  
                                                                    
                                 <div class="form-group text-right"><button type="submit" class="btn btn-primary"> <span class="fa fa-save "></span> Save</button></div>

                                    </div>
                               
                                </div>
						
				  </div>
			 </div>
			
                        </form:form>
			<!-- End Content -->
        </div>
    </div>
	<!-- End Page Content -->


 <jsp:include page="../footer.jsp" />

   
