<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html;charset=UTF-8" %>
<jsp:include page="../header.jsp" />
<jsp:include page="../navigation.jsp" />
<!-- Page Content -->
<div id="page-wrapper">
    <div class="container-fluid">
        <!-- start Content -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">ลงทะเบียนผ่านบัตร</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover">
                        <thead>
                            <tr>
                                <th class="text-center">ลำดับ</th>
                                <th class="text-center">ชื่อ-นามสกุล</th>
                                <th class="text-center">ดูรายละเอียด</th>
                                <th class="text-center">แก้ไข</th>
                                <th class="text-center">ลบ</th>

                            </tr>
                        </thead>
                        <tbody>

                            <c:forEach var="registerlist" items="${registerlist}" varStatus="loop">
                                <tr>
                                    <td class="text-center">${registerlist.reg_id}</td>
                                    <td class="text-center">${registerlist.firstname} &nbsp;&nbsp;${registerlist.lastname}</td>
                                    <td class="text-center">
                                        <!--                                        <form  action="selectview" method="post" >    
                                                                                    <input type="hidden" value="${registerlist.reg_id}" name="reg_id" />
                                                                                    <button type="submit"  class="no-border tran-bg"><i  style="color: green;" class="fa fa-search-plus" style="font-size: 30px"></i></button>
                                                                                </form>  -->
                                        <button type="button" class="no-border tran-bg" data-toggle="modal" data-target="#flipFlop${registerlist.reg_id}">
                                            <i  style="color: green;" class="fa fa-search-plus" style="font-size: 30px"></i>
                                        </button>
                                    </td>
                                    <td class="text-center">
                                        <c:if test="${page_Edit != null}">
                                            <form  action="selectedit" method="post" >
                                                <input type="hidden" value="${registerlist.reg_id}" name="reg_id"/>
                                                <button type="submit" class="no-border tran-bg "><i class="fa fa-edit edit-btn" style="font-size: 30px"></i></button>
                                            </form>
                                        </c:if>
                                    </td>
                                    <td class="text-center">
                                        <c:if test="${page_Edit != null}">
                                            <form  id="delete_${registerlist.reg_id}" action="deleteregs" method="post" >                                                                  
                                                <input type="hidden" value="${registerlist.reg_id}" name="reg_id"/>
                                                <div  onclick="chkConfirm(${registerlist.reg_id});" ><i style="color: red;" class="fa fa-trash fa-2x" aria-hidden="true"></i></div>
                                            </form>
                                        </c:if>
                                    </td>
                                </tr>
                            </c:forEach>


                        </tbody>
                    </table>
                </div>
                <div class="row">
                    <center>
                        <div class="pagination_center">
                            <nav aria-label="Page navigation col-centered">
                                <ul class="pagination">
                                    <c:if test="${page_priv == true}">  
                                        <li class="page-item"><a class="page-link"  href="${pageContext.request.contextPath}/regsall?page=${page-1}"><i class="fa fa-angle-left  "></i></a></li>
                                            </c:if>
                                            <c:if test="${page_count != 0}">
                                                <c:forEach begin="0" end="${page_count-1}" varStatus="loop">
                                                    <c:if test="${page == loop.index}"> 
                                                <li class="page-item"><a class="page-link active"  href="${pageContext.request.contextPath}/regsall?page=${loop.index}">${loop.index+1}</a></li>
                                                </c:if> 
                                                <c:if test="${page != loop.index}"> 
                                                <li class="page-item"><a class="page-link"  href="${pageContext.request.contextPath}/regsall?page=${loop.index}">${loop.index+1}</a></li>
                                                </c:if> 
                                            </c:forEach>
                                        </c:if>
                                        <c:if test="${page_next == true}"> 
                                        <li class="page-item"><a class="page-link" href="${pageContext.request.contextPath}/regsall?page=${page+1}"><i class="fa fa-angle-right "></i></a></li>
                                            </c:if>
                                </ul>
                            </nav>
                        </div>
                </div><!--end row--><br><br><br>
            </div>
        </div>

    </div>


</div>

<!-- End Content -->
<script language="JavaScript">
    function chkConfirm(reg_id) {
        bootbox.confirm({
            message: "คุณแน่ใจหรือว่าต้องการลบ ?",
            buttons: {
                confirm: {
                    label: 'ยืนยัน',
                    className: 'btn-success'
                },
                cancel: {
                    label: 'ยกเลิก',
                    className: 'btn-danger'
                }
            },
            callback: function (result) {
                if (result) {
                    $("#delete_" + reg_id).submit();
                }
                console.log('This was logged in the callback: ' + result + "#delete_" + reg_id);
            }
        });
    }
</script>








<jsp:include page="../footer.jsp" />
