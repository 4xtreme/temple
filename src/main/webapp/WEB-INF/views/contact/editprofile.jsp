<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html;charset=UTF-8" %>
<jsp:include page="../header.jsp" />
<jsp:include page="../navigation_contact.jsp" />
<!-- Page Content -->
<div id="page-wrapper">
    <div class="container-fluid">

        <!-- start Content -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">แก้ไขข้อมูลส่วนตัว</h1>
            </div>
        </div>

        <form:form action="savecontactprofile" method="POST" modelAttribute="Register" nctype="multipart/form-data">

            <div class="row">
                <div class="col-lg-12">

                    <div class="tab-content">
                        <div class="tab-pane fade active in" id="Profile">
                            <h4>แก้ไขข้อมูลส่วนตัว</h4>
                            <p></p>
                            <div class="row">
                                <div class="col-lg-1"></div>
                                <div class="col-lg-4">
                                    <dl class="dl-horizontal">
                                        <dd>
                                            <form:select class="form-control"  path="nametitle">
                                                <form:option value="นาย">นาย</form:option>
                                                <form:option value="นาง">นาง</form:option>
                                                <form:option value="นางสาว">นางสาว</form:option>
                                            </form:select>
                                        </dd>
                                        <dt>ชื่อ:</dt> <dd> <form:input class="form-control"   path="firstname"  /></dd>
                                        <dt>นามสกุล:</dt> <dd><form:input class="form-control"   path="lastname"  /></dd>
                                        <dt>หมายเลขบัตรประชาชน:</dt> <dd><form:input class="form-control"   path="personid" maxlength="13" name="personid" /></dd>
                                        <dt>สัญชาติ: </dt> <dd><form:input class="form-control"   path="royal" name="royal" /></dd>
                                        <dt>เชื้อชาติ:</dt> <dd><form:input class="form-control"   path="nationality" name="nationality" /></dd>

                                    </dl>
                                </div>
                                <BR>
                                <div class="col-lg-4">
                                    <dl class="dl-horizontal">

                                        <dt>วันเกิด:</dt> <dd><form:input class="form-control form_birthdate-1"  path="bd_birth" name="bd_birth" placeholder="ปปปป/เดือน/วัน"/></dd>
                                        <dt>เพศ:</dt> <dd>
                                            <form:select class="form-control" name="gender" path="gender">
                                                <form:option value="ชาย">ชาย</form:option>
                                                <form:option value="หญิง">หญิง</form:option>
                                            </form:select>
                                        </dd>
                                        <dt>เบอร์โทรศัพท์:</dt> <dd><form:input class="form-control" name="telephone"  path="telephone"  maxlength="10" /></dd>
                                        <dt>อีเมล</dt> <dd><form:input class="form-control" path="email" name="email" /></dd><dd><form:errors class="text-danger" path="email"/></dd>
                                    </dl>
                                </div>
                            </div>

                            <h4>ที่อยู่</h4>
                            <p></p>
                            <div class="row">
                                <div class="col-lg-1"></div>
                                <div class="col-lg-4">
                                    <dl class="dl-horizontal">

                                        <dt>บ้านเลขที่</dt> <dd><form:input class="form-control"  name="address" path="address"/></dd>
                                        <dt>หมู่:</dt> <dd><form:input class="form-control"  name="village" path="village"  /></dd>
                                        <dt>ตำบล:</dt> <dd><form:input class="form-control" name="district"  path="district"  /></dd>
                                        <dt>อำเภอ: </dt> <dd><form:input class="form-control"  name="city" path="city"  /></dd>

                                    </dl>
                                </div>

                                <div class="col-lg-4">
                                    <dl class="dl-horizontal">

                                        <dt>จังหวัด:</dt> <dd><form:input class="form-control" name="province"  path="province"  /></dd>
                                        <dt>รหัสไปรษณีย์:</dt> <dd><form:input class="form-control" name="postalcode"  path="postalcode" maxlength="5" /></dd>
                                        <dt>รหัสผ่าน:</dt> <dd><form:input class="form-control" id="password" type="password" path="password"  /></dd>
                                        <dt>ยืนยันรหัสผ่าน:</dt> <dd><form:input class="form-control" type="password" id="repassword"  path="repassword"  /></dd>


                                    </dl>
                                </div>
                            </div>

                            <form:hidden path="reg_id" value ="${reg_id}"/>

                            <div class="col-lg-5"></div>
                            <button type="submit" class="btn btn-primary" id="submit"> <span class="fa fa-save "></span>บันทึก</button>

                            <BR><BR><BR><BR> <BR>
                        </div>

                    </div>

                </div>

            </div>
        </form:form>
        <!-- End Content -->

    </div>
</div>
<!-- End Page Content -->
<script>
//    function succesRegis() {
//        var password = $("#password").val();
//        var repassword = $("#repassword").val();
//        if (password != repassword) {
//            alert('กรุณาใส่รหัสผ่านให้ตรงกัน!!!!!!');
//        } else {
//            $("#addevents").submit();
//        }
//    }

    $(function () {
        $("#submit").click(function () {
            var password = $("#password").val();
            var confirmPassword = $("#repassword").val();
            if (password != confirmPassword) {
                alert("รหัสผ่านไม่ตรงกัน!");
                return false;
            }
            return true;
        });
    });

</script>


<jsp:include page="../footer.jsp" /> 


