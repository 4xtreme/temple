<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html;charset=UTF-8" %>
<jsp:include page="../header.jsp" />
<c:if test="${roleadmin == 'admin'}">
    <jsp:include page="../navigation.jsp" />
</c:if>
<c:if test="${roleadmin == 'user'}">
    <jsp:include page="../navigation_top.jsp" />
</c:if>
<!-- Page Content -->
<style>
    label {
        margin-bottom: 5px;
        margin-top: 10px;
    }

    .error-msg {
        color: red;
    }

</style>
<div id="page-wrapper">
    <div class="container-fluid">
        <!-- start Content -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header" id="title_head">เพิ่มบุคลากรในวัด</h1>
            </div>
        </div>
        <form:form action="${action}" id="MonkForm" method="POST" modelAttribute="Monk" enctype="multipart/form-data" >
            <fieldset>

                <div class="container">

                    <div class="col-lg-6">
                        <label for="monk_type">ประเภท:</label>
                        <form:errors class="text-danger" path="monk_type" />
                        <form:select id="monk_type" class="form-control"   path="monk_type" name="monk_type" >
                            <form:option value="พระ">พระ</form:option>
                            <form:option value="สามเณร">สามเณร</form:option>
                            <form:option value="พระสัทธิวิหาริก">พระสัทธิวิหาริก</form:option>
                            <form:option value="พระอาคันตุกะ">พระอาคันตุกะ</form:option>
                            <form:option value="สามเราสิทธิวิหาริก">สามเราสิทธิวิหาริก</form:option>
                            <form:option value="ภิกษุณี">ภิกษุณี</form:option>
                            <form:option value="แม่ชี">แม่ชี</form:option>
                            <form:option value="แม่ชีอาคันตุกะ">แม่ชีอาคันตุกะ</form:option>
                            <form:option value="ภิกษุต่างประเทศ">ภิกษุต่างประเทศ</form:option>
                            <form:option value="แม่ชีต่างประเทศ">แม่ชีต่างประเทศ</form:option>
                            <form:option value="โยคีประจำ">โยคีประจำ</form:option>
                            <form:option value="จิตอาสา">จิตอาสา</form:option>
                        </form:select>
                        <label for="firstname">ชื่อปัจจุบัน:</label>
                        <form:errors class="text-danger" path="firstname" />
                        <form:input id="firstname" class="form-control"   path="firstname" name="firstname" />
                        <label for="nickname">ฉายา:</label>
                        <form:errors class="text-danger" path="nickname" />
                        <form:input id="nickname" class="form-control"   path="nickname" name="nickname" /> 
                        <label for="lastname">นามสกุล:</label>
                        <form:errors class="text-danger" path="lastname" />
                        <form:input id="lastname" class="form-control"   path="lastname" name="lastname" />
                        <label for="personid">หมายเลขบัตรประชาชน:</label>
                        <form:errors class="text-danger" path="personid" />
                        <form:input id="personid" class="form-control"   path="personid" maxlength="13" name="personid"/>
                        <label for="position">วิทยฐานะ:</label>
                        <form:errors class="text-danger" path="position" />
                        <form:input id="position" class="form-control"   path="position" name="position" />
                        <label for="sect">สังกัดนิกาย:</label>
                        <form:errors class="text-danger" path="sect" />
                        <form:input id="sect" class="form-control"   path="sect" name="sect" />
                    </div>
                    <div class="col-lg-6">
                        <label for="address">ที่อยู่:</label> 
                        <form:errors class="text-danger" path="address" />
                        <form:input id="address" class="form-control"   path="address" name="address"/>
                        <label for="district">ตำบล:</label> 
                        <form:errors class="text-danger" path="district" />
                        <form:input id="district" class="form-control"   path="district" name="district" />
                        <label for="amphoe">อำเภอ: </label> 
                        <form:errors class="text-danger" path="amphoe" />
                        <form:input id="amphoe" class="form-control"   path="amphoe"  name="amphoe"/>
                        <label for="province">จังหวัด:</label> 
                        <form:errors class="text-danger" path="province" />
                        <form:input id="province" class="form-control"   path="province" name="province" />
                        <label for="postcode">รหัสไปรษณีย์:</label> 
                        <form:errors class="text-danger" path="postcode" />
                        <form:input id="postcode" class="form-control"   path="postcode" maxlength="5" name="postcode"/>
                        <label for="file">รูปภาพ:</label> 
                        <form:input type="file" id="pic_monk" name="pic_monk" path="pic_monk" />
                    </div>
                </div>
            </fieldset>
            <hr>
            <c:if test="${roleadmin == 'admin'}">
                <div class="col-lg-5"></div>
                <div class="form-group text-left"><button type="button" class="btn btn-primary" onclick="succesRegis()"> <span class="fa fa-save "></span> บันทึก</button></div>
            </c:if>
            <c:if test="${roleadmin == 'user'}">
                <div class="col-lg-12" style="text-align: center;">
                    <div class="form-group text-center"><button type="button" class="btn btn-primary" onclick="succesRegis()"> <span class="fa fa-save "></span> บันทึก</button></div>
                </div>

            </c:if>
        </form:form>
        <!-- End Content -->
    </div>
    <!-- End Page Content -->
    <script>
        function succesRegis() {
            
                $("#MonkForm").submit();
            
        }

    </script>


    <jsp:include page="../footer.jsp" /> 


