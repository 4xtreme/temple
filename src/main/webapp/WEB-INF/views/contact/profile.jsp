<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html;charset=UTF-8" %>
<jsp:include page="../header.jsp" />
<jsp:include page="../navigation_contact.jsp" />
<!-- Page Content -->
<div id="page-wrapper">
    <div class="container-fluid">
        <!-- start Content -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">ข้อมูลส่วนตัว</h1>
            </div>
        </div>

        <form:form action="editcontactprofile" method="POST" modelAttribute="Register" >

            <div class="row">
                <div class="col-lg-12">
                    <div class="tab-content">
                        <div class="tab-pane fade active in" id="Profile">
                            <h4>ข้อมูลส่วนตัว</h4>
                            <p></p>
                            <div class="row">
                                <div class="col-lg-1"></div>
                                <div class="col-lg-4">
                                    <dl class="dl-horizontal">
                                        <dd>${nametitle}</dd>
                                        <dt>ชื่อ:</dt> <dd>${Register.firstname}</dd>
                                        <dt>นามสกุล:</dt> <dd>${Register.lastname}</dd>
                                        <dt>หมายเลขบัตรประชาชน:</dt> <dd>${Register.personid}</dd>
                                        <dt>สัญชาติ: </dt> <dd>${Register.royal}</dd>
                                        <dt>เชื้อชาติ:</dt> <dd>${Register.nationality}</dd>
                                    </dl>
                                </div>
                                <div class="col-lg-4">
                                    <dl class="dl-horizontal">
                                        <br>
                                        <dt>วันเกิด:</dt> <dd>${Register.bd_birth}</dd>
                                        <dt>เพศ:</dt> <dd>${gender}</dd>
                                        <dt>เบอร์โทรศัพท์:</dt> <dd>${Register.telephone}</dd>
                                        <dt>อีเมล:</dt> <dd>${Register.email}</dd></dl>
                                </div>
                            </div> 
                            <h4>ที่อยู่</h4>
                            <p></p>
                            <div class="row">
                                <div class="col-lg-1"></div>
                                <div class="col-lg-4">
                                    <dl class="dl-horizontal">

                                        <dt>บ้านเลขที่</dt> <dd>${Register.address}</dd>
                                        <dt>หมู่:</dt> <dd>${Register.village}</dd>
                                        <dt>ตำบล:</dt> <dd>${Register.district}</dd>
                                    </dl>
                                </div>
                                <div class="col-lg-4">
                                    <dl class="dl-horizontal">
                                        <dt>อำเภอ: </dt> <dd>${Register.city}</dd>
                                        <dt>จังหวัด:</dt> <dd>${Register.province}</dd>
                                        <dt>รหัสไปรษณีย์:</dt><dd>${Register.postalcode}</dd>
                                    </dl>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form:form>
        <!-- End Content -->
    </div>
</div>
<!-- End Page Content -->

<jsp:include page="../footer.jsp" /> 


