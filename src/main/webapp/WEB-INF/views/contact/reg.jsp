<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html;charset=UTF-8" %>
<jsp:include page="../header.jsp" />
<jsp:include page="../bar.jsp" />
<!-- Page Content --> 
<div id="page-wrapper">
    <div class="container-fluid">
        <!-- start Content -->
        <div class="row">
            <div class="col-lg-10">
                <center>
                    <h1 class="page-header">สมัครสมาชิก</h1></center>
            </div>
        </div>

        <form:form action="${action}" id="addevents" method="POST" modelAttribute="Register">
            <div class="row">
                <div class="col-lg-10">
                    <div class="tab-content">
                        <div class="tab-pane fade active in" id="Profile">
                            <div class="row">
                                <div class="col-lg-2">
                                </div>
                                <div class="col-lg-6">
                                </div>
                            </div>

                            <div class="row">
                                <div class="row">
                                    <div class="col-lg-3">
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <dt>วัด</dt> 
                                            <dd>
                                                <form:select path="temple_id" class="form-control" style="width:60%">
                                                <option  value="" disabled=" " selected="">เลือกรายการ</option>
                                                <c:forEach var="temple" items="${temple}" varStatus="loop">
                                                    <option  value="${temple.id}">${temple.name}</option>
                                                </c:forEach>
                                            </form:select><form:errors class="text-danger" path="temple_id" />
                                            </dd><br><br>
                                        </div>
                                    </div>   
                                    <div>
                                        <div class="col-lg-3">
                                        </div>
                                        <div class="col-lg-4">        
                                            <h4>ข้อมูลส่วนตัว</h4>
                                            <form:select class="form-control types" id="nametitle" name="nametitle" path="nametitle" style="width:30%">
                                                <form:option value="0">คำนำหน้า</form:option>
                                                <form:option value="นาย">นาย</form:option>
                                                <form:option value="นาง">นาง</form:option>
                                                <form:option value="นางสาว">นางสาว</form:option>
                                                <form:option value="พระ">พระ</form:option>
                                                <form:option value="แม่ชี" >แม่ชี</form:option>
                                                <form:option value="สามเณร">สามเณร</form:option>
                                            </form:select><br>
                                            <div id="nickname" style="display: none">    
                                                ฉายา: <form:input class="form-control"   path="nickname" id="nickname" name="nickname" style="width:60%;"/><form:errors class="text-danger" path="nickname"/><br>
                                            </div>
                                            ชื่อ:  <form:input class="form-control"   path="firstname"  name="firstname" style="width:60%"/><form:errors class="text-danger" path="firstname"/><br>
                                            นามสกุล: <form:input class="form-control"   path="lastname" name="lastname" style="width:60%" /><form:errors class="text-danger" path="lastname"/><br>
                                            หมายเลขบัตรประชาชน: <form:input class="form-control"   path="personid" maxlength="13" autocomplete="off" name="personid" style="width:60%"/><form:errors class="text-danger" path="personid"/>
                                            <c:if test="${invalidpersonID!=null}"><div style="color: red;">เลขบัตรประจำตัวประชาชนถูกใช้ไปแล้ว</div></c:if><br>
                                            สัญชาติ:  <form:input class="form-control"   path="royal" name="royal" style="width:60%"/><form:errors class="text-danger" path="royal" /><br>
                                            เชื้อชาติ: <form:input class="form-control"   path="nationality" name="nationality" style="width:60%"/><form:errors class="text-danger" path="nationality" />
                                        </div>

                                        <div class="col-lg-4">
                                            <h4 style="visibility:hidden;">ข้อมูลส่วนตัว</h4>
                                            <select class="form-control" style="width:30%;visibility:hidden;" >
                                            </select><br>
                                            เพศ: 
                                            <form:select class="form-control" name="gender" path="gender" style="width:25%">
                                                <form:option value="0">เพศ</form:option>
                                                <form:option value="ชาย">ชาย</form:option>
                                                <form:option value="หญิง">หญิง</form:option>
                                                <form:option value="ต่างชาติชาย">ต่างชาติชาย</form:option>
                                                <form:option value="ต่างชาติหญิง">ต่างชาติหญิง</form:option>
                                                <form:option value="ภิกษุ">ภิกษุ</form:option>
                                                <form:option value="ภิกษุณี">ภิกษุณี</form:option>
                                                <form:option value="สามเณร">สามเณร</form:option>
                                            </form:select>
                                            วันเกิด: <form:input style="width:60%" class="form-control form_birthdate-1"  path="bd_birth" name="bd_birth" placeholder="ปี/เดือน/วัน"/><br>
                                            เบอร์โทรศัพท์: <form:input class="form-control" name="telephone"  path="telephone"  maxlength="10" style="width:60%"/><form:errors class="text-danger" path="telephone"/><br>
                                            อีเมล: <form:input class="form-control" path="email" name="email" style="width:60%"/><form:errors class="text-danger" path="email"/>
                                            <c:if test="${invalidmail!=null}"><div style="color: red;">อีเมลไม่ถูกต้องหรืออีเมลซ้ำ</div></c:if><br>
                                            รหัสผ่าน: <form:input class="form-control" name="password" type="password" path="password" style="width:60%" /><form:errors class="text-danger" path="password" /><br>
                                            ยืนยันรหัสผ่าน: <form:input class="form-control" type="password" name="repassword"  path="repassword" style="width:60%" /><form:errors class="text-danger" path="repassword" />
                                        </div>
                                    </div>
                                </div>

                                <hr>

                                <div class="row">
                                    <div class="col-lg-3">
                                    </div>
                                    <div class="col-lg-4">
                                        <h4>ที่อยู่</h4>
                                        ที่อยู่ <form:input class="form-control"  name="address" path="address"  style="width:60%"/><form:errors class="text-danger" path="address" /><br>
                                        อาชีพ<form:input class="form-control"  name="village" path="village"  style="width:60%"/><form:errors class="text-danger" path="village" /><br>
                                        ตำบล: <form:input class="form-control" name="district"  path="district"  style="width:60%"/><form:errors class="text-danger" path="district" /><br>
                                    </div>
                                    <div class="col-lg-4">
                                        <h4 style="visibility:hidden;">fff</h4>
                                        อำเภอ:  <form:input class="form-control"  name="city" path="city"  style="width:60%"/><form:errors class="text-danger" path="city" /><br>
                                        จังหวัด: <form:input class="form-control" name="province"  path="province"  style="width:60%"/><form:errors class="text-danger" path="province" /><br>
                                        รหัสไปรษณีย์: <form:input class="form-control" name="postalcode"  path="postalcode" maxlength="5" style="width:60%"/><form:errors class="text-danger" path="postalcode" /><br>
                                    </div>
                                </div>

                                <form:hidden path="reg_id" value ="${reg_id}"/>

                                <br>
                                <center>
                                    <c:if test="${invalidcap!=null}"><div style="color: red;">คุณติด captcha</div></c:if>
                                        <div class="g-recaptcha"  data-sitekey="6Lebh1AUAAAAAF4gyidmYqjTxzz7sgLhWWDnejvD" name="gRecaptchaResponse" id="gRecaptchaResponse" align="center"></div><br>
                                    </center>
                                    <div class="form-group text-center"><button type="button" class="btn btn-primary" onclick="succesRegis()"> <span class="fa fa-save "></span>  บันทึก</button></div>
                                    </br> </br> </br>
                                </div>
                            </div>
                        </div>
                    </div>
            </form:form>
            <!-- End Content -->
        </div>
    </div>
    <!-- End Page Content -->
    <script>

        $(document).ready(function () {
            $('#nametitle').change(function () {
                if ($('#nametitle').val() == 'พระ' || $('#nametitle').val() == 'แม่ชี')
                {
                    $('#nickname').css('display', 'block');
                } else
                {
                    $('#nickname').css('display', 'none');
                }
            });
        });

        function succesRegis() {
            var temple_id = $("#temple_id").val();
            var nametitle = $("#nametitle").val();
            var firstname = $("#firstname").val();
            var lastname = $("#lastname").val();
            var personid = $("#personid").val();
            var password = $("#password").val();
            var repassword = $("#repassword").val();
            var royal = $("#royal").val();
            var nationality = $("#nationality").val();
            var gender = $("#gender").val();
            var bd_birth = $("#bd_birth").val();
            var telephone = $("#telephone").val();
            var email = $("#email").val();

            var address = $("#address").val();
            var village = $("#village").val();
            var district = $("#district").val();
            var city = $("#city").val();
            var province = $("#province").val();
            var postalcode = $("#postalcode").val();

            if (temple_id <= 0) {
                alert('กรุณาเลือกวัด');
                $("#temple_id").focus();
            } else if (nametitle == 0) {
                alert('กรุณาเลือกคำนำหน้า');
                $("#nametitle").focus();
            } else if (firstname == '') {
                alert('กรุณาใส่ชื่อ');
                $("#firstname").focus();
            } else if (lastname == '') {
                alert('กรุณาใส่นามสกุล');
                $("#lastname").focus();
            } else if (personid == '') {
                alert('กรุณาใส่เลขบัตรประจำตัว');
                $("#personid").focus();
            } else if (personid.length < 13) {
                alert('เลขบัตรประจำตัวไม่ถูกต้อง');
                $('#personid').focus();
            } else if (royal == '') {
                alert('กรุณาใส่สัญชาติ');
                $("#royal").focus();
            } else if (nationality == '') {
                alert('กรุณาใส่เชื้อชาติ');
                $("#nationality").focus();
            } else if (gender == 0) {
                alert('กรุณาเลือกเพศ');
                $("#gender").focus();
            } else if (bd_birth == '') {
                alert('กรุณาใส่วันเกิด');
                $("#bd_birth").focus();
            } else if (telephone == '') {
                alert('กรุณาใส่เบอร์โทรศัพท์');
                $("#telephone").focus();
            } else if (email == '') {
                alert('กรุณาใส่อีเมลล์');
                $("#email").focus();
            } else if (password == '') {
                alert('กรุณาใส่รหัสผ่าน');
                $("#password").focus();
            } else if (repassword == '') {
                alert('กรุณาใส่รหัสผ่านอีกครั้ง');
                $("#repassword").focus();
            } else if (address == '') {
                alert('กรุณาใส่ที่อยู่');
                $("#address").focus();
            }
//            else if (village == 0) {
//                alert('กรุณาใส่ที่อยู่');
//                $("#village").focus();
//            } 
            else if (district == '') {
                alert('กรุณาระบุตำบล');
                $("#district").focus();
            } else if (city == '') {
                alert('กรุณาระบุอำเภอ');
                $("#city").focus();
            } else if (province == '') {
                alert('กรุณาระบุจังหวัด');
                $("#province").focus();
            } else if (postalcode == '') {
                alert('กรุณาระบุรหัสไปรษณีย์');
                $("#postalcode").focus();
            } else if (postalcode.length < 5) {
                alert('รหัสไปรษณีย์ไม่ถูกต้อง');
                $('#postalcode').focus();
            } else if (password != repassword) {
                alert('กรุณาใส่รหัสผ่านให้ตรงกัน!!');
            } else {
                $("#addevents").submit();
            }
        }
    </script>



    <jsp:include page="../footer.jsp" /> 


