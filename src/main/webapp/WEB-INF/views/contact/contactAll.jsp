<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html;charset=UTF-8" %>
<jsp:include page="../header.jsp" />
<c:if test="${roleadmin == 'admin'}">
    <jsp:include page="../navigation.jsp" />
</c:if>
<c:if test="${roleadmin == 'user'}">
    <jsp:include page="../navigation_top.jsp" />
</c:if>

<!-- Page Content -->
<div id="page-wrapper">
    <div class="container-fluid">
        <!-- start Content -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header" id="title_head">สมาชิก</h1>
            </div>
        </div>
        <form action="searchreg" method="POST">
            <div>
                <select class="form-control width_15" name="typeSearch">
                    <option value="firstname">ชื่อ</option>
                    <option value="lastname">นามสกุล</option>
                    <option value="gender_text">เพศ</option>
                </select>&nbsp;
                <input type="text" class="form-control width_20" name="message">&nbsp;
                <button type="submit" class="btn btn-primary btn-signup width_10 align-center">
                    <i class="glyphicon glyphicon-search"></i> ค้นหา
                </button>
            </div>
        </form><br>
        <div class="row">
            <div class="col-lg-12">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover">
                        <thead>
                            <tr>
                                <th class="text-center">ลำดับ</th>
                                <th class="text-center">ชื่อ-นามสกุล</th>
                                <th class="text-center">เชื้อชาติ,สัญชาติ</th>
                                <th class="text-center">วันเดือนปีเกิด</th>
                                <th class="text-center">เพศ</th>
                                <th class="text-center">หมายเลขบัตรประชาชน</th>
                                <th class="text-center">อำเภอ</th>
                                <th class="text-center">จังหวัด</th>
                                <th class="text-center">สถานะ</th>
                                <th class="text-center">ดูรายละเอียด</th>
                                <th class="text-center">แก้ไข</th>
                                <th class="text-center">เปิด/ปิดสถานะการใช้งาน</th>
                            </tr>
                        </thead>
                        <tbody>
                            <c:forEach var="registerlist" items="${registerlist}" varStatus="loop">
                                <tr>
                                    <td class="text-center">${registerlist.reg_id}</td>
                                    <td class="text-center">${registerlist.firstname} &nbsp;&nbsp;${registerlist.lastname}</td>
                                    <td class="text-center">${registerlist.royal},&nbsp;&nbsp;${registerlist.nationality}</td>
                                    <td class="text-center">${registerlist.bd_birth}</td>
                                    <td class="text-center">${registerlist.gender_text}</td>
                                    <td class="text-center">${registerlist.personid}</td>
                                    <td class="text-center">${registerlist.city}</td>
                                    <td class="text-center">${registerlist.province}</td>
                                    <td class="text-center">
                                        <c:if test="${registerlist.is_delete == false}">
                                            ปกติ
                                        </c:if>
                                        <c:if test="${registerlist.is_delete == true}">
                                            ปิดการใช้งาน
                                        </c:if>    

                                    </td>
                                    <td class="text-center">
                                        <button type="button" class="no-border tran-bg" data-toggle="modal" data-target="#flipFlop${registerlist.reg_id}">
                                            <i  style="color: green;" class="fa fa-search-plus" style="font-size: 30px"></i>
                                        </button>
                                    </td>
                                    <td class="text-center">
                                        <c:if test="${page_Edit != null}">
                                            <form  action="selectedit" method="post" >
                                                <input type="hidden" value="${registerlist.reg_id}" name="reg_id"/>
                                                <button type="submit" class="no-border tran-bg "><i class="fa fa-edit edit-btn" style="font-size: 30px"></i></button>
                                            </form>
                                        </c:if>
                                    </td>
                                    <td class="text-center">
                                        <c:if test="${page_Edit != null}">
                                            <c:if test="${registerlist.is_delete == false}">
                                                <form  id="close_${registerlist.reg_id}" action="deleteregs" method="post" >                                                                  
                                                    <input type="hidden" value="${registerlist.reg_id}" name="reg_id"/>
                                                    <!--<div  onclick="chkConfirm(${registerlist.reg_id});" ><i style="color: green;" class="fa fa-lock-open fa-2x" aria-hidden="true"></i></div>-->
                                                    <div data-toggle="modal" data-target="#del${registerlist.reg_id}"  ><i style="color: green;" class="fa fa-lock-open fa-2x" aria-hidden="true"></i></div>
                                                </form>
                                            </c:if>
                                            <c:if test="${registerlist.is_delete == true}">
                                                <form  id="open_${registerlist.reg_id}" action="reopenregs" method="post" >                                                                  
                                                    <input type="hidden" value="${registerlist.reg_id}" name="reg_id"/>
                                                    <div  onclick="chkConfirmOpen(${registerlist.reg_id});" ><i style="color: red;" class="fa fa-lock fa-2x" aria-hidden="true"></i></div>
                                                </form>
                                            </c:if>
                                        </c:if>
                                    </td>
                                </tr>

                                <!--modal-->
                            <div class="modal fade" id="flipFlop${registerlist.reg_id}" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                            <h4 class="modal-title" id="modalLabel">ข้อมูล</h4>
                                        </div>
                                        <div class="modal-body">
                                            <p></p>
                                            <div class="row" style="font-size: 20px">
                                                <div class="col-lg-12 ">
                                                    <dl class="dl-horizontal">
                                                        <dt>ชื่อ-นามสกุล :</dt> <dd>${registerlist.firstname} ${registerlist.lastname}</dd>
                                                        <dt>เชื้อชาติ ,สัญชาติ :</dt> <dd>${registerlist.royal},&nbsp;&nbsp;${registerlist.nationality}</dd>
                                                        <dt>วันเดือนปีเกิด</dt> <dd>${registerlist.bd_birth}</dd>
                                                        <dt>เพศ</dt><dd>${registerlist.gender_text}</dd>
                                                        <dt>หมายเลขบัตรประชาชน</dt><dd>${registerlist.personid}</dd>
                                                        <c:if test="${not empty registerlist.drug}">
                                                            <dt>ข้าพเจ้า</dt><dd>${registerlist.drug}</dd>
                                                        </c:if>
                                                        <c:if test="${not empty registerlist.health}">
                                                            <dt>สุขภาพร่างกาย</dt><dd>${registerlist.health}</dd>
                                                        </c:if>
                                                        <c:if test="${not empty registerlist.mental}">
                                                            <dt>สุขภาพจิต</dt><dd>${registerlist.mental}</dd>
                                                        </c:if>
                                                        <dt>ที่อยู่</dt><dd>${registerlist.address}</dd>
                                                        <dt>ตำบล</dt><dd>${registerlist.district}</dd>
                                                        <dt>อำเภอ</dt><dd>${registerlist.city}</dd>
                                                        <dt>จังหวัด</dt><dd>${registerlist.province}</dd>
                                                        <dt>เบอร์โทรศัพท์</dt><dd>${registerlist.telephone}</dd>
                                                    </dl>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <c:if test="${page_Edit != null}">
                                                <form  action="selectedit" method="post" >
                                                    <input type="hidden" value="${registerlist.reg_id}" name="reg_id"/>
                                                    <button type="submit" class="btn btn-success">แก้ไขข้อมูล</button>
                                                </form>
                                            </c:if>
                                            <button type="button" class="btn btn-danger" data-dismiss="modal">ปิด</button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="modal fade" id="del${registerlist.reg_id}" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                            <h4 class="modal-title" id="modalLabel">คุณต้องการที่จะปิดการใช้งานหรือไม่ ?</h4>
                                        </div>
                                        <div class="modal-body">
                                            <p></p>
                                            <form  id="close_${registerlist.reg_id}" action="deleteregs" method="post" >                                                                  
                                                <input type="hidden" value="${registerlist.reg_id}" name="reg_id"/>
                                                <!--<div  onclick="chkConfirm(${registerlist.reg_id});" ><i style="color: green;" class="fa fa-lock-open fa-2x" aria-hidden="true"></i></div>-->
                                                <div class="form-group">
                                                    <label for="reason">เหตุผลที่ต้องปิดการใช้งาน</label>
                                                    <input type="text" class="form-control" id="reason" name="reason" placeholder="เหตุผลที่ปิดการใช้งาน" />
                                                </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="submit" class="btn btn-success">ยืนยัน</button>
                                            </form>

                                            <button type="button" class="btn btn-danger" data-dismiss="modal">ปิด</button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-content" style="display:none;">
                                <form  id="close_${registerlist.reg_id}" action="deleteregs" method="post" >                                                                  
                                    <input type="hidden" value="${registerlist.reg_id}" name="reg_id"/>
                                    <!--<div  onclick="chkConfirm(${registerlist.reg_id});" ><i style="color: green;" class="fa fa-lock-open fa-2x" aria-hidden="true"></i></div>-->
                                    <div class="form-group">
                                        <label for="reason">เหตุผลที่ต้องปิดการใช้งาน</label>
                                        <input type="text" class="form-control" id="reason" name="reason" placeholder="เหตุผลที่ปิดการใช้งาน" />
                                    </div>
                                </form>
                            </div>
                            <!------------------------------->
                        </c:forEach>
                        </tbody>
                    </table>
                </div>
                <div class="row">
                    <center>
                        <div class="pagination_center">
                            <nav aria-label="Page navigation col-centered">
                                <ul class="pagination">
                                    <c:if test="${page_priv == true}">  
                                        <li class="page-item"><a class="page-link"  href="${pageContext.request.contextPath}/regsall?page=${page-1}"><i class="fa fa-angle-left  "></i></a></li>
                                            </c:if>
                                            <c:if test="${page_count != 0}">
                                                <c:forEach begin="0" end="${page_count-1}" varStatus="loop">
                                                    <c:if test="${page == loop.index}"> 
                                                <li class="page-item"><a class="page-link active"  href="${pageContext.request.contextPath}/regsall?page=${loop.index}">${loop.index+1}</a></li>
                                                </c:if> 
                                                <c:if test="${page != loop.index}"> 
                                                <li class="page-item"><a class="page-link"  href="${pageContext.request.contextPath}/regsall?page=${loop.index}">${loop.index+1}</a></li>
                                                </c:if> 
                                            </c:forEach>
                                        </c:if>
                                        <c:if test="${page_next == true}"> 
                                        <li class="page-item"><a class="page-link" href="${pageContext.request.contextPath}/regsall?page=${page+1}"><i class="fa fa-angle-right "></i></a></li>
                                            </c:if>
                                </ul>
                            </nav>
                        </div>
                </div><!--end row--><br><br><br>
            </div>
        </div>

    </div>


</div>

<!-- End Content -->
<script language="JavaScript">
    function chkConfirm(reg_id) {
        bootbox.prompt({

            title: "คุณแน่ใจหรือว่าต้องการปิดการใช้งาน ?",
            inputType: 'text',
            buttons: {
                confirm: {
                    label: 'ยืนยัน',
                    className: 'btn-success'
                },
                cancel: {
                    label: 'ยกเลิก',
                    className: 'btn-danger'
                }
            },
            callback: function (result) {
                if (result) {
                    $("#close_" + reg_id).submit();
                }
                console.log('This was logged in the callback: ' + result + "#delete_" + reg_id);
            }
        });
    }

    function chkConfirmOpen(reg_id) {
        bootbox.confirm({
            message: "คุณแน่ใจหรือว่าต้องการเปิดการใช้งาน ?",
            buttons: {
                confirm: {
                    label: 'ยืนยัน',
                    className: 'btn-success'
                },
                cancel: {
                    label: 'ยกเลิก',
                    className: 'btn-danger'
                }
            },
            callback: function (result) {
                if (result) {
                    $("#open_" + reg_id).submit();
                }
                console.log('This was logged in the callback: ' + result + "#delete_" + reg_id);
            }
        });
    }



</script>








<jsp:include page="../footer.jsp" />