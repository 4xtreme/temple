<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html;charset=UTF-8" %>
<jsp:include page="../header.jsp" />
<c:if test="${roleadmin == 'admin'}">
    <jsp:include page="../navigation.jsp" />
</c:if>
<c:if test="${roleadmin == 'user'}">
    <jsp:include page="../navigation_top.jsp" />
</c:if>
<style>
    label {
        margin-bottom: 5px;
        margin-top: 10px;
    }

    .error-msg {
        color: red;
    }

</style>
<!-- Page Content --> 
<div id="page-wrapper">
    <div class="container-fluid">
        <!-- start Content -->
        <div class="row">
            <div class="col-lg-12">
                <center>
                    <h1 class="page-header">ลงทะเบียน</h1></center>
            </div>
        </div>

        <form:form action="regcreatenow" method="POST" id="addevents" modelAttribute="Register">
            <fieldset>
                <div class="container">
                    <div class="col-lg-3">                   
                        <label for="nametitle">คำนำหน้า:</label> 
                        <form:errors class="text-danger" path="nametitle"/>
                        <form:select class="form-control" name="nametitle" path="nametitle" id="nametitle" >
                            <form:option value="" disabled="true" selected="selected">คำนำหน้า</form:option>
                            <form:option value="เด็กชาย">เด็กชาย</form:option>
                            <form:option value="เด็กหญิง">เด็กหญิง</form:option>
                            <form:option value="นาย">นาย</form:option>
                            <form:option value="นาง">นาง</form:option>
                            <form:option value="นางสาว">นางสาว</form:option>
                            <form:option value="พระ">พระ</form:option>                           
                            <form:option value="แม่ชี">แม่ชี</form:option>                            
                            <form:option value="สามเณร">สามเณร</form:option>                           
                        </form:select>
                    </div>
                    <div class="col-lg-3"></div>
                    <div class="col-lg-3">
                        <label fo="drug">เกี่ยวกับยาเสพติด</label>
                        <form:select class="form-control" id="nametitle" path="drug" name="drug">
                            <form:option value="ไม่เคยเสพสารเสพติดใดๆเลย">ไม่เคยเสพสารเสพติดใดๆเลย</form:option>
                            <form:option value="เคยเสพสารเสพติดแต่ปัจจุบันเลิกแล้ว">เคยเสพสารเสพติดแต่ปัจจุบันเลิกแล้ว</form:option>
                            <form:option value="ปัจจุบันยังเสพอยู่">ปัจจุบันยังเสพอยู่</form:option>
                        </form:select>
                    </div>
                </div>
                <div class="container">
                    <div class="col-lg-6">
                        <label for="firstname">ชื่อ:</label> 
                        <form:errors class="text-danger" path="firstname"/>
                        <form:input class="form-control"   path="firstname" name="firstname" id="firstname"/>

                        <label for="lastname">นามสกุล:</label> 
                        <form:errors class="text-danger" path="lastname"/>
                        <form:input class="form-control"   path="lastname" name="lastname" id="lastname" />

                        <label for="personid">หมายเลขบัตรประชาชน:</label> 
                        <c:if test="${invalidpersonID!=null}"><div style="color: red;">เลขบัตรประจำตัวประชาชนถูกใช้ไปแล้ว</div></c:if>
                        <form:errors class="text-danger" path="personid"/>
                        <form:input class="form-control" id="personid"  path="personid" maxlength="13" autocomplete="off" name="personid" value=""/>

                        <label for="royal">สัญชาติ:</label> 
                        <form:errors class="text-danger" path="royal" />
                        <form:input class="form-control"   path="royal" name="royal" id="royal"/>

                        <label for="nationality">เชื้อชาติ:</label> 
                        <form:errors class="text-danger" path="nationality" />
                        <form:input class="form-control"   path="nationality" name="nationality" id="nationality" />

                        <label for="gender_text">เพศ:</label> 
                        <form:errors path="gender_text" class="text-danger"/>
                        <form:select class="form-control" name="gender_text" path="gender_text" id="gender_text" >
                            <form:option value="" disabled="true" selected="true">เพศ</form:option>
                            <form:option value="ชาย">ชาย</form:option>
                            <form:option value="หญิง">หญิง</form:option>
                            <form:option value="ต่างชาติชาย">ต่างชาติชาย</form:option>
                            <form:option value="ต่างชาติหญิง">ต่างชาติหญิง</form:option>
                            <form:option value="ภิกษุ">ภิกษุ</form:option>
                            <form:option value="ภิกษุณี">ภิกษุณี</form:option>
                            <form:option value="แม่ชี">แม่ชี</form:option>
                            <form:option value="สามเณร">สามเณร</form:option>
                        </form:select>

                        <label for="status">สถานะ:</label> 
                        <form:errors path="status" class="text-danger"/>
                        <form:select class="form-control" name="status" path="status" id="status" >
                            <form:option value="" disabled="true" selected="true">สถานะ</form:option>
                            <form:option value="บวชพระ">บวชพระ</form:option>
                            <form:option value="รอบวชพระ">รอบวชพระ</form:option>
                            <form:option value="รอบวชชี">รอบวชชี</form:option>
                            <form:option value="รอบวชเณร">รอบวชเณร</form:option>
                        </form:select>

                        <label for="bd_birth">วันเกิด:</label> 
                        <form:errors path="bd_birth" class="text-danger"/>
                        <form:input class="form-control form_birthdate-1" autocomplete="off"  path="bd_birth" name="bd_birth" id="bd_birth" placeholder="ปี/เดือน/วัน"/>
                        <form:input hidden="true"  path="email" value="contact@email.com" />
                        <form:input hidden="true"  path="password" value="1234" />
                        <form:input hidden="true"  path="repassword" value="1234" />

                        <label for="occupation">อาชีพ:</label>
                        <form:errors class="text-danger" path="occupation" />
                        <form:input class="form-control"  name="occupation" path="occupation" id="occupation"  />
                    </div>

                    <div class="col-lg-6">

                        <label for="nickname">สุขภาพ:</label>
                        <form:radiobutton class="form-check-input"  path="health" name="health" onclick="document.getElementById('unhealthtype').disabled = true;" id="inlineRadio1" value="ปกติ" />
                        <label class="form-check-label" for="inlineRadio1">แข็งแรง</label>&nbsp;&nbsp;


                        <form:radiobutton class="form-check-input"  path="health" name="health" onclick="document.getElementById('unhealthtype').disabled = false;" id="inlineRadio2" value="nostrong" />
                        <label class="form-check-label" for="inlineRadio2" id="unhealthcheck">มีโรคประจำตัว</label>
                        <input type="text" class="form-control" name="disease" disabled="" value="${disease}" id="unhealthtype" placeholder="โรคประจำตัวทีมี" >
                        <!--</div>-->


                        <label fo="drug">สุขภาพจิต:</label>
                        <form:select class="form-control" id="nametitle" path="mental" name="mental" onchange="checkMental(this)">
                            <form:option value="ปกติ">ปกติ</form:option>
                            <form:option value="เคยเข้าบำบัด">เคยเข้าบำบัด</form:option>
                            <form:option value="ปัจจุบันยังต้องทานยารักษาโรคจิตประสาทอยู่">ปัจจุบันยังต้องทานยารักษาโรคจิตประสาทอยู่</form:option>
                            <form:option value="ไม่ได้ทานยามานาน">ไม่ได้ทานยามานาน</form:option>
                        </form:select>

                        <div style="display: none" id="mental3">
                            <div class="col-lg-6" style="padding-left: 0px">
                                <label for="mental" style="">เคยเข้าบำบัดที่:</label>
                                <input type="text" id="nametitle" name="mentalwhere" value="${mentalwhere}" class="form-control" style="float: left" placeholder="เคยเข้าบำบัดที่:" >
                            </div>
                            <div class="col-lg-6" style="padding-right: 0px">
                                <label fo="mental" style="">เมื่อ:</label>
                                <input type="text" name="mentaltime" id="nametitle" value="${mentaltime}" class="form-control" placeholder="ครั้งล่าสุดที่เข้าการบำบัด(ปี)">
                            </div>

                        </div>
                        <div class="col-lg-12" style="display:none;padding-left: 0px;" id="mental4">
                            <div class="col-lg-6" style="padding-left: 0px">
                                <label fo="nickname" style="">ระยะเวลาที่ไม่ได้ทานยา(ปี):</label>
                                <input type="text" id="nametitle" name="mentalyear" value="${mentalyear}" class="form-control" placeholder="ระยะเวลาที่ไม่ได้ทานยา...ปี">
                            </div>
                        </div>



                        <label for="nickname">ฉายา:</label> 
                        <form:errors class="text-danger" path="nickname" /> 
                        <form:input class="form-control"  name="nickname" path="nickname" id="nickname" />

                        <label for="telephone">เบอร์โทรศัพท์:</label> 
                        <form:errors class="text-danger" path="telephone"/>
                        <form:input class="form-control" name="telephone"  path="telephone" id="telephone"  maxlength="10" />

                        <label for="address">ที่อยู่:</label> 
                        <form:errors class="text-danger" path="address" />
                        <form:input class="form-control"  name="address" path="address" id="address" />

                        <label for="district">ตำบล:</label>
                        <form:errors class="text-danger" path="district" />
                        <form:input class="form-control" name="district"  path="district" id="district"  />

                        <label for="city">อำเภอ:</label>
                        <form:errors class="text-danger" path="city" />
                        <form:input class="form-control"  name="city" path="city" id="city" />

                        <label for="province">จังหวัด:</label>
                        <form:errors class="text-danger" path="province" />
                        <form:input class="form-control" name="province"  path="province" id="province" />

                        <label for="postalcode">รหัสไปรษณีย์:</label>
                        <form:errors class="text-danger" path="postalcode" />
                        <form:input class="form-control" name="postalcode"  path="postalcode" id="postalcode" maxlength="5" />                               

                    </div>
                </div>
            </fieldset>

            <br>
            <div  class="form-group text-center">
                <button type="button" class="btn btn-primary" onclick="succesRegis()"> เพิ่มสมาชิก  <span class="fa fa-plus-circle "></span>  </button>
                <button type="button" class="btn btn-primary" onclick="regFromCard()">ดึงข้อมูลบัตร<span class=" "></span> </button>    
            </div>
        </form:form>
        <div  class="form-group text-center">
            <form action="regcreatenowcard" method="POST" id="regfromcard">

            </form>
        </div>
        <!-- End Content -->
    </div>
</div>
<!-- End Page Content -->
<script>
    function succesRegis() {
        $("#addevents").submit();
    }
    function regFromCard() {
        $('#regfromcard').submit();
    }
    function checkMental(that) {
        if (that.value === "เคยเข้าบำบัด") {
            document.getElementById("mental3").style.display = "block";
            document.getElementById("mental4").style.display = "none";
        } else if (that.value === "ไม่ได้ทานยามานาน") {
            document.getElementById("mental4").style.display = "block";
            document.getElementById("mental3").style.display = "none";
        } else {
            document.getElementById("mental3").style.display = "none";
            document.getElementById("mental4").style.display = "none";
        }
    }

</script>


<jsp:include page="../footer.jsp" />





