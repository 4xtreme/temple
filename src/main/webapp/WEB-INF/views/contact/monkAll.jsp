<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html;charset=UTF-8" %>
<jsp:include page="../header.jsp" />
<c:if test="${roleadmin == 'admin'}">
    <jsp:include page="../navigation.jsp" />
</c:if>
<c:if test="${roleadmin == 'user'}">
    <jsp:include page="../navigation_top.jsp" />
</c:if>
<!-- Page Content -->
<div id="page-wrapper">
    <div class="container-fluid">
        <!-- start Content -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header" id="title_head">บุคลากรในวัด</h1>
            </div>
        </div>
        <div class="row">
            <c:if test="${roleadmin == 'admin'}">
                <div class="col-lg-12">
                </c:if>
                <c:if test="${roleadmin == 'user'}">
                    <div class="col-lg-10 col-lg-offset-1">
                    </c:if>
                    <div>
                        <form method="POST" action="searchmonk"> 
                            <input class="form-control width_20" type="text" name="search">
                            <button type="submit" class="btn btn-primary btn-signup width_10 align-center">
                                <i class="glyphicon glyphicon-search"></i> ค้นหา
                            </button>
                        </form>
                    </div>
                    <br>
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th class="text-center">ลำดับ</th>
                                    <th class="text-center">ประเภท</th>
                                    <th class="text-center">รูป</th>
                                    <th class="text-center">ชื่อ-นามสกุล</th>
                                    <th class="text-center">ฉายา</th>
                                    <th class="text-center">วิทยฐานะ</th>
                                    <th class="text-center">สังกัด</th>                                  
                                    <th class="text-center">ดูรายละเอียด</th>
                                    <th class="text-center">แก้ไข</th>
                                    <th class="text-center">ลบ</th>
                                    <th class="text-center">เปิด/ปิดสถานะการใช้งาน</th>
                                </tr>
                            </thead>
                            <tbody>
                                <c:forEach var="monklist" items="${monklist}" varStatus="loop">
                                    <tr>
                                        <td class="text-center">${monklist.monk_id}</td>
                                        <td class="text-center">${monklist.monk_type}</td>
                                        <td class="text-center"><img src="data:image/jpg;base64, <c:out value='${monklist.monkString}'/>"width="104" height="142" /></td>
                                        <td class="text-center">${monklist.firstname} &nbsp;&nbsp;${monklist.lastname}</td>
                                        <td class="text-center">${monklist.nickname}</td>
                                        <td class="text-center">${monklist.position}</td>
                                        <td class="text-center">${monklist.sect}</td>                                       
                                        <td class="text-center">
                                            <!--                                        <form  action="viewMonk" method="post" >    
                                                                                        <input type="hidden" value="${monklist.monk_id}" name="monk_id" />
                                                                                        <button type="submit"  class="no-border tran-bg"><i  style="color: green;" class="fa fa-search-plus" style="font-size: 30px"></i></button>
                                                                                    </form>  -->
                                            <button type="button" class="no-border tran-bg" data-toggle="modal" data-target="#flipFlop${monklist.monk_id}">
                                                <i  style="color: green;" class="fa fa-search-plus" style="font-size: 30px"></i>
                                            </button>
                                        </td>
                                        <td class="text-center">
                                            <c:if test="${page_Edit != null}">
                                                <form  action="selectmonkedit" method="post" >
                                                    <input type="hidden" value="${monklist.monk_id}" name="monk_id"/>
                                                    <button type="submit" class="no-border tran-bg"><i class="fa fa-edit edit-btn" style="font-size: 30px"></i></button>
                                                </form>
                                            </c:if>
                                        </td>
                                        <td class="text-center">
                                            <c:if test="${page_Delete != null}">
                                                <form  id="delete_${monklist.monk_id}" action="deletemonk" method="post" >                                                                  
                                                    <input type="hidden" value="${monklist.monk_id}" name="monk_id"/>
                                                    <div  onclick="chkConfirm(${monklist.monk_id});" ><i style="color: red;" class="fa fa-trash fa-2x" aria-hidden="true"></i></div>
                                                </form>
                                            </c:if>
                                        </td>
                                        <td class="text-center">
                                            <c:if test="${page_Edit != null}">
                                                <c:if test="${monklist.is_deactivate == false}">
                                                    <form  id="deavtivate_${monklist.monk_id}" action="deactivatemonk" method="post" >                                                                  
                                                        <input type="hidden" value="${monklist.monk_id}" name="monk_id"/>
                                                        <div  onclick="chkDeactivate(${monklist.monk_id});" ><i style="color: green;" class="fa fa-lock-open fa-2x" aria-hidden="true"></i></div>
                                                    </form>
                                                </c:if>
                                                <c:if test="${monklist.is_deactivate == true}">
                                                    <form  id="activate_${monklist.monk_id}" action="activatemonk" method="post" >                                                                  
                                                        <input type="hidden" value="${monklist.monk_id}" name="monk_id"/>
                                                        <div  onclick="chkActivate(${monklist.monk_id});" ><i style="color: red;" class="fa fa-lock fa-2x" aria-hidden="true"></i></div>
                                                    </form>
                                                </c:if>
                                            </c:if>
                                        </td>
                                    </tr>
                                    <!-- The modal -->
                                <div class="modal fade" id="flipFlop${monklist.monk_id}" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                                <h4 class="modal-title" id="modalLabel">พระวิปัสสนาจารย์</h4>
                                            </div>
                                            <div class="modal-body">
                                                <p></p>
                                                <div class="row" style="font-size: 20px">
                                                    <div class="col-lg-12 ">
                                                        <dl class="dl-horizontal">
                                                            <dt>ฉายา</dt> <dd>${monklist.nickname}</dd>
                                                            <dt>ชื่อ:</dt> <dd>${monklist.firstname}</dd>
                                                            <dt>นามสกุล:</dt> <dd>${monklist.lastname}</dd>
                                                            <dt>หมายเลขบัตรประชาชน:</dt> <dd>${monklist.personid}</dd>
                                                            <dt>วิทยฐานะ </dt> <dd>${monklist.position}</dd>
                                                            <dt>สังกัด</dt> <dd>${monklist.sect}</dd>
                                                        </dl>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <c:if test="${page_Edit != null}">
                                                    <form  action="selectmonkedit" method="post" >
                                                        <input type="hidden" value="${monklist.monk_id}" name="monk_id"/>
                                                        <button type="submit" class="btn btn-primary">แก้ไขข้อมูล</button>
                                                    </form>
                                                </c:if>
                                                <button type="button" class="btn btn-danger" data-dismiss="modal">ปิด</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!------------------------------->
                            </c:forEach>
                            </tbody>
                        </table>
                    </div>
                    <div class="row" style="text-align: center;">
                        <div class="pagination_center">
                            <nav aria-label="Page navigation col-centered">
                                <ul class="pagination">
                                    <c:if test="${page_count != 0}">
                                        <c:forEach begin="0" end="${page_count-1}" varStatus="loop">
                                            <c:if test="${page == loop.index}"> 
                                                <li class="page-item">
                                                    <a class="page-link active"  href="${pageContext.request.contextPath}/monkall?page=${loop.index}">${loop.index+1}</a>
                                                </li>
                                            </c:if> 
                                            <c:if test="${page != loop.index}"> 
                                                <li class="page-item">
                                                    <a class="page-link"  href="${pageContext.request.contextPath}/monkall?page=${loop.index}">${loop.index+1}</a>
                                                </li>
                                            </c:if> 
                                        </c:forEach>
                                    </c:if>
                                    <c:if test="${page_next == true}"> 
                                        <li class="page-item">
                                            <a class="page-link" href="${pageContext.request.contextPath}/monkall?page=${page+1}">
                                                <i class="fa fa-angle-right "></i>
                                            </a>
                                        </li>
                                    </c:if>
                                </ul>
                            </nav>
                        </div>                    
                        <br/><br/><br/><br/>
                    </div><!--end row-->
                </div>
            </div>
        </div>
    </div>

    <!-- End Content -->
    <script language="JavaScript">
        function chkConfirm(monk_id) {
            bootbox.confirm({
                message: "Are you sure you want to delete?" + monk_id,
                buttons: {
                    confirm: {
                        label: 'Yes',
                        className: 'btn-success'
                    },
                    cancel: {
                        label: 'No',
                        className: 'btn-danger'
                    }
                },
                callback: function (result) {
                    if (result) {
                        $("#delete_" + monk_id).submit();
                    }
                    console.log('This was logged in the callback: ' + result + "#delete_" + monk_id);
                }
            });
        }

        function chkDeactivate(monk_id) {
            bootbox.confirm({
                message: "คุณแน่ใจหรือไม่ว่าต้องการยกเลิกการใช้งาน",
                buttons: {
                    confirm: {
                        label: 'Yes',
                        className: 'btn-success'
                    },
                    cancel: {
                        label: 'No',
                        className: 'btn-danger'
                    }
                },
                callback: function (result) {
                    if (result) {
                        $("#deavtivate_" + monk_id).submit();
                    }
                    console.log('This was logged in the callback: ' + result + "#deavtivate_" + monk_id);
                }
            });
        }

        function chkActivate(monk_id) {
            bootbox.confirm({
                message: "คุณแน่ใจหรือว่าต้องการเปิดใช้งาน",
                buttons: {
                    confirm: {
                        label: 'Yes',
                        className: 'btn-success'
                    },
                    cancel: {
                        label: 'No',
                        className: 'btn-danger'
                    }
                },
                callback: function (result) {
                    if (result) {
                        $("#activate_" + monk_id).submit();
                    }
                    console.log('This was logged in the callback: ' + result + "#activate_" + monk_id);
                }
            });
        }
    </script>
    <jsp:include page="../footer.jsp" />
