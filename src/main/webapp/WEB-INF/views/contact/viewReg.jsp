<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html;charset=UTF-8" %>
<jsp:include page="../header.jsp" />
<c:if test="${roleadmin == 'admin'}">
    <jsp:include page="../navigation.jsp" />
</c:if>
<c:if test="${roleadmin == 'user'}">
    <jsp:include page="../navigation_top.jsp" />
</c:if>
<!-- Page Content -->
<div id="page-wrapper">
    <div class="container-fluid">
        <!-- start Content -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header" id="title_head">ดูรายละเอียดข้อมูลสมาชิก</h1>
            </div>
        </div>
        <form:form action="${action}" method="POST" modelAttribute="Register" >
            <div class="row">
                <div class="col-lg-12">

                    <div class="tab-content">
                        <div class="tab-pane fade active in" id="Profile">

                            <div class="row">
                                <div class="col-lg-1"></div>
                                <div class="col-lg-4">
                                    <dl class="dl-horizontal" items="${temple}" var="${temple}">
                                        <dt><h4><strong>วัด</strong></h4></dt>
                                        <dd>                                                                                  
                                            <c:forEach  var="temple" items="${temple}" varStatus="loop">
                                                <c:if test="${temple_id eq temple.id}"><h4>${temple.name}</h4></c:if>  
                                            </c:forEach>     
                                        </dd>
                                    </dl>
                                </div>
                                <div class ="col-lg-6">
                                </div>
                            </div>
                            <hr>
                            <h4><strong>ข้อมูลส่วนตัว</strong></h4>
                            <p></p>
                            <div class="row" style="font-size: 20px">
                                <div class="col-lg-1"></div>
                                <div class="col-lg-4">
                                    <dl class="dl-horizontal">
                                        <dt>คำนำหน้า:</dt>
                                        <dd>
                                            ${nametitle}
                                        </dd>
                                        <dt>ฉายา:</dt> <dd>${Register.nickname}</dd>
                                        <dt>ชื่อ:</dt> <dd>${firstname}</dd>
                                        <dt>นามสกุล:</dt> <dd>${Register.lastname}</dd>
                                        <dt>หมายเลขบัตรประชาชน:</dt> <dd>${Register.personid}</dd>
                                        <dt>สัญชาติ: </dt> <dd>${Register.royal}</dd>
                                        <dt>เชื้อชาติ:</dt> <dd>${Register.nationality}</dd>
                                    </dl>
                                </div>
                                <div class="col-lg-4">
                                    <dl class="dl-horizontal">
                                        <dt>วันเกิด:</dt> <dd>${Register.bd_birth}</dd>
                                        <dt>เพศ:</dt> <dd>${gender}</dd>
                                        <dt>เบอร์โทรศัพท์:</dt> <dd>${Register.telephone}</dd>
                                        <dt>อีเมลล์:</dt> <dd>${Register.email}</dd><dd><form:errors class="text-danger" path="email"/></dd>
                                    </dl>
                                </div>
                            </div>
                            <hr>
                            <h4><strong>ที่อยู่</strong></h4>
                            <p></p>
                            <div class="row" style="font-size: 20px">
                                <div class="col-lg-1"></div>
                                <div class="col-lg-4">
                                    <dl class="dl-horizontal">

                                        <dt>ที่อยู่:</dt> <dd>${Register.address}</dd>
                                        <dt>หมู่บ้าน:</dt> <dd>${Register.village}</dd>
                                        <dt>ตำบล:</dt> <dd>${Register.district}</dd>

                                    </dl>
                                </div>
                                <div class="col-lg-4">
                                    <dl class="dl-horizontal">
                                        <dt>อำเภอ: </dt> <dd>${Register.city}</dd>
                                        <dt>จังหวัด:</dt> <dd>${Register.province}</dd>
                                        <dt>รหัสไปรษณีย์:</dt> <dd>${Register.postalcode}</dd>
                                    </dl>
                                </div>
                            </div>
                            <form:hidden path="reg_id" value ="${reg_id}"/>
                        </div>
                    </div>
                </div>
            </div>
        </form:form><br><br><br>
        <!-- End Content -->
    </div>
</div>
<!-- End Page Content -->
<script>

</script>


<jsp:include page="../footer.jsp" /> 


