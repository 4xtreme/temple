<%-- 
    Document   : confirmbooking
    Created on : Apr 24, 2018, 8:35:34 AM
    Author     : pop
--%>

<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:include page="../header.jsp" />
<c:if test="${roleadmin == 'admin'}">
    <jsp:include page="../navigation.jsp" />
</c:if>
<c:if test="${roleadmin == 'user'}">
    <jsp:include page="../navigation_top.jsp" />
</c:if>

<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header" id="title_head">ยืนยันการจองห้อง</h1>
            </div>
        </div>
        <div class="row">
            <div class="tab-content">
                <div class="tab-pane fade active in"> 
                    <div class="dl-horizontal">
                        <div class="col-lg-4"></div>
                        <div class="col-lg-4">

                            <form id='checkin' action='checkin' method="post" >
                                <div class="form-group">
                                    <span class="label_title">เลขบัตรประจำตัวประชาชน</span>
                                    <input type="text" readonly="" value="${personal_id}" class="form-control" name="personal_id" />
                                </div>
                                <div class="form-group">
                                    <span class="label_title">พระ</span>
                                    <input type="text" readonly="" value="${vipassana_name}" class="form-control"  />
                                    <input type="hidden" readonly="" name="vipassana_id" value="${vipassana_id}"   />
                                </div>
                                <div class="form-group">
                                    <span class="label_title">อาคาร/กุฏิ</span>
                                    <input type="text" readonly="" value="${builder_name}" class="form-control"  />
                                </div>
                                <div class="form-group">
                                    <span class="label_title">ห้อง</span>
                                    <input type="text" readonly="" value="${room_name}" class="form-control"  />
                                </div>

                                <div class="form-group">
                                    <span class="label_title">เช็คอิน</span> 
                                    <input type="text" readonly="" value="${check_in}" class="form-control checkin" name="check_in"  />
                                </div>
                                <div class="form-group">
                                    <span class="label_title">เช็คเอาท์</span> 
                                    <input type="text" readonly="" value="${check_out}" class="form-control checkout" name="check_out"  />
                                </div>
                                <div class="form-group" style="text-align: center">
                                    <input type="text" readonly="" value="${temple_id}" hidden="" name="temple_id"/>
                                    <input type="text" readonly="" value="${room_name_id}" hidden="" name="room_name_id"/>
                                    <input type="text" readonly="" value="${room_id}" hidden="" name="room_id"/>
                                    <input type="text" readonly="" value="${contact_id}" hidden="" name="contact_id"/>
                                    <input type="text" hidden="" value="${raw_indate}" name="raw_indate"/>
                                    <input type="text" hidden="" value="${raw_outdate}" name="raw_outdate"/>
                                    <button  type="submit" class="btn  button-size-sucess"style="margin-left: 20px;margin-right: 20px">เช็คอิน</button>
                                </div>
                            </form>

                        </div>
                        <div class="col-lg-4"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<jsp:include page="../footer.jsp" />
