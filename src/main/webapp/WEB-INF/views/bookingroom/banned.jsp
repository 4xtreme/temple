<%-- 
    Document   : alreadybooking
    Created on : Apr 25, 2018, 10:33:09 AM
    Author     : pop
--%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:include page="../header.jsp" />
<c:if test="${roleadmin == 'admin'}">
    <jsp:include page="../navigation.jsp" />
</c:if>
<c:if test="${roleadmin == 'user'}">
    <jsp:include page="../navigation_top.jsp" />
</c:if>

<div id="page-wrapper">
    <div class="container-fluid">
        <!-- start Content -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header text-center" style="color: red" >ไม่สามารถจองห้องได้เนื่องจากคุณมีประวัติ  ${contact.reason}</h1>
            </div>
            <div class="col text-center">
                <a class="btn btn-primary" href="bookingRoom">ย้อนกลับ</a>
            </div>
        </div>
    </div><!-- End Content -->    
</div><!-- End Page Content -->

    <jsp:include page="../footer.jsp" />
