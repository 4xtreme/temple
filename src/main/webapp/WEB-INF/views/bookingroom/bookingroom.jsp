<%-- 
    Document   : withdrawlist
    Created on : Dec 1, 2017, 8:20:20 AM
    Author     : pop
--%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:include page="../header.jsp" />
<c:if test="${roleadmin == 'admin'}">
    <jsp:include page="../navigation.jsp" />
</c:if>
<c:if test="${roleadmin == 'user'}">
    <jsp:include page="../navigation_top.jsp" />
</c:if>

<div id="page-wrapper">
    <div class="container-fluid">
        <!-- start Content -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header" id="title_head">เช็คอินภายในวัด</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="tab-content">
                    <div class="tab-pane fade active in"> 
                        <div class="dl-horizontal">
                            <form id="addroom"  action="bookingRoom" method="POST"  enctype="multipart/form-data" >
                                <c:if test="${status != null}">
                                    <p style="color: red;">ไม่มีเลขบัตรประจำตัวประชาชนนี้อยู่ในระบบ !!</p>
                                </c:if>
                                <div class="form-group">
                                    <span class="label_title">เลขบัตรประจำตัวประชาชน</span>
                                    <input type="text" id="personal_cardid" autocomplete="off" name="personal_cardid" maxlength="13" required=""  class="form-control width_15"  value="${personal_cardid}" />
                                    <span class="label_title">เช็คอิน</span>
                                    <input type="text" name="" required="true"  disabled=""  autocomplete="off"  class="form-control width_15  datetime"   />
                                    <input type="text" name="start" required="true" hidden="" value="${dateval}"  autocomplete="off" class="form-control width_15 "   />
                                    &nbsp;&nbsp;&nbsp;&nbsp;
                                    
                                    <select id="monk" name="vipassana_id"  class="form-control width_15"  onchange="showTable()">
                                        <option  value="" disabled=" " selected="">เลือกพระวิปัสสนาจารย์</option>
                                        <c:forEach var="vipassana" items="${vipassanas}" varStatus="loop">
                                            <option <c:if test="${vipassana_id eq vipassana.id}"> selected="selected" </c:if> value="${vipassana.id}">${vipassana.firstname}&nbsp;&nbsp;${vipassana.lastname}(${vipassana.nickname})</option>
                                        </c:forEach>
                                    </select>
                                    &nbsp;&nbsp;&nbsp;&nbsp;
                                    <span class="label_title">วันที่กลับ</span>
<!--                                    <select name="count"  class="form-control width_15" onchange="showTable()">
                                        <c:forEach var="count_array" items="${count_array}" varStatus="loop">
                                            <option <c:if test="${count == count_array}"> selected="selected" </c:if> value="${count_array}">${count_array}</option>
                                        </c:forEach>
                                    </select>-->
                                    <input type="text" name="end" required="true" id="end" required="" value="${datetmr}"  class="form-control width_15  checkout"  autocomplete="off" />
                                    <button onclick="showTable()" type="button" class="btn  button-size-sucess"style="margin-left: 20px;margin-right: 20px">ค้นหา</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <br>
        <br>
        <div class="row">
            <div class="col-lg-12">
                <div class="table-responsive" >
                    <table class="table table-striped table-bordered table-hover">
                        <thead>
                            <tr>
                                <th class="text-center">พระวิปัสสนาจารย์</th>
                                <c:forEach var="date_table" items="${date_table}" varStatus="loop">
                                <th class="text-center">${date_table}</th>
                                 </c:forEach>
                                
                         
                            </tr>
                        </thead>
                        <tbody>
                            <c:forEach var="monk_date" items="${monk_date}" varStatus="loop">
                                <tr>
                                    <td class="text-center">${monk_date.monk_name}</td>
                                    <td class="text-center">${monk_date.day1} คน</td>
                                    <td class="text-center">${monk_date.day2} คน</td>
                                    <td class="text-center">${monk_date.day3} คน</td>
                                    <td class="text-center">${monk_date.day4} คน</td>
                                    <td class="text-center">${monk_date.day5} คน</td>
                                    <td class="text-center">${monk_date.day6} คน</td>
                                    <td class="text-center">${monk_date.day7} คน</td>
                                    <td class="text-center">${monk_date.day8} คน</td>
                                    <td class="text-center">${monk_date.day9} คน</td>
                                    <td class="text-center">${monk_date.day10} คน</td>
                                </tr>
                                           
                            </c:forEach>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-lg-2"></div>
        <div class="row">
            <div class="col-lg-8" >
                <div class="table-responsive" >
                    <input type="text"  id="check_data" value="${page_count}" hidden=""/>



                    <table class="table table-striped table-bordered table-hover" id="myTable" >
                        <thead>
                            <tr>
                                <th class="text-center">ชื่อห้อง</th>
                                <th class="text-center">สถานะ</th>
                                <th class="text-center">จำนวนคนอยู่</th>
                            </tr>
                        </thead>
                        <tbody id="tableBody">
                            <c:if test="${builder[0].name != null}">  
                                <c:forEach var="builder" items="${builder}" varStatus="loop">
                                    <tr>
                                        <td class=""  colspan="4">
                                            ${builder.type} ${builder.name}

                                        </td>

                                    </tr>
                                    <c:forEach var="rooms" items="${rooms}" varStatus="loop">
                                        <c:if test="${rooms.room_id == builder.id}">
                                            <tr>
                                                <td class="text-center" >
                                                    ${rooms.name}
                                                    <input type="text" hidden="" id="room${loop.index}" value="${rooms.id}"/>
                                                    <input type="text" hidden="" id="roomname${loop.index}" value="${rooms.name}"/>
                                                    <input type="text" hidden="" id="builder${loop.index}" value="${rooms.room_id}"/>
                                                </td>
                                                <c:if test="${rooms.status == 0}">
                                                    <td class="text-center">ว่าง</td>
                                                </c:if>
                                                <c:if test="${rooms.status == 1}">
                                                    <td class="text-center">ไม่ว่าง</td>
                                                </c:if>
                                                <td class="text-center">${rooms.live}/${rooms.amount}</td>
                                                <td class="text-center">
                                                    <div  onclick="getRowData(${loop.index})"><a href="#">จอง</a></div>
                                                </td>
                                            </tr>
                                        </c:if>
                                    </c:forEach>
                                </c:forEach>
                            </c:if>
                            
                        </tbody>
                    </table>
                </div>


                <div class="row" hidden="true">
                    <center>
                        <div class="pagination_center">
                            <nav aria-label="Page navigation col-centered">
                                <ul class="pagination">
                                    <c:if test="${page_priv == true}">  
                                        <li class="page-item"><a class="page-link"  href="${pageContext.request.contextPath}/bookingRoom?page=${page-1}"><i class="fa fa-angle-left  "></i></a></li>
                                            </c:if>
                                            <c:if test="${page_count != 0}">
                                                <c:forEach begin="0" end="${page_count-1}" varStatus="loop">
                                                    <c:if test="${page == loop.index}"> 
                                                <li class="page-item" ><a class="page-link active"  href="${pageContext.request.contextPath}/bookingRoom?page=${loop.index}">${loop.index+1}</a></li>
                                                </c:if> 
                                                <c:if test="${page != loop.index}"> 
                                                <li class="page-item"><a class="page-link"  href="${pageContext.request.contextPath}/bookingRoom?page=${loop.index}">${loop.index+1}</a></li>
                                                </c:if> 
                                            </c:forEach>
                                        </c:if>
                                        <c:if test="${page_next == true}"> 
                                        <li class="page-item"><a class="page-link" href="${pageContext.request.contextPath}/bookingRoom?page=${page+1}"><i class="fa fa-angle-right "></i></a></li>
                                            </c:if>
                                </ul>
                            </nav>
                        </div>
                    </center>
                </div><!--end row-->
            </div>
            <form id="confirm_book" action="confirmbook" method="POST" >
                <input type="text" name="check_in" value="${startdate}" id="check_in" hidden=""  autocomplete="off"/>
                <input type="text" name="check_out" value="${enddate}" id="check_out" hidden="" autocomplete="off"/>
                <input type="text" name="room_id" id="room_id" hidden="" value=""/>
                <input type="text" name="room_name_id" id="room_name_id" hidden="" value=""/>
                <input type="text" name="personal_id" id="personal_id" hidden="" value=""/>
                <input type="text" name="room_name" id="room_name" hidden="" value=""/>
                <input type="text" name="contact_id"  id="contact_id" hidden="" value="${contact_id}"/>
                <input type="text" name="raw_indate" id="raw_indate" hidden="" value="${raw_indate}"/>
                <input type="text" name="raw_outdate" id="raw_outdate" hidden="" value="${raw_outdate}"/>
                <input type="text" name="vipassana_id"  hidden="" value="${vipassana_id}"/>

            </form>
        </div>
    </div><!-- End Content -->    
</div><!-- End Page Content -->
<script language="JavaScript">
    $(document).ready(function () {

        // check table show/hide
        var builder = document.getElementById("check_data");
        if (builder.value == 0) {
            $("#myTable").hide();
        } else {
            $("#myTable").show();
        }

    });

    // Search room 
    function showTable() {
        $("#check_data").val(1);
        var personal_cardid = $("#personal_cardid").val();
        var monk = $("#monk").val();
        var enddate = $("#end").val();
        if (personal_cardid == "") {
            alert("โปรดกรอกเลขบัตรประจำตัวประชาชน");
        } else if (monk == null) {
            alert("โปรดเลือกพระ");
        } else if (enddate == ""){
            alert("โปรดเลือกวันกลับ")
        } else {
            $("#myTable").show();
            document.getElementById("addroom").submit();
        }
    }


    function getRowData(index) {


        var builder_id = document.getElementById("builder" + index).value;

        var room_id = document.getElementById("room" + index).value;
        var room_name = document.getElementById("roomname" + index).value;
        var personal_id = document.getElementById("personal_cardid").value;


        $("#room_id").attr("value", builder_id);

        $("#room_name_id").attr("value", room_id);
        $("#room_name").attr("value", room_name);
        $("#personal_id").attr("value", personal_id);

        $("#confirm_book").submit();
    }
</script>
<jsp:include page="../footer.jsp" />
