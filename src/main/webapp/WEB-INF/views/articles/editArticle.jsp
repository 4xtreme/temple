<%-- 
    Document   : withdrawlist
    Created on : Dec 1, 2017, 8:20:20 AM
    Author     : pop
--%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:include page="../header.jsp" />
<c:if test="${roleadmin == 'admin'}">
    <jsp:include page="../navigation.jsp" />
</c:if>
<c:if test="${roleadmin == 'user'}">
    <jsp:include page="../navigation_top.jsp" />
</c:if>
<div id="page-wrapper">
    <div class="container-fluid">
        <!-- start Content -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">บทความ</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="tab-content">
                    <div class="tab-pane fade active in"> 
                        <div class="dl-horizontal">

                            <form:form id="articles" action="updateArticle"  method="POST" enctype="multipart/form-data" modelAttribute="articles" >   
                                <form:hidden path="article_id" />
                                <form:hidden  path="staff_id" />
                                <div class="form-group">
                                    <span  class="label_title">หัวข้อ:</span>&nbsp;
                                    <form:input id="title" path="title" name = "title" rows="5"  class="form-control width_15"   />
                                    <form:errors path="title" class="text-danger"/> 

                                    <br><br>
                                    <div class="form-group">
                                        <label>เนื้อหา:</label>
                                        <form:errors path="contents" class="text-danger"/>

                                        <form:textarea  class="form-control" path="contents" name="contents" rows="5" id="comment"/>
                                    </div> 

                                    <br>
                                    <input name="article_img" type="file"   />
                                    <br>

                                    <div class="form-group text-left"><button type="button" class="btn btn-primary" onclick="succesRegis()"> <span class="fa fa-save "></span> บันทึก</button></div>
                                </div>
                            </form:form> 
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <br>
        <br>
        <div class="row">
            <div class="col-lg-8" >



                <div class="row">
                    <center>
                        <div class="pagination_center">
                            <nav aria-label="Page navigation col-centered">

                            </nav>
                        </div>
                    </center>
                </div><!--end row-->
            </div>

        </div>
    </div><!-- End Content -->    
</div><!-- End Page Content -->
<script>
    function succesRegis() {
        $("#articles").submit();

    }

</script>
<jsp:include page="../footer.jsp" />    
