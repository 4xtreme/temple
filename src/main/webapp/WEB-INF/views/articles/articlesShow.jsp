<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html;charset=UTF-8" %>
<jsp:include page="../header.jsp" />
<c:if test="${roleadmin == 'admin'}">
    <jsp:include page="../navigation.jsp" />
</c:if>
<c:if test="${roleadmin == 'user'}">
    <jsp:include page="../navigation_top.jsp" />
</c:if>
<!-- Page Content -->
<div id="page-wrapper">
    <div class="container-fluid">
        <!-- start Content -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">บทความ</h1>
            </div>
        </div>




        <div class="row">
            <div class="col-lg-12">
                <div>
                    <form method="POST" action="searcharticle"> 
                        <input class="form-control width_20" type="text" name="search">
                        <button type="submit" class="btn btn-primary btn-signup width_10 align-center">
                            <i class="glyphicon glyphicon-search"></i> ค้นหา
                        </button>
                    </form>
                </div>
                <br>
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover">
                        <thead>
                            <tr>
                                <th class="text-center">บทความ</th>
                                <th class="text-center">คนเขียน</th>
                                <th class="text-center">รูป</th>
                                <th class="text-center">หัวข้อ</th>
                                <th class="text-center">เนื้อหา</th>
                                <th class="text-center">แก้ไข</th>
                                <th class="text-center">ลบ</th>						
                            </tr>
                        </thead>
                        <tbody>

                            <c:forEach var="article" items="${articlelist}" varStatus="loop">
                                <tr>
                            <input type="hidden" values="${article.staff_id}"/>
                            <td class="text-center">${article.article_id}</td>
                            <td class="text-center">${article.firstname}&nbsp;${article.lastname}</td>
                            <td class="text-center"><img src="data:image/jpg;base64, <c:out value='${article.article_string}'/>"width="104" height="142" /></td>
                            <td class="text-center">${article.title}</td>
                            <td class="text-center">
                                <!--                                <form  action="selectarticleView" method="post" >    
                                                                    <input type="hidden" value="${article.article_id}" name="article_id" />
                                                                    <button type="submit"  class="no-border tran-bg"><i  style="color: green;" class="fa fa-search-plus" style="font-size: 30px"></i></button>
                                                                </form>  -->
                                <button type="button" class="no-border tran-bg" data-toggle="modal" data-target="#flipFlop${article.article_id}">
                                    <i  style="color: green;" class="fa fa-search-plus" style="font-size: 30px"></i>
                                </button>
                            </td>
                            <td class="text-center">
                                <form  action="selectarticleEdit" method="post" >
                                    <input type="hidden" value="${article.article_id}" name="article_id"/>
                                    <button type="submit" class="no-border tran-bg"><i class="fa fa-edit edit-btn" style="font-size: 30px"></i></button>
                                </form>
                            </td>
                            <td class="text-center">
                                <form  id="delete_${article.article_id}" action="deleteArticle" method="post" >                                                                  
                                    <input type="hidden" value="${article.article_id}" name="article_id"/>
                                    <div  onclick="chkConfirm(${article.article_id});" ><i style="color: red;" class="fa fa-trash fa-2x" aria-hidden="true"></i></div>
                                </form>
                            </td>
                            </tr>

                            <!-- The modal -->
                            <div class="modal fade" id="flipFlop${article.article_id}" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                            <h3 class="modal-title" id="modalLabel"  >${article.title}</h3>
                                        </div>
                                        <div class="modal-body">


                                            <div class="row" style="font-size: 20px">

                                                <div class="col-lg-12 ">
                                                    <dl class="dl-horizontal">

                                                        <div class="row">
                                                            <div class="col-sm-3" >
                                                                <img src="data:image/jpg;base64, <c:out value='${article.article_string}'/>"width="104" height="142" />
                                                            </div>
                                                            <div class="col-sm-9" >
                                                                ${article.contents}
                                                            </div>

                                                        </div>

                                                    </dl>
                                                </div>

                                            </div>



                                        </div>
                                        <div class="modal-footer">

                                            <form  action="selectarticleEdit" method="post" > 
                                                <input type="hidden" value="${article.article_id}" name="article_id"/>
                                                <button type="submit" class="btn btn-primary">แก้ไขข้อมูล</button>
                                            </form> 


                                            <button type="button" class="btn btn-danger" data-dismiss="modal">ปิด</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!------------------------------->
                        </c:forEach>


                        </tbody>
                    </table>
                </div>

                <div class="row">
                    <center>
                        <div class="pagination_center">
                            <nav aria-label="Page navigation col-centered">
                                <ul class="pagination">
                                    <c:if test="${page_priv == true}">  
                                        <li class="page-item"><a class="page-link"  href="${pageContext.request.contextPath}/articlesall?page=${page-1}"><i class="fa fa-angle-left  "></i></a></li>
                                            </c:if>
                                            <c:if test="${page_count != 0}">
                                                <c:forEach begin="0" end="${page_count-1}" varStatus="loop">
                                                    <c:if test="${page == loop.index}"> 
                                                <li class="page-item"><a class="page-link active"  href="${pageContext.request.contextPath}/articlesall?page=${loop.index}">${loop.index+1}</a></li>
                                                </c:if> 
                                                <c:if test="${page != loop.index}"> 
                                                <li class="page-item"><a class="page-link"  href="${pageContext.request.contextPath}/articlesall?page=${loop.index}">${loop.index+1}</a></li>
                                                </c:if> 
                                            </c:forEach>
                                        </c:if>
                                        <c:if test="${page_next == true}"> 
                                        <li class="page-item"><a class="page-link" href="${pageContext.request.contextPath}/articlesall?page=${page+1}"><i class="fa fa-angle-right "></i></a></li>
                                            </c:if>
                                </ul>
                            </nav>
                        </div>                    <br><br><br><br>

                        </div><!--end row-->
                        </div>
                        </div>

                        </div>


                        </div>

                        <!-- End Content -->
                        <script language="JavaScript">
                            function chkConfirm(article_id) {
                                bootbox.confirm({
                                    message: "Are you sure you want to delete?" + article_id,
                                    buttons: {
                                        confirm: {
                                            label: 'Yes',
                                            className: 'btn-success'
                                        },
                                        cancel: {
                                            label: 'No',
                                            className: 'btn-danger'
                                        }
                                    },
                                    callback: function (result) {
                                        if (result) {
                                            $("#delete_" + article_id).submit();
                                        }
                                        console.log('This was logged in the callback: ' + result + "#delete_" + article_id);
                                    }
                                });
                            }
                        </script>
                        <jsp:include page="../footer.jsp" />
