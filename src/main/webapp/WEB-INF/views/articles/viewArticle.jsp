<%-- 
    Document   : withdrawlist
    Created on : Dec 1, 2017, 8:20:20 AM
    Author     : pop
--%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:include page="../header.jsp" />
<c:if test="${roleadmin == 'admin'}">
    <jsp:include page="../navigation.jsp" />
</c:if>
<c:if test="${roleadmin == 'user'}">
    <jsp:include page="../navigation_top.jsp" />
</c:if>
<div id="page-wrapper">
    <div class="container-fluid">
        <!-- start Content -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">บทความ</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="tab-content">
                    <div class="tab-pane fade active in"> 
                        <div class="dl-horizontal">

                            <form:form action="updateArticle"  method="POST" enctype="multipart/form-data" modelAttribute="articles" >   
                                <form:input type="hidden" path="article_id" />
                                <form:input type="hidden" path="staff_id" />
                                <div class="form-group">
                                    <span class="label_title">หัวข้อ:</span>&nbsp;
                                    ${articles.title}<br><br>
                                    <div class="form-group">
                                        <label>เนื้อหา:</label>

                                        <article>
                                            <p>
                                                ${articles.contents} 
                                            </p>
                                        </article>
                                    </div> 

                                </div>
                            </form:form> 
                            <button type="submit" onclick="history.back()" class="btn  button-size-sucess"style="margin-left: 20px;margin-right: 20px">ย้อนกลับ</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <br>
        <br>
        <div class="row">
            <div class="col-lg-8" >



                <div class="row">
                    <center>
                        <div class="pagination_center">
                            <nav aria-label="Page navigation col-centered">

                            </nav>
                        </div>
                    </center>
                </div><!--end row-->
            </div>

        </div>
    </div><!-- End Content -->    
</div><!-- End Page Content -->

<jsp:include page="../footer.jsp" />    
