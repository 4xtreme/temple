<%-- 
    Document   : withdrawlist
    Created on : Dec 1, 2017, 8:20:20 AM
    Author     : pop
--%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:include page="../header.jsp" />
<jsp:include page="../navigation_contact.jsp" />

<div id="page-wrapper">
    <div class="container-fluid">
        <!-- start Content -->
        <div class="row">
            <div class="col-lg-12">
                <h3 class="page-header">จองห้อง  ${temple.name} (${temple.tel})</h3>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="tab-content">
                    <div class="tab-pane fade active in"> 
                        <div class="dl-horizontal">
                            <form id="addroom"  action="bookingOnline" method="POST"  enctype="multipart/form-data" >
                                <div class="form-group">
                                    <span class="label_title">เช็คอิน</span>
                                    <input type="text" id="start" name="start" required="true" autocomplete="off" value="${dateval}" class="form-control width_15 form_start"   />
                                    &nbsp;&nbsp;&nbsp;&nbsp;
                                       <span class="label_title">จำนวนคืน</span>
                                    <select name="count"  class="form-control width_5">
                                        <option  value="1" selected="">1</option>
                                        <option  value="2">2</option>
                                        <option  value="3" >3</option>
                                        <option  value="4">4</option>
                                        <option  value="5" >5</option>
                                        <option  value="6">6</option>
                                        <option  value="7" >7</option>
                                        <option  value="8">8</option>
                                        <option  value="9" >9</option>
                                        <option  value="10">10</option>
                                        <option  value="11" >11</option>
                                        <option  value="12">12</option>
                                        <option  value="13" >13</option>
                                        <option  value="14">14</option>
                                        <option  value="15" >15</option>
                                        <option  value="16">16</option>
                                        <option  value="17" >17</option>
                                        <option  value="18">18</option>
                                        <option  value="19" >19</option>
                                        <option  value="20">20</option>
                                    </select>
                                    <button onclick="showTable()" type="button" class="btn  button-size-sucess"style="margin-left: 20px;margin-right: 20px">ค้นหา</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <br>
        <br>
        <div class="col-lg-2"></div>
        <div class="row">
            <div class="col-lg-8" >
                <div class="table-responsive" >
                    <input type="text"  id="check_data" value="${page_count}" hidden=""/>



                    <table class="table table-striped table-bordered table-hover" id="myTable" >
                        <thead>
                            <tr>
                                <th class="text-center">ชื่อห้อง</th>
                                <th class="text-center">สถานะ</th>
                                <th class="text-center">จำนวนคนอยู่</th>
                            </tr>
                        </thead>
                        <tbody id="tableBody">
                            <c:if test="${builder[0].name != null}">  
                                <c:forEach var="builder" items="${builder}" varStatus="loop">
                                    <tr>
                                        <td class=""  colspan="4">
                                            ${builder.type} ${builder.name}

                                        </td>

                                    </tr>
                                    <c:forEach var="rooms" items="${rooms}" varStatus="loop">
                                        <c:if test="${rooms.room_id == builder.id}">
                                            <tr>
                                                <td class="text-center" >
                                                    ${rooms.name}
                                                    <input type="text" hidden="" id="room${loop.index}" value="${rooms.id}"/>
                                                    <input type="text" hidden="" id="roomname${loop.index}" value="${rooms.name}"/>
                                                    <input type="text" hidden="" id="builder${loop.index}" value="${rooms.room_id}"/>
                                                </td>
                                                <c:if test="${rooms.status == 0}">
                                                    <td class="text-center">ว่าง</td>
                                                </c:if>
                                                <c:if test="${rooms.status == 1}">
                                                    <td class="text-center">ไม่ว่าง</td>
                                                </c:if>
                                                <td class="text-center">${rooms.live}/${rooms.amount}</td>
                                                <td class="text-center">
                                                    <div  onclick="getRowData(${loop.index})"><a href="#">จอง</a></div>
                                                </td>
                                            </tr>
                                        </c:if>
                                    </c:forEach>
                                </c:forEach>
                            </c:if>
                        </tbody>
                    </table>
                </div>


                    <div class="row" hidden="true">
                    <center>
                        <div class="pagination_center">
                            <nav aria-label="Page navigation col-centered">
                                <ul class="pagination">
                                    <c:if test="${page_priv == true}">  
                                        <li class="page-item"><a class="page-link"  href="${pageContext.request.contextPath}/bookingRoom?page=${page-1}"><i class="fa fa-angle-left  "></i></a></li>
                                            </c:if>
                                            <c:if test="${page_count != 0}">
                                                <c:forEach begin="0" end="${page_count-1}" varStatus="loop">
                                                    <c:if test="${page == loop.index}"> 
                                                <li class="page-item" ><a class="page-link active"  href="${pageContext.request.contextPath}/bookingRoom?page=${loop.index}">${loop.index+1}</a></li>
                                                </c:if> 
                                                <c:if test="${page != loop.index}"> 
                                                <li class="page-item"><a class="page-link"  href="${pageContext.request.contextPath}/bookingRoom?page=${loop.index}">${loop.index+1}</a></li>
                                                </c:if> 
                                            </c:forEach>
                                        </c:if>
                                        <c:if test="${page_next == true}"> 
                                        <li class="page-item"><a class="page-link" href="${pageContext.request.contextPath}/bookingRoom?page=${page+1}"><i class="fa fa-angle-right "></i></a></li>
                                            </c:if>
                                </ul>
                            </nav>
                        </div>
                    </center>
                </div><!--end row-->
            </div>
            <form id="confirm_book" action="confirmbookOnline" method="POST" >
                <input type="text" name="check_in" value="${startdate}" id="check_in" hidden=""/>
                <input type="text" name="check_out" value="${enddate}" id="check_out" hidden=""/>
                <input type="text" name="room_id" id="room_id" hidden="" value=""/>
                <input type="text" name="room_name_id" id="room_name_id" hidden="" value=""/>
                <input type="text" name="room_name" id="room_name" hidden="" value=""/>
                <input type="text" name="contact_id"  id="contact_id" hidden="" value="${contact_id}"/>
                <input type="text" name="raw_indate" id="raw_indate" hidden="" value="${raw_indate}"/>
                <input type="text" name="raw_outdate" id="raw_outdate" hidden="" value="${raw_outdate}"/>

            </form>
        </div>
    </div><!-- End Content -->    
</div><!-- End Page Content -->
<script <script language="JavaScript">
    $(document).ready(function () {

        // check table show/hide
        var builder = document.getElementById("check_data");
        if (builder.value == 0) {
            $("#myTable").hide();
        } else {
            $("#myTable").show();
        }

    });
    
    // Search room 
    function showTable() {
            $("#check_data").val(1);          
            var date = $("#start").val();
            if(date == ""){
                alert("โปรดใส่วันที่");
            }else{
                 $("#myTable").show();
                 document.getElementById("addroom").submit();
            }
           
    }
    
    
    function getRowData(index) {

        var builder_id = document.getElementById("builder" + index).value;

        var room_id = document.getElementById("room" + index).value;
        var room_name = document.getElementById("roomname" + index).value;


        $("#room_id").attr("value", builder_id);

        $("#room_name_id").attr("value", room_id);
        $("#room_name").attr("value", room_name);

        $("#confirm_book").submit();
    }
    </script>
    <jsp:include page="../footer.jsp" />
