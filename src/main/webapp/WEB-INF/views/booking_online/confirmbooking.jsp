<%-- 
    Document   : confirmbooking
    Created on : Apr 24, 2018, 8:35:34 AM
    Author     : pop
--%>

<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:include page="../header.jsp" />
<jsp:include page="../navigation_contact.jsp" />

<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">ยืนยันการจองห้อง</h1>
            </div>
        </div>
        <div class="row">
            <div class="tab-content">
                <div class="tab-pane fade active in"> 
                    <div class="dl-horizontal">
                        <div class="col-lg-4"></div>
                        <div class="col-lg-4">

                            <form id='checkin' action='checkinOnline' method="post" >
                                <div class="form-group">
                                    <span class="label_title">อาคาร/กุฏิ</span>
                                    <input type="text" readonly="" value="${builder_name}" class="form-control"  />
                                </div>
                                <div class="form-group">
                                    <span class="label_title">ห้อง</span>
                                    <input type="text" readonly="" value="${room_name}" class="form-control"  />
                                </div>

                                <div class="form-group">
                                    <span class="label_title">เช็คอิน</span> 
                                    <input type="text" readonly="" value="${check_in}" class="form-control" name="check_in"  />
                                </div>
                                <div class="form-group">
                                    <span class="label_title">เช็คเอาท์</span> 
                                    <input type="text" readonly="" value="${check_out}" class="form-control" name="check_out"  />
                                </div>
                                <div class="form-group" style="text-align: center">
                                    <input type="text" readonly="" value="${room_name_id}" hidden="" name="room_name_id"/>
                                    <input type="text" readonly="" value="${room_id}" hidden="" name="room_id"/>
                                    <input type="text" readonly="" value="${contact_id}" hidden="" name="contact_id"/>
                                    <input type="text" hidden="" value="${raw_indate}" name="raw_indate"/>
                                    <input type="text" hidden="" value="${raw_outdate}" name="raw_outdate"/>
                                    <button  type="submit"  class="btn  button-size-sucess" onclick="succesRegis()" style="margin-left: 20px;margin-right: 20px">ยืนยันการจอง</button>
                                </div>
                            </form>

                        </div>
                        <div class="col-lg-4"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    function succesRegis() {
        alert("จองสำเร็จ");
    }
</script>
<jsp:include page="../footer.jsp" />
