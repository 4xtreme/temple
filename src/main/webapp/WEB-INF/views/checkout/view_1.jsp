<%-- 
    Document   : withdrawlist
    Created on : Dec 1, 2017, 8:20:20 AM
    Author     : pop
--%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:include page="../header.jsp" />
<jsp:include page="../navigation.jsp" />

<div id="page-wrapper">
    <div class="container-fluid">
        <!-- start Content -->
        <div class="row">
            <div class="col-lg-6">   
                <center>
                <h1 class="page-header">เช็คอิน</h1>            
            </div>
            </center>
                <center>
            <div class="col-lg-6">    
                <h1 class="page-header">เช็คเอาท์</h1>          
            </div></center>
        </div>
        <div class="row">

            <div class="col-lg-6">
                <div class="tab-content">
                    <div class="tab-pane fade active in"> 
                        <div class="dl-horizontal">
                            <form   action="checkOnline" method="POST"   >     
                                <center>
                                <div class="form-group">
                                    <span class="label_title">รหัสบัตรประชาชน</span>
                                    <input type="text" name="personal_cardid" maxlength="13" required="true"  class="form-control width_30"   />
                                    <br>
                                    <br>  
                                    <span class="label_title">พระ</span>
                                    <select name="monk_id"  class="form-control width_30">
                                        <option  value="" abled=" " selected="">เลือกพระ</option>
                                        <c:forEach var="monklist" items="${monklist}" varStatus="loop">
                                            <option value="${monklist.monk_id}">${monklist.firstname}&nbsp;&nbsp;${monklist.lastname}(${monklist.nickname})</option>
                                        </c:forEach>
                                    </select>
                                    <button type="submit" class="btn btn-primary btn-sm"><span class="glyphicon glyphicon-search"></span> ค้นหา</button>
                                </div>
                                    </center>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6" >
                <div class="tab-content">
                    <div class="tab-pane fade active in"> 
                        <div class="dl-horizontal">
                            <form   action="checkOutOnline" method="POST"   >    
                                <center>
                                <div class="form-group">
                                    <span class="label_title">รหัสบัตรประชาชน</span>
                                    <input type="text" name="personal_cardid" maxlength="13" required=""  class="form-control width_30"   />
                                    <button type="submit" class="btn btn-primary btn-sm"><span class="glyphicon glyphicon-search"></span> ค้นหา</button>
                                </div>
                                    </center>
                            </form>
                        </div>
                    </div>
                </div>    
            </div>
        </div>
        <br>
        <br>
        <div class="row">

            <div class="col-lg-6">
                <center>
                <h3 class="">รายละเอียดการจอง</h3>
                <c:if test="${statusCheck != null}">
                    <p style="color: red;">ไม่มีข้อมูล !!!</p>
                </c:if>
                <div class="tab-content">
                    <div class="tab-pane fade active in"> 
                        <div class="dl-horizontal">
                            <div class="form-group">
                                <span class="label_title">อาคาร / กุฏิ</span>
                                ${room}
                            </div>
                            <div class="form-group">
                                <span class="label_title">ห้อง</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                                ${roomname}
                            </div>
                            <div class="form-group">
                                <span class="label_title">วันที่จอง</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                ${createdate}
                            </div>
                            <div class="form-group">
                                <span class="label_title">เช็คอิน</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                ${startDate}
                            </div>
                            <div class="form-group">
                                <span class="label_title">เช็คเอาท์</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                ${endDate}
                            </div>
                            <div class="form-group">
                                <span class="label_title">พระ</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                ${monk_name}
                            </div>

                        </div>
                    </div>
                </div>
                           
                <form action="updatecheckin" method="POST">
                    <input type="hidden" name="id" value="${id}">
                    <input type="hidden" name="monk_id" value="${monk_id}">
                    <c:if test="${btCheckin != null}">
                        <button type="submit" onclick="succesCheckin()" class="btn btn-primary btn-sm"> <span class="glyphicon glyphicon-plus-sign"></span> เช็คอิน</button>   
                    </c:if>                    
                </form> 
                    </center> 
            </div>
                    
            <div class="col-lg-6">
                <center>
                <h3 class="">รายละเอียดการจอง</h3>
                <c:if test="${statusOut != null}">
                    <p style="color: red;">ไม่มีข้อมูล !!!</p>
                </c:if>
                <div class="tab-content">
                    <div class="tab-pane fade active in"> 
                        <div class="dl-horizontal">
                            <div class="form-group">
                                <span class="label_title">อาคาร / กุฏิ</span>
                                ${room_out}
                            </div>
                            <div class="form-group">
                                <span class="label_title">ห้อง</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                                ${roomname_out}
                            </div>
                            <div class="form-group">
                                <span class="label_title">เช็คอิน</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                ${startDate_out}
                            </div>
                            <div class="form-group">
                                <span class="label_title">เช็คเอาท์</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                ${endDate_out}
                            </div>
                        </div>
                    </div>
                </div>
                           
                <form action="updatecheckout" method="POST">
                    <input type="hidden" name="id" value="${id}">
                    <c:if test="${btCheckOut != null}">
                        <button type="submit" onclick="succesCheckOut()" class="btn btn-primary btn-sm"> <span class="glyphicon glyphicon-plus-sign"></span> เช็คเอาท์</button>    
                    </c:if>                    
                </form>   
                            </center>
            </div>

        </div>
    </div><!-- End Content --> 
    <BR><BR><BR>
</div><!-- End Page Content -->
<script language="JavaScript">
    function succesCheckin() {
        alert("เช็คอินสำเร็จ");
    }
        function succesCheckOut() {
        alert("เช็คเอาท์สำเร็จ");
    }  
</script>
    <jsp:include page="../footer.jsp" />
