<%-- 
    Document   : withdrawlist
    Created on : Dec 1, 2017, 8:20:20 AM
    Author     : pop
--%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:include page="../header.jsp" />
<c:if test="${roleadmin == 'admin'}">
    <jsp:include page="../navigation.jsp" />
</c:if>
<c:if test="${roleadmin == 'user'}">
    <jsp:include page="../navigation_top.jsp" />
</c:if>

<div id="page-wrapper">
    <div class="container-fluid">
        <!-- start Content -->
        <div class="row">
                <center>
            <div class="col-lg-12">    
                <h1 class="page-header">เช็คอินออนไลน์</h1>          
            </div></center>
        </div>
        <div class="row">
            <div class="col-lg-12" >
                <div class="tab-content">
                    <div class="tab-pane fade active in"> 
                        <div class="dl-horizontal">
                            <form   action="checkInOnline" method="POST"   >     
                                <center>
                                <div class="form-group">
                                    <span class="label_title">รหัสบัตรประชาชน</span>
                                    <input type="number" autocomplete="off" name="personal_cardid" maxlength="13" required=""  class="form-control width_20"   />
                                    <br>
                                    <br>  
                                    <span class="label_title">พระ</span>
                                    <select name="monk_id"  class="form-control width_20">
                                        <option  value="" abled=" " selected="">เลือกพระ</option>
                                        <c:forEach var="monklist" items="${monklist}" varStatus="loop">
                                            <option value="${monklist.monk_id}">${monklist.firstname}&nbsp;&nbsp;${monklist.lastname}(${monklist.nickname})</option>
                                        </c:forEach>
                                    </select>
                                    <button type="submit" class="btn btn-primary btn-sm"><span class="glyphicon glyphicon-search"></span> ค้นหา</button>
                                </div>
                                    </center>
                            </form>
                        </div>
                    </div>
                </div>    
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="table-responsive" >
                    <table class="table table-striped table-bordered table-hover">
                        <thead>
                            <tr>
                                <th class="text-center">พระวิปัสสนาจารย์</th>
                                <c:forEach var="date_table" items="${date_table}" varStatus="loop">
                                <th class="text-center">${date_table}</th>
                                 </c:forEach>
                                
                         
                            </tr>
                        </thead>
                        <tbody>
                            <c:forEach var="monk_date" items="${monk_date}" varStatus="loop">
                                <tr>
                                    <td class="text-center">${monk_date.monk_name}</td>
                                    <td class="text-center">${monk_date.day1} คน</td>
                                    <td class="text-center">${monk_date.day2} คน</td>
                                    <td class="text-center">${monk_date.day3} คน</td>
                                    <td class="text-center">${monk_date.day4} คน</td>
                                    <td class="text-center">${monk_date.day5} คน</td>
                                    <td class="text-center">${monk_date.day6} คน</td>
                                    <td class="text-center">${monk_date.day7} คน</td>
                                    <td class="text-center">${monk_date.day8} คน</td>
                                    <td class="text-center">${monk_date.day9} คน</td>
                                    <td class="text-center">${monk_date.day10} คน</td>
                                </tr>
                                           
                            </c:forEach>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <br>
        <br>
        <div class="row">
                    
            <div class="col-lg-12">
                <center>
                <c:if test="${statusCheck != null}">
                    <h4 style="color: red;">ไม่มีข้อมูล !!!</h4>
                </c:if>
                
                    <c:if test="${room != null}">
                        <div class="tab-content">
                            <div class="tab-pane fade active in"> 
                                <div class="dl-horizontal">     
                                    <h2 >รายละเอียดการจอง</h2>
                                    <h3 >อาคาร / กุฏิ  &nbsp; : &nbsp;&nbsp;   ${room}</h3>                                                    
                                    <h3 >ห้อง &nbsp;  : &nbsp;&nbsp;${roomname}</h3>                       
                                    <h3 >วันที่จอง &nbsp;  : &nbsp;&nbsp;${createdate} </h3>
                                    <h3 >เช็คอิน &nbsp;  : &nbsp;&nbsp;${startDate} </h3>
                                    <h3 >เช็คเอาท์  &nbsp;  : &nbsp;&nbsp;${endDate_out} </h3>
                                    <h3 >พระ  &nbsp;  : &nbsp;&nbsp;${monk_name} </h3>
                                </div>
                            </div>
                        </div>   
                    </c:if>
                
                           
                <form action="updatecheckin" method="POST">
                    <input type="hidden" name="id" value="${id}">
                    <input type="hidden" name="monk_id" value="${monk_id}">
                    <c:if test="${btCheckin != null}">
                        <button type="submit" onclick="succesCheckin()" class="btn btn-primary btn-sm"> <span class="glyphicon glyphicon-plus-sign"></span> เช็คอิน</button>    
                    </c:if>                    
                </form>   
                            </center>
            </div>

        </div>
    </div><!-- End Content --> 
    <BR><BR><BR>
</div><!-- End Page Content -->
<script language="JavaScript">
    function succesCheckin() {
       alert("เช็คอินสำเร็จ");
       
    }  
</script>
    <jsp:include page="../footer.jsp" />
