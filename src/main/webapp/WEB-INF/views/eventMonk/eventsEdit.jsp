<%-- 
    Document   : withdrawlist
    Created on : Dec 1, 2017, 8:20:20 AM
    Author     : pop
--%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:include page="../header.jsp" />
<c:if test="${roleadmin == 'admin'}">
    <jsp:include page="../navigation.jsp" />
</c:if>
<c:if test="${roleadmin == 'user'}">
    <jsp:include page="../navigation_top.jsp" />
</c:if>
<style>
    .row {
        margin-bottom: 10px;
    }

    label {
        margin: 7px 0;
    }

    .error-msg {
        color: red;
    }

    .btn-action {
        width: 100%;
        margin: 5px 0;
    }
    select[multiple] {
        height: 360px;;
    }
</style>

<div id="page-wrapper">
    <div class="container-fluid">
        <!-- start Content -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header" id="title_head">แก้ไขกิจนิมนต์</h1>
            </div>
        </div>
        <div class="row">
            <div class="container">
                <div class="tab-content">
                    <div class="tab-pane fade active in"> 
                        <div class="dl-horizontal">
                            <form:form action="updateEvent" modelAttribute="events" id="updateEvent" method="POST" >
                                <div class="row">
                                    <div class="col-lg-2">
                                        <label for="event_name" id="event_name_label">กิจนิมนต์:</label>
                                    </div>
                                    <div class="col-lg-4">
                                        <form:errors path="event_name" class="text-danger"/>
                                        <form:input path="event_name" id="event_name" class="form-control"/>
                                    </div>
                                    <div class="col-lg-2">
                                        <label for="event_income" id="event_name_label">ปัจจัย:</label>
                                    </div>
                                    <div class="col-lg-4">
                                        <form:errors path="income" class="text-danger"/>
                                        <form:input path="income" id="event_income" class="form-control"/>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-2">
                                        <label for="event_start" id="event_name_label">เลือกวันที่มีกิจนิมนต์:</label>
                                    </div>
                                    <div class="col-lg-4">
                                        <form:errors path="event_start" class="text-danger"/>
                                        <form:input path="event_start"  id="event_start" name = "event_start" class="form-control event_date" autocomplete="off"   />
                                    </div>
                                    <div class="col-lg-2">
                                        <label for="event_end" id="event_name_label">ถึงวันสิ้นสุดกิจนิมนต์:</label>
                                    </div>
                                    <div class="col-lg-4">
                                        <form:errors path="event_end" class="text-danger"/>
                                        <form:input path="event_end" id="event_end" name="event_end" class="form-control event_end_date" autocomplete="off" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-2">
                                        <label for="description" id="event_name_label">รายละเอียด:</label>
                                    </div> 
                                    <div class="col-lg-10">
                                        <form:errors path="description" class="text-danger"/>
                                        <form:textarea class="form-control" path="description"/>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-2">

                                    </div>
                                    <div class="col-lg-4">
                                        <form:select id="monk"  class="form-control " path="delete_monk_id" multiple="multiple" size="10">
                                            <c:forEach var="monk" items="${monklist}">     
                                                <c:if test="${not(monk.monk_id eq 0)}">
                                                    <form:option value="${monk.monk_id}">${monk.firstname}&nbsp;${monk.lastname}</form:option>

                                                </c:if> 
                                            </c:forEach>
                                        </form:select>
                                    </div>
                                    <div class="col-lg-2"style="text-align: center;" >
                                        <button class="btn btn-primary btn-action" type="button" onclick="selectMonk()">เลือก</button>
                                        <br/>
                                        <button class="btn btn-primary btn-action" type="button" onclick="removeMonk()">ลบ</button>
                                        <br/>
                                        <button class="btn btn-primary btn-action" type="button" onclick="selectAllMonk()">เลือกทั้งหมด</button>
                                        <br/>
                                        <button class="btn btn-primary btn-action" type="button" onclick="removeAllMonk()">ลบทั้งหมด</button>

                                    </div>
                                    <div class="col-lg-4">
                                        <!--${events.monks}-->
                                        <form:select id="select_monk" path="monks_id"  class="form-control " multiple="multiple" size="10">
                                            <c:forEach var="selectMonk" items="${events.monks}">                                          
                                                <form:option value="${selectMonk.monk_id}">${selectMonk.firstname}&nbsp;${selectMonk.lastname}</form:option>
                                            </c:forEach>
                                        </form:select>
                                    </div>

                                </div>
                                <form:hidden path="event_id" value ="${events.event_id}"/>
                                <%--<form:hidden path="monks" value ="${events.monks}"/>--%>
                            </form:form>
                            <br>
                            <center>
                                <div class="form-group text-center"><button type="button" class="btn btn-primary" onclick="succesRegis()"> <span class="fa fa-save "></span> บันทึก</button></div>
                            </center>                                     
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- End Content -->    
</div><!-- End Page Content -->

<script>
    function succesRegis() {
        var monk_id = $("#monk_id").val();
        var event_start = $("#event_start").val();
        var event_end = $("#event_end").val();
        var event_name = $("#event_name").val();
        var description = $("#description").val();



        $.each($("#select_monk option"), function () {
//                $(this).remove();
            $(this).attr('selected', true);

        });
        $.each($("#monk option"), function () {
//                $(this).remove();
            $(this).attr('selected', true);

        });
        $("#updateEvent").submit();

    }

    function selectMonk() {
        $.each($("#monk option:selected"), function () {
            $(this).remove();
            var html = '<option value="' + $(this).val() + '" selected>' + $(this).html();
            html += '</option>';
            $('#select_monk').append(html);
        });
//        alert("You have selected the country - " + countries.join(", "));
    }

    function selectAllMonk() {
        $.each($("#monk option"), function () {
            $(this).remove();
            var html = '<option value="' + $(this).val() + '">' + $(this).html();
            html += '</option>';
            $('#select_monk').append(html);
        });
    }

    function removeMonk() {
        $.each($("#select_monk option:selected"), function () {
            $(this).remove();
            var html = '<option value="' + $(this).val() + '">' + $(this).html();
            html += '</option>';
            $('#monk').append(html);
        });
    }

    function removeAllMonk() {
        $.each($("#select_monk option"), function () {
            $(this).remove();
            var html = '<option value="' + $(this).val() + '">' + $(this).html();
            html += '</option>';
            $('#monk').append(html);
        });
    }

</script>
<jsp:include page="../footer.jsp" />    
