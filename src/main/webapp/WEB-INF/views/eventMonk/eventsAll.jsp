<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html;charset=UTF-8" %>
<jsp:include page="../header.jsp" />
<c:if test="${roleadmin == 'admin'}">
    <jsp:include page="../navigation.jsp" />
</c:if>
<c:if test="${roleadmin == 'user'}">
    <jsp:include page="../navigation_top.jsp" />
</c:if>
<style>

    @media only screen and (min-width: 1024px) and (max-width: 1366px){
        .modal-dialog{
            width: 800px;
        }
    }

    @media only screen and (min-width: 1440px) and (max-width: 1980px){
        .modal-dialog{
            width: 900px;
        }
    }
</style>
<!-- Page Content -->
<div id="page-wrapper">
    <div class="container-fluid">
        <!-- start Content -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header" id="title_head">กิจนิมนต์</h1>    
            </div>
        </div>

        <div class="row">
            <c:if test="${roleadmin == 'admin'}">
                <div class="col-lg-12">
                </c:if>
                <c:if test="${roleadmin == 'user'}">
                    <div class="col-lg-10 col-lg-offset-1">
                    </c:if>
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover">
                            <thead>
                                <tr>

                                    <th class="text-center">กิจนิมนต์</th>
                                    <th class="text-center">วันที่</th>
                                    <th class="text-center">ถึงวันที่</th>
                                    <th class="text-center">ปัจจัย</th>
                                    <th class="text-center">ดูรายละเอียด</th>
                                    <th class="text-center">แก้ไข</th>
                                    <th class="text-center">ลบ</th>
                                </tr>
                            </thead>
                            <tbody >
                                <c:forEach var="events" items="${eventslist}"  varStatus="loop">
                                    <tr>

                                        <td class="text-center">${events.event_name}</td> 
                                        <td class="text-center" id="start${loop.index}">${events.event_start}</td>
                                        <td class="text-center" id="end${loop.index}">${events.event_end}</td>
                                        <td class="text-center">${events.income} บาท</td>
                                        <td class="text-center">
                                            <button type="button" class="no-border tran-bg" data-toggle="modal" data-target="#flipFlop${events.event_id}">
                                                <i  style="color: green;" class="fa fa-search-plus" style="font-size: 30px"></i>
                                            </button>
                                        </td>
                                        <td class="text-center">
                                            <c:if test="${page_Edit != null}">
                                                <form  action="selecteditevents" method="get" >
                                                    <input type="hidden" value="${events.event_id}" name="event_id"/>
                                                    <button type="submit" class="no-border tran-bg "><i class="fa fa-edit edit-btn" style="font-size: 30px"></i></button>
                                                </form>
                                            </c:if>
                                        </td>
                                        <td class="text-center">
                                            <c:if test="${page_Delete != null}">
                                                <form  id="delete_${events.event_id}" action="deleteevent" method="post" >                                                                  
                                                    <input type="hidden" value="${events.event_id}" name="event_id"/>
                                                    <div  onclick="chkConfirm(${events.event_id});" ><i style="color: red;" class="fa fa-trash fa-2x" aria-hidden="true"></i></div>
                                                </form>
                                            </c:if> 
                                        </td>
                                    </tr>
                                    <!-- The modal -->
                                <div class="modal fade" id="flipFlop${events.event_id}" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <div class="row">
                                                    <div class="col-lg-12 " style="font-size: 20px;">
                                                        <dl class="dl-horizontal">
                                                            <div class="form-group">
                                                                <div class="col-lg-6">
                                                                    <span class="label_title modal_label">กิจนิมนต์ : </span> 
                                                                    <span id="modal_span">${events.event_name}</span> 
                                                                </div>
                                                                <div class="col-lg-6">
                                                                    <span class="label_title modal_label">ปัจจัย: </span> 
                                                                    <span id="modal_span">${events.income} บาท</span> 
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <div class="col-lg-6">
                                                                    <span class="label_title modal_label">เลือกวันที่มีกิจนิมนต์ : </span><span id="modal_span">${events.event_start}</span> 
                                                                </div>
                                                                <div class="col-lg-6">
                                                                    <span class="label_title modal_label">ถึงวันสิ้นสุดกิจนิมนต์ : </span><span id="modal_span">${events.event_end}</span>
                                                                </div>
                                                            </div>                                     
                                                            <div class="form-group">
                                                                <div class="col-lg-12">
                                                                    <span class="label_title modal_label">รายละเอียด : </span>
                                                                    <br>
                                                                    <span id="modal_span">
                                                                        ${events.description}
                                                                    </span>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <div class="col-lg-12">
                                                                    <span class="label_title modal_label">พระลูกวัด : </span>
                                                                    <br>
                                                                    <c:forEach var="monks" items="${events.monks}"  varStatus="loop">
                                                                        <span id="modal_span">
                                                                            - ${monks.firstname} &nbsp;&nbsp; ${monks.lastname}
                                                                        </span><br/>
                                                                    </c:forEach>
                                                                </div>
                                                            </div>
                                                        </dl>
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="modal-footer">
                                                <c:if test="${page_Edit != null}">
                                                    <form  action="selecteditevents" method="get" >
                                                        <input type="hidden" value="${events.event_id}" name="event_id"/>
                                                        <button type="submit" class="btn btn-primary">แก้ไขข้อมูล</button>
                                                    </form>
                                                </c:if>
                                                <a id="downloadReport${events.event_id}" href="#" type="button"  class="btn btn-success" onclick="submitFromCSV(${events.event_id})">ออกรายงาน</a>
                                                <button type="button" class="btn btn-danger" data-dismiss="modal">ปิด</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!------------------------------->

                            </c:forEach>
                            </tbody>
                        </table>
                    </div>
                    <div class="row">
                        <center>
                            <div class="pagination_center">
                                <nav aria-label="Page navigation col-centered">
                                    <ul class="pagination">
                                        <c:if test="${page_priv == true}">  
                                            <li class="page-item"><a class="page-link"  href="${pageContext.request.contextPath}/eventsall?page=${page-1}"><i class="fa fa-angle-left  "></i></a></li>
                                                </c:if>
                                                <c:if test="${page_count != 0}">
                                                    <c:forEach begin="0" end="${page_count-1}" varStatus="loop">
                                                        <c:if test="${page == loop.index}"> 
                                                    <li class="page-item"><a class="page-link active"  href="${pageContext.request.contextPath}/eventsall?page=${loop.index}">${loop.index+1}</a></li>
                                                    </c:if> 
                                                    <c:if test="${page != loop.index}"> 
                                                    <li class="page-item"><a class="page-link"  href="${pageContext.request.contextPath}/eventsall?page=${loop.index}">${loop.index+1}</a></li>
                                                    </c:if> 
                                                </c:forEach>
                                            </c:if>
                                            <c:if test="${page_next == true}"> 
                                            <li class="page-item"><a class="page-link" href="${pageContext.request.contextPath}/eventsall?page=${page+1}"><i class="fa fa-angle-right "></i></a></li>
                                                </c:if>
                                    </ul>
                                </nav>
                            </div>
                    </div><!--end row-->
                </div>
            </div>

        </div>


    </div>

    <!-- End Content -->
    <script language="JavaScript">
        $(document).ready(function () {
            for (var i = 0; i < 5; i++) {

                var start = new Date($('#start' + i).html());
                var day = start.getDate();
                var month = start.getMonth() + 1;
                var year = start.getFullYear() + 543;
                var fullStartDate = day + "/" + month + "/" + year;
                $('#start' + i).html(fullStartDate);

                var end = new Date($('#end' + i).html());
                var endDay = end.getDate();
                var endMonth = end.getMonth() + 1;
                var endYear = end.getFullYear() + 543;
                var fullendDate = endDay + "/" + endMonth + "/" + endYear;
                $('#end' + i).html(fullendDate);


            }
        });
        function chkConfirm(reg_id) {
            bootbox.confirm({
                message: "ต้องการลบหรือไม่ ?",
                buttons: {
                    confirm: {
                        label: 'Yes',
                        className: 'btn-success'
                    },
                    cancel: {
                        label: 'No',
                        className: 'btn-danger'
                    }
                },
                callback: function (result) {
                    if (result) {
                        $("#delete_" + reg_id).submit();
                    }
                    console.log('This was logged in the callback: ' + result + "#delete_" + reg_id);
                }
            });
        }

        const b64toBlob = (b64Data, contentType = '', sliceSize = 512) => {
            console.log(contentType);
            const byteCharacters = atob(b64Data);
            const byteArrays = [];

            for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
                const slice = byteCharacters.slice(offset, offset + sliceSize);

                const byteNumbers = new Array(slice.length);
                for (let i = 0; i < slice.length; i++) {
                    byteNumbers[i] = slice.charCodeAt(i);
                }

                const byteArray = new Uint8Array(byteNumbers);
                byteArrays.push(byteArray);
            }

            const blob = new Blob(byteArrays, {type: contentType});
            return blob;
        };

        function submitFromCSV(id) {
            let data = {
                "event_id": id,
                "action": ''
            };
            $.ajax({
                url: "${pageContext.request.contextPath}/eventreport",
                type: 'POST',
                data: data,
                async: false,
                success: function (data, textStatus, jqXHR) {
                    const blob = b64toBlob(data, 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
                    blobUrl = URL.createObjectURL(blob);
                }, complete: function (jqXHR, textStatus) {
                    $('#downloadReport' + id).attr({
                        'download': 'รายงานสรุปกิจนิมนต์.xlsx',
                        'href': blobUrl
                    });
                }
            });
//        $('#print_form').submit();
        }
    </script>
    <jsp:include page="../footer.jsp" />  

