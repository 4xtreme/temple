<%-- 
    Document   : withdrawlist
    Created on : Dec 1, 2017, 8:20:20 AM
    Author     : pop
--%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:include page="../header.jsp" />
<c:if test="${roleadmin == 'admin'}">
    <jsp:include page="../navigation.jsp" />
</c:if>
<c:if test="${roleadmin == 'user'}">
    <jsp:include page="../navigation_top.jsp" />
</c:if>
<div id="page-wrapper">
    <div class="container-fluid">
        <!-- start Content -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">รายละเอียดกิจนิมนต์</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-2"></div>
            <div class="col-lg-8">
                <div class="tab-content">
                    <div class="tab-pane fade active in"> 
                        <div class="dl-horizontal">
                            <form action="updateEvent"  method="POST"  >


                                <div class="form-group">
                                    <input type="hidden" name="event_id" value="${eventmonk.event_id}">
                                    <span class="label_title">พระวิปัสสนาจารย์</span>
                                    
                                        
                                        <c:forEach var = "monk" items="${monklist}">
                                            <c:if test="${monkid eq monk.monk_id}">
                                            ${monk.firstname}&nbsp;${monk.lastname}
                                            </c:if> 
                                        </c:forEach>
                                    
                                 </div>   
                                    <div class="form-group">
                                    <span class="label_title">เลือกวันที่มีกิจนิมนต์</span>${eventmonk.event_start}                                    
                                    &nbsp;&nbsp;&nbsp;&nbsp;
                                    <span class="label_title">ถึงวันสิ้นสุดกิจนิมนต์</span>${eventmonk.event_end}
                                    </div>
                                    <div class="form-group">
                                         <span class="label_title">กิจนิมนต์</span>${eventmonk.event_name}                                         
                                       </div>
                                    <div class="form-group">
                                         <span class="label_title">รายละเอียด</span>
                                         <br><br>
                                         <article>
                                             
                                             ${eventmonk.description}
                                             
                                         </article>
                                       </div>
                                     <br>                                    
                                
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-2"></div>
        </div>
    </div><!-- End Content -->    
</div><!-- End Page Content -->
<script <script language="JavaScript">
    $(document).ready(function () {

        var builder = document.getElementById("check_data");

        if (builder.value == 0) {
            $("#myTable").hide();
        } else {
            $("#myTable").show();
        }

        function showTable() {
            $("#check_data").val(1);
            $("#myTable").show();
        }



    });
    function getRowData(test) {
        alert(test);
        //$("#checkin").submit();
    }
    </script>
 <jsp:include page="../footer.jsp" />    
