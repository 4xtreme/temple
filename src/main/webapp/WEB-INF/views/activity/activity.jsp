<%-- 
    Document   : activity
    Created on : Sep 4, 2019, 10:35:54 AM
    Author     : pop
--%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html;charset=UTF-8" %>
<jsp:include page="../header.jsp" />
<c:if test="${roleadmin == 'admin'}">
    <jsp:include page="../navigation.jsp" />
</c:if>
<c:if test="${roleadmin == 'user'}">
    <jsp:include page="../navigation_top.jsp" />
</c:if>
<!-- Page Content -->
<div id="page-wrapper">
    <div class="container-fluid">
        <!-- start Content -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header" id="title_head">ปฏิบัติศาสนกิจ</h1>
            </div>
        </div>
        <div class="row">
            <c:if test="${roleadmin == 'admin'}">
                <div class="col-lg-12">
                </c:if>
                <c:if test="${roleadmin == 'user'}">
                    <div class="col-lg-10 col-lg-offset-1">
                    </c:if>
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th class="text-center">พระวิปัสสนาจารย์</th>
                                    <th class="text-center">ศาสนกิจ</th>
                                    <th class="text-center">วันที่</th>
                                    <th class="text-center">ถึงวันที่</th>
                                    <th class="text-center">ดูรายละเอียด</th>
                                    <th class="text-center">แก้ไข</th>
                                    <th class="text-center">ลบ</th>
                                </tr>
                            </thead>
                            <tbody >
                                <c:forEach var="activity" items="${activities}"  varStatus="loop">
                                    <tr>
                                        <td class="text-center">${activity.vipassana_name}</td>
                                        <td class="text-center">${activity.activity_name}</td> 
                                        <td class="text-center" id="start${loop.index}">${activity.activity_start}</td>
                                        <td class="text-center" id="end${loop.index}">${activity.activity_end}</td>
                                        <td class="text-center">
                                            <button type="button" class="no-border tran-bg" data-toggle="modal" data-target="#flipFlop${activity.id}">
                                                <i  style="color: green;" class="fa fa-search-plus" style="font-size: 30px"></i>
                                            </button>
                                        </td>
                                        <td class="text-center">
                                            <c:if test="${monk_page.edit_page == true}">
                                                <form  action="editactivity" method="get" >
                                                    <input type="hidden" value="${activity.id}" name="id"/>
                                                    <button type="submit" class="no-border tran-bg "><i class="fa fa-edit edit-btn" style="font-size: 30px"></i></button>
                                                </form>
                                            </c:if>
                                        </td>
                                        <td class="text-center">
                                            <c:if test="${monk_page.delete_page == true}">
                                                <form  id="delete_${activity.id}" action="deleteactivity" method="post" >                                                                  
                                                    <input type="hidden" value="${activity.id}" name="id"/>
                                                    <div  onclick="chkConfirm(${activity.id});" ><i style="color: red;" class="fa fa-trash fa-2x" aria-hidden="true"></i></div>
                                                </form>
                                            </c:if> 
                                        </td>
                                    </tr>
                                    <!-- The modal -->
                                <div class="modal fade" id="flipFlop${activity.id}" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                                <h3 class="modal-title" id="modalLabel"  >${activity.vipassana_name}</h3>
                                            </div>
                                            <div class="modal-body">
                                                <div class="row">
                                                    <div class="col-lg-12 " style="font-size: 20px;">
                                                        <dl class="dl-horizontal">
                                                            <div class="form-group">
                                                                <span class="label_title modal_label">เลือกวันที่มีกิจนิมนต์ : </span><span id="modal_span">${activity.activity_start}</span>  <br>                                  
                                                                <span class="label_title modal_label">ถึงวันสิ้นสุดกิจนิมนต์ : </span><span id="modal_span">${activity.activity_end}</span>
                                                            </div>
                                                            <div class="form-group">
                                                                <span class="label_title modal_label">กิจนิมนต์ : </span><span id="modal_span">${activity.activity_name}</span>                                    
                                                            </div>
                                                            <div class="form-group">
                                                                <span class="label_title modal_label">รายละเอียด : </span>
                                                                <br>
                                                                <span id="modal_span">
                                                                    ${activity.detail}
                                                                </span>
                                                            </div>
                                                        </dl>
                                                    </div>

                                                </div>

                                            </div>
                                            <div class="modal-footer">
                                                <c:if test="${monk_page.edit_page == true}">
                                                    <form  action="editactivity" method="get" >
                                                        <input type="hidden" value="${activity.id}" name="id"/>
                                                        <button type="submit" class="btn btn-primary">แก้ไขข้อมูล</button>
                                                    </form>
                                                </c:if>
                                                <button type="button" class="btn btn-danger" data-dismiss="modal">ปิด</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!------------------------------->

                            </c:forEach>
                            </tbody>
                        </table>
                    </div>
                    <div class="row">
                        <center>
                            <div class="pagination_center">
                                <nav aria-label="Page navigation col-centered">
                                    <ul class="pagination">
                                        <c:if test="${page_priv == true}">  
                                            <li class="page-item">
                                                <a class="page-link"  href="${pageContext.request.contextPath}/activity?page=${page-1}">
                                                    <i class="fa fa-angle-left  "></i>
                                                </a>
                                            </li>
                                        </c:if>
                                        <c:if test="${page_count != 0}">
                                            <c:forEach begin="0" end="${page_count-1}" varStatus="loop">
                                                <c:if test="${page == loop.index}"> 
                                                    <li class="page-item">
                                                        <a class="page-link active"  href="${pageContext.request.contextPath}/activity?page=${loop.index}">${loop.index+1}</a>
                                                    </li>
                                                </c:if> 
                                                <c:if test="${page != loop.index}"> 
                                                    <li class="page-item">
                                                        <a class="page-link"  href="${pageContext.request.contextPath}/activity?page=${loop.index}">${loop.index+1}</a>
                                                    </li>
                                                </c:if> 
                                            </c:forEach>
                                        </c:if>
                                        <c:if test="${page_next == true}"> 
                                            <li class="page-item">
                                                <a class="page-link" href="${pageContext.request.contextPath}/activity?page=${page+1}"><i class="fa fa-angle-right "></i></a>
                                            </li>
                                        </c:if>
                                    </ul>
                                </nav>
                            </div>
                        </center>
                    </div><!--end row-->
                </div>
            </div>

        </div>


    </div>

    <!-- End Content -->
    <script language="JavaScript">
        $(document).ready(function () {
            for (var i = 0; i < 5; i++) {
                var start = new Date($('#start' + i).html());
                var end = new Date($('#end' + i).html());
                var day = start.getDate();
                var month = start.getMonth() + 1;
                var year = start.getFullYear() + 543;
                var fullStartDate = day + "/" + month + "/" + year;

                var endDay = end.getDate();
                var endMonth = end.getMonth() + 1;
                var endYear = end.getFullYear() + 543;
                var fullendDate = endDay + "/" + endMonth + "/" + endYear;
                $('#start' + i).html(fullStartDate);
                $('#end' + i).html(fullendDate);

            }
        });
        function chkConfirm(reg_id) {
            bootbox.confirm({
                message: "ต้องการลบหรือไม่ ?",
                buttons: {
                    confirm: {
                        label: 'Yes',
                        className: 'btn-success'
                    },
                    cancel: {
                        label: 'No',
                        className: 'btn-danger'
                    }
                },
                callback: function (result) {
                    if (result) {
                        $("#delete_" + reg_id).submit();
                    }
                    console.log('This was logged in the callback: ' + result + "#delete_" + reg_id);
                }
            });
        }


    </script>
    <jsp:include page="../footer.jsp" />  


