<%-- 
    Document   : edit_activity
    Created on : Sep 4, 2019, 1:02:09 PM
    Author     : pop
--%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:include page="../header.jsp" />
<c:if test="${roleadmin == 'admin'}">
    <jsp:include page="../navigation.jsp" />
</c:if>
<c:if test="${roleadmin == 'user'}">
    <jsp:include page="../navigation_top.jsp" />
</c:if>
<style>
    .row {
        margin-bottom: 10px;
    }

    label {
        margin: 7px 0;
    }
    
    .error-msg {
        color: red;
    }
</style>
<div id="page-wrapper">
    <div class="container-fluid">
        <!-- start Content -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header" id="title_head">เพิ่มศาสนกิจ</h1>
            </div>
        </div>
        <div class="row">
            <div class="container">
                <div class="tab-content">
                    <div class="tab-pane fade active in"> 
                        <div class="dl-horizontal">
                            <form:form action="editactivity" id="editactivity" modelAttribute="activity" method="POST"  >
                                <fieldset>
                                    <div class="row">
                                        <div class="col-lg-2">
                                            <label for="activity_name">ชื่อศาสนกิจ:</label>
                                        </div> 
                                        <div class="col-lg-10">
                                            <form:errors path="activity_name" class="error-msg"/>
                                            <form:input class="form-control" path="activity_name"/>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-2">
                                            <label for="vipassana_id">พระวิปัสสนาจารย์:</label>
                                        </div> 
                                        <div class="col-lg-10">
                                            <form:errors path="vipassana_id" class="error-msg"/>
                                            <form:select path="vipassana_id" id="vipassana_id" class="form-control"> 
                                                <form:option value="0" disabled="true" selected="true">รายชื่อพระวิปัสสนาจารย์</form:option>
                                                <c:forEach var = "vipassana" items="${vipassanas}">         
                                                    <form:option value="${vipassana.id}" >${vipassana.firstname}&nbsp;${vipassana.lastname}</form:option>
                                                </c:forEach>
                                            </form:select>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-2">
                                            <label for="activity_start">วันเริ่มปฏิบัติศาสนกิจ:</label>
                                        </div>                                        
                                        <div class="col-lg-4">
                                            <!--<input value="${activity.activity_start}" type="text" id="activity_start" autocomplete="off" name="activityStart" class="form-control form_start "/>-->
                                            <form:errors path="activity_start" class="error-msg"/>
                                            <form:input path="activity_start" type="text" id="activity_start" autocomplete="off" name="activityStart"  class="form-control event_date"/>
                                        </div>
                                        <div class="col-lg-2" style="text-align: center;">
                                            <label for="activity_end" >วันสิ้นสุดปฏิบัติศาสนกิจ:</label>
                                        </div>
                                        <div class="col-lg-4">
                                            <!--<input value="${activity.activity_end}" type="text" id="activity_end" autocomplete="off" name="activityEnd" class="form-control form_start"/>-->
                                            <form:errors path="activity_end" class="error-msg"/>
                                            <form:input path="activity_end" type="text" id="activity_end" autocomplete="off" name="activityEnd"  class="form-control event_end_date"/>

                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-2">
                                            <label for="detail">รายละเอียดศาสนกิจ:</label>
                                        </div> 
                                        <div class="col-lg-10">
                                            <form:errors path="detail" class="error-msg"/>
                                            <form:textarea class="form-control" path="detail"/>
                                        </div>
                                    </div>
                                    <form:hidden path="temple_id" value ="${activity.temple_id}"/>
                                    <form:hidden path="id" value ="${activity.id}"/>
                                    <br>
                                    <center>
                                        <div class="col-lg-5"></div>
                                        <div class="form-group text-left"><button type="submit" class="btn btn-primary"  <span class="fa fa-save "></span> บันทึก</button></div>
                                    </center>  
                                </fieldset>

                            </form:form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- End Content -->    
</div><!-- End Page Content -->
<script>
    function succesRegis() {
        var id = $("#monk_id").val();
        var activity_start = $("#event_start").val();
        var activity_end = $("#event_end").val();
        var event_name = $("#event_name").val();
        var description = $("#description").val();


        if (monk_id === null) {
            alert('กรุณาเลือกพระวิปัสสนาจารย์');
        } else if (event_start == '') {
            alert('เลือกวันที่มีกิจนิมนต์');
        } else if (event_end == '') {
            alert('กรุณาวันสิ้นสุดกิจนิมนต์');
        } else if (event_name == '') {
            alert('กรุณาเพิ่มกิจนิมนต์');
        } else if (description == '') {
            alert('กรุณาเพิ่มรายละเอียด');
        } else {
            $("#addevents").submit();
        }
    }
</script>
<jsp:include page="../footer.jsp" />    
