<%-- 
    Document   : addwithdraw
    Created on : Nov 24, 2017, 8:33:19 AM
    Author     : pop
--%>

<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<jsp:include page="../header.jsp" />
<jsp:include page="../navigation.jsp" />
<!-- Page Content -->
<div id="page-wrapper">
    <div class="container-fluid">
        <!-- start Content -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header"><spring:message code="header.withdraw"/></h1>
            </div>
        </div>
        <div class="row" id="eiei">
            <div class="col-lg-12">
                <div class="tab-content">
                    <div class="tab-pane fade active in">
                        <form action="withdrawlist" method="GET" id="tolist"></form>
                        <form:form action="addwithdraw" modelAttribute="withdraw"   method="POST" enctype="multipart/form-data">
                            <fieldset>
                                <div class="row">
                                    <div class="col-lg-8" >
                                        <div class="dl-horizontal">
                                            <div class="form-group">
                                                <span class="label_title">ผู้ขอเบิก</span>
                                                &emsp;
                                                <form:select path="withdrawal_id" name="withdrawal_id" class="form-control width_20">
                                                    <option  value="0" disabled=" " selected="">เลือกรายการ</option>
                                                    <c:forEach var="staff" items="${staff}" varStatus="loop">
                                                        <option value="${staff.id}">${staff.fristname}</option>
                                                    </c:forEach>
                                                </form:select><form:errors path="withdrawal_id" />
                                                &emsp;&emsp;&emsp;&emsp;&emsp;
                                                <span class="label_title">คนขับ/ควบคุมเครื่อง</span>
                                                &emsp;
                                                <form:select path="driver_id" class="form-control width_20">
                                                    <option  value="" disabled=" " selected="">เลือกรายการ</option>
                                                    <c:forEach var="staff" items="${staff}" varStatus="loop">
                                                        <option value="${staff.id}">${staff.fristname}</option>
                                                    </c:forEach>
                                                </form:select><form:errors path="driver_id" /> 
                                            </div>
                                            <div class="form-group">
                                                <span class="label_title">จำนวนเงิน</span>
                                                <form:input type="text" path="amount" class="form-control width_20" /><form:errors path="amount"/>
                                                <span>บาท</span>
                                                &emsp;&emsp;&nbsp;&nbsp;
                                                <span class="label_title">ใช้ในโครงการ</span>
                                                <form:select path="project_id" class="form-control width_25">
                                                    <option  value="" disabled=" " selected="">เลือกรายการ</option>
                                                    <c:forEach var="project" items="${project}" varStatus="loop">
                                                        <option value="${project.project_id}">${project.project_name}</option>
                                                    </c:forEach>
                                                </form:select><form:errors path="project_id" /> 
                                            </div>
                                            <div class="form-group">
                                                <div class="col-lg-4">
                                                    <dt style="text-align: left; width: 100px"><span>วิธีจ่ายเงิน</span></dt> 
                                                    <dd style="text-align: left;margin-left: 100px">
                                                        <label style="display: block">
                                                            <input  id="pay1" type="radio" name="radio" value="1" checked="true" onclick="getCheckedSection()"/><span class="label_title"> เงินสด </span>
                                                        </label >
                                                        <label style="display: block">
                                                            <input  id="pay2" type="radio" name="radio" value="2" onclick="getCheckedSection()"/><span class="label_title"> เชื่อ/เครดิต </span>
                                                        </label>
                                                        <label style="display: block">
                                                            <input  id="pay3" type="radio" name="radio" value="3" onclick="getCheckedSection()"/><span class="label_title"> โอนธนาคาร </span>
                                                        </label>
                                                        <label style="display: block">
                                                            <input id="pay4" type="radio" name="radio" value="4" onclick="getCheckedSection()"/><span class="label_title"> เช็ค </span>
                                                        </label>
                                                        <form:input hidden="true" path="pay_type" id="pay_value" value="1"/>
                                                    </dd>
                                                </div>

                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                <span>กำหนดชำระ</span>
                                                <i class="fa fa-calendar " style="font-size: 20px"></i>
                                                <form:input path="pay_date" type="text" class="datetime"  id="datepick"/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<form:errors path="pay_date"/></br>
                                                <span class="margin_left10" >อีก 
                                                    <input  disabled="true" class="no-border tran-bg" id="dayleft" value="0" style="width: 20px;text-align: center"/> 
                                                    <form:input path="pay_date_count" hidden="true" class="no-border tran-bg" id="dayleft2" value="0" style="width: 20px;text-align: center"/> 
                                                    วัน</span>
                                            </div></br></br></br></br>

                                            <div class="form-group">
                                                <span>จ่ายให้กับ</span>
                                                <select path="" class="form-control width_25">
                                                    <option  value="" disabled=" " selected="">เลือกรายการ</option>
                                                    <option value="5">ร้าย XYZ อะไหล่ (2000)</option>
                                                </select><form:errors path=""/>
                                                <p class="margin_left10"></p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="dl-horizontal">
                                            <div class="form-group">
                                                <span> วันที่ </span>
                                                <input type="text" path="" class="no-border tran-bg " disabled="" value="${date}"/>
                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            </div>
                                            <div class="form-group">
                                                <span>หมายเลขใบเบิก : </span>
                                                <input type="text" path="" class="no-border tran-bg "disabled=""/>
                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            </div>
                                            <div class="form-group">
                                                <span>ผู้กรอกข้อมูล : </span>
                                                <input type="text" path="" class="no-border tran-bg" disabled="" value="${user.firstname}"/>
                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            </div>
                                            <div class="form-group">
                                                <span>สถานะ : </span>
                                                <input type="text" path="" class="no-border tran-bg " value="ร่าง" disabled=""/>
                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            </div>
                                            <div class="form-group">
                                                <span>ปรับปรุงหลังสุดเมื่อ  : </span>
                                                <input type="text" path="" class="no-border tran-bg "disabled=""/>
                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <span>สำหรับเครื่องจักร หมายเลขทะเบียน/รหัส</span>
                                            <input type="text" name="searchnum" form="search_machine" id="search_input" />
                                            <i type="button" onclick="searchMachine()"   class="fa fa-search" style="font-size:25px; color: skyblue;"></i>
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        </div>
                                        <div class="form-group">
                                            <form:input path="machine_id" id="machineId" hidden="true" value=""/>
                                            <input type="text" id="machineInfo" class="margin_left10 width_15" disabled="disable"value=""/>
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <span>หมวดเครื่องจัก : </span> 
                                            <input type="text" id="machineCategory" class="no-border tran-bg " disabled="disable"value="" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <span>อายุใช้งาน : </span>
                                            <input type="text" path="" class="no-border tran-bg" disabled="disable"value="3/4/20"/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <span>ที่ตั้งปัจจุบัน : </span>
                                            <input type="text" path="" class="no-border tran-bg" disabled="disable"value="โครงการ XYZ"/>
                                        </div>
                                    </div>
                                </div>
                                </br>
                                </br>
                                </br>
                                <div class="row">
                                    <div class="col-lg-12" style="border-top: solid; border-width: 1px; ">
                                        </br></br>
                                        <div class="dl-horizontal">
                                            <input type="button" onclick="createRow()" id="add-btn" type="botton" value="เพิ่มรายการในตาราง" class="btn" />
                                            </br>
                                            </br>
                                            <div class="form-group ">
                                                <span>อุปกรณ์ที่เสียหาย/รายการ</span>
                                                <select id='brokenitem'  class="form-control width_25"> 
                                                    <option  value="" disabled=" " selected="">เลือกรายการ</option>
                                                    <option  value="ยาง"  >ยาง</option>
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <span>ประเภทการเสีย/ซ่อม</span>
                                                <select id="type" class="form-control width_10"> 
                                                    <option  value="" disabled=" " selected="">เลือกรายการ</option>
                                                    <option  value="ซ่อม"  >ซ่อม</option>
                                                </select>
                                                <span class="margin_left10">จำนวน</span>
                                                <input type="number" name="quantity" min="0"  value="0" id="count" class="form-control width_5">
                                                <span class="margin_left10">ราคาประเมินต่อหน่วย</span>
                                                <input type="text" path="" id="price" value="0" class="width_10"/>
                                                <span >บาท</span>
                                                <span class="margin_left10">ยี่ห้อ/หมายเหตุ</span>
                                                <input type="text" path="" id="brand" value="-"/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                </br>
                                <div class="row">
                                    <div class="col-lg-10">
                                        <div class="table-responsive" id="app1">
                                            <table class="table table-striped table-bordered table-hover del-line" id="mytable">
                                                <thead >
                                                    <tr >
                                                        <th class="text-center">ลำดับ</th>
                                                        <th class="text-center">อุปกรณที่เสีย</th>
                                                        <th class="text-center">ประเภทการเสียซ่อม</th>
                                                        <th class="text-center">จำนวน</th>
                                                        <th class="text-center">ราคา/หน่วย</th>
                                                        <th class="text-center">ราคารวม</th>
                                                        <th class="text-center">ยี่ห่อ/หมายเหตุ</th>
                                                        <th class="text-center" hidden="true" style="border: none"></th>
                                                        <th class="text-center"hidden="true" style="border: none"></th>
                                                    </tr>
                                                </thead>

                                                <tbody   id="tableToModify" >
                                                    <!-- สร้างตารางใหม่ -->

                                                </tbody>
                                                <input name="itemslist" type="hidden" value="0,0,0,0,0,0,0">
                                            </table>
                                        </div>
                                        <div class="form-group">
                                            <div class="row margin_top10"> 
                                                <div class="col-xs-6 " style="margin-left: 15px">
                                                    <label class="fileContainer">
                                                        <i class="fa fa-link fa-flip-horizontal attach-file" aria-hidden="true"></i>
                                                        <input type="file" id="uploadBtn" path="withdraw_file"  name="file_withdraw"  />
                                                    </label>
                                                    <span style="line-height: 60px">แนบไฟล์</span></br>
                                                    <input id="uploadFile" class="form-control-sm no-border width_50 file_dir2" type="text" value="No file selected."  disabled="disabled" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<form:errors path="withdraw_file" /><br>
                                                </div>
                                                <div class="col-xs-5">
                                                    <strong>
                                                        <span>รวมทั้งสิ้น  </span>
                                                        <input id="totalPrice" type="text" path="" class="no-border tran-bg-right width_15" disabled="disable"value="0"/>
                                                        <form:input hidden="true" id="totalPrice2" type="text" path="total_price" value="0"/>
                                                        <span>  บาท  </span>
                                                    </strong>
                                                    </br>
                                                    </br>

                                                </div>		
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <c:if test="${user.role  == 'user'}">
                                        <center>
                                            <div class="form-group">
                                                <span>บันทึกผู้ขอเบิก&nbsp;</span>
                                                <form:textarea path="withdrawal_note" class="custom-textarea"></form:textarea>
                                                </div>
                                            </center>
                                    </c:if>  
                                    <c:if test="${user.role  == 'money'}">
                                        <center>

                                            <div class="form-group">
                                                <div style="width: 120px;display: inline-block">
                                                    <span >บันทึกผู้ตรวจสอบ / พนักงานการเงิน</span>
                                                </div>
                                                <form:textarea path="inspector_note" class="custom-textarea"></form:textarea>
                                                </div>
                                            </center>
                                    </c:if>
                                    <c:if test="${user.role  == 'admin'}">
                                        <center>
                                            <div class="form-group">
                                                <span>บันทึกผู้อนุมัติ &nbsp;</span>
                                                <form:textarea path="approvers_note" class="custom-textarea"></form:textarea>
                                                </div>
                                            </center>
                                    </c:if>
                                </div>
                                </br>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <center>
                                            <button class="btn  button-size-cancel" id="cancel_form" form="tolist">Cancel</button>
                                            <button class="btn  button-size-save"style="margin-left: 20px;margin-right: 20px" id="save_form">Save</button>
                                            <button class="btn  button-size-sucess" type="submit" id="submit_form">Submit</button>
                                        </center>
                                    </div>
                                    <form:input type="hidden" path="status" value="" id="status_input"/>
                                </div>
                                </br>
                                </br>
                                </br>
                            </fieldset>
                        </form:form>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- End Content -->
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js" ></script>  
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/uikit/3.0.0-beta.20/js/uikit.min.js"></script>
<script src="https://unpkg.com/vue"></script>

<script>
                                                document.getElementById("uploadBtn").onchange = function () {
                                                    var split = this.value;
                                                    split = split.substr(12, );
                                                    document.getElementById("uploadFile").value = split;
                                                };
                                                function getCheckedSection()
                                                {
                                                    var sections = ["1", "2", "3", "4"];
                                                    for (var i = 0; i < sections.length; i++)
                                                    {
                                                        if (document.getElementById("pay" + sections[i]).checked)
                                                        {
                                                            document.getElementById("pay_value").value = parseInt(sections[i]);
                                                        }
                                                    }
                                                }

                                                function searchMachine() {
                                                    var machineInfo = document.getElementById("machineInfo");
                                                    var machineId = document.getElementById("machineId");
                                                    var machineCategory = document.getElementById("machineCategory");
                                                    var searchnum = document.getElementById("search_input").value;


                                                    $.ajax({url: "${pageContext.request.contextPath}/searchmachine/" + searchnum, success: function (result) {
                                                            if (result.machine_name !== null) {
                                                                machineInfo.value = result.machine_name + "/" + result.type + "/" + result.machine_model + "/" + result.car_regis_num;
                                                                machineId.value = result.id;
                                                                machineCategory.value = result.category;
                                                            } else {
                                                                alert("ไม่พบข้อมูล");
                                                            }
                                                        }
                                                    });
                                                }
                                                ;

                                                $('#tolist').submit(function () {
                                                    var check = confirm("แน่ใจที่จะยกเลิกหรือไม่");
                                                    return check;
                                                });

                                                $('#save_form').on("click", function () {
                                                    $('#status_input').val("1");
                                                });

                                                $('#submit_form').on("click", function () {
                                                    $('#status_input').val("6");
                                                });


</script>

<script>

    var idcount = 0;
    var countrow = 0;
    var indexValue = [];
    function createRow() {
        idcount += 1;
        countrow += 1;
        var brokenitem = document.getElementById("brokenitem").value;
        var type = document.getElementById("type").value;
        var count = document.getElementById("count").value;
        var price = document.getElementById("price").value;
        var brand = document.getElementById("brand").value;


        var sumCount = price * count;

        var row = document.createElement('tr'); // create row node
        row.setAttribute("id", "row" + idcount);
        row.setAttribute("class", "trow");
        var col = document.createElement('td'); // create column node
        var col2 = document.createElement('td'); // create second column node
        var col3 = document.createElement('td');
        var col4 = document.createElement('td');
        var col5 = document.createElement('td');
        var col6 = document.createElement('td');
        col6.setAttribute("id", "colPrice" + idcount);
        col6.setAttribute("value", "0");
        var col7 = document.createElement('td');
        var col8 = document.createElement('td');
        col8.setAttribute("style", "border:0;width:50px");
        var col9 = document.createElement('td');
        col9.setAttribute("style", "border:0;width:50px");

        var broken_input = document.createElement("input");
        broken_input.setAttribute("type", "text");
        broken_input.setAttribute("class", "no-border tran-bg");
        broken_input.setAttribute("id", "bk_input" + idcount);
        broken_input.setAttribute("disabled", "true");


        var type_input = document.createElement("input");
        type_input.setAttribute("type", "text");
        type_input.setAttribute("class", "no-border tran-bg");
        type_input.setAttribute("id", "type_input" + idcount);
        type_input.setAttribute("disabled", "true");

        var qty_input = document.createElement("input");
        qty_input.setAttribute("type", "text");
        qty_input.setAttribute("class", "no-border tran-bg");
        qty_input.setAttribute("id", "qty_input" + idcount);
        qty_input.setAttribute("disabled", "true");

        var price_input = document.createElement("input");
        price_input.setAttribute("type", "text");
        price_input.setAttribute("class", "no-border tran-bg");
        price_input.setAttribute("id", "price_input" + idcount);
        price_input.setAttribute("disabled", "true");

        var note_input = document.createElement("input");
        note_input.setAttribute("type", "text");
        note_input.setAttribute("class", "no-border tran-bg");
        note_input.setAttribute("id", "note_input" + idcount);
        note_input.setAttribute("disabled", "true");

        var editbtn = document.createElement("i");
        editbtn.setAttribute("type", "button");
        editbtn.setAttribute("class", "fa fa-edit edit-btn ");
        editbtn.setAttribute("style", "font-size:30px;");
        editbtn.setAttribute("id", "edit-btn" + idcount);
        editbtn.setAttribute("onclick", "editRow(this)");

        var deletebtn = document.createElement("input");
        deletebtn.setAttribute("type", "button");
        deletebtn.setAttribute("class", "btn btn-circle-plus");
        deletebtn.setAttribute("value", "-");
        deletebtn.setAttribute("id", "del-btn" + idcount);
        deletebtn.setAttribute("onclick", "deleteRow(this)");

        var center = document.createElement('center');

        row.appendChild(col); // append first column to row
        row.appendChild(col2); // append second column to row
        row.appendChild(col3);
        row.appendChild(col4);
        row.appendChild(col5);
        row.appendChild(col6);
        row.appendChild(col7);
        row.appendChild(col8);
        row.appendChild(col9);

        col.appendChild(center);
        center.innerHTML = countrow; // put data in first column

        col2.appendChild(broken_input);
        broken_input.value = brokenitem;

        col3.appendChild(type_input);
        type_input.value = type;

        col4.appendChild(qty_input);
        qty_input.value = count;

        col5.appendChild(price_input);
        price_input.value = price;// put data in second column

        col6.innerHTML = sumCount;
        col6.value = sumCount;

        col7.appendChild(note_input);
        note_input.value = brand;

        col8.appendChild(editbtn);

        col9.appendChild(deletebtn);

        var table = document.getElementById("tableToModify"); // find table to append to
        table.appendChild(row); // append row to table

        var input = document.createElement("input");
        input.setAttribute("type", "hidden");
        input.setAttribute("name", "itemslist");
        input.setAttribute("id", "itemslist" + idcount);
        input.setAttribute("value", [idcount, brokenitem, type, count, price, sumCount, brand, "0"]);
        table.appendChild(input);
        var totalPrice = document.getElementById("totalPrice");
        totalPrice.value = parseInt(totalPrice.value) + sumCount;
        document.getElementById("totalPrice2").value = totalPrice.value;
        var arrayString = [input.value];
        indexValue.push(arrayString);

        $("#edit-btn" + idcount).hide();
        $("#del-btn" + idcount).hide();

    }

    function deleteRow(r) {

        var i = r.parentNode.parentNode.rowIndex;
        var split = r.id;
        split = split.substr(7, );
        var colPrice = document.getElementById("colPrice" + split).value;
        var decreasePrice = document.getElementById("totalPrice");
        decreasePrice.value = parseInt(decreasePrice.value) - colPrice;
        document.getElementById("totalPrice2").value = parseInt(decreasePrice.value);

        document.getElementById("mytable").deleteRow(i);
        countrow -= 1;
        var i = indexValue.indexOf(document.getElementById("itemslist" + split).value);
        indexValue.splice(i, 1);

        var renum = 1;
        $("tr td center").each(function () {
            $(this).text(renum);
            renum++;
        });
        $('#itemslist' + split).remove();
    }

    function editRow(r) {

        var i = r.parentNode.parentNode.rowIndex;
        var split = r.id;
        split = split.substr(8, );

        var edit_btn = document.getElementById("edit-btn" + split);
        edit_btn.removeAttribute("class");
        edit_btn.removeAttribute("onclick");
        edit_btn.setAttribute("class", "fa fa-floppy-o edit-btn");
        edit_btn.setAttribute("onclick", "saveRow(this)");


        var broken_input = document.getElementById("bk_input" + split);
        broken_input.removeAttribute("disabled");
        broken_input.removeAttribute("class");

        var type_input = document.getElementById("type_input" + split);
        type_input.removeAttribute("disabled");
        type_input.removeAttribute("class");

        var qty_input = document.getElementById("qty_input" + split);
        qty_input.removeAttribute("disabled");
        qty_input.removeAttribute("class");

        var price_input = document.getElementById("price_input" + split);
        price_input.removeAttribute("disabled");
        price_input.removeAttribute("class");

        var note_input = document.getElementById("note_input" + split);
        note_input.removeAttribute("disabled");
        note_input.removeAttribute("class");

    }

    function saveRow(r) {

        var i = r.parentNode.parentNode.rowIndex;
        var split = r.id;
        split = split.substr(8, );

        var edit_btn = document.getElementById("edit-btn" + split);
        edit_btn.removeAttribute("class");
        edit_btn.removeAttribute("onclick");
        edit_btn.setAttribute("class", "fa fa-edit edit-btn");
        edit_btn.setAttribute("onclick", "editRow(this)");


        var broken_input = document.getElementById("bk_input" + split);
        broken_input.setAttribute("disabled", "true");
        broken_input.setAttribute("class", "no-border tran-bg");

        var type_input = document.getElementById("type_input" + split);
        type_input.setAttribute("disabled", "true");
        type_input.setAttribute("class", "no-border tran-bg");

        var qty_input = document.getElementById("qty_input" + split);
        qty_input.setAttribute("disabled", "true");
        qty_input.setAttribute("class", "no-border tran-bg");

        var price_input = document.getElementById("price_input" + split);
        price_input.setAttribute("disabled", "true");
        price_input.setAttribute("class", "no-border tran-bg");

        var note_input = document.getElementById("note_input" + split);
        note_input.setAttribute("disabled", "true");
        note_input.setAttribute("class", "no-border tran-bg");

        var sumCount = qty_input.value * price_input.value;
        var colPrice = document.getElementById("colPrice" + split);
        var sumPrice;
        var totalPrice = document.getElementById("totalPrice");

        if (colPrice.value > sumCount) {
            sumPrice = colPrice.value - sumCount;
            totalPrice.value = parseInt(totalPrice.value) - sumPrice;
            alert("Decrease " + sumPrice);
        } else if (colPrice.value < sumCount) {
            sumPrice = sumCount - colPrice.value;
            totalPrice.value = parseInt(totalPrice.value) + sumPrice;
            alert("Increase " + sumPrice);
        }


        $('#itemslist' + split).remove();
        var table = document.getElementById("tableToModify");
        var input = document.createElement("input");
        input.setAttribute("type", "hidden");
        input.setAttribute("name", "itemslist");
        input.setAttribute("id", "itemslist" + split);
        input.setAttribute("value", [split, broken_input.value, type_input.value, qty_input.value, price_input.value, sumCount, note_input.value]);
        table.appendChild(input);

        colPrice.value = sumCount;
        colPrice.innerHTML = colPrice.value;
        document.getElementById("totalPrice2").value = totalPrice.value;

    }




</script>
<script>
    //  show / hide edit and delete button on table
    $(document).ready(function () {
        $('#mytable').mouseover(function () {
            $('.trow').hover(function () {
                var rowId = this.id;
                var split = rowId;
                split = split.substr(3, );
                $("#edit-btn" + split).show();
                $("#del-btn" + split).show();
            }, function () {
                var rowId = this.id;
                var split = rowId;
                split = split.substr(3, );
                $("#edit-btn" + split).hide();
                $("#del-btn" + split).hide();
            });
        });

        $('.datetime').change(function () {
            var oneDay = 24 * 60 * 60 * 1000;
            var selectDate = document.getElementById("datepick");
            var split = selectDate.value.substr(0, 10);
            var firstDate = new Date(split);
            var secondDate = new Date();
            var diffDays = Math.round(Math.abs((firstDate.getTime() - secondDate.getTime()) / (oneDay)));
            document.getElementById("dayleft").value = parseInt(diffDays);
            document.getElementById("dayleft2").value = parseInt(diffDays);

        });
    });
</script>

<jsp:include page="../footer.jsp" />
