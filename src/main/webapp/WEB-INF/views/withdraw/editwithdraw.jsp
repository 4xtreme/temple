<%-- 
    Document   : editwithdraw
    Created on : Dec 1, 2017, 11:01:19 AM
    Author     : pop
--%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:include page="../header.jsp" />
<jsp:include page="../navigation.jsp" />
<!-- Page Content -->
<div id="page-wrapper">
    <div class="container-fluid">
        <!-- start Content -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header"><spring:message code="header.withdraw"/></h1>
            </div>
        </div>
        <div class="row" id="eiei">
            <div class="col-lg-12">
                <div class="tab-content">
                    <div class="tab-pane fade active in">
                        <form id="history" action="oldwithdrawlist" method="POST" target="_new">
                            <input type="hidden" value="${withdraw.id}" name="withdraw_id">
                        </form>
                        <form:form action="editedwithdraw" modelAttribute="withdraw"   method="POST" enctype="multipart/form-data">
                            <fieldset>
                                <form:input type="hidden" path="id" value="${withdraw.id}"/>
                                <form:input type="hidden" path="create_date" value="${withdraw.create_date}"/>
                                <div class="row">
                                    <div class="col-lg-8" >
                                        <div class="dl-horizontal">
                                            <div class="form-group">
                                                <span class="label_title">ผู้ขอเบิก</span>
                                                &emsp;
                                                <form:select path="withdrawal_id" name="withdrawal_id" class="form-control width_20">
                                                    <c:forEach var="staff" items="${staff}" varStatus="loop">
                                                        <c:choose>
                                                            <c:when test="${staff.id eq withdraw.withdrawal_id}">
                                                                <option value="${staff.id}" selected="">${staff.fristname}</option>
                                                            </c:when>
                                                            <c:otherwise>
                                                                <option value="${staff.id}">${staff.fristname}</option>
                                                            </c:otherwise>
                                                        </c:choose>
                                                    </c:forEach>
                                                </form:select><form:errors path="withdrawal_id" /> 
                                                &emsp;&emsp;&emsp;&emsp;&emsp;
                                                <span class="label_title">คนขับ/ควบคุมเครื่อง</span>
                                                &emsp;
                                                <form:select path="driver_id" class="form-control width_20">
                                                    <c:forEach var="staff" items="${staff}" varStatus="loop">
                                                        <c:choose>
                                                            <c:when test="${staff.id eq withdraw.driver_id}">
                                                                <option value="${staff.id}" selected="">${staff.fristname}</option>
                                                            </c:when>
                                                            <c:otherwise>
                                                                <option value="${staff.id}">${staff.fristname}</option>
                                                            </c:otherwise>
                                                        </c:choose>
                                                    </c:forEach>
                                                </form:select><form:errors path="driver_id" /> 
                                            </div>
                                            <div class="form-group">
                                                <span class="label_title">จำนวนเงิน</span>
                                                <form:input type="text" path="amount" class="form-control width_20" /><form:errors path="amount"/>
                                                <span>บาท</span>
                                                &emsp;&emsp;&nbsp;&nbsp;
                                                <span class="label_title">ใช้ในโครงการ</span>
                                                <form:select path="project_id" class="form-control width_25">
                                                    <c:forEach var="project" items="${project}" varStatus="loop">
                                                        <c:choose>
                                                            <c:when test="${project.project_id eq withdraw.project_id}">
                                                                <option value="${project.project_id}" selected="">${project.project_name}</option>
                                                            </c:when>
                                                            <c:otherwise>
                                                                <option value="${project.project_id}">${project.project_name}</option>
                                                            </c:otherwise>
                                                        </c:choose>
                                                    </c:forEach>
                                                </form:select><form:errors path="project_id" /> 
                                            </div>

                                            <div class="form-group">
                                                <div class="col-lg-4">
                                                    <dt style="text-align: left; width: 100px"><span>วิธีจ่ายเงิน</span></dt> 
                                                    <dd style="text-align: left;margin-left: 100px" >
                                                        <label style="display: block">
                                                            <input  id="pay1" type="radio" name="radio" value="1" onclick="getCheckedSection()"/><span class="label_title"> เงินสด </span>
                                                        </label >
                                                        <label style="display: block">
                                                            <input  id="pay2" type="radio" name="radio" value="2" onclick="getCheckedSection()"/><span class="label_title"> เชื่อ/เครดิต </span>
                                                        </label>
                                                        <label style="display: block">
                                                            <input  id="pay3" type="radio" name="radio" value="3" onclick="getCheckedSection()"/><span class="label_title"> โอนธนาคาร </span>
                                                        </label>
                                                        <label style="display: block">
                                                            <input id="pay4" type="radio" name="radio" value="4" onclick="getCheckedSection()"/><span class="label_title"> เช็ค </span>
                                                        </label>
                                                        <form:input hidden="true" path="pay_type" id="pay_value" value="${withdraw.pay_type}"/>
                                                    </dd>
                                                </div>

                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                <span>กำหนดชำระ</span>
                                                <i class="fa fa-calendar " style="font-size: 20px"></i>
                                                <form:input path="pay_date" type="text" class="datetime"  id="datepick" value="${withdraw.pay_date}"/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<form:errors path="pay_date"/></br>
                                                <span class="margin_left10" >อีก 
                                                    <input  disabled="true" class="no-border tran-bg" id="dayleft" value="${withdraw.pay_date_count}" style="width: 20px;text-align: center"/> 
                                                    <form:input path="pay_date_count" hidden="true" class="no-border tran-bg" id="dayleft2" value="0" style="width: 20px;text-align: center"/> 
                                                    วัน</span>
                                            </div></br></br></br></br>

                                            <div class="form-group">
                                                <span>จ่ายให้กับ</span>
                                                <select path="" class="form-control width_25">
                                                    <option  value="" disabled=" " selected="">เลือกรายการ</option>
                                                    <option value="5">ร้าย XYZ อะไหล่ (2000)</option>
                                                </select><form:errors path=""/>
                                                <p class="margin_left10"></p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="dl-horizontal">
                                            <div class="form-group">
                                                <span> วันที่ </span>
                                                <input type="text" path="" class="no-border tran-bg " disabled="" value="${date}"/>
                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            </div>
                                            <div class="form-group">
                                                <span>หมายเลขใบเบิก : </span>
                                                <input type="text" path="" class="no-border tran-bg "disabled="" value="${withdraw.id}"/>
                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            </div>
                                            <div class="form-group">
                                                <span>ผู้กรอกข้อมูล : </span>
                                                <input type="text" path="" class="no-border tran-bg" disabled="" value="${user.firstname}"/>
                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            </div>
                                            <div class="form-group">
                                                <span>สถานะ : </span>
                                                <input type="text" path="" class="no-border tran-bg " value="${withdraw.status}" disabled="" id="status"/>
                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            </div>
                                            <div class="form-group">
                                                <span>ปรับปรุงหลังสุดเมื่อ  : </span>
                                                <input type="text" path="" class="no-border tran-bg width_50"disabled="" value="${withdraw.update_date} ${user.firstname}"/>
                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <span>สำหรับเครื่องจักร หมายเลขทะเบียน/รหัส</span>
                                            <input type="text" name="searchnum" form="search_machine" id="search_input" value="${machine.car_regis_num}" />
                                            <i type="button" onclick="searchMachine()"   class="fa fa-search" style="font-size:25px; color: skyblue;"></i>
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        </div>
                                        <div class="form-group">
                                            <form:input path="machine_id" id="machineId" hidden="true" value="${machine_id}"/>
                                            <input type="text" id="machineInfo" class="margin_left10 width_15" disabled="disable"value="${machine.machine_name}/${machine.type}/${machine.machine_model}/${machine.car_regis_num}"/>
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <span>หมวดเครื่องจัก : </span> 
                                            <input type="text" id="machineCategory" class="no-border tran-bg " disabled="disable"value="${machine.category}" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <span>อายุใช้งาน : </span>
                                            <input type="text" path="" class="no-border tran-bg" disabled="disable"value="3/4/20"/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <span>ที่ตั้งปัจจุบัน : </span>
                                            <input type="text" path="" class="no-border tran-bg" disabled="disable"value="โครงการ XYZ"/>
                                        </div>
                                    </div>
                                </div>
                                </br>
                                </br>
                                </br>
                                <div class="row">
                                    <div class="col-lg-12" style="border-top: solid; border-width: 1px; ">
                                        </br></br>
                                        <div class="dl-horizontal">
                                            <input type="button" onclick="createRow()" id="add-btn" type="botton" value="เพิ่มรายการในตาราง" class="btn" />
                                            </br>
                                            </br>
                                            <div class="form-group ">
                                                <span>อุปกรณ์ที่เสียหาย/รายการ</span>
                                                <select id='brokenitem'  class="form-control width_25"> 
                                                    <option  value="" disabled=" " selected="">เลือกรายการ</option>
                                                    <option  value="ยาง"  >ยาง</option>
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <span>ประเภทการเสีย/ซ่อม</span>
                                                <select id="type" class="form-control width_10"> 
                                                    <option  value="" disabled=" " selected="">เลือกรายการ</option>
                                                    <option  value="ซ่อม"  >ซ่อม</option>
                                                </select>
                                                <span class="margin_left10">จำนวน</span>
                                                <input type="number" name="quantity" min="0"  value="0" id="count" class="form-control width_5">
                                                <span class="margin_left10">ราคาประเมินต่อหน่วย</span>
                                                <input type="text" path="" id="price" value="0" class="width_10"/>
                                                <span >บาท</span>
                                                <span class="margin_left10">ยี่ห้อ/หมายเหตุ</span>
                                                <input type="text" path="" id="brand" value="-"/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                </br>
                                <div class="row">
                                    <div class="col-lg-10">
                                        <div class="table-responsive" id="app1">
                                            <table class="table table-striped table-bordered table-hover del-line" id="mytable">
                                                <thead >
                                                    <tr >
                                                        <th class="text-center">ลำดับ</th>
                                                        <th class="text-center">อุปกรณที่เสีย</th>
                                                        <th class="text-center">ประเภทการเสียซ่อม</th>
                                                        <th class="text-center">จำนวน</th>
                                                        <th class="text-center">ราคา/หน่วย</th>
                                                        <th class="text-center">ราคารวม</th>
                                                        <th class="text-center">ยี่ห่อ/หมายเหตุ</th>
                                                        <th class="text-center" hidden="true" style="border: none"></th>
                                                        <th class="text-center"hidden="true" style="border: none"></th>
                                                    </tr>
                                                </thead>

                                                <tbody   id="tableToModify" >
                                                    <c:if test="${orderWithdrawlist != null}">  
                                                        <c:forEach var="orderWithdrawlist" items="${orderWithdrawlist}" varStatus="loop">
                                                            <tr id="row${orderWithdrawlist.index_number}" class="trow">
                                                                <td><center>${orderWithdrawlist.index_number}</center></td>
                                                        <td>
                                                            <input type="text" class="no-border tran-bg" id="bk_input${orderWithdrawlist.index_number}" disabled="true" value=" ${orderWithdrawlist.broken_item}"/>
                                                        </td>
                                                        <td>
                                                            <input type="text" class="no-border tran-bg" id="type_input${orderWithdrawlist.index_number}" disabled="true" value="${orderWithdrawlist.type}"/>
                                                        </td>
                                                        <td>
                                                            <input type="text" class="no-border tran-bg" id="qty_input${orderWithdrawlist.index_number}" disabled="true" value="${orderWithdrawlist.quantity}"/>
                                                        </td>
                                                        <td>
                                                            <input type="text" class="no-border tran-bg" id="price_input${orderWithdrawlist.index_number}" disabled="true" value="${orderWithdrawlist.amount}"/>
                                                        </td>
                                                        <td>
                                                            <input type="text" class="no-border tran-bg" id="colPrice${orderWithdrawlist.index_number}" disabled="true" value=" ${orderWithdrawlist.sum_price}"/>
                                                        </td>
                                                        <td>
                                                            <input type="text" class="no-border tran-bg" id="note_input${orderWithdrawlist.index_number}" disabled="true" value="${orderWithdrawlist.note}"/>
                                                        </td>
                                                        <td style="border: 0;width: 50px">
                                                            <i type="button" class="fa fa-edit edit-btn" style="font-size:30px" id="edit-btn${orderWithdrawlist.index_number}" onclick="editRow(this)"></i>
                                                        </td>
                                                        <td  style="border: 0;width: 50px">
                                                            <input type="button" class="btn btn-circle-plus"  id="del-btn${orderWithdrawlist.index_number}" onclick="deleteRow(this)" value="-" />
                                                        </td>
                                                        </tr>
                                                        <input type="hidden" name="itemslist" id="itemslist${orderWithdrawlist.index_number}" value="${orderWithdrawlist.id},${orderWithdrawlist.index_number},${orderWithdrawlist.broken_item},${orderWithdrawlist.type},${orderWithdrawlist.quantity},${orderWithdrawlist.amount},${orderWithdrawlist.sum_price},${orderWithdrawlist.note},${orderWithdrawlist.is_delete},old"/>
                                                    </c:forEach>
                                                </c:if>
                                                </tbody>
                                                <input name="itemslist" type="hidden" value="0,0,0,0,0,0,0">
                                            </table>
                                        </div>
                                        <div class="form-group">
                                            <div class="row margin_top10"> 
                                                <div class="col-xs-6 " style="margin-left: 15px">
                                                    <label class="fileContainer">
                                                        <i class="fa fa-link fa-flip-horizontal attach-file" aria-hidden="true"></i>
                                                        <input type="file" id="uploadBtn" path="withdraw_file"  name="file_withdraw" accept="application/pdf"  />
                                                        <input type="hidden" name="oldfile" value="${withdraw.withdraw_file}">
                                                    </label>
                                                    <span style="line-height: 60px">แนบไฟล์</span></br>
                                                    <a id="pdf_link" href="https://docs.google.com/viewerng/viewer?url=${withdraw.withdraw_file}" target="_blank" >
                                                        <input  id="uploadFile" class="form-control-sm no-border width_60 file_dir2" type="text" value="${withdraw.withdraw_file}"  disabled="disabled" />
                                                    </a>
                                                </div>
                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<form:errors path="withdraw_file" /><br>
                                                <div class="col-xs-5">
                                                    <strong>
                                                        <span>รวมทั้งสิ้น  </span>
                                                        <input id="totalPrice" type="text" path="" class="no-border tran-bg-right width_15" disabled="disable"value="${withdraw.total_price}"/>
                                                        <form:input hidden="true" id="totalPrice2" type="text" path="total_price" value="${withdraw.total_price}"/>
                                                        <span>  บาท  </span>
                                                    </strong>
                                                    </br>
                                                    </br>

                                                </div>		
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row" id="note_div">
                                    <input type="hidden" id="role" value="${login_user.role}"/>
                                    <div class="form-group" id="user_role">
                                        <center>
                                            <div class="form-group">
                                                <span>บันทึกผู้ขอเบิก&nbsp;</span>
                                                <form:textarea path="withdrawal_note" class="custom-textarea" value="${withdraw.withdrawal_note}"></form:textarea>
                                                </div>
                                            </center>
                                        </div>
                                        <div class="form-group" id="money_role">
                                            <center>
                                                <div class="form-group">
                                                    <div style="width: 120px;display: inline-block">
                                                        <span >บันทึกผู้ตรวจสอบ / พนักงานการเงิน</span>
                                                    </div>
                                                <form:textarea path="inspector_note" class="custom-textarea" value="${withdraw.inspector_note}"></form:textarea>
                                                </div>
                                            </center>
                                        </div>
                                        <div class="form-group" id="admin_role">
                                            <center>
                                                <div class="form-group">
                                                    <span>บันทึกผู้อนุมัติ &nbsp;</span>
                                                <form:textarea path="approvers_note" class="custom-textarea" value="${withdraw.approvers_note}"></form:textarea>
                                                </div>
                                            </center>
                                        </div>
                                    </div>
                                    </br>
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <center>
                                            <c:if test="${login_user.role == 'user'}">
                                                <button class="btn  button-size-cancel" id="cancel_form" >ยกเลิก</button>
                                                <button class="btn  button-size-save"style="margin-left: 20px;margin-right: 20px" id="save_form">บันทึก</button>
                                                <button class="btn  button-size-sucess" type="submit" id="submit_form">เบิก</button>
                                            </c:if>
                                            <c:if test="${login_user.role == 'money'}">
                                                <button class="btn  button-size-cancel" form="history" >ดูประวัติ</button>
                                                <button class="btn  button-size-save"style="margin-left: 20px;margin-right: 20px" id="notpass_form">ไม่ผ่าน</button>
                                                <button class="btn  button-size-sucess" type="submit" id="require_form">ขออนุมัติ</button>
                                            </c:if>
                                            <c:if test="${login_user.role == 'admin'}">
                                                <button class="btn  button-size-cancel" form="history" >ดูประวัติ</button>
                                                <button class="btn  button-size-save"style="margin-left: 20px;margin-right: 20px" id="notreqire_form">ไม่อนุมัติ</button>
                                                <button class="btn  button-size-sucess" type="submit" id="accept_form">อนุมัติ</button>
                                            </c:if>
                                        </center>
                                    </div>
                                    <form:input type="hidden" path="status" value="" id="status_input"/>
                                </div>
                                </br>
                                </br>
                                </br>
                            </fieldset>
                        </form:form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Content -->
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js" ></script>  
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/uikit/3.0.0-beta.20/js/uikit.min.js"></script>
<script src="https://unpkg.com/vue"></script>

<script>
                                                                $("#uploadBtn").on("change", function () {
                                                                    var split = this.value;
                                                                    split = split.substr(12, );
                                                                    $("#uploadFile").val(split);
                                                                    $('#pdf_link').attr("href", "#");
                                                                    $('#pdf_link').attr("target", "_self");
                                                                });

                                                                $('#pdf_link').on("hover", function () {
                                                                    $('#pdf_link').attr("style", "color: blue");
                                                                });

                                                                $('#cancel_form').on("click", function () {
                                                                    var check = confirm("แน่ใจที่จะยกเลิกหรือไม่");
                                                                    $('#status_input').val("4");
                                                                    return check;
                                                                });

                                                                $('#save_form').on("click", function () {
                                                                    $('#status_input').val("1");
                                                                });

                                                                $('#submit_form').on("click", function () {
                                                                    $('#status_input').val("6");
                                                                });

                                                                $('#notpass_form').on("click", function () {
                                                                    $('#status_input').val("1");
                                                                });

                                                                $('#require_form').on("click", function () {
                                                                    $('#status_input').val("2");
                                                                });

                                                                $('#notrequire_form').on("click", function () {
                                                                    $('#status_input').val("3");
                                                                });

                                                                $('#accept_form').on("click", function () {
                                                                    $('#status_input').val("5");
                                                                });

</script>

<script>


    var idcount = 0;
    var countrow = 0;
    $("tr td center").each(function () {
        idcount++;
        countrow++;
    });


    var indexValue = [];
    function createRow() {

        idcount += 1;
        countrow += 1;
        var brokenitem = document.getElementById("brokenitem").value;
        var type = document.getElementById("type").value;
        var count = document.getElementById("count").value;
        var price = document.getElementById("price").value;
        var brand = document.getElementById("brand").value;


        var sumCount = price * count;

        var row = document.createElement('tr'); // create row node
        row.setAttribute("id", "row" + idcount);
        row.setAttribute("class", "trow");
        var col = document.createElement('td'); // create column node
        var col2 = document.createElement('td'); // create second column node
        var col3 = document.createElement('td');
        var col4 = document.createElement('td');
        var col5 = document.createElement('td');
        var col6 = document.createElement('td');
        col6.setAttribute("id", "colPrice" + idcount);
        col6.setAttribute("value", "0");
        var col7 = document.createElement('td');
        var col8 = document.createElement('td');
        col8.setAttribute("style", "border:0;width:50px");
        var col9 = document.createElement('td');
        col9.setAttribute("style", "border:0;width:50px");

        var broken_input = document.createElement("input");
        broken_input.setAttribute("type", "text");
        broken_input.setAttribute("class", "no-border tran-bg");
        broken_input.setAttribute("id", "bk_input" + idcount);
        broken_input.setAttribute("disabled", "true");


        var type_input = document.createElement("input");
        type_input.setAttribute("type", "text");
        type_input.setAttribute("class", "no-border tran-bg");
        type_input.setAttribute("id", "type_input" + idcount);
        type_input.setAttribute("disabled", "true");

        var qty_input = document.createElement("input");
        qty_input.setAttribute("type", "text");
        qty_input.setAttribute("class", "no-border tran-bg");
        qty_input.setAttribute("id", "qty_input" + idcount);
        qty_input.setAttribute("disabled", "true");

        var price_input = document.createElement("input");
        price_input.setAttribute("type", "text");
        price_input.setAttribute("class", "no-border tran-bg");
        price_input.setAttribute("id", "price_input" + idcount);
        price_input.setAttribute("disabled", "true");

        var note_input = document.createElement("input");
        note_input.setAttribute("type", "text");
        note_input.setAttribute("class", "no-border tran-bg");
        note_input.setAttribute("id", "note_input" + idcount);
        note_input.setAttribute("disabled", "true");

        var editbtn = document.createElement("i");
        editbtn.setAttribute("type", "button");
        editbtn.setAttribute("class", "fa fa-edit edit-btn ");
        editbtn.setAttribute("style", "font-size:30px;");
        editbtn.setAttribute("id", "edit-btn" + idcount);
        editbtn.setAttribute("onclick", "editRow(this)");

        var deletebtn = document.createElement("input");
        deletebtn.setAttribute("type", "button");
        deletebtn.setAttribute("class", "btn btn-circle-plus");
        deletebtn.setAttribute("value", "-");
        deletebtn.setAttribute("id", "del-btn" + idcount);
        deletebtn.setAttribute("onclick", "deleteRow(this)");

        var center = document.createElement('center');

        row.appendChild(col); // append first column to row
        row.appendChild(col2); // append second column to row
        row.appendChild(col3);
        row.appendChild(col4);
        row.appendChild(col5);
        row.appendChild(col6);
        row.appendChild(col7);
        row.appendChild(col8);
        row.appendChild(col9);

        col.appendChild(center);
        center.innerHTML = countrow; // put data in first column

        col2.appendChild(broken_input);// put data in second column
        broken_input.value = brokenitem;

        col3.appendChild(type_input);
        type_input.value = type;

        col4.appendChild(qty_input);
        qty_input.value = count;

        col5.appendChild(price_input);
        price_input.value = price;

        col6.innerHTML = sumCount;
        col6.value = sumCount;

        col7.appendChild(note_input);
        note_input.value = brand;

        col8.appendChild(editbtn);

        col9.appendChild(deletebtn);

        var table = document.getElementById("tableToModify"); // find table to append to
        table.appendChild(row); // append row to table

        var renum = 1;
        $("tr td center").each(function () {
            $(this).text(renum);
            renum++;
        });

        var input = document.createElement("input");
        input.setAttribute("type", "hidden");
        input.setAttribute("name", "itemslist");
        input.setAttribute("id", "itemslist" + idcount);
        input.setAttribute("value", ["0", idcount, brokenitem, type, count, price, sumCount, brand, "0", "new"]);
        table.appendChild(input);
        var totalPrice = document.getElementById("totalPrice");
        totalPrice.value = parseInt(totalPrice.value) + sumCount;
        document.getElementById("totalPrice2").value = totalPrice.value;
        var arrayString = [input.value];
        indexValue.push(arrayString);



        $("#edit-btn" + idcount).hide();
        $("#del-btn" + idcount).hide();

    }

    function deleteRow(r) {

        var i = r.parentNode.parentNode.rowIndex;
        var split = r.id;
        split = split.substr(7, );
        var colPrice = document.getElementById("colPrice" + split).value;
        var decreasePrice = document.getElementById("totalPrice");
        decreasePrice.value = parseInt(decreasePrice.value) - colPrice;
        document.getElementById("totalPrice2").value = parseInt(decreasePrice.value);

        document.getElementById("mytable").deleteRow(i);
        countrow -= 1;
        var i = indexValue.indexOf(document.getElementById("itemslist" + split).value);
        indexValue.splice(i, 1);

        var renum = 1;
        $("tr td center").each(function () {
            $(this).text(renum);
            renum++;
        });

        var olditemlist = $('#itemslist' + split).val().split(",");
        var status = olditemlist[9];
        if (status === "old") {
            $('#itemslist' + split).val([olditemlist[0], olditemlist[1], olditemlist[2], olditemlist[3], olditemlist[4], olditemlist[5], olditemlist[6], olditemlist[7], "1", status]);
        } else if (status === "new") {
            $('#itemslist' + split).remove();
        }
    }

    function editRow(r) {

        var i = r.parentNode.parentNode.rowIndex;
        var split = r.id;
        split = split.substr(8, );

        var edit_btn = document.getElementById("edit-btn" + split);
        edit_btn.removeAttribute("class");
        edit_btn.removeAttribute("onclick");
        edit_btn.setAttribute("class", "fa fa-floppy-o edit-btn");
        edit_btn.setAttribute("onclick", "saveRow(this)");


        var broken_input = document.getElementById("bk_input" + split);
        broken_input.removeAttribute("disabled");
        broken_input.removeAttribute("class");

        var type_input = document.getElementById("type_input" + split);
        type_input.removeAttribute("disabled");
        type_input.removeAttribute("class");

        var qty_input = document.getElementById("qty_input" + split);
        qty_input.removeAttribute("disabled");
        qty_input.removeAttribute("class");

        var price_input = document.getElementById("price_input" + split);
        price_input.removeAttribute("disabled");
        price_input.removeAttribute("class");

        var note_input = document.getElementById("note_input" + split);
        note_input.removeAttribute("disabled");
        note_input.removeAttribute("class");

    }

    function saveRow(r) {

        var i = r.parentNode.parentNode.rowIndex;
        var split = r.id;
        split = split.substr(8, );

        var edit_btn = document.getElementById("edit-btn" + split);
        edit_btn.removeAttribute("class");
        edit_btn.removeAttribute("onclick");
        edit_btn.setAttribute("class", "fa fa-edit edit-btn");
        edit_btn.setAttribute("onclick", "editRow(this)");


        var broken_input = document.getElementById("bk_input" + split);
        broken_input.setAttribute("disabled", "true");
        broken_input.setAttribute("class", "no-border tran-bg");

        var type_input = document.getElementById("type_input" + split);
        type_input.setAttribute("disabled", "true");
        type_input.setAttribute("class", "no-border tran-bg");

        var qty_input = document.getElementById("qty_input" + split);
        qty_input.setAttribute("disabled", "true");
        qty_input.setAttribute("class", "no-border tran-bg");

        var price_input = document.getElementById("price_input" + split);
        price_input.setAttribute("disabled", "true");
        price_input.setAttribute("class", "no-border tran-bg");

        var note_input = document.getElementById("note_input" + split);
        note_input.setAttribute("disabled", "true");
        note_input.setAttribute("class", "no-border tran-bg");

        var sumCount = qty_input.value * price_input.value;
        var colPrice = document.getElementById("colPrice" + split);
        var sumPrice;
        var totalPrice = document.getElementById("totalPrice");


        if (colPrice.value > sumCount) {
            sumPrice = colPrice.value - sumCount;
            totalPrice.value = parseInt(totalPrice.value) - sumPrice;
        } else if (colPrice.value < sumCount) {
            sumPrice = sumCount - colPrice.value;
            totalPrice.value = parseInt(totalPrice.value) + sumPrice;
        }
        var olditemlist = $('#itemslist' + split).val().split(",");
        var status = olditemlist[9];
        $('#itemslist' + split).remove();
        var table = document.getElementById("tableToModify");
        var input = document.createElement("input");
        input.setAttribute("type", "hidden");
        input.setAttribute("name", "itemslist");
        input.setAttribute("id", "itemslist" + split);
        input.setAttribute("value", [olditemlist[0], split, broken_input.value, type_input.value, qty_input.value, price_input.value, sumCount, note_input.value, "0", status]);
        table.appendChild(input);

        colPrice.value = sumCount;
        colPrice.innerHTML = colPrice.value;
        document.getElementById("totalPrice2").value = totalPrice.value;

    }

    function getCheckedSection()
    {
        var sections = ["1", "2", "3", "4"];
        for (var i = 0; i < sections.length; i++)
        {
            if (document.getElementById("pay" + sections[i]).checked)
            {
                document.getElementById("pay_value").value = parseInt(sections[i]);
            }
        }
    }

    function searchMachine() {
        var machineInfo = document.getElementById("machineInfo");
        var machineId = document.getElementById("machineId");
        var machineCategory = document.getElementById("machineCategory");
        var searchnum = document.getElementById("search_input").value;


        $.ajax({url: "${pageContext.request.contextPath}/searchmachine/" + searchnum, success: function (result) {
                if (result.machine_name !== null) {
                    machineInfo.value = result.machine_name + "/" + result.type + "/" + result.machine_model + "/" + result.car_regis_num;
                    machineId.value = result.id;
                    machineCategory.value = result.category;
                } else {
                    alert("ไม่พบข้อมูล");
                }
            }
        });
    }


    function openPDF(urlToPdfFile) {
        window.open(urlToPdfFile, 'pdf');
    }


</script>
<script>
    //  show / hide edit and delete button on table
    $(document).ready(function () {
        $('#mytable').mouseover(function () {
            $('.trow').hover(function () {
                var rowId = this.id;
                var split = rowId;
                split = split.substr(3, );
                $("#edit-btn" + split).show();
                $("#del-btn" + split).show();
            }, function () {
                var rowId = this.id;
                var split = rowId;
                split = split.substr(3, );
                $("#edit-btn" + split).hide();
                $("#del-btn" + split).hide();
            });
        });

        $('.datetime').change(function () {
            var oneDay = 24 * 60 * 60 * 1000;
            var selectDate = document.getElementById("datepick");
            var split = selectDate.value.substr(0, 10);
            var firstDate = new Date(split);
            var secondDate = new Date();
            var diffDays = Math.round(Math.abs((firstDate.getTime() - secondDate.getTime()) / (oneDay)));
            document.getElementById("dayleft").value = parseInt(diffDays);
            document.getElementById("dayleft2").value = parseInt(diffDays);

        });

        $('.datetime').ready(function () {
            var oneDay = 24 * 60 * 60 * 1000;
            var selectDate = document.getElementById("datepick");
            var split = selectDate.value.substr(0, 10);
            var firstDate = new Date(split);
            var secondDate = new Date();
            var diffDays = Math.round(Math.abs((firstDate.getTime() - secondDate.getTime()) / (oneDay)));
            document.getElementById("dayleft").value = parseInt(diffDays);
            document.getElementById("dayleft2").value = parseInt(diffDays);
        });

        var renum = 1;
        $("tr td center").each(function () {
            $("#edit-btn" + renum).hide();
            $("#del-btn" + renum).hide();
            $(this).text(renum);
            renum++;
        });

        $('#pay_type').ready(function () {
            var pay_value = $('#pay_value');
            switch (pay_value.val()) {
                case '1':
                    $('#pay1').attr("checked", "true");
                    break;
                case '2':
                    $('#pay2').attr("checked", "true");
                    break;
                case '3':
                    $('#pay3').attr("checked", "true");
                    break;
                case '4':
                    $('#pay4').attr("checked", "true");
                    break;
            }
        });

        $('#status').ready(function () {

            switch ($('#status').val()) {
                case '1':
                    $('#status').val("ร่าง");
                    break;
                case '2':
                    $('#status').val("รออนุมัติ");
                    break;
                case '3':
                    $('#status').val(" ไม่อนุมัติ");
                    break;
                case '4':
                    $('#status').val("ยกเลิก");
                    break;
                case '5':
                    $('#status').val("จ่ายเงินแล้ว");
                    break;
                case '6':
                    $('#status').val("ขอเบิก");
                    break;

            }
        });

        $('#uploadFile').ready(function () {
            var split = $('#uploadFile').val();
            split = split.substr(48, );
            $('#uploadFile').val(split);
        });

        $('#note_div').ready(function () {
           if($('#role').val() === 'user'){
               $('#money_role').hide();
               $('#admin_role').hide();
           }else if($('#role').val() === 'money'){
               $('#user_role').hide();
               $('#admin_role').hide();
           }else if($('#role').val() === 'admin') {
               $('#user_role').hide();
               $('#money_role').hide();
           }
        });

    });
</script>

<jsp:include page="../footer.jsp" />
