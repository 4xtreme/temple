<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html;charset=UTF-8" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<jsp:include page="../header.jsp" />
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>

        <meta charset="utf-8">
            <meta name="viewport" content="width=device-width, initial-scale=1"/>
            <title> Login </title>
            <link href="${pageContext.request.contextPath}/css/bootstrap.css" rel="stylesheet">
                <link href="${pageContext.request.contextPath}/css/regis-form.css" rel="stylesheet" />



                </head>

                <style>

                    /* Always set the map height explicitly to define the size of the div
                     * element that contains the map. */
                    #map {
                        height: 1200px;
                    }

                </style>


                <body>

                    <div class="col-xs-8">
                        <div class="row">

                            <div id="map"></div>
                            <BR></BR><BR>
                        </div>


                    </div><!-- /col-xs-8 -->
                    <div class="col-xs-4">


                        <div class="row">
                            <div class="col-lg-12">
                                <center><h1 class="page-header">ศูนย์ปฏิบัติธรรม</h1></center>
                            </div>
                        </div>

                        <center> 
                            <div class="col-lg-7" style="float: none">
                                <!--                                <form class="form-signup" id="mainform" action="login" method="post" >
                                <c:if test="${status != null}">
                                    <center>
                                        <p style="color: red;">ชื่อผู้ใช้หรือรหัสผ่านไม่ถูกต้อง  !!!</p>   
                                    </center>                   
                                </c:if>
                                <c:if test="${status_del == 'delete'}">
                                    <center>
                                        <p style="color: red;">ผู้ใช้นี้ไม่สามารถเข้าสู่ระบบได้ กรุณาติดต่อเจ้าหน้าที่  </p>   
                                    </center>                   
                                </c:if>
                                <span id="reauth-email" class="reauth-email"></span>
                                <input id="inputEmail" name="email" class="form-control" placeholder="อีเมล์" required="" autofocus="" type="email"/>
                                <input id="inputPassword" name="password" class="form-control" placeholder="รหัสผ่าน" required="" type="password"/>
                                <select id="temple_id" name="temple_id" id="select_temple" class="form-control">
                                    <option  value="" disabled=" " selected="">เลือกรายการวัด</option>
                                <c:forEach var="temple" items="${temple}" varStatus="loop">
                                    <option  value="${temple.id}">${temple.name}</option>
                                </c:forEach>
                            </select><form:errors path="temple_id" /><br>
                                <button class="btn btn-lg btn-primary btn-block btn-signup"  type="button" onclick="submitForm()" style="width:40%">เข้าสู่ระบบ</button>
                        </form>-->

                                <form class="form-signup" id="mainform" action="loginstaff" method="post">
                                    <span id="reauth-email" class="reauth-email"></span>
                                    <input id="inputEmail" name="email" class="form-control" placeholder="อีเมล์" required="" autofocus="" type="email"/>
                                    <input id="inputPassword" name="password" class="form-control" placeholder="รหัสผ่าน" required="" type="password"/>
                                    <select id="temple_id" name="temple_id" id="select_temple" class="form-control">
                                        <option  value="" disabled=" " selected="">เลือกรายการวัด</option>
                                        <c:forEach var="temple" items="${temple}" varStatus="loop">
                                            <option  value="${temple.id}">${temple.name}</option>
                                        </c:forEach>
                                    </select><form:errors path="temple_id" /><br/>
                                    <button class="btn btn-lg btn-primary btn-block btn-signup" type="button" onclick="submitForm()">เข้าสู่ระบบ</button>
                                </form>       
                                <c:if test="${status != null}">
                                    <center>
                                        <p style="color: red;">ชื่อผู้ใช้หรือรหัสผ่านไม่ถูกต้อง !!!</p>   
                                    </center>                   
                                </c:if>

                                <!--<a href="regcreate">ลงทะเบียน</a> |-->
                                <!--<a href="forgotpassword">ลืมรหัสผ่าน</a><br/>-->
                                <!--<a href="loginstaff">สำหรับเจ้าหน้าที่</a>-->

                            </div>
                        </center>   

                    </div><!-- /col-xs-6 -->
                    <div class="col-xs-4">



                    </div>
                    <div class="col-xs-4">
                        <div class="row">
                            <div class="col-lg-12">
                                <center><h1 class="page-header">ข่าว</h1></center>
                            </div>
                        </div>
                        <c:forEach var="articlelist" items="${articlelist}" varStatus="loop">   
                            <div class="media column">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="media-left media-middle col-lg-4 ">
                                            <img src="data:image/jpg;base64, <c:out value='${articlelist.article_string}'/>"width="100%" />

                                        </div>
                                        <div class="media-body col-lg-8">
                                            <strong>${articlelist.title}</strong></br>${articlelist.contents}
                                        </div>
                                    </div>
                                </div>
                            </div>   
                        </c:forEach>
                        <div class="row">
                            <center>
                                <div class="pagination_center">
                                    <nav aria-label="Page navigation col-centered">
                                        <ul class="pagination">
                                            <c:if test="${page_priv == true}">  
                                                <li class="page-item"><a class="page-link"  href="${pageContext.request.contextPath}/?page=${page-1}"><i class="fa fa-angle-left  "></i></a></li>
                                                    </c:if>
                                                    <c:if test="${page_count != 0}">
                                                        <c:forEach begin="0" end="${page_count-1}" varStatus="loop">
                                                            <c:if test="${page == loop.index}"> 
                                                        <li class="page-item"><a class="page-link "  href="${pageContext.request.contextPath}/?page=${loop.index}">${loop.index+1}</a></li>
                                                        </c:if> 
                                                        <c:if test="${page != loop.index}"> 
                                                        <li class="page-item"><a class="page-link"  href="${pageContext.request.contextPath}/?page=${loop.index}">${loop.index+1}</a></li>
                                                        </c:if> 
                                                    </c:forEach>
                                                </c:if>
                                                <c:if test="${page_next == true}"> 
                                                <li class="page-item"><a class="page-link" href="${pageContext.request.contextPath}/?page=${page+1}"><i class="fa fa-angle-right "></i></a></li>
                                                    </c:if>
                                        </ul>
                                    </nav>
                                </div></center>
                        </div><!--end row-->
                        <BR> <BR></BR>
                    </div><!-- /col-xs-6 -->


                    <!-- jQuery -->
                    <script src="${pageContext.request.contextPath}/js/jquery.min.js"></script>  

                    <!-- Bootstrap Core JavaScript -->
                    <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>


                    <script>

                                        function initMap() {
                                            var map = new google.maps.Map(document.getElementById('map'), {
                                                zoom: 8,
                                                center: {lat: 18.8059154, lng: 98.9536562}
                                            });

                                            $.ajax({
                                                url: "${pageContext.request.contextPath}/gettemple",
                                                success: function (result) {
                                                    var marker = [];
                                                    marker = createVariables(result.length, "marker");
                                                    var infowindow = [];
                                                    infowindow = createVariables(result.length, "infowindow");
                                                    var i;
                                                    for (i = 0; i < result.length; i++) {

                                                        marker[i] = new google.maps.Marker({
                                                            position: {lat: parseFloat(result[i].latitude), lng: parseFloat(result[i].longitude)},
                                                            map: map,
                                                            title: result[i].name
                                                        });
                                                        var count = i;
                                                        var text = [];
                                                        text[i] = result[i].name + "<br/>" + result[i].address + "<br/>  " + result[i].district + "   " + result[i].prefecture + "<br/> " + result[i].province + "  " + result[i].postal_code;
                                                        infowindow[i] = new google.maps.InfoWindow({
                                                            content: text[count]
                                                        });

                                                        markerEvent(count, result[i].id);

                                                    }

                                                    function markerEvent(count, temple_id) {

                                                        marker[count].addListener('click', function () {

                                                            openInfowindow(map, this, infowindow, count, temple_id);

                                                        });
                                                    }

                                                }
                                            });
                                        }


                                        function openInfowindow(map, maker, infowindow, count, temple_id) {
                                            for (i = 0; i < infowindow.length; i++) {

                                                infowindow[i].close();
                                            }


                                            $("#select_temple").prop('selectedIndex', count + 1);
                                            infowindow[count].open(map, maker);

                                        }

                                        function createVariables(size, name) {

                                            var accounts = [];

                                            for (var i = 0; i < size; i++) {
                                                accounts[i] = name + i;

                                            }

                                            return accounts;
                                        }

                                        function submitForm() {
                                            var email = document.getElementById("inputEmail").value;
                                            var pass = document.getElementById("inputPassword").value;
                                            var temple = document.getElementById("temple_id").value;
                                            if (email == '') {
                                                alert('โปรดใส่อีเมล์');
                                            } else if (pass == '') {
                                                alert('โปรดใส่รหัสผ่าน')
                                            } else if (temple == '') {
                                                alert('โปรดเลือกวัด');
                                            } else {
                                                document.getElementById("mainform").submit();
                                            }
                                        }


                    </script>
                    <script async defer
                            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC7ROIXTKM7N0d6fNNn110jVyEbLvq5-rI&callback=initMap&language=th&region=TH">
                    </script>

                </body>
                </html>
                <jsp:include page="../footer.jsp" />
