<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html;charset=UTF-8" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
   <meta charset="utf-8">
   <meta name="viewport" content="width=device-width, initial-scale=1">
   <title> เข้าสู่ระบบ </title>
   <link href="${pageContext.request.contextPath}/css/bootstrap.css" rel="stylesheet">
   <link href="${pageContext.request.contextPath}/css/regis-form.css" rel="stylesheet" />
</head>
 <body>
    <div class="container">
       
        <div class="card card-container">
          
            <div class="row">
                <div class="col-lg-12">
                    <center><h1 class="page-header">สำหรับเจ้าหน้าที่ </h1></center>
                </div>
            </div>
            <c:if test="${status != null}">
                <center>
                 <p style="color: red;">ชื่อผู้ใช้หรือรหัสผ่านไม่ถูกต้อง !!!</p>   
                </center>                   
            </c:if>
   
            <form class="form-signup" id="mainform" action="loginstaff" method="post">
                <span id="reauth-email" class="reauth-email"></span>
                <input id="inputEmail" name="email" class="form-control" placeholder="อีเมล์" required="" autofocus="" type="email"/>
                <input id="inputPassword" name="password" class="form-control" placeholder="รหัสผ่าน" required="" type="password"/>
                <select id="temple_id" name="temple_id" id="select_temple" class="form-control">
                                <option  value="" disabled=" " selected="">เลือกรายการวัด</option>
                                <c:forEach var="temple" items="${temple}" varStatus="loop">
                                    <option  value="${temple.id}">${temple.name}</option>
                                </c:forEach>
                            </select><form:errors path="temple_id" /><br>
                            <button class="btn btn-lg btn-primary btn-block btn-signup" type="button" onclick="submitForm()">เข้าสู่ระบบ</button>
            </form><!-- /form -->
            ${pop}                                                       
            <center>
                 <a href="login">สำหรับสมาชิก</a>
            </center>
           
        </div><!-- /card-container -->
    </div><!-- /container -->
   <!-- jQuery -->
<script src="${pageContext.request.contextPath}/js/jquery.min.js"></script>  

<!-- Bootstrap Core JavaScript -->
<script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
<script>
function submitForm(){
    var email = document.getElementById("inputEmail").value;
    var pass = document.getElementById("inputPassword").value;
    var temple = document.getElementById("temple_id").value;
        if (email == '') {
            alert('โปรดใส่อีเมล์');
        } else if (pass == '') {
            alert('โปรดใส่รหัสผ่าน')
        } else if (temple == '') {
           alert('โปรดเลือกวัด');
        } else {
            document.getElementById("mainform").submit();
        }
}    
</script>
</body>
</html>
<jsp:include page="../footer.jsp" />