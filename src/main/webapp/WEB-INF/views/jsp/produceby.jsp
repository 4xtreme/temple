<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:include page="../header.jsp" />
<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="navbar-header">
        <a class="navbar-brand" href="  ">ระบบจองห้องศูนย์ปฏิบัติธรรม</a>
    </div>
    </nav>
<!-- Page Content -->
<br>
<style>
    .table23 {
    border-radius: 5px;
    width: 30%;
    margin: 0px auto;
    float: none;
}
</style>
    <div class="container-fluid">
        <!-- start Content -->
        <div class="row">
            <div class="col-lg-12" align="center">
                <h1 class="page-header ">คณะผู้จัดทำ </h1>
            </div>
        </div>
    </div>
    <!-- End Page Content -->
    <div class="container-fluid" >
    <div class="col-lg-12">
        <table class="table23">
        <tbody
        <thead>
            <th><u>บริษัท โฟร์เอ็กซ์ตรีม จำกัด</u></th>
        </thead>
            <tr>
                <td>นายชัชรินทร์</td>
                <td>ปิงเขียว</td>
                <td>ผู้จัดการ</td>
            </tr>
            <tr>
                <td>นายภูวิศ</td>
                <td>สุริวงค์ใย</td>
                <td>พัฒนาระบบ</td>
            </tr>
            <tr>
                <td>นายสุธี</td>
                <td>ใจกุลทรัพย์</td>
                <td>พัฒนาระบบ</td>
            </tr>
            <tr>
                <td>นางสาวเกษิณี</td>
                <td>พ่วงหงษ์</td>
                <td>ทดสอบระบบ</td>
            </tr>
            <tr>
                <td>นายพงศธร</td>
                <td>ศรีจันทร์</td>
                <td>ทดสอบระบบ</td>
            </tr>
            <tr>
                <td>นายกษิดิส</td>
                <td>เพิ่มผล</td>
                <td>ทดสอบระบบ</td>
            </tr>
        </tbody>
    </table>
        </div>
        </div>


<jsp:include page="../footer.jsp" />