var ganttData = [
	{
		id: 1, name: "Feature 1", series: [
			{ name: "Planned", start: new Date(2018,07,17), end: new Date(2018,07,20) },
			{ name: "Actual", start: new Date(2018,07,12), end: new Date(2010,07,13), color: "#f0f0f0" }
		]
	}, 
	{
		id: 2, name: "Feature 2", series: [
			{ name: "Planned", start: new Date(2018,07,15), end: new Date(2018,07,20) },
			{ name: "Actual", start: new Date(2018,07,16), end: new Date(2018,07,17), color: "#f0f0f0" },
			{ name: "Projected", start: new Date(2018,07,16), end: new Date(2018,07,17), color: "#e0e0e0" }
		]
	}, 
	{
		id: 3, name: "Feature 3", series: [
			{ name: "Planned", start: new Date(2018,07,11), end: new Date(2018,07,13) },
			{ name: "Actual", start: new Date(2018,07,15), end: new Date(2018,07,18), color: "#f0f0f0" }
		]
	}, 
	{
		id: 4, name: "Feature 4", series: [
			{ name: "Planned", start: new Date(2018,07,16), end: new Date(2018,07,30) },
			{ name: "Actual", start: new Date(2018,07,15), end: new Date(2018,07,30), color: "#f0f0f0" }
		]
	},
	{
		id: 5, name: "Feature 5", series: [
			{ name: "Planned", start: new Date(2018,07,11), end: new Date(2018,07,20) },
			{ name: "Actual", start: new Date(2018,07,10), end: new Date(2018,07,26), color: "#f0f0f0" }
		]
	}, 
	{
		id: 6, name: "Feature 6", series: [
			{ name: "Planned", start: new Date(2018,07,15), end: new Date(2018,07,20) },
			{ name: "Actual", start: new Date(2018,07,11), end: new Date(2018,07,16), color: "#f0f0f0" },
			{ name: "Projected", start: new Date(2018,07,11), end: new Date(2018,07,20), color: "#e0e0e0" }
		]
	}, 
	{
		id: 7, name: "Feature 7", series: [
			{ name: "Planned", start: new Date(2018,07,11), end: new Date(2018,07,12) }
		]
	}, 
	{
		id: 8, name: "Feature 8", series: [
			{ name: "Planned", start: new Date(2018,07,11), end: new Date(2018,07,15) },
			{ name: "Actual", start: new Date(2018,07,11), end: new Date(2018,07,15), color: "#f0f0f0" }
		]
	}
];