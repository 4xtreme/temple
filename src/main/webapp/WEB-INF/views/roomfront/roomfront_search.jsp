<%-- 
    Document   : roomfront
    Created on : Apr 23, 2018, 9:30:12 AM
    Author     : pop
--%>
<html>
    <head>
        <%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
        <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
        <%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
        <%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
        <%@page contentType="text/html" pageEncoding="UTF-8"%>
        <jsp:include page="../header.jsp" />
        <c:if test="${roleadmin == 'admin'}">
            <jsp:include page="../navigation.jsp" />
        </c:if>
        <c:if test="${roleadmin == 'user'}">
            <jsp:include page="../navigation_top.jsp" />
        </c:if>



        <style type="text/css">

            .container {

                position: relative;
                padding-left: 35px;
                margin-bottom: 12px;
                width:auto;

                -webkit-user-select: none;
                -moz-user-select: none;
                -ms-user-select: none;
                user-select: none;
            }

            .checkmark {
                position: absolute;
                top: 0;
                left: 0;
                height: 20px;
                width: 20px;
                background-color: #eee;
            }

            /* Style the tab */
            .tab {
                overflow: hidden;
                border: 1px solid #ccc;
                background-color: #f1f1f1;
            }

            /* Style the buttons inside the tab */
            .tab button {
                background-color: inherit;

                border: none;
                outline: none;
                cursor: pointer;
                padding: 14px 16px;
                transition: 0.3s;
                font-size: 17px;
            }

            /* Change background color of buttons on hover */
            .tab button:hover {
                background-color: #ddd;
            }

            /* Create an active/current tablink class */
            .tab button.active {
                color: white;
                background-color: #add8e6;
            }

            /* Style the tab content */
            .tabcontent {
                display: none;
                padding: 6px 12px;
                border: 1px solid #ccc;
                border-top: none;
                max-height: 410px;
                overflow: auto;
            }

            /*** PANEL INFO ***/
            .with-nav-tabs.panel-info .nav-tabs > li > a,
            .with-nav-tabs.panel-info .nav-tabs > li > a:hover,
            .with-nav-tabs.panel-info .nav-tabs > li > a:focus {
                color: #31708f;
            }
            .with-nav-tabs.panel-info .nav-tabs > .open > a,
            .with-nav-tabs.panel-info .nav-tabs > .open > a:hover,
            .with-nav-tabs.panel-info .nav-tabs > .open > a:focus,
            .with-nav-tabs.panel-info .nav-tabs > li > a:hover,
            .with-nav-tabs.panel-info .nav-tabs > li > a:focus {
                color: #31708f;
                background-color: #bce8f1;
                border-color: transparent;
            }
            .with-nav-tabs.panel-info .nav-tabs > li.active > a,
            .with-nav-tabs.panel-info .nav-tabs > li.active > a:hover,
            .with-nav-tabs.panel-info .nav-tabs > li.active > a:focus {
                color: #31708f;
                background-color: #fff;
                border-color: #bce8f1;
                border-bottom-color: transparent;
            }
            .with-nav-tabs.panel-info .nav-tabs > li.dropdown .dropdown-menu {
                background-color: #d9edf7;
                border-color: #bce8f1;
            }
            .with-nav-tabs.panel-info .nav-tabs > li.dropdown .dropdown-menu > li > a {
                color: #31708f;   
            }
            .with-nav-tabs.panel-info .nav-tabs > li.dropdown .dropdown-menu > li > a:hover,
            .with-nav-tabs.panel-info .nav-tabs > li.dropdown .dropdown-menu > li > a:focus {
                background-color: #bce8f1;
            }
            .with-nav-tabs.panel-info .nav-tabs > li.dropdown .dropdown-menu > .active > a,
            .with-nav-tabs.panel-info .nav-tabs > li.dropdown .dropdown-menu > .active > a:hover,
            .with-nav-tabs.panel-info .nav-tabs > li.dropdown .dropdown-menu > .active > a:focus {
                color: #fff;
                background-color: #31708f;
            }

            #head_living{
                margin: 0;
                margin-bottom: 15px;
                text-align: center;
                font-weight: 600;
            }
        </style>
    </head>
    <body>


        <div id="page-wrapper">
            <div class="container-fluid">
                <!-- start Content -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header" id="title_head">แผนกต้อนรับส่วนหน้า  </h1>
                    </div>
                </div>
                <div class="row" >
                    <div class="panel with-nav-tabs panel-info">
                        <div class="panel-heading">
                            <ul class="nav nav-tabs">
                                <li class="active"><a href="#tab1info" data-toggle="tab">สถานะประจำวัน</a></li>
                                <li><a href="#tab2info" data-toggle="tab">ค้นหาผู้ปฏิบัติธรรม</a></li>
                                <!--<li><a href="#tab3info" data-toggle="tab">Info 3</a></li>-->
                                <!--                                <li class="dropdown">
                                                                    <a href="#" data-toggle="dropdown">Dropdown <span class="caret"></span></a>
                                                                    <ul class="dropdown-menu" role="menu">
                                                                        <li><a href="#tab4info" data-toggle="tab">Info 4</a></li>
                                                                        <li><a href="#tab5info" data-toggle="tab">Info 5</a></li>
                                                                    </ul>
                                                                </li>-->
                            </ul>
                        </div>
                        <div class="panel-body">
                            <div class="tab-content">
                                <div class="tab-pane fade in active" id="tab1info">
                                    <div class="col-lg-12"style="text-align: center;">
                                        <div class="row" >
                                            <label class="container">จอง
                                                <span class="checkmark" style="background-color:#91d3ff "></span>
                                            </label>

                                            <label class="container">เช็คอิน
                                                <span class="checkmark" style="background-color:#41f477 "></span>
                                            </label>

                                            <label class="container">เช็คเอาท์

                                                <span class="checkmark" style="background-color:#f7e0b9 "></span>
                                            </label>


                                            <label class="container">อาคาร/กุฏิ</label>
                                            <select id="select_builder" class="form-control width_20">
                                                <option value="0" disabled="" selected="">เลือกอาคาร</option> 
                                                <c:forEach var="builder_list" items="${builder}" varStatus="loop">
                                                    <option value="${builder_list.id}">${builder_list.name}</option>
                                                </c:forEach>
                                            </select>
                                            <label class="container">ห้อง</label>
                                            <select id="select_room"class="form-control width_15">
                                                <option value="0"  selected="">ทั้งหมด</option> 
                                            </select>
                                            <button onclick="getData()" class="btn btn-primary btn-signup width_10 align-center" id='getdata_btn'>ค้นหารายการทั้งหมด</button> 
                                        </div>
                                    </div>
                                    <!--                                    <div class="col-lg-12 text-center">
                                                                            <button onclick="getData()" id='getdata_btn'>รายการทั้งหมด</button> 
                                                                        </div>-->
                                </div>

                                <div class="tab-pane fade" id="tab2info">
                                    <div class="row" >
                                        <div class="col-lg-12" style="text-align: center;">
                                            <div class="row" >

                                                <form action="searchpeople" id="searchpeople" method="POST">
                                                    <div>
                                                        <label>ค้นหาผู้ปฏิบัติธรรม</label>
                                                        &nbsp;&nbsp;
                                                        <select class="form-control search-person" name="typeSearch">
                                                            <option value="firstname">ชื่อ</option>
                                                            <option value="phone">หมายเลขโทรศัพท์</option>
                                                            <option value="card">หมายเลขบัตรประชาชน</option>
                                                        </select>&nbsp;
                                                        <input type="text" class="form-control search-person-txt" id="message" name="message">&nbsp;
                                                        <button type="button" onclick="checkSearchData()" class="btn btn-primary btn-signup width_10 align-center">
                                                            <i class="glyphicon glyphicon-search"></i> ค้นหา
                                                        </button>
                                                    </div>
                                                </form>

                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <!--                                <div class="tab-pane fade" id="tab3info">Info 3</div>
                                                                <div class="tab-pane fade" id="tab4info">Info 4</div>
                                                                <div class="tab-pane fade" id="tab5info">Info 5</div>-->
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row" >  

                    <c:if test="${roleadmin == 'admin'}">
                        <div class="col-lg-4" id="overall_view">
                        </c:if>
                        <c:if test="${roleadmin == 'user'}">
                            <div class="col-lg-4 col-lg-offset-1" id="overall_view">
                            </c:if>
                            <div class="row text-center" style="border: 1px solid;" >
                                <span class="label_title "><label>สถานะประจำวัน</label></span><br/>
                                <span ><label id="curDate"></label></span>
                                <div class="tab">      
                                    <button class="tablinks" onclick="openCity(event, 'reserve')">จอง</button>
                                    <button class="tablinks active" id="view_checkin_btn"  onclick="openCity(event, 'lived')">เช็คอิน</button>
                                    <button class="tablinks" onclick="openCity(event, 'exit')">เช็คเอาท์</button>
                                </div>

                                <div id="reserve" class="tabcontent">
                                    <table class="table table-striped table-bordered table-hover" style="overflow: scroll">
                                        <thead id="reserve_head">
                                            <tr>
                                                <th class="text-center">ชื่อ</th>
                                                <th class="text-center">ห้อง</th>
                                                <th class="text-center">แก้ไข</th>
                                            </tr>
                                        </thead>
                                        <tbody id="reserve_body">

                                        </tbody>
                                    </table>
                                </div>

                                <div id="lived" class="tabcontent">
                                    <table class="table table-striped table-bordered table-hover">
                                        <thead id="lived_head">
                                            <tr>
                                                <th class="text-center">ห้อง</th>
                                                <th class="text-center">จำนวนคนอยู่</th>
                                            </tr>
                                        </thead>
                                        <tbody id="lived_body">

                                        </tbody>
                                    </table>
                                </div>

                                <div id="exit" class="tabcontent">
                                    <table class="table table-striped table-bordered table-hover">
                                        <thead id="exit_head">
                                            <tr>
                                                <th class="text-center">ชื่อ</th>
                                                <th class="text-center">ห้อง</th>

                                            </tr>
                                        </thead>
                                        <tbody id="exit_body">

                                        </tbody>

                                    </table>

                                </div>

                            </div>
                            <br/><br/>
                            <h5 id="head_living">ผู้ปฏิบัติธรรมที่เข้าพัก</h5>
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th class="text-center">ชื่อ - นามสกุล</th>
                                            <th class="text-center">ชื่ออาคาร</th>
                                            <th class="text-center">ชื่อห้อง</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <c:if test="${living != ''}">
                                            <c:forEach var="living" items="${living}" varStatus="loop">
                                                <tr>
                                                    <td class="text-center">${living.firstname} &nbsp;&nbsp;${living.lastname}</td>
                                                    <td class="text-center">${living.builder_name}</td>
                                                    <td class="text-center">${living.room_name}</td>
                                                </tr>
                                            </c:forEach>
                                        </c:if>
                                        <c:if test="${living == []}">
                                            <tr>
                                                <td colspan="3" style="text-align: center;"><span style="color:red;"> ไม่พบข้อมูลผู้ปฏิบัติธรรม โปรดตรวจสอบข้อมูลอีกครั้ง</span></td>
                                            </tr>
                                        </c:if>
                                    </tbody>
                                </table>
                            </div>              
                        </div>
                        <c:if test="${roleadmin == 'admin'}">
                            <div class="col-lg-8" id="overall_view" style="margin-bottom: 30px;" >
                            </c:if>
                            <c:if test="${roleadmin == 'user'}">
                                <div class="col-lg-7" id="overall_view" style="margin-bottom: 30px;" >
                                </c:if>
                                <div id="ganttChart" ></div>
                            </div>
                        </div>
                    </div>
                </div>

                <script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-1.4.2.js"></script>
                <script type="text/javascript" src="${pageContext.request.contextPath}/js/date.js"></script>
                <script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-ui-1.8.4.js"></script>
                <script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery.ganttView.js"></script>       

                <script type="text/javascript">

                                        /*ขึ้นหัวข้อวันที่ตาราง*/
                                        $(document).ready(function () {
                                            var options = {weekday: 'long', year: 'numeric', month: 'long', day: 'numeric'};
                                            $("#curDate").text(new Date().toLocaleDateString('th-TH', options));
                                            $("#overall_view").show();
                                            document.getElementById("lived").style.display = "block";
                                            $("#select_room").show();
                                            $("#select_builder").show();
                                            $("#getdata_btn").show();
                                            $("#select_builder").on("change", function () {
                                                $.ajax({
                                                    url: "${pageContext.request.contextPath}/roomfront/getroom/" + $("#select_builder").val(),
                                                    success: function (result) {
                                                        $("#getdata_btn").show();
                                                        $("#select_room").html('<option value="0" selected="">ทั้งหมด</option> ');
                                                        $("#select_room").show();
                                                        for (i = 0; i < result.length; i++) {
                                                            $("#select_room").append('<option value="' + result[i].id + '">' + result[i].name + '</option>');
                                                        }
                                                    }
                                                });
                                            });
                                        });
                                        function getData() {
                                            if ($("#select_builder").val() === null) {
                                                alert('กรุณาเลือกอาคาร');
                                                $("#select_builder").focus();
                                            } else if ($("#select_room").val() == 0) {
                                                getDataByBuilder();
                                            } else {
                                                getDataByRoom();
                                            }

                                        }
                                        /*การจอง  แสดงจำนวนคนที่จอง*/
                                        function getDataByBuilder() {
                                            $.ajax({
                                                url: "${pageContext.request.contextPath}/roomfront/getcheckins/" + $("#select_builder").val() + "/" + 0,
                                                success: function (data) {
                                                    var options = {year: 'numeric', month: 'numeric', day: 'numeric'};
                                                    $("#overall_view").show();
                                                    $("#reserve_body").html("");
                                                    $("#lived_body").html("");
                                                    $("#exit_body").html("");
                                                    $("#reserve_head").html("<tr><td>" + "ชื่อ" + "</td><td>" + "ห้อง" + "</td><td>" + "แก้ไข" + "</td></tr>");
                                                    $("#lived_head").html("<tr><td>" + "ห้อง" + "</td><td>" + "จำนวนคนอยู่" + "</td></tr>");
                                                    $("#exit_head").html("<tr><td>" + "ชื่อ" + "</td><td>" + "ห้อง" + "</td></tr>");
                                                    var overview = [];
                                                    var series = [];
                                                    var color = "";
                                                    for (room_name = 0; room_name < data.room_name.length; room_name++) {
                                                        $("#lived_body").append("<tr><td>" + data.room_name[room_name].name + "</td><td>" + data.room_name[room_name].live + "/" + data.room_name[room_name].amount + "</td></tr>");
                                                    }
                                                    for (var i = 0; i < data.room_name.length; i++) {

                                                        for (var j = 0; j < data.overview.length; j++) {
                                                            if (data.room_name[i].id == data.overview[j].room_name_id) {
                                                                var check_in_date = new Date(data.overview[j].checkin_year, data.overview[j].checkin_month - 1, data.overview[j].checkin_day);
                                                                var check_out_date = new Date(data.overview[j].checkout_year, data.overview[j].checkout_month - 1, data.overview[j].checkout_day);
                                                                var curdate = new Date();
                                                                curdate.setHours(00);
                                                                curdate.setMinutes(00);
                                                                curdate.setSeconds(00);
                                                                if (data.overview[j].type == 0) {
                                                                    color = "#91d3ff";
                                                                    $("#reserve_body").append("<tr><td>" + data.overview[j].contact_name + "</td><td>" + data.overview[j].room_name + "</td><td><button id='cancel_btn" + data.overview[j].id + "'" + "  onclick='cancel_reserve(" + data.overview[j].id + ")'>ยกเลิกกการจอง</button></td></tr>");
                                                                    if (check_in_date >= curdate) {
                                                                        $("#cancel_btn" + data.overview[j].id).remove("disabled");
                                                                    }

                                                                } else if (data.overview[j].type == 1) {
                                                                    color = "#41f477";
                                                                    //genGrant(overview);
                                                                    $('#lived_head').html("");
                                                                    $('#lived_head').append("<tr><th>ห้อง</th><th>จำนวนคนอยู่</th></tr>");
                                                                    if (check_in_date >= curdate) {
                                                                        $("#lived_body").append("<tr><td>" + data.overview[j].contact_name + "</td><td>" + data.overview[j].room_name + "</td></tr>");
                                                                    }
                                                                } else if (data.overview[j].type == 2) {
                                                                    color = "#f7e0b9";
                                                                    $("#exit_body").append("<tr><td>" + data.overview[j].contact_name + "</td><td>" + data.overview[j].room_name + "</td></tr>");
                                                                    //  if (check_in_date <= curdate) {
                                                                    //  $("#exit_body").append("<tr><td>" + data.overview[j].contact_name + "</td><td>" + data.overview[j].room_name + "</td></tr>");                                   
                                                                    //  }
                                                                }
                                                                series.push(
                                                                        {
                                                                            name: data.overview[j].contact_name,
                                                                            start: new Date(data.overview[j].checkin_year),
                                                                            end: new Date(data.overview[j].checkout_year),
                                                                            color: color
                                                                        });
                                                            }

                                                        }
                                                        if (series.length != 0) {
                                                            overview.push(
                                                                    {
                                                                        id: i, name: data.room_name[i].name, series: series
                                                                    }
                                                            );
                                                        }

                                                        series = [];
                                                    }
                                                    genGrant(overview);
                                                }
                                            });
                                        }
                                        function getDataByRoom() {
                                            $.ajax({
                                                url: "${pageContext.request.contextPath}/roomfront/getcheckins/" + $("#select_room").val() + "/" + 1,
                                                success: function (data) {
                                                    var options = {year: 'numeric', month: 'numeric', day: 'numeric'};
                                                    $("#overall_view").show();
                                                    $("#reserve_body").html("");
                                                    $("#lived_body").html("");
                                                    $("#exit_body").html("");
                                                    $("#reserve_head").html("<tr><td>" + "ชื่อ" + "</td><td>" + "ห้อง" + "</td><td>" + "แก้ไข" + "</td></tr>");
                                                    $("#lived_head").html("<tr><td>" + "ห้อง" + "</td><td>" + "จำนวนคนอยู่" + "</td></tr>");
                                                    $("#exit_head").html("<tr><td>" + "ชื่อ" + "</td><td>" + "ห้อง" + "</td></tr>");
                                                    var overview = [];
                                                    var series = [];
                                                    var color = "";
                                                    for (room_name = 0; room_name < data.room_name.length; room_name++) {
                                                        $("#lived_body").append("<tr><td>" + data.room_name[room_name].name + "</td><td>" + data.room_name[room_name].live + "/" + data.room_name[room_name].amount + "</td></tr>");
                                                    }
                                                    for (var i = 0; i < data.room.length; i++) {
                                                        for (var j = 0; j < data.overview.length; j++) {
                                                            if (data.room[i].id == data.overview[j].room_id) {
                                                                var check_in_date = new Date(data.overview[j].checkin_year, data.overview[j].checkin_month - 1, data.overview[j].checkin_day);
                                                                var check_out_date = new Date(data.overview[j].checkout_year, data.overview[j].checkout_month - 1, data.overview[j].checkout_day);
                                                                var curdate = new Date();
                                                                curdate.setHours(00);
                                                                curdate.setMinutes(00);
                                                                curdate.setSeconds(00);
                                                                if (data.overview[j].type == 0) {
                                                                    color = "#91d3ff";
                                                                    $("#reserve_body").append("<tr><td>" + data.overview[j].contact_name + "</td><td>" + data.overview[j].room_name + "</td><td><button id='cancel_btn" + data.overview[j].id + "'" + " disabled='' onclick='cancel_reserve(" + data.overview[j].id + ")'>ยกเลิกกการจอง</button></td></tr>");
                                                                    if (check_in_date >= curdate) {
                                                                        $("#cancel_btn" + data.overview[j].id).remove("disabled");
                                                                    }
                                                                } else if (data.overview[j].type == 1) {
                                                                    color = "#41f477";
                                                                    //if (check_in_date <= curdate) {
                                                                    $('#lived_head').html("");
                                                                    $('#lived_head').append("<tr><th>ชื่อ</th><th>ห้อง</th></tr>");
                                                                    $("#lived_body").append("<tr><td>" + data.overview[j].contact_name + "</td><td>" + data.overview[j].room_name + "</td></tr>");
                                                                    //}

                                                                } else if (data.overview[j].type == 2) {
                                                                    color = "#f7e0b9";
                                                                    $("#exit_body").append("<tr><td>" + data.overview[j].contact_name + "</td><td>" + data.overview[j].room_name + "</td></tr>");
                                                                    //if (check_in_date >= curdate) {
                                                                    // $("#exit_body").append("<tr><td>" + data.overview[j].contact_name + "</td><td>" + data.overview[j].room_name + "</td></tr>");                                   
                                                                    // }
                                                                }
                                                                series.push(
                                                                        {
                                                                            name: data.overview[j].contact_name,
                                                                            start: new Date(data.overview[j].checkin_year),
                                                                            end: new Date(data.overview[j].checkout_year),
                                                                            color: color
                                                                        });
                                                            }

                                                        }
                                                        if (series.length != 0) {
                                                            overview.push(
                                                                    {
                                                                        id: i,
                                                                        name: data.room[i].name,
                                                                        series: series

                                                                    }
                                                            );
                                                        }
                                                        series = [];
                                                    }
                                                    genGrant(overview);
                                                }
                                            });
                                        }
                                        /*เพิ่มสีลงปฏิทิน*/
                                        function genGrant(overview) {
                                            $("#ganttChart").html("");
                                            $("#ganttChart").ganttView({
                                                data: overview,
                                                slideWidth: '600',
                                                behavior: {
                                                    onClick: function (data) {
                                                        var msg = "You clicked on an event: { start: " + data.start.toString("M/d/yyyy") + ", end: " + data.end.toString("M/d/yyyy") + " }";
                                                        console.log(msg);
                                                    },
                                                    onResize: function (data) {
                                                        var msg = "You resized an event: { start: " + data.start.toString("M/d/yyyy") + ", end: " + data.end.toString("M/d/yyyy") + " }";
                                                        $("#eventMessage").text(msg);
                                                    },
                                                    onDrag: function (data) {
                                                        var msg = "You dragged an event: { start: " + data.start.toString("M/d/yyyy") + ", end: " + data.end.toString("M/d/yyyy") + " }";
                                                        $("#eventMessage").text(msg);
                                                    }
                                                }
                                            });
                                        }

                                        function openCity(evt, tabName) {
                                            var i, tabcontent, tablinks;
                                            tabcontent = document.getElementsByClassName("tabcontent");
                                            for (i = 0; i < tabcontent.length; i++) {
                                                tabcontent[i].style.display = "none";
                                            }
                                            tablinks = document.getElementsByClassName("tablinks");
                                            for (i = 0; i < tablinks.length; i++) {
                                                tablinks[i].className = tablinks[i].className.replace(" active", "");
                                            }
                                            document.getElementById(tabName).style.display = "block";
                                            evt.currentTarget.className += "active";
                                        }


                                        function cancel_reserve(checkin_id) {
                                            var check = confirm("คุณแน่ใจที่จะยกเลิกการจองรายการนี้");
                                            if (check == true) {
                                                $.ajax({
                                                    url: "${pageContext.request.contextPath}/roomfront/cancelreserve/" + checkin_id,
                                                    success: function (result) {
                                                        $("#getdata_btn").click();
                                                    }
                                                });
                                            }

                                        }

                                        function checkSearchData() {
                                            var txt = $('#message').val();
                                            if (txt == '') {
                                                alert("กรุณาระบุข้อมูลที่ต้องการค้นหา");
                                                $('#message').focus();
                                            } else {
                                                $("#searchpeople").submit();
                                            }
                                        }

                </script>
                </body>
                <jsp:include page="../footer.jsp" />
                <html>
