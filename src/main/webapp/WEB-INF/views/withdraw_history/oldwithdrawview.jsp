<%-- 
    Document   : oldwithdrawview
    Created on : Dec 6, 2017, 11:34:17 AM
    Author     : pop
--%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:include page="../header.jsp" />
<jsp:include page="../navigation.jsp" />
<!-- Page Content -->
<div id="page-wrapper">
    <div class="container-fluid">
        <!-- start Content -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header"><spring:message code="header.withdraw"/></h1>
            </div>
        </div>
        <div class="row" id="eiei">
            <div class="col-lg-12">
                <div class="tab-content">
                    <div class="tab-pane fade active in">
                        <div class="row">
                            <div class="col-lg-6" >
                                <div class="dl-horizontal">
                                    <dt>
                                        <div class="form-group ">
                                            <span class="label_title">ผู้ขอเบิก</span>
                                        </div>
                                    </dt>
                                    <dd>
                                        <div class="form-group text-left">
                                            <input class="no-border tran-bg" disabled="" value="${withdrawal.fristname}    ${withdrawal.lastname}"/>
                                        </div>
                                    </dd>
                                    <dt>
                                        <div class="form-group">
                                            <span class="label_title">คนขับ/ควบคุมเครื่อง</span>
                                        </div>
                                    </dt>
                                    <dd>
                                        <div class="form-group">
                                            <input class="no-border tran-bg" disabled="" value="${driver.fristname}    ${driver.lastname}"/>
                                        </div>
                                    </dd>
                                    <dt>
                                        <div class="form-group">
                                            <span class="label_title">จำวนวเงิน</span>
                                        </div>
                                    </dt>
                                    <dd>
                                        <div class="form-group">
                                            <input class="no-border tran-bg width_15" disabled=""value="${oldwithdraw.amount}" />
                                            <span>บาท</span>
                                        </div>
                                    </dd>
                                    <dt>
                                        <div class="form-group">
                                            <span class="label_title">ใช้ในโครงการ</span>
                                        </div>
                                    </dt>
                                    <dd>
                                        <div class="form-group">
                                            <input class="no-border tran-bg" disabled="" value="${oldwithdraw.project_id}" />
                                        </div>
                                    </dd>
                                    <dt>
                                        <div class="form-group">
                                            <span>วิธีจ่ายเงิน</span>
                                        </div>
                                    </dt>
                                    <dd>
                                        <div class="form-group">
                                            <input class="no-border tran-bg" disabled="" id="pay_type"  value="${oldwithdraw.pay_type}"/>
                                        </div>
                                    </dd>
                                    <dt>
                                        <div class="form-group">
                                            <span>กำหนดชำระ</span>
                                        </div>
                                    </dt>
                                    <dd>
                                        <div class="form-group">
                                            <input class="no-border tran-bg" disabled="" value="${oldwithdraw.pay_date}"/>
                                        </div>
                                    </dd>
                                    <dt>
                                        <div class="form-group">
                                            <span>จ่ายให้กับ</span>

                                        </div>
                                    </dt>
                                    <dd>
                                        <div class="form-group">
                                            <input class="no-border tran-bg" disabled="" value="${oldwithdraw.store_id}" />
                                            <p >asdascasdas</p>
                                        </div>
                                    </dd>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="dl-horizontal">
                                    <dt>
                                        <div class="form-group">
                                            <span> วันที่   :</span>
                                        </div>
                                    </dt>
                                    <dd>
                                        <div class="form-group">
                                            <input type="text" path="" class="no-border tran-bg " disabled="" value="${oldwithdraw.update_date}"/>
                                        </div>
                                    </dd>
                                    <dt>
                                        <div class="form-group">
                                            <span>หมายเลขใบเบิก : </span>
                                        </div>
                                    </dt>
                                    <dd>
                                        <div class="form-group">
                                            <input type="text" value="${oldwithdraw.withdraw_id}" class="no-border tran-bg "disabled=""/>
                                        </div>
                                    </dd>
                                    <dt>
                                        <div class="form-group">
                                            <span>ผู้กรอกข้อมูล : </span>
                                        </div>
                                    </dt>
                                    <dd>
                                        <div class="form-group">
                                            <input type="text" path="" class="no-border tran-bg" disabled="" value="${oldwithdraw.user_id}"/>
                                        </div>
                                    </dd>
                                    <dt>
                                        <div class="form-group">
                                            <span>สถานะ : </span>
                                        </div>
                                    </dt>
                                    <dd>
                                        <div class="form-group">
                                            <input type="text" path="" class="no-border tran-bg " value="${oldwithdraw.status}" disabled=""/>
                                        </div>
                                    </dd>

                                </div>
                            </div>
                        </div>


                        <div class="col-lg-12">
                            <div class="form-group">
                                <span>สำหรับเครื่องจักร หมายเลขทะเบียน/รหัส</span>
                                <input type="text" name="searchnum" form="search_machine" id="search_input" />
                                <i type="button" onclick="searchMachine()"   class="fa fa-search" style="font-size:25px; color: skyblue;"></i>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            </div>
                            <div class="form-group">
                                <input path="machine_id" id="machineId" hidden="true" value=""/>
                                <input type="text" id="machineInfo" class="margin_left10 width_15" disabled="disable"value=""/>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <span>หมวดเครื่องจัก : </span> 
                                <input type="text" id="machineCategory" class="no-border tran-bg " disabled="disable"value="" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <span>อายุใช้งาน : </span>
                                <input type="text" path="" class="no-border tran-bg" disabled="disable"value="3/4/20"/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <span>ที่ตั้งปัจจุบัน : </span>
                                <input type="text" path="" class="no-border tran-bg" disabled="disable"value="โครงการ XYZ"/>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-10">
                            <div class="table-responsive" id="app1">
                                <table class="table table-striped table-bordered table-hover del-line" id="mytable">
                                    <thead >
                                        <tr >
                                            <th class="text-center">ลำดับ</th>
                                            <th class="text-center">อุปกรณที่เสีย</th>
                                            <th class="text-center">ประเภทการเสียซ่อม</th>
                                            <th class="text-center">จำนวน</th>
                                            <th class="text-center">ราคา/หน่วย</th>
                                            <th class="text-center">ราคารวม</th>
                                            <th class="text-center">ยี่ห่อ/หมายเหตุ</th>
                                            <th class="text-center" hidden="true" style="border: none"></th>
                                            <th class="text-center"hidden="true" style="border: none"></th>
                                        </tr>
                                    </thead>

                                    <tbody   id="tableToModify" >


                                    </tbody>
                                    <input name="itemslist" type="hidden" value="0,0,0,0,0,0,0">
                                </table>
                            </div>
                            <div class="form-group">
                                <div class="row margin_top10"> 
                                    <div class="col-xs-6 " style="margin-left: 15px">
                                        <label class="fileContainer">
                                            <i class="fa fa-link fa-flip-horizontal attach-file" aria-hidden="true"></i>
                                            <input type="file" id="uploadBtn" path="withdraw_file"  name="file_withdraw"  />
                                        </label>
                                        <span style="line-height: 60px">แนบไฟล์</span></br>
                                        <input id="uploadFile" class="form-control-sm no-border width_50 file_dir2" type="text" value="No file selected."  disabled="disabled" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<form:errors path="withdraw_file" /><br>
                                    </div>
                                    <div class="col-xs-5">
                                        <strong>
                                            <span>รวมทั้งสิ้น  </span>
                                            <input id="totalPrice" type="text" path="" class="no-border tran-bg-right width_15" disabled="disable"value="0"/>
                                            <input hidden="true" id="totalPrice2" type="text" path="total_price" value="0"/>
                                            <span>  บาท  </span>
                                        </strong>
                                        </br>
                                        </br>
                                    </div>		
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <c:if test="${login_user.role  == 'user'}">
                            <center>
                                <div class="form-group">
                                    <span>บันทึกผู้ขอเบิก&nbsp;</span>
                                    <textarea  class="custom-textarea"></textarea>
                                </div>
                            </center>
                        </c:if>  
                        <c:if test="${login_user.role  == 'money'}">
                            <center>

                                <div class="form-group">
                                    <div style="width: 120px;display: inline-block">
                                        <span >บันทึกผู้ตรวจสอบ / พนักงานการเงิน</span>
                                    </div>
                                    <textarea  class="custom-textarea"></textarea>
                                </div>
                            </center>
                        </c:if>
                        <c:if test="${login_user.role  == 'admin'}">
                            <center>
                                <div class="form-group">
                                    <span>บันทึกผู้อนุมัติ &nbsp;</span>
                                    <extarea  class="custom-textarea"></textarea>
                                </div>
                            </center>
                        </c:if>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Content -->

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js" ></script>  
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/uikit/3.0.0-beta.20/js/uikit.min.js"></script>
<script>
                                    $('#pay_type').ready(function () {
                                        var pay_value = $('#pay_type');
                                        switch (pay_value.val()) {
                                            case '1':
                                                pay_value.val("A");
                                                break;
                                            case '2':
                                                pay_value.val("B");
                                                break;
                                            case '3':
                                                pay_value.val("C");
                                                break;
                                            case '4':
                                                pay_value.val("D");
                                                break;
                                        }
                                    });
</script>
