<%-- 
    Document   : oldwithdrawlist
    Created on : Dec 6, 2017, 10:26:22 AM
    Author     : pop
--%>

<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:include page="../header.jsp" />
<jsp:include page="../navigation.jsp" />

<div id="page-wrapper">
    <div class="container-fluid">
        <!-- start Content -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">oldwithdrawHistory</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="table-responsive" >
                    <table class="table table-striped table-bordered table-hover">
                        <thead>
                            <tr>
                                <th class="text-center">Withdraw ID</th>
                                <th class="text-center">Status</th>
                                <th class="text-center">Last update</th>
                                <th class="text-center">More Detail</th>
                            </tr>
                        </thead>
                        <tbody>
                            <c:if test="${oldwithdraws != null}">  
                                <c:forEach var="oldwithdraws" items="${oldwithdraws}" varStatus="loop">
                                    <tr>
                                        <td class="text-center">${oldwithdraws.withdraw_id}</td>
                                        <td class="text-center">
                                            <input  disabled="true" class="no-border tran-bg width_15 text-center" value="${oldwithdraws.status}"/>
                                        </td>
                                        <td class="text-center">${oldwithdraws.update_date}</td>
                                        <td class="text-center">
                                            <form  action="oldwithdrawview" method="POST" >                                     
                                                <button type="submit" value="${oldwithdraws.id}" name="oldwithdrawid" class="no-border tran-bg"><i class="fa fa-edit edit-btn" style="font-size: 30px"></i></button>
                                            </form>  
                                        </td>
                                    </tr>
                                </c:forEach>
                            </c:if>
                        </tbody>
                    </table>
                </div>
                <div class="row">
                    <center>
                        <div class="pagination_center">
                            <nav aria-label="Page navigation col-centered">
                                <ul class="pagination">
                                    <c:if test="${page_priv == true}">  
                                        <li class="page-item"><a class="page-link"  href="${pageContext.request.contextPath}/oldwithdrawlist?page=${page-1}"><i class="fa fa-angle-left  "></i></a></li>
                                            </c:if>
                                            <c:if test="${page_count != 0}">
                                                <c:forEach begin="0" end="${page_count-1}" varStatus="loop">
                                                    <c:if test="${page == loop.index}"> 
                                                <li class="page-item"><a class="page-link active"  href="${pageContext.request.contextPath}/oldwithdrawlist?page=${loop.index}">${loop.index+1}</a></li>
                                                </c:if> 
                                                <c:if test="${page != loop.index}"> 
                                                <li class="page-item"><a class="page-link"  href="${pageContext.request.contextPath}/oldwithdrawlist?page=${loop.index}">${loop.index+1}</a></li>
                                                </c:if> 
                                            </c:forEach>
                                        </c:if>
                                        <c:if test="${page_next == true}"> 
                                        <li class="page-item"><a class="page-link" href="${pageContext.request.contextPath}/oldwithdrawlist?page=${page+1}"><i class="fa fa-angle-right "></i></a></li>
                                            </c:if>
                                </ul>
                            </nav>
                        </div>
                </div><!--end row-->
            </div>
        </div>
    </div><!-- End Content -->    
</div><!-- End Page Content -->
<script src="https://unpkg.com/vue"></script>
<script>

    $(document).ready(function () {

        $("tr td input").each(function () {
            switch ($(this).val()) {
                case '1':
                    $(this).val("ร่าง");
                    break;
                case '2':
                    $(this).val("รออนุมัติ");
                    break;
                case '3':
                    $(this).val(" ไม่อนุมัติ");
                    break;
                case '4':
                    $(this).val("ยกเลิก");
                    break;
                case '5':
                    $(this).val("จ่ายเงินแล้ว");
                    break;
                case '6':
                    $(this).val("ขอเบิก");
                    break;
            }
        });
    });

    function loadRow() {
        alert("status");
    }



</script>
<jsp:include page="../footer.jsp" />
